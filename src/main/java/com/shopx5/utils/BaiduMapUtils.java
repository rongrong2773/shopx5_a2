package com.shopx5.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;


import net.sf.json.JSONObject;

/**
 * 根据地质查出经纬度;根据经纬度查出地址
 * @author sjx
 * 2019年7月26日 下午5:53:34
 */
public class BaiduMapUtils {
	
	/****/
	private static final String URL = "http://api.map.baidu.com/reverse_geocoding/v3/";
	/** 用户申请注册的key，自v2开始参数修改为“ak”，之前版本参数为“key” **/
	private static final String AK = "ftcRS69koxpeKxiW8hfER8LV1MMLmVYw";
	/** 若用户所用ak的校验方式为sn校验时该参数必须sn生成 **/
	private static final String SK = "";
	
	private BaiduMapUtils() {
		throw new AssertionError("不能产生实例");
	}
	
	public static void main(String[] args) {
		JSONObject obj = getCity("34.27", "108.93").getJSONObject("result");
		System.out.println("地区编码："+obj.getJSONObject("addressComponent").getString("adcode"));
		System.out.println("地址："+obj.getString("formatted_address"));
//		System.out.println(getLngAndLat("浙江省杭州市萧山区民和路"));
	}
	// 根据经纬度获取地址
	public static String getAddress(String lat, String lng){
		JSONObject obj = getCity(lat, lng).getJSONObject("result");
		return obj.getString("formatted_address");
	}
	/**
	 * 百度地图通过经纬度来获取地址,传入参数纬度lat、经度lng
	 * @param lat
	 * @param lng
	 * @return
	 */
	public static JSONObject getCity(String lat, String lng) {	
		
		Map<String, String> paramsMap = new LinkedHashMap<String, String>();
		paramsMap.put("location", lat+","+lng);
		paramsMap.put("output", "json");
		paramsMap.put("pois", "1");
		paramsMap.put("ak", AK);
		String sn = getSN(paramsMap);
		paramsMap.put("sn", sn);
		String params = toQueryString(paramsMap);
		String res = null;
		JSONObject obj = null;
		try {
			//发送请求
			res = HttpUtils.doGet(URL, null, params);
			//转换为josn字符串
			obj = JSONObject.fromObject(res);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return obj;
	}
 
	/**
	 * 百度地图通过地址来获取经纬度，传入参数address
	 * @param address
	 * @return
	 */
	public static JSONObject getLngAndLat(String address) {
		Map<String, String> paramsMap = new LinkedHashMap<String, String>();
		paramsMap.put("address", address);
		paramsMap.put("output", "json");
		paramsMap.put("ak", AK);
		String sn = getSN(paramsMap);
		paramsMap.put("sn", sn);
		String params = toQueryString(paramsMap);
		String res = null;
		JSONObject obj = null;
		try {
			//发送请求
			res = HttpUtils.doGet(URL, null, params);
			//转换为josn字符串
			obj = JSONObject.fromObject(res);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return obj;
	}
	
	//sn就是要计算的，sk不需要在url里出现，但是在计算sn的时候需要sk（假设sk=yoursk）
	private static String getSN(Map<String, String> paramsMap) {
		
		String sn = null;
		try {
			// 调用下面的toQueryString方法，对LinkedHashMap内所有value作utf8编码，拼接返回结果address=%E7%99%BE%E5%BA%A6%E5%A4%A7%E5%8E%A6&output=json&ak=yourak
			String paramsStr = toQueryString(paramsMap);
			// 对paramsStr前面拼接上/geocoder/v2/?，后面直接拼接yoursk得到/geocoder/v2/?address=%E7%99%BE%E5%BA%A6%E5%A4%A7%E5%8E%A6&output=json&ak=yourakSK
			String wholeStr = new String("/geocoder/v3/?" + paramsStr + SK);
			// 对上面wholeStr再作utf8编码
			String tempStr = URLEncoder.encode(wholeStr, "UTF-8");
			// 调用下面的MD5方法得到最后的sn签名7de5a22212ffaa9e326444c75a58f9a0
			sn = MD5(tempStr);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return sn;
	}
	
	// 对Map内所有value作utf8编码，拼接返回结果
	private static String toQueryString(Map<?, ?> data){
		StringBuffer queryString = null;
		try {
			queryString = new StringBuffer();
			for (Entry<?, ?> pair : data.entrySet()) {
				queryString.append(pair.getKey() + "=");
				queryString.append(URLEncoder.encode((String) pair.getValue(), "UTF-8") + "&");
			}
			if (queryString.length() > 0) {
				queryString.deleteCharAt(queryString.length() - 1);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return queryString.toString();
	}
	
	// 来自stackoverflow的MD5计算方法，调用了MessageDigest库函数，并把byte数组结果转换成16进制
	private static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest
							.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
					sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
									.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
}

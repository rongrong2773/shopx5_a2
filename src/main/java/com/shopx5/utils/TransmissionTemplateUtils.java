package com.shopx5.utils;

import java.util.ArrayList;
import java.util.List;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.ListMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.base.notify.Notify;
import com.gexin.rp.sdk.dto.GtReq.NotifyInfo.Type;
import com.gexin.rp.sdk.http.IGtPush;
//import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.TransmissionTemplate;
/**
 * unipush透传推送
 * @author sjx
 * 2019年7月31日 下午5:23:39
 */
public class TransmissionTemplateUtils {

	//采用"Java SDK 快速入门"， "第二步 获取访问凭证 "中获得的应用配置，用户可以自行替换
	public static final String appId = "0IBHL1I4GC84ejnywH09z7";
	public static final String appKey = "WGBBu4VTDJ98aPnzpXV2u6";
	public static final String masterSecret = "tmzERdlg4F6If9LtnnkNo5";

//	static String CID1 = "29d67f88e48f00b4ca4b85a217d77a51";
//	static String CID2 = "29d67f88e48f00b4ca4b85a217d77a51";
	//别名推送方式
 //   static String Alias1 = "";
 //   static String Alias2 = "";
	static String host = "http://sdk.open.api.igexin.com/apiex.htm";
	
	public static void main(String[] args) throws Exception {
		pushTransMsg("87128ef3795eaddce0ed7ff7c5c45d42", "hello,apwla");
	}
	
	
	@SuppressWarnings("unchecked")
	public static void pushTransMsg(String cid, String transmissionStr) throws Exception {
		System.setProperty("gexin.rp.sdk.pushlist.needDetails", "true");   // 配置返回每个用户返回用户状态，可选
		IGtPush push = new IGtPush(host, appKey, masterSecret);
		
		TransmissionTemplate template = transmissionTemplateDemo(transmissionStr);    // 通知透传模板
		
		ListMessage message = new ListMessage();
		message.setData(template);
		message.setOffline(true);    // 设置消息离线，并设置离线时间
		message.setOfflineExpireTime(24 * 1000 * 3600);    // 离线有效时间，单位为毫秒，可选
		
		@SuppressWarnings("rawtypes")
		List targets = new ArrayList();    // 配置推送目标
		Target target1 = new Target();
		target1.setAppId(appId);
		target1.setClientId(cid);
		targets.add(target1);
		
//		Target target2 = new Target();
	//	target1.setAlias(Alias1);
//		target2.setAppId(appId);
//		target2.setClientId(CID2);
	//	target2.setAlias(Alias2);
//		targets.add(target2);
		
		String taskId = push.getContentId(message);    // taskId用于在推送时去查找对应的message
		IPushResult ret = push.pushMessageToList(taskId, targets);
		
		System.out.println(ret.getResponse().toString());
	}
	
	public static TransmissionTemplate transmissionTemplateDemo(String transStr) throws Exception {
		TransmissionTemplate template = new TransmissionTemplate();
		template.setAppId(appId);
		template.setAppkey(appKey);
		template.setTransmissionType(1);    // 透传消息设置，1为强制启动应用，客户端接收到消息后就会立即启动应用；2为等待应用启动
		template.setTransmissionContent(transStr);
		
		Notify notify = new Notify();
		notify.setTitle("title");
		notify.setContent("content");
		notify.setIntent("intent:#Intent;launchFlags=0x10000000;package=com.wosiwz.xunsi;component=com.wosiwz.xunsi.MainActivity;S.title=test_title;S.content=test_content;S.payload=test;end");
		notify.setType(Type._intent);
		
//		notify.setUrl("https://dev.getui.com/");
//		notify.setType(Type._url);
		template.set3rdNotifyInfo(notify);    // 设置第三方通知
		
		// 设置定时展示时间
//		 template.setDuration("2019-07-24 17:40:00", "2019-07-24 17:41:00");
		return template;
	}
}

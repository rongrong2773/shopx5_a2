package com.shopx5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.TbDebateMapper;
import com.shopx5.pojo.TbDebateTimeLimited;
import com.shopx5.service.xunsi.DebateService;

@Service
public class DebateServiceImpl implements DebateService{

	@Autowired
	private TbDebateMapper debateMapper;
	
	@Override
	public void save(TbDebateTimeLimited entity1) {
		debateMapper.save(entity1);
	}

	@Override
	public List<TbDebateTimeLimited> findRunningDebateListById(String userId) {
		return debateMapper.findRunningDebateListById(userId);
	}
}

package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.ScAreaMapper;
import com.shopx5.pojo.ScArea;
import com.shopx5.pojo.ScAreaExample;
import com.shopx5.pojo.ScAreaExample.Criteria;
import com.shopx5.service.manager.ScAreaService;
/**
 * Area服务实现层
 * @author ShopX5 tang
 */
@Service
public class ScAreaServiceImpl implements ScAreaService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private ScAreaMapper scAreaMapper;
	/**
	 * 查询全部   带参
	 */
	public List<ScArea> findAllByParentId(Integer parentId){
		ScAreaExample example = new ScAreaExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		return scAreaMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<ScArea> findAll() {
		// 取所有
		ScAreaExample example = new ScAreaExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return scAreaMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<ScArea> findPage(int page) {
		PageBean<ScArea> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<ScArea> list = this.findAll();
		// 取查询结果
		PageInfo<ScArea> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<ScArea> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "parentId":
				param.setSort("PARENT_ID");
				break;
			case "shortName":
				param.setSort("SHORT_NAME");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<ScArea> pageBean = new EasyUIDataGridResult<ScArea>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		ScAreaExample example = new ScAreaExample();
		Criteria criteria = example.createCriteria();
	//*	criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
	//*		criteria.andNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
	//*		criteria.andCreateTimeBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<ScArea> list = scAreaMapper.selectByExample(example);
		// 取查询结果
		PageInfo<ScArea> pageInfo = new PageInfo<>(list);
		// 设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(ScArea scArea) {
		scAreaMapper.insert(scArea);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(ScArea scArea) {
		scAreaMapper.updateByPrimaryKeySelective(scArea);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public ScArea getOne(Integer id) {
		return scAreaMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Integer id) {
		scAreaMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(Integer[] ids) {
		for (Integer id : ids) {
		//	this.delete(id);
			this.deleteById(id);       	  //批量递归,删除子项
		}
	}
	
	//批量递归,删除子项
	public void deleteById(Integer id){
		//查询当前父部们下子部门列表
		List<ScArea> list = this.findAllByParentId(id);
		if (list!=null && list.size()>0) {
			for (ScArea scArea : list) {
				this.deleteById(scArea.getId());
			}
		}
		scAreaMapper.deleteByPrimaryKey(id);
	}
	
}



package com.shopx5.service.manager.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.SpRoleButtonMapper;
import com.shopx5.pojo.SpRoleButton;
import com.shopx5.pojo.SpRoleButtonExample;
import com.shopx5.pojo.SpRoleButtonExample.Criteria;
import com.shopx5.service.manager.RoleButtonService;
/**
 * RoleButton服务实现层
 * @author ShopX5 tang
 */
@Service
public class RoleButtonServiceImpl implements RoleButtonService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpRoleButtonMapper spRoleButtonMapper;
	/**
	 * 查询全部   带参
	 */
	public List<SpRoleButton> findAllByParentId(String parentId){
		SpRoleButtonExample example = new SpRoleButtonExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId);
		return spRoleButtonMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpRoleButton> findAll() {
		// 取所有
		SpRoleButtonExample example = new SpRoleButtonExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spRoleButtonMapper.selectByExample(example);
	}
	/**
	 * 增加
	 */
	@Override
	public void add(String roleId, String buttonIds) {
		String[] arr = buttonIds.split(",");
		//1 删除用户，所有角色
		this.delete(roleId);
		if (StringUtils.isBlank(buttonIds)) return;
		//2 增加用户，新的角色
		for (String buttonId : arr) {
			SpRoleButton spRoleButton = new SpRoleButton();
			spRoleButton.setRoleId(roleId);
			spRoleButton.setButtonId(buttonId);
			spRoleButtonMapper.insert(spRoleButton);
		}
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpRoleButton spRoleButton) {
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpRoleButton getOne(String id) {
		return null;
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String roleId) {
		SpRoleButtonExample example = new SpRoleButtonExample();
		Criteria criteria = example.createCriteria();
		criteria.andRoleIdEqualTo(roleId);
		spRoleButtonMapper.deleteByExample(example);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
}



package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SpRoleUserMapper;
import com.shopx5.pojo.SpRoleUser;
import com.shopx5.pojo.SpRoleUserExample;
import com.shopx5.pojo.SpRoleUserExample.Criteria;
import com.shopx5.service.manager.RoleUserService;
/**
 * RoleUser服务实现层
 * @author ShopX5 tang
 */
@Service
public class RoleUserServiceImpl implements RoleUserService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpRoleUserMapper spRoleUserMapper;
	/**
	 * 查询全部   带参
	 */
	public List<SpRoleUser> findAllByParenId(String parentId){
		SpRoleUserExample example = new SpRoleUserExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId);
		return spRoleUserMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpRoleUser> findAll() {
		// 取所有
		SpRoleUserExample example = new SpRoleUserExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spRoleUserMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SpRoleUser> findPage(int page) {
		PageBean<SpRoleUser> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SpRoleUser> list = this.findAll();
		// 取查询结果
		PageInfo<SpRoleUser> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<SpRoleUser> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		/*switch (param.getSort()) {
			case "updateTime":
				param.setSort("UPDATE_TIME");
				break;
			default:
				break;
		}*/
		EasyUIDataGridResult<SpRoleUser> pageBean = new EasyUIDataGridResult<SpRoleUser>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		SpRoleUserExample example = new SpRoleUserExample();
		Criteria criteria = example.createCriteria();
	//*	criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
	//*		criteria.andNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
	//*		criteria.andCreateTimeBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SpRoleUser> list = spRoleUserMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SpRoleUser> pageInfo = new PageInfo<>(list);
		// 设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(String userId, String roleIds) {
		String[] arr = roleIds.split(",");
		//1 删除用户，所有角色
		this.delete(userId);
		if (StringUtils.isBlank(roleIds)) return;
		//2 增加用户，新的角色
		for (String roleId : arr) {
			SpRoleUser spRoleUser = new SpRoleUser();
			spRoleUser.setUserId(userId);
			spRoleUser.setRoleId(roleId);
			spRoleUserMapper.insert(spRoleUser);
		}
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpRoleUser getOne(String id) {
		return null;
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String userId) {
		SpRoleUserExample example = new SpRoleUserExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		spRoleUserMapper.deleteByExample(example);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
}



package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.pojo.Content;
import com.shopx5.mapper.SxContentMapper;
import com.shopx5.pojo.SxContent;
import com.shopx5.pojo.SxContentCategory;
import com.shopx5.pojo.SxContentExample;
import com.shopx5.pojo.SxContentExample.Criteria;
import com.shopx5.service.manager.AdContentCategoryService;
import com.shopx5.service.manager.AdContentService;
/**
 * Content服务实现层
 * @author ShopX5 tang
 */
@Service
public class AdContentServiceImpl implements AdContentService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SxContentMapper sxContentMapper;
	@Autowired
	private AdContentCategoryService contentCategoryService;
	/**
	 * 查询全部   带参
	 */
	public List<SxContent> findAllByParenId(String parentId){
		SxContentExample example = new SxContentExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId);
		return sxContentMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SxContent> findAll() {
		// 取所有
		SxContentExample example = new SxContentExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return sxContentMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SxContent> findPage(int page) {
		PageBean<SxContent> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SxContent> list = this.findAll();
		// 取查询结果
		PageInfo<SxContent> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<Content> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "categoryId":
				param.setSort("CATEGORY_ID");
				break;
			case "subTitle":
				param.setSort("SUB_TITLE");
				break;
			case "titleDesc":
				param.setSort("TITLE_DESC");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<Content> pageBean = new EasyUIDataGridResult<Content>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		SxContentExample example = new SxContentExample();
		Criteria criteria = example.createCriteria();
	//*	criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
			criteria.andTitleLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
			criteria.andUpdatedBetween(dateFrom, dateTo);
		}
		// 点击广告类目, 展示类目内容 ***
		if (StringUtils.isNoneBlank(param.getCategoryId())) {
			criteria.andCategoryIdEqualTo(Long.valueOf(param.getCategoryId()));
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SxContent> list = sxContentMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SxContent> pageInfo = new PageInfo<>(list);
		
		//遍历数据 封装分类名称 *
		List<Content> fList = new ArrayList<>();
		for (SxContent sxContent : list) {
			Content content = new Content(sxContent);
			SxContentCategory scc = contentCategoryService.getOne(String.valueOf(sxContent.getCategoryId()));
			SxContentCategory scc2 = contentCategoryService.getOne(String.valueOf(scc.getParentId()));
			content.setCategoryName(scc2.getName()+"-"+scc.getName());
			fList.add(content);
		}
				
		// 设置数据
		pageBean.setRows(fList);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SxContent sxContent) {
		sxContentMapper.insert(sxContent);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SxContent sxContent) {
		sxContentMapper.updateByPrimaryKeySelective(sxContent);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SxContent getOne(String id) {
		return sxContentMapper.selectByPrimaryKey(Long.valueOf(id));
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id) {
		sxContentMapper.deleteByPrimaryKey(Long.valueOf(id));
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
}



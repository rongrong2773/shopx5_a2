package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SxContentCategoryMapper;
import com.shopx5.pojo.SxContentCategory;
import com.shopx5.pojo.SxContentCategoryExample;
import com.shopx5.pojo.SxContentCategoryExample.Criteria;
import com.shopx5.service.manager.AdContentCategoryService;
/**
 * ContentCategory服务实现层
 * @author ShopX5 tang
 */
@Service
public class AdContentCategoryServiceImpl implements AdContentCategoryService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SxContentCategoryMapper sxContentCategoryMapper;
	/**
	 * 查询全部   带参
	 */
	public List<SxContentCategory> findAllByParenId(Long parentId){
		SxContentCategoryExample example = new SxContentCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		return sxContentCategoryMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SxContentCategory> findAll() {
		// 取所有
		SxContentCategoryExample example = new SxContentCategoryExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return sxContentCategoryMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SxContentCategory> findPage(int page) {
		PageBean<SxContentCategory> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SxContentCategory> list = this.findAll();
		// 取查询结果
		PageInfo<SxContentCategory> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<SxContentCategory> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "parentId":
				param.setSort("PARENT_ID");
				break;
			case "sortOrder":
				param.setSort("SORT_ORDER");
				break;
			case "isParent":
				param.setSort("IS_PARENT");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<SxContentCategory> pageBean = new EasyUIDataGridResult<SxContentCategory>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		SxContentCategoryExample example = new SxContentCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andStatusEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
			criteria.andNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
			criteria.andUpdatedBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SxContentCategory> list = sxContentCategoryMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SxContentCategory> pageInfo = new PageInfo<>(list);
		// 设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SxContentCategory sxContentCategory) {
		sxContentCategoryMapper.insert(sxContentCategory);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SxContentCategory sxContentCategory) {
		sxContentCategoryMapper.updateByPrimaryKeySelective(sxContentCategory);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SxContentCategory getOne(String id) {
		return sxContentCategoryMapper.selectByPrimaryKey(Long.valueOf(id));
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id) {
		sxContentCategoryMapper.deleteByPrimaryKey(Long.valueOf(id));
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
}



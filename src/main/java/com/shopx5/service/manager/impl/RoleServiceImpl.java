package com.shopx5.service.manager.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SpRoleMapper;
import com.shopx5.mapper.result.RoleMapper;
import com.shopx5.pojo.SpRole;
import com.shopx5.pojo.SpRoleExample;
import com.shopx5.pojo.SpRoleExample.Criteria;
import com.shopx5.pojo.result.Role;
import com.shopx5.service.manager.RoleService;
import com.shopx5.utils.RightsHelper;
/**
 * Role服务实现层
 * @author ShopX5 tang
 */
@Service
public class RoleServiceImpl implements RoleService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpRoleMapper spRoleMapper;
	@Autowired
	private RoleMapper roleMapper;
	/**
	 * 查询全部   带参
	 */
	public List<SpRole> findAllByParenId(String parentId){
		SpRoleExample example = new SpRoleExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId);
		return spRoleMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpRole> findAll() {
		// 取所有
		SpRoleExample example = new SpRoleExample();
		// Criteria criteria = example.createCriteria(); SHOPX5
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spRoleMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SpRole> findPage(int page) {
		PageBean<SpRole> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SpRole> list = this.findAll();
		// 取查询结果
		PageInfo<SpRole> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<SpRole> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "updateTime":
				param.setSort("UPDATE_TIME");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<SpRole> pageBean = new EasyUIDataGridResult<SpRole>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		SpRoleExample example = new SpRoleExample();
		Criteria criteria = example.createCriteria();
	//*	criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
			criteria.andNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
			criteria.andCreateTimeBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SpRole> list = spRoleMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SpRole> pageInfo = new PageInfo<>(list);
		// 设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SpRole spRole) {
		spRoleMapper.insert(spRole);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpRole spRole) {
		spRoleMapper.updateByPrimaryKeySelective(spRole);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpRole getOne(String id) {
		return spRoleMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id) {
		spRoleMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
	//////////////////////////////
	
	//取角色/List<模块>
	@Override
	public Role findRoleAndModule(String roleId) {
		return roleMapper.findRoleAndModule(roleId);
	}
	//取角色/List<按钮>
	@Override
	public Role findRoleAndButton(String roleId) {
		return roleMapper.findRoleAndButton(roleId);
	}
	
	//添加角色  addQX权限(操作模块)
	@Override
	public void addQX(String roleId, String moduleIds, String curdQX) {
		BigInteger sum = new BigInteger("0");
		if (StringUtils.isNotBlank(moduleIds)) {
			String[] arr = moduleIds.split(",");
			sum = RightsHelper.sumRights(arr);
		}
		//更新权限值
		SpRole spRole = new SpRole();
		spRole.setRoleId(roleId);
		switch (curdQX) {
			case "addQX":
				spRole.setAddQx(sum.toString());
				break;
			case "delQX":
				spRole.setDelQx(sum.toString());
				break;
			case "editQX":
				spRole.setEditQx(sum.toString());
				break;
			case "seeQX":
				spRole.setSeeQx(sum.toString());
				break;
			default:
				break;
		}		
		spRoleMapper.updateByPrimaryKeySelective(spRole);
	}
	
}



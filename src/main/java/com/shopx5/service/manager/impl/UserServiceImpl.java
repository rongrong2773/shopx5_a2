package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EUser;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SpUserInfoMapper;
import com.shopx5.mapper.SpUserMapper;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpDept;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserExample;
import com.shopx5.pojo.SpUserExample.Criteria;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.result.User;
import com.shopx5.service.manager.DeptService;
import com.shopx5.service.manager.UserInfoService;
import com.shopx5.service.manager.UserService;
import com.shopx5.utils.Encrypt;
import com.shopx5.utils.IDUtils;
/**
 * User服务实现层
 * @author ShopX5 tang
 */
@Service
public class UserServiceImpl implements UserService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpUserMapper spUserMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private SpUserInfoMapper spUserInfoMapper;
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private DeptService deptService;
	/**
	 * 查询全部   带参
	 */
	public List<SpUser> findAllByParentId(String parentId){
		SpUserExample example = new SpUserExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId); 扩展了在一对一中执行
		return spUserMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpUser> findAll() {
		// 取所有
		SpUserExample example = new SpUserExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spUserMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SpUser> findPage(int page) {
		PageBean<SpUser> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SpUser> list = this.findAll();
		// 取查询结果
		PageInfo<SpUser> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<EUser> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "updateTime":
				param.setSort("UPDATE_TIME");
				break;
			case "userName":
				param.setSort("USER_NAME");
				break;
			case "deptId":
				param.setSort("DEPT_ID");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<EUser> pageBean = new EasyUIDataGridResult<EUser>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		SpUserExample example = new SpUserExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
			criteria.andUserNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
			criteria.andCreateTimeBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SpUser> list = spUserMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SpUser> pageInfo = new PageInfo<>(list);
		
		//遍历数据 封装所属部门
		List<EUser> fList = new ArrayList<>();
		for (SpUser spUser : list) {
			EUser eUser = new EUser(spUser);
			SpDept dept = deptService.getOne(spUser.getDeptId());
			if (dept!=null) {
				eUser.setDeptName(dept.getDeptName());
			}
			fList.add(eUser);
		}
		
		// 设置数据
	//	pageBean.setRows(list);
		pageBean.setRows(fList);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SpUser spUser) {
		spUserMapper.insert(spUser);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpUser spUser) {
		spUserMapper.updateByPrimaryKey(spUser);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpUser getOne(String id) {
		return spUserMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id) {
		spUserMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
	/////////////////
	
	//返回全部列表 一对一 用户+info
	@Override
	public List<User> findUserAndInfo(){
		return userMapper.findAllUserAndInfo();
	}
	//增加用户+详情
	@Override
	public void saveAll(SpUser spUser, SpUserInfo spUserInfo) {
		String userId = IDUtils.get32UUID();
		//补充数据
		spUser.setUserId(userId);
		spUser.setCreateTime(new Date());
		spUser.setUpdateTime(new Date());
		//默认密码123456 Encrypt.md5加密
		String md5 = Encrypt.md5("123456", spUser.getUserName());
		spUser.setPassword(md5);
		spUserMapper.insert(spUser);		
		//补充数据
		spUserInfo.setUserInfoId(userId);
		spUserInfo.setCreateTime(new Date());
		spUserInfo.setUpdateTime(new Date());
		//spUserInfoMapper.insert(spUserInfo);
		userInfoService.add(spUserInfo);
	}
	//更新用户+详情
	@Override
	public void updateAll(SpUser spUser, SpUserInfo spUserInfo) {
		spUserMapper.updateByPrimaryKeySelective(spUser);
		userInfoService.update(spUserInfo);
	}
	//删除用户+详情
	@Override
	public void deleteAll(String[] ids) {
		userInfoService.delete(ids);
		this.delete(ids);
	}
	//获取用户的List<角色>
	@Override
	public User findUserAndRole(String userId) {
		return userMapper.findUserAndRole(userId);
	}
	
}



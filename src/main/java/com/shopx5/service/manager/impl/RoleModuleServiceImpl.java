package com.shopx5.service.manager.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.SpRoleModuleMapper;
import com.shopx5.pojo.SpRoleModule;
import com.shopx5.pojo.SpRoleModuleExample;
import com.shopx5.pojo.SpRoleModuleExample.Criteria;
import com.shopx5.service.manager.RoleModuleService;
/**
 * RoleModule服务实现层
 * @author ShopX5 tang
 */
@Service
public class RoleModuleServiceImpl implements RoleModuleService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpRoleModuleMapper spRoleModuleMapper;
	/**
	 * 查询全部   带参
	 */
	public List<SpRoleModule> findAllByParenId(String parentId){
		SpRoleModuleExample example = new SpRoleModuleExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId);
		return spRoleModuleMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpRoleModule> findAll() {
		// 取所有
		SpRoleModuleExample example = new SpRoleModuleExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spRoleModuleMapper.selectByExample(example);
	}
	/**
	 * 增加
	 */
	@Override
	public void add(String roleId, String moduleIds) {
		String[] arr = moduleIds.split(",");
		//1 删除用户，所有角色
		this.delete(roleId);
		if (StringUtils.isBlank(moduleIds)) return;
		//2 增加用户，新的角色
		for (String moduleId : arr) {
			SpRoleModule spRoleModule = new SpRoleModule();
			spRoleModule.setRoleId(roleId);
			spRoleModule.setModuleId(Integer.valueOf(moduleId));
			spRoleModuleMapper.insert(spRoleModule);
		}
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpRoleModule spRoleModule) {
		
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpRoleModule getOne(String id) {
		return null;
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String roleId) {
		SpRoleModuleExample example = new SpRoleModuleExample();
		Criteria criteria = example.createCriteria();
		criteria.andRoleIdEqualTo(roleId);
		spRoleModuleMapper.deleteByExample(example);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
}



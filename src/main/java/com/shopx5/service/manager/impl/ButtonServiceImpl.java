package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SpButtonMapper;
import com.shopx5.pojo.SpButton;
import com.shopx5.pojo.SpButtonExample;
import com.shopx5.pojo.SpButtonExample.Criteria;
import com.shopx5.service.manager.ButtonService;
/**
 * Button服务实现层
 * @author ShopX5 tang
 */
@Service
public class ButtonServiceImpl implements ButtonService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpButtonMapper spButtonMapper;
	/**
	 * 查询全部   带参
	 */
	public List<SpButton> findAllByParenId(String parentId){
		SpButtonExample example = new SpButtonExample();
		Criteria criteria = example.createCriteria();
	//	criteria.andParentIdEqualTo(parentId);
		return spButtonMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpButton> findAll() {
		// 取所有
		SpButtonExample example = new SpButtonExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spButtonMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SpButton> findPage(int page) {
		PageBean<SpButton> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SpButton> list = this.findAll();
		// 取查询结果
		PageInfo<SpButton> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<SpButton> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "buttonId":
				param.setSort("BUTTON_ID");
				break;
			case "buttonName":
				param.setSort("BUTTON_NAME");
				break;
			case "qxName":
				param.setSort("QX_NAME");
				break;
			case "remark":
				param.setSort("REMARK");
				break;
			case "orderNo":
				param.setSort("ORDER_NO");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<SpButton> pageBean = new EasyUIDataGridResult<SpButton>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), param.getRows());
		// 取产品
		SpButtonExample example = new SpButtonExample();
		Criteria criteria = example.createCriteria();
	//*	criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
			criteria.andButtonNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
	//*		criteria.andCreateTimeBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SpButton> list = spButtonMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SpButton> pageInfo = new PageInfo<>(list);
		// 设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SpButton spButton) {
		spButtonMapper.insert(spButton);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpButton spButton) {
		spButtonMapper.updateByPrimaryKeySelective(spButton);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpButton getOne(String id) {
		return spButtonMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id) {
		spButtonMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
}



package com.shopx5.service.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.jedis.JedisClient;
import com.shopx5.mapper.SpModuleMapper;
import com.shopx5.pojo.SpModule;
import com.shopx5.pojo.SpModuleExample;
import com.shopx5.pojo.SpModuleExample.Criteria;
import com.shopx5.service.manager.ModuleService;
/**
 * Module服务实现层
 * @author ShopX5 tang
 */
@Service
public class ModuleServiceImpl implements ModuleService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Value("${ALL_MODULE}")
	private String ALL_MODULE;
	@Autowired
	private SpModuleMapper spModuleMapper;
	@Autowired
	private JedisClient jedisClient;
	/**
	 * 查询全部   带参
	 */
	public List<SpModule> findAllByParentId(Integer parentId){
		SpModuleExample example = new SpModuleExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		return spModuleMapper.selectByExample(example);
	}
	@Override
	public List<SpModule> findAllByParentId(Integer parentId, DataGridParam param) {
		// sql字段转换
		switch (param.getSort()) {
			case "updateTime":
				param.setSort("UPDATE_TIME");
				break;
			case "orderNo":
				param.setSort("ORDER_NO");
				break;
			default:
				break;
		}
		SpModuleExample example = new SpModuleExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		return spModuleMapper.selectByExample(example);
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<SpModule> findAll() {
		// 取所有
		SpModuleExample example = new SpModuleExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spModuleMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SpModule> findPage(int page) {
		PageBean<SpModule> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SpModule> list = this.findAll();
		// 取查询结果
		PageInfo<SpModule> pageInfo = new PageInfo<>(list);
		// 进一步处理		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 查询datagrid分页数据
	 */
	@Override
	public EasyUIDataGridResult<SpModule> getDataGridResult(DataGridParam param) throws Exception {
		// sql字段转换
		switch (param.getSort()) {
			case "updateTime":
				param.setSort("UPDATE_TIME");
				break;
			case "orderNo":
				param.setSort("ORDER_NO");
				break;
			default:
				break;
		}
		EasyUIDataGridResult<SpModule> pageBean = new EasyUIDataGridResult<SpModule>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(param.getPage(), 15/*param.getRows()*/);
		// 取产品
		SpModuleExample example = new SpModuleExample();
		Criteria criteria = example.createCriteria();
		criteria.andStateEqualTo(1);
		// 搜索:查询名称
		if (StringUtils.isNoneBlank(param.getTitle())) {
			criteria.andNameLike("%" + param.getTitle() + "%");
		}
		// 搜索:查询时间
		if (StringUtils.isNoneBlank(param.getDate_from()) && StringUtils.isNoneBlank(param.getDate_to())) {
			// 时间转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateFrom = sdf.parse(param.getDate_from());
			Date dateTo   = sdf.parse(param.getDate_to());
			criteria.andCreateTimeBetween(dateFrom, dateTo);
		}
		// 添加排序
		example.setOrderByClause(param.getSort() + " " + param.getOrder());
		List<SpModule> list = spModuleMapper.selectByExample(example);
		// 取查询结果
		PageInfo<SpModule> pageInfo = new PageInfo<>(list);
		// 设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SpModule spModule) {
		spModuleMapper.insert(spModule);
		jedisClient.del(ALL_MODULE); // 删除缓存
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpModule spModule) {
		spModuleMapper.updateByPrimaryKeySelective(spModule);
		jedisClient.del(ALL_MODULE); // 删除缓存
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpModule getOne(Integer id) {
		return spModuleMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Integer id) {
		spModuleMapper.deleteByPrimaryKey(id);
		jedisClient.del(ALL_MODULE); // 删除缓存
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(Integer[] ids) {
		for (Integer id : ids) {
			this.delete(id);
		}
	}
	
	/////////////////////////////
	
	//通过父id 获取子列表
	@Override
	public List<SpModule> findListByParentId(Integer moduleId) {
		SpModuleExample example = new SpModuleExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(moduleId);
		List<SpModule> list = spModuleMapper.selectByExample(example);
		return list;
	}
	@Override
	public List<SpModule> getByCURL(String curl) {
		SpModuleExample example = new SpModuleExample();
		Criteria criteria = example.createCriteria();
		criteria.andCurlEqualTo(curl);
		return spModuleMapper.selectByExample(example);
	}
	
}



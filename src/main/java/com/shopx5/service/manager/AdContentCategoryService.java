package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.pojo.SxContentCategory;
/**
 * ContentCategory服务层接口
 * @author ShopX5 tang
 */
public interface AdContentCategoryService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SxContentCategory> findAllByParenId(Long parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SxContentCategory> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<SxContentCategory> findPage(int page);
	/**
	 * 返回datagrid分页数据
	 * @return
	 */
	public EasyUIDataGridResult<SxContentCategory> getDataGridResult(DataGridParam param) throws Exception;
	/**
	 * 增加
	 */
	public void add(SxContentCategory sxContentCategory);
	/**
	 * 修改
	 */
	public void update(SxContentCategory sxContentCategory);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SxContentCategory getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
}

package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.pojo.ScArea;
/**
 * Area服务层接口
 * @author ShopX5 tang
 */
public interface ScAreaService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<ScArea> findAllByParentId(Integer parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<ScArea> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<ScArea> findPage(int page);
	/**
	 * 返回datagrid分页数据
	 * @return
	 */
	public EasyUIDataGridResult<ScArea> getDataGridResult(DataGridParam param) throws Exception;
	/**
	 * 增加
	 */
	public void add(ScArea scArea);
	/**
	 * 修改
	 */
	public void update(ScArea scArea);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public ScArea getOne(Integer id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Integer id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Integer[] ids);
	
}

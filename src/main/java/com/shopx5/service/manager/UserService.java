package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EUser;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.result.User;
/**
 * User服务层接口
 * @author ShopX5 tang
 */
public interface UserService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SpUser> findAllByParentId(String parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpUser> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<SpUser> findPage(int page);
	/**
	 * 返回datagrid分页数据
	 * @return
	 */
	public EasyUIDataGridResult<EUser> getDataGridResult(DataGridParam param) throws Exception;
	/**
	 * 增加
	 */
	public void add(SpUser spUser);
	/**
	 * 修改
	 */
	public void update(SpUser spUser);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpUser getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
	///////////////////////
	
	//返回全部列表 一对一 用户+info
	public List<User> findUserAndInfo();
	//增加用户+详情
	public void saveAll(SpUser spUser, SpUserInfo spUserInfo);
	//更新用户+详情
	public void updateAll(SpUser spUser, SpUserInfo spUserInfo);
	//删除用户+详情
	public void deleteAll(String [] ids);
	
	//获取用户的List<角色>
	public User findUserAndRole(String userId);
	
}

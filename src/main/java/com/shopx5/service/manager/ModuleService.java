package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.pojo.SpModule;
/**
 * Module服务层接口
 * @author ShopX5 tang
 */
public interface ModuleService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SpModule> findAllByParentId(Integer parentId);
	public List<SpModule> findAllByParentId(Integer parentId, DataGridParam param);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpModule> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<SpModule> findPage(int page);
	/**
	 * 返回datagrid分页数据
	 * @return
	 */
	public EasyUIDataGridResult<SpModule> getDataGridResult(DataGridParam param) throws Exception;
	/**
	 * 增加
	 */
	public void add(SpModule spModule);
	/**
	 * 修改
	 */
	public void update(SpModule spModule);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpModule getOne(Integer id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Integer id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Integer[] ids);
	
	///////////////////////
	
	//通过父id 获取子列表
	public List<SpModule> findListByParentId(Integer moduleId);
	//根据curl获取实体
	public List<SpModule> getByCURL(String curl);
}

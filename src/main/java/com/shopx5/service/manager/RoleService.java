package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.pojo.SpRole;
import com.shopx5.pojo.result.Role;
/**
 * Role服务层接口
 * @author ShopX5 tang
 */
public interface RoleService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SpRole> findAllByParenId(String parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpRole> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<SpRole> findPage(int page);
	/**
	 * 返回datagrid分页数据
	 * @return
	 */
	public EasyUIDataGridResult<SpRole> getDataGridResult(DataGridParam param) throws Exception;
	/**
	 * 增加
	 */
	public void add(SpRole spRole);
	/**
	 * 修改
	 */
	public void update(SpRole spRole);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpRole getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
	/////////////
	
	//取角色/List<模块>
	public Role findRoleAndModule(String roleId);
	//取角色/List<按钮>
	public Role findRoleAndButton(String roleId);
	//添加角色  addQX权限(操作模块)
	public void addQX(String roleId, String moduleIds, String curdQX);

}

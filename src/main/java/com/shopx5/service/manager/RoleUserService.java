package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.pojo.SpRoleUser;
/**
 * RoleUser服务层接口
 * @author ShopX5 tang
 */
public interface RoleUserService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SpRoleUser> findAllByParenId(String parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpRoleUser> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<SpRoleUser> findPage(int page);
	/**
	 * 返回datagrid分页数据
	 * @return
	 */
	public EasyUIDataGridResult<SpRoleUser> getDataGridResult(DataGridParam param) throws Exception;
	/**
	 * 增加
	 */
	public void add(String userId, String roleIds);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpRoleUser getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
}

package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.pojo.SpRoleModule;
/**
 * RoleModule服务层接口
 * @author ShopX5 tang
 */
public interface RoleModuleService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SpRoleModule> findAllByParenId(String parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpRoleModule> findAll();
	/**
	 * 增加
	 */
	public void add(String roleId, String moduleIds);
	/**
	 * 修改
	 */
	public void update(SpRoleModule spRoleModule);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpRoleModule getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
}

package com.shopx5.service.manager;

import java.util.List;

import com.shopx5.pojo.SpRoleButton;
/**
 * RoleButton服务层接口
 * @author ShopX5 tang
 */
public interface RoleButtonService {
	/**
	 * 返回全部列表
	 * @param parentId
	 * @return
	 */
	public List<SpRoleButton> findAllByParentId(String parentId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpRoleButton> findAll();
	/**
	 * 增加
	 */
	public void add(String roleId, String buttonIds);
	/**
	 * 修改
	 */
	public void update(SpRoleButton spRoleButton);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpRoleButton getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
}

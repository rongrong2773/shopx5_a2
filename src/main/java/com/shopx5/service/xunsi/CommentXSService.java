package com.shopx5.service.xunsi;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbCommentXS;
import com.shopx5.pojo.TbQuestionXS;
import com.shopx5.pojo.TbSpeechXS;

public interface CommentXSService {

	void save(TbCommentXS tbComment);

	void delById(String aimInfoId);

	PageResult findPage(String aimInfoId, Integer pageNum, Integer pageSize);

	PageResult findFstCommentPage(String aimInfoId, String type, Integer pageNum, Integer pageSize);

	PageResult findSndCommentPage(String aimInfoId,String type, Integer pageNum, Integer pageSize);

	void saveSpeechEntity(TbSpeechXS entity);

	PageResult findSpeechPage(String thesisId, String orderType, String searchType, Integer pageNum, Integer pageSize);
	
	void saveQuesEntity(TbQuestionXS entity);

	PageResult findQuestionPage(String speechId, Integer pageNum, Integer pageSize);
	
	List<String> findUserListInSpeechByThesisId(String thesisId);    // 查找某个论题下的用户id列表

}

package com.shopx5.service.xunsi;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbPicVideoChar;
import com.shopx5.pojo.TbQuestionXS;
import com.shopx5.pojo.TbSpeechXS;
import com.shopx5.pojo.TbThesis;

public interface ThesisService {

	// 保存论题(论道)信息
	void save(TbThesis tbThesis);
	
	// 添加新思路的关联信息
	void addTwvConn(String primaryId, String connTwvId);

	PageResult findThesisByPage(String type, Integer page, Integer rows);

	void delThesisById(String thesisId, String usrId);
	// 个人中心: 我的发布 - 论题(论道)
	PageResult findThesisByIdByPage(String usrId, String type, Integer page, Integer rows);

	// 查找论题(论道)和新思路论题
	List<TbThesis> findAllTypeThesisById(String usrId);

	// 查询论题(论道)的数量
	Integer findThesisCountById(String userId);
	
	// 获取从某条寻思发起的议题分页列表
	PageResult findConnTwvThesisPage(Integer pageNum, Integer pageSize, String aimInfoId, String type);
	
	// 根据主键id查询实体
	TbThesis findEntityById(String aimInfoId);

	void updateHeatCount(Integer heatCount, String theisId);

	// 根据主键查询发言实体
	TbSpeechXS findSpeechEntity(String aimInfoId);

	void updateHeatCountSpeech(Integer heatCount, String aimInfoId);

	TbQuestionXS findQuestionEntity(String questionId);

	void updateQuestionEntityAgree(TbQuestionXS tbQuestionXS);

	void updateQuestionEntityDisagree(TbQuestionXS tbQuestionXS);
	
}

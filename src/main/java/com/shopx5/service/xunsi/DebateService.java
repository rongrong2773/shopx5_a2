package com.shopx5.service.xunsi;

import java.util.List;

import com.shopx5.pojo.TbDebateTimeLimited;

public interface DebateService {

	void save(TbDebateTimeLimited entity1);

	List<TbDebateTimeLimited> findRunningDebateListById(String userId);

}

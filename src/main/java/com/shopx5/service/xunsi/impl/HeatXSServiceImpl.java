package com.shopx5.service.xunsi.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.TbHeatXSMapper;
import com.shopx5.pojo.TbHeatXS;
import com.shopx5.service.xunsi.HeatXSService;
/**
 * 寻思、原创文章、论题 点赞
 * @author sjx
 * 2019年8月16日 下午5:31:30
 */
@Service
public class HeatXSServiceImpl implements HeatXSService{

	@Autowired
	private TbHeatXSMapper heatXSMapper;
	
	@Override
	public void save(TbHeatXS entity) {
		heatXSMapper.save(entity);
	}

	@Override
	public TbHeatXS findEntity(String userId, String aimInfoId, String type) {
		return heatXSMapper.findEntity(userId,aimInfoId,type);
	}

	@Override
	public void cancel(String userId, String aimInfoId, String type) {
		heatXSMapper.cancel(userId,aimInfoId,type);
	}

}

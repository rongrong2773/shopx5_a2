package com.shopx5.service.xunsi.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbQuesXSMapper;
import com.shopx5.mapper.TbSpeechXSMapper;
import com.shopx5.mapper.TbThesisMapper;
import com.shopx5.pojo.TbQuestionXS;
import com.shopx5.pojo.TbSpeechXS;
import com.shopx5.pojo.TbThesis;
import com.shopx5.pojo.TbThesisExample;
import com.shopx5.pojo.TbThesisExample.Criteria;
import com.shopx5.service.xunsi.ThesisService;
/**
 * @author sjx
 * 2019年7月1日 上午10:37:46
 */
@Service
public class ThesisServiceImpl implements ThesisService {

	@Autowired
	private TbThesisMapper thesisMapper;
	
	@Autowired
	private TbSpeechXSMapper speechXSMapper;
	
	@Autowired
	private TbQuesXSMapper tbQuesXSMapper;
	
	@Override
	public void save(TbThesis tbThesis) {
		thesisMapper.save(tbThesis);
	}

	@Override
	public void addTwvConn(String primaryId, String connTwvId) {
		thesisMapper.saveTwvConn(primaryId, connTwvId);
	}

	@Override
	public PageResult findThesisByPage(String type, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		TbThesisExample example = new TbThesisExample();
		if ("byHeat".equals(type)) {
			example.setOrderByClause("heat_count desc");
		}else if ("byTime".equals(type)) {
			example.setOrderByClause("create_time desc");
		}
		
//		Criteria criteria = example.createCriteria();
		Page<TbThesis> page = (Page<TbThesis>)thesisMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public void delThesisById(String thesisId, String usrId) {
		thesisMapper.delThesisById(thesisId, usrId);
	}

	@Override
	public PageResult findThesisByIdByPage(String usrId, String type, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		TbThesisExample example = new TbThesisExample();
		if ("byHeat".equals(type)) {
			example.setOrderByClause("heat_count desc");
		}else if ("byTime".equals(type)) {
			example.setOrderByClause("create_time desc");
		}
		Criteria criteria = example.createCriteria();
		criteria.andUsrIdEqualTo(Long.parseLong(usrId));
		Page<TbThesis> page = (Page<TbThesis>)thesisMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public List<TbThesis> findAllTypeThesisById(String usrId) {
		return thesisMapper.findAllTypeThesisById(usrId);
	}

	@Override
	public Integer findThesisCountById(String userId) {
		return thesisMapper.findThesisCountById(userId);
	}

	// // 获取从某条寻思发起的议题分页列表
	@Override
	public PageResult findConnTwvThesisPage(Integer pageNum, Integer pageSize, String aimInfoId, String type) {
		PageHelper.startPage(pageNum,pageSize);
		TbThesisExample example = new TbThesisExample();
		if ("byHeat".equals(type)) {
			example.setOrderByClause("heat_count desc");
		}else if ("byTime".equals(type)) {
			example.setOrderByClause("create_time desc");
		}
		Criteria criteria = example.createCriteria();
		criteria.andisConnTwvEqualTo("yes");
		criteria.andConnTwvIdEqualTo(aimInfoId);
		Page<TbThesis> page = (Page<TbThesis>)thesisMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	// 根据主键id查询实体
	@Override
	public TbThesis findEntityById(String aimInfoId) {
		return thesisMapper.findEntityById(aimInfoId);
	}

	@Override
	public void updateHeatCount(Integer heatCount,String thesisId) {
		thesisMapper.updateHeatCount(heatCount,thesisId);
	}

	// 根据主键查询发言实体
	@Override
	public TbSpeechXS findSpeechEntity(String aimInfoId) {
		return speechXSMapper.findSpeechEntity(aimInfoId);
	}

	@Override
	public void updateHeatCountSpeech(Integer heatCount, String aimInfoId) {
		speechXSMapper.updateHeatCountSpeech(heatCount, aimInfoId);
	}

	@Override
	public TbQuestionXS findQuestionEntity(String questionId) {
		return tbQuesXSMapper.findQuestionEntity(questionId);
	}

	@Override
	public void updateQuestionEntityAgree(TbQuestionXS tbQuestionXS) {
		tbQuesXSMapper.updateQuestionEntityAgree(tbQuestionXS);
	}

	@Override
	public void updateQuestionEntityDisagree(TbQuestionXS tbQuestionXS) {
		tbQuesXSMapper.updateQuestionEntityDisagree(tbQuestionXS);
	}
}

package com.shopx5.service.xunsi.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbArticleMapper;
import com.shopx5.pojo.TbArticle;
import com.shopx5.pojo.TbArticleExample;
import com.shopx5.pojo.TbArticleExample.Criteria;
import com.shopx5.service.xunsi.ArticleService;
/**
 * 原创文章: 学富 - 推荐
 * @author sjx
 * 2019年7月18日 上午11:13:04
 */
@Service
public class ArticleServiceImpl implements ArticleService{

	@Autowired
	private TbArticleMapper tbArticleMapper;
	
	@Override
	public void add(TbArticle bean) {
		tbArticleMapper.add(bean);
	}

	@Override
	public TbArticle findOne(String id) {
		return tbArticleMapper.findOne(id);
	}
	
	@Override
	public PageResult findPage(Long usrId, Integer pageNum, Integer pageSize){
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		TbArticleExample example = new TbArticleExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(usrId);
		// PageHelper第二种方式
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	@Override
	public void update(TbArticle bean){
		tbArticleMapper.update(bean);
	}
}

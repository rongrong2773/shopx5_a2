package com.shopx5.service.xunsi.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.AppInfoMapper;
import com.shopx5.pojo.TbAppInfo;
import com.shopx5.service.xunsi.AppInfoService;
/**
 * 
 * @author sjx
 * 2019年7月2日 上午9:09:02
 */
@Service
public class AppInfoServiceImpl implements AppInfoService{

	@Autowired
	private AppInfoMapper appInfoMapper;
	

	@Override
	public List<TbAppInfo> findVers() {
		return appInfoMapper.findVers();
	}


	@Override
	public void save(TbAppInfo tbAppInfo) {
		appInfoMapper.save(tbAppInfo);
	}

}

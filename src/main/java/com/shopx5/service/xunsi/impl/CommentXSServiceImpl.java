package com.shopx5.service.xunsi.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbCommentXSMapper;
import com.shopx5.mapper.TbQuesXSMapper;
import com.shopx5.mapper.TbSpeechXSMapper;
import com.shopx5.pojo.TbCommentXS;
import com.shopx5.pojo.TbCommentXSExample;
import com.shopx5.pojo.TbCommentXSExample.Criteria;
import com.shopx5.pojo.TbQuestionXS;
import com.shopx5.pojo.TbQuestionXSExample;
import com.shopx5.pojo.TbSpeechXS;
import com.shopx5.pojo.TbSpeechXSExample;
import com.shopx5.service.xunsi.CommentXSService;
/**
 * 
 * @author sjx
 * 2019年7月4日 下午3:24:54
 */
@Service
public class CommentXSServiceImpl implements CommentXSService{

	@Autowired
	private TbCommentXSMapper tbCommentXSMapper;
	
	@Autowired
	private TbSpeechXSMapper tbSpeechXSMapper;
	
	@Autowired
	private TbQuesXSMapper tbQuesXSMapper;
	
	@Override
	public void save(TbCommentXS tbComment) {
		tbCommentXSMapper.save(tbComment);
	}

	@Override
	public void delById(String aimInfoId) {
		tbCommentXSMapper.delById(aimInfoId);
	}

	@Override
	public PageResult findPage(String aimInfoId, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbCommentXSExample example = new TbCommentXSExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		criteria.andAimInfoIdEqualTo(aimInfoId);
		// PageHelper第二种方式
		Page<TbCommentXS> page = (Page<TbCommentXS>) tbCommentXSMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	// 获取一级评论分页信息
	@Override
	public PageResult findFstCommentPage(String aimInfoId,String type,  Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbCommentXSExample example = new TbCommentXSExample();
		if ("byTime".equals(type)) {
			example.setOrderByClause("create_time desc");
		}else if ("byHeat".equals(type)) {
			example.setOrderByClause("heat_count desc");
		}
		Criteria criteria = example.createCriteria();
		// 设置条件:
		criteria.andAimInfoIdEqualTo(aimInfoId);
		criteria.andCommentIdIsNotNull();
		Page<TbCommentXS> page = (Page<TbCommentXS>) tbCommentXSMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	// 获取二级评论分页信息
	@Override
	public PageResult findSndCommentPage(String aimInfoId, String type, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbCommentXSExample example = new TbCommentXSExample();
		if ("byTime".equals(type)) {
			example.setOrderByClause("create_time desc");
		}else if ("byHeat".equals(type)) {
			example.setOrderByClause("heat_count desc");
		}
		Criteria criteria = example.createCriteria();
		// 设置条件:
		criteria.andAimInfoIdEqualTo(aimInfoId);
		criteria.andCommentIdIsNull();
		Page<TbCommentXS> page = (Page<TbCommentXS>) tbCommentXSMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public void saveSpeechEntity(TbSpeechXS entity) {
		tbSpeechXSMapper.saveSpeechEntity(entity);
	}

	@Override
	public PageResult findSpeechPage(String thesisId, String orderType, String searchType, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		TbSpeechXSExample example = new TbSpeechXSExample();
		if ("byHeat".equals(orderType)) {
			example.setOrderByClause("heat_count desc");
		}else if ("byTime".equals(orderType)) {
			example.setOrderByClause("create_time desc");
		}

		com.shopx5.pojo.TbSpeechXSExample.Criteria criteria = example.createCriteria();
		criteria.andThesisIdEqualTo(thesisId);
		if ("mixType".equals(searchType)) {
		}else if ("videoType".equals(searchType)) {
			criteria.andVideoUrlIsNull();
		}
		Page<TbSpeechXS> page = (Page<TbSpeechXS>) tbSpeechXSMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public void saveQuesEntity(TbQuestionXS entity) {
		tbQuesXSMapper.saveQuesEntity(entity);
	}

	@Override
	public PageResult findQuestionPage(String speechId, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		TbQuestionXSExample example = new TbQuestionXSExample();
		example.setOrderByClause("create_time desc");
		Page<TbQuestionXS> page = (Page<TbQuestionXS>) tbQuesXSMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public List<String> findUserListInSpeechByThesisId(String thesisId) {
		return tbSpeechXSMapper.findUserListInSpeechByThesisId(thesisId);
	}

}

package com.shopx5.service.xunsi.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbMobilePartUserMapper;
import com.shopx5.mapper.TbMobileUserMapper;
import com.shopx5.mapper.TbMobileUserMsgMapper;
import com.shopx5.mapper.TbUserConnectedMapper;
import com.shopx5.pojo.TbMobilePartUser;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbMobileUserConnect;
import com.shopx5.pojo.TbMobileUserExample;
import com.shopx5.pojo.TbMobileUserExample.Criteria;
import com.shopx5.pojo.TbMobileUserMsg;
import com.shopx5.pojo.TbMobileUserMsgExample;
import com.shopx5.pojo.TbMobileUserRemark;
import com.shopx5.pojo.TbPushXSMsg;
import com.shopx5.pojo.TbUserConnected;
import com.shopx5.service.xunsi.MobileUserService;

@Service
public class MobileUserServiceImpl implements MobileUserService{

	@Autowired
	private TbMobileUserMapper mUsrMapper;
	
	@Autowired
	private TbMobilePartUserMapper mPartUsrMapper;
	
	@Autowired
	private TbUserConnectedMapper usrConnectMapper;
	
	@Autowired
	private TbMobileUserMsgMapper tbMobileUserMsgMapper;
	
	// 查询分页，带条件
	@Override
	public PageResult findPageUsrAllInfo(Long ownerId, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbMobileUser> page = mUsrMapper.selectByPrimaryKey(ownerId);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public PageResult findPageUsrPartInfo(Long ownerId, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbMobilePartUser> page = mUsrMapper.selectByPrimaryKeyPart(ownerId);
		return new PageResult(page.getTotal(), page.getResult());
	}

	// 查出所有认证用户
	@Override
	public List<TbMobileUser> findaAuthUsrsList() {
		return mUsrMapper.findAuthUsrsList();
	}

	@Override
	public String findTokenById(Long usrId) {
		return mUsrMapper.findTokenById(usrId);
	}

	@Override
	public List<String> findTokenList() {
		return mUsrMapper.findTokenList();
	}

	@Override
	public void addConnWith(Long usrId, Long aimUsrId) {
		mUsrMapper.addConnWith(usrId,aimUsrId);
	}

	@Override
	public Long findCascadedIdByCascadingId(Long usrId) {
		return mUsrMapper.findCascadedIdByCascadingId(usrId);
	}

	@Override
	public void cancelConnWith(Long usrId, Long aimUsrId) {
		mUsrMapper.cancelConnWith(usrId, aimUsrId);
	}

	@Override
	public PageResult findMyConcernPageById(List<String> usrIdList,Long usrId, Integer pageNum, Integer pageSize) {
		
		PageHelper.startPage(pageNum, pageSize);
		TbMobileUserExample example = new TbMobileUserExample();
		Criteria criteria = example.createCriteria();
		criteria.andStrIdIn(usrIdList);
		Page<TbMobileUserConnect> page =  mUsrMapper.selectByExample(example);
		
		List<TbMobileUserConnect> tmpList = new ArrayList<TbMobileUserConnect>();
		for(TbMobileUserConnect user : page.getResult()){
			String remark = mUsrMapper.findRemarkById(usrId, user.getId().toString());
			user.setRemark(remark);
			tmpList.add(user);
		}
		return new PageResult(tmpList.size(), tmpList);
//		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public List<String> findMyConcernListById(String usrId) {
		return mUsrMapper.findMyConcernListById(usrId);
	}

	// 获取关注我的人/我的粉丝list
	@Override
	public List<String> findMyFansIdsById(String usrId) {
		return mUsrMapper.findMyFansIdsById(usrId);
	}

	@Override
	public PageResult findPage(Long usrId, Integer pageNum, Integer pageSize) {
		List<Long> list = new ArrayList<Long>();
		list = usrConnectMapper.findConnectList(usrId);
		if (list.size() == 0) {    // 新注册用户，则直接返回空
			return new PageResult(0, new ArrayList<Long>());
		}
		PageHelper.startPage(pageNum, pageSize);
		TbMobileUserExample example = new TbMobileUserExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdIn(list);
		Page<TbMobilePartUser> page = mPartUsrMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public PageResult findPage2(Long usrId, Integer pageNum, Integer pageSize) {
		List<Long> list = new ArrayList<Long>();
		list = usrConnectMapper.findConnectList2(usrId);    // 此处usrId为cascadedId
		if (list.size() == 0) {    // 新注册用户，则直接返回空
			return new PageResult(0, new ArrayList<Long>());
		}
		PageHelper.startPage(pageNum, pageSize);
		TbMobileUserExample example = new TbMobileUserExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdIn(list);
		Page<TbMobilePartUser> page = mPartUsrMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	// // 查询"个人中心 - 我的消息  - 系统消息"
	@Override
	public PageResult findMsgByPage(Integer pageNum, Integer pageSize,String userId) {
		PageHelper.startPage(pageNum, pageSize);
		TbMobileUserMsgExample example = new TbMobileUserMsgExample();
		TbMobileUserMsgExample.Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(Long.parseLong(userId));
		example.setOrderByClause("create_time desc");
		Page<TbMobileUserMsg> page = tbMobileUserMsgMapper.selectMsgByExample(example);
		return new PageResult(page.getTotal(),page.getResult());
	}
	
	@Override
	public HashSet<String> findMyConcernHashSetById(String usrId) {
		return mUsrMapper.findMyConcernSetById(usrId);
	}

	@Override
	public HashSet<String> findMyFansHashSetById(String usrId) {
		return mUsrMapper.findMyFansIdsHashSetById(usrId);
	}

	@Override
	public TbMobileUser findEntityById(String usrId) {
		return mUsrMapper.findEntityById(Long.parseLong(usrId));
	}
	
	@Override
	public TbMobileUser findEntityByIdToken(Long usrId, String token) {
		return mUsrMapper.findEntityByIdToken(usrId, token);
	}
	
	@Override
	public void update(TbMobileUser bean) {
		mUsrMapper.update(bean);
	}
	
	// 查找所有认证用户
	@Override
	public HashSet<String> findAuthedUsrHashSet() {
		return mUsrMapper.findAuthedUsrHashSet();
	}

	@Override
	public String findClientIdByPrimaryId(Long aimUsrId) {
		return mUsrMapper.findClientIdByPrimaryId(aimUsrId);
	}
	
	// 查询关注表中两用户的关联关系: usrId 是否关注了 aimUsrId
	@Override
	public TbUserConnected findEntityByUsrAimUsrId(Long usrId, Long aimUsrId) {
		return mUsrMapper.findEntityByUsrAimUsrId(usrId, aimUsrId);
	}

	@Override
	public String findNickNameByPrimaryId(Long usrId) {
		return mUsrMapper.findNickNameByPrimaryId(usrId);
	}

	// 返回个人中心信息
	@Override
	public TbMobileUser findEntity(String userId, String token, String clientId) {
		return mUsrMapper.findEntity(userId, token, clientId);
	}
	// 返回“个人中心 - 我的消息” 数量
	@Override
	public Integer findMsgCountById(String userId) {
		return mUsrMapper.findMsgCountById(userId);
	}

	@Override
	public List<TbMobileUser> findSimiliarUserList() {
		return mUsrMapper.findSimiliarUserList();
	}

	@Override
	public void updateRemark(String userId, String aimUserId, String aimUserRemark) {
		mUsrMapper.updateRemark(userId,aimUserId,aimUserRemark);
	}

	@Override
	public void shieldUser(Long userId, String aimUserId) {
		mUsrMapper.shieldUser(userId,aimUserId);
	}

	public String findShieldUserById(String userId) {
		return mUsrMapper.findShieldUserById(userId);
	}

	@Override
	public void savePushMsgEntity(TbPushXSMsg bean) {
		mUsrMapper.savePushMsgEntity(bean);
	}
}

package com.shopx5.service.xunsi.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbArticleMapper;
import com.shopx5.mapper.TbMobileUserMapper;
import com.shopx5.mapper.TbPicVideoCharMapper;
import com.shopx5.mapper.TbPicVideoCharReadedInfoMapper;
import com.shopx5.mapper.TbPushXSMsgMapper;
import com.shopx5.pojo.TbArticle;
import com.shopx5.pojo.TbArticleExample;
import com.shopx5.pojo.TbCollectXS;
import com.shopx5.pojo.TbPicVideoChar;
import com.shopx5.pojo.TbPicVideoCharConnected;
import com.shopx5.pojo.TbPushXSMsg;
import com.shopx5.pojo.TbPushXSMsgExample;
import com.shopx5.pojo.TbPicCharVideoExample;
import com.shopx5.pojo.TbPicCharVideoExample.Criteria;
import com.shopx5.service.xunsi.PicCharVideoArticleService;

@Service
public class PicCharVideoServiceImpl implements PicCharVideoArticleService{
	
	@Autowired
	private TbPicVideoCharMapper tbPicVideoCharMapper;

	@Autowired
	private TbMobileUserMapper tbMobileUsrMapper;
	
	@Autowired
	private TbArticleMapper tbArticleMapper;
	
	@Autowired
	private TbPicVideoCharReadedInfoMapper tbPicVideoCharReadedInfoMapper;
	
	@Autowired
	private TbPushXSMsgMapper tbPushXSMsgMapper;
	
	@Override
	public void save(TbPicVideoChar tbPicVideoChar) {
		tbPicVideoCharMapper.save(tbPicVideoChar);
	}

	@Override
	public void saveArticle(TbArticle tbArticle) {
		tbPicVideoCharMapper.saveArticle(tbArticle);
	}

	// 查询图文/视频的分页信息 - 按时间递减排序
	@Override
	public PageResult findPageByTimeDesc(String usrId, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbPicCharVideoExample example = new TbPicCharVideoExample();
		example.setOrderByClause("create_time desc");
//		Criteria criteria = example.createCriteria();
//		// 设置条件:
//		criteria.andMusicTypeIsNotNull();
		
		List<TbPicVideoChar> list = new ArrayList<TbPicVideoChar>();
		List<String> list2 = new ArrayList<String>();
		String shieldStr = tbMobileUsrMapper.findShieldUserById(usrId);
		String[] shieldArr = shieldStr.split(",");
		for(int i=0;i < shieldArr.length;i++){
			list2.add(shieldArr[i].trim());
		}
		// PageHelper第二种方式
		Page<TbPicVideoChar> page = (Page<TbPicVideoChar>) tbPicVideoCharMapper.selectByExample(usrId, example);

		for(TbPicVideoChar bean:page.getResult()){
			boolean flag = true;
			for(String tmpStr:list2){
				if (tmpStr.trim().equals(bean.getUserId())) {
					flag=false;
				}
			}
			if (flag == true) {    // 触发变更
				list.add(bean);
			}
		}
//		return new PageResult(page.getTotal(), page.getResult());
		return new PageResult(list.size(), list);
	}

	@Override
	public PageResult findPageByHeatDesc(String userId, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbPicCharVideoExample example = new TbPicCharVideoExample();
		example.setOrderByClause("heat desc");
		Criteria criteria = example.createCriteria();
		
		List<TbPicVideoChar> list = new ArrayList<TbPicVideoChar>();
		List<String> list2 = new ArrayList<String>();
		String shieldStr = tbMobileUsrMapper.findShieldUserById(userId);
		String[] shieldArr = shieldStr.split(",");
		for(int i=0;i < shieldArr.length;i++){
			list2.add(shieldArr[i].trim());
		}
		
		// PageHelper第二种方式
		Page<TbPicVideoChar> page = (Page<TbPicVideoChar>) tbPicVideoCharMapper.selectByExample(userId,example);
		
		for(TbPicVideoChar bean:page.getResult()){
			boolean flag = true;
			for(String tmpStr:list2){
				if (tmpStr.trim().equals(bean.getUserId())) {
					flag=false;
				}
			}
			if (flag == true) {    // 触发变更
				list.add(bean);
			}
		}
//		return new PageResult(page.getTotal(), page.getResult());
		return new PageResult(list.size(), list);
	}

	@Override
	public TbPicVideoChar findEntityByPrimaryId(String id) {
		return tbPicVideoCharMapper.findEntityByPrimaryId(id);
	}

	@Override
	public void updateHeatCount(Integer heatCount, String pvcId) {
		tbPicVideoCharMapper.updateHeatCount(heatCount, pvcId);
	}

	@Override
	public TbArticle findArtiEntityByPrimaryId(String id) {
		return tbPicVideoCharMapper.findArtiEntityByPrimaryId(id);
	}


	@Override
	public PageResult findPageByConcern(Long usrId, Integer pageNum, Integer pageSize) {
		
		List<Long> list = tbMobileUsrMapper.findCasacdedIdsById(usrId);
		
		PageHelper.startPage(pageNum, pageSize);
		
		// 进行条件查询
		TbPicCharVideoExample example = new TbPicCharVideoExample();
		example.setOrderByClause("create_time desc");
		Criteria criteria = example.createCriteria();
		
		// 设置条件:
		criteria.andCustomeSql(list);
		// PageHelper第二种方式
		Page<TbPicVideoChar> page = (Page<TbPicVideoChar>) tbPicVideoCharMapper.selectByExample(usrId.toString(),example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public PageResult findArtiPageById(Long usrId, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andUsrIdEqualTo(usrId.toString());
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	@Override
	public PageResult findArtiPage(Long usrId, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	// 增加图文/视频
	@Override
	public void addReadTimeTwv(Long usrId, String infoId) {
		TbPicVideoChar bean = tbPicVideoCharMapper.findEntityByPrimaryId(infoId);
		if (null == bean.getReadTimes()) {
			bean.setReadTimes(1);
		}else{
			bean.setReadTimes(bean.getReadTimes()+1);    // 浏览数+1 TbPicVideoCharReadedInfoMapper
		}
		tbPicVideoCharMapper.updateEntity(bean);
		
		tbPicVideoCharReadedInfoMapper.addConnInfo(usrId, infoId);    // 增加用户浏览图文/视频的记录
	}

	@Override
	public void addReadTimeArti(Long usrId, String infoId) {
		TbArticle bean = tbArticleMapper.findEntityByPrimaryId(infoId);
		if(null == bean.getReadCount()){
			bean.setReadCount(1);
		}else{
			bean.setReadCount(bean.getReadCount()+1);
		}
		tbArticleMapper.updateEntity(bean);
		tbArticleMapper.addConnInfo(usrId, infoId);
	}

	@Override
	public Integer getReadTimeTwv(String infoId) {
		return tbPicVideoCharMapper.findTwvReadtimes(infoId);
	}

	@Override
	public Integer getReadTimeArti(String infoId) {
		return tbArticleMapper.findArtiReadtimes(infoId);
	}

	@Override
	public void addCollect(String usrId, String aimId, String aimType) {
		tbPicVideoCharMapper.addCollect(usrId, aimId, aimType);
	}

	@Override
	public TbCollectXS findIsCollected(String usrId, String aimId) {
		return tbPicVideoCharMapper.findIsCollected(usrId, aimId);
	}

	@Override
	public void cancelCollect(String usrId, String aimId) {
		tbPicVideoCharMapper.cancelCollect(usrId, aimId);
	}

	@Override
	public PageResult findPageByIdByTimeDesc(String usrId, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbPicCharVideoExample example = new TbPicCharVideoExample();
//		example.setOrderByClause("create_time desc");
		Criteria criteria = example.createCriteria();
//		// 设置条件:
		criteria.andUserIdEqualTo(usrId);
		// PageHelper第二种方式
		Page<TbPicVideoChar> page = (Page<TbPicVideoChar>) tbPicVideoCharMapper.selectByExample(usrId,example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public void delTwvById(String usrId, String twvId) {
		tbPicVideoCharMapper.delTwvById(usrId, twvId);
	}

	@Override
	public void save2(TbPicVideoCharConnected bean2) {
		tbPicVideoCharMapper.save2(bean2);
	}

	@Override
	public TbPicVideoCharConnected findEntityByUsrIdPcvId(String usrId,String pcvId) {
		return tbPicVideoCharMapper.findEntityByUsrIdPcvId(usrId,pcvId);
	}

	@Override
	public void delConnEntity(String usrId, String pcvId) {
		tbPicVideoCharMapper.delConnEntity(usrId, pcvId);
	}

	@Override
	public Integer findPicCharCountById(String userId) {
		return tbPicVideoCharMapper.findPicCharCountById(userId);
	}

	@Override
	public Integer findArtiCountById(String userId) {
		return tbPicVideoCharMapper.findArtiCountById(userId);
	}

	@Override
	public List<TbPicVideoChar> findListByHeat(String usrId) {
		return tbPicVideoCharMapper.findListByHeatById(usrId);
	}

	@Override
	public void updateAricleHeat(Integer heatCount, String articleId) {
		tbArticleMapper.updateAricleHeat(heatCount,articleId);
	}

	@Override
	public void updateCollectHeat(Integer collectCount, String pvcId) {
		tbPicVideoCharMapper.updateCollectHeat(collectCount,pvcId);
	}
	// 科技
	@Override
	public PageResult findTechTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andTechTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 名人
	@Override
	public PageResult findCelebrityTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andCelebrityTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 生活
	@Override
	public PageResult findLifeTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andLifeTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 影视
	@Override
	public PageResult findFilmTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andFilmTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 哲学
	@Override
	public PageResult findPhilosophyTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andPhilosophyTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 工业
	@Override
	public PageResult findIndustryTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andIndustryTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 文学
	@Override
	public PageResult findLiteratureTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andLiteratureTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 互联网
	@Override
	public PageResult findInternetTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andInternetTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 体育
	@Override
	public PageResult findSportsTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andSportsTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	// 健康
	@Override
	public PageResult findHealthTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andHealthTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public PageResult findEconomicsTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andEconomicsTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public PageResult findOtherTypeArtiPage(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum,pageSize);
		TbArticleExample example = new TbArticleExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbArticleExample.Criteria criteria = example.createCriteria();
		criteria.andOtherTypeEqualTo("yes");
		Page<TbArticle> page = (Page<TbArticle>) tbArticleMapper.findTechTypePage(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	// 获取用户推送消息分页列表
	@Override
	public PageResult findPushMsgPage(String userId, String msgType, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbPushXSMsgExample example = new TbPushXSMsgExample();
		example.setOrderByClause("create_time desc");
		com.shopx5.pojo.TbPushXSMsgExample.Criteria criteria = example.createCriteria();
//		// 设置条件:
		criteria.andTargetUserIdEqualTo(userId);
		criteria.andMsgTypeEqualTo(msgType);
		
		Page<TbPushXSMsg> page = (Page<TbPushXSMsg>) tbPushXSMsgMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}

}
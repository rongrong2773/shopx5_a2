package com.shopx5.service.xunsi;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbArticle;
import com.shopx5.pojo.TbCollectXS;
import com.shopx5.pojo.TbPicVideoChar;
import com.shopx5.pojo.TbPicVideoCharConnected;
/**
 * 
 * @author sjx
 * 2019年7月3日 下午10:59:09
 */
public interface PicCharVideoArticleService {

	void save(TbPicVideoChar tbPicVideoChar);
	
	void saveArticle(TbArticle tbArticle);
	
	void updateHeatCount(Integer heatCount, String pvcId);
	
	// 增加图文/视频浏览量
	void addReadTimeTwv(Long usrId, String infoId);
	
	// 增加原创文章浏览量
	void addReadTimeArti(Long usrId, String infoId);

	PageResult findPageByTimeDesc(String usrId,Integer pageNum, Integer pageSize);
	
	PageResult findPageByHeatDesc(String userId,Integer pageNum, Integer pageSize);
	// 查找关注的人发布的图文/视频 分页信息
	PageResult findPageByConcern(Long usrId,Integer pageNum, Integer pageSize);
	
	// 获取原创文章分页列表
	PageResult findArtiPageById(Long usrId, Integer pageNum, Integer pageSize);
	
	PageResult findArtiPage(Long usrId, Integer pageNum, Integer pageSize);
	
	TbPicVideoChar findEntityByPrimaryId(String id);
	
	TbArticle findArtiEntityByPrimaryId(String id);


	// 获取图文/视频 浏览数
	Integer getReadTimeTwv(String infoId);
	// 获取原创文章 浏览数
	Integer getReadTimeArti(String infoId);
	
	// 添加/取消收藏
	void addCollect(String usrId, String aimId, String aimType);
	
	// 查看是否被收藏
	TbCollectXS findIsCollected(String usrId, String aimId);
	
	// 取消收藏
	void cancelCollect(String usrId, String aimId);
	
	PageResult findPageByIdByTimeDesc(String usrId, Integer pageNum, Integer pageSize);
	
	// 删除图文/视频
	void delTwvById(String usrId, String twvId);
	
	// 保存 图文、视频的 "点赞/收藏/分享/浏览" 实体信息
	void save2(TbPicVideoCharConnected bean2);
	
	// 根据用户id和图文/视频id查找数据库记录
	TbPicVideoCharConnected findEntityByUsrIdPcvId(String usrId,String pcvId);
	
	void delConnEntity(String usrId, String pcvId);
	
	// 查看用户发布的图文/视频 数目
	Integer findPicCharCountById(String userId);
	
	// 查看原创文章数目
	Integer findArtiCountById(String userId);
	
	List<TbPicVideoChar> findListByHeat(String usrId);
	
	void updateAricleHeat(Integer heatCount,String articleId);

	void updateCollectHeat(Integer collectCount, String pvcId);
	
	// 科技
	PageResult findTechTypeArtiPage(Integer pageNum, Integer pageSize);
	// 生活
	PageResult findLifeTypeArtiPage(Integer pageNum, Integer pageSize);
	// 名人
	PageResult findCelebrityTypeArtiPage(Integer pageNum, Integer pageSize);
	// 影视
	PageResult findFilmTypeArtiPage(Integer pageNum, Integer pageSize);
	// 哲学
	PageResult findPhilosophyTypeArtiPage(Integer pageNum, Integer pageSize);
	// 工业
	PageResult findIndustryTypeArtiPage(Integer pageNum, Integer pageSize);
	// 文学
	PageResult findLiteratureTypeArtiPage(Integer pageNum, Integer pageSize);
	// 互联网
	PageResult findInternetTypeArtiPage(Integer pageNum, Integer pageSize);
	// 体育
	PageResult findSportsTypeArtiPage(Integer pageNum, Integer pageSize);
	// 健康
	PageResult findHealthTypeArtiPage(Integer pageNum, Integer pageSize);
	// 经济
	PageResult findEconomicsTypeArtiPage(Integer pageNum, Integer pageSize);
	// 其他
	PageResult findOtherTypeArtiPage(Integer pageNum, Integer pageSize);
	
	
	// 获取用户推送消息分页列表
	PageResult findPushMsgPage(String userId, String msgType, Integer pageNum, Integer pageSize);
}

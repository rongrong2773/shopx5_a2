package com.shopx5.service.xunsi;

import java.util.List;

import com.shopx5.pojo.TbAppInfo;

/**
 * 
 * @author sjx
 * 2019年7月2日 上午9:00:32
 */
public interface AppInfoService {

	List<TbAppInfo> findVers();

	void save(TbAppInfo tbAppInfo);
}

package com.shopx5.service.xunsi;

import java.util.HashSet;
import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbPushXSMsg;
import com.shopx5.pojo.TbUserConnected;

/**
 * 
 * @author sjx
 * 2019年6月29日 下午5:35:37
 */
public interface MobileUserService {

	/**
	 * 建立关注
	 * @param usrId    用户id
	 * @param aimUsrId    被关注的用户id
	 */
	void addConnWith(Long usrId, Long aimUsrId);
	// 取消关注
	void cancelConnWith(Long usrId, Long aimUsrId);
	
	void update(TbMobileUser bean);
	
	TbMobileUser findEntityById(String usrId);
	TbMobileUser findEntityByIdToken(Long usrId, String token);
	
	
	// 查询用户好友分页列表，以及返回好友详情的所有字段
	PageResult findPageUsrAllInfo(Long ownerId, Integer page, Integer rows);
	// 查询用户好友分页列表，以及返回好友详情的部分字段
	PageResult findPageUsrPartInfo(Long ownerId, Integer page, Integer rows);
	
	PageResult findPage(Long usrId, Integer pageNum, Integer pageSize);
	
	PageResult findPage2(Long usrId, Integer pageNum, Integer pageSize);
	
	PageResult findMyConcernPageById(List<String> usrIdList,Long usrId, Integer pageNum, Integer pageSize);

	// 查找所有认证用户 
	List<TbMobileUser> findaAuthUsrsList();
	// 获取所有的用户token
	List<String> findTokenList();
	// 查找我关注的人id列表
	List<String> findMyConcernListById(String usrId);
	// 查找关注我的人/我的粉丝id列表
	List<String> findMyFansIdsById(String usrId);
	// 查询"个人中心 - 我的消息  - 系统消息"
	PageResult findMsgByPage(Integer pageNum, Integer pageSize,String userId);

	
	// 根据用户id查询token
	String findTokenById(Long usrId);
	
	Long findCascadedIdByCascadingId(Long usrId);
	
	HashSet<String> findMyConcernHashSetById(String usrId);
	
	HashSet<String> findMyFansHashSetById(String usrId);
	
	HashSet<String> findAuthedUsrHashSet();    // 查找所有认证用户
	
	String findClientIdByPrimaryId(Long aimUsrId);
	
	// 查询关注表中两用户的关联关系: usrId 是否关注了 aimUsrId
	TbUserConnected findEntityByUsrAimUsrId(Long usrId, Long aimUsrId);

	String findNickNameByPrimaryId(Long usrId);    // 根据id查询用户昵称
	
	TbMobileUser findEntity(String userId, String token, String clientId);    // 返回个人中信息
	
	Integer findMsgCountById(String userId);    // 查询 ”个人中心 - 我的消息“ 数目

	List<TbMobileUser> findSimiliarUserList();    // 取出20个趣味相同的用户
	
	/**
	 * 给aimUserId增加备注
	 * @param userId    登录用户id
	 * @param aimUserId    被关注的用户id
	 * @param aimUserRemark    备注
	 */
	void updateRemark(String userId, String aimUserId, String aimUserRemark);
	
	void shieldUser(Long userId, String aimUserId);    // 拉黑用户(aimUserId)
	
	String findShieldUserById(String userId);    // 查询用户userId的拉黑列表
	
	void savePushMsgEntity(TbPushXSMsg bean);
	
	
}

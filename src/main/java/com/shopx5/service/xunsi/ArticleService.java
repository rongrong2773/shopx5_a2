package com.shopx5.service.xunsi;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbArticle;

/**
 * 原创文章,学富 - 推荐
 * @author sjx
 * 2019年7月18日 上午11:12:10
 */
public interface ArticleService {

	void add(TbArticle bean);

	TbArticle findOne(String id);

	PageResult findPage(Long usrId, Integer page, Integer rows);

	void update(TbArticle bean);
}

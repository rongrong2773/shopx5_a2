package com.shopx5.service.xunsi;

import com.shopx5.pojo.TbHeatXS;

/**
 * 寻思、
 * @author sjx
 * 2019年8月16日 下午5:29:23
 */
public interface HeatXSService {

	void save(TbHeatXS entity);

	TbHeatXS findEntity(String userId, String aimInfoId, String type);

	void cancel(String userId, String aimInfoId, String type);
}

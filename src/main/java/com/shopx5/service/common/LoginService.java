package com.shopx5.service.common;

import com.shopx5.common.Result;

public interface LoginService {
	Result login(String username, String password, String auto_login); //1  用户登录 设置token
	Result getUserByToken(String token);            //2  通过token查询用户信息
	Result logout(String token);                    //3  安全退出	
}

package com.shopx5.service.common;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.shopx5.pojo.SpUser;

public interface IndexService {
	//1 获取user
	public SpUser getUser(HttpServletRequest request);
	//角色CURD权限,和按钮BUTTON权限  *
	public Map<String, Integer> getPower(HttpServletRequest request);
}

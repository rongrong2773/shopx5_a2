package com.shopx5.service.common;

import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;

public interface RegisterService {
	String checkData(String data, int type);                   //1.校验数据 1用户名 2手机 3email 4code
	Result register(SpUser spUser, SpUserInfo spUserInfo);    //2.注册用户 及详情
	SpUserInfo getUserInfo(String phone);                      //3.通过手机获取用户详情对象	
	SpUser getSpUserByPhone(String phone);                     //4.通过手机获取用户对象
}

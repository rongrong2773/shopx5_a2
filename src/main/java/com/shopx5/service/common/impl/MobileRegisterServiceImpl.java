package com.shopx5.service.common.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.TbMobileUserMapper;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbMobileUserLoginInfo;
import com.shopx5.service.common.MobileRegisterService;
/**
 * 用户注册Service
 * @author sjx
 * 2019年6月26日 下午3:54:00
 */
@Service
public class MobileRegisterServiceImpl implements MobileRegisterService{

	@Autowired
	private TbMobileUserMapper mobileUserMapper;
	
	@Override
	public Long checkPhone(String phone) {
		return mobileUserMapper.checkPhone(phone);
	}

	@Override
	public void add(TbMobileUser mbUser) {
		mobileUserMapper.add(mbUser);
	}

	@Override
	public Long findPrimaryIdByPhone(String phone) {
		return mobileUserMapper.findPriIdByPhone(phone);
	}

	@Override
	public void updateTokenClientIdLoginTimes(String tokenId,String clientId, Integer loginTimes,Long usrId) {
		mobileUserMapper.updateTokenClientIdLoginTimes(tokenId,clientId,loginTimes, usrId);
	}

	@Override
	public TbMobileUser findPartFieldsByPhone(String phone) {
		return mobileUserMapper.findPartFieldsByPhone(phone);
	}
	
	// 保存  个人中心 - 设置 - 最新登陆信息
	@Override
	public void saveLoginInfoEntity(TbMobileUserLoginInfo tbLoginInfo) {
		mobileUserMapper.saveUsrLoginInfoEntity(tbLoginInfo);
	}
}

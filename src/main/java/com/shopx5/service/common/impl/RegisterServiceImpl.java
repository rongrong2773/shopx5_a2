package com.shopx5.service.common.impl;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.common.Result;
import com.shopx5.mapper.SpUserInfoMapper;
import com.shopx5.mapper.SpUserMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserExample;
import com.shopx5.pojo.SpUserExample.Criteria;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.SpUserInfoExample;
import com.shopx5.service.common.RegisterService;
import com.shopx5.utils.Encrypt;
import com.shopx5.utils.IDUtils;
/**
 * shopx5管理系统
 * @author sjx
 * 2019年6月26日 下午3:57:40
 */
@Service
public class RegisterServiceImpl implements RegisterService {
	@Autowired
	private SpUserMapper userMapper;
	@Autowired
	private SpUserInfoMapper userInfoMapper;
	
	@Override
	public String checkData(String param, int type) {
		//设置查询条件
		SpUserExample example = new SpUserExample();
		Criteria criteria = example.createCriteria();		
		SpUserInfoExample infoExample = new SpUserInfoExample();
		com.shopx5.pojo.SpUserInfoExample.Criteria infoCriteria = infoExample.createCriteria();
		//1.判断用户名是否可用
		if (type == 1) {
			criteria.andUserNameEqualTo(param);
		//2.判断手机号是否可以使用 
		}else if (type == 2) {
			infoCriteria.andTelephoneEqualTo(param);
		//3.判断邮箱是否可以使用
		}else if (type == 3) {
			infoCriteria.andEmailEqualTo(param);
		}else {
			return "非法数据";
		}
		//执行查询
		if (type == 1){
			List<SpUser> list = userMapper.selectByExample(example);
			if (list != null && list.size() > 0) {
				//查询到数据，说明已占用，返回false
				return "false";
			}
		}else if (type == 2 ||type == 3) {
			List<SpUserInfo> infoList = userInfoMapper.selectByExample(infoExample);
			if (infoList != null && infoList.size() > 0) {
				//查询到数据，说明已占用，返回false
				return "false";
			}
		}
		return "true";
	}

	@Override
	public Result register(SpUser spUser, SpUserInfo spUserInfo) {
		//补全pojo的属性
		String str32 = IDUtils.get32UUID();
		spUser.setUserId(str32);
		spUser.setState(1);
		spUser.setCreateTime(new Date());
		spUser.setUpdateTime(new Date());
		//密码要进行md5加密
		String md5 = Encrypt.md5(spUser.getPassword(), spUser.getUserName());
		spUser.setPassword(md5);
		//本工程,特许补充
	    //spUser.setDeptId("b8a02ba94b174c28b4327f2112bde9ba"); //商城注册用户,无管理部门
		spUserInfo.setUserInfoId(str32);                        //用户扩展
		spUserInfo.setHeadPic("/shop/images/default_user_portrait.gif");
		//保存数据
		userMapper.insert(spUser);
		userInfoMapper.insert(spUserInfo);
		//返回注册成功
		return Result.ok();
	}

	@Override
	public SpUserInfo getUserInfo(String phone) {
		//获取UserInfo对象
		SpUserInfoExample infoExample = new SpUserInfoExample();
		com.shopx5.pojo.SpUserInfoExample.Criteria infoCriteria = infoExample.createCriteria();
		infoCriteria.andTelephoneEqualTo(phone);
		List<SpUserInfo> list = userInfoMapper.selectByExample(infoExample);
		//取对象
		SpUserInfo spUserInfo = null;
		if (list != null && list.size() > 0) {
			spUserInfo = list.get(0);
		}
		return spUserInfo;
	}

	@Override
	public SpUser getSpUserByPhone(String phone) {
		SpUserInfo userInfo = getUserInfo(phone);
		SpUser spUser = userMapper.selectByPrimaryKey(userInfo.getUserInfoId());
		return spUser;
	}
	
}



package com.shopx5.service.common.impl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopx5.common.Result;
import com.shopx5.jedis.JedisClient;
import com.shopx5.pojo.SpUser;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.common.LoginService;
import com.shopx5.utils.CookieUtils;
import com.shopx5.utils.JsonUtils;
/**
 * @author s h o p X 5 管理系统
 * @qq 1503501970
 */
@Service
public class IndexServiceImpl implements IndexService {
	@Value("${TOKEN_KEY}")
	private String TOKEN_KEY;
	@Value("${CART_KEY}")
	private String CART_KEY;     //X5_CART
	@Value("${USER_CART}")
	private String USER_CART;
	@Autowired
	private LoginService loginService;
	@Autowired
	private JedisClient jedisClient;
	
	@Value("${USER_QX}")
	private String USER_QX;
	@Value("${USER_BUTTON}")
	private String USER_BUTTON;
	
	//1 获取user
	@Override
	public SpUser getUser(HttpServletRequest request) {
		String token = CookieUtils.getCookieValue(request, TOKEN_KEY);
		if (StringUtils.isBlank(token))	return null;
		Result result = loginService.getUserByToken(token);
		if (result.getData() == null) return null;
		return (SpUser) result.getData();
	}
	
	//角色CURD权限,和按钮BUTTON权限  ***
	public Map<String, Integer> getPower(HttpServletRequest request) {
		String token = CookieUtils.getCookieValue(request, TOKEN_KEY);
		//重新封装QX
		Map<String, Integer> map = new HashMap<>();
		String json = jedisClient.get(USER_QX + ":" + token);
		Map<String, Integer> qMap = JsonUtils.jsonToPojo(json, Map.class);
		for (String string : qMap.keySet()) {
			map.put(string, qMap.get(string));
		}
		//重新封装BUTTON
		String json2 = jedisClient.get(USER_BUTTON + ":" + token);
		Map<String, Integer> bMap = JsonUtils.jsonToPojo(json2, Map.class);
		for (String string : bMap.keySet()) {
			map.put(string, bMap.get(string));
		}
		return map;
	}
}



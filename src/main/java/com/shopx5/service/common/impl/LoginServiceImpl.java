package com.shopx5.service.common.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopx5.common.Result;
import com.shopx5.jedis.JedisClient;
import com.shopx5.mapper.SpUserMapper;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserExample;
import com.shopx5.pojo.SpUserExample.Criteria;
import com.shopx5.service.common.LoginService;
import com.shopx5.utils.Encrypt;
import com.shopx5.utils.JsonUtils;
/**
 * @author s h o p X 5 管理系统
 * @qq 1503501970
 */
@Service
public class LoginServiceImpl implements LoginService {
	@Value("${USER_SESSION}")
	private String USER_SESSION; //默认登录
	@Value("${USER_SESSIONAUTO}")
	private String USER_SESSIONAUTO; //7天登录	
	@Value("${SESSION_EXPIRE}")
	private Integer SESSION_EXPIRE;	
	@Autowired
	private JedisClient jedisClient;
	@Autowired
	private SpUserMapper userMapper;
	@Autowired
	private UserMapper uMapper;

	@Value("${USER_QX}")
	private String USER_QX;
	@Value("${USER_BUTTON}")
	private String USER_BUTTON;
	
	//1   用户登录 设置token
	@Override
	public Result login(String username, String password, String auto_login) {
		//判断用户名和密码是否正确
		SpUserExample example = new SpUserExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNameEqualTo(username);
		List<SpUser> list = userMapper.selectByExample(example);
		if (list == null || list.size() == 0) {
			//返回登录失败
			return Result.build(400, "用户名或密码不正确");
		}
		SpUser user = list.get(0);
		
//		//一对一:用户+详情 user+userInfo
//		User user = uMapper.findUserAndUserInfo(username);
//		if (user == null) {
//			//返回登录失败
//			return Result.build(400, "用户名或密码不正确");
//		}		
		
		//密码进行md5加密然后再校验
		//if (!DigestUtils.md5DigestAsHex(password.getBytes()).equals(user.getPassword())) {
		if(!password.equals("phonePass")){
			if (!Encrypt.md5(password, username).equals(user.getPassword())) {
				//返回登录失败
				return Result.build(400, "用户名或密码不正确");
			}
		}
		//生成token,使用uuid
		String token = UUID.randomUUID().toString();
		//清空密码
		user.setPassword(null);
		
		//把用户信息保存到redis，key就是token，value就是用户信息
		jedisClient.set(USER_SESSION + ":" + token, JsonUtils.objectToJson(user));
		if (auto_login == null) {
			//设置key的过期时间  正常默认半小时
			jedisClient.expire(USER_SESSION + ":" + token, SESSION_EXPIRE);
			return Result.ok(token);
		}
		//勾选_七天自动登录时,604800
		jedisClient.expire(USER_SESSIONAUTO + ":" + token, SESSION_EXPIRE);
		//返回登录成功，其中要把token返回。
		return Result.ok(token);
	}

	//2   通过token查询用户信息    ::返回  为user的Json对象 或 Result.data(user)  200 "OK" user ★★
	@Override
	public Result getUserByToken(String token) {
		String json = jedisClient.get(USER_SESSION + ":" + token);   //取String类型 user  
		if (StringUtils.isBlank(json)) {
			json = jedisClient.get(USER_SESSIONAUTO + ":" + token);             //七天自动登录
			if (StringUtils.isNotBlank(json)) {
				SpUser spUser = JsonUtils.jsonToPojo(json, SpUser.class);     //取对象类型 user
				//重置QX/BUTTON过期时间   权限与登陆过期同步
				jedisClient.expire(USER_QX + ":" + token,         SESSION_EXPIRE);
				jedisClient.expire(USER_BUTTON + ":" + token,     SESSION_EXPIRE);
				return Result.ok(spUser);
			}
			return Result.build(400, "用户登录已经过期");
		}
		//重置Session过期时间
		jedisClient.expire(USER_SESSION + ":" + token, SESSION_EXPIRE);
		//把json转换成User对象
		SpUser spUser = JsonUtils.jsonToPojo(json, SpUser.class);  //2.取对象类型 user
		//重置QX/BUTTON过期时间   权限与登陆过期同步
		jedisClient.expire(USER_QX + ":" + token,         SESSION_EXPIRE);
		jedisClient.expire(USER_BUTTON + ":" + token,     SESSION_EXPIRE);
		return Result.ok(spUser);
	}

	//3   安全退出
	@Override
	public Result logout(String token) {
		//通过token设置redis非数据库 设置时间过期
		jedisClient.del(USER_SESSION + ":" + token);
		//设置QX/BUTTON过期时间  设置权限后需重新登陆
		jedisClient.del(USER_QX + ":" + token);
		jedisClient.del(USER_BUTTON + ":" + token);
		//返回响应
		return Result.ok();
	}
	
}



package com.shopx5.service.common.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.ScAreaMapper;
import com.shopx5.pojo.ScArea;
import com.shopx5.pojo.ScAreaExample;
import com.shopx5.pojo.ScAreaExample.Criteria;
import com.shopx5.service.common.AreaService;
/**
 * @author s h o p X 5 管理系统
 * @qq 1503501970
 */
@Service
public class AreaServiceImpl implements AreaService {
	@Autowired
	private ScAreaMapper areaMapper;
	
	//1 保存省
	@Override
	public void addArea(Integer id, Integer parentId, String name, String shortName, Integer lev, Integer code) {
		ScArea area = new ScArea();
		area.setId(id);
		area.setParentId(parentId);
		area.setName(name);
		area.setShortName(shortName);		
		area.setLev(lev);
		area.setCode(code);		
		areaMapper.insert(area);
	}
	
	//2 保存市和区
	@Override
	public void addArea(Integer id, Integer parentId, String name, Integer lev, Integer code) {
		ScArea area = new ScArea();
		area.setId(id);
		area.setParentId(parentId);
		area.setName(name);	
		area.setLev(lev);
		area.setCode(code);		
		areaMapper.insert(area);
	}
	
	//1 自动加载联动一级菜单
	@Override
	public List<ScArea> getArea(Integer parentId) {
		ScAreaExample example = new ScAreaExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		return areaMapper.selectByExample(example);
	}	
	
}



package com.shopx5.service.common.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.TbMobilePartUserMapper;
import com.shopx5.pojo.TbMobilePartUser;
import com.shopx5.service.common.MobilePartService;
/**
 * @author sjx
 * 2019年7月19日 上午11:45:52
 */
@Service
public class MobilePartServiceImpl implements MobilePartService{

	@Autowired
	private TbMobilePartUserMapper tbMPartUserMapper;
	
	@Override
	public void add(TbMobilePartUser mPartUser) {
		tbMPartUserMapper.add(mPartUser);
	}

}

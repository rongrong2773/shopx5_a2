package com.shopx5.service.common;


import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbMobileUserLoginInfo;

/**
 * 
 * @author sjx
 * 2019年6月26日 下午3:51:06
 */
public interface MobileRegisterService {
	
	// 查询手机号是否已经注册
	Long checkPhone(String phone);

	// 新增用户
	void add(TbMobileUser mbUser);

	Long findPrimaryIdByPhone(String phone);

	void updateTokenClientIdLoginTimes(String tokenId, String clientId,Integer loginTimes,Long usrId);

	TbMobileUser findPartFieldsByPhone(String phone);

	// 保存最新登陆信息
	void saveLoginInfoEntity(TbMobileUserLoginInfo tbLoginInfo);


}

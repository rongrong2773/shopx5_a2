package com.shopx5.service.common;

import java.util.List;

import com.shopx5.pojo.ScArea;

public interface AreaService {
	//1 保存省
	void addArea(Integer id, Integer parentId, String name, String shortName, Integer lev, Integer code);	
	//2 保存市和区
	void addArea(Integer id, Integer parentId, String name, Integer lev, Integer code);
	//3 自动加载联动一级菜单
	List<ScArea> getArea(Integer parentId);
}

package com.shopx5.service.shop;

import java.util.List;
import java.util.Map;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.SsItemCat;

/**
 * ItemCat服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface ItemCatService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<SsItemCat> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<SsItemCat> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param itemCat, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(SsItemCat itemCat, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(SsItemCat itemCat);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SsItemCat findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(SsItemCat itemCat);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
	//缓存分类
	public void cacheCat();
	
}

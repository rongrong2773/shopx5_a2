package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbOrder;
import com.shopx5.pojo.TbOrderItem;
import com.shopx5.pojo.TbPayLog;

/**
 * Order服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface OrderService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbOrder> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbOrder> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param order, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbOrder order, Integer pageNum, Integer pageSize, Integer grade);
	
	/**
	 * 增加
	 */
	public String add(TbOrder order);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbOrder findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbOrder order);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	// 根据用户ID获取支付日志
	public TbPayLog searchPayLogFromRedis(String userId);
		
	// 支付成功修改状态 wxpay
	public void updateOrderStatus(String out_trade_no, String transaction_id);
	// 支付成功修改状态 alipay bankpay
	public void updateOrderStatus(String orderId, int num);
	
	// 根据orderId 获取订单项
	public List<TbOrderItem> findItem(Long orderId);
	
	//更新状态
	public void updateStatus(Long orderId, String status);
}

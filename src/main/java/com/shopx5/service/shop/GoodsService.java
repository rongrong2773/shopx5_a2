package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbGoods;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.group.Good;
import com.shopx5.pojo.group.Goods;

/**
 * Goods服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface GoodsService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbGoods> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbGoods> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param goods, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbGoods goods, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(Goods goods);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public Goods findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(Goods goods);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
	// 商品审核
	public void updateStatus(Long[] ids, String status);
	
	// 上下架
	public void updateMarketable(Long[] ids, String status);
	
	// 根据SPU_ID集合查询SKU列表
	public List<TbItem> findItemListByGoodsIdListAndStatus(Long[] goodsIds, String status);
	
	// 获取用户+详情
	public Good getGoodAndInfo(Long goodsId);
	// 获取用户+详情 并处理image
	public Good getGood(Long goodsId);
}

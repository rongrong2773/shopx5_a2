package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbSeckillGoods;
import com.shopx5.pojo.TbSeckillOrder;

/**
 * SeckillOrder服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface SeckillOrderService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbSeckillOrder> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbSeckillOrder> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param seckillOrder, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbSeckillOrder seckillOrder, Integer pageNum, Integer pageSize, Integer grade);
	
	/**
	 * 增加
	 */
	public void add(TbSeckillOrder seckillOrder);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbSeckillOrder findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbSeckillOrder seckillOrder);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */

	// 秒杀下单
	public void submitOrder(Long seckillId, String userId);
	
	// 从缓存中提取订单
	public TbSeckillOrder searchOrderFromRedisByUserId(String userId);
	// 保存订单到数据库
	public void saveOrderFromRedisToDb(String userId, Long orderId, String transactionId);
	// 删除缓存中订单
	public void deleteOrderFromRedis(String userId, Long orderId);
	
	// 根据orderId 获取订单项
	public TbSeckillGoods findItem(Long orderId);
	// 更新状态
	public void updateStatus(Long orderId, String status);
}





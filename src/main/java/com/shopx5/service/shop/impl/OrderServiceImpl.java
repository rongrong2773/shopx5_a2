package com.shopx5.service.shop.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.IdWorker;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbOrderItemMapper;
import com.shopx5.mapper.TbOrderMapper;
import com.shopx5.mapper.TbPayLogMapper;
import com.shopx5.mapper.TbSellerMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbOrder;
import com.shopx5.pojo.TbOrderExample;
import com.shopx5.pojo.TbOrderExample.Criteria;
import com.shopx5.pojo.TbOrderItem;
import com.shopx5.pojo.TbOrderItemExample;
import com.shopx5.pojo.TbPayLog;
import com.shopx5.pojo.TbSeller;
import com.shopx5.pojo.group.Cart;
import com.shopx5.service.shop.OrderService;
import com.shopx5.utils.CookieUtils;

/**
 * Order服务层实现
 * @author tang shopx5 多商户 商城 系统
 * @qq 1503501970
 */
@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private TbOrderMapper orderMapper;
	@Autowired
	private TbOrderItemMapper orderItemMapper;
	@Autowired
	private TbPayLogMapper payLogMapper;
	@Autowired
	private TbSellerMapper sellerMapper;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private RedisTemplate redisTemplate;
	
	@Autowired
	@Qualifier(value="idWorker")
	private IdWorker idWorker;
	
	@Autowired
	@Qualifier(value="idWorker2")
	private IdWorker idWorker2;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbOrder> findAll() {
		return orderMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbOrder> findByParentId(Long parentId) {
		TbOrderExample example = new TbOrderExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return orderMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param order, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbOrder order, Integer pageNum, Integer pageSize, Integer grade) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbOrderExample example = new TbOrderExample();
		Criteria criteria = example.createCriteria();
		// 设置级别: 1运营 2商户 3用户
		SpUser user = (SpUser) request.getAttribute("spUser");
		if(grade==2){
			if(user.getSellerId()==null) return null;
			criteria.andSellerIdEqualTo(user.getSellerId());
		}
		if(grade==3){
			criteria.andUserIdEqualTo(user.getUserId());
		}
		// 设置条件:
		if(order != null){
			if(order.getStatus()!=null && order.getStatus().length()>0){
				criteria.andStatusEqualTo(order.getStatus());
			}
			if(order.getReceiver()!=null && order.getReceiver().length()>0){
				criteria.andReceiverLike("%"+order.getReceiver()+"%");
			}
		}
		// PageHelper第二种方式
		Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加 1
	 */
	@Override
	public String add(TbOrder order) {
		//1.从redis中提取购物车列表 校验已选
		List<Cart> cartList = (List<Cart>) redisTemplate.boundHashOps("cartList").get(order.getUserId());
		// 获取已选itemIds
		String itemIds = CookieUtils.getCookieValue(request, "itemIds", "UTF-8");
		List<String> list = JSON.parseArray(itemIds, String.class);
		// 重新封装已选
		cartList = checkCartList(cartList, list, true);
		
		//订单ID集合
		List<String> orderIdList = new ArrayList();
		//总金额   :支付日志使用
		double total_money = 0;
		
		//2.循环购物车列表添加订单
		for(Cart cart:cartList){
			if(cart.getOrderItemList().size()==0) continue;
			TbOrder tbOrder = new TbOrder();
			long orderId = idWorker.nextId(); //获取ID			
			tbOrder.setOrderId(orderId);
			tbOrder.setPaymentType(order.getPaymentType());//支付类型
			tbOrder.setStatus("1");//未付款 
			tbOrder.setCreateTime(new Date());//下单时间
			tbOrder.setUpdateTime(new Date());//更新时间
			tbOrder.setUserId(order.getUserId());//当前用户
			tbOrder.setReceiverAreaName(order.getReceiverAreaName());//收货人地址
			tbOrder.setReceiverMobile(order.getReceiverMobile());//收货人电话
			tbOrder.setReceiver(order.getReceiver());//收货人
			tbOrder.setSourceType(order.getSourceType());//订单来源
			//商家ID - 店铺名
			//tbOrder.setSellerId(cart.getSellerId());
			TbSeller seller = sellerMapper.selectByPrimaryKey(cart.getSellerId());
			tbOrder.setSellerId(seller.getNickName());
			
			double money = 0;//合计数
			//循环购物车中每条明细记录
			for(TbOrderItem orderItem:cart.getOrderItemList()){
				orderItem.setId(idWorker2.nextId());//主键
				orderItem.setOrderId(orderId);//订单编号
				orderItem.setSellerId(cart.getSellerId());//商家ID  **未设置
				orderItemMapper.insert(orderItem);
				money += orderItem.getTotalFee().doubleValue();
			}
			tbOrder.setPayment(new BigDecimal(money));//合计
			orderMapper.insert(tbOrder);
			
			orderIdList.add(orderId+"");
			total_money += money;
		}
		
		//添加支付日志 在线支付1
		String outTradeNo = idWorker.nextId()+"";
		if("1".equals(order.getPaymentType())){
			TbPayLog payLog = new TbPayLog();
			payLog.setOutTradeNo(outTradeNo);//支付日志订单号
			payLog.setCreateTime(new Date());
			payLog.setUserId(order.getUserId());//用户ID
			payLog.setOrderList(orderIdList.toString().replace("[", "").replace("]", ""));//订单ID串
			payLog.setTotalFee((long)(total_money*100));//金额（分）
			payLog.setTradeState("0");//交易状态
			payLog.setPayType("1");//微信
			payLogMapper.insert(payLog);
			redisTemplate.boundHashOps("payLog").put(order.getUserId(), payLog); //放入缓存
		}
		
		//3.清除redis中的购物车 已选的
		List<Cart> cartListN = (List<Cart>) redisTemplate.boundHashOps("cartList").get(order.getUserId());
		//cartListN = checkCartList(cartListN, list, false);
		for (String itemId : list) {
			cartListN = checkCart(cartListN, Long.valueOf(itemId));
		}
		redisTemplate.boundHashOps("cartList").put(order.getUserId(), cartListN);
		
		//4.返回订单id串
		return outTradeNo;
	}
	
	// 重新封装已选 迭代处理
	public List<Cart> checkCartList(List<Cart> cartList, List<String> list, Boolean bool) {
		for (Cart cart : cartList) {
			List<TbOrderItem> orderItemList = cart.getOrderItemList();
			Iterator<TbOrderItem> it = orderItemList.iterator();
			while(it.hasNext()){
			    TbOrderItem item = it.next();
			    if(bool){
			    	if(!list.contains(String.valueOf(item.getItemId()))){  //如果itemId不包含,移除
				        it.remove();
				    }
			    }else{
			    	if(list.contains(String.valueOf(item.getItemId()))){  //如果itemId包含,移除
				        it.remove();
				    }
				}
			}
		}
		return cartList;
	}
	// 移除
	private List<Cart> checkCart(List<Cart> cartList, Long itemId){
		for (Cart cart : cartList) {
			List<TbOrderItem> orderItemList = cart.getOrderItemList();
			for (TbOrderItem orderItem : orderItemList) {
				if (orderItem.getItemId().equals(itemId)) {
					orderItemList.remove(orderItem);
					break;
				}
			}
			if(orderItemList.size()==0){
				cartList.remove(cart);
				break;
			}
		}
		return cartList;
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbOrder findOne(Long id) {
		return orderMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbOrder order) {
		orderMapper.updateByPrimaryKey(order);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			TbOrder order = orderMapper.selectByPrimaryKey(id);
			order.setStatus("6");
			orderMapper.updateByPrimaryKeySelective(order);
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 根据用户ID获取支付日志
	@Override
	public TbPayLog searchPayLogFromRedis(String userId) {		
		return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
	}
	
	// 支付成功修改状态 wxpay
	@Override
	public void updateOrderStatus(String out_trade_no, String transaction_id) {
		//1.修改支付日志的状态及相关字段
		TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
		payLog.setPayTime(new Date());//支付时间
		payLog.setTradeState("1");//交易成功
		payLog.setTransactionId(transaction_id);//微信的交易流水号
		payLogMapper.updateByPrimaryKey(payLog);//修改
		
		//2.修改订单表的状态
		String orderList = payLog.getOrderList();// 订单ID 串
		String[] orderIds = orderList.split(",");
		for(String orderId:orderIds){
			TbOrder order = orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
			order.setStatus("2");//已付款状态
			order.setPaymentTime(new Date());//支付时间
			orderMapper.updateByPrimaryKey(order);			
		}
		
		//3.清除缓存中的payLog
		redisTemplate.boundHashOps("payLog").delete(payLog.getUserId());
	}
	
	// 支付成功修改状态 alipay bankpay
	@Override
	public void updateOrderStatus(String orderId, int num) {
		TbOrder order = orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
		order.setStatus(num+"");//已付款状态
		order.setPaymentTime(new Date());//支付时间
		orderMapper.updateByPrimaryKey(order);
	}
	
	// 根据orderId 获取订单项
	@Override
	public List<TbOrderItem> findItem(Long orderId) {
		TbOrderItemExample example = new TbOrderItemExample();
		com.shopx5.pojo.TbOrderItemExample.Criteria criteria = example.createCriteria();
		criteria.andOrderIdEqualTo(orderId);
		List<TbOrderItem> list = orderItemMapper.selectByExample(example);
		return list;
	}
	
	//更新状态
	@Override
	public void updateStatus(Long orderId, String status) {
		TbOrder order = orderMapper.selectByPrimaryKey(orderId);
		order.setStatus(status);
		orderMapper.updateByPrimaryKeySelective(order);
	}
}



package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsSpecificationOptionMapper;
import com.shopx5.pojo.SsSpecificationOption;
import com.shopx5.pojo.SsSpecificationOptionExample;
import com.shopx5.pojo.SsSpecificationOptionExample.Criteria;
import com.shopx5.service.shop.SpecificationOptionService;

/**
 * SpecificationOption服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class SpecificationOptionServiceImpl implements SpecificationOptionService {
	@Autowired
	private SsSpecificationOptionMapper specificationOptionMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<SsSpecificationOption> findAll() {
		return specificationOptionMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<SsSpecificationOption> findByParentId(Long parentId) {
		SsSpecificationOptionExample example = new SsSpecificationOptionExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return specificationOptionMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<SsSpecificationOption> page = (Page<SsSpecificationOption>) specificationOptionMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param specificationOption, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(SsSpecificationOption specificationOption, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		SsSpecificationOptionExample example = new SsSpecificationOptionExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(specificationOption != null){			
			/*if(specificationOption.getName()!=null && specificationOption.getName().length()>0){
				criteria.andNameLike("%"+specificationOption.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<SsSpecificationOption> page = (Page<SsSpecificationOption>) specificationOptionMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(SsSpecificationOption specificationOption) {
		specificationOptionMapper.insert(specificationOption);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SsSpecificationOption findOne(Long id) {
		return specificationOptionMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(SsSpecificationOption specificationOption) {
		specificationOptionMapper.updateByPrimaryKey(specificationOption);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			specificationOptionMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
}

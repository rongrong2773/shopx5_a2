package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsSpecificationOptionMapper;
import com.shopx5.mapper.SsTemplateTypeMapper;
import com.shopx5.pojo.SsBrand;
import com.shopx5.pojo.SsSpecificationOption;
import com.shopx5.pojo.SsSpecificationOptionExample;
import com.shopx5.pojo.SsTemplateType;
import com.shopx5.pojo.SsTemplateTypeExample;
import com.shopx5.pojo.SsTemplateTypeExample.Criteria;
import com.shopx5.service.shop.BrandService;
import com.shopx5.service.shop.TemplateTypeService;

/**
 * TemplateType服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class TemplateTypeServiceImpl implements TemplateTypeService {
	@Autowired
	private SsTemplateTypeMapper templateTypeMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<SsTemplateType> findAll() {
		return templateTypeMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<SsTemplateType> findByParentId(Long parentId) {
		SsTemplateTypeExample example = new SsTemplateTypeExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return templateTypeMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<SsTemplateType> page = (Page<SsTemplateType>) templateTypeMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param templateType, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(SsTemplateType templateType, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		SsTemplateTypeExample example = new SsTemplateTypeExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(templateType != null){			
			if(templateType.getName()!=null && templateType.getName().length()>0){
				criteria.andNameLike("%"+templateType.getName()+"%");
			}
		}
		// PageHelper第二种方式
		Page<SsTemplateType> page = (Page<SsTemplateType>) templateTypeMapper.selectByExample(example);
		
		// 将品牌列表与规格列表放入缓存
		saveToRedis();
		System.out.println("======缓存品牌列表,与规格======");
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(SsTemplateType templateType) {
		templateTypeMapper.insert(templateType);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SsTemplateType findOne(Long id) {
		return templateTypeMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(SsTemplateType templateType) {
		templateTypeMapper.updateByPrimaryKey(templateType);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			templateTypeMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	@Override
	public List<Map> selectOptionList() {
		return templateTypeMapper.selectOptionList();
	}
	
	
	// 根据模板ID,获得规格封装选项
	public List<Map> findSpecList(Long id) {
		//根据ID查询到模板对象
		SsTemplateType typeTemplate = findOne(id);
		// 获得规格的数据spec_ids
		String specIds = typeTemplate.getSpecIds();
		// 将specIds的字符串转成JSON的List<Map>
		List<Map> list = JSON.parseArray(specIds, Map.class);
		// 获得每条记录:                     // [{"id":27,"text":"网络"},{"id":32,"text":"机身内存"}]
		for (Map map : list) {
			// 根据规格的ID 查询规格选项的数据:
			// 设置查询条件:
			SsSpecificationOptionExample example = new SsSpecificationOptionExample();
			com.shopx5.pojo.SsSpecificationOptionExample.Criteria criteria = example.createCriteria();
			criteria.andSpecIdEqualTo(new Long((Integer)map.get("id")));
			List<SsSpecificationOption> specOptionList = specificationOptionMapper.selectByExample(example);
		
			map.put("options", specOptionList); 
		}   // [{"id":27,"text":"网络", options: [{id:98,optionName:"移动2G",spec_id:27,orders:1}]}]
		return list;
	}
	
	
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private SsSpecificationOptionMapper specificationOptionMapper;
	@Autowired
	private BrandService brandService;
	
	/**
	 * 将品牌列表与规格列表放入缓存   solr搜索应用  "在查询模版list时缓存数据"
	 */
	private void saveToRedis(){
		redisTemplate.delete("brandList");
		redisTemplate.delete("specList");
		
		// 查询所有模版
		List<SsTemplateType> templateList = findAll();
		for(SsTemplateType template : templateList){
			//得到品牌列表    以模版ID为key: 将List<品牌>放入缓存 
			//[{"id":1,"text":"联想"}]
			//扩展: 索引需求,增加图片
			List<Map> brandList= JSON.parseArray(template.getBrandIds(), Map.class);
			for (Map map : brandList) {
				long id = (int) map.get("id");
				SsBrand brand = brandService.findOne(id);
				map.put("image", brand.getImage());
				map.put("firstChar", brand.getFirstChar());
			}
			
			//List brandList= JSON.parseArray(template.getBrandIds(), Map.class); // List<Map>
			redisTemplate.boundHashOps("brandList").put(template.getId(), brandList);
			System.out.println("======solr2: 'brandList'品牌 存入======");
			
			//得到规格列表   以模版ID为key: 将List<规格>放入缓存
			//[{"id":27,"text":"网络", options: [{id:98,optionName:"移动2G",spec_id:27,orders:1}]}]
			List<Map> specList = findSpecList(template.getId());
			redisTemplate.boundHashOps("specList").put(template.getId(), specList);
			System.out.println("======solr3: 'specList'规格 存入======");
		}		
	}
	
	
		
}

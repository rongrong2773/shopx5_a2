package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbGoodsDescMapper;
import com.shopx5.pojo.TbGoodsDesc;
import com.shopx5.pojo.TbGoodsDescExample;
import com.shopx5.pojo.TbGoodsDescExample.Criteria;
import com.shopx5.service.shop.GoodsDescService;

/**
 * GoodsDesc服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class GoodsDescServiceImpl implements GoodsDescService {
	@Autowired
	private TbGoodsDescMapper goodsDescMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbGoodsDesc> findAll() {
		return goodsDescMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbGoodsDesc> findByParentId(Long parentId) {
		TbGoodsDescExample example = new TbGoodsDescExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return goodsDescMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbGoodsDesc> page = (Page<TbGoodsDesc>) goodsDescMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param goodsDesc, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbGoodsDesc goodsDesc, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbGoodsDescExample example = new TbGoodsDescExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(goodsDesc != null){			
			/*if(goodsDesc.getName()!=null && goodsDesc.getName().length()>0){
				criteria.andNameLike("%"+goodsDesc.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbGoodsDesc> page = (Page<TbGoodsDesc>) goodsDescMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbGoodsDesc goodsDesc) {
		goodsDescMapper.insert(goodsDesc);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbGoodsDesc findOne(Long id) {
		return goodsDescMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbGoodsDesc goodsDesc) {
		goodsDescMapper.updateByPrimaryKey(goodsDesc);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			goodsDescMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
}

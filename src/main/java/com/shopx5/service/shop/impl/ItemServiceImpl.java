package com.shopx5.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbItemMapper;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.TbItemExample;
import com.shopx5.pojo.TbItemExample.Criteria;
import com.shopx5.service.shop.ItemService;

/**
 * Item服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class ItemServiceImpl implements ItemService {
	@Autowired
	private TbItemMapper itemMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbItem> findAll() {
		return itemMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbItem> findByParentId(Long parentId) {
		TbItemExample example = new TbItemExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return itemMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbItem> page = (Page<TbItem>) itemMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param item, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbItem item, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbItemExample example = new TbItemExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(item != null){			
			/*if(item.getName()!=null && item.getName().length()>0){
				criteria.andNameLike("%"+item.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbItem> page = (Page<TbItem>) itemMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbItem item) {
		itemMapper.insert(item);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbItem findOne(Long id) {
		return itemMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbItem item) {
		itemMapper.updateByPrimaryKey(item);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			itemMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
}

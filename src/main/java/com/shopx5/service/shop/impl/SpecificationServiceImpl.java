package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsSpecificationMapper;
import com.shopx5.mapper.SsSpecificationOptionMapper;
import com.shopx5.pojo.SsSpecification;
import com.shopx5.pojo.SsSpecificationExample;
import com.shopx5.pojo.SsSpecificationExample.Criteria;
import com.shopx5.pojo.SsSpecificationOption;
import com.shopx5.pojo.SsSpecificationOptionExample;
import com.shopx5.pojo.group.Specification;
import com.shopx5.service.shop.SpecificationService;

/**
 * Specification服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class SpecificationServiceImpl implements SpecificationService {
	@Autowired
	private SsSpecificationMapper specificationMapper;
	@Autowired
	private SsSpecificationOptionMapper specificationOptionMapper;
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<SsSpecification> findAll() {
		return specificationMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<SsSpecification> findByParentId(Long parentId) {
		SsSpecificationExample example = new SsSpecificationExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return specificationMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<SsSpecification> page = (Page<SsSpecification>) specificationMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param specification, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(SsSpecification specification, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		SsSpecificationExample example = new SsSpecificationExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(specification != null){			
			if(specification.getSpecName()!=null && specification.getSpecName().length()>0){
				criteria.andSpecNameLike("%"+specification.getSpecName()+"%");
			}
		}
		// PageHelper第二种方式
		Page<SsSpecification> page = (Page<SsSpecification>) specificationMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(Specification specification) {
		// 增加规格
		specificationMapper.insert(specification.getSpecification());
		// 增加规格选项
		for (SsSpecificationOption ssSpecificationOption : specification.getSpecificationOptionList()) {
			// 设置规格id 此设置mapper*
			ssSpecificationOption.setSpecId(specification.getSpecification().getId());
			specificationOptionMapper.insert(ssSpecificationOption);
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public Specification findOne(Long id) {
		Specification specification = new Specification();
		// 根据规格ID查询规格对象
		SsSpecification ssSpecification = specificationMapper.selectByPrimaryKey(id);
		// 根据规格的ID查询规格选项
		SsSpecificationOptionExample example = new SsSpecificationOptionExample();
		com.shopx5.pojo.SsSpecificationOptionExample.Criteria criteria = example.createCriteria();
		criteria.andSpecIdEqualTo(id);
		List<SsSpecificationOption> list = specificationOptionMapper.selectByExample(example);
		// 封装数据
		specification.setSpecification(ssSpecification);
		specification.setSpecificationOptionList(list);
		return specification;
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(Specification specification) {
		// 修改规格
		specificationMapper.updateByPrimaryKey(specification.getSpecification());

		// 注意: 先删除规格选项
		SsSpecificationOptionExample example = new SsSpecificationOptionExample();
		com.shopx5.pojo.SsSpecificationOptionExample.Criteria criteria = example.createCriteria();
		criteria.andSpecIdEqualTo(specification.getSpecification().getId());
		specificationOptionMapper.deleteByExample(example);
		
		// 再添加规格选项
		for (SsSpecificationOption ssSpecificationOption : specification.getSpecificationOptionList()) {
			// 设置规格的ID:
			ssSpecificationOption.setSpecId(specification.getSpecification().getId());
			specificationOptionMapper.insert(ssSpecificationOption);
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			// 删除规格
			specificationMapper.deleteByPrimaryKey(id);
			// 删除规格选项:
			SsSpecificationOptionExample example = new SsSpecificationOptionExample();
			com.shopx5.pojo.SsSpecificationOptionExample.Criteria criteria = example.createCriteria();
			criteria.andSpecIdEqualTo(id);
			specificationOptionMapper.deleteByExample(example);
		}
	}
	
	/**
	 * 自定义
	 */
	@Override
	public List<Map> selectOptionList() {
		return specificationMapper.selectOptionList();
	}
	
}

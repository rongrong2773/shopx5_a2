package com.shopx5.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbContentMapper;
import com.shopx5.pojo.TbContent;
import com.shopx5.pojo.TbContentExample;
import com.shopx5.pojo.TbContentExample.Criteria;
import com.shopx5.service.shop.ContentService;

/**
 * Content服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class ContentServiceImpl implements ContentService {
	@Autowired
	private TbContentMapper contentMapper;
	@Autowired
	private RedisTemplate redisTemplate;
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbContent> findAll() {
		return contentMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbContent> findByParentId(Long parentId) {
		TbContentExample example = new TbContentExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return contentMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbContent> page = (Page<TbContent>) contentMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param content, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbContent content, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbContentExample example = new TbContentExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(content != null){			
			/*if(content.getName()!=null && content.getName().length()>0){
				criteria.andNameLike("%"+content.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbContent> page = (Page<TbContent>) contentMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbContent content) {
		contentMapper.insert(content);
		// 清除缓存  当前分类
		redisTemplate.boundHashOps("content").delete(content.getCategoryId());
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbContent findOne(Long id) {
		return contentMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbContent content) {
		TbContent oldContent = contentMapper.selectByPrimaryKey(content.getId());
		// 清除缓存 当前分类
		redisTemplate.boundHashOps("content").delete(oldContent.getCategoryId());
		
		contentMapper.updateByPrimaryKey(content);
		// 清除缓存 若为新分类
		if(content.getCategoryId() != oldContent.getCategoryId()){
			redisTemplate.boundHashOps("content").delete(content.getCategoryId());
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			TbContent content = contentMapper.selectByPrimaryKey(id);
			// 清除缓存 当前分类1
			redisTemplate.boundHashOps("content").delete(content.getCategoryId());
			contentMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 通过内容类目ID获取数据
	public List<TbContent> findByCategoryId(Long categoryId) {
		// 缓存是否存在:
		List<TbContent> list = (List<TbContent>) redisTemplate.boundHashOps("content").get(categoryId);
		if(list == null){			
			TbContentExample example = new TbContentExample();
			Criteria criteria = example.createCriteria();
			criteria.andStatusEqualTo("1"); //有效
			criteria.andCategoryIdEqualTo(categoryId);
			example.setOrderByClause("sort_order"); //排序
			list = contentMapper.selectByExample(example);
			// 加入缓存
			redisTemplate.boundHashOps("content").put(categoryId, list);
		}else{
			System.out.println("---缓存中获取'content'数据-----------");
		}
		return list;
	}
}

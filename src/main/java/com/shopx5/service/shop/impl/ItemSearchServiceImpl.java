package com.shopx5.service.shop.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FilterQuery;
import org.springframework.data.solr.core.query.GroupOptions;
import org.springframework.data.solr.core.query.HighlightOptions;
import org.springframework.data.solr.core.query.HighlightQuery;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleFilterQuery;
import org.springframework.data.solr.core.query.SimpleHighlightQuery;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.result.GroupEntry;
import org.springframework.data.solr.core.query.result.GroupPage;
import org.springframework.data.solr.core.query.result.GroupResult;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightEntry.Highlight;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.group.SolrGoods;
import com.shopx5.service.shop.ItemSearchService;
/**
 * shopx5 多商户 商城 系统
 */
@Service
public class ItemSearchServiceImpl implements ItemSearchService {
	@Autowired
	private SolrTemplate solrTemplate;
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	// 搜索 1
	@Override
	public Map search(Map searchMap) {
	/*	Map map = new HashMap();
		Query query = new SimpleQuery("*:*");
		Criteria criteria = new Criteria("item_keywordss").is(searchMap.get("keywords")); //条件查询
		query.addCriteria(criteria);
		ScoredPage<TbItem> page = solrTemplate.queryForPage(query, TbItem.class);
		map.put("rows", page.getContent());
		return map; */
		
		Map map = new HashMap();
		// 关键字去掉空格
		String keywords = (String) searchMap.get("keywords");
		searchMap.put("keywords", keywords.replace(" ", ""));
		// 1.查询列表  条件查询:高亮
		map.putAll(searchList(searchMap));
		
		// 2.分组查询  商品分类列表
		List<String> categoryList = searchCategoryList(searchMap);
		map.put("categoryList", categoryList);

 		// 3.查询当前分组的品牌和规格列表 分类切换*
		String category = (String) searchMap.get("category");//参数存在,独立查询
		if (!category.equals("")) {                       
			map.putAll(searchBrandAndSpecList(category));
		} else {
			if (categoryList.size() > 0) {                   //不存在,查询相关分组第一个[0]
				map.putAll(searchBrandAndSpecList(categoryList.get(0)));
			}
		}
		return map;
	}
	
	// 查询列表
	private Map searchList(Map searchMap) {
		Map map = new HashMap();
		// 初始化高亮选项 query
		HighlightQuery query = new SimpleHighlightQuery();
		
		// 1.1 前缀设置
		HighlightOptions highlightOptions = new HighlightOptions().addField("item_title"); //高亮域
		highlightOptions.setSimplePrefix("<em style='color:red'>"); //前缀
		highlightOptions.setSimplePostfix("</em>");
		query.setHighlightOptions(highlightOptions); //为查询对象设置高亮选项

		// 1.2 关键字查询
		Criteria criteria = new Criteria("item_keywordss").is(searchMap.get("keywords")); //Object
		query.addCriteria(criteria);

		// 2.1 按商品分类过滤   filterQuery
		if (!"".equals(searchMap.get("category"))) { //若选择了分类
			FilterQuery filterQuery = new SimpleFilterQuery();
			Criteria filterCriteria = new Criteria("item_category").is(searchMap.get("category"));
			filterQuery.addCriteria(filterCriteria);
			query.addFilterQuery(filterQuery);
		}

		// 2.2 按品牌过滤
		if (!"".equals(searchMap.get("brand"))) { //若选择了品牌
			FilterQuery filterQuery = new SimpleFilterQuery();
			Criteria filterCriteria = new Criteria("item_brand").is(searchMap.get("brand"));
			filterQuery.addCriteria(filterCriteria);
			query.addFilterQuery(filterQuery);
		}
		
		// 2.3 按规格过滤
		if (searchMap.get("spec") != null) {
			Map<String, String> specMap = (Map<String, String>) searchMap.get("spec"); //Map接收转换
			for (String key : specMap.keySet()) {
				FilterQuery filterQuery = new SimpleFilterQuery();
			  //Criteria filterCriteria = new Criteria("item_spec_" + key).is(specMap.get(key));
				Criteria filterCriteria = new Criteria("item_spec_" + key).contains(specMap.get(key));
				filterQuery.addCriteria(filterCriteria);
				query.addFilterQuery(filterQuery);
			}
		}

		// 2.4 按价格区间过滤  0-199 99999999-*
		if (!"".equals(searchMap.get("price"))) {
			String[] price = ((String) searchMap.get("price")).split("-");
			if (!price[0].equals("0")) { //如果最低价格不等于0
				FilterQuery filterQuery = new SimpleFilterQuery();
				Criteria filterCriteria = new Criteria("item_prices").greaterThanEqual(price[0]); //大于等于
				filterQuery.addCriteria(filterCriteria);
				query.addFilterQuery(filterQuery);
			}
			if (!price[1].equals("*")) { //如果最高价格不等于*
				FilterQuery filterQuery = new SimpleFilterQuery();
				Criteria filterCriteria = new Criteria("item_prices").lessThanEqual(price[1]); //小于等于
				filterQuery.addCriteria(filterCriteria);
				query.addFilterQuery(filterQuery);
			}
		}

		// 2.5 分页
		Integer pageNo = (Integer) searchMap.get("pageNo"); //获取页码
		if (pageNo == null) {
			pageNo = 1;
		}
		Integer pageSize = (Integer) searchMap.get("pageSize"); //获取页大小
		if (pageSize == null) {
			pageSize = 20;
		}
		query.setOffset((pageNo - 1) * pageSize); //起始索引
		query.setRows(pageSize);                  //每页记录数

		// 2.6 排序
		String sortValue = (String) searchMap.get("sort");     //升序ASC 降序DESC
		String sortField = (String) searchMap.get("sortField");//排序字段
		if (sortValue != null && !sortValue.equals("")) {
			if (sortValue.equals("ASC")) {
				Sort sort = new Sort(Sort.Direction.ASC, "item_" + sortField);
				query.addSort(sort);
			}
			if (sortValue.equals("DESC")) {
				Sort sort = new Sort(Sort.Direction.DESC, "item_" + sortField);
				query.addSort(sort);
			}
		}

		// *********** 获取高亮结果集 ***********
		// 1.3 高亮页对象
		HighlightPage<SolrGoods> page = solrTemplate.queryForHighlightPage(query, SolrGoods.class);
		// 高亮入口集合(每条记录的高亮入口) :title有高亮,其他无高亮
		List<HighlightEntry<SolrGoods>> entryList = page.getHighlighted();
		for (HighlightEntry<SolrGoods> entry : entryList) {
			// 获取高亮列表(高亮域的个数)
			List<Highlight> highlightList = entry.getHighlights();
			/*
			for(Highlight h : highlightList){
				List<String> sns = h.getSnipplets(); //每个域有可能存储多值
				System.out.println(sns);
			}*/
			// 设置title高亮代码
			if (highlightList.size() > 0 && highlightList.get(0).getSnipplets().size() > 0) {
				SolrGoods item = entry.getEntity();
				item.setTitle(highlightList.get(0).getSnipplets().get(0));
			}
		}
		
		// image[{\"\"}]转json对象
		List<SolrGoods> content = page.getContent();
		for (SolrGoods goods : content) {
			String image = goods.getImage();
			List<Map> list = JSON.parseArray(image, Map.class);
			goods.setImageList(list);
		}
		
		map.put("rows", page.getContent());         // 数据List
		map.put("totalPages", page.getTotalPages());// 总页数
		map.put("total", page.getTotalElements());  // 总记录数
		return map;
	}
	
	/**
	 * 分组查询（查询商品分类列表,过滤出有几种）
	 * @return
	 */
	private List<String> searchCategoryList(Map searchMap) {
		List<String> list = new ArrayList();
		// 初始化 选项query
		Query query = new SimpleQuery("*:*");
		// 根据关键字查询
		Criteria criteria = new Criteria("item_keywordss").is(searchMap.get("keywords")); // where ..
		query.addCriteria(criteria);
		// 设置分组选项 *
		GroupOptions groupOptions = new GroupOptions().addGroupByField("item_category"); // group by ..
		query.setGroupOptions(groupOptions);
		// 获取分组页
		GroupPage<TbItem> page = solrTemplate.queryForGroupPage(query, TbItem.class);
		// 获取分组结果对象
		GroupResult<TbItem> groupResult = page.getGroupResult("item_category");
		// 获取分组入口页
		Page<GroupEntry<TbItem>> groupEntries = groupResult.getGroupEntries();
		// 获取分组入口集合
		List<GroupEntry<TbItem>> entryList = groupEntries.getContent();
		// 将分组的结果添加到返回值中
		for (GroupEntry<TbItem> entry : entryList) {
			list.add(entry.getGroupValue()); 
		}
		return list;
	}

	/**
	 * 根据商品分类名称查询品牌和规格列表
	 * @param category 商品分类名称
	 * @return
	 */
	private Map searchBrandAndSpecList(String category) {
		Map map = new HashMap();
		// 1.根据商品分类名,获取模板ID 如:35
		Long templateId = (Long) redisTemplate.boundHashOps("itemCat").get(category);
		System.out.println("======solr4: 'itemCat' 模版ID 获取======"+templateId);
		
		if (templateId != null) {
			// 2.根据模板ID获取品牌列表
			List brandList = (List) redisTemplate.boundHashOps("brandList").get(templateId);
			map.put("brandList", brandList);
			System.out.println("======solr5: 'brandList' 品牌 获取 条数======"+brandList.size());

			// 3.根据模板ID获取规格列表
			List specList = (List) redisTemplate.boundHashOps("specList").get(templateId);
			map.put("specList", specList);
			System.out.println("======solr6: 'specList' 规格 获取 条数======："+specList.size());
		}
		return map;
	}
	
	
	// 导入
	@Override
	public void importList(List<TbItem> list) {
		// 动态域处理
		for (TbItem item : list) {
			Long goodsId = item.getGoodsId();
		}
		// 动态域处理
		for (TbItem item : list) {
			Map specMap = JSON.parseObject(item.getSpec(), Map.class); //规格为json字符串
			item.setSpecMap(specMap);
		}
		// 执行
		solrTemplate.saveBeans(list);
		solrTemplate.commit();
	}
	
	// 删除
	@Override
	public void deleteByGoodsIds(List goodsIds) { //Arrays.asList(ids)转换
		Query query = new SimpleQuery("*:*");
		Criteria criteria = new Criteria("item_goodsid").in(goodsIds);
		query.addCriteria(criteria);
		// 执行
		solrTemplate.delete(query);
		solrTemplate.commit();
	}
}

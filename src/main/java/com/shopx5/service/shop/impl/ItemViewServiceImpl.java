package com.shopx5.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.mapper.SxViewMapper;
import com.shopx5.pojo.SxView;
import com.shopx5.pojo.SxViewExample;
import com.shopx5.pojo.SxViewExample.Criteria;
import com.shopx5.service.shop.ItemViewService;

@Service
public class ItemViewServiceImpl implements ItemViewService {
	@Autowired
	private SxViewMapper viewMapper;
	
	//获取商品浏览历史
	@Override
	public List<SxView> findViews(String userId) {
		SxViewExample example = new SxViewExample();
		example.setOrderByClause("id desc");
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		List<SxView> list = viewMapper.selectByExample(example);
		return list;
	}
	
	// 添加商品浏览历史
	@Override
	public void addView(SxView view) {
		viewMapper.insert(view);
	}
	
	// 删除
	@Override
	public void delView(SxView view) {
		viewMapper.deleteByPrimaryKey(view.getId());
	}
	
}



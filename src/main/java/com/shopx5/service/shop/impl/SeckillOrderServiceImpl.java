package com.shopx5.service.shop.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.IdWorker;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbSeckillGoodsMapper;
import com.shopx5.mapper.TbSeckillOrderMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbSeckillGoods;
import com.shopx5.pojo.TbSeckillOrder;
import com.shopx5.pojo.TbSeckillOrderExample;
import com.shopx5.pojo.TbSeckillOrderExample.Criteria;
import com.shopx5.service.shop.SeckillOrderService;

/**
 * SeckillOrder服务层实现
 * @author tang shopx5 多商户 商城 系统
 * @qq 1503501970
 */
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {
	@Autowired
	private TbSeckillOrderMapper seckillOrderMapper;
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbSeckillOrder> findAll() {
		return seckillOrderMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbSeckillOrder> findByParentId(Long parentId) {
		TbSeckillOrderExample example = new TbSeckillOrderExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return seckillOrderMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbSeckillOrder> page = (Page<TbSeckillOrder>) seckillOrderMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param seckillOrder, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbSeckillOrder seckillOrder, Integer pageNum, Integer pageSize, Integer grade) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbSeckillOrderExample example = new TbSeckillOrderExample();
		Criteria criteria = example.createCriteria();
		// 设置级别: 1运营 2商户 3用户
		SpUser user = (SpUser) request.getAttribute("spUser");
		if(grade==2){
			if(user.getSellerId()==null) return null;
			criteria.andSellerIdEqualTo(user.getSellerId());
		}
		if(grade==3){
			criteria.andUserIdEqualTo(user.getUserId());
		}
		// 设置条件:
		if(seckillOrder != null){
			if(seckillOrder.getStatus()!=null && seckillOrder.getStatus().length()>0){
				criteria.andStatusEqualTo(seckillOrder.getStatus());
			}
			if(seckillOrder.getReceiver()!=null && seckillOrder.getReceiver().length()>0){
				criteria.andReceiverLike("%"+seckillOrder.getReceiver()+"%");
			}
		}
		// PageHelper第二种方式
		Page<TbSeckillOrder> page = (Page<TbSeckillOrder>) seckillOrderMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbSeckillOrder seckillOrder) {
		seckillOrderMapper.insert(seckillOrder);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbSeckillOrder findOne(Long id) {
		return seckillOrderMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbSeckillOrder seckillOrder) {
		seckillOrderMapper.updateByPrimaryKey(seckillOrder);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			seckillOrderMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	@Autowired
	private TbSeckillGoodsMapper seckillGoodsMapper;
	
	@Autowired
	private IdWorker idWorker;

	// 秒杀下单
	@Override
	public void submitOrder(Long seckillId, String userId) {
		//1.查询缓存中的商品    1 1 1
		TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate.boundHashOps("seckillGoods2").get(seckillId);
		if(seckillGoods==null){
			throw new RuntimeException("商品不存在");
		}
		if(seckillGoods.getStockCount()<=0){
			throw new RuntimeException("商品已经被抢光");
		}
		//2.减少库存 1 0 1
		seckillGoods.setStockCount(seckillGoods.getStockCount()-1);//减库存
		redisTemplate.boundHashOps("seckillGoods2").put(seckillId, seckillGoods);//存入缓存  0
		if(seckillGoods.getStockCount()==0){
			seckillGoodsMapper.updateByPrimaryKey(seckillGoods);	//更新数据库
			redisTemplate.boundHashOps("seckillGoods2").delete(seckillId);
			System.out.println("商品同步到数据库...");
		}
		
		//3.存储秒杀订单 (不向数据库存 ,只向缓存中存储 )
		TbSeckillOrder seckillOrder=new TbSeckillOrder();
		seckillOrder.setId(idWorker.nextId());
		seckillOrder.setSeckillId(seckillId);
		seckillOrder.setMoney(seckillGoods.getCostPrice());
		seckillOrder.setUserId(userId);
		seckillOrder.setSellerId(seckillGoods.getSellerId());//商家ID
		seckillOrder.setCreateTime(new Date());
		seckillOrder.setStatus("0");//状态
		
		redisTemplate.boundHashOps("seckillOrder2").put(userId, seckillOrder); //seckillOrder+seckillId 多个
		System.out.println("保存订单成功(redis)");
	}
	
	// 从缓存中提取订单
	@Override
	public TbSeckillOrder searchOrderFromRedisByUserId(String userId) {
		return (TbSeckillOrder) redisTemplate.boundHashOps("seckillOrder2").get(userId);
	}

	// 保存订单到数据库 秒杀支付
	@Override
	public void saveOrderFromRedisToDb(String userId, Long orderId, String transactionId) {
		//1.从缓存中提取订单数据
		TbSeckillOrder seckillOrder = searchOrderFromRedisByUserId(userId);
		if(seckillOrder==null){
			throw  new  RuntimeException("不存在订单");
		}
		if(seckillOrder.getId().longValue()!=orderId.longValue()){
			throw  new  RuntimeException("订单号不符");
		}
		//2.修改订单实体的属性
		seckillOrder.setPayTime(new Date());//支付日期
		seckillOrder.setStatus("1");//已支付 状态
		seckillOrder.setTransactionId(transactionId);
		
		//3.将订单存入数据库
		seckillOrderMapper.insert(seckillOrder);
		
		//4.清除缓存中的订单 
		redisTemplate.boundHashOps("seckillOrder2").delete(userId);
	}

	// 删除缓存中订单 task工程
	@Override
	public void deleteOrderFromRedis(String userId, Long orderId) {
		//1.查询出缓存中的订单
		TbSeckillOrder seckillOrder = searchOrderFromRedisByUserId(userId);
		if(seckillOrder!=null){
			//2.删除缓存中的订单 
			redisTemplate.boundHashOps("seckillOrder2").delete(userId);
			
			//3.库存回退
			TbSeckillGoods  seckillGoods 
			= (TbSeckillGoods) redisTemplate.boundHashOps("seckillGoods2").get(seckillOrder.getSeckillId());
			if(seckillGoods!=null){ //如果不为空
				seckillGoods.setStockCount(seckillGoods.getStockCount()+1);
				redisTemplate.boundHashOps("seckillGoods2").put(seckillOrder.getSeckillId(), seckillGoods);
			}else{
				seckillGoods=new TbSeckillGoods();
				seckillGoods.setId(seckillOrder.getSeckillId());
				//属性要设置。。。。省略
				seckillGoods.setStockCount(1);//数量为1
				redisTemplate.boundHashOps("seckillGoods2").put(seckillOrder.getSeckillId(), seckillGoods);
			}
			System.out.println("订单取消："+orderId);
		}
	}
	
	
	// 根据orderId 获取订单项
	@Override
	public TbSeckillGoods findItem(Long orderId) {
		TbSeckillOrder seckillOrder = seckillOrderMapper.selectByPrimaryKey(orderId);
		TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(seckillOrder.getSeckillId());
		return seckillGoods;
	}
	
	//更新状态
	@Override
	public void updateStatus(Long orderId, String status) {
		TbSeckillOrder seckillOrder = seckillOrderMapper.selectByPrimaryKey(orderId);
		seckillOrder.setStatus(status);
		seckillOrderMapper.updateByPrimaryKeySelective(seckillOrder);
	}
}





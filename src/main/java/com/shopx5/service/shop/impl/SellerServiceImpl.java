package com.shopx5.service.shop.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SpRoleUserMapper;
import com.shopx5.mapper.SpUserInfoMapper;
import com.shopx5.mapper.SpUserMapper;
import com.shopx5.mapper.TbSellerMapper;
import com.shopx5.pojo.SpRoleUser;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.TbSeller;
import com.shopx5.pojo.TbSellerExample;
import com.shopx5.pojo.TbSellerExample.Criteria;
import com.shopx5.service.shop.SellerService;
import com.shopx5.utils.Encrypt;
import com.shopx5.utils.IDUtils;

/**
 * Seller服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class SellerServiceImpl implements SellerService {
	@Autowired
	private TbSellerMapper sellerMapper;
	@Autowired
	private SpUserMapper userMapper;
	@Autowired
	private SpUserInfoMapper userInfoMapper;
	@Autowired
	private SpRoleUserMapper roleUserMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbSeller> findAll() {
		return sellerMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbSeller> findByParentId(Long parentId) {
		TbSellerExample example = new TbSellerExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return sellerMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbSeller> page = (Page<TbSeller>) sellerMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param seller, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbSeller seller, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbSellerExample example = new TbSellerExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(seller != null){			
			if(seller.getName()!=null && seller.getName().length()>0){
				criteria.andNameLike("%"+seller.getName()+"%");
			}
			if(seller.getNickName()!=null && seller.getNickName().length()>0){
				criteria.andNickNameLike("%"+seller.getNickName()+"%");
			}
			if(seller.getStatus()!=null && seller.getStatus().length()>0){
				criteria.andStatusLike("%"+seller.getStatus()+"%");
			}
		}
		// PageHelper第二种方式
		Page<TbSeller> page = (Page<TbSeller>) sellerMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加    #注册
	 */
	@Override
	public void add(TbSeller seller) {
		//保存user
		SpUser spUser = new SpUser();
		String sellerId = seller.getSellerId();
		String nickName = seller.getNickName();
		String userId = IDUtils.get32UUID();
		spUser.setUserId(userId);
		spUser.setSellerId(seller.getSellerId()); //sellerId
		spUser.setNickName(nickName); 			  //公司
		spUser.setName(seller.getName());         //店铺
		//进行shiromd5加密
		spUser.setUserName(sellerId);
		String md5 = Encrypt.md5(seller.getPassword(), sellerId);
		spUser.setPassword(md5);
		spUser.setState(1);
		userMapper.insertSelective(spUser);
		//保存userInfo
		SpUserInfo spUserInfo = new SpUserInfo();
		spUserInfo.setUserInfoId(userId);
		spUserInfo.setName(nickName);
		spUserInfo.setEmail(seller.getEmail());
		userInfoMapper.insertSelective(spUserInfo);
		//保存userRole
		SpRoleUser spRoleUser = new SpRoleUser();
		spRoleUser.setUserId(userId);
		spRoleUser.setRoleId("9f4295bde12842fda243f55cf24eaa7e");
		roleUserMapper.insert(spRoleUser);
		//保存seller
		seller.setStatus("0");
		seller.setPassword(null);
		sellerMapper.insert(seller);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbSeller findOne(String id) {
		return sellerMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbSeller seller) {
		sellerMapper.updateByPrimaryKey(seller);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			sellerMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
	//更新状态
	@Override
	public void updateStatus(String sellerId, String status) {
		TbSeller seller = sellerMapper.selectByPrimaryKey(sellerId);
		seller.setStatus(status);
		sellerMapper.updateByPrimaryKey(seller);
	}
	
	//校验数据
	@Override
	public String checkData(String param, String name) {
		TbSellerExample example = new TbSellerExample();
		Criteria criteria = example.createCriteria();
		// 设置参数
		if(name.equals("nickName")){
			criteria.andNickNameEqualTo(param);
		}
		List<TbSeller> list = sellerMapper.selectByExample(example);
		if(list!=null && list.size()>0){
			return "false";
		}
		return "true";
	}
}

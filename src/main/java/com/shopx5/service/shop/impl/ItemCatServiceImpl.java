package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsItemCatMapper;
import com.shopx5.pojo.SsItemCat;
import com.shopx5.pojo.SsItemCatExample;
import com.shopx5.pojo.SsItemCatExample.Criteria;
import com.shopx5.service.shop.ItemCatService;

/**
 * ItemCat服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class ItemCatServiceImpl implements ItemCatService {
	@Autowired
	private SsItemCatMapper itemCatMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<SsItemCat> findAll() {
		return itemCatMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<SsItemCat> findByParentId(Long parentId) {
		SsItemCatExample example = new SsItemCatExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		criteria.andParentIdEqualTo(parentId);
		return itemCatMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<SsItemCat> page = (Page<SsItemCat>) itemCatMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param itemCat, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(SsItemCat itemCat, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		SsItemCatExample example = new SsItemCatExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(itemCat != null){			
			/*if(itemCat.getName()!=null && itemCat.getName().length()>0){
				criteria.andNameLike("%"+itemCat.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<SsItemCat> page = (Page<SsItemCat>) itemCatMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(SsItemCat itemCat) {
		itemCatMapper.insert(itemCat);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SsItemCat findOne(Long id) {
		return itemCatMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(SsItemCat itemCat) {
		itemCatMapper.updateByPrimaryKey(itemCat);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			itemCatMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	//缓存分类
	@Override
	public void cacheCat(){
		redisTemplate.delete("itemCat");
		
		// 查询所有分类   以分类名为key: 将模板ID放入缓存
		System.out.println("======solr1: 'itemCat'模版ID存入 开始======");
		long s1 = System.currentTimeMillis();
		
		List<SsItemCat> itemCatList = findAll();
		for(SsItemCat itemCat : itemCatList){       //[{"音乐":35}]
			redisTemplate.boundHashOps("itemCat").put(itemCat.getName(), itemCat.getTypeId());
		}
		
		System.out.println("======solr1: 'itemCat'模版ID存入 结束======");
		long s2 = System.currentTimeMillis();
		double s = (double)(s2-s1)/1000;
		System.out.println("花费时间: "+s+"秒");
	};
}

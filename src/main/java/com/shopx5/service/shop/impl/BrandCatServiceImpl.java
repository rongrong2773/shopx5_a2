package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsBrandCatMapper;
import com.shopx5.pojo.SsBrandCat;
import com.shopx5.pojo.SsBrandCatExample;
import com.shopx5.pojo.SsBrandCatExample.Criteria;
import com.shopx5.service.shop.BrandCatService;

/**
 * BrandCat服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class BrandCatServiceImpl implements BrandCatService {
	@Autowired
	private SsBrandCatMapper brandCatMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<SsBrandCat> findAll() {
		return brandCatMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<SsBrandCat> findByParentId(Long parentId) {
		SsBrandCatExample example = new SsBrandCatExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return brandCatMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<SsBrandCat> page = (Page<SsBrandCat>) brandCatMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param brandCat, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(SsBrandCat brandCat, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		SsBrandCatExample example = new SsBrandCatExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(brandCat != null){			
			/*if(brandCat.getName()!=null && brandCat.getName().length()>0){
				criteria.andNameLike("%"+brandCat.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<SsBrandCat> page = (Page<SsBrandCat>) brandCatMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(SsBrandCat brandCat) {
		brandCatMapper.insert(brandCat);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SsBrandCat findOne(Long id) {
		return brandCatMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(SsBrandCat brandCat) {
		brandCatMapper.updateByPrimaryKey(brandCat);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			brandCatMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	@Override
	public List<Map> selectOptionList() {
		return brandCatMapper.selectOptionList();
	}
	
}

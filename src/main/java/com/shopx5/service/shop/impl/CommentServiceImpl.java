package com.shopx5.service.shop.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbCommentMapper;
import com.shopx5.mapper.TbCommentReplyMapper;
import com.shopx5.pojo.TbComment;
import com.shopx5.pojo.TbCommentExample;
import com.shopx5.pojo.TbCommentExample.Criteria;
import com.shopx5.pojo.TbCommentReply;
import com.shopx5.pojo.TbCommentReplyExample;
import com.shopx5.pojo.group.Comment;
import com.shopx5.service.shop.CommentService;

/**
 * Comment服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	private TbCommentMapper commentMapper;
	@Autowired
	private TbCommentReplyMapper commentReplyMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbComment> findAll() {
		return commentMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbComment> findByParentId(Long parentId) {
		TbCommentExample example = new TbCommentExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return commentMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbComment> page = (Page<TbComment>) commentMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param comment, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbComment comment, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbCommentExample example = new TbCommentExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(comment != null){			
			/*if(comment.getName()!=null && comment.getName().length()>0){
				criteria.andNameLike("%"+comment.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbComment> page = (Page<TbComment>) commentMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbComment comment) {
		commentMapper.insert(comment);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbComment findOne(Long id) {
		return commentMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbComment comment) {
		commentMapper.updateByPrimaryKey(comment);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			commentMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	//                    有序:回复时间
	// [{userId,content, +reply[{userId,content,created}, ]}  ,  ]
	// 获取SPU评论,数据列表  根据goodsId
	@Override
	public List<Comment> findByGoodsId(Long goodsId) {		
		// 1.获取评论
		TbCommentExample example = new TbCommentExample();
		Criteria criteria = example.createCriteria();
		criteria.andGoodsIdEqualTo(goodsId);
		criteria.andStatusEqualTo("1");
		List<TbComment> list = commentMapper.selectByExample(example);
		
		List<Comment> listAll = new ArrayList<>();
		// 2.遍历评论获取回复
		for (TbComment comment : list) {
			TbCommentReplyExample exampleR = new TbCommentReplyExample();
			com.shopx5.pojo.TbCommentReplyExample.Criteria criteriaR = exampleR.createCriteria();
			criteriaR.andGoodsIdEqualTo(goodsId);
			criteriaR.andUserIdEqualTo(comment.getUserId());
			exampleR.setOrderByClause("created ASC");
			List<TbCommentReply> listR = commentReplyMapper.selectByExample(exampleR);
			// 封装
			Comment commentNew = new Comment(comment);
			if (listR!=null && listR.size()>0) {
				commentNew.setReply(listR);
			}
			listAll.add(commentNew);
		}
		return listAll;
	}
}





package com.shopx5.service.shop.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SxViewMapper;
import com.shopx5.pojo.SxView;
import com.shopx5.pojo.SxViewExample;
import com.shopx5.pojo.SxViewExample.Criteria;
import com.shopx5.pojo.group.Good;
import com.shopx5.service.shop.GoodsService;
import com.shopx5.service.shop.ViewService;
/**
 * View服务实现层
 * @author ShopX5 tang
 */
@Service
public class ViewServiceImpl implements ViewService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SxViewMapper sxViewMapper;
	@Autowired
	private GoodsService goodsService;
	/**
	 * 查询全部
	 */
	@Override
	public List<SxView> findAll(String userId) {
		// 取所有
		SxViewExample example = new SxViewExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC"); SH-OP-X-5
		return sxViewMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<Good> findPage(int page, String userId) {
		PageBean<Good> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SxView> list = this.findAll(userId);
		// 取查询结果
		PageInfo<SxView> pageInfo = new PageInfo<>(list);
		// 进一步处理
		List<Good> goodsList = new ArrayList<>();
		for (SxView view : list) {
			Good goods = goodsService.getGood(view.getItemId());
			if (goods != null) {
				goods.setCollectionId(view.getId());
				goodsList.add(goods);
			}
		}
		// 设置数据
		pageBean.setList(goodsList);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SxView sxView) {
		sxViewMapper.insert(sxView);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SxView sxView) {
		sxViewMapper.updateByPrimaryKey(sxView);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SxView findOne(Long id) {
		return sxViewMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Long id) {
		sxViewMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			this.delete(id);
		}
	}
	
}



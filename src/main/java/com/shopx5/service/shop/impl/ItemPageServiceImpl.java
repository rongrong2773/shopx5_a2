package com.shopx5.service.shop.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.alibaba.fastjson.JSON;
import com.shopx5.common.ItemImages;
import com.shopx5.mapper.SsItemCatMapper;
import com.shopx5.mapper.TbGoodsDescMapper;
import com.shopx5.mapper.TbGoodsMapper;
import com.shopx5.mapper.TbItemMapper;
import com.shopx5.mapper.TbSellerMapper;
import com.shopx5.pojo.TbGoods;
import com.shopx5.pojo.TbGoodsDesc;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.TbItemExample;
import com.shopx5.pojo.TbItemExample.Criteria;
import com.shopx5.pojo.TbSeller;
import com.shopx5.service.shop.ItemPageService;
import com.shopx5.utils.JsonUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Service
public class ItemPageServiceImpl implements ItemPageService {
	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;
	@Value("${pagedir}")
	private String pagedir;
	@Value("${pagedir2}")
	private String pagedir2;
	@Autowired
	private TbGoodsMapper goodsMapper;
	@Autowired
	private TbGoodsDescMapper goodsDescMapper;
	@Autowired
	private SsItemCatMapper itemCatMapper;
	@Autowired
	private TbItemMapper itemMapper;
	@Autowired
	private TbSellerMapper sellerMapper;
	/**
	 * 生成静态页
	 */
	@Override
	public boolean genItemHtml(Long goodsId) {
		Configuration configuration = freeMarkerConfigurer.getConfiguration();
		try {
			Template template = configuration.getTemplate("item.ftl");
			// 创建数据模型
			Map dataModel = new HashMap<>();
			// 1.商品主表数据
			TbGoods goods = goodsMapper.selectByPrimaryKey(goodsId);
			dataModel.put("goods", goods);
			// 2.获取商户店铺名
			TbSeller seller = sellerMapper.selectByPrimaryKey(goods.getSellerId());
			dataModel.put("seller", seller);
			// 3.商品扩展表数据
			TbGoodsDesc goodsDesc = goodsDescMapper.selectByPrimaryKey(goodsId);
			dataModel.put("goodsDesc", goodsDesc);
			// 4.读取商品分类
			String itemCat1 = itemCatMapper.selectByPrimaryKey(goods.getCategory1Id()).getName();
			String itemCat2 = itemCatMapper.selectByPrimaryKey(goods.getCategory2Id()).getName();
			String itemCat3 = itemCatMapper.selectByPrimaryKey(goods.getCategory3Id()).getName();
			dataModel.put("itemCat1", itemCat1);
			dataModel.put("itemCat2", itemCat2);
			dataModel.put("itemCat3", itemCat3);
			// 5.读取SKU列表
			TbItemExample example = new TbItemExample();
			Criteria criteria = example.createCriteria();
			criteria.andGoodsIdEqualTo(goodsId); // SPU的ID
			criteria.andStatusEqualTo("1");      // 状态有效
			example.setOrderByClause("is_default desc");// 默认字段_降序,目的:返回结果第一条为默认SKU
			List<TbItem> itemList = itemMapper.selectByExample(example);
			dataModel.put("itemList", itemList);
			// 6.1 SPU_ID
			dataModel.put("goodsId", String.valueOf(goodsId));
			// 6.2 商品轮播图
			//取商品介绍图大图 封装    [{"_small":"_60.jpg","_mid":"_360.jpg","_big":"_1280.jpg"},..]
			List<ItemImages> list = new ArrayList<>();
			List<Map> imgsList = new ArrayList<>();
			if (goodsDesc != null) {
				String itemImages = goodsDesc.getItemImages();
				List<Map> array = JSON.parseArray(itemImages, Map.class);
				for (Map map : array) {
					String color = (String) map.get("color");
					if (color.contains("大图")) {
						ItemImages image = new ItemImages();
						image.set_small((String)map.get("url"));
						image.set_mid((String)map.get("url"));
						image.set_big((String)map.get("url"));
						list.add(image);   //封装大中小图JSON
						imgsList.add(map); //封装大图
					}
				}
			}
			String imagesList = JsonUtils.objectToJson(list);
			dataModel.put("imagesList", imagesList);
			dataModel.put("imgsList", imgsList);
			Writer out = new FileWriter(pagedir+goodsId+".htm");
			template.process(dataModel, out);// 输出
			out.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除静态页
	 */
	@Override
	public boolean deleteItemHtml(Long[] goodsIds) {
		try {
			for (Long goodsId : goodsIds) {
				new File(pagedir2 + goodsId + ".htm").delete();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}

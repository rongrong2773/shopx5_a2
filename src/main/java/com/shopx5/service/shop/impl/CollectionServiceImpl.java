package com.shopx5.service.shop.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SxCollectionMapper;
import com.shopx5.pojo.SxCollection;
import com.shopx5.pojo.SxCollectionExample;
import com.shopx5.pojo.SxCollectionExample.Criteria;
import com.shopx5.pojo.group.Good;
import com.shopx5.service.shop.CollectionService;
import com.shopx5.service.shop.GoodsService;
/**
 * Collection服务实现层
 * @author ShopX5 tang
 */
@Service
public class CollectionServiceImpl implements CollectionService {
	@Value("${PAGE_SIZE_COLL}")
	private String PAGE_SIZE_SCROLL;
	@Autowired
	private SxCollectionMapper sxCollectionMapper;
	@Autowired
	private GoodsService goodsService;
	/**
	 * 返回全部列表
	 */
	@Override
	public List<SxCollection> findAll(String userId) {
		// 取所有
		SxCollectionExample example = new SxCollectionExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return sxCollectionMapper.selectByExample(example);
	}
	/**
	 * 返回分页列表
	 */
	@Override
	public PageBean<Good> findPage(int page, String userId) {
		PageBean<Good> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE_SCROLL));
		// 取所有
		List<SxCollection> list = this.findAll(userId);
		// 取查询结果
		PageInfo<SxCollection> pageInfo = new PageInfo<>(list);
		// 进一步处理
		List<Good> goodsList = new ArrayList<>();
		for (SxCollection collection : list) {
			Good goods = goodsService.getGood(collection.getItemId());
			if (goods != null) {
				goods.setCollectionId(collection.getId());
				goodsList.add(goods);
			}
		}
		// 设置数据
		pageBean.setList(goodsList);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SxCollection sxCollection) {
		sxCollectionMapper.insert(sxCollection);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SxCollection sxCollection) {
		sxCollectionMapper.updateByPrimaryKey(sxCollection);
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SxCollection getOne(Long id) {
		return sxCollectionMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Long id) {
		sxCollectionMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			this.delete(id);
		}
	}
	
}



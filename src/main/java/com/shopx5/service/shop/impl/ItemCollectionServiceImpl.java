package com.shopx5.service.shop.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopx5.common.Result;
import com.shopx5.mapper.SxCollectionMapper;
import com.shopx5.mapper.result.GoodMapper;
import com.shopx5.pojo.SxCollection;
import com.shopx5.pojo.SxCollectionExample;
import com.shopx5.pojo.SxCollectionExample.Criteria;
import com.shopx5.service.shop.ItemCollectionService;

@Service
public class ItemCollectionServiceImpl implements ItemCollectionService {
	@Autowired
	private SxCollectionMapper collectionMapper;
	@Autowired
	private GoodMapper goodMapper;
	
	@Override
	public Result indexCollect(String goodsId, String userId) {
		//1 是否已收藏
		SxCollectionExample example = this.getExample(userId, goodsId);
		List<SxCollection> list = collectionMapper.selectByExample(example);
		if (list == null || list.size() < 1) {
			return Result.build(400, "未收藏!");
		}
		//2 被收藏次数
		SxCollectionExample example2 = this.getExample(null, goodsId);
		List<SxCollection> list2 = collectionMapper.selectByExample(example2);
		return Result.ok(list2.size());
	}

	@Override
	public Result addCollect(SxCollection collection, String goodsId) {
		//添加收藏
		if (goodsId != null) {
			collection.setItemId(Long.valueOf(goodsId));
			collectionMapper.insert(collection);
		}else {
			collectionMapper.insert(collection);
		}
		//获取收藏次数
		SxCollectionExample example = this.getExample(null, String.valueOf(collection.getItemId()));
		List<SxCollection> list = collectionMapper.selectByExample(example);
		return Result.ok(list.size());
	}

	@Override
	public Result deleteCollect(String goodsId, String userId) {
		//删除收藏+更新收藏次数 SH@OP@X@5
		SxCollectionExample example = this.getExample(userId, goodsId);
		collectionMapper.deleteByExample(example);
		//更新收藏次数
		SxCollectionExample example2 = this.getExample(null, goodsId);
		List<SxCollection> list = collectionMapper.selectByExample(example2);
		return Result.ok(list.size());
	}
	
	//example封装方法
	public SxCollectionExample getExample(String userId, String itemId){
		SxCollectionExample example = new SxCollectionExample();
		Criteria criteria = example.createCriteria();
		if (StringUtils.isNotBlank(userId))	criteria.andUserIdEqualTo(userId);
		if (StringUtils.isNotBlank(itemId))	criteria.andItemIdEqualTo(Long.valueOf(itemId));
		return example;
	}
}



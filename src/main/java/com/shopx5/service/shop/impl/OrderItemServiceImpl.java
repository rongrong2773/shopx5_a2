package com.shopx5.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbOrderItemMapper;
import com.shopx5.pojo.TbOrderItem;
import com.shopx5.pojo.TbOrderItemExample;
import com.shopx5.pojo.TbOrderItemExample.Criteria;
import com.shopx5.service.shop.OrderItemService;

/**
 * OrderItem服务层实现
 * @author tang shopx5 多商户 商城 系统
 * @qq 1503501970
 */
@Service
public class OrderItemServiceImpl implements OrderItemService {
	@Autowired
	private TbOrderItemMapper orderItemMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbOrderItem> findAll() {
		return orderItemMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbOrderItem> findByParentId(Long parentId) {
		TbOrderItemExample example = new TbOrderItemExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return orderItemMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbOrderItem> page = (Page<TbOrderItem>) orderItemMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param orderItem, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbOrderItem orderItem, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbOrderItemExample example = new TbOrderItemExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(orderItem != null){			
			/*if(orderItem.getName()!=null && orderItem.getName().length()>0){
				criteria.andNameLike("%"+orderItem.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbOrderItem> page = (Page<TbOrderItem>) orderItemMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbOrderItem orderItem) {
		orderItemMapper.insert(orderItem);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbOrderItem findOne(Long id) {
		return orderItemMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbOrderItem orderItem) {
		orderItemMapper.updateByPrimaryKey(orderItem);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			orderItemMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
}

package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbPayLogMapper;
import com.shopx5.pojo.TbPayLog;
import com.shopx5.pojo.TbPayLogExample;
import com.shopx5.pojo.TbPayLogExample.Criteria;
import com.shopx5.service.shop.PayLogService;

/**
 * PayLog服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class PayLogServiceImpl implements PayLogService {
	@Autowired
	private TbPayLogMapper payLogMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbPayLog> findAll() {
		return payLogMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbPayLog> findByParentId(Long parentId) {
		TbPayLogExample example = new TbPayLogExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return payLogMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbPayLog> page = (Page<TbPayLog>) payLogMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param payLog, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbPayLog payLog, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbPayLogExample example = new TbPayLogExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(payLog != null){			
			/*if(payLog.getName()!=null && payLog.getName().length()>0){
				criteria.andNameLike("%"+payLog.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbPayLog> page = (Page<TbPayLog>) payLogMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbPayLog payLog) {
		payLogMapper.insert(payLog);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbPayLog findOne(Long id) {
		return payLogMapper.selectByPrimaryKey(String.valueOf(id));
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbPayLog payLog) {
		payLogMapper.updateByPrimaryKey(payLog);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			payLogMapper.deleteByPrimaryKey(String.valueOf(id));
		}
	}
	
	/**
	 * 自定义
	 */
	
}

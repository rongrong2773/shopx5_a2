package com.shopx5.service.shop.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsBrandMapper;
import com.shopx5.mapper.SsItemCatMapper;
import com.shopx5.mapper.TbGoodsDescMapper;
import com.shopx5.mapper.TbGoodsMapper;
import com.shopx5.mapper.TbItemMapper;
import com.shopx5.mapper.TbSellerMapper;
import com.shopx5.mapper.result.GoodMapper;
import com.shopx5.pojo.SsBrand;
import com.shopx5.pojo.SsItemCat;
import com.shopx5.pojo.TbGoods;
import com.shopx5.pojo.TbGoodsDesc;
import com.shopx5.pojo.TbGoodsExample;
import com.shopx5.pojo.TbGoodsExample.Criteria;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.TbItemExample;
import com.shopx5.pojo.TbSeller;
import com.shopx5.pojo.group.Good;
import com.shopx5.pojo.group.Goods;
import com.shopx5.service.shop.GoodsService;

/**
 * Goods服务层实现
 * @author tang shopx5 多商户 商城 系统
 * @qq 1503501970
 */
@Service
public class GoodsServiceImpl implements GoodsService {
	@Autowired
	private TbGoodsMapper goodsMapper;
	@Autowired
	private GoodMapper goodMapper;
	@Autowired
	private TbGoodsDescMapper goodsDescMapper;
	@Autowired
	private TbItemMapper itemMapper;
	@Autowired
	private SsItemCatMapper itemCatMapper;
	@Autowired
	private SsBrandMapper brandMapper;
	@Autowired
	private TbSellerMapper sellerMapper;
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbGoods> findAll() {
		return goodsMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbGoods> findByParentId(Long parentId) {
		TbGoodsExample example = new TbGoodsExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return goodsMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbGoods> page = (Page<TbGoods>) goodsMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param goods, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbGoods goods, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbGoodsExample example = new TbGoodsExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(goods != null){
			if(goods.getGoodsName()!=null && goods.getGoodsName().length()>0){
				criteria.andGoodsNameLike("%"+goods.getGoodsName()+"%");
			}
			if(goods.getAuditStatus()!=null && goods.getAuditStatus().length()>0){
				criteria.andAuditStatusLike("%"+goods.getAuditStatus()+"%");
			}
			if(goods.getSellerId()!=null && goods.getSellerId().length()>0){
				criteria.andSellerIdLike("%"+goods.getSellerId()+"%");
			}
		}
		// PageHelper第二种方式
		Page<TbGoods> page = (Page<TbGoods>) goodsMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	

	/**
	 * 增加
	 */
	@Override
	public void add(Goods goods) {
		//保存商品信息 
		goods.getGoods().setAuditStatus("0"); // 设置审核的状态
		goods.getGoods().setCreateTime(new Date());
		goods.getGoods().setUpdateTime(new Date());
		//设置为第一个图片url
		List<Map> list = JSON.parseArray(goods.getGoodsDesc().getItemImages(), Map.class);
		if(list.size()>0){
			goods.getGoods().setSmallPic((String)list.get(0).get("url"));
		}
		goodsMapper.insert(goods.getGoods());
		//保存商品扩展信息
		goods.getGoodsDesc().setGoodsId(goods.getGoods().getId()); // 设置商品id
		goodsDescMapper.insert(goods.getGoodsDesc());
		//是否启用规格,保存SKU信息
		setItemList(goods);
	}
	
	// 保存SKU信息
	// "itemList":[{"spec":{"网络":"移动4G","机身内存":"128G"},"price":9.99,"num":999,"status":"1","isDefault":"0"},]
	// 设置ItemList下item的title
	private void setItemList(Goods goods){
		if("1".equals(goods.getGoods().getIsEnableSpec())){
			// 启用规格 保存SKU列表信息
			for(TbItem item : goods.getItemList()){
				// 设置SKU数据
				String title = goods.getGoods().getGoodsName();
				Map<String, String> map = JSON.parseObject(item.getSpec(), Map.class);
				for (String key : map.keySet()) {
					title += " "+map.get(key);
				}
				item.setTitle(title);
				setValue(goods,item); //填充item扩展属性后,保存
				itemMapper.insert(item);
			}
		}else{
			// 没有启用规格 设置初始数据保存
			TbItem item = new TbItem();
			item.setTitle(goods.getGoods().getGoodsName());
			item.setPrice(goods.getGoods().getPrice());
			item.setNum(999);
			item.setStatus("0");
			item.setIsDefault("1");
			item.setSpec("{}");
			setValue(goods,item); //填充item扩展属性后,保存
			itemMapper.insert(item);
		}
	}

	// 设置SKU相关属性
	private void setValue(Goods goods,TbItem item){
		// 设置为第一个图片url  扩展为颜色图
		item.setImage(goods.getGoods().getSmallPic());
		// 保存三级分类的ID:
		item.setCategoryid(goods.getGoods().getCategory3Id());
		// 设置商品ID
		item.setGoodsId(goods.getGoods().getId());
		item.setSellerId(goods.getGoods().getSellerId());
		// 设置分类
		SsItemCat itemCat = itemCatMapper.selectByPrimaryKey(goods.getGoods().getCategory3Id());
		item.setCategory(itemCat.getName());
		// 设置品牌
		SsBrand brand = brandMapper.selectByPrimaryKey(goods.getGoods().getBrandId());
		item.setBrand(brand.getName());
		// 设置商家店铺名称
		TbSeller seller = sellerMapper.selectByPrimaryKey(goods.getGoods().getSellerId());
		item.setSeller(seller.getNickName());
		// 设置时间
		item.setCreateTime(new Date());
		item.setUpdateTime(new Date());
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public Goods findOne(Long id) {
		Goods goods = new Goods();
		// 查询商品表的信息
		TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
		goods.setGoods(tbGoods);
		// 查询商品扩展表的信息
		TbGoodsDesc tbGoodsDesc = goodsDescMapper.selectByPrimaryKey(id);
		goods.setGoodsDesc(tbGoodsDesc);
		
		// 查询SKU表的信息:
		TbItemExample example = new TbItemExample();
		com.shopx5.pojo.TbItemExample.Criteria criteria = example.createCriteria();
		criteria.andGoodsIdEqualTo(id);
		List<TbItem> list = itemMapper.selectByExample(example);
		goods.setItemList(list);
		
		return goods;
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(Goods goods) {
		// 修改商品信息
		goods.getGoods().setAuditStatus("0");
		goodsMapper.updateByPrimaryKey(goods.getGoods());
		// 修改商品扩展信息:
		goodsDescMapper.updateByPrimaryKey(goods.getGoodsDesc());
		// 修改SKU信息: 先删除，再保存
		TbItemExample example = new TbItemExample();
		com.shopx5.pojo.TbItemExample.Criteria criteria = example.createCriteria();
		criteria.andGoodsIdEqualTo(goods.getGoods().getId());
		itemMapper.deleteByExample(example);
		// 保存SKU的信息
		setItemList(goods);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			//goodsMapper.deleteByPrimaryKey(id);
			TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
			tbGoods.setIsDelete("1"); //逻辑删除
			goodsMapper.updateByPrimaryKey(tbGoods);
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 商品审核: 运营商审核商家已发布商品
	@Override
	public void updateStatus(Long[] ids, String status) {
		for (Long id : ids) {
			TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
			tbGoods.setAuditStatus(status);
			goodsMapper.updateByPrimaryKey(tbGoods);
		}
	}
	// 上下架: 状态更新
	@Override
	public void updateMarketable(Long[] ids, String status) {
		for (Long id : ids) {
			TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
			tbGoods.setIsMarketable(status);
			goodsMapper.updateByPrimaryKey(tbGoods);
		}
	}
		
	/**
	 * 根据SPU_ID集合查询SKU列表   审核商品且状态为1导入到索引库
	 * @param goodsIds
	 * @param status
	 * @return
	 */
	@Override
	public List<TbItem>	findItemListByGoodsIdListAndStatus(Long[] goodsIds, String status){
		TbItemExample example = new TbItemExample();
		com.shopx5.pojo.TbItemExample.Criteria criteria = example.createCriteria();
		criteria.andStatusEqualTo(status); //状态
		criteria.andGoodsIdIn(Arrays.asList(goodsIds)); //指定条件：SPU的id集合
		return itemMapper.selectByExample(example);
	}

	/**
	 * 获取用户+详情
	 */
	@Override
	public Good getGoodAndInfo(Long goodsId) {
		return goodMapper.getGoodAndInfo(goodsId);
	}
	
	/**
	 * 获取用户+详情 并处理image
	 */
	@Override
	public Good getGood(Long goodsId) {
		Good good = this.getGoodAndInfo(goodsId);
		if(good == null) return null;
		String images = good.getGoodInfo().getItemImages();
		if(StringUtils.isBlank(images)) return null;
		List<Map> list = JSON.parseArray(images, Map.class);
		//默认设置第一张图
		String url = (String) list.get(0).get("url");
		good.getGoodInfo().setItemImages(url);
		/*for (Map map : list) {
			String color = (String) map.get("color");
			if (color.contains("默认")) {
				String url = (String) map.get("url");
				good.getGoodInfo().setItemImages(url);
				break;
			}
		}*/
		return good;
	}
}



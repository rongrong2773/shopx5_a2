package com.shopx5.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.SpUserAddrMapper;
import com.shopx5.pojo.SpUserAddr;
import com.shopx5.pojo.SpUserAddrExample;
import com.shopx5.pojo.SpUserAddrExample.Criteria;
import com.shopx5.service.shop.UserAddrService;
/**
 * UserAddr服务实现层
 * @author ShopX5 tang
 */
@Service
public class UserAddrServiceImpl implements UserAddrService {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private SpUserAddrMapper spUserAddrMapper;
	
	//1 添加用户地址
	@Override
	public Integer addUserAddr(SpUserAddr spUserAddr) {
		return spUserAddrMapper.insert(spUserAddr);
	}
	//2 获取当前用户  默认地址1个
	@Override
	public SpUserAddr getUserAddrByUserId(String userId, Integer i) {
		SpUserAddrExample example = new SpUserAddrExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		if (i != null) {
			criteria.andDfEqualTo(1); //默认地址 1
		}
		List<SpUserAddr> list = spUserAddrMapper.selectByExample(example);
		if(list == null || list.size()==0) return null;
		return list.get(0);
	}
	//3 获取当前用户 所有地址
	@Override
	public List<SpUserAddr> findUserAddrByUserId(String userId) {
		SpUserAddrExample example = new SpUserAddrExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		List<SpUserAddr> list = spUserAddrMapper.selectByExample(example);
		return list;
	}
	//4 获取当前用户  默认地址1个
	@Override
	public SpUserAddr getUserAddrByAddrId(String addrId) {
		return spUserAddrMapper.selectByPrimaryKey(addrId);
	}
	
	/**
	 * 查询全部
	 */
	@Override
	public List<SpUserAddr> findAll() {
		// 取所有
		SpUserAddrExample example = new SpUserAddrExample();
		// Criteria criteria = example.createCriteria();
		// criteria.andStateEqualTo(1);
		// example.setOrderByClause("CREATE_TIME DESC");
		return spUserAddrMapper.selectByExample(example);
	}
	/**
	 * 查询Page
	 */
	@Override
	public PageBean<SpUserAddr> findPage(int page) {
		PageBean<SpUserAddr> pageBean = new PageBean<>();
		// 设置分页信息 页码,数量
		PageHelper.startPage(page, Integer.parseInt(PAGE_SIZE));
		// 取所有
		List<SpUserAddr> list = this.findAll();
		// 取查询结果
		PageInfo<SpUserAddr> pageInfo = new PageInfo<>(list);
		// 进一步处理
		
		// 设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	/**
	 * 增加
	 */
	@Override
	public void add(SpUserAddr spUserAddr, String userId) {
		if (spUserAddr.getDf() != null && spUserAddr.getDf() == 1) {
			this.updateDF(userId);
		}
		spUserAddrMapper.insert(spUserAddr);
	}
	/**
	 * 修改
	 */
	@Override
	public void update(SpUserAddr spUserAddr, String userId) {
		if (spUserAddr.getDf() != null && spUserAddr.getDf() == 1) {
			this.updateDF(userId);
		}
		spUserAddrMapper.updateByPrimaryKey(spUserAddr);
	}
	/**
	 * 默认地址已有 删除
	 */
	public void updateDF(String userId) {
		SpUserAddr addr = getUserAddrByUserId(userId, 1);
		if(addr != null){
			addr.setDf(0);
			spUserAddrMapper.updateByPrimaryKey(addr);
		}
	}
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SpUserAddr getOne(String id) {
		return spUserAddrMapper.selectByPrimaryKey(id);
	}
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id) {
		spUserAddrMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 批量删除
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			this.delete(id);
		}
	}
	
	// 获取登录用户 地址列表
	@Override
	public List<SpUserAddr> findListByUserId(String userId) {
		SpUserAddrExample example = new SpUserAddrExample();
		 Criteria criteria = example.createCriteria();
		 criteria.andUserIdEqualTo(userId);
		return spUserAddrMapper.selectByExample(example);
	}
	
}



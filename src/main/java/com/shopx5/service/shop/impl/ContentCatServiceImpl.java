package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbContentCatMapper;
import com.shopx5.pojo.TbContentCat;
import com.shopx5.pojo.TbContentCatExample;
import com.shopx5.pojo.TbContentCatExample.Criteria;
import com.shopx5.service.shop.ContentCatService;

/**
 * ContentCat服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class ContentCatServiceImpl implements ContentCatService {
	@Autowired
	private TbContentCatMapper contentCatMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbContentCat> findAll() {
		return contentCatMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbContentCat> findByParentId(Long parentId) {
		TbContentCatExample example = new TbContentCatExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return contentCatMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbContentCat> page = (Page<TbContentCat>) contentCatMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param contentCat, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbContentCat contentCat, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbContentCatExample example = new TbContentCatExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(contentCat != null){			
			/*if(contentCat.getName()!=null && contentCat.getName().length()>0){
				criteria.andNameLike("%"+contentCat.getName()+"%");
			}*/
		}
		// PageHelper第二种方式
		Page<TbContentCat> page = (Page<TbContentCat>) contentCatMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbContentCat contentCat) {
		contentCatMapper.insert(contentCat);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbContentCat findOne(Long id) {
		return contentCatMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbContentCat contentCat) {
		contentCatMapper.updateByPrimaryKey(contentCat);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			contentCatMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	
}

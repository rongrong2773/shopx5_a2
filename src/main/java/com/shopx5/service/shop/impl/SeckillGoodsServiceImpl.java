package com.shopx5.service.shop.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.TbSeckillGoodsMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbSeckillGoods;
import com.shopx5.pojo.TbSeckillGoodsExample;
import com.shopx5.pojo.TbSeckillGoodsExample.Criteria;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.SeckillGoodsService;

/**
 * SeckillGoods服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class SeckillGoodsServiceImpl implements SeckillGoodsService {
	@Autowired
	private TbSeckillGoodsMapper seckillGoodsMapper;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private IndexService indexService;
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<TbSeckillGoods> findAll() {
		return seckillGoodsMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<TbSeckillGoods> findByParentId(Long parentId) {
		TbSeckillGoodsExample example = new TbSeckillGoodsExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return seckillGoodsMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<TbSeckillGoods> page = (Page<TbSeckillGoods>) seckillGoodsMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param seckillGoods, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(TbSeckillGoods seckillGoods, Integer pageNum, Integer pageSize, Integer grade) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		TbSeckillGoodsExample example = new TbSeckillGoodsExample();
		Criteria criteria = example.createCriteria();
		// 设置级别: 1运营 2商户 3用户
		SpUser user = indexService.getUser(request);
		if(user!=null && grade==2){
			criteria.andSellerIdEqualTo(user.getSellerId());
		}
		// 设置条件:
		if(seckillGoods != null){
			if(seckillGoods.getStatus()!=null && seckillGoods.getStatus().length()>0){
				criteria.andStatusEqualTo(seckillGoods.getStatus());
			}
			if(seckillGoods.getTitle()!=null && seckillGoods.getTitle().length()>0){
				criteria.andTitleLike("%"+seckillGoods.getTitle()+"%");
			}
		}
		// PageHelper第二种方式
		Page<TbSeckillGoods> page = (Page<TbSeckillGoods>) seckillGoodsMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(TbSeckillGoods seckillGoods) {
		seckillGoodsMapper.insert(seckillGoods);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbSeckillGoods findOne(Long id) {
		return seckillGoodsMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(TbSeckillGoods seckillGoods) {
		seckillGoodsMapper.updateByPrimaryKey(seckillGoods);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			seckillGoodsMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	@Autowired
	private RedisTemplate redisTemplate;
	
	// 返回正在参与秒杀的商品
	@Override
	public List<TbSeckillGoods> findList() {
		redisTemplate.delete("seckillGoods2");
		
		List<TbSeckillGoods> seckillGoodsList =	redisTemplate.boundHashOps("seckillGoods2").values();
		if(true){
	//	if(seckillGoodsList==null || seckillGoodsList.size()==0){
			TbSeckillGoodsExample example = new TbSeckillGoodsExample();
			Criteria criteria = example.createCriteria();
			criteria.andStatusEqualTo("1");      //审核通过的商品
			criteria.andStockCountGreaterThan(0);//库存数大于0
			criteria.andStartTimeLessThanOrEqualTo(new Date()); //开始日期小于等于当前日期
			criteria.andEndTimeGreaterThanOrEqualTo(new Date());//截止日期大于等于当前日期
			seckillGoodsList = seckillGoodsMapper.selectByExample(example);
			//将列表数据装入缓存
			for(TbSeckillGoods seckillGoods:seckillGoodsList){
				redisTemplate.boundHashOps("seckillGoods2").put(seckillGoods.getId(), seckillGoods);
			}
			System.out.println("从数据库中读取数据装入缓存");
		}else{
			System.out.println("从缓存中读取数据");
		}
		return seckillGoodsList;
	}
	
	// 根据ID获取实体(从缓存中读取)
	@Override
	public TbSeckillGoods findOneFromRedis(Long id) {
		return (TbSeckillGoods)redisTemplate.boundHashOps("seckillGoods2").get(id);
	}
}

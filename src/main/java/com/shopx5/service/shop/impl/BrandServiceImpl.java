package com.shopx5.service.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shopx5.common.shop.PageResult;
import com.shopx5.mapper.SsBrandMapper;
import com.shopx5.pojo.SsBrand;
import com.shopx5.pojo.SsBrandExample;
import com.shopx5.pojo.SsBrandExample.Criteria;
import com.shopx5.service.shop.BrandService;

/**
 * Brand服务层实现
 * @author tang
 * @qq 1503501970
 */
@Service
public class BrandServiceImpl implements BrandService {
	@Autowired
	private SsBrandMapper brandMapper;
	
	/**
	 * 查询全部
	 * @return
	 */
	@Override
	public List<SsBrand> findAll() {
		return brandMapper.selectByExample(null);
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@Override
	public List<SsBrand> findByParentId(Long parentId) {
		SsBrandExample example = new SsBrandExample();
		Criteria criteria = example.createCriteria();
		// 设置条件
		// criteria.andParentIdEqualTo(parentId);
		return brandMapper.selectByExample(example);
	}
	
	/**
	 * 查询分页
	 * @param pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// PageHelper第二种方式
		Page<SsBrand> page = (Page<SsBrand>) brandMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 查询分页: 带条件
	 * @param brand, pageNum, pageSize
	 * @return
	 */
	@Override
	public PageResult findPage(SsBrand brand, Integer pageNum, Integer pageSize) {
		// 使用分页插件
		PageHelper.startPage(pageNum, pageSize);
		// 进行条件查询
		SsBrandExample example = new SsBrandExample();
		Criteria criteria = example.createCriteria();
		// 设置条件:
		if(brand != null){			
			if(brand.getName()!=null && brand.getName().length()>0){
				criteria.andNameLike("%"+brand.getName()+"%");
			}
			if(brand.getFirstChar()!=null && brand.getFirstChar().length()>0){
				criteria.andFirstCharLike("%"+brand.getFirstChar()+"%");
			}
		}
		// PageHelper第二种方式
		Page<SsBrand> page = (Page<SsBrand>) brandMapper.selectByExample(example);
		return new PageResult(page.getTotal(), page.getResult());
	}
	
	/**
	 * 增加
	 */
	@Override
	public void add(SsBrand brand) {
		brandMapper.insert(brand);
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@Override
	public SsBrand findOne(Long id) {
		return brandMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改
	 */
	@Override
	public void update(SsBrand brand) {
		brandMapper.updateByPrimaryKey(brand);
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@Override
	public void delete(Long[] ids) {
		for (Long id : ids) {
			brandMapper.deleteByPrimaryKey(id);
		}
	}
	
	/**
	 * 自定义
	 */
	@Override
	public List<Map> selectOptionList() {
		return brandMapper.selectOptionList();
	}
	
}

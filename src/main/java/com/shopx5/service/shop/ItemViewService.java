package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.pojo.SxView;

public interface ItemViewService {
	// 获取商品浏览历史
	public List<SxView> findViews(String userId);
	// 添加商品浏览历史
	public void addView(SxView view);
	// 删除
	public void delView(SxView view);
}

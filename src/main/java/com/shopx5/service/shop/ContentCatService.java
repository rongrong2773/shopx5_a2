package com.shopx5.service.shop;

import java.util.List;
import java.util.Map;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbContentCat;

/**
 * ContentCat服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface ContentCatService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbContentCat> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbContentCat> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param contentCat, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbContentCat contentCat, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(TbContentCat contentCat);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbContentCat findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbContentCat contentCat);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
}

package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.PageBean;
import com.shopx5.pojo.SxCollection;
import com.shopx5.pojo.group.Good;
/**
 * Collection服务层接口
 * @author ShopX5 tang
 */
public interface CollectionService {
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SxCollection> findAll(String userId);
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<Good> findPage(int page, String userId);
	/**
	 * 增加
	 */
	public void add(SxCollection sxCollection);
	/**
	 * 修改
	 */
	public void update(SxCollection sxCollection);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SxCollection getOne(Long id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Long id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
}


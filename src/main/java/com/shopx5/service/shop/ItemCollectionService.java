package com.shopx5.service.shop;

import com.shopx5.common.Result;
import com.shopx5.pojo.SxCollection;

public interface ItemCollectionService {
	//1 判断是否已收藏+收藏次数
	Result indexCollect(String itemId, String userId);
	//2 添加收藏+更新收藏次数
	Result addCollect(SxCollection collection, String skuId);
	//3 删除收藏+更新收藏次数
	Result deleteCollect(String itemId, String userId);
}

package com.shopx5.service.shop;

import java.util.List;
import java.util.Map;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbContent;

/**
 * Content服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface ContentService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbContent> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbContent> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param content, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbContent content, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(TbContent content);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbContent findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbContent content);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
	// 通过内容类目ID获取数据
	public List<TbContent> findByCategoryId(Long categoryId);
}

package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbOrderItem;

/**
 * OrderItem服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface OrderItemService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbOrderItem> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbOrderItem> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param orderItem, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbOrderItem orderItem, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(TbOrderItem orderItem);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbOrderItem findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbOrderItem orderItem);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
}

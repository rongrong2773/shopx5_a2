package com.shopx5.service.shop;

import java.util.List;
import java.util.Map;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.SsTemplateType;

/**
 * TemplateType服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface TemplateTypeService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<SsTemplateType> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<SsTemplateType> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param templateType, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(SsTemplateType templateType, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(SsTemplateType templateType);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SsTemplateType findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(SsTemplateType templateType);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	public List<Map> selectOptionList();
	
	// 根据模板ID,获得规格封装选项
	public List<Map> findSpecList(Long id);
	
}

package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbComment;
import com.shopx5.pojo.group.Comment;

/**
 * Comment服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface CommentService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbComment> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbComment> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param comment, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbComment comment, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(TbComment comment);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbComment findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbComment comment);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
	// 根据goodsId 获取SPU评论数据
	public List<Comment> findByGoodsId(Long goodsId);
	
}

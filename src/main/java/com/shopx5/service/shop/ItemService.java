package com.shopx5.service.shop;

import java.util.List;
import java.util.Map;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbItem;

/**
 * Item服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface ItemService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbItem> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbItem> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param item, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbItem item, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(TbItem item);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbItem findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbItem item);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	
}

package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.PageBean;
import com.shopx5.pojo.SpUserAddr;
/**
 * UserAddr服务层接口
 * @author ShopX5 tang
 */
public interface UserAddrService {
	//1 添加用户地址
	Integer addUserAddr(SpUserAddr spUserAddr);
	//2 获取当前用户  默认地址1个
	SpUserAddr getUserAddrByUserId(String userId, Integer i);
	//3 获取当前用户 所有地址
	List<SpUserAddr> findUserAddrByUserId(String userId);
	//4 获取当前用户  默认地址1个
	SpUserAddr getUserAddrByAddrId(String addrId);
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SpUserAddr> findAll();
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<SpUserAddr> findPage(int page);
	/**
	 * 增加
	 */
	public void add(SpUserAddr spUserAddr, String userId);
	/**
	 * 修改
	 */
	public void update(SpUserAddr spUserAddr, String userId);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SpUserAddr getOne(String id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(String id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
	// 获取登录用户 地址列表
	public List<SpUserAddr> findListByUserId(String userId);
}

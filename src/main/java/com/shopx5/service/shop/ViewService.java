package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.PageBean;
import com.shopx5.pojo.SxView;
import com.shopx5.pojo.group.Good;
/**
 * View服务层接口
 * @author ShopX5 tang
 */
public interface ViewService {
	/**
	 * 返回全部列表
	 * @return
	 */
	public List<SxView> findAll(String userId);
	/**
	 * 返回分页列表
	 * @return
	 */
	public PageBean<Good> findPage(int page, String userId);
	/**
	 * 增加
	 */
	public void add(SxView sxView);
	/**
	 * 修改
	 */
	public void update(SxView sxView);
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public SxView findOne(Long id);
	/**
	 * 根据id删除实体
	 * @param id
	 */
	public void delete(Long id);
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
}

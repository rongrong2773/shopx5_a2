package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbSeckillGoods;

/**
 * SeckillGoods服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface SeckillGoodsService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbSeckillGoods> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbSeckillGoods> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param seckillGoods, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbSeckillGoods seckillGoods, Integer pageNum, Integer pageSize, Integer grade);
	
	/**
	 * 增加
	 */
	public void add(TbSeckillGoods seckillGoods);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbSeckillGoods findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(TbSeckillGoods seckillGoods);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */

	// 返回正在参与秒杀的商品
	public List<TbSeckillGoods> findList();
	
	// 根据ID获取实体(从缓存中读取)
	public TbSeckillGoods findOneFromRedis(Long id);
}

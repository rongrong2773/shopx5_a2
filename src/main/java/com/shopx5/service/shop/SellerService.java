package com.shopx5.service.shop;

import java.util.List;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbSeller;

/**
 * Seller服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface SellerService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<TbSeller> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<TbSeller> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param seller, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(TbSeller seller, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加    #注册
	 */
	public void add(TbSeller seller);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public TbSeller findOne(String id);
	
	/**
	 * 修改
	 */
	public void update(TbSeller seller);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(String[] ids);
	
	/**
	 * 自定义
	 */
	
	//更新状态
	public void updateStatus(String sellerId, String status);
	
	//校验数据
	public String checkData(String param, String name);
}

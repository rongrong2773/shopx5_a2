package com.shopx5.service.shop;

import java.util.List;
import java.util.Map;

import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.SsSpecification;
import com.shopx5.pojo.group.Specification;

/**
 * Specification服务层接口
 * @author tang
 * @qq 1503501970
 */
public interface SpecificationService {
	/**
	 * 返回全部
	 * @return
	 */
	public List<SsSpecification> findAll();
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	public List<SsSpecification> findByParentId(Long parentId);
	
	/**
	 * 分页
	 * @param pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(Integer pageNum, Integer pageSize);
	
	/**
	 * 分页 : 带条件
	 * @param specification, pageNum, pageSize
	 * @return
	 */
	public PageResult findPage(SsSpecification specification, Integer pageNum, Integer pageSize);
	
	/**
	 * 增加
	 */
	public void add(Specification specification);
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	public Specification findOne(Long id);
	
	/**
	 * 修改
	 */
	public void update(Specification specification);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Long[] ids);
	
	/**
	 * 自定义
	 */
	public List<Map> selectOptionList();
	
}

package com.shopx5.service.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shopx5.service.shop.ItemSearchService;
import com.shopx5.service.shop.SolrUtilService;
/**
 * 监听类
 * @author tang Shop X5 多商户 商城系统
 * @qq 150 3501 970
 */
@Component
public class ItemSearchListener implements MessageListener {
	@Autowired
	private ItemSearchService itemSearchService;
	
	@Autowired
	private SolrUtilService solrUtilService;
	
	@Override
	public void onMessage(Message message) {
		ObjectMessage objectMessage = (ObjectMessage) message;
		try {
			Long[] goodsIds = (Long[]) objectMessage.getObject();
			System.out.println("监听solr索引库：Long[] goodsIds ：" + goodsIds);
			solrUtilService.saveBeans(goodsIds);
			System.out.println("导入solr索引库：true");
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
		/*TextMessage textMessage = (TextMessage) message;
		try {
			String text = textMessage.getText(); //json字符串
			System.out.println("监听到消息:"+text);
			List<TbItem> itemList = JSON.parseArray(text, TbItem.class);
			itemSearchService.importList(itemList);
			System.out.println("导入到solr索引库");
		} catch (JMSException e) {
			e.printStackTrace();
		}*/
	}
}

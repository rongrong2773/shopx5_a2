package com.shopx5.service.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shopx5.service.shop.ItemPageService;
/**
 * 监听类
 * @author tang Shop X5 多商户 商城系统
 * @qq 150 3501 970
 */
@Component
public class PageDeleteListener implements MessageListener {
	@Autowired
	private ItemPageService itemPageService;

	@Override
	public void onMessage(Message message) {
		ObjectMessage objectMessage = (ObjectMessage) message;
		try {
			Long[] goodsIds = (Long[]) objectMessage.getObject();
			System.out.println("接收page静态：goodsIds ：" + goodsIds);
			boolean b = itemPageService.deleteItemHtml(goodsIds);
			System.out.println("删除page静态：" + b);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}

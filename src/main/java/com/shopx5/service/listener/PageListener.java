package com.shopx5.service.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shopx5.service.shop.ItemPageService;
/**
 * 监听类（用于生成网页）
 * @author tang Shop X5 多商户 商城系统
 * @qq 150 3501 970
 */
@Component
public class PageListener implements MessageListener {
	@Autowired
	private ItemPageService itemPageService;

	@Override
	public void onMessage(Message message) {
		TextMessage textMessage = (TextMessage) message;
		try {
			String goodsId = textMessage.getText();
			System.out.println("接收page静态：goodsIds ：" + goodsId);
			boolean b = itemPageService.genItemHtml(Long.parseLong(goodsId));
			System.out.println("生成page静态：" + b);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}

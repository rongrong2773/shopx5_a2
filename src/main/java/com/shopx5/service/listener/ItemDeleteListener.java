package com.shopx5.service.listener;

import java.util.Arrays;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shopx5.service.shop.ItemSearchService;
/**
 * 监听类（用于生成网页）
 * @author tang Shop X5 多商户 商城系统
 * @qq 150 3501 970
 */
@Component
public class ItemDeleteListener implements MessageListener {
	@Autowired
	private ItemSearchService itemSearchService;
	
	@Override
	public void onMessage(Message message) {
		ObjectMessage objectMessage = (ObjectMessage) message;
		try {
			Long[] goodsIds= (Long[]) objectMessage.getObject();
			System.out.println("监听solr索引库：goodsIds ："+goodsIds);
			itemSearchService.deleteByGoodsIds(Arrays.asList(goodsIds));
			System.out.println("删除solr索引库：true");
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}

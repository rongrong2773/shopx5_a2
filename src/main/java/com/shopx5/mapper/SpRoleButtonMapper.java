package com.shopx5.mapper;

import com.shopx5.pojo.SpRoleButton;
import com.shopx5.pojo.SpRoleButtonExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpRoleButtonMapper {
    int countByExample(SpRoleButtonExample example);

    int deleteByExample(SpRoleButtonExample example);

    int insert(SpRoleButton record);

    int insertSelective(SpRoleButton record);

    List<SpRoleButton> selectByExample(SpRoleButtonExample example);

    int updateByExampleSelective(@Param("record") SpRoleButton record, @Param("example") SpRoleButtonExample example);

    int updateByExample(@Param("record") SpRoleButton record, @Param("example") SpRoleButtonExample example);
}
package com.shopx5.mapper;

import com.shopx5.pojo.ScLog;
import com.shopx5.pojo.ScLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ScLogMapper {
    int countByExample(ScLogExample example);

    int deleteByExample(ScLogExample example);

    int deleteByPrimaryKey(String logId);

    int insert(ScLog record);

    int insertSelective(ScLog record);

    List<ScLog> selectByExample(ScLogExample example);

    ScLog selectByPrimaryKey(String logId);

    int updateByExampleSelective(@Param("record") ScLog record, @Param("example") ScLogExample example);

    int updateByExample(@Param("record") ScLog record, @Param("example") ScLogExample example);

    int updateByPrimaryKeySelective(ScLog record);

    int updateByPrimaryKey(ScLog record);
}
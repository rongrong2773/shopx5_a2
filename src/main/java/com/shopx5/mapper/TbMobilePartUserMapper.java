package com.shopx5.mapper;

import org.apache.ibatis.annotations.Insert;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbMobilePartUser;
import com.shopx5.pojo.TbMobileUserExample;

public interface TbMobilePartUserMapper {

	
	@Insert("insert into tb_mobile_part_user(id, client_id, image, real_name, phone, nick_name, user_type, token) values (#{id},"
			+ "#{clientId}, #{image}, #{realName}, #{phone}, #{nickName}, #{userType}, #{token})")
	void add(TbMobilePartUser mPartUser);

	Page<TbMobilePartUser> selectByExample(TbMobileUserExample example);

}

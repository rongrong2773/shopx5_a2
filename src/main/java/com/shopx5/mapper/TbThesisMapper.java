package com.shopx5.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbThesis;
import com.shopx5.pojo.TbThesisExample;

/**
 * 论题mapper
 * @author sjx
 * 2019年7月1日 下午5:07:57
 */
public interface TbThesisMapper {

	@Insert("insert into tb_thesis(id,user_id,is_anon,title,url,background,heat_count,shared_times,comment_count,collect_count,"
			+ "is_conn_twv,conn_twv_id,thesis_type,mobile_user_id,"
			+ "left_opinion,right_opinion,left_count,right_count,"
			
			+ "tech_type,environment_type,film_type,celebrity_type,emotion_type,economics_type,literature_type,"
			+ "philosophy_type,journey_type,sports_type,science_type,creativity_type,industry_type,law_type,"
			+ "food_type, medical_type,other_type)"
			
			+ " values(#{id},#{userId},#{isAnon},#{title},#{url},#{background},#{heatCount},#{sharedTimes},#{commentCount},#{collectCount},#{isConnTwv},#{connTwvId},#{thesisType},"
			+ " #{mobileUserId},#{leftOpinion}, #{rightOpinion},#{leftCount},#{rightCount},"
			
			+ "#{techType},#{environmentType},#{filmType},#{celebrityType},#{emotionType},#{economicsType},#{literatureType},"
			+ "#{philosophyType},#{journeyType},#{sportsType},#{scienceType},#{creativityType},#{industryType},#{lawType},"
			+ "#{foodType},#{medicalType},#{otherType})")
	public void save(TbThesis tbThesis);

	@Insert("insert into tb_thesis_twv_conn (thesis_id, twv_id) value(#{0}, #{1})")
	public void saveTwvConn(String thesisId, String connTwvId);

	public Page<TbThesis> selectByExample(TbThesisExample example);

	@Delete("delete from tb_thesis where id = #{0} and user_id = #{1}")
	public void delThesisById(String thesisId, String usrId);

	@Select("select * from tb_thesis where mobile_user_id = #{usrId} and is_conn_twv = 'yes'")
	public List<TbThesis> findAllTypeThesisById(String usrId);
	
	@Select("select count(id) from tb_thesis where mobile_user_id=#{0}")
	public Integer findThesisCountById(String userId);

	@Select("select * from tb_thesis where id = #{0}")
	@Results({
		@Result(id=true,column="id",property="id"),
		@Result(column="title",property="title"),
		@Result(column="url",property="url"),
		@Result(column="background",property="background"),
		@Result(column="thesis_type",property="thesisType"),
		@Result(column="tech_type",property="techType"),
		@Result(column="environment_type",property="environmentType"),
		@Result(column="film_type",property="filmType"),
		@Result(column="celebrity_type",property="celebrityType"),
		@Result(column="emotion_type",property="emotionType"),
		@Result(column="economics_type",property="economicsType"),
		@Result(column="literature_type",property="literatureType"),
		@Result(column="philosophy_type",property="philosophyType"),
		@Result(column="journey_type",property="journeyType"),
		@Result(column="sports_type",property="sportsType"),
		@Result(column="science_type",property="scienceType"),
		@Result(column="creativity_type",property="creativityType"),
		@Result(column="industry_type",property="industryType"),
		@Result(column="law_type",property="lawType"),
		@Result(column="food_type",property="foodType"),
		@Result(column="medical_type",property="medicalType"),
		@Result(column="other_type",property="otherType"),
		@Result(column="mobile_user_id",property="mobileUserId"),
		@Result(column="heat_count",property="heatCount"),
		@Result(column="shared_times",property="sharedTimes"),
		@Result(column="create_time",property="createTime"),
		@Result(column="left_opinion",property="leftOpinion"),
		@Result(column="right_opinion",property="rightOpinion"),
		@Result(column="comment_count",property="commentCount"),
		@Result(column="left_count",property="leftCount"),
		@Result(column="right_count",property="rightCount"),
		@Result(column="collect_count",property="collectCount"),
		@Result(column="is_conn_twv",property="isConnTwv"),
		@Result(column="user_id",property="userId"),
		@Result(column="is_anon",property="isAnon"),
		@Result(column="conn_twv_id",property="connTwvId")
	})
	public TbThesis findEntityById(String aimInfoId);

	@Update("update tb_thesis set heat_count = #{0} where id = #{1}")
	public void updateHeatCount(Integer heatCount, String thesisId);
	

}

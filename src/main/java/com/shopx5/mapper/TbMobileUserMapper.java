package com.shopx5.mapper;

import java.util.HashSet;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbMobilePartUser;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbMobileUserConnect;
import com.shopx5.pojo.TbMobileUserExample;
import com.shopx5.pojo.TbMobileUserLoginInfo;
import com.shopx5.pojo.TbPushXSMsg;
import com.shopx5.pojo.TbUserConnected;

/**
 * 采用xml和注解方式混合开发
 * @author sjx
 * 2019年6月26日 下午3:59:12
 */
public interface TbMobileUserMapper {

	// 检测手机号是否已经注册
	@Select("select id from tb_mobile_user where phone = #{phone}")
	@Results({
		@Result(id=true,column="id",property="id")
		})
	Long checkPhone(String phone);

	
	@Insert("insert into tb_mobile_user(id,nick_name,real_name,phone,client_id,image,"
			+ "release_counts, money_counts, jf_counts, unread_msg_cnts,"
			+ "concern_count, fans_count, user_type,lic_authority, company_name, credential_url,"
			+ "signature, prefix, suffix, heat_collect_notify, comment_reply_notify, concern_notify, "
			+ "login_times,token,create_time,update_time) "
			+ "values(#{id},#{nickName},#{realName},#{phone},#{clientId},#{image},"
			+ "#{releaseCounts},#{moneyCounts},#{jfCounts},#{unReadMsgCnts},"
			+ "#{concernCount}, #{fansCount},#{userType}, #{licAuthority}, #{companyName}, #{credentialUrl},"
			+ "#{signature}, #{prefix},#{suffix},#{heatCollectNotify},#{commentReplyNotify},#{concernNotify}, #{loginTimes},#{token}, #{createTime},#{updateTime})")
	void add(TbMobileUser mbUser);

//	PageResult findPage(Long ownerId, Integer page, Integer rows);

	
	// 查询用户好友分页列表 #### 该表达实际上还是查询出了实体所有字段，需要考虑的别的解决方案
	@Select("select id, image, token, real_name, nick_name, user_type, phone from tb_mobile_user"
			+ " where id in (SELECT friend_id FROM tb_mobile_user_friends WHERE owner_id=#{owner_id})")
	@Results({
		@Result(id=true,column="id",property="id"),
		@Result(column="image",property="image"),
		@Result(column="token",property="token"),
		@Result(column="real_name",property="realName"),
		@Result(column="nick_name",property="nickName"),
		@Result(column="user_type",property="userType"),
		@Result(column="phone",property="phone")
	})
	Page<TbMobileUser> selectByPrimaryKey(Long ownerId);


	// 查询用户好友页列表  - 部分字段分
	@Select("select id, image, token, real_name, nick_name, user_type, phone from tb_mobile_part_user"
			+ " where id in (SELECT friend_id FROM tb_mobile_user_friends WHERE owner_id=#{owner_id})")
	@Results({
		@Result(id=true,column="id",property="id"),
		@Result(column="image",property="image"),
		@Result(column="token",property="token"),
		@Result(column="real_name",property="realName"),
		@Result(column="nick_name",property="nickName"),
		@Result(column="user_type",property="userType"),
		@Result(column="phone",property="phone")
	})
	Page<TbMobilePartUser> selectByPrimaryKeyPart(Long ownerId);


	// 查找我关注的人
	@Select("SELECT cascaded_id FROM tb_mobile_user_connected WHERE cascading_id=#{cascading_id}")
	@Results({
		@Result(column="cascading_id", property="cascadingId"),
		@Result(column="cascaded_id", property="cascadedId")
	})
	List<TbUserConnected> selectListByPrimaryKey(Long userId);

	@Select("SELECT cascading_id FROM tb_mobile_user_connected WHERE cascaded_id=#{cascaded_id}")
	@Results({
		@Result(column="cascading_id", property="cascadingId"),
		@Result(column="cascaded_id", property="cascadedId")
	})
	// 关注我的人 id列表
	List<TbUserConnected> selectListByPrimaryKey2(Long userId);

	@Select("select id from tb_mobile_user where auth_info is not null")
	// 查找所有认证用户
	List<TbMobileUser> findAuthUsrsList();


	@Select("select id from tb_mobile_user where phone = #{phone}")
	@Results({
		@Result(id=true,column="id",property="id")
	})
	Long findPriIdByPhone(String phone);


	@Insert("update tb_mobile_user set token=#{0}, client_id = #{1}, login_times = #{2} where id=#{3}")
	void updateTokenClientIdLoginTimes(String token,String clientId, Integer loginTimes, Long id);


	@Select("select token from tb_mobile_user where id = #{usrId}")
	@Results({
		@Result(column="token",property="token")
	})
	String findTokenById(Long usrId);

	// 获取所有用户的token
	@Select("select token from tb_mobile_user")
	@Results({
		@Result(column="token", property="token")
	})
	List<String> findTokenList();



	@Select("select id,image,nick_name,signature,prefix,suffix,heat_collect_notify,comment_reply_notify, concern_notify,"
			+ "login_times, fans_count,concern_count,user_type,release_counts,"
			+ "jf_counts,money_counts,unread_msg_cnts from tb_mobile_user where phone = #{phone}")
	@Results({
		@Result(id=true,column="id", property="id"),
		@Result(column="image", property="image"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="signature", property="signature"),
		@Result(column="prefix", property="prefix"),
		@Result(column="suffix", property="suffix"),
		@Result(column="heat_collect_notify", property="heatCollectNotify"),
		@Result(column="comment_reply_notify", property="commentReplyNotify"),
		@Result(column="concern_notify", property="concernNotify"),
		@Result(column="login_times", property="loginTimes"),
		@Result(column="fans_count", property="fansCount"),
		@Result(column="concern_count", property="concernCount"),
		@Result(column="user_type", property="userType"),
		@Result(column="release_counts", property="releaseCounts"),
		@Result(column="jf_counts", property="jfCounts"),
		@Result(column="money_counts", property="moneyCounts"),
		@Result(column="unread_msg_cnts", property="unReadMsgCnts"),
	})
	TbMobileUser findPartFieldsByPhone(String phone);

	// 关注功能
	@Insert("insert into tb_mobile_user_connected (cascading_id, cascaded_id) values (#{0}, #{1})")
	void addConnWith(Long usrId, Long aimUsrId);

	@Select("select cascaded_id from tb_mobile_user_connected where cascading_id= #{usrId}")
	Long findCascadedIdByCascadingId(Long usrId);

	@Delete("delete from tb_mobile_user_connected where cascading_id= #{usrId} and cascaded_id = #{aimUsrId}")
	void cancelConnWith(@Param("usrId") Long usrId, @Param("aimUsrId") Long aimUsrId);
	

	// 查找我关注的人id列表
	@Select("select cascaded_id from tb_mobile_user_connected where cascading_id = #{usrId}")
	List<String> findMyConcernListById(String usrId);
	
	// 查找我关注的人id列表
	@Select("select cascaded_id from tb_mobile_user_connected where cascading_id = #{usrId}")
	HashSet<String> findMyConcernSetById(String usrId);
	
	// 查找某用户关注的用户
	@Select("select cascaded_id from tb_mobile_user_connected where cascading_id=#{usrId}")
	@Results({
		@Result(column="cascaded_id",property="cascadedId")
	})
	List<Long> findCasacdedIdsById(Long usrId);
	
	// 查看关注我的人/我的粉丝id列表
	@Select("select cascading_id from tb_mobile_user_connected where cascaded_id = #{usrId}")
	List<String> findMyFansIdsById(String usrId);
	
	@Select("select cascading_id from tb_mobile_user_connected where cascaded_id = #{usrId}")
	HashSet<String> findMyFansIdsHashSetById(String usrId);
	
	Page<TbMobileUserConnect> selectByExample(TbMobileUserExample example);

	
	@Select("select * from tb_mobile_user where id = #{usrId}")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="image", property="image"),
		@Result(column="real_name", property="realName"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="gender", property="gender"),
		@Result(column="fans_count", property="fansCount"),
		@Result(column="concern_count", property="concernCount"),
		@Result(column="rigion", property="rigion"),
		@Result(column="user_type", property="userType"),
		@Result(column="id_type", property="idType"),
		@Result(column="id_number", property="idNumber"),
		@Result(column="phone", property="phone"),
		
		@Result(column="heat_collect_notify", property="heatCollectNotify"),
		@Result(column="comment_reply_notify", property="commentReplyNotify"),
		@Result(column="concern_notify", property="concernNotify"),
		@Result(column="client_id", property="clientId"),
		
		@Result(column="login_times", property="loginTimes"),
		@Result(column="login_device", property="loginDevice"),
		@Result(column="last_login_time", property="lastLoginTime"),
		@Result(column="last_login_location", property="lastLoginLocation"),
		@Result(column="create_time", property="createTime"),
		@Result(column="update_time", property="updateTime"),
		
		@Result(column="token", property="token"),
		@Result(column="signature", property="signature"),
		@Result(column="release_counts", property="releaseCounts"),
		@Result(column="money_counts", property="moneyCounts"),
		@Result(column="jf_counts", property="jfCounts"),
		@Result(column="unread_msg_cnts", property="unReadMsgCnts"),
		
		@Result(column="prefix", property="prefix"),
		@Result(column="suffix", property="suffix"),
	})
	TbMobileUser findEntityById(Long usrId);
	
	@Select("select * from tb_mobile_user where id = #{0} and token = #{1}")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="image", property="image"),
		@Result(column="real_name", property="realName"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="gender", property="gender"),
		@Result(column="fans_count", property="fansCount"),
		@Result(column="concern_count", property="concernCount"),
		@Result(column="rigion", property="rigion"),
		@Result(column="user_type", property="userType"),
		@Result(column="id_type", property="idType"),
		@Result(column="id_number", property="idNumber"),
		@Result(column="phone", property="phone"),
		
		@Result(column="heat_collect_notify", property="heatCollectNotify"),
		@Result(column="comment_reply_notify", property="commentReplyNotify"),
		@Result(column="concern_notify", property="concernNotify"),
		@Result(column="client_id", property="clientId"),
		
		@Result(column="login_times", property="loginTimes"),
		@Result(column="login_device", property="loginDevice"),
		@Result(column="last_login_time", property="lastLoginTime"),
		@Result(column="last_login_location", property="lastLoginLocation"),
		@Result(column="create_time", property="createTime"),
		@Result(column="update_time", property="updateTime"),
		
		@Result(column="token", property="token"),
		@Result(column="signature", property="signature"),
		@Result(column="release_counts", property="releaseCounts"),
		@Result(column="money_counts", property="moneyCounts"),
		@Result(column="jf_counts", property="jfCounts"),
		@Result(column="unread_msg_cnts", property="unReadMsgCnts"),
		
		@Result(column="prefix", property="prefix"),
		@Result(column="suffix", property="suffix")
	})
	TbMobileUser findEntityByIdToken(Long usrId, String token);
	

	@Update("update tb_mobile_user set image = #{image}, real_name = #{realName}, nick_name = #{nickName}, gender = #{gender}, "
			+ "fans_count = #{fansCount}, concern_count = #{concernCount}, rigion = #{rigion}, user_type = #{userType},"
			+ "lic_authority = #{licAuthority}, company_name = #{companyName}, credential_url = #{credentialUrl},"
			+ "id_type = #{idType},id_number = #{idNumber}, phone = #{phone}, login_times = #{loginTimes}, "
			+ "token = #{token}, signature = #{signature}, release_counts = #{releaseCounts}, money_counts = #{moneyCounts},"
			+ "jf_counts = #{jfCounts}, unread_msg_cnts = #{unReadMsgCnts}, prefix = #{prefix}, suffix = #{suffix},"
			+ "heat_collect_notify = #{heatCollectNotify},comment_reply_notify = #{commentReplyNotify},concern_notify = #{concernNotify} where id = #{id}")
	void update(TbMobileUser bean);


	@Insert("insert into tb_mobile_user_login_info(id, device, longitude, latitude) values (#{id}, #{device}, #{longitude}, #{latitude})")
	void saveUsrLoginInfoEntity(TbMobileUserLoginInfo tbLoginInfo);

	// 查询个人中心 - 推送设置3个字段
	@Select("select heat_collect_notify, comment_reply_notify, concern_notify from tb_mobile_user where id = #{0} and token = #{1}")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="heat_collect_notify", property="heatCollectNotify"),
		@Result(column="comment_reply_notify", property="commentReplyNotify"),
		@Result(column="concern_notify", property="concernNotify")
	})
	TbMobileUser findPushFieldsByIdAndToken(String usrId, String token);

	
	// 查找所有认证用户
	@Select("select * from tb_mobile_user where prefix != null and suffix != null")
	HashSet<String> findAuthedUsrHashSet();

	
	@Select("select client_id from tb_mobile_user where id = #{aimUsrId}")
	@Results({
		@Result(column="client_id", property="clientId")
	})
	String findClientIdByPrimaryId(Long aimUsrId);

	// 查询关注表中两用户的关联关系: usrId 是否关注了 aimUsrId
	@Select("select * from tb_mobile_user_connected where cascading_id = #{0} and cascaded_id = #{1}")
	@Results({
		@Result(column = "cascading_id", property = "cascadingId"),
		@Result(column = "cascaded_id", property = "cascadedId"),
		@Result(column = "create_time", property = "createTime")
	})
	TbUserConnected findEntityByUsrAimUsrId(Long usrId, Long aimUsrId);

	
	@Select("select nick_name from tb_mobile_user where id = #{usrId}")
	@Results({
		@Result(column="nick_name", property="nickName")
	})
	String findNickNameByPrimaryId(Long usrId);

	
	@Select("select * from tb_mobile_user where id = #{0} and token = #{1} and client_id=#{2}")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="image", property="image"),
		@Result(column="real_name", property="realName"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="gender", property="gender"),
		@Result(column="fans_count", property="fansCount"),
		@Result(column="concern_count", property="concernCount"),
		@Result(column="rigion", property="rigion"),
		@Result(column="user_type", property="userType"),
		@Result(column="id_type", property="idType"),
		@Result(column="id_number", property="idNumber"),
		@Result(column="phone", property="phone"),
		
		@Result(column="heat_collect_notify", property="heatCollectNotify"),
		@Result(column="comment_reply_notify", property="commentReplyNotify"),
		@Result(column="concern_notify", property="concernNotify"),
		@Result(column="client_id", property="clientId"),
		
		@Result(column="login_times", property="loginTimes"),
		@Result(column="login_device", property="loginDevice"),
		@Result(column="last_login_time", property="lastLoginTime"),
		@Result(column="last_login_location", property="lastLoginLocation"),
		@Result(column="create_time", property="createTime"),
		@Result(column="update_time", property="updateTime"),
		
		@Result(column="token", property="token"),
		@Result(column="signature", property="signature"),
		@Result(column="release_counts", property="releaseCounts"),
		@Result(column="money_counts", property="moneyCounts"),
		@Result(column="jf_counts", property="jfCounts"),
		@Result(column="unread_msg_cnts", property="unReadMsgCnts"),
		
		@Result(column="prefix", property="prefix"),
		@Result(column="suffix", property="suffix"),
		@Result(column="company_name", property="companyName"),
		@Result(column="credential_url", property="credentialUrl"),
		@Result(column="id_number", property="idNumber"),
		@Result(column="lic_authority", property="licAuthority")
	})
	TbMobileUser findEntity(String userId, String token, String clientId);

	// 返回“个人中心 - 我的消息” 数量
	@Select("select count(id) from tb_mobile_user_msg where user_id = #{userId}")
	Integer findMsgCountById(String userId);

	@Select("select id, image, nick_name, prefix, suffix from tb_mobile_user limit 20")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="image", property="image"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="prefix", property="prefix"),
		@Result(column="suffix", property="suffix")
		})
	List<TbMobileUser> findSimiliarUserList();


	/**
	 * 查询备注
	 * @param usrId   登录用户id
	 * @param id    被关注的用户id
	 * @return
	 */
	@Select("select aim_user_remark from tb_mobile_user_remark where user_id = #{0} and aim_user_id = #{1}")
	@Results({
		@Result(column="aim_user_remark", property="aimUserRemark")
	})
	String findRemarkById(Long usrId, String aimUserId);

	/**
	 * 增加备注
	 * @param userId
	 * @param aimUserId
	 * @param aimUserRemark
	 * @return
	 */
	@Update("update tb_mobile_user_remark set user_id = #{0}, aim_user_id = #{1}, aim_user_remark = #{2}")
	void updateRemark(String userId, String aimUserId, String aimUserRemark);

	/**
	 * 拉黑用户
	 * @param userId
	 * @param aimUserId    被拉黑的用户id
	 */
	@Update("update tb_mobile_user set shield_user_list = #{1} where id = #{0}")
	void shieldUser(Long userId, String aimUserId);
	
	// 查询用户userId的拉黑列表
	@Select("select shield_user_list from tb_mobile_user where id = #{0}")
	String findShieldUserById(String userId);

	@Insert("insert into tb_push_xs_msg (id, target_user_id, msg_type, msg_content) values ("
			+ "#{id}, #{targetUserId}, #{msgType}, #{msgContent})")
	void savePushMsgEntity(TbPushXSMsg bean);

}

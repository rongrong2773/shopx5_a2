package com.shopx5.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.shopx5.pojo.TbAppInfo;

/**
 * 
 * @author sjx
 * 2019年7月2日 上午9:38:26
 */

public interface AppInfoMapper {

	@Select("select version,download_url,update_desc,is_force_update from tb_app_info order by release_time desc")
	@Results({
			@Result(column="version", property="version"),
			@Result(column="download_url",property="downloadUrl"),
			@Result(column="update_desc", property="updateDesc"),
			@Result(column="is_force_update",property="isForceUpdate")
	})
	List<TbAppInfo> findVers();

	@Insert("insert into tb_app_info(id, version, release_time, download_url, update_desc, is_force_update) values (#{id},#{version},#{releaseTime},#{downloadUrl},"
			+ "#{updateDesc},#{isForceUpdate})")
	void save(TbAppInfo tbAppInfo);
}

package com.shopx5.mapper.result;

import java.util.List;

import com.shopx5.common.OrderParam;
import com.shopx5.pojo.result.Order;

public interface OrderMapper {
	
	public List<Order> findAllOrders();
	public List<Order> findOrders(OrderParam orderParam);
	public Order getOrderItems(String orderId);
}

package com.shopx5.mapper.result;

import com.shopx5.pojo.group.Good;

public interface GoodMapper {
	// 获取用户+详情, 足迹 sho p x5
	public Good getGoodAndInfo(Long goodsId);
}

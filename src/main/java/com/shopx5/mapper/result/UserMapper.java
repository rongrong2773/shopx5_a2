package com.shopx5.mapper.result;

import java.util.List;

import com.shopx5.pojo.result.User;

public interface UserMapper {
	
	List<User> findAllUserAndInfo();
	User findUserAndInfo(String userName);
	User findUserAndRole(String userId);
	User getUserByPhone(String phone);
}

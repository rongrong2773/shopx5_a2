package com.shopx5.mapper.result;

import java.util.List;

import com.shopx5.pojo.result.ItemSearch;

public interface ItemSearchMapper {
	
	List<ItemSearch> getItemList();
	ItemSearch getItemById(long itemId);
}

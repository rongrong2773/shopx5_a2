package com.shopx5.mapper.result;

import com.shopx5.pojo.result.Role;

public interface RoleMapper {
	
	Role findRoleAndModule(String roleId);
	Role findRoleAndButton(String roleId);
}

package com.shopx5.mapper;

import com.shopx5.pojo.SxContentCategory;
import com.shopx5.pojo.SxContentCategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SxContentCategoryMapper {
    int countByExample(SxContentCategoryExample example);

    int deleteByExample(SxContentCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SxContentCategory record);

    int insertSelective(SxContentCategory record);

    List<SxContentCategory> selectByExample(SxContentCategoryExample example);

    SxContentCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SxContentCategory record, @Param("example") SxContentCategoryExample example);

    int updateByExample(@Param("record") SxContentCategory record, @Param("example") SxContentCategoryExample example);

    int updateByPrimaryKeySelective(SxContentCategory record);

    int updateByPrimaryKey(SxContentCategory record);
}
package com.shopx5.mapper;

import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpUserMapper {
    int countByExample(SpUserExample example);

    int deleteByExample(SpUserExample example);

    int deleteByPrimaryKey(String userId);

    int insert(SpUser record);

    int insertSelective(SpUser record);

    List<SpUser> selectByExample(SpUserExample example);

    SpUser selectByPrimaryKey(String userId);

    int updateByExampleSelective(@Param("record") SpUser record, @Param("example") SpUserExample example);

    int updateByExample(@Param("record") SpUser record, @Param("example") SpUserExample example);

    int updateByPrimaryKeySelective(SpUser record);

    int updateByPrimaryKey(SpUser record);
}
package com.shopx5.mapper;

import com.shopx5.pojo.SsSpecificationOption;
import com.shopx5.pojo.SsSpecificationOptionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SsSpecificationOptionMapper {
    int countByExample(SsSpecificationOptionExample example);

    int deleteByExample(SsSpecificationOptionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SsSpecificationOption record);

    int insertSelective(SsSpecificationOption record);

    List<SsSpecificationOption> selectByExample(SsSpecificationOptionExample example);

    SsSpecificationOption selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SsSpecificationOption record, @Param("example") SsSpecificationOptionExample example);

    int updateByExample(@Param("record") SsSpecificationOption record, @Param("example") SsSpecificationOptionExample example);

    int updateByPrimaryKeySelective(SsSpecificationOption record);

    int updateByPrimaryKey(SsSpecificationOption record);
}
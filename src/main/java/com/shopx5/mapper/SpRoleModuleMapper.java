package com.shopx5.mapper;

import com.shopx5.pojo.SpRoleModule;
import com.shopx5.pojo.SpRoleModuleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpRoleModuleMapper {
    int countByExample(SpRoleModuleExample example);

    int deleteByExample(SpRoleModuleExample example);

    int insert(SpRoleModule record);

    int insertSelective(SpRoleModule record);

    List<SpRoleModule> selectByExample(SpRoleModuleExample example);

    int updateByExampleSelective(@Param("record") SpRoleModule record, @Param("example") SpRoleModuleExample example);

    int updateByExample(@Param("record") SpRoleModule record, @Param("example") SpRoleModuleExample example);
}
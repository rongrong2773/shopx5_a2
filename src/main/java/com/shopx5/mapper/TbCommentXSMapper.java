package com.shopx5.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbCommentXS;
import com.shopx5.pojo.TbCommentXSExample;

/**
 * 
 * @author sjx
 * 2019年7月4日 下午3:25:47
 */
public interface TbCommentXSMapper {

	
	@Insert("insert into tb_comment_xs(id,aim_info_id,user_id,user_image_url, user_nick_name, comment,comment_id,heat_count,"
			+ "reply_count, create_time) values(#{id},#{aimInfoId},#{userId},#{userImageUrl},#{userNickName},#{comment},#{commentId},#{heatCount},#{replyCount},#{createTime})")
	void save(TbCommentXS tbComment);

	@Delete("delete from tb_comment_xs where id = #{id}")
	void delById(@Param("id")String id);

	Page<TbCommentXS> selectByExample(TbCommentXSExample example);

}

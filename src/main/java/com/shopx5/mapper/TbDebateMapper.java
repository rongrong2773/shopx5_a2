package com.shopx5.mapper;


import java.util.List;

import com.shopx5.pojo.TbDebateTimeLimited;

public interface TbDebateMapper {

	void save(TbDebateTimeLimited entity1);
	
	List<TbDebateTimeLimited> findRunningDebateListById(String userId);

}

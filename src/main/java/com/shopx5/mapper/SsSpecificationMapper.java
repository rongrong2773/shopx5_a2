package com.shopx5.mapper;

import com.shopx5.pojo.SsSpecification;
import com.shopx5.pojo.SsSpecificationExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SsSpecificationMapper {
    int countByExample(SsSpecificationExample example);

    int deleteByExample(SsSpecificationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SsSpecification record);

    int insertSelective(SsSpecification record);

    List<SsSpecification> selectByExample(SsSpecificationExample example);

    SsSpecification selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SsSpecification record, @Param("example") SsSpecificationExample example);

    int updateByExample(@Param("record") SsSpecification record, @Param("example") SsSpecificationExample example);

    int updateByPrimaryKeySelective(SsSpecification record);

    int updateByPrimaryKey(SsSpecification record);
    
    List<Map> selectOptionList();
}
package com.shopx5.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbQuestionXS;
import com.shopx5.pojo.TbQuestionXSExample;

/**
 * 
 * @author sjx
 * 2019年8月20日 下午5:50:24
 */
public interface TbQuesXSMapper {

	@Insert("insert into tb_question_xs(id, user_id, user_image_url, nick_name, suffix, speech_id, thesis_id, url,"
			+ "ques_reason, ques_material, agree_count, disagree_count,agree_collect, disagree_collect) values(#{id}, #{userId}, #{userImageUrl}, #{nickName}, #{suffix}, #{speechId}, #{thesisId}, #{url},"
			+ "#{quesReason}, #{quesMaterial}, #{agreeCount}, #{disagreeCount}, #{agreeCollect}, #{disagreeCollect})")
	void saveQuesEntity(TbQuestionXS entity);

	Page<TbQuestionXS> selectByExample(TbQuestionXSExample example);
	
	@Select("select * from tb_question_xs where id = #{0}")
	@Results({
		@Result(id=true,column="id" ,property="id"),
		@Result(column="user_id", property="userId"),
		@Result(column="user_image_url", property="userImageUrl"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="suffix", property="suffix"),
		@Result(column="speech_id", property="speechId"),
		@Result(column="thesis_id", property="thesisId"),
		@Result(column="url", property="url"),
		@Result(column="ques_reason", property="quesReason"),
		@Result(column="ques_material", property="quesMaterial"),
		@Result(column="agree_count", property="agreeCount"),
		@Result(column="disagree_count", property="disagreeCount"),
		@Result(column="agree_collect", property="agreeCollect"),
		@Result(column="disagree_collect", property="disagreeCollect"),
		@Result(column="create_time", property="createTime")
	})
	TbQuestionXS findQuestionEntity(String questionId);

	@Update("update tb_question_xs set agree_count = #{agreeCount},agree_collect = #{agreeCollect}")
	void  updateQuestionEntityAgree(TbQuestionXS tbQuestionXS);

	@Update("update tb_question_xs set disagree_count = #{disagreeCount},disagree_collect = #{disagreeCollect}")
	void updateQuestionEntityDisagree(TbQuestionXS tbQuestionXS);

}

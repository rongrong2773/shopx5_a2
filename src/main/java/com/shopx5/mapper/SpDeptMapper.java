package com.shopx5.mapper;

import com.shopx5.pojo.SpDept;
import com.shopx5.pojo.SpDeptExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpDeptMapper {
    int countByExample(SpDeptExample example);

    int deleteByExample(SpDeptExample example);

    int deleteByPrimaryKey(String deptId);

    int insert(SpDept record);

    int insertSelective(SpDept record);

    List<SpDept> selectByExample(SpDeptExample example);

    SpDept selectByPrimaryKey(String deptId);

    int updateByExampleSelective(@Param("record") SpDept record, @Param("example") SpDeptExample example);

    int updateByExample(@Param("record") SpDept record, @Param("example") SpDeptExample example);

    int updateByPrimaryKeySelective(SpDept record);

    int updateByPrimaryKey(SpDept record);
}
package com.shopx5.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
/**
 * @author sjx
 * 2019年7月19日 下午4:30:17
 */
public interface TbUserConnectedMapper {

	// 查找我关注的用户id
	@Select("select cascaded_id from tb_user_connected where cascading_id = #{usrId}")
	@Results({
		@Result(column = "cascaded_id" ,property="cascadedId")
	})
	List<Long> findConnectList(Long usrId);
	
	// 查找关注我的用户id
	@Select("select cascading_id from tb_user_connected where cascaded_id = #{usrId}")
	@Results({
		@Result(column = "cascading_id" ,property="cascadingId")
	})
	List<Long> findConnectList2(Long usrId);

}

package com.shopx5.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.shopx5.pojo.TbHeatXS;

/**
 * 寻思、原创文章、论题 点赞
 * @author sjx
 * 2019年8月16日 下午5:34:40
 */
public interface TbHeatXSMapper {

	@Insert("insert into tb_heat_xs(user_id,aim_info_id,type) values(#{userId},#{aimInfoId},#{type})")
	void save(TbHeatXS entity);

	
	@Select("select * from tb_heat_xs where user_id=#{0} and aim_info_id=#{1} and type=#{2}")
	@Results({
		@Result(column="user_id",property="userId"),
		@Result(column="aim_info_id",property="aimInfoId"),
		@Result(column="type",property="type")
	})
	TbHeatXS findEntity(String userId, String aimInfoId, String type);


	@Delete("delete from tb_heat_xs where user_id = #{0} and aim_info_id=#{1} and type = #{2}")
	void cancel(String userId, String aimInfoId, String type);

}

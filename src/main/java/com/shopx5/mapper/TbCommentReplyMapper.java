package com.shopx5.mapper;

import com.shopx5.pojo.TbCommentReply;
import com.shopx5.pojo.TbCommentReplyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbCommentReplyMapper {
    int countByExample(TbCommentReplyExample example);

    int deleteByExample(TbCommentReplyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbCommentReply record);

    int insertSelective(TbCommentReply record);

    List<TbCommentReply> selectByExample(TbCommentReplyExample example);

    TbCommentReply selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbCommentReply record, @Param("example") TbCommentReplyExample example);

    int updateByExample(@Param("record") TbCommentReply record, @Param("example") TbCommentReplyExample example);

    int updateByPrimaryKeySelective(TbCommentReply record);

    int updateByPrimaryKey(TbCommentReply record);
}
package com.shopx5.mapper;

import com.shopx5.pojo.SpRoleUser;
import com.shopx5.pojo.SpRoleUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpRoleUserMapper {
    int countByExample(SpRoleUserExample example);

    int deleteByExample(SpRoleUserExample example);

    int insert(SpRoleUser record);

    int insertSelective(SpRoleUser record);

    List<SpRoleUser> selectByExample(SpRoleUserExample example);

    int updateByExampleSelective(@Param("record") SpRoleUser record, @Param("example") SpRoleUserExample example);

    int updateByExample(@Param("record") SpRoleUser record, @Param("example") SpRoleUserExample example);
}
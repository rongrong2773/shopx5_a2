package com.shopx5.mapper;

import com.shopx5.pojo.TbNewsDesc;

/**
 * 
 * @author sjx
 * 2019年6月24日 下午9:00:37
 */
public interface TbNewsDescMapper {

	public TbNewsDesc selectByPrimaryKey(Long id);
	
	// 新增资讯详情实体
	public void insert(TbNewsDesc newsDesc);

	public void updateByPrimaryKey(TbNewsDesc newsDesc);

}

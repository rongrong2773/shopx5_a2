package com.shopx5.mapper;

import com.shopx5.pojo.SpButton;
import com.shopx5.pojo.SpButtonExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpButtonMapper {
    int countByExample(SpButtonExample example);

    int deleteByExample(SpButtonExample example);

    int deleteByPrimaryKey(String buttonId);

    int insert(SpButton record);

    int insertSelective(SpButton record);

    List<SpButton> selectByExample(SpButtonExample example);

    SpButton selectByPrimaryKey(String buttonId);

    int updateByExampleSelective(@Param("record") SpButton record, @Param("example") SpButtonExample example);

    int updateByExample(@Param("record") SpButton record, @Param("example") SpButtonExample example);

    int updateByPrimaryKeySelective(SpButton record);

    int updateByPrimaryKey(SpButton record);
}
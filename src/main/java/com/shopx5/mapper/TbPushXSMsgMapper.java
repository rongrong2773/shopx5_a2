package com.shopx5.mapper;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbPushXSMsg;
import com.shopx5.pojo.TbPushXSMsgExample;

/**
 * @author sjx
 * 2019年8月23日 下午2:23:37
 */
public interface TbPushXSMsgMapper {

	Page<TbPushXSMsg> selectByExample(TbPushXSMsgExample example);

}

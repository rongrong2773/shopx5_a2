package com.shopx5.mapper;

import com.shopx5.pojo.SpUserAddr;
import com.shopx5.pojo.SpUserAddrExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpUserAddrMapper {
    int countByExample(SpUserAddrExample example);

    int deleteByExample(SpUserAddrExample example);

    int deleteByPrimaryKey(String addrId);

    int insert(SpUserAddr record);

    int insertSelective(SpUserAddr record);

    List<SpUserAddr> selectByExample(SpUserAddrExample example);

    SpUserAddr selectByPrimaryKey(String addrId);

    int updateByExampleSelective(@Param("record") SpUserAddr record, @Param("example") SpUserAddrExample example);

    int updateByExample(@Param("record") SpUserAddr record, @Param("example") SpUserAddrExample example);

    int updateByPrimaryKeySelective(SpUserAddr record);

    int updateByPrimaryKey(SpUserAddr record);
}
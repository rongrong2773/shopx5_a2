package com.shopx5.mapper;

import com.shopx5.pojo.SsItemCat;
import com.shopx5.pojo.SsItemCatExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SsItemCatMapper {
    int countByExample(SsItemCatExample example);

    int deleteByExample(SsItemCatExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SsItemCat record);

    int insertSelective(SsItemCat record);

    List<SsItemCat> selectByExample(SsItemCatExample example);

    SsItemCat selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SsItemCat record, @Param("example") SsItemCatExample example);

    int updateByExample(@Param("record") SsItemCat record, @Param("example") SsItemCatExample example);

    int updateByPrimaryKeySelective(SsItemCat record);

    int updateByPrimaryKey(SsItemCat record);
}
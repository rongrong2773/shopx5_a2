package com.shopx5.mapper;

import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.SpUserInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpUserInfoMapper {
    int countByExample(SpUserInfoExample example);

    int deleteByExample(SpUserInfoExample example);

    int deleteByPrimaryKey(String userInfoId);

    int insert(SpUserInfo record);

    int insertSelective(SpUserInfo record);

    List<SpUserInfo> selectByExample(SpUserInfoExample example);

    SpUserInfo selectByPrimaryKey(String userInfoId);

    int updateByExampleSelective(@Param("record") SpUserInfo record, @Param("example") SpUserInfoExample example);

    int updateByExample(@Param("record") SpUserInfo record, @Param("example") SpUserInfoExample example);

    int updateByPrimaryKeySelective(SpUserInfo record);

    int updateByPrimaryKey(SpUserInfo record);
}
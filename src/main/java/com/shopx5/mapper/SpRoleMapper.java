package com.shopx5.mapper;

import com.shopx5.pojo.SpRole;
import com.shopx5.pojo.SpRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpRoleMapper {
    int countByExample(SpRoleExample example);

    int deleteByExample(SpRoleExample example);

    int deleteByPrimaryKey(String roleId);

    int insert(SpRole record);

    int insertSelective(SpRole record);

    List<SpRole> selectByExample(SpRoleExample example);

    SpRole selectByPrimaryKey(String roleId);

    int updateByExampleSelective(@Param("record") SpRole record, @Param("example") SpRoleExample example);

    int updateByExample(@Param("record") SpRole record, @Param("example") SpRoleExample example);

    int updateByPrimaryKeySelective(SpRole record);

    int updateByPrimaryKey(SpRole record);
}
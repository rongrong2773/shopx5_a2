package com.shopx5.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbArticle;
import com.shopx5.pojo.TbArticleExample;

/**
 * @author sjx
 * 2019年7月16日 上午11:41:11
 */
public interface TbArticleMapper {

	
	// 查询原创文章/学富分页列表
	Page<TbArticle> selectByExampleArticle(TbArticleMapper example);

	@Select("select * from tb_article where id = #{0}")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="read_count", property="readCount")
	})
	TbArticle findEntityByPrimaryId(String infoId);

	@Update("update tb_article set read_count = #{readCount} where id = #{id}")
	void updateEntity(TbArticle bean);
	
	@Insert("insert into tb_article_readconn (user_id, info_id) values(#{0}, #{1})")
	void addConnInfo(Long usrId, String infoId);
	
	@Select("select read_count from tb_article where id = #{infoId}")
	@Results({
		@Result(column = "read_count", property="readCount")
	})
	Integer findArtiReadtimes(String infoId);

	@Update("update tb_article set heat_count = #{0} where id = #{1}")
	void updateAricleHeat(Integer heatCount, String articleId);

	Page<TbArticle> findTechTypePage(TbArticleExample example);

	@Insert("insert into tb_article(id, user_id, user_image_url, nick_name, title, content, read_perm,"
			+ "price, read_count, heat_count, shared_count,"
			+ "tech_type,life_type,celebrity_type,film_type,philosophy_type,industry_type,"
			+ "literature_type,internet_type,sports_type,health_type,economics_type,other_type,is_allow_share,is_anon) "
			+ "values (#{id}, #{userId}, #{userImageUrl}, #{nickName},#{title},#{content},#{read_perm},#{price},#{readCount},#{heatCount},#{sharedCount}, "
			+ "#{techType}, #{lifeType}, #{celebrityType}, #{filmType}, #{philosophyType}, #{industryType},#{literatureType},#{internetType},#{sportsType},"
			+ "#{healthType},#{economicsType},#{otherType},#{isAllowShare},#{isAnon})")
	void add(TbArticle bean);

	@Select("select * from tb_article where id = #{id}")
	@Results({
		@Result(id=true, column="id", property="id"),
		@Result(column="user_id", property="userId"),
		@Result(column="title", property="title"),
		@Result(column="content", property="content"),
		@Result(column="read_perm", property="readPerm"),
		@Result(column="price", property="price"),
		@Result(column="read_count", property="readCount"),
		@Result(column="heat_count", property="heatCount"),
		@Result(column="shared_count", property="sharedCount"),
		@Result(column="tech_type", property="techType"),
		@Result(column="lifeType", property="lifeType"),
		@Result(column="celebrity_type", property="celebrityType"),
		@Result(column="film_type", property="filmType"),
		@Result(column="philosophy_type", property="philosophyType"),
		@Result(column="industry_type", property="industryType"),
		@Result(column="literature_type", property="literatureType"),
		@Result(column="internet_type", property="internetType"),
		@Result(column="sports_type", property="sportsType"),
		@Result(column="health_type", property="healthType"),
		@Result(column="economics_type", property="economicsType"),
		@Result(column="other_type", property="otherType"),
		@Result(column="createTime", property="createTime"),
		@Result(column="user_image_url", property="userImageUrl"),
		@Result(column="nick_name", property="nickName"),
		@Result(column="is_allow_share", property="isAllowShare"),
		@Result(column="is_anon", property="isAnon")
		
	})
	TbArticle findOne(String id);

	Page<TbArticle> selectByExample(TbArticleExample example);
	
	@Update("update tb_article set title = #{title}, content = #{content}, user_id = #{userId} where id = #{id}")
	void update(TbArticle bean);
}

package com.shopx5.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbSpeechXS;
import com.shopx5.pojo.TbSpeechXSExample;

/**
 * 论道 -> 发言mapper
 * @author sjx
 * 2019年8月20日 上午11:07:19
 */
public interface TbSpeechXSMapper {

	@Insert("insert into tb_speech_xs (id, user_id, user_image_url, nick_name, suffix, thesis_id, speech_flag, video_url, speech1, speech2, heat_count,at_people)"
			+ "values(#{id}, #{userId}, #{userImageUrl}, #{nickName}, #{suffix}, #{thesisId}, #{speechFlag}, #{videoUrl}, #{speech1}, #{speech2}, #{heatCount},#{atPeople})")
	void saveSpeechEntity(TbSpeechXS entity);

	Page<TbSpeechXS> selectByExample(TbSpeechXSExample example);

	@Select("select id, user_id,user_image_url,nick_name,thesis_id, speech_flag,video_url,speech1,speech2, heat_count, create_time from tb_speech_xs where id = #{0}")
	@Results({
		@Result(id=true,property="id",column="id"),
		@Result(column="user_id",property="userId"),
		@Result(column="user_image_url",property="userImageUrl"),
		@Result(column="nick_name",property="nickName"),
		@Result(column="thesis_id",property="thesisId"),
		@Result(column="speech_flag",property="speechFlag"),
		@Result(column="video_url",property="videoUrl"),
		@Result(column="speech1",property="speech1"),
		@Result(column="speech2",property="speech2"),
		@Result(column="heat_count",property="heatCount"),
		@Result(column="create_time",property="createTime")
	})
	TbSpeechXS findSpeechEntity(String aimInfoId);

	@Update("update tb_speech_xs set heat_count = #{0} where id = #{1}")
	void updateHeatCountSpeech(Integer heatCount, String aimInfoId);

	@Select("select user_id from tb_speech_xs where thesis_id = #{0}")
	List<String> findUserListInSpeechByThesisId(String thesisId);

}

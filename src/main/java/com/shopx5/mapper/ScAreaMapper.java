package com.shopx5.mapper;

import com.shopx5.pojo.ScArea;
import com.shopx5.pojo.ScAreaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ScAreaMapper {
    int countByExample(ScAreaExample example);

    int deleteByExample(ScAreaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ScArea record);

    int insertSelective(ScArea record);

    List<ScArea> selectByExample(ScAreaExample example);

    ScArea selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ScArea record, @Param("example") ScAreaExample example);

    int updateByExample(@Param("record") ScArea record, @Param("example") ScAreaExample example);

    int updateByPrimaryKeySelective(ScArea record);

    int updateByPrimaryKey(ScArea record);
}
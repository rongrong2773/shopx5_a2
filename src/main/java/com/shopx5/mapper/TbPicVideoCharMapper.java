package com.shopx5.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbArticle;
import com.shopx5.pojo.TbCollectXS;
import com.shopx5.pojo.TbPicCharVideoExample;
import com.shopx5.pojo.TbPicVideoChar;
import com.shopx5.pojo.TbPicVideoCharConnected;
/**
 * @author sjx
 * 2019年7月4日 下午2:17:27
 */
public interface TbPicVideoCharMapper {

	
	@Insert("insert into tb_pic_video_char(id,user_id,user_suffix,user_image_url,user_nick_name,title,content,url,is_anon,"
			+ "heat_count,read_times,collect_times,comments_count) values "
			+ "(#{id},#{userId},#{userSuffix},#{userImageUrl},#{userNickName},#{title},#{content},#{url},#{isAnon},"
			+ "#{heatCount}, #{readTimes},#{collectTimes}, #{commentsCount})")
	void save(TbPicVideoChar tbPicChar);

	@Insert("insert into tb_article(id,user_id,user_image_url,nick_name,title,content,read_perm,is_allow_share,is_anon,price,read_count,heat_count,shared_count,"
			+ "tech_type,life_type,celebrity_type,film_type,philosophy_type,industry_type,literature_type,internet_type,"
			+ "sports_type,health_type,economics_type,other_type)"
			+ " values(#{id},#{userId},#{userImageUrl},#{nickName},#{title},#{content},#{readPerm},#{isAllowShare},#{isAnon},#{price},#{readCount},"
			+ "#{heatCount},#{sharedCount},"
			+ "#{techType},#{lifeType},#{celebrityType},#{filmType},#{philosophyType},#{industryType},#{literatureType},"
			+ "#{internetType},#{sportsType},#{healthType},#{economicsType},#{otherType})")
	void saveArticle(TbArticle tbArticle);

	// 按时间递减查出图文/视频分页信息
	Page<TbPicVideoChar> selectByExample(@Param("usrId") String usrId,@Param("example") TbPicCharVideoExample example);

	TbPicVideoChar findEntityByPrimaryId(String id);
	
	@Update("update tb_pic_video_char set heat_count=#{0} where id=#{1}")
	void updateHeatCount(Integer heatCount,String pvcId);

	
	@Select("select * from tb_article where id=#{id}")
	@Results({
		@Result(id=true,column="id",property="id"),
		@Result(column="user_id",property="userId"),
		@Result(column="user_image_url",property="userImageUrl"),
		@Result(column="nick_name",property="nickName"),
		@Result(column="title",property="title"),
		@Result(column="content",property="content"),
		@Result(column="read_perm",property="readPerm"),
		@Result(column="price",property="price"),
		@Result(column="read_count",property="readCount"),
		@Result(column="heat_count",property="heatCount"),
		@Result(column="shared_count",property="sharedCount"),
		
		@Result(column="tech_type",property="techType"),
		@Result(column="life_type",property="lifeType"),
		@Result(column="celebrity_type",property="celebrityType"),
		@Result(column="film_type",property="filmType"),
		@Result(column="philosophy_type",property="philosophyType"),
		@Result(column="industry_type",property="industryType"),
		@Result(column="literature_type",property="literatureType"),
		@Result(column="internet_type",property="internetType"),
		@Result(column="sports_type",property="sportsType"),
		@Result(column="health_type",property="healthType"),
		@Result(column="economics_type",property="economicsType"),
		@Result(column="other_type",property="otherType"),
		@Result(column="create_time",property="createTime")
		})
	TbArticle findArtiEntityByPrimaryId(String id);


	
	@Update("update tb_pic_video_char set read_times = #{readTimes} where id = #{id}")
	void updateEntity(TbPicVideoChar bean);

	
	@Select("select read_times from tb_pic_video_char where id = #{infoId}")
	@Results({
		@Result(column="read_times", property="readTimes")
	})
	Integer findTwvReadtimes(String infoId);

	@Insert("insert into tb_collect_xs (user_id, aim_id, aim_type) values(#{0}, #{1}, #{2})")
	void addCollect(String usrId, String aimId, String aimType);
	
	
	@Select("select * from tb_collect_xs where user_id = #{usrId} and aim_id =#{aimId}")
	@Results({
		@Result(column="user_id", property="userId"),
		@Result(column="aim_id", property="aimId"),
		@Result(column="aim_type", property="aimType")
	})
	TbCollectXS findIsCollected(@Param("usrId") String usrId,@Param("aimId") String aimId);

	@Delete("delete from tb_collect_xs where user_id = #{usrId} and aim_id = #{aimId}")
	void cancelCollect(@Param("usrId") String usrId, @Param("aimId") String aimId);

	@Delete("delete from tb_pic_video_char where user_id = #{0} and id = #{1}")
	void delTwvById(String usrId, String twvId);

	@Insert("insert into tb_pic_video_char_connected (user_id, pcv_id, is_pic_char_or_video, op_type) values"
			+ "(#{usrId},#{pcvId},#{isPicCharOrVideo},#{opType})")
	void save2(TbPicVideoCharConnected bean2);

	@Select("select * from tb_pic_video_char_connected where user_id=#{0} and pcv_id=#{1}")
	@Results({
		@Result(column="user_id", property="usrId"),
		@Result(column="pcv_id", property="pcvId"),
		@Result(column="is_pic_char_or_video", property="isPicCharOrVideo"),
		@Result(column="op_type", property="opType"),
		@Result(column="create_time", property="createTime")
	})
	TbPicVideoCharConnected findEntityByUsrIdPcvId(String usrId,String pcvId);

	
	@Delete("delete from tb_pic_video_char_connected where user_id = #{0} and pcv_id = #{1}")
	void delConnEntity(String usrId, String pcvId);

	@Select("select count(id) from tb_pic_video_char where user_id = #{userId}")
	Integer findPicCharCountById(String userId);

	@Select("select count(id) from tb_thesis where mobile_user_id = #{userId}")
	Integer findArtiCountById(String userId);
	
	@Select("select * from tb_pic_video_char where user_id = #{0}")
	List<TbPicVideoChar> findListByHeatById(String usrId);

	@Update("update tb_pic_video_char set collect_times = #{0} where id = {1}")
	void updateCollectHeat(Integer collectCount, String pvcId);

	Page<TbPicVideoChar> findTechTypePage(@Param("example") TbPicCharVideoExample example);
	
}

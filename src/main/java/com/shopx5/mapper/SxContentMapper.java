package com.shopx5.mapper;

import com.shopx5.pojo.SxContent;
import com.shopx5.pojo.SxContentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SxContentMapper {
    int countByExample(SxContentExample example);

    int deleteByExample(SxContentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SxContent record);

    int insertSelective(SxContent record);

    List<SxContent> selectByExampleWithBLOBs(SxContentExample example);

    List<SxContent> selectByExample(SxContentExample example);

    SxContent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SxContent record, @Param("example") SxContentExample example);

    int updateByExampleWithBLOBs(@Param("record") SxContent record, @Param("example") SxContentExample example);

    int updateByExample(@Param("record") SxContent record, @Param("example") SxContentExample example);

    int updateByPrimaryKeySelective(SxContent record);

    int updateByPrimaryKeyWithBLOBs(SxContent record);

    int updateByPrimaryKey(SxContent record);
}
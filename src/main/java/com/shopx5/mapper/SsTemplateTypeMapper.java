package com.shopx5.mapper;

import com.shopx5.pojo.SsTemplateType;
import com.shopx5.pojo.SsTemplateTypeExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SsTemplateTypeMapper {
    int countByExample(SsTemplateTypeExample example);

    int deleteByExample(SsTemplateTypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SsTemplateType record);

    int insertSelective(SsTemplateType record);

    List<SsTemplateType> selectByExample(SsTemplateTypeExample example);

    SsTemplateType selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SsTemplateType record, @Param("example") SsTemplateTypeExample example);

    int updateByExample(@Param("record") SsTemplateType record, @Param("example") SsTemplateTypeExample example);

    int updateByPrimaryKeySelective(SsTemplateType record);

    int updateByPrimaryKey(SsTemplateType record);
    
    List<Map> selectOptionList();
}
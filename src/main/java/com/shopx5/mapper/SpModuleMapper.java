package com.shopx5.mapper;

import com.shopx5.pojo.SpModule;
import com.shopx5.pojo.SpModuleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SpModuleMapper {
    int countByExample(SpModuleExample example);

    int deleteByExample(SpModuleExample example);

    int deleteByPrimaryKey(Integer moduleId);

    int insert(SpModule record);

    int insertSelective(SpModule record);

    List<SpModule> selectByExample(SpModuleExample example);

    SpModule selectByPrimaryKey(Integer moduleId);

    int updateByExampleSelective(@Param("record") SpModule record, @Param("example") SpModuleExample example);

    int updateByExample(@Param("record") SpModule record, @Param("example") SpModuleExample example);

    int updateByPrimaryKeySelective(SpModule record);

    int updateByPrimaryKey(SpModule record);
}
package com.shopx5.mapper;

import com.shopx5.pojo.SsBrand;
import com.shopx5.pojo.SsBrandExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SsBrandMapper {
    int countByExample(SsBrandExample example);

    int deleteByExample(SsBrandExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SsBrand record);

    int insertSelective(SsBrand record);

    List<SsBrand> selectByExample(SsBrandExample example);

    SsBrand selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SsBrand record, @Param("example") SsBrandExample example);

    int updateByExample(@Param("record") SsBrand record, @Param("example") SsBrandExample example);

    int updateByPrimaryKeySelective(SsBrand record);

    int updateByPrimaryKey(SsBrand record);
    
    List<Map> selectOptionList();
}
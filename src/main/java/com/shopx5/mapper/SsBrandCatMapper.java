package com.shopx5.mapper;

import com.shopx5.pojo.SsBrandCat;
import com.shopx5.pojo.SsBrandCatExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SsBrandCatMapper {
    int countByExample(SsBrandCatExample example);

    int deleteByExample(SsBrandCatExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SsBrandCat record);

    int insertSelective(SsBrandCat record);

    List<SsBrandCat> selectByExample(SsBrandCatExample example);

    SsBrandCat selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SsBrandCat record, @Param("example") SsBrandCatExample example);

    int updateByExample(@Param("record") SsBrandCat record, @Param("example") SsBrandCatExample example);

    int updateByPrimaryKeySelective(SsBrandCat record);

    int updateByPrimaryKey(SsBrandCat record);
    
    List<Map> selectOptionList();
}
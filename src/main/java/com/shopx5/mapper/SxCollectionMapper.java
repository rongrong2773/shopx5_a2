package com.shopx5.mapper;

import com.shopx5.pojo.SxCollection;
import com.shopx5.pojo.SxCollectionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SxCollectionMapper {
    int countByExample(SxCollectionExample example);

    int deleteByExample(SxCollectionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SxCollection record);

    int insertSelective(SxCollection record);

    List<SxCollection> selectByExample(SxCollectionExample example);

    SxCollection selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SxCollection record, @Param("example") SxCollectionExample example);

    int updateByExample(@Param("record") SxCollection record, @Param("example") SxCollectionExample example);

    int updateByPrimaryKeySelective(SxCollection record);

    int updateByPrimaryKey(SxCollection record);
}
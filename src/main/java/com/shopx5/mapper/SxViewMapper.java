package com.shopx5.mapper;

import com.shopx5.pojo.SxView;
import com.shopx5.pojo.SxViewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SxViewMapper {
    int countByExample(SxViewExample example);

    int deleteByExample(SxViewExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SxView record);

    int insertSelective(SxView record);

    List<SxView> selectByExample(SxViewExample example);

    SxView selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SxView record, @Param("example") SxViewExample example);

    int updateByExample(@Param("record") SxView record, @Param("example") SxViewExample example);

    int updateByPrimaryKeySelective(SxView record);

    int updateByPrimaryKey(SxView record);
}
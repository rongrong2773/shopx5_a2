package com.shopx5.mapper;

import com.github.pagehelper.Page;
import com.shopx5.pojo.TbMobileUserMsg;
import com.shopx5.pojo.TbMobileUserMsgExample;
/**
 * @author sjx
 * 2019年8月5日 下午2:56:12
 */
public interface TbMobileUserMsgMapper {

	// 查询"个人中心 - 我的消息"
	Page<TbMobileUserMsg> selectMsgByExample(TbMobileUserMsgExample example);
}

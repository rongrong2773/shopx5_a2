package com.shopx5.mapper;

import org.apache.ibatis.annotations.Insert;

/**
 * @author sjx
 * 2019年7月17日 上午10:47:18
 */
public interface TbPicVideoCharReadedInfoMapper {

	@Insert("insert into tb_pvc_readconn (user_id, info_id) values(#{0}, #{1})")
	void addConnInfo(Long usrId, String infoId);

}

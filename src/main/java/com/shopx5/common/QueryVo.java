package com.shopx5.common;
import java.io.Serializable;
import java.util.List;

import com.shopx5.pojo.ScLog;
/**
 * @author XS 多商户商城
 */
public class QueryVo implements Serializable{	
	private List<Integer> ids;
	private ScLog row; // 添加,修改 datagrid json对象封装
	
	public List<Integer> getIds() {
		return ids;
	}
	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}
	public ScLog getRow() {
		return row;
	}
	public void setRow(ScLog row) {
		this.row = row;
	}
}

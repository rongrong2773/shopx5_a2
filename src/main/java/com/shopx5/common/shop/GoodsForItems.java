package com.shopx5.common.shop;

import java.io.Serializable;
import java.util.List;

import com.shopx5.pojo.TbItem;
/**
 * @author tang S h op X 5多商 户商城
 * @qq 1503501970
 */
public class GoodsForItems implements Serializable {
	private List<TbItem> itemList;

	public List<TbItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<TbItem> itemList) {
		this.itemList = itemList;
	}
}

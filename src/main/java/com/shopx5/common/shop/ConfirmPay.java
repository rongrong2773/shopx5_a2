package com.shopx5.common.shop;

import java.io.Serializable;
/**
 * @author tang S ho p X5 多商 户商城
 * @qq 1503501970
 */
public class ConfirmPay implements Serializable {
	private String outTradeNo;
	private String orderId;
	private String paymentCode;

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
}

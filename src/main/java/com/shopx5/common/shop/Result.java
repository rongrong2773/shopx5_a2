package com.shopx5.common.shop;

import java.io.Serializable;
import java.util.List;

/**
 * 用于向页面传递信息的类
 * XS 多用户商城
 * @author sjx
 * 2019年6月26日 下午10:45:07
 */
public class Result implements Serializable{
	private boolean success;
	private boolean flag;
	private String message;
	private List list;
	
	public Result(boolean success, String message) {
		super();
		this.success=success;
		this.flag = success;
		this.message = message;
	}
	public Result(boolean success, List list) {
		super();
		this.success=success;
		this.flag = success;
		this.list = list;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
}

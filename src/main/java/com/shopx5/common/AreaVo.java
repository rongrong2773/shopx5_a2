package com.shopx5.common;

import java.io.Serializable;
import java.util.List;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class AreaVo implements Serializable {
	private Integer id;
    private Integer parentId;
    private String name;
    private String shortName;
    private Integer lev;
    private Integer code;
    
    private List<AreaVo> provinceList; //省  "省"内包含List<市> 
    private List<AreaVo> cityList;     //市  "市"内包含List<区>
    private List<AreaVo> areaList;     //区    
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public Integer getLev() {
		return lev;
	}
	public void setLev(Integer lev) {
		this.lev = lev;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public List<AreaVo> getProvinceList() {
		return provinceList;
	}
	public void setProvinceList(List<AreaVo> provinceList) {
		this.provinceList = provinceList;
	}
	public List<AreaVo> getCityList() {
		return cityList;
	}
	public void setCityList(List<AreaVo> cityList) {
		this.cityList = cityList;
	}
	public List<AreaVo> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<AreaVo> areaList) {
		this.areaList = areaList;
	}
}

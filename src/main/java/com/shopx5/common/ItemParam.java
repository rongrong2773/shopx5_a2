package com.shopx5.common;

import java.io.Serializable;
import java.util.List;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class ItemParam implements Serializable {
	private String group;
	private List<ItemParamData> params;
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public List<ItemParamData> getParams() {
		return params;
	}
	public void setParams(List<ItemParamData> params) {
		this.params = params;
	}
}

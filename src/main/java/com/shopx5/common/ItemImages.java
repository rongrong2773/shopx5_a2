package com.shopx5.common;

import java.io.Serializable;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class ItemImages implements Serializable {
	private String _small;
	private String _mid;
	private String _big;
	
	public String get_small() {
		return _small;
	}
	public void set_small(String _small) {
		this._small = _small;
	}
	public String get_mid() {
		return _mid;
	}
	public void set_mid(String _mid) {
		this._mid = _mid;
	}
	public String get_big() {
		return _big;
	}
	public void set_big(String _big) {
		this._big = _big;
	}
}

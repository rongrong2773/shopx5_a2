package com.shopx5.common;

import java.io.Serializable;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class ItemParamData implements Serializable {
	private String k;
	private String v;
	
	public String getK() {
		return k;
	}
	public void setK(String k) {
		this.k = k;
	}
	public String getV() {
		return v;
	}
	public void setV(String v) {
		this.v = v;
	}	
}

package com.shopx5.common;

import java.io.Serializable;
/**
 * @author t an g sh o p X5 多 商户 商城
 * @qq 15 035 019 70
 */
public class AmChartsNode implements Serializable {
	private String country;
	private String visits;
	private String color;
	
	private String title;
	private String value;
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getVisits() {
		return visits;
	}
	public void setVisits(String visits) {
		this.visits = visits;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}

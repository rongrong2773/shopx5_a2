package com.shopx5.common;

import java.io.Serializable;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class OrderParam implements Serializable {
	private String  userId;
	private Integer state_type = 0;	
	private Integer page = 1;
	
	private String keyword;
	private String query_start_date;
	private String query_end_date;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getState_type() {
		return state_type;
	}
	public void setState_type(Integer state_type) {
		this.state_type = state_type;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getQuery_start_date() {
		return query_start_date;
	}
	public void setQuery_start_date(String query_start_date) {
		this.query_start_date = query_start_date;
	}
	public String getQuery_end_date() {
		return query_end_date;
	}
	public void setQuery_end_date(String query_end_date) {
		this.query_end_date = query_end_date;
	}
}

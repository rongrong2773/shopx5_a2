package com.shopx5.common;

import java.io.Serializable;

import com.shopx5.pojo.SpUser;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class EUser extends SpUser implements Serializable {
	public EUser(SpUser spUser) {
		//初始化属性	    
		this.setUserId(spUser.getUserId());
		this.setDeptId(spUser.getDeptId());
		this.setUserName(spUser.getUserName());
		this.setPassword(spUser.getPassword());
		this.setState(spUser.getState());
		this.setCreateBy(spUser.getCreateBy());
		this.setCreateDept(spUser.getCreateDept());
		this.setCreateTime(spUser.getCreateTime());
		this.setUpdateBy(spUser.getUpdateBy());
		this.setUpdateTime(spUser.getUpdateTime());
	}

    private String deptName;
    
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}    
}

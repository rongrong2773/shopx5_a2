package com.shopx5.common.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.shopx5.pojo.ScArea;
/**
 * @author tang Sh o p X 5 多商户商城
 * @qq 1503501970
 */
public class TreeGridNode extends ScArea implements Serializable{
	public TreeGridNode(ScArea scArea) {
		//初始化属性
		this.setId(scArea.getId());
		this.setParentId(scArea.getId());
		this.setName(scArea.getName());
		this.setShortName(scArea.getShortName());
		this.setLev(scArea.getLev());
		this.setCode(scArea.getCode());
	}
	private Integer aid;
	private String  aname;
	private Date    date;
	private List<TreeGridNode> children;
	
	public Integer getAid() {
		return aid;
	}
	public void setAid(Integer aid) {
		this.aid = aid;
	}
	public String getAname() {
		return aname;
	}
	public void setAname(String aname) {
		this.aname = aname;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<TreeGridNode> getChildren() {
		return children;
	}
	public void setChildren(List<TreeGridNode> children) {
		this.children = children;
	}
}

package com.shopx5.common.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.shopx5.pojo.SpModule;
/**
 * @author tang S h o p X 5 多商 户 商城
 * @qq 1503501970
 */
public class TreeGridModule implements Serializable{
	public TreeGridModule(SpModule spModule) {
		//初始化属性
		this.setModuleId(spModule.getModuleId());
		this.setName(spModule.getName());
		this.setParentId(spModule.getParentId());
		this.setParentName(spModule.getParentName());
		this.setCurl(spModule.getCurl());
		this.setCtype(spModule.getCtype());
		this.setLayerNum(spModule.getLayerNum());
		this.setOrderNo(spModule.getOrderNo());
		this.setRemark(spModule.getRemark());
		this.setUpdateTime(spModule.getUpdateTime());
	}
	private Integer moduleId; //*
	private String name;      //*
    private Integer parentId;
    private String parentName;
    private String curl;
    private Integer ctype;
    private Integer layerNum;
    private Integer orderNo;
    private String remark;
    private Date updateTime;
    
	private String state;     //*
	private List<TreeGridModule> children; //#
	private String iconCls;
	
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public String getCurl() {
		return curl;
	}
	public void setCurl(String curl) {
		this.curl = curl;
	}
	public Integer getCtype() {
		return ctype;
	}
	public void setCtype(Integer ctype) {
		this.ctype = ctype;
	}
	public Integer getLayerNum() {
		return layerNum;
	}
	public void setLayerNum(Integer layerNum) {
		this.layerNum = layerNum;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<TreeGridModule> getChildren() {
		return children;
	}
	public void setChildren(List<TreeGridModule> children) {
		this.children = children;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
}

package com.shopx5.common.pojo;

import java.io.Serializable;

import com.shopx5.pojo.SxContent;
/**
 * @author tang S h o p X5多商户商城
 * @qq 1503501970
 */
public class Content extends SxContent implements Serializable {
	public Content(SxContent sxContent) {
		//初始化属性
		this.setId(sxContent.getId());
		this.setCategoryId(sxContent.getCategoryId());
		this.setTitle(sxContent.getTitle());
		this.setSubTitle(sxContent.getSubTitle());
		this.setTitleDesc(sxContent.getTitleDesc());
		this.setUrl(sxContent.getUrl());
		this.setPic(sxContent.getPic());
		this.setPic2(sxContent.getPic2());
		this.setCreated(sxContent.getCreated());
		this.setUpdated(sxContent.getUpdated());
	}
	
    private String categoryName;
    
	@Override
	public String toString() {
		return "Content [categoryName=" + categoryName + "]";
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}

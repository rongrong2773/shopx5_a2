package com.shopx5.common.pojo;

import java.io.Serializable;
import java.util.List;

import com.shopx5.pojo.ScArea;
/**
 * @author tang S hop X 5 多商户商城
 * @qq 1503501970
 */
public class TreeGridArea extends ScArea implements Serializable{
	public TreeGridArea(ScArea scArea) {
		//初始化属性
		this.setId(scArea.getId());     //*
		this.setParentId(scArea.getParentId());
		this.setName(scArea.getName()); //*
		this.setShortName(scArea.getShortName());
		this.setLev(scArea.getLev());
		this.setCode(scArea.getCode());
	}
	
	private String state; //*
	private List<TreeGridArea> children; //#
	private String iconCls;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<TreeGridArea> getChildren() {
		return children;
	}
	public void setChildren(List<TreeGridArea> children) {
		this.children = children;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
}

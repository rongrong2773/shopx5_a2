package com.shopx5.common.pojo;

import java.io.Serializable;
/**
 * @author tang S h op X 5 多商户商城
 * @qq 1503501970
 */
public class CheckUser implements Serializable {
	private String userName;
	private String email;
	private String captcha;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCaptcha() {
		return captcha;
	}
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
}

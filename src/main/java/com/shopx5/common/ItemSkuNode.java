package com.shopx5.common;

import java.util.List;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class ItemSkuNode {
	private List<String> colors;
	private String sign;
	private String url;
	
	public List<String> getColors() {
		return colors;
	}
	public void setColors(List<String> colorList) {
		this.colors = colorList;
	}	
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}

package com.shopx5.common;

import java.io.Serializable;
import java.util.List;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class EasyUITreeNode implements Serializable{
	private long id;      //*
	private long pid;
	private String text;  //*
	private String state; //*
	
	private List<EasyUITreeNode> children; //#
	
	private String curl;  //#
	private String iconCls;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}	
	public long getPid() {
		return pid;
	}
	public void setPid(long pid) {
		this.pid = pid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<EasyUITreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<EasyUITreeNode> children) {
		this.children = children;
	}
	public String getCurl() {
		return curl;
	}
	public void setCurl(String curl) {
		this.curl = curl;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
}

package com.shopx5.common;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class ResultPay {
	private boolean success;
	private String msg;
	
	public ResultPay(){
	}
	public ResultPay(boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}

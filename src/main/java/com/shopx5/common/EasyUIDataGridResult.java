package com.shopx5.common;

import java.io.Serializable;
import java.util.List;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class EasyUIDataGridResult<T> implements Serializable{
	private long total;
	private List<T> rows;
	
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
}

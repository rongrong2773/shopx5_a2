package com.shopx5.common;

import java.io.Serializable;
import java.util.List;
/**
 * @author tang S h o p X 5 多商户商城
 * @qq 1503501970
 */
public class PageBean<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<T> list;       //                 *
	private Integer currPage;   //当前页码    参数 *
	private Integer pageSize;   //每页显示数  常量 *
	private Integer totalCount; //总条数           *
	private Integer totalPage;  //总页数      计算 *
	private Integer currSize;   //当前页_条数

	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public Integer getCurrPage() {
		return currPage;
	}
	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getTotalPage() {
		//总页数计算
		if (totalPage == null) {
			return (int)Math.ceil(totalCount*1.0/pageSize);
		}
		return totalPage;
	}
	//  总页数计算2
	//	long recordCount = result.getRecordCount();     // 总条数       solr
	//	long pages =  recordCount / SEARCH_RESULT_ROWS; // 总条数/条数
	//	if (recordCount % SEARCH_RESULT_ROWS > 0) {     // 总条数%条数  >0时,页数+1,否则不变
	//		pages++;
	//	}
	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	public Integer getCurrSize() {
		return currSize;
	}
	public void setCurrSize(Integer currSize) {
		this.currSize = currSize;
	}
}

package com.shopx5.jedis;

import java.util.List;

/**
 * @author XS多商户商城系统
 */
public interface JedisClient {
	String set(String key, String value);
	String get(String key);
	Long del(String key);
	Boolean exists(String key);
	Long expire(String key, int seconds);
	Long ttl(String key);
	Long incr(String key);
	Long hset(String key, String field, String value);
	String hget(String key, String field);
	Long hdel(String key, String... field);
	
	// 用Byte数据结构保存
	public void hSetByte(String key, Object object);
	
	// 读取Byte数据
	public byte[] hGetByte(String key);
	
}

package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SpModule implements Serializable{

	private Integer moduleId;
    private Integer parentId;
    private String parentName;
    private String name;
    private Integer layerNum;
    private Integer isLeaf;
    private String ico;
    private String cpermission;
    private String curl;
    private Integer ctype;
    private Integer state;
    private String belong;
    private String cwhich;
    private Integer quoteNum;
    private String remark;
    private Integer orderNo;
    private String createBy;
    private String createDept;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
    
    private String target;              // mainFrame, treeFrame 
    private List<SpModule> subModule;   // 子导航
    private List<SpModule> children;
	private boolean roleModule = false; // 重构
    
    
    @Override
	public String toString() {
		return "SpModule [moduleId=" + moduleId + ", parentId=" + parentId + ", parentName=" + parentName + ", name="
				+ name + ", layerNum=" + layerNum + ", isLeaf=" + isLeaf + ", ico=" + ico + ", cpermission="
				+ cpermission + ", curl=" + curl + ", ctype=" + ctype + ", state=" + state + ", belong=" + belong
				+ ", cwhich=" + cwhich + ", quoteNum=" + quoteNum + ", remark=" + remark + ", orderNo=" + orderNo
				+ ", createBy=" + createBy + ", createDept=" + createDept + ", createTime=" + createTime + ", updateBy="
				+ updateBy + ", updateTime=" + updateTime + ", target=" + target + ", subModule=" + subModule
				+ ", children=" + children + ", roleModule=" + roleModule + "]";
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public List<SpModule> getSubModule() {
		return subModule;
	}

	public void setSubModule(List<SpModule> subModule) {
		this.subModule = subModule;
	}

	public List<SpModule> getChildren() {
		return children;
	}

	public void setChildren(List<SpModule> children) {
		this.children = children;
	}

	public boolean isRoleModule() {
		return roleModule;
	}

	public void setRoleModule(boolean roleModule) {
		this.roleModule = roleModule;
	}

	public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName == null ? null : parentName.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getLayerNum() {
        return layerNum;
    }

    public void setLayerNum(Integer layerNum) {
        this.layerNum = layerNum;
    }

    public Integer getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Integer isLeaf) {
        this.isLeaf = isLeaf;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico == null ? null : ico.trim();
    }

    public String getCpermission() {
        return cpermission;
    }

    public void setCpermission(String cpermission) {
        this.cpermission = cpermission == null ? null : cpermission.trim();
    }

    public String getCurl() {
        return curl;
    }

    public void setCurl(String curl) {
        this.curl = curl == null ? null : curl.trim();
    }

    public Integer getCtype() {
        return ctype;
    }

    public void setCtype(Integer ctype) {
        this.ctype = ctype;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getBelong() {
        return belong;
    }

    public void setBelong(String belong) {
        this.belong = belong == null ? null : belong.trim();
    }

    public String getCwhich() {
        return cwhich;
    }

    public void setCwhich(String cwhich) {
        this.cwhich = cwhich == null ? null : cwhich.trim();
    }

    public Integer getQuoteNum() {
        return quoteNum;
    }

    public void setQuoteNum(Integer quoteNum) {
        this.quoteNum = quoteNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public String getCreateDept() {
        return createDept;
    }

    public void setCreateDept(String createDept) {
        this.createDept = createDept == null ? null : createDept.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
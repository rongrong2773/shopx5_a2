package com.shopx5.pojo.result;

import java.io.Serializable;
import java.util.List;

public class ResultSearch implements Serializable{
	private List<ItemSearch> itemList;
	private Integer currPage;    //当前页  参
	private long recordCount;    //总条数
	private long totalPage;      //总页数

	public long getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(long totalPage) {
		this.totalPage = totalPage;
	}
	public long getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(long recordCount) {
		this.recordCount = recordCount;
	}
	public Integer getCurrPage() {
		return currPage;
	}
	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}
	public List<ItemSearch> getItemList() {
		return itemList;
	}
	public void setItemList(List<ItemSearch> itemList) {
		this.itemList = itemList;
	}
}

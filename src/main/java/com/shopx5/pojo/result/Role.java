package com.shopx5.pojo.result;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Role implements Serializable{
	private String roleId;
    private String name;
    private String parentId;
    private String addQx;
    private String delQx;
    private String editQx;
    private String seeQx;
    private String remark;
    private Integer orderNo;
    private String createBy;
    private String createDept;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
    private Integer category;
    
    private List<Module> moduleList; //一对多
    private List<Button> buttonList; //一对多
    
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getAddQx() {
		return addQx;
	}
	public void setAddQx(String addQx) {
		this.addQx = addQx;
	}
	public String getDelQx() {
		return delQx;
	}
	public void setDelQx(String delQx) {
		this.delQx = delQx;
	}
	public String getEditQx() {
		return editQx;
	}
	public void setEditQx(String editQx) {
		this.editQx = editQx;
	}
	public String getSeeQx() {
		return seeQx;
	}
	public void setSeeQx(String seeQx) {
		this.seeQx = seeQx;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getCreateDept() {
		return createDept;
	}
	public void setCreateDept(String createDept) {
		this.createDept = createDept;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public List<Module> getModuleList() {
		return moduleList;
	}
	public void setModuleList(List<Module> moduleList) {
		this.moduleList = moduleList;
	}
	public List<Button> getButtonList() {
		return buttonList;
	}
	public void setButtonList(List<Button> buttonList) {
		this.buttonList = buttonList;
	}
}

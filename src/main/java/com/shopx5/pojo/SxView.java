package com.shopx5.pojo;

import java.io.Serializable;

public class SxView implements Serializable{
	
    private Long id;
    private String userId;
    private Long itemId;
    
    @Override
	public String toString() {
		return "SxView [id=" + id + ", userId=" + userId + ", itemId=" + itemId + "]";
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
}
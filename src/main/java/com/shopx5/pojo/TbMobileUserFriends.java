package com.shopx5.pojo;

/**
 * 用户 - 用户好友中间表
 * @author sjx
 * 2019年6月29日 上午10:40:09
 */
public class TbMobileUserFriends {

	private Long ownerId;    // 登录用户id
	private Long friendId;    // 好友id
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Long getFriendId() {
		return friendId;
	}
	public void setFriendId(Long friendId) {
		this.friendId = friendId;
	}
	@Override
	public String toString() {
		return "TbMobileUserFriends [ownerId=" + ownerId + ", friendId=" + friendId + "]";
	}

}

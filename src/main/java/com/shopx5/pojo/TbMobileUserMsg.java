package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 个人中心 - 消息内容实体
 * @author sjx
 * 2019年8月5日 上午10:42:27
 */
@SuppressWarnings("serial")
public class TbMobileUserMsg implements Serializable{

	private String id;
	private String userId;
	// 消息类型。 heatType - 点赞消息;  sysType - 系统消息;  concernType - 关注消息;
	// commentType - 评论消息;  replyType - 回复类型 
	private String msgType;
	private String msgContent;
	private Date createTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbMobileUserMsg [id=" + id + ", userId=" + userId + ", msgType=" + msgType + ", msgContent="
				+ msgContent + ", createTime=" + createTime + "]";
	}
}

package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户关注信息实体
 * @author sjx
 * 2019年7月1日 下午2:28:13
 */
@SuppressWarnings("serial")
public class TbUserConnected implements Serializable{

	private String cascadingId;    // 用户id
	private String cascadedId;    // 被关注的人用户id
	private Date createTime;

	public String getCascadingId() {
		return cascadingId;
	}


	public void setCascadingId(String cascadingId) {
		this.cascadingId = cascadingId;
	}


	public String getCascadedId() {
		return cascadedId;
	}


	public void setCascadedId(String cascadedId) {
		this.cascadedId = cascadedId;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	@Override
	public String toString() {
		return "TbUserConnected [cascadingId=" + cascadingId + ", cascadedId=" + cascadedId + ", createTime="
				+ createTime + "]";
	}
	
}

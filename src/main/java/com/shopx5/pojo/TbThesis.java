package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 论题
 * @author sjx
 * 2019年6月29日 上午9:52:49
 */
@SuppressWarnings("serial")
public class TbThesis implements Serializable{

	private String id;
	private String userId;
	private String title;    // 标题
	private String url;    // 图片地址
	private String background;    // 论题背景
	private String isConnTwv;    // 是否从图文/视频的新思路产生    sql默认值为no
	private String connTwvId = "";    // 寻思id
	/**
	 * 论题类型： 共同探讨 - 0/两方辩论 - 1
	 * 共同探讨表示所有人都可以发表评论;
	 * 两方辩论只能两方之间评论
	 */
	private String thesisType;
	private Integer heatCount;    // 热度/点赞数目    sql默认值为0
	private Integer sharedTimes;    // 分享次数    sql默认值为0
	private Integer commentCount;    // 发言数    sql默认值为0
	private Integer leftCount;    // 正方票数    sql默认值为0
	private Integer rightCount;    // 反方票数    sql默认值为0
	private Integer collectCount;    // 收藏次数    sql默认值为0
	
	private String leftOpinion;    // 正方观点
	private String rightOpinion;    // 反方观点
	
	private String isAnon;    // 是否为匿名发布, yes - 是; no - 否   sql默认值为no
	private String mobileUserId;    // 用户id
	
	
	/**
	 * yes 表示有该类型被选中, no表示未选中
	 */
	private String techType = "";    // 科技
	private String environmentType = "";    // 环保
	private String filmType = "";    // 影视
	private String celebrityType = "";    // 名人
	private String emotionType = "";    // 情感
	private String economicsType = "";    // 经济
	private String literatureType = "";    // 文学
	private String philosophyType = "";    // 哲学
	private String journeyType = "";    // 旅行
	private String sportsType = "";    // 运动
	private String scienceType = "";    // 理学
	private String creativityType = "";    // 创意
	private String industryType = "";    // 工业
	private String lawType = "";    // 法律
	private String foodType = "";    // 美食
	private String medicalType = "";    // 医学
	private String otherType = "";    // 其他
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getIsConnTwv() {
		return isConnTwv;
	}

	public void setIsConnTwv(String isConnTwv) {
		this.isConnTwv = isConnTwv;
	}

	public String getConnTwvId() {
		return connTwvId;
	}

	public void setConnTwvId(String connTwvId) {
		this.connTwvId = connTwvId;
	}

	public String getThesisType() {
		return thesisType;
	}

	public void setThesisType(String thesisType) {
		this.thesisType = thesisType;
	}

	public Integer getHeatCount() {
		return heatCount;
	}

	public void setHeatCount(Integer heatCount) {
		this.heatCount = heatCount;
	}

	public Integer getSharedTimes() {
		return sharedTimes;
	}

	public void setSharedTimes(Integer sharedTimes) {
		this.sharedTimes = sharedTimes;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public Integer getLeftCount() {
		return leftCount;
	}

	public void setLeftCount(Integer leftCount) {
		this.leftCount = leftCount;
	}

	public Integer getRightCount() {
		return rightCount;
	}

	public void setRightCount(Integer rightCount) {
		this.rightCount = rightCount;
	}

	public Integer getCollectCount() {
		return collectCount;
	}

	public void setCollectCount(Integer collectCount) {
		this.collectCount = collectCount;
	}

	public String getLeftOpinion() {
		return leftOpinion;
	}

	public void setLeftOpinion(String leftOpinion) {
		this.leftOpinion = leftOpinion;
	}

	public String getRightOpinion() {
		return rightOpinion;
	}

	public void setRightOpinion(String rightOpinion) {
		this.rightOpinion = rightOpinion;
	}

	public String getIsAnon() {
		return isAnon;
	}

	public void setIsAnon(String isAnon) {
		this.isAnon = isAnon;
	}

	public String getMobileUserId() {
		return mobileUserId;
	}

	public void setMobileUserId(String mobileUserId) {
		this.mobileUserId = mobileUserId;
	}

	public String getTechType() {
		return techType;
	}

	public void setTechType(String techType) {
		this.techType = techType;
	}

	public String getEnvironmentType() {
		return environmentType;
	}

	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}
	
	public String getFilmType() {
		return filmType;
	}

	public void setFilmType(String filmType) {
		this.filmType = filmType;
	}

	public String getCelebrityType() {
		return celebrityType;
	}

	public void setCelebrityType(String celebrityType) {
		this.celebrityType = celebrityType;
	}

	public String getEmotionType() {
		return emotionType;
	}

	public void setEmotionType(String emotionType) {
		this.emotionType = emotionType;
	}

	public String getEconomicsType() {
		return economicsType;
	}

	public void setEconomicsType(String economicsType) {
		this.economicsType = economicsType;
	}

	public String getLiteratureType() {
		return literatureType;
	}

	public void setLiteratureType(String literatureType) {
		this.literatureType = literatureType;
	}

	public String getPhilosophyType() {
		return philosophyType;
	}

	public void setPhilosophyType(String philosophyType) {
		this.philosophyType = philosophyType;
	}

	public String getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	public String getSportsType() {
		return sportsType;
	}

	public void setSportsType(String sportsType) {
		this.sportsType = sportsType;
	}

	public String getScienceType() {
		return scienceType;
	}

	public void setScienceType(String scienceType) {
		this.scienceType = scienceType;
	}

	public String getCreativityType() {
		return creativityType;
	}

	public void setCreativityType(String creativityType) {
		this.creativityType = creativityType;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getLawType() {
		return lawType;
	}

	public void setLawType(String lawType) {
		this.lawType = lawType;
	}

	public String getFoodType() {
		return foodType;
	}

	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}

	public String getMedicalType() {
		return medicalType;
	}

	public void setMedicalType(String medicalType) {
		this.medicalType = medicalType;
	}

	public String getOtherType() {
		return otherType;
	}

	public void setOtherType(String otherType) {
		this.otherType = otherType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "TbThesis [id=" + id + ", userId=" + userId + ", title=" + title + ", url=" + url + ", background="
				+ background + ", isConnTwv=" + isConnTwv + ", connTwvId=" + connTwvId + ", thesisType=" + thesisType
				+ ", heatCount=" + heatCount + ", sharedTimes=" + sharedTimes + ", commentCount=" + commentCount
				+ ", leftCount=" + leftCount + ", rightCount=" + rightCount + ", collectCount=" + collectCount
				+ ", leftOpinion=" + leftOpinion + ", rightOpinion=" + rightOpinion + ", isAnon=" + isAnon
				+ ", mobileUserId=" + mobileUserId + ", techType=" + techType + ", environmentType=" + environmentType
				+ ", filmType=" + filmType + ", celebrityType=" + celebrityType + ", emotionType=" + emotionType
				+ ", economicsType=" + economicsType + ", literatureType=" + literatureType + ", philosophyType="
				+ philosophyType + ", journeyType=" + journeyType + ", sportsType=" + sportsType + ", scienceType="
				+ scienceType + ", creativityType=" + creativityType + ", industryType=" + industryType + ", lawType="
				+ lawType + ", foodType=" + foodType + ", medicalType=" + medicalType + ", otherType=" + otherType
				+ ", createTime=" + createTime + "]";
	}

}

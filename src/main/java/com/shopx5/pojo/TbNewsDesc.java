package com.shopx5.pojo;

import java.io.Serializable;

/**
 * 资讯详情
 * @author sjx
 * 2019年6月21日 上午10:41:36
 */
public class TbNewsDesc implements Serializable{

	private Long newsId;    // 资讯id
	private String newsDesc;    // 资讯详情
	public Long getNewsId() {
		return newsId;
	}
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public String getNewsDesc() {
		return newsDesc;
	}
	public void setNewsDesc(String newsDesc) {
		this.newsDesc = newsDesc;
	}
	@Override
	public String toString() {
		return "TbNewsDesc [newsId=" + newsId + ", newsDesc=" + newsDesc + "]";
	}
	
}

package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 论题 - 发言实体
 * @author sjx
 * 2019年8月20日 上午10:59:35
 */
@SuppressWarnings("serial")
public class TbSpeechXS implements Serializable{

	private String id;
	private String userId;
	private String userImageUrl;
	private String nickName;
	private String suffix;    // 认证后缀
	private String thesisId;
	private String speechFlag;    // left - 代表正方发表观点;  right - 代表反方发表观点
	private String videoUrl;
	private String speech1;
	private String speech2;
	private Integer heatCount;    // 点赞数
	
	private String atPeople;    // 发言时@的人id集合,逗号,隔开
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getThesisId() {
		return thesisId;
	}

	public void setThesisId(String thesisId) {
		this.thesisId = thesisId;
	}

	public String getSpeechFlag() {
		return speechFlag;
	}

	public void setSpeechFlag(String speechFlag) {
		this.speechFlag = speechFlag;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getSpeech1() {
		return speech1;
	}

	public void setSpeech1(String speech1) {
		this.speech1 = speech1;
	}

	public String getSpeech2() {
		return speech2;
	}

	public void setSpeech2(String speech2) {
		this.speech2 = speech2;
	}
	
	public Integer getHeatCount() {
		return heatCount;
	}

	public void setHeatCount(Integer heatCount) {
		this.heatCount = heatCount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getAtPeople() {
		return atPeople;
	}

	public void setAtPeople(String atPeople) {
		this.atPeople = atPeople;
	}

	@Override
	public String toString() {
		return "TbSpeechXS [id=" + id + ", userId=" + userId + ", userImageUrl=" + userImageUrl + ", nickName="
				+ nickName + ", suffix=" + suffix + ", thesisId=" + thesisId + ", speechFlag=" + speechFlag
				+ ", videoUrl=" + videoUrl + ", speech1=" + speech1 + ", speech2=" + speech2 + ", heatCount="
				+ heatCount + ", atPeople=" + atPeople + ", createTime=" + createTime + "]";
	}
}

package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 评论实体
 * @author sjx
 * 2019年7月4日 下午3:19:04
 */
@SuppressWarnings("serial")
public class TbCommentXS implements Serializable{

	private String id;
	private String aimInfoId;
	
	private String userId;
	private String userImageUrl;
	private String userNickName;
	
	private String comment;
	private String commentId;
	private Integer heatCount = 0;
	private Integer replyCount = 0;
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAimInfoId() {
		return aimInfoId;
	}

	public void setAimInfoId(String aimInfoId) {
		this.aimInfoId = aimInfoId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getUserNickName() {
		return userNickName;
	}

	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public Integer getHeatCount() {
		return heatCount;
	}

	public void setHeatCount(Integer heatCount) {
		this.heatCount = heatCount;
	}

	public Integer getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(Integer replyCount) {
		this.replyCount = replyCount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "TbCommentXS [id=" + id + ", aimInfoId=" + aimInfoId + ", userId=" + userId + ", userImageUrl="
				+ userImageUrl + ", userNickName=" + userNickName + ", comment=" + comment + ", commentId=" + commentId
				+ ", heatCount=" + heatCount + ", replyCount=" + replyCount + ", createTime=" + createTime + "]";
	}

}

package com.shopx5.pojo.group;

import java.io.Serializable;
import java.util.List;

import com.shopx5.pojo.SsSpecification;
import com.shopx5.pojo.SsSpecificationOption;
/**
 * 组合实体类
 * @author tang ShopX5多商户商城系统
 */
public class Specification implements Serializable {
	private SsSpecification specification;                      // 规格实体
	private List<SsSpecificationOption> specificationOptionList;// 规格选项集合

	public SsSpecification getSpecification() {
		return specification;
	}

	public void setSpecification(SsSpecification specification) {
		this.specification = specification;
	}

	public List<SsSpecificationOption> getSpecificationOptionList() {
		return specificationOptionList;
	}

	public void setSpecificationOptionList(List<SsSpecificationOption> specificationOptionList) {
		this.specificationOptionList = specificationOptionList;
	}
}

package com.shopx5.pojo.group;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Good implements Serializable {
	private Long id;
	private String goodsName;
	private String caption;
	private String sellerId;
	private Long defaultItemId;
	private String smallPic;
	private BigDecimal price;
	private Long brandId;
	private Long category1Id;
	private Long category2Id;
	private Long category3Id;
	private String isEnableSpec;
	private Long templateTypeId;
	private String auditStatus;
	private String isMarketable;
	private String isDelete;
	private Date createTime;
	private Date updateTime;

	private GoodInfo goodInfo; // 一对一
	private Long collectionId; // 收藏
	private String viewId; // 足迹

	public GoodInfo getGoodInfo() {
		return goodInfo;
	}
	public void setGoodInfo(GoodInfo goodInfo) {
		this.goodInfo = goodInfo;
	}
	public Long getCollectionId() {
		return collectionId;
	}
	public void setCollectionId(Long collectionId) {
		this.collectionId = collectionId;
	}
	public String getViewId() {
		return viewId;
	}
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName == null ? null : goodsName.trim();
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption == null ? null : caption.trim();
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId == null ? null : sellerId.trim();
	}

	public Long getDefaultItemId() {
		return defaultItemId;
	}

	public void setDefaultItemId(Long defaultItemId) {
		this.defaultItemId = defaultItemId;
	}

	public String getSmallPic() {
		return smallPic;
	}

	public void setSmallPic(String smallPic) {
		this.smallPic = smallPic == null ? null : smallPic.trim();
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Long getCategory1Id() {
		return category1Id;
	}

	public void setCategory1Id(Long category1Id) {
		this.category1Id = category1Id;
	}

	public Long getCategory2Id() {
		return category2Id;
	}

	public void setCategory2Id(Long category2Id) {
		this.category2Id = category2Id;
	}

	public Long getCategory3Id() {
		return category3Id;
	}

	public void setCategory3Id(Long category3Id) {
		this.category3Id = category3Id;
	}

	public String getIsEnableSpec() {
		return isEnableSpec;
	}

	public void setIsEnableSpec(String isEnableSpec) {
		this.isEnableSpec = isEnableSpec == null ? null : isEnableSpec.trim();
	}

	public Long getTemplateTypeId() {
		return templateTypeId;
	}

	public void setTemplateTypeId(Long templateTypeId) {
		this.templateTypeId = templateTypeId;
	}

	public String getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus == null ? null : auditStatus.trim();
	}

	public String getIsMarketable() {
		return isMarketable;
	}

	public void setIsMarketable(String isMarketable) {
		this.isMarketable = isMarketable == null ? null : isMarketable.trim();
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete == null ? null : isDelete.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}

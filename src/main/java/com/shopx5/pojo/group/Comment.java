package com.shopx5.pojo.group;

import java.io.Serializable;
import java.util.List;

import com.shopx5.pojo.TbComment;
import com.shopx5.pojo.TbCommentReply;

public class Comment extends TbComment implements Serializable{
	public Comment(TbComment tbComment) {
		//初始化属性
		this.setId(tbComment.getId());
		this.setGoodsId(tbComment.getGoodsId());
		this.setUserId(tbComment.getUserId());
		this.setSellerId(tbComment.getSellerId());
		this.setRates(tbComment.getRates());
		this.setContent(tbComment.getContent());
		this.setDescription(tbComment.getDescription());
		this.setService(tbComment.getService());
		this.setLogistics(tbComment.getLogistics());
		this.setStatus(tbComment.getStatus());
		this.setCreated(tbComment.getCreated());
		this.setUpdated(tbComment.getUpdated());
	}
    private List<TbCommentReply> reply;
    
	public List<TbCommentReply> getReply() {
		return reply;
	}
	public void setReply(List<TbCommentReply> reply) {
		this.reply = reply;
	}
}

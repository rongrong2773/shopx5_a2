package com.shopx5.pojo.group;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Dynamic;
/**
 * 商品导入索引库实体
 * @author ShopX5多商户商城系统
 */
public class SolrGoods implements Serializable{
	@Field
    private Long id;
	
	@Field("item_title")
    private String title;
	
    @Field("item_prices")
    private BigDecimal price;
    
    @Field("item_image")
    private String image;
    
    @Field("item_category")
    private String category;
    
    @Field("item_brand")
    private String brand;
    
    @Field("item_goodsid")
    private Long goodsId;
    
    @Field("item_seller")
    private String seller;
    
    @Field("item_updatetime")
    private Date updateTime;
    
    @Dynamic
	@Field("item_spec_*")
	private Map<String, String> specMap;
    
    private List<Map> imageList; //image json
    private String isDefault;
    private BigDecimal marketPrice;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Long getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Map<String, String> getSpecMap() {
		return specMap;
	}
	public void setSpecMap(Map<String, String> specMap) {
		this.specMap = specMap;
	}
	public List<Map> getImageList() {
		return imageList;
	}
	public void setImageList(List<Map> imageList) {
		this.imageList = imageList;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
}

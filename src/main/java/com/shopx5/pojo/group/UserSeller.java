package com.shopx5.pojo.group;

import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbSeller;

public class UserSeller {
	private SpUser user;
	private TbSeller seller;

	public SpUser getUser() {
		return user;
	}
	public void setUser(SpUser user) {
		this.user = user;
	}
	public TbSeller getSeller() {
		return seller;
	}
	public void setSeller(TbSeller seller) {
		this.seller = seller;
	}
}

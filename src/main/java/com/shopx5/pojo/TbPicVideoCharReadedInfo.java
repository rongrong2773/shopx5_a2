package com.shopx5.pojo;

import java.io.Serializable;
import java.sql.Date;

/**
 * 记录图文/视频 实体浏览信息
 * @author sjx
 * 2019年7月17日 上午10:43:02
 */
public class TbPicVideoCharReadedInfo implements Serializable{

	
	private String infoId;    // 图文/视频 主键id
	private String userId;    // 用户id
	private Date createTime;    // 浏览时间
	public String getInfoId() {
		return infoId;
	}
	public void setInfoId(String infoId) {
		this.infoId = infoId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbPicVideoCharReadedInfo [infoId=" + infoId + ", userId=" + userId + ", createTime=" + createTime + "]";
	}
}

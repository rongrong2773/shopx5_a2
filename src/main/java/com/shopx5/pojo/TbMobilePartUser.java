package com.shopx5.pojo;

import java.io.Serializable;
/**
 * app端用户部分字段，返回给app用于显示还有列表用
 * 字段内容与 TbMobileUser同步
 * @author sjx
 * 2019年6月29日 下午9:03:01
 */
@SuppressWarnings("serial")
public class TbMobilePartUser implements Serializable{

	private Long id;
	private String clientId;    // unipush客户端id
	private String image;    // 头像
	private String realName;    // 真实姓名
	private String phone;    // 手机号
	private String nickName;    // 昵称
	private String userType;    // 用户类型    0-个人   1-企业
	private String token;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public String toString() {
		return "TbMobilePartUser [id=" + id + ", clientId=" + clientId + ", image=" + image + ", realName=" + realName
				+ ", phone=" + phone + ", nickName=" + nickName + ", userType=" + userType + ", token=" + token + "]";
	}
}

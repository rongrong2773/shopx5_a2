package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 图文、视频的 "点赞/收藏/分享/浏览"  记录
 * @author sjx
 * 2019年8月1日 上午11:42:08
 */
@SuppressWarnings("serial")
public class TbPicVideoCharConnected implements Serializable{

	private String usrId;    // 用户id
	private String pcvId;    // 图文/视频id
	private String isPicCharOrVideo;    // 取值： pic_char - 图文、video_char - 视频
	private String opType;    // heat - 点赞;  collect - 收藏;  share - 分享;  read - 浏览
	private Date createTime;
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public String getPcvId() {
		return pcvId;
	}
	public void setPcvId(String pcvId) {
		this.pcvId = pcvId;
	}
	public String getIsPicCharOrVideo() {
		return isPicCharOrVideo;
	}
	public void setIsPicCharOrVideo(String isPicCharOrVideo) {
		this.isPicCharOrVideo = isPicCharOrVideo;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbPicVideoCharConnected [usrId=" + usrId + ", pcvId=" + pcvId + ", isPicCharOrVideo=" + isPicCharOrVideo
				+ ", opType=" + opType + ", createTime=" + createTime + "]";
	}
}

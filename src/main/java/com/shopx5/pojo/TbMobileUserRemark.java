package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户备注实体
 * @author sjx
 * 2019年8月15日 下午3:49:39
 */
@SuppressWarnings("serial")
public class TbMobileUserRemark implements Serializable{

	private String userId = "";    // 登录用户id
	private String aimUserId = "";    // 被关注的用户id
	private String aimUserRemark = "";    // 被关注的用户备注
	
	private Date createTime;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAimUserId() {
		return aimUserId;
	}
	public void setAimUserId(String aimUserId) {
		this.aimUserId = aimUserId;
	}
	public String getAimUserRemark() {
		return aimUserRemark;
	}
	public void setAimUserRemark(String aimUserRemark) {
		this.aimUserRemark = aimUserRemark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbMobileUserRemark [userId=" + userId + ", aimUserId=" + aimUserId + ", aimUserRemark=" + aimUserRemark
				+ ", createTime=" + createTime + "]";
	}
}

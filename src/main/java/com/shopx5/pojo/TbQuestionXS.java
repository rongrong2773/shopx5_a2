package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;
/**
 * 发言的质疑实体
 * @author sjx
 * 2019年8月20日 下午4:59:23
 */

@SuppressWarnings("serial")
public class TbQuestionXS implements Serializable{

	private String id;
	private String userId;
	private String userImageUrl;
	private String nickName;
	private String suffix;
	
	private String speechId;
	private String thesisId;
	private String url;
	private String quesReason;    // 对应app输入框 - "请输入质疑原因"
	private String quesMaterial;    // 对应app输入框 - "请举例说明"
	
	private Integer agreeCount;    // 赞同数目
	private Integer disagreeCount;    // 不赞同数目
	
	private String agreeCollect;   // 赞同的人id集合
	private String disagreeCollect;    // 反对的人id集合
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSpeechId() {
		return speechId;
	}

	public void setSpeechId(String speechId) {
		this.speechId = speechId;
	}

	public String getThesisId() {
		return thesisId;
	}

	public void setThesisId(String thesisId) {
		this.thesisId = thesisId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getQuesReason() {
		return quesReason;
	}

	public void setQuesReason(String quesReason) {
		this.quesReason = quesReason;
	}

	public String getQuesMaterial() {
		return quesMaterial;
	}

	public void setQuesMaterial(String quesMaterial) {
		this.quesMaterial = quesMaterial;
	}

	public Integer getAgreeCount() {
		return agreeCount;
	}

	public void setAgreeCount(Integer agreeCount) {
		this.agreeCount = agreeCount;
	}

	public Integer getDisagreeCount() {
		return disagreeCount;
	}

	public void setDisagreeCount(Integer disagreeCount) {
		this.disagreeCount = disagreeCount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	

	public String getAgreeCollect() {
		return agreeCollect;
	}

	public void setAgreeCollect(String agreeCollect) {
		this.agreeCollect = agreeCollect;
	}

	public String getDisagreeCollect() {
		return disagreeCollect;
	}

	public void setDisagreeCollect(String disagreeCollect) {
		this.disagreeCollect = disagreeCollect;
	}

	@Override
	public String toString() {
		return "TbQuestionXS [id=" + id + ", userId=" + userId + ", userImageUrl=" + userImageUrl + ", nickName="
				+ nickName + ", suffix=" + suffix + ", speechId=" + speechId + ", thesisId=" + thesisId + ", url=" + url
				+ ", quesReason=" + quesReason + ", quesMaterial=" + quesMaterial + ", agreeCount=" + agreeCount
				+ ", disagreeCount=" + disagreeCount + ", agreeCollect=" + agreeCollect + ", disagreeCollect="
				+ disagreeCollect + ", createTime=" + createTime + "]";
	}

	public void updateQuestionEntityDisagree(TbQuestionXS tbQuestionXS) {
		// TODO Auto-generated method stub
		
	}

}

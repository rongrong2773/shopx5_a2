package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 寻思(原图文、视频)
 * @author sjx
 * 2019年7月2日 下午7:47:36
 */
@SuppressWarnings("serial")
public class TbPicVideoChar implements Serializable{

	private String id;

	private String title;    // 寻思标题
	private String content;    // 寻思正文
	private String isAnon;    // 是否为匿名发布,yes - 是, no - 否
	private String url;    // 图片/视频地址 - json数组
	private Integer heatCount;    // 点赞数目,  sql默认值为0
	private Integer sharedTimes;    // 分享次数,  sql默认值为0
	private Integer commentsCount;    // 评论个数,  sql默认值为0
	private String relatedTopic;    // 新思路 - id组成的json数组
	private Integer readTimes;    // 浏览次数,  sql默认值为0
	private Integer collectTimes;    // 收藏次数,  sql默认值为0
	
	private String userId;    // 用户主键id
	private String userSuffix;    // 用户认证后缀
	private String userImageUrl;    // 用户头像url
	private String userNickName;    // 用户昵称
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsAnon() {
		return isAnon;
	}

	public void setIsAnon(String isAnon) {
		this.isAnon = isAnon;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getHeatCount() {
		return heatCount;
	}

	public void setHeatCount(Integer heatCount) {
		this.heatCount = heatCount;
	}

	public Integer getSharedTimes() {
		return sharedTimes;
	}

	public void setSharedTimes(Integer sharedTimes) {
		this.sharedTimes = sharedTimes;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	public String getRelatedTopic() {
		return relatedTopic;
	}

	public void setRelatedTopic(String relatedTopic) {
		this.relatedTopic = relatedTopic;
	}

	public Integer getReadTimes() {
		return readTimes;
	}

	public void setReadTimes(Integer readTimes) {
		this.readTimes = readTimes;
	}

	public Integer getCollectTimes() {
		return collectTimes;
	}

	public void setCollectTimes(Integer collectTimes) {
		this.collectTimes = collectTimes;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserSuffix() {
		return userSuffix;
	}

	public void setUserSuffix(String userSuffix) {
		this.userSuffix = userSuffix;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getUserNickName() {
		return userNickName;
	}

	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@Override
	public String toString() {
		return "TbPicVideoChar [id=" + id + ", title=" + title + ", content=" + content + ", isAnon=" + isAnon
				+ ", url=" + url + ", heatCount=" + heatCount + ", sharedTimes=" + sharedTimes + ", commentsCount="
				+ commentsCount + ", relatedTopic=" + relatedTopic + ", readTimes=" + readTimes + ", collectTimes="
				+ collectTimes + ", userId=" + userId + ", userSuffix=" + userSuffix + ", userImageUrl=" + userImageUrl
				+ ", userNickName=" + userNickName + ", createTime=" + createTime + "]";
	}
}
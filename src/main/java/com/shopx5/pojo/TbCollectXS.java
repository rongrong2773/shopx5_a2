package com.shopx5.pojo;

import java.io.Serializable;
/**
 * XS收藏信息实体
 * @author sjx
 * 2019年7月22日 上午10:19:26
 */
public class TbCollectXS implements Serializable{

	private String userId;
	private String aimId;
	private String aimType;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAimId() {
		return aimId;
	}
	public void setAimId(String aimId) {
		this.aimId = aimId;
	}
	public String getAimType() {
		return aimType;
	}
	public void setAimType(String aimType) {
		this.aimType = aimType;
	}
	@Override
	public String toString() {
		return "TbCollectXS [userId=" + userId + ", aimId=" + aimId + ", aimType=" + aimType + "]";
	}
}

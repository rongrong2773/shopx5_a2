package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sjx
 * 2019年7月22日 下午2:26:51
 */
public class TbThesisTwvConn implements Serializable{

	private String thesisId;    // 论题id
	private String twvId;    // 图文/视频id
	private Date createTime;
	
	public String getThesisId() {
		return thesisId;
	}
	public void setThesisId(String thesisId) {
		this.thesisId = thesisId;
	}
	public String getTwvId() {
		return twvId;
	}
	public void setTwvId(String twvId) {
		this.twvId = twvId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbThesisTwvConn [thesisId=" + thesisId + ", twvId=" + twvId + ", createTime=" + createTime + "]";
	}
	
}

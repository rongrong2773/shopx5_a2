package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 个人中心 - 设置 - 最新登陆信息
 * @author sjx
 * 2019年7月29日 上午9:49:43
 */
@SuppressWarnings("serial")
public class TbMobileUserLoginInfo implements Serializable{

	private String id;
	private String device;    // 登陆设备
	private double longitude;    // 经度
	private double latitude;    // 纬度
	private Date loginTime;    // 登陆时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	@Override
	public String toString() {
		return "TbMobileUserLoginInfo [id=" + id + ", device=" + device + ", longitude=" + longitude + ", latitude="
				+ latitude + ", loginTime=" + loginTime + "]";
	}
}

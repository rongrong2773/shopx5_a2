package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * app端用户详情
 * @author sjx
 * 2019年6月26日 下午3:17:22
 */
@SuppressWarnings("serial")
public class TbMobileUser implements Serializable{

	private Long id;
	private String image;    // 头像
	private String realName;    // 真实姓名
	private String prefix;    // 认证前缀;  个人认证、学校认证、企业认证
	private String suffix;    // 认证后缀;  xxxxxx、来自xxx学校、来自xxx企业
	private String gender;    // 性别
	private String phone;    // 手机号
	private String nickName;    // 昵称
	private Integer fansCount;    // 粉丝个数
	private Integer concernCount;    // 关注个数
	private String rigion = "";    // 地区
	private String userType;    // 用户类型    0-个人/THINKER   1-企业   2 - 学校
	private String licAuthority;    // 认证机关
	private String companyName;    // 单位/公司名称
	private String credentialUrl;    // 证明材料
	
	/**
	 * 个人中心 - 设置 - 推送设置
	 */
	private String heatCollectNotify;   // 默认打开;    打开 - open  /  关闭 - close
	private String commentReplyNotify;;
	private String concernNotify;
	
	private String idType;    // 证件类型
	private String idNumber;    // 证件号码
	private Integer loginTimes;    // 登录次数
	private String clientId;    // 客户端个推id
	
	
	private String token;    // 用户token
	private String signature;    // 签名
	private Integer releaseCounts;    // 发布的图文/视频/原创文章数目
	private Integer jfCounts;    // 积分数目
	private double moneyCounts;    // 账户余额  (不是积分余额) 
	private Integer unReadMsgCnts;    // 未读消息数目
	
	private String sheildUserList;    // 拉黑的用户
	private String verifyFlag;    // 认证标志  yes - 已认证;  no - 未认证
	
    private Date createTime;    // 注册时间
    private Date updateTime;    // 个人信息更新时间
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Integer getFansCount() {
		return fansCount;
	}
	public void setFansCount(Integer fansCount) {
		this.fansCount = fansCount;
	}
	public Integer getConcernCount() {
		return concernCount;
	}
	public void setConcernCount(Integer concernCount) {
		this.concernCount = concernCount;
	}
	public String getRigion() {
		return rigion;
	}
	public void setRigion(String rigion) {
		this.rigion = rigion;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getLicAuthority() {
		return licAuthority;
	}
	public void setLicAuthority(String licAuthority) {
		this.licAuthority = licAuthority;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCredentialUrl() {
		return credentialUrl;
	}
	public void setCredentialUrl(String credentialUrl) {
		this.credentialUrl = credentialUrl;
	}
	public String getHeatCollectNotify() {
		return heatCollectNotify;
	}
	public void setHeatCollectNotify(String heatCollectNotify) {
		this.heatCollectNotify = heatCollectNotify;
	}
	public String getCommentReplyNotify() {
		return commentReplyNotify;
	}
	public void setCommentReplyNotify(String commentReplyNotify) {
		this.commentReplyNotify = commentReplyNotify;
	}
	public String getConcernNotify() {
		return concernNotify;
	}
	public void setConcernNotify(String concernNotify) {
		this.concernNotify = concernNotify;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public Integer getLoginTimes() {
		return loginTimes;
	}
	public void setLoginTimes(Integer loginTimes) {
		this.loginTimes = loginTimes;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public Integer getReleaseCounts() {
		return releaseCounts;
	}
	public void setReleaseCounts(Integer releaseCounts) {
		this.releaseCounts = releaseCounts;
	}
	public Integer getJfCounts() {
		return jfCounts;
	}
	public void setJfCounts(Integer jfCounts) {
		this.jfCounts = jfCounts;
	}
	public double getMoneyCounts() {
		return moneyCounts;
	}
	public void setMoneyCounts(double moneyCounts) {
		this.moneyCounts = moneyCounts;
	}
	public Integer getUnReadMsgCnts() {
		return unReadMsgCnts;
	}
	public void setUnReadMsgCnts(Integer unReadMsgCnts) {
		this.unReadMsgCnts = unReadMsgCnts;
	}
	public String getSheildUserList() {
		return sheildUserList;
	}
	public void setSheildUserList(String sheildUserList) {
		this.sheildUserList = sheildUserList;
	}
	
	public String getVerifyFlag() {
		return verifyFlag;
	}
	public void setVerifyFlag(String verifyFlag) {
		this.verifyFlag = verifyFlag;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "TbMobileUser [id=" + id + ", image=" + image + ", realName=" + realName + ", prefix=" + prefix
				+ ", suffix=" + suffix + ", gender=" + gender + ", phone=" + phone + ", nickName=" + nickName
				+ ", fansCount=" + fansCount + ", concernCount=" + concernCount + ", rigion=" + rigion + ", userType="
				+ userType + ", licAuthority=" + licAuthority + ", companyName=" + companyName + ", credentialUrl="
				+ credentialUrl + ", heatCollectNotify=" + heatCollectNotify + ", commentReplyNotify="
				+ commentReplyNotify + ", concernNotify=" + concernNotify + ", idType=" + idType + ", idNumber="
				+ idNumber + ", loginTimes=" + loginTimes + ", clientId=" + clientId + ", token=" + token
				+ ", signature=" + signature + ", releaseCounts=" + releaseCounts + ", jfCounts=" + jfCounts
				+ ", moneyCounts=" + moneyCounts + ", unReadMsgCnts=" + unReadMsgCnts + ", sheildUserList="
				+ sheildUserList + ", verifyFlag=" + verifyFlag + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}
}
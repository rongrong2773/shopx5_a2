package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 原创文章
 * @author sjx
 * 2019年7月3日 上午9:54:10
 */
@SuppressWarnings("serial")
public class TbArticle implements Serializable{

	private String id;    // 32位uuid
	private String userId;    // 15位
	private String userImageUrl;
	private String nickName;
	private String isAnon;    // yes - 匿名发布; no-非匿名发布
	private String title;
	private String content;
	private String readPerm;    // 阅读权限  free - 免费;  charge - 收费
	private String isAllowShare;    // yes - 允许分享;   no - 不能分享
	private String price;
	
	private Integer readCount;    // 浏览次数,sql默认设置为0
	private Integer heatCount;    // 热度/点赞数,sql默认设置为0
	private Integer sharedCount;    // 分享次数,sql默认设置为0
	
	/**
	 * yes表示有该类型被选中, no表示未选中
	 * sql默认值no
	 */
	private String techType;    // 科技
	private String lifeType;    // 生活
	private String celebrityType;    // 名人
	private String filmType;    // 影视
	private String philosophyType;    // 哲学
	private String industryType;    // 工业
	private String literatureType;    // 文学
	private String internetType;    // 互联网
	
	private String sportsType;    // 体育
	private String healthType;    // 理学
	private String economicsType;    // 经济
	private String otherType;    // 其他
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getIsAnon() {
		return isAnon;
	}

	public void setIsAnon(String isAnon) {
		this.isAnon = isAnon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReadPerm() {
		return readPerm;
	}

	public void setReadPerm(String readPerm) {
		this.readPerm = readPerm;
	}
	
	public String getIsAllowShare() {
		return isAllowShare;
	}

	public void setIsAllowShare(String isAllowShare) {
		this.isAllowShare = isAllowShare;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getReadCount() {
		return readCount;
	}

	public void setReadCount(Integer readCount) {
		this.readCount = readCount;
	}

	public Integer getHeatCount() {
		return heatCount;
	}

	public void setHeatCount(Integer heatCount) {
		this.heatCount = heatCount;
	}

	public Integer getSharedCount() {
		return sharedCount;
	}

	public void setSharedCount(Integer sharedCount) {
		this.sharedCount = sharedCount;
	}

	public String getTechType() {
		return techType;
	}

	public void setTechType(String techType) {
		this.techType = techType;
	}

	public String getLifeType() {
		return lifeType;
	}

	public void setLifeType(String lifeType) {
		this.lifeType = lifeType;
	}

	public String getCelebrityType() {
		return celebrityType;
	}

	public void setCelebrityType(String celebrityType) {
		this.celebrityType = celebrityType;
	}

	public String getFilmType() {
		return filmType;
	}

	public void setFilmType(String filmType) {
		this.filmType = filmType;
	}

	public String getPhilosophyType() {
		return philosophyType;
	}

	public void setPhilosophyType(String philosophyType) {
		this.philosophyType = philosophyType;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getLiteratureType() {
		return literatureType;
	}

	public void setLiteratureType(String literatureType) {
		this.literatureType = literatureType;
	}

	public String getInternetType() {
		return internetType;
	}

	public void setInternetType(String internetType) {
		this.internetType = internetType;
	}

	public String getSportsType() {
		return sportsType;
	}

	public void setSportsType(String sportsType) {
		this.sportsType = sportsType;
	}

	public String getHealthType() {
		return healthType;
	}

	public void setHealthType(String healthType) {
		this.healthType = healthType;
	}

	public String getEconomicsType() {
		return economicsType;
	}

	public void setEconomicsType(String economicsType) {
		this.economicsType = economicsType;
	}

	public String getOtherType() {
		return otherType;
	}

	public void setOtherType(String otherType) {
		this.otherType = otherType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "TbArticle [id=" + id + ", userId=" + userId + ", userImageUrl=" + userImageUrl + ", nickName="
				+ nickName + ", isAnon=" + isAnon + ", title=" + title + ", content=" + content + ", readPerm="
				+ readPerm + ", isAllowShare=" + isAllowShare + ", price=" + price + ", readCount=" + readCount
				+ ", heatCount=" + heatCount + ", sharedCount=" + sharedCount + ", techType=" + techType + ", lifeType="
				+ lifeType + ", celebrityType=" + celebrityType + ", filmType=" + filmType + ", philosophyType="
				+ philosophyType + ", industryType=" + industryType + ", literatureType=" + literatureType
				+ ", internetType=" + internetType + ", sportsType=" + sportsType + ", healthType=" + healthType
				+ ", economicsType=" + economicsType + ", otherType=" + otherType + ", createTime=" + createTime + "]";
	}

}

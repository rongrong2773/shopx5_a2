package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 议题详情两方辩论和共同探讨的评论实体
 * @author sjx
 * 2019年7月21日 下午3:40:03
 */
@SuppressWarnings("serial")
public class TbThesisComment implements Serializable{

	private String id;
	private String comment;
	private String commentUsrId;
	private Date createTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommentUsrId() {
		return commentUsrId;
	}
	public void setCommentUsrId(String commentUsrId) {
		this.commentUsrId = commentUsrId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbThesisComment [id=" + id + ", comment=" + comment + ", commentUsrId=" + commentUsrId + ", createTime="
				+ createTime + "]";
	}
}

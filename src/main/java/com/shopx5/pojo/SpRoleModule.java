package com.shopx5.pojo;

import java.io.Serializable;

public class SpRoleModule implements Serializable{
    private String roleId;

    private Integer moduleId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }
}
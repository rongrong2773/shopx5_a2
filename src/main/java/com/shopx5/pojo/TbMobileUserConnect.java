package com.shopx5.pojo;

import java.io.Serializable;

import org.springframework.data.annotation.Transient;

/**
 * 数据库没对应表啊，TbMobileUserConnect.xml文件用，用于过滤select *from tb_mobile_user中null
 * @author sjx
 * 2019年7月15日 下午2:42:15
 */
@SuppressWarnings("serial")
public class TbMobileUserConnect implements Serializable{

	private Long id;
	private String image;    // 头像
	private String nickName;    // 昵称
	private String userType;    // 用户类型    0-个人   1-企业   2 - 未认证
	
	@Transient
	private String remark;    // 备注, 不存入数据库
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "TbMobileUserConnect [id=" + id + ", image=" + image + ", nickName=" + nickName + ", userType="
				+ userType + ", remark=" + remark + "]";
	}

}

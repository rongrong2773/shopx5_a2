package com.shopx5.pojo;

import java.io.Serializable;

/**
 * app端用户详情
 * @author sjx
 * 2019年6月26日 下午3:17:22
 */
@SuppressWarnings("serial")
public class TbMobileUserTransient implements Serializable{

	private String id;
	private String image;    // 头像
	private String prefix;    // 认证前缀;  个人认证、学校认证、企业认证
	private String suffix;    // 认证后缀;  xxxxxx、来自xxx学校、来自xxx企业
	private String nickName;    // 昵称
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	@Override
	public String toString() {
		return "TbMobileUserTransient [id=" + id + ", image=" + image + ", prefix=" + prefix + ", suffix=" + suffix
				+ ", nickName=" + nickName + "]";
	}
}
package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;
/**
 * 寻思推送消息实体
 * @author sjx
 * 2019年8月22日 下午5:07:29
 */
@SuppressWarnings("serial")
public class TbPushXSMsg implements Serializable{

	private String id;
	private String targetUserId;    // 推送目标用户id
	private String msgType;
	private String msgContent;
	private Date createTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTargetUserId() {
		return targetUserId;
	}
	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbPushXSMsg [id=" + id + ", targetUserId=" + targetUserId + ", msgType=" + msgType + ", msgContent="
				+ msgContent + ", createTime=" + createTime + "]";
	}

}

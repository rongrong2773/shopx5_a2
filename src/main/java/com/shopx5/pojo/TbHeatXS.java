package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;
/**
 * 寻思、论题、原创文章点赞
 * @author sjx
 * 2019年8月16日 下午5:23:10
 */

@SuppressWarnings("serial")
public class TbHeatXS implements Serializable{

	private String userId;
	private String aimInfoId;
	private String type;    // 点赞对象的类型, article - 原创文章; thesis - 论题
	private Date createTime;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAimInfoId() {
		return aimInfoId;
	}
	public void setAimInfoId(String aimInfoId) {
		this.aimInfoId = aimInfoId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbHeatXS [userId=" + userId + ", aimInfoId=" + aimInfoId + ", type=" + type + ", createTime="
				+ createTime + "]";
	}
	
}

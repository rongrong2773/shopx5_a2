package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * APP信息
 * @author sjx
 * 2019年7月2日 上午9:05:20
 */
public class TbAppInfo implements Serializable{

	private String id;    // 主键id
	private String version;    // 版本
	private Date releaseTime;    // 发布时间
	private String downloadUrl;    // 下载url
	private String updateDesc;    // 更新内容
	private String isForceUpdate;    // 是否强制更新    0 - 是，  1- 否
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Date getReleaseTime() {
		return releaseTime;
	}
	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getUpdateDesc() {
		return updateDesc;
	}
	public void setUpdateDesc(String updateDesc) {
		this.updateDesc = updateDesc;
	}
	public String getIsForceUpdate() {
		return isForceUpdate;
	}
	public void setIsForceUpdate(String isForceUpdate) {
		this.isForceUpdate = isForceUpdate;
	}
	@Override
	public String toString() {
		return "TbAppInfo [id=" + id + ", version=" + version + ", releaseTime=" + releaseTime + ", downloadUrl="
				+ downloadUrl + ", updateDesc=" + updateDesc + ", isForceUpdate=" + isForceUpdate + "]";
	}
}

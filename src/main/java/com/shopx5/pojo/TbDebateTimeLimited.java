package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;
/**
 * 星秀 - 限时辩论
 * @author sjx
 * 2019年8月5日 下午5:02:48
 */
@SuppressWarnings("serial")
public class TbDebateTimeLimited implements Serializable{

	private String id;
	private String userId;    // 用户id
	private String title;    // 标题
	private String debateType;    // 共同探讨 - 0     两方辩论 - 1
	private String positiveOp;    // 正方观点
	private String negativeOp;    // 反方观点
	private String background;    // 辩论背景
	private String url;    // 视频/图片 url
	
	private String isHasIntegral;    // 是否有奖池,   yes - 有;  no - 没有
	private Integer integral;    // 奖池中的积分数量
	private Integer debeatDuration;    // 辩论时效,以s(秒)为单位进行计算
	private String isLimitPersonCount;    // 是否限制辩论人数,  yes - 限制了;    no - 不限制
	private Integer debeatPersonCount;    // 辩论人数。有值时值为人数，无值时默认不限制人数
	private String isStopped;    // stopped - 论题已截至,  running - 论题进行中
	
	private Date createTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDebateType() {
		return debateType;
	}
	public void setDebateType(String debateType) {
		this.debateType = debateType;
	}
	public String getPositiveOp() {
		return positiveOp;
	}
	public void setPositiveOp(String positiveOp) {
		this.positiveOp = positiveOp;
	}
	public String getNegativeOp() {
		return negativeOp;
	}
	public void setNegativeOp(String negativeOp) {
		this.negativeOp = negativeOp;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIsHasIntegral() {
		return isHasIntegral;
	}
	public void setIsHasIntegral(String isHasIntegral) {
		this.isHasIntegral = isHasIntegral;
	}
	public Integer getIntegral() {
		return integral;
	}
	public void setIntegral(Integer integral) {
		this.integral = integral;
	}
	public Integer getDebeatDuration() {
		return debeatDuration;
	}
	public void setDebeatDuration(Integer debeatDuration) {
		this.debeatDuration = debeatDuration;
	}
	public String getIsLimitPersonCount() {
		return isLimitPersonCount;
	}
	public void setIsLimitPersonCount(String isLimitPersonCount) {
		this.isLimitPersonCount = isLimitPersonCount;
	}
	public Integer getDebeatPersonCount() {
		return debeatPersonCount;
	}
	public void setDebeatPersonCount(Integer debeatPersonCount) {
		this.debeatPersonCount = debeatPersonCount;
	}
	public String getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(String isStopped) {
		this.isStopped = isStopped;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TbDebateTimeLimited [id=" + id + ", userId=" + userId + ", title=" + title + ", debateType="
				+ debateType + ", positiveOp=" + positiveOp + ", negativeOp=" + negativeOp + ", background="
				+ background + ", url=" + url + ", isHasIntegral=" + isHasIntegral + ", integral=" + integral
				+ ", debeatDuration=" + debeatDuration + ", isLimitPersonCount=" + isLimitPersonCount
				+ ", debeatPersonCount=" + debeatPersonCount + ", isStopped=" + isStopped + ", createTime=" + createTime
				+ "]";
	}
}

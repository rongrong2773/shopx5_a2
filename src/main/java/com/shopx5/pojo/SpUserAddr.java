package com.shopx5.pojo;

import java.io.Serializable;
import java.util.Date;

public class SpUserAddr implements Serializable{

	private String addrId;
    private String userId;
    private String receiverName;
    private String receiverPhone;
    private String receiverMobile;

    private String receiverState;
    private String receiverCity;
    private String receiverDistrict;

    private String receiverAddress;
    private String receiverZip;
    private Date createTime;
    private Date updateTime;
    private Integer df;

	//取receiverState[]
  	public String[] getState() {
  		String rstate = this.getReceiverState();
  		if (rstate != null && !"".equals(rstate)) {
  			String[] state = rstate.split("&");
  			return state;
  		}
  		return null;
  	}
	//取receiverCity[]
  	public String[] getCity() {
  		String rcity = this.getReceiverCity();
  		if (rcity != null && !"".equals(rcity)) {
  			String[] city = rcity.split("&");
  			return city;
  		}
  		return null;
  	}
	//取receiverDistrict[]
  	public String[] getDistrict() {
  		String rdistrict = this.getReceiverDistrict();
  		if (rdistrict != null && !"".equals(rdistrict)) {
  			String[] district = rdistrict.split("&");
  			return district;
  		}
  		return null;
  	}
  	
    public String getAddrId() {
        return addrId;
    }

    public void setAddrId(String addrId) {
        this.addrId = addrId == null ? null : addrId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName == null ? null : receiverName.trim();
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone == null ? null : receiverPhone.trim();
    }

    public String getReceiverMobile() {
        return receiverMobile;
    }

    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile == null ? null : receiverMobile.trim();
    }

    public String getReceiverState() {
        return receiverState;
    }

    public void setReceiverState(String receiverState) {
        this.receiverState = receiverState == null ? null : receiverState.trim();
    }

    public String getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity == null ? null : receiverCity.trim();
    }

    public String getReceiverDistrict() {
        return receiverDistrict;
    }

    public void setReceiverDistrict(String receiverDistrict) {
        this.receiverDistrict = receiverDistrict == null ? null : receiverDistrict.trim();
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress == null ? null : receiverAddress.trim();
    }

    public String getReceiverZip() {
        return receiverZip;
    }

    public void setReceiverZip(String receiverZip) {
        this.receiverZip = receiverZip == null ? null : receiverZip.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDf() {
        return df;
    }

    public void setDf(Integer df) {
        this.df = df;
    }
	@Override
	public String toString() {
		return "SpUserAddr [addrId=" + addrId + ", userId=" + userId + ", receiverName=" + receiverName
				+ ", receiverPhone=" + receiverPhone + ", receiverMobile=" + receiverMobile + ", receiverState="
				+ receiverState + ", receiverCity=" + receiverCity + ", receiverDistrict=" + receiverDistrict
				+ ", receiverAddress=" + receiverAddress + ", receiverZip=" + receiverZip + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", df=" + df + "]";
	}
    
}
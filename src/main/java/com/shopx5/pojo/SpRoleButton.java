package com.shopx5.pojo;

import java.io.Serializable;

public class SpRoleButton implements Serializable{
    private String roleId;

    private String buttonId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public String getButtonId() {
        return buttonId;
    }

    public void setButtonId(String buttonId) {
        this.buttonId = buttonId == null ? null : buttonId.trim();
    }
}
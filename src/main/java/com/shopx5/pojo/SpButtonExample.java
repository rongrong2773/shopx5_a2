package com.shopx5.pojo;

import java.util.ArrayList;
import java.util.List;

public class SpButtonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SpButtonExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andButtonIdIsNull() {
            addCriterion("BUTTON_ID is null");
            return (Criteria) this;
        }

        public Criteria andButtonIdIsNotNull() {
            addCriterion("BUTTON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andButtonIdEqualTo(String value) {
            addCriterion("BUTTON_ID =", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdNotEqualTo(String value) {
            addCriterion("BUTTON_ID <>", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdGreaterThan(String value) {
            addCriterion("BUTTON_ID >", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdGreaterThanOrEqualTo(String value) {
            addCriterion("BUTTON_ID >=", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdLessThan(String value) {
            addCriterion("BUTTON_ID <", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdLessThanOrEqualTo(String value) {
            addCriterion("BUTTON_ID <=", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdLike(String value) {
            addCriterion("BUTTON_ID like", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdNotLike(String value) {
            addCriterion("BUTTON_ID not like", value, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdIn(List<String> values) {
            addCriterion("BUTTON_ID in", values, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdNotIn(List<String> values) {
            addCriterion("BUTTON_ID not in", values, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdBetween(String value1, String value2) {
            addCriterion("BUTTON_ID between", value1, value2, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonIdNotBetween(String value1, String value2) {
            addCriterion("BUTTON_ID not between", value1, value2, "buttonId");
            return (Criteria) this;
        }

        public Criteria andButtonNameIsNull() {
            addCriterion("BUTTON_NAME is null");
            return (Criteria) this;
        }

        public Criteria andButtonNameIsNotNull() {
            addCriterion("BUTTON_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andButtonNameEqualTo(String value) {
            addCriterion("BUTTON_NAME =", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameNotEqualTo(String value) {
            addCriterion("BUTTON_NAME <>", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameGreaterThan(String value) {
            addCriterion("BUTTON_NAME >", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameGreaterThanOrEqualTo(String value) {
            addCriterion("BUTTON_NAME >=", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameLessThan(String value) {
            addCriterion("BUTTON_NAME <", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameLessThanOrEqualTo(String value) {
            addCriterion("BUTTON_NAME <=", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameLike(String value) {
            addCriterion("BUTTON_NAME like", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameNotLike(String value) {
            addCriterion("BUTTON_NAME not like", value, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameIn(List<String> values) {
            addCriterion("BUTTON_NAME in", values, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameNotIn(List<String> values) {
            addCriterion("BUTTON_NAME not in", values, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameBetween(String value1, String value2) {
            addCriterion("BUTTON_NAME between", value1, value2, "buttonName");
            return (Criteria) this;
        }

        public Criteria andButtonNameNotBetween(String value1, String value2) {
            addCriterion("BUTTON_NAME not between", value1, value2, "buttonName");
            return (Criteria) this;
        }

        public Criteria andQxNameIsNull() {
            addCriterion("QX_NAME is null");
            return (Criteria) this;
        }

        public Criteria andQxNameIsNotNull() {
            addCriterion("QX_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andQxNameEqualTo(String value) {
            addCriterion("QX_NAME =", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameNotEqualTo(String value) {
            addCriterion("QX_NAME <>", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameGreaterThan(String value) {
            addCriterion("QX_NAME >", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameGreaterThanOrEqualTo(String value) {
            addCriterion("QX_NAME >=", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameLessThan(String value) {
            addCriterion("QX_NAME <", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameLessThanOrEqualTo(String value) {
            addCriterion("QX_NAME <=", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameLike(String value) {
            addCriterion("QX_NAME like", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameNotLike(String value) {
            addCriterion("QX_NAME not like", value, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameIn(List<String> values) {
            addCriterion("QX_NAME in", values, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameNotIn(List<String> values) {
            addCriterion("QX_NAME not in", values, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameBetween(String value1, String value2) {
            addCriterion("QX_NAME between", value1, value2, "qxName");
            return (Criteria) this;
        }

        public Criteria andQxNameNotBetween(String value1, String value2) {
            addCriterion("QX_NAME not between", value1, value2, "qxName");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNull() {
            addCriterion("ORDER_NO is null");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNotNull() {
            addCriterion("ORDER_NO is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNoEqualTo(Integer value) {
            addCriterion("ORDER_NO =", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotEqualTo(Integer value) {
            addCriterion("ORDER_NO <>", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThan(Integer value) {
            addCriterion("ORDER_NO >", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("ORDER_NO >=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThan(Integer value) {
            addCriterion("ORDER_NO <", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThanOrEqualTo(Integer value) {
            addCriterion("ORDER_NO <=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoIn(List<Integer> values) {
            addCriterion("ORDER_NO in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotIn(List<Integer> values) {
            addCriterion("ORDER_NO not in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoBetween(Integer value1, Integer value2) {
            addCriterion("ORDER_NO between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotBetween(Integer value1, Integer value2) {
            addCriterion("ORDER_NO not between", value1, value2, "orderNo");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
package com.shopx5.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.shopx5.mapper.SpUserInfoMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.JsonUtils;
/**
 * @author tang S h o p X 5 多商户商城系统
 * @qq 1503501970
 */
@Controller
public class PictureController {
	@Autowired
	private SpUserInfoMapper userInfoMapper;
	
	//图片上传
	@RequestMapping("/picture/upload")
	@ResponseBody
	public String picUpload(MultipartFile uploadFile, HttpServletRequest request) {
		try {
			//重命名
			String fileStr = uploadFile.getOriginalFilename();
			String newFileName = IDUtils.genImageName() + fileStr.substring(fileStr.lastIndexOf("."));
			//保存目录
			String path = request.getSession().getServletContext().getRealPath("/images/picture");
			File uploadPic = new File(path + "/" + newFileName);
			if(!uploadPic.exists()) uploadPic.mkdirs();
			// 保存到硬盘
			uploadFile.transferTo(uploadPic);
			
			//响应上传图片的url
			Map result = new HashMap<>();
			result.put("error", 0);
			result.put("url", "/images/picture/"+newFileName);
			
			//判断用户上传头像 ,并保存
			SpUser user = (SpUser) request.getAttribute("spUser");
			if(user != null){
				System.out.println("当前用户: "+user.getUserId());
				SpUserInfo userInfo = userInfoMapper.selectByPrimaryKey(user.getUserId());
				userInfo.setHeadPic("/images/picture/"+newFileName);
				userInfoMapper.updateByPrimaryKeySelective(userInfo);
			}
			return JsonUtils.objectToJson(result);
		} catch (Exception e) {
			e.printStackTrace();
			Map result = new HashMap<>();
			result.put("error", 1);
			result.put("message", "图片上传失败");
			return JsonUtils.objectToJson(result);
		}
	}
}

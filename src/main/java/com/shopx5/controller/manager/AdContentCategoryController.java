package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.EasyUITreeNode;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.common.TreeNode;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SxContentCategory;
import com.shopx5.service.manager.AdContentCategoryService;
/**
 * ContentCategory表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/contentCategory")
public class AdContentCategoryController extends BaseController {
	@Autowired
	private AdContentCategoryService contentCategoryService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SxContentCategory> list = contentCategoryService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<SxContentCategory> pageBean = contentCategoryService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/contentCategory/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<SxContentCategory> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<SxContentCategory> datagrid = contentCategoryService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param SxContentCategory
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SxContentCategory sxContentCategory, HttpServletRequest request){
		try {
			//取用户
			//SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			sxContentCategory.setCreated(new Date());
			sxContentCategory.setUpdated(new Date());
			contentCategoryService.add(sxContentCategory);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SxContentCategory
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SxContentCategory sxContentCategory){
		try {
			sxContentCategory.setUpdated(new Date());
			contentCategoryService.update(sxContentCategory);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id, Model model){
		SxContentCategory sxContentCategory = contentCategoryService.getOne(id);
		return Result.ok(sxContentCategory);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			contentCategoryService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	////////////////////////
	
	//1 分类树 easyui tree
	@RequestMapping("/treeList")
	@ResponseBody
	public List<EasyUITreeNode> getContentTreeList(@RequestParam(name="id", defaultValue="0") Long parentId){
		//执行查询
		List<SxContentCategory> list = contentCategoryService.findAllByParenId(parentId);
		//转换成EasyUITreeNode列表
		List<EasyUITreeNode> resultList = new ArrayList<>();
		for (SxContentCategory sxContentCategory : list) {
			EasyUITreeNode node = new EasyUITreeNode();
			node.setId(sxContentCategory.getId());
			node.setText(sxContentCategory.getName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			node.setState(sxContentCategory.getIsParent() ? "closed" : "open");  //逆向工程将0,1转成boolean型
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	/**
	 * 返回easyui.tree数据
	 * @param  parentId  0||null||""取顶级 循环
	 * @return
	 */
	@RequestMapping("/findContentCategoryEUtree")
	@ResponseBody
	public List<TreeNode> findUserTreeList(@RequestParam(name="id", defaultValue="0") Long parentId){
		//查询全部列表
		List<SxContentCategory> list = contentCategoryService.findAllByParenId(parentId);
		//转换成TreeNode列表
		List<TreeNode> resultList = new ArrayList<>();
		for (SxContentCategory scc : list) {
			TreeNode node = new TreeNode();
			node.setId(String.valueOf(scc.getId()));
			node.setText(scc.getName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			node.setState(scc.getIsParent() ? "closed" : "open");  //逆向工程会将值0和1转成boolean型*
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
}

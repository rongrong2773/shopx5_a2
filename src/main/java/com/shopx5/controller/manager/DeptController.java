package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.common.TreeNode;
import com.shopx5.common.ZTreeNode;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SpDept;
import com.shopx5.service.manager.DeptService;
import com.shopx5.utils.IDUtils;
/**
 * Dept表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/dept")
public class DeptController extends BaseController {
	@Autowired
	private DeptService deptService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpDept> list = deptService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<SpDept> pageBean = deptService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/dept/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<SpDept> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("(button)datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<SpDept> datagrid = deptService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param SpDept
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SpDept spDept, String stateON){
		try {
			//补充数据
			if (stateON != null && stateON.equals("on")) {
				spDept.setState(1);
			}else {
				spDept.setState(0);
			}
			spDept.setDeptId(IDUtils.get32UUID());
			deptService.add(spDept);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SpDept
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SpDept spDept, String stateON){
		try {
			if (stateON != null && stateON.equals("on")) {
				spDept.setState(1);
			}else {
				spDept.setState(0);
			}
			deptService.update(spDept);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id){
		SpDept spDept = deptService.getOne(id);
		return Result.ok(spDept);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			deptService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	///////////////////////////////////////////////
	
	/**
	 * 返回jquery.zTree数据
	 * @return
	 */
	@RequestMapping("/findDeptZtree")
	@ResponseBody
	public List<ZTreeNode> findDeptTreeList(){
		//查询全部列表
		List<SpDept> list = deptService.findAll();
		//转换成TreeNode列表
		List<ZTreeNode> resultList = new ArrayList<>();
		for (SpDept spDept : list) {
			ZTreeNode node = new ZTreeNode();
		//	node.setId(spDept.getId());
		//	node.setpId(spDept.getParentId());
		//	node.setName(spDept.getName());
		//	node.setChecked("false");
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	/**
	 * 返回easyui.tree数据  此使用
	 * @param  parentId  0||null||""取顶级 循环
	 * @return
	 */
	@RequestMapping("/findDeptEUtree")
	@ResponseBody
	public List<TreeNode> findDeptTreeList(@RequestParam(name="id", defaultValue="0") String parentId){
		//查询全部列表
		List<SpDept> list = deptService.findAllByParenId(parentId);
		//转换成TreeNode列表
		List<TreeNode> resultList = new ArrayList<>();
		for (SpDept spDept : list) {
			TreeNode node = new TreeNode();
			node.setId(spDept.getDeptId());
			node.setText(spDept.getDeptName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			if(deptService.findAllByParenId(spDept.getDeptId()).size() > 0){
				node.setState("closed");
			}else {
				node.setState("open");
			}
		//	node.setState(spDept.getIsParent() ? "closed" : "open");  //逆向工程会将值0和1转成boolean型*
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
}

package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.common.TreeNode;
import com.shopx5.common.pojo.TreeGridArea;
import com.shopx5.common.pojo.TreeGridNode;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.ScArea;
import com.shopx5.service.manager.ScAreaService;
/**
 * Area表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/area")
public class ScAreaController extends BaseController {
	@Autowired
	private ScAreaService scareaService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<ScArea> list = scareaService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<ScArea> pageBean = scareaService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/area/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<ScArea> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<ScArea> datagrid = scareaService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param ScArea
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(ScArea scArea, HttpServletRequest request){
		try {
			//取用户
			//SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			if (scArea.getParentId() == null) {
				scArea.setParentId(0);
			}
			scareaService.add(scArea);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param ScArea
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(ScArea scArea){
		try {
			if (scArea.getParentId() == null) {
				scArea.setParentId(0);
			}
			scareaService.update(scArea);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(Integer id, Model model){
		ScArea scArea = scareaService.getOne(id);
		return Result.ok(scArea);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(Integer [] ids){
		try {
			scareaService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	////////////////////
	
	/**
	 * 返回easyui.treeGrid数据     暂停使用.. 原因:需一次加载完所有list
	 */
	@RequestMapping("/findTreeGrid_0")
	@ResponseBody
	public List<TreeGridNode> findTreeGrid(@RequestParam(name="id", defaultValue="0") Integer parentId){
		//取所有地址
		List<TreeGridNode> resultList = this.allAreaList(parentId);
		return resultList;
	}
	//循环封装 area List<TreeGridNode>                0
	public List<TreeGridNode> allAreaList(Integer parentId) {
		//1.获取列表
		List<ScArea> aList = scareaService.findAllByParentId(parentId);
		List<TreeGridNode> resultList = new ArrayList<>();
		if (aList != null && aList.size() > 0) {		
			//2.获取子类 递归处理
			for(ScArea scArea : aList){
				TreeGridNode node = new TreeGridNode(scArea); //多继承属性
				node.setAid(scArea.getId());
				node.setAname(scArea.getName());
				node.setDate(null);
				// 递归处理*
				node.setChildren(this.allAreaList(scArea.getId()));
				resultList.add(node);
			}
		}
		return resultList;
	}
	
	/**
	 * 返回easyui.treeGrid数据   异步加载
	 */
	@RequestMapping("/findTreeGrid")
	@ResponseBody
	public List<TreeGridArea> findTreeGridList(@RequestParam(name="id", defaultValue="0") Integer parentId){
		//查询列表
		List<ScArea> list = scareaService.findAllByParentId(parentId);
		//转换成TreeNode列表
		List<TreeGridArea> resultList = new ArrayList<>();
		for (ScArea ScArea : list) {
			TreeGridArea node = new TreeGridArea(ScArea);
			//如果节点下有子节点“closed”，如果没有子节点“open”
			if(scareaService.findAllByParentId(ScArea.getId()).size() > 0){
				node.setState("closed");
			}else {
				node.setState("open");
			}
			//添加到结果列表
			resultList.add(node);
		}
		System.out.println(resultList);
		return resultList;
	}
	
	/**
	 * 返回easyui.tree数据
	 * @param  parentId  0||null||""取顶级 循环
	 * @return
	 */
	@RequestMapping("/findAreaEUtree")
	@ResponseBody
	public List<TreeNode> findAreaTreeList(@RequestParam(name="id", defaultValue="0") Integer parentId){
		//查询全部列表
		List<ScArea> list = scareaService.findAllByParentId(parentId);
		//转换成TreeNode列表
		List<TreeNode> resultList = new ArrayList<>();
		for (ScArea scArea : list) {
			TreeNode node = new TreeNode();
			node.setId(String.valueOf(scArea.getId()));
			node.setText(scArea.getName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			if(scareaService.findAllByParentId(scArea.getId()).size() > 0){
				node.setState("closed");
			}else {
				node.setState("open");
			}
		//	node.setState(spUser.getIsParent() ? "closed" : "open");  //逆向工程会将值0和1转成boolean型*
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
}

package com.shopx5.controller.manager;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.ScLog;
import com.shopx5.service.manager.LogService;
import com.shopx5.utils.IDUtils;
/**
 * Log表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/log")
public class LogController extends BaseController {
	@Autowired
	private LogService logService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<ScLog> list = logService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<ScLog> pageBean = logService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/log/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<ScLog> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<ScLog> datagrid = logService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param ScLog
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(ScLog scLog, HttpServletRequest request){
		try {
			//取用户
			//SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			scLog.setLogId(IDUtils.get32UUID());
			scLog.setCreateTime(new Date());
			logService.add(scLog);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param ScLog
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(ScLog scLog){
		try {
			logService.update(scLog);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id, Model model){
		ScLog scLog = logService.getOne(id);
		return Result.ok(scLog);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			logService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
}

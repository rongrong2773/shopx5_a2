package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.EasyUITreeNode;
import com.shopx5.pojo.SpModule;
import com.shopx5.service.manager.ModuleService;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/manage/1")
public class EuTreeController1 {
	@Autowired
	ModuleService moduleService;
		
	//获得easyui树tree数据
	@RequestMapping("/treeList")
	@ResponseBody
	public List<EasyUITreeNode> findTree(@RequestParam(defaultValue="0") Integer parentId){
		System.out.println("------parentId: "+parentId);
		//取所有菜单
		List<SpModule> allMenuList = this.allMenuList(parentId);
		//所有菜单 循环 封装到EasyUITreeNode对象中 这次2次循环封装,更复杂★★       找共同点是:(spModule.getSubModule().size()>0)有子类就循环★
		List<EasyUITreeNode> list = new ArrayList<>();
		for (SpModule spModule : allMenuList) {
			EasyUITreeNode tree = new EasyUITreeNode();
			tree.setId(spModule.getModuleId());
			tree.setText(spModule.getName());
			tree.setCurl(spModule.getCurl());
			List<SpModule> subList = spModule.getSubModule();
			if (subList != null && subList.size() > 0) {				
				tree.setState("closed");
				//共同点在这,循环封装开始...
				List<EasyUITreeNode> subList2 = this.subList(subList);
				tree.setChildren(subList2);
			}
			list.add(tree);
		}
		System.out.println(list);
		return list;
	}
	
	//循环封装 EasyUITreeNode的List<EasyUITreeNode>
	public List<EasyUITreeNode> subList(List<SpModule> subList){
		//处理tree树的 children子类 循环赋值
		List<EasyUITreeNode> list = new ArrayList<>();
		for (SpModule spModule : subList) {
			EasyUITreeNode tree = new EasyUITreeNode();
			tree.setId(spModule.getModuleId());
			tree.setText(spModule.getName());
			tree.setCurl(spModule.getCurl());
			List<SpModule> subList2 = spModule.getSubModule();
			if (subList2.size() > 0) {				
				tree.setState("closed");
				this.subList(subList2);
			}
			list.add(tree);
		}
		return list;
	}
	
	//循环封装 module的List<SysPModule>           0
	public List<SpModule> allMenuList(Integer moduleId) {
		//1.获取父类 0
		List<SpModule> moduleList = moduleService.findListByParentId(moduleId);
		if (moduleList != null && moduleList.size() > 0) {		
			//2.获取子类 递归处理
			for(SpModule module : moduleList){
				module.setSubModule(this.allMenuList(module.getModuleId()));
			  //menu.setMENU_URL("menu/toEdit/MENU_ID/"+menu.getMENU_ID());  // 可附加
			  //menu.setTarget("treeFrame");                                 // 可附加
			}
		}
		return moduleList;
	}
}

package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EUser;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.common.TreeNode;
import com.shopx5.common.ZTreeNode;
import com.shopx5.controller.BaseController;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpRole;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.result.Role;
import com.shopx5.pojo.result.User;
import com.shopx5.service.manager.RoleService;
import com.shopx5.service.manager.UserInfoService;
import com.shopx5.service.manager.UserService;
import com.shopx5.utils.Encrypt;
/**
 * User表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/user")
public class UserController extends BaseController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserMapper userMapper;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpUser> list = userService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<SpUser> pageBean = userService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/shop/user/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<EUser> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<EUser> datagrid = userService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param SpUser
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SpUser spUser, String stateON, SpUserInfo spUserInfo, HttpServletRequest request){
		try {
			if (stateON != null && stateON.equals("on")) {
				spUser.setState(1);
			}else {
				spUser.setState(0);
			}
			userService.saveAll(spUser, spUserInfo);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SpUser
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SpUser spUser, String stateON, SpUserInfo spUserInfo){
		try {
			System.out.println("1"+spUser);
			System.out.println("2"+stateON);
			System.out.println("3"+spUserInfo);
			
			if (stateON != null && stateON.equals("on")) {
				spUser.setState(1);
			}else {
				spUser.setState(0);
			}
			userService.updateAll(spUser, spUserInfo);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id){
		SpUser spUser = userService.getOne(id);
		return Result.ok(spUser);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			userService.deleteAll(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	//////////////////////////////
	
	/**
	 * 返回jquery.zTree数据  此使用
	 * @return
	 */
	@RequestMapping("/findUserZtree")
	@ResponseBody
	public List<ZTreeNode> findUserZtreeList(){
		//查询全部列表
		List<User> list = userService.findUserAndInfo();
		//转换成zTreeNode列表
		List<ZTreeNode> resultList = new ArrayList<>();
		for (User user : list) {
			ZTreeNode node = new ZTreeNode();
			node.setId(user.getUserId());
			node.setpId(user.getUserInfo().getManagerId());
			node.setName(user.getUserName());
			node.setChecked("false");
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	/**
	 * 返回easyui.tree数据
	 * @param  parentId  0||null||""取顶级 循环
	 * @return
	 */
	@RequestMapping("/findUserEUtree")
	@ResponseBody
	public List<TreeNode> findUserTreeList(@RequestParam(name="id", defaultValue="0") String parentId){
		//查询全部列表
		List<SpUser> list = userService.findAllByParentId(parentId);
		//转换成TreeNode列表
		List<TreeNode> resultList = new ArrayList<>();
		for (SpUser spUser : list) {
			TreeNode node = new TreeNode();
		//	node.setId(spUser.getId());
		//	node.setText(spUser.getName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			/*if(deptService.findAll(spUser.getId()).size() > 0){
				node.setState("closed");
			}else {
				node.setState("open");
			}*/
		//	node.setState(spUser.getIsParent() ? "closed" : "open");  //逆向工程会将值0和1转成boolean型*
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	//返回 用户的角色 zTree数据
	@RequestMapping("/findRoleZtree")
	@ResponseBody
	public List<ZTreeNode> findRoleZtreeList(@RequestParam("id") String userId){
		//查询全部列表
		//取用户的角色列表
		User user = userService.findUserAndRole(userId);
		List<String> list = new ArrayList<>();
		//组合角色名		
		if (user != null) {		
			List<Role> roleList = user.getRoleList();
			for (Role role : roleList) {
				list.add(role.getName());
			}			
		}	
		//取角色列表
		List<SpRole> listR = roleService.findAll();		
		//转换成zTreeNode列表
		List<ZTreeNode> resultList = new ArrayList<>();
		for (SpRole spRole : listR) {
			ZTreeNode node = new ZTreeNode();
			node.setId(spRole.getRoleId());
			node.setpId(spRole.getParentId());
			node.setName(spRole.getName());
			//判断 选中*
			if(list.contains(spRole.getName())){
				node.setChecked("true");
			}else{
				node.setChecked("false");
			}			
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	//Manager管理后台 获取用户信息
	@RequestMapping("/getInfo")
	@ResponseBody
	public Result getInfo(HttpServletRequest request){
		User user = null;
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			user = userMapper.findUserAndInfo(spUser.getUserName());
		} catch (Exception e) {
			return Result.build(400, "获取失败");
		}
		return Result.ok(user);
	}
	//Manager管理后台 更新用户信息
	@RequestMapping("/updateInfo")
	@ResponseBody
	public Result updateInfo(HttpServletRequest request, SpUser spUser, SpUserInfo spUserInfo){
		try {
			//取用户
			SpUser user = (SpUser) request.getAttribute("spUser");
			//spUser
			spUser.setUserId(user.getUserId());
			String md5 = Encrypt.md5(spUser.getPassword(), spUser.getUserName());
			spUser.setPassword(md5);
			userService.update(spUser);
			//spUserInfo
			spUserInfo.setUserInfoId(user.getUserId());
			userInfoService.update(spUserInfo);
		} catch (Exception e) {
			return Result.build(400, "更新失败");
		}
		return Result.ok();
	}
}

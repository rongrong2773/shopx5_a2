package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.EasyUITreeNode;
import com.shopx5.mapper.result.RoleMapper;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpModule;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.result.Module;
import com.shopx5.pojo.result.Role;
import com.shopx5.pojo.result.User;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.manager.ModuleService;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/manage")
public class EuTreeController {
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private IndexService indexService;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;
	
/*	//获得easyui树tree数据
	@RequestMapping("/treeList")
	@ResponseBody
	public List<EasyUITreeNode> findTree(@RequestParam(defaultValue="0") Integer parentId){
		System.out.println("1------parentId: "+parentId);
		//取所有菜单
		List<SpModule> allMenuList = this.allMenuList(parentId);
		System.out.println("2------allMenuList: "+allMenuList);		
		//所有菜单 循环 封装到EasyUITreeNode对象中 这次2次循环封装,更复杂★★       找共同点是:(spModule.getSubModule().size()>0)有子类就循环★
		List<EasyUITreeNode> list = this.subList(allMenuList);
		System.out.println("3------EasyUITreeNodeList: "+list);
		return list;
	}
	
	//循环封装 EasyUITreeNode的List<EasyUITreeNode>
	public List<EasyUITreeNode> subList(List<SpModule> spModuleList){
		List<EasyUITreeNode> list = new ArrayList<>();
		for (SpModule spModule : spModuleList) { //spModule此对象中包含 多级### List<SpModule> subModule;			
			EasyUITreeNode tree = new EasyUITreeNode();
			tree.setId(spModule.getModuleId());
			tree.setText(spModule.getName());
			tree.setCurl(spModule.getCurl());
			List<SpModule> subList = spModule.getSubModule();
			if (subList != null && subList.size() > 0) {
				tree.setState("closed");
				//共同点在这,循环封装开始...
				List<EasyUITreeNode> list2 = this.subList(subList);
				tree.setChildren(list2);
			}
			list.add(tree);
		}
		return list;
	}
	
	//循环封装 module的List<SysPModule>           0
	public List<SpModule> allMenuList(Integer moduleId) {
		//1.获取父类 0
		List<SpModule> moduleList = moduleService.findListByParentId(moduleId);
		if (moduleList != null && moduleList.size() > 0) {		
			//2.获取子类 递归处理
			for(SpModule module : moduleList){
				module.setSubModule(this.allMenuList(module.getModuleId()));
			  //menu.setMENU_URL("menu/toEdit/MENU_ID/"+menu.getMENU_ID());  // 可附加
			  //menu.setTarget("treeFrame");                                 // 可附加
			}
		}
		return moduleList;
	}	
	*/
	
	//1--- 直接递归封装***
	
	//获得easyui树tree数据
	@RequestMapping("/treeList0")
	@ResponseBody
	public List<EasyUITreeNode> findTree(@RequestParam(defaultValue="0") Integer parentId){
		//所有菜单 循环 递归封装
		List<EasyUITreeNode> resultList = this.allMenuList(parentId);
		return resultList;
	}
	//循环封装 module的List<SysPModule>           0
	public List<EasyUITreeNode> allMenuList(Integer moduleId) {
		//1.获取父类 0
		List<SpModule> moduleList = moduleService.findListByParentId(moduleId);
		List<EasyUITreeNode> resultList = new ArrayList<>();
		if (moduleList != null && moduleList.size() > 0) {		
			//2.获取子类 递归处理
			for(SpModule spModule : moduleList){
				EasyUITreeNode tree = new EasyUITreeNode();
				tree.setId(spModule.getModuleId());
				tree.setText(spModule.getName());
				tree.setCurl(spModule.getCurl());
				tree.setChildren((this.allMenuList(spModule.getModuleId()))); //递归处理*
				resultList.add(tree);
			}
		}
		return resultList;
	}
	
	//2--- 模块权限过滤,递归封装 ***
	@RequestMapping("/treeList")
	@ResponseBody
	public List<EasyUITreeNode> findTreeVsModule(@RequestParam(defaultValue="0") Integer parentId, HttpServletRequest request){
		//1.获取用户角色模块
		SpUser spUser = indexService.getUser(request);
		//2.获取用户角色
		User user = userMapper.findUserAndRole(spUser.getUserId());
		if (user == null || user.getRoleList() == null) return null;
		List<Role> rList = user.getRoleList();
		//3.获取角色模块的id 封装到list中 用于对比
		List<Integer> list = new ArrayList<>();
		for (Role role : rList) {
			role = roleMapper.findRoleAndModule(role.getRoleId());
			if (role == null || role.getModuleList() == null) continue;
			List<Module> moduleList = role.getModuleList();
			for (Module module : moduleList) {
				list.add(module.getModuleId());
			}
		}
		//所有菜单 循环 递归封装
		List<EasyUITreeNode> resultList = this.findAllMenuList(parentId, list);
		return resultList;
	}
	//循环封装
	public List<EasyUITreeNode> findAllMenuList(Integer moduleId, List<Integer> list) {
		//1.获取父类 0
		List<SpModule> moduleList = moduleService.findListByParentId(moduleId);
		List<EasyUITreeNode> resultList = new ArrayList<>();
		if (moduleList != null && moduleList.size() > 0) {		
			//2.获取子类 递归处理
			for(SpModule spModule : moduleList){
				if (list.contains(spModule.getModuleId())) { //模块权限 包含当前ModuleId
					EasyUITreeNode tree = new EasyUITreeNode();
					tree.setId(spModule.getModuleId());
					tree.setText(spModule.getName());
					tree.setCurl(spModule.getCurl());
					tree.setChildren((this.findAllMenuList(spModule.getModuleId(), list))); //递归处理*
					resultList.add(tree);
				}
			}
		}
		return resultList;
	}
}

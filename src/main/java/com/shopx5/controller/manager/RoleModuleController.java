package com.shopx5.controller.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.Result;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SpRoleModule;
import com.shopx5.pojo.SpUser;
import com.shopx5.service.manager.RoleModuleService;
/**
 * RoleModule表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/roleModule")
public class RoleModuleController extends BaseController {
	@Autowired
	private RoleModuleService roleModuleService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpRoleModule> list = roleModuleService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 增加
	 * @param SpRoleModule
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(String roleId, String moduleIds, HttpServletRequest request){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//增加数据
			roleModuleService.add(roleId, moduleIds);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SpRoleModule
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SpRoleModule spRoleModule){
		try {
			roleModuleService.update(spRoleModule);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id, Model model){
		SpRoleModule spRoleModule = roleModuleService.getOne(id);
		return Result.ok(spRoleModule);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			roleModuleService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
}

package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.common.ZTreeNode;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SpButton;
import com.shopx5.pojo.SpModule;
import com.shopx5.pojo.SpRole;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.result.Button;
import com.shopx5.pojo.result.Module;
import com.shopx5.pojo.result.Role;
import com.shopx5.service.manager.ButtonService;
import com.shopx5.service.manager.ModuleService;
import com.shopx5.service.manager.RoleService;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.RightsHelper;
/**
 * Role表现层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/manage/role")
public class RoleController extends BaseController {
	@Autowired
	private RoleService roleService;
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private ButtonService buttonService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpRole> list = roleService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<SpRole> pageBean = roleService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/role/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<SpRole> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("(role)datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<SpRole> datagrid = roleService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param SpRole
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SpRole spRole, HttpServletRequest request){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			spRole.setRoleId(IDUtils.get32UUID());
			spRole.setUpdateTime(new Date());
			roleService.add(spRole);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SpRole
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SpRole spRole){
		try {
			spRole.setUpdateTime(new Date());
			roleService.update(spRole);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id, Model model){
		SpRole spRole = roleService.getOne(id);
		return Result.ok(spRole);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			roleService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	/////////////////////////////////
	
	//返回1  角色的模块  zTree数据
	@RequestMapping("/findModuleZtree")
	@ResponseBody
	public List<ZTreeNode> findRoleModuleZtreeList(@RequestParam("id") String roleId){
		//取角色的模块列表
		Role role = roleService.findRoleAndModule(roleId);
		List<String> list = new ArrayList<>();
		//组合模块名
		if (role != null) {		
			List<Module> moduleList = role.getModuleList();
			for (Module module : moduleList) {
				list.add(module.getName());
			}			
		}	
		//取模块列表 SH OP X 5
		List<SpModule> mList = moduleService.findAll();		
		//转换成zTreeNode列表
		List<ZTreeNode> resultList = new ArrayList<>();
		for (SpModule spModule : mList) {
			ZTreeNode node = new ZTreeNode();
			node.setId(String.valueOf(spModule.getModuleId()));
			node.setpId(String.valueOf(spModule.getParentId()));
			node.setName(spModule.getName());
			//判断 选中*
			if(list.contains(spModule.getName())){
				node.setChecked("true");
			}else{
				node.setChecked("false");
			}
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	//返回2  角色的按钮  zTree数据
	@RequestMapping("/findButtonZtree")
	@ResponseBody
	public List<ZTreeNode> findRoleButtonZtreeList(@RequestParam("id") String roleId){
		//取角色的模块列表
		Role role = roleService.findRoleAndButton(roleId);
		List<String> list = new ArrayList<>();
		//组合模块名
		if (role != null) {		
			List<Button> buttonList = role.getButtonList();
			for (Button button : buttonList) {
				list.add(button.getButtonName());
			}			
		}	
		//取模块列表
		List<SpButton> bList = buttonService.findAll();		
		//转换成zTreeNode列表
		List<ZTreeNode> resultList = new ArrayList<>();
		for (SpButton spButton : bList) {
			ZTreeNode node = new ZTreeNode();
			node.setId(String.valueOf(spButton.getButtonId()));
		//	node.setpId();
			node.setName(spButton.getButtonName());
			//判断 选中*
			if(list.contains(spButton.getButtonName())){
				node.setChecked("true");
			}else{
				node.setChecked("false");
			}
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	//返回3  角色的[CURD]模块权限  zTree数据
	@RequestMapping("/findRoleQXZtree")
	@ResponseBody
	public List<ZTreeNode> findRoleCURDZtreeList(@RequestParam("id") String roleId, String curdQX){
		//取角色的模块列表
		SpRole spRole = roleService.getOne(roleId);
		List<String> list = new ArrayList<>();
		//组合模块名
		String rightQx = null;
		if (spRole != null && curdQX.equals("addQX")) {		
			 rightQx = spRole.getAddQx();			
		}
		if (spRole != null && curdQX.equals("delQX")) {		
			 rightQx = spRole.getDelQx();			
		}
		if (spRole != null && curdQX.equals("editQX")) {		
			 rightQx = spRole.getEditQx();			
		}
		if (spRole != null && curdQX.equals("seeQX")) {		
			 rightQx = spRole.getSeeQx();			
		}
		//取模块列表
		List<SpModule> mList = moduleService.findAll();		
		//转换成zTreeNode列表
		List<ZTreeNode> resultList = new ArrayList<>();
		for (SpModule spModule : mList) {
			ZTreeNode node = new ZTreeNode();
			node.setId(String.valueOf(spModule.getModuleId()));
			node.setpId(String.valueOf(spModule.getParentId()));
			node.setName(spModule.getName());
			//判断 选中*
			if(rightQx != null && RightsHelper.testRights(rightQx, spModule.getModuleId())){
				node.setChecked("true");
			}else{
				node.setChecked("false");
			}
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	//添加角色  addQX权限(操作模块)
	@RequestMapping("/updateQX")
	@ResponseBody
	public Result addQX(String roleId, String moduleIds, String curdQX,  HttpServletRequest request){
		try {
			//取用户
			//SpUser spUser = (SpUser) request.getAttribute("spUser");
			//增加数据
			roleService.addQX(roleId, moduleIds, curdQX);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
}

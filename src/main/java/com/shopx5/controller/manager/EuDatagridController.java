package com.shopx5.controller.manager;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.mapper.ScLogMapper;
import com.shopx5.pojo.ScLog;
import com.shopx5.pojo.ScLogExample;
import com.shopx5.pojo.ScLogExample.Criteria;
import com.shopx5.utils.DateUtils;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.JsonUtils;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/manage") ///
public class EuDatagridController {
	@Autowired
	private ScLogMapper logMapper;
	
	//1.datagrid测试 栏目1
	@RequestMapping("/datagrid")
	@ResponseBody
	public EasyUIDataGridResult jsonDate(@RequestParam(defaultValue="1") Integer page , Integer rows, String sort, String order, 
										 String username, String date_from, String date_to) throws Exception{
//		1.无分页,查询所有信息
//		SysLogExample example = new SysLogExample();
//		List<SysLog> list = logMapper.selectByExample(example);
//		return list;
		
//		2.查询分页  使用datagrid字段
		EasyUIDataGridResult datagrid = this.getResult(page,rows,sort,order,username,date_from,date_to);
		Thread.sleep(300);
		return datagrid;
	}
		
	public EasyUIDataGridResult getResult(Integer page,Integer rows,String sort,String order,String username,
										   String date_from, String date_to) {
		switch (sort) {
			case "username":
				sort = "USERNAME";
				break;
			case "content":
				sort = "CONTENT";
				break;
			case "ipAddress":
				sort = "IP_ADDRESS";
				break;
			case "logTime":
				sort = "LOG_TIME";
				break;
			default:
				System.out.println("非法参数datagrid...");
				break;
		}
		EasyUIDataGridResult pageBean = new EasyUIDataGridResult();
		//设置分页信息   页码,数量
		PageHelper.startPage(page, rows);
		//取所有部门
		ScLogExample example = new ScLogExample();
		Criteria criteria = example.createCriteria();
//		criteria.andStateEqualTo(1);
		//搜索:查询名称
		if (StringUtils.isNoneBlank(username)) {
			criteria.andUsernameLike("%"+username+"%");
		}
		//搜索:查询时间
		if (StringUtils.isNoneBlank(date_from) && StringUtils.isNoneBlank(date_to)) {
			criteria.andCreateTimeBetween(DateUtils.stringToDate(date_from), DateUtils.stringToDate(date_to));
		}		
		example.setOrderByClause(sort+" "+order); //添加排序
		List<ScLog> list = logMapper.selectByExample(example);  //核心:这时已通过配置的插件interceptor拦截,查询limit1,5 
		//取查询结果                                                                                             //结果list实质是Page<E> extends ArrayList<E>
		PageInfo<ScLog> pageInfo = new PageInfo<>(list);        //Page封装到PageInfo
		//设置数据
		pageBean.setRows(list);
		pageBean.setTotal(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
//		pageBean.setCurrPage(pageInfo.getPageNum());
//		pageBean.setPageSize(pageInfo.getPageSize());
//		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
//		pageBean.setTotalPage(pageInfo.getPages());
//		pageBean.setCurrSize(pageInfo.getSize());
		return pageBean;
	}
	
	//2 新增 表格add,增加对象
	@RequestMapping("/grid_add")
	@ResponseBody
	public String addUser(String row){
		ScLog scLog = JsonUtils.jsonToPojo(row, ScLog.class);
		//补充数据
		String uid = IDUtils.get32UUID();
		scLog.setLogId(uid);
		//保存数据
		int key = logMapper.insert(scLog);
		//
		return String.valueOf(key);
	}
	
	//3 新增 弹出add,增加字段
	@RequestMapping("/data_add")
	@ResponseBody
	public String addUser(ScLog scLog, String auth){
		//补充数据
		String uid = IDUtils.get32UUID();
		scLog.setLogId(uid);                System.out.println(auth+"---"+scLog);		
		//保存数据
		int key = logMapper.insert(scLog);
		//
		return String.valueOf(key);
	}
	
	//4 获取
	@RequestMapping(value="/grid_get", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public String getManager(String logId){
		System.out.println(logId);
		ScLog scLog = logMapper.selectByPrimaryKey(logId);
		String json = JsonUtils.objectToJson(scLog);
		return json;
	}
		
	//5 修改
	@RequestMapping("/grid_update")
	@ResponseBody
	public String updateUser(String row){
		ScLog scLog = JsonUtils.jsonToPojo(row, ScLog.class);
		//保存数据
		int key = logMapper.updateByPrimaryKeySelective(scLog);
		return String.valueOf(key);
	}
	
	//6 修改2 弹出修改:接收的是每个字段
	@RequestMapping("/grid_update2")
	@ResponseBody
	public String updateUser(ScLog scLog){
		System.out.println(scLog);
		//保存数据
		int key = logMapper.updateByPrimaryKeySelective(scLog);
		return String.valueOf(key);
	}
		
	//7 删除
	@RequestMapping("/grid_delete")
	@ResponseBody
	public String deleteLogById(@RequestParam("ids") String[] ids){
		for (String id : ids) {
		 	this.deleteLogByIds(id);       //批量删除,或更新state=0
		}
		return String.valueOf(ids.length);
	}
	//批量删除,或更新
	public void deleteLogByIds(String id){
		//设置数据 更新
		//SysPUser sysPUser = pUserMapper.selectByPrimaryKey(id);
		//sysPUser.setState(0);
		//pUserMapper.updateByPrimaryKeySelective(sysPUser);
		//删除
		logMapper.deleteByPrimaryKey(id);
	}	
	
	//easyui树datagrid数据
	@RequestMapping(value="/manager_data", produces="application/json")
	@ResponseBody
	public String findData(){		
		String s = "[{\"id\":\"12\", \"manager\":\"22\", \"auth\":\"33\", \"date\":\"66\"}]";
		return s;
	}
		
	//easyui其他组件  form 等加载
	@RequestMapping("/formUI")
	@ResponseBody
	public String formUI(String name,String email,String code){		
		String s = "[{\"name\":\""+name+"\", \"email\":\""+email+"\", \"code\":\""+code+"\"}]";
		return s;
		
	}
	@RequestMapping("/formUI2")
	@ResponseBody
	public String formUI2(){		
		String s = "{\"name\":\"123666\", \"email\":\"33@qq.com\"}";
		return s;
	}
}

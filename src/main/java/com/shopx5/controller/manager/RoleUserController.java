package com.shopx5.controller.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SpRoleUser;
import com.shopx5.pojo.SpUser;
import com.shopx5.service.manager.RoleUserService;
/**
 * RoleUser表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/roleUser")
public class RoleUserController extends BaseController {
	@Autowired
	private RoleUserService roleUserService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpRoleUser> list = roleUserService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<SpRoleUser> pageBean = roleUserService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/roleUser/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<SpRoleUser> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<SpRoleUser> datagrid = roleUserService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param SpRoleUser
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(String userId, String roleIds, HttpServletRequest request){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//增加数据
			roleUserService.add(userId, roleIds);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id, Model model){
		SpRoleUser spRoleUser = roleUserService.getOne(id);
		return Result.ok(spRoleUser);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			roleUserService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
}

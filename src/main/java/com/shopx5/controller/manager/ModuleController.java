package com.shopx5.controller.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.DataGridParam;
import com.shopx5.common.EasyUIDataGridResult;
import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.common.TreeNode;
import com.shopx5.common.pojo.TreeGridModule;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SpModule;
import com.shopx5.pojo.SpUser;
import com.shopx5.service.manager.ModuleService;
/**
 * Module表现层
 * @author XS多商户商城系统
 */
@Controller
@RequestMapping("/manage/module")
public class ModuleController extends BaseController {
	@Autowired
	private ModuleService moduleService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpModule> list = moduleService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public String findPage(@RequestParam(defaultValue="1") Integer page, Model model){
		PageBean<SpModule> pageBean = moduleService.findPage(page);
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/manager/module/list";
	}
	/**
	 * 返回datagrid分页数据  EasyUI
	 * @return
	 */
	@RequestMapping("/findGridPage")
	@ResponseBody
	private EasyUIDataGridResult<SpModule> getDataGridResult(DataGridParam param) throws Exception {
		logger.info("(module)datagrid参数: "+param);
		//查询 datagrid分页数据
		EasyUIDataGridResult<SpModule> datagrid = moduleService.getDataGridResult(param);
		return datagrid;
	}
	/**
	 * 增加
	 * @param Module
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SpModule spModule, String stateON, HttpServletRequest request){
		try {
			if (spModule.getParentId() == null) {
				spModule.setParentId(0);
			}
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			if (stateON != null && stateON.equals("on")) {
				spModule.setState(1);
			}else {
				spModule.setState(0);
			}
			spModule.setCreateTime(new Date());
			spModule.setUpdateTime(new Date());
			moduleService.add(spModule);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param Module
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SpModule spModule){
		try {
			spModule.setUpdateTime(new Date());
			moduleService.update(spModule);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(Integer id, Model model){
		SpModule spModule = moduleService.getOne(id);
		return Result.ok(spModule);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(Integer [] ids){
		try {
			moduleService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	//////////////////////////////
	
	/**
	 * 返回easyui.treeGrid数据   异步加载
	 */
	@RequestMapping("/findTreeGrid")
	@ResponseBody
	public List<TreeGridModule> findTreeGridList(@RequestParam(name="id", defaultValue="0") Integer parentId, DataGridParam param){
		//查询列表
		List<SpModule> list = moduleService.findAllByParentId(parentId, param);
		//转换成TreeNode列表
		List<TreeGridModule> resultList = new ArrayList<>();
		for (SpModule spModule : list) {
			TreeGridModule node = new TreeGridModule(spModule);
			//如果节点下有子节点“closed”，如果没有子节点“open”
			if(moduleService.findAllByParentId(spModule.getModuleId()).size() > 0){
				node.setState("closed");
			}else {
				node.setState("open");
			}
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	/**
	 * 返回easyui.tree数据  此使用
	 * @param  parentId  0||null||""取顶级 循环
	 * @return
	 */
	@RequestMapping("/findModuleEUtree")
	@ResponseBody
	public List<TreeNode> findDeptTreeList(@RequestParam(name="id", defaultValue="0") Integer parentId){
		//查询全部列表
		List<SpModule> list = moduleService.findAllByParentId(parentId);
		//转换成TreeNode列表
		List<TreeNode> resultList = new ArrayList<>();
		for (SpModule spModule : list) {
			TreeNode node = new TreeNode();
			node.setId(String.valueOf(spModule.getModuleId()));
			node.setText(spModule.getName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			if(moduleService.findAllByParentId(spModule.getModuleId()).size() > 0){
				node.setState("closed");
			}else {
				node.setState("open");
			}
		//	node.setState(spDept.getIsParent() ? "closed" : "open");  //逆向工程会将值0和1转成boolean型*
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
}

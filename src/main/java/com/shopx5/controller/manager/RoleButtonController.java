package com.shopx5.controller.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.Result;
import com.shopx5.controller.BaseController;
import com.shopx5.pojo.SpRoleButton;
import com.shopx5.service.manager.RoleButtonService;
/**
 * RoleButton表现层
 * @author tang ShopX5多商户商城系统
 */
@Controller
@RequestMapping("/manage/roleButton")
public class RoleButtonController extends BaseController {
	@Autowired
	private RoleButtonService roleButtonService;
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	@ResponseBody
	public Result findAll(){
		//查询全部列表
		List<SpRoleButton> list = roleButtonService.findAll();
		//返回结果
		return Result.ok(list);
	}
	/**
	 * 增加
	 * @param SpRoleButton
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(String roleId, String buttonIds, HttpServletRequest request){
		try {
			//取用户
			//SpUser spUser = (SpUser) request.getAttribute("spUser");
			//增加数据
			roleButtonService.add(roleId, buttonIds);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SpRoleButton
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SpRoleButton spRoleButton){
		try {
			roleButtonService.update(spRoleButton);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	@ResponseBody
	public Result getOne(String id, Model model){
		SpRoleButton spRoleButton = roleButtonService.getOne(id);
		return Result.ok(spRoleButton);
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			roleButtonService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
}

package com.shopx5.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.shopx5.mapper.ScLogMapper;
import com.shopx5.pojo.ScLog;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.LoggerSlf4j;
/**
 * Controller继承
 * @author XS 多商户商城系统
 */
public class BaseController {
	@Autowired
	ScLogMapper logMapper;
	
	/**
	 * 创建日志slf4j
	 */
	protected Logger logger = LoggerSlf4j.getLogger(this.getClass());
	public static void logBefore(Logger logger, String message){
		logger.info("-----logger__start-----");
		logger.info(message);
	}
	public static void logAfter(Logger logger, String message){
		logger.info("-----logger__end-----");
		logger.info(message);
	}
	
	/**
	 * 操作日志,持久化
	 */
	public void saveLog(String username, String content, HttpServletRequest request){
		String ip = "";
		if (request.getHeader("x-forwarded-for") == null) {  
			ip = request.getRemoteAddr();  
	    }else{
	    	ip = request.getHeader("x-forwarded-for");  
	    }
		ScLog log = new ScLog();
		log.setLogId(IDUtils.get32UUID());
		log.setUsername(username);
		log.setContent(content);
		log.setIpAddress(ip);
		logMapper.insert(log); //保存
	}
}

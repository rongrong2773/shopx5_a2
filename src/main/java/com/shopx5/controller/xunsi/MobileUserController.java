package com.shopx5.controller.xunsi;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.common.Result;
import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbThesis;
import com.shopx5.pojo.TbUserConnected;
import com.shopx5.service.xunsi.MobileUserService;
import com.shopx5.service.xunsi.PicCharVideoArticleService;
import com.shopx5.service.xunsi.ThesisService;
import com.shopx5.utils.TransmissionTemplateUtils;

import freemarker.core.ReturnInstruction.Return;


/**
 * 移动端用户控制器
 * @author sjx
 * 2019年6月29日 上午10:51:42
 */
@Controller
@CrossOrigin
@RequestMapping("/mobile/xunsi/user")
public class MobileUserController {

	@Autowired
	private MobileUserService mUsrService;
	
	@Autowired
	private ThesisService thesisService;
	
	@Autowired
	private PicCharVideoArticleService picCharVideoService;
	
	// 个人中心 - 拉黑/去拉黑
	@ResponseBody
	@RequestMapping(value="/shieldUser",method=RequestMethod.POST)
	private Object shieldUser(@RequestBody JSONObject obj, HttpServletRequest request){
		
		String userId = obj.getString("userId");
		String aimUserId = obj.getString("aimUserId");
		String clientId = obj.getString("clientId");
		String token = request.getHeader("Authorization");
		try {
			TbMobileUser mobileUser = mUsrService.findEntityById(aimUserId);
			System.out.println(mobileUser);
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "无效的aimUserId,拒绝访问","FAIL");
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "无效的userId,拒绝访问","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "无效的token,拒绝访问","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "无效的clientId,拒绝访问","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "无效的userId,拒绝访问","FAIL");
		}
		/**
		 * 更新拉黑列表
		 */
		String shieldUserList = mUsrService.findShieldUserById(userId);
		boolean flag = true;    // 判断操作是拉黑/取消拉黑标记,true - 拉黑;  false - 取消拉黑
		if(!(null == shieldUserList || "".equals(shieldUserList))){
			String[] userArr = shieldUserList.split(",");
			List<String> list = new ArrayList<String>();
			for(int i=0; i<userArr.length;i++){
				if (userArr[i].trim().equals(aimUserId)) {
					flag = false;
				}else {
					list.add(userArr[i].trim());
				}
			}
			if (flag == true) {
				list.add(aimUserId.trim());
			}
			if (0 == list.size()) {
				aimUserId = "";
			}else {
				aimUserId = list.toString().replace("[", "").replace("]", "").replace(" ", "");
			}
		}
		mUsrService.shieldUser(Long.parseLong(userId),aimUserId+",");
		if (flag==true) {
			return Result.build(200, "拉黑成功!","SUCCESS");
		}else if (flag==false) {
			return Result.build(200, "取消拉黑成功!","SUCCESS");
		}
		return Result.build(500, "程序执行异常!","FAIL");
	} 
	
	
	// 增加/修改备注
	@ResponseBody
	@RequestMapping(value="/updateRemark",method = RequestMethod.POST)
	private Object modifyRemark(@RequestBody JSONObject obj, HttpServletRequest request){
		
		String userId = obj.getString("userId");
		String aimUserId = obj.getString("aimUserId");
		String clientId = obj.getString("clientId");
		String aimUserRemark = obj.getString("aimUserRemark");
		String token = request.getHeader("Authorization");
		if (null == aimUserRemark || "".equals(aimUserRemark)) {
			return Result.build(200, "aimUserRemark不合法,拒绝访问","FAIL");
		}
		try {
			TbMobileUser mobileUser = mUsrService.findEntityById(aimUserId);
			System.out.println(mobileUser);
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "无效的aimUserId,拒绝访问","FAIL");
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "无效的userId,拒绝访问","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "无效的token,拒绝访问","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "无效的clientId,拒绝访问","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "无效的userId,拒绝访问","FAIL");
		}
		try {
			mUsrService.updateRemark(userId, aimUserId, aimUserRemark);
			return Result.build(200, "备注修改成功!", "SUCCESS");
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "系统内部错误!", "FAIL");
		}
	}
	
	
	// 主页获取趣味相同的用户
	@ResponseBody
	@RequestMapping(value="/getSimiliarUserList", method=RequestMethod.POST)
	private Object getSimiliarUserList(@RequestBody JSONObject obj, HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		
		String userId = obj.getString("userId");
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		TbMobileUser entity = null;
		try {
			entity = mUsrService.findEntityByIdToken(Long.parseLong(userId), token);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "用户id不合法");
			map.put("status", "FAIL");
			return map;
		}
		
		if (null == entity) {
			map.put("msg", "用户id与token信息不符,校验失败");
			map.put("status", "FAIL");
			return map;
		}
		TbMobileUser entity2 = mUsrService.findEntity(userId, token, clientId);
		if(null == entity2){
			map.put("msg", "无效的clientId");
			map.put("status","FAIL");
			return map;
		}else {
			// 取20个 ,limit 20
			List<TbMobileUser> list = mUsrService.findSimiliarUserList();
			return list;
		}
	}
	
	
	// 返回个人中心信息
	@ResponseBody
	@RequestMapping(value="/getPersonalCenterInfo",method = RequestMethod.POST)
	private Object getPersonalCenterInfo(@RequestBody JSONObject obj ,HttpServletRequest request){
		
		List<Object> list = new ArrayList<Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		String userId = obj.getString("userId");
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		TbMobileUser entity = null;
		try {
			entity = mUsrService.findEntityByIdToken(Long.parseLong(userId), token);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "用户id不合法");
			map.put("status", "FAIL");
			return map;
		}
		
		if (null == entity) {
			map.put("msg", "用户id与token信息不符,校验失败");
			map.put("status", "FAIL");
			return map;
		}
		TbMobileUser entity2 = mUsrService.findEntity(userId, token, clientId);
		if(null == entity2){
			map.put("msg", "无效的clientId");
			map.put("status","FAIL");
			return map;
		}else {
			Integer picCharCount = picCharVideoService.findPicCharCountById(userId);  // 图文/视频
			Integer thesisCount = thesisService.findThesisCountById(userId);    // 论题
//			Integer xingxiuCount = xiuxingService.findXingxiuCountById(userId);    // 星秀
			Integer articleCount = picCharVideoService.findArtiCountById(userId);    // 原创文章
			
			Integer msgCount = mUsrService.findMsgCountById(userId);    // 消息数目
			
			map.put("userInfo", entity2);
			map.put("publishCount", picCharCount+thesisCount+articleCount);
			map.put("msgCount", msgCount);
			list.add(map);
			return list;
		}
	}
	
	// 获取"个人中心 - 我的消息"分页列表
	@ResponseBody
	@RequestMapping(value="getMsgByPage" ,method = RequestMethod.POST)
	private Object getMsgByPage(@RequestBody JSONObject obj, HttpServletRequest request){
		
		Map<String, Object> map = new HashMap<String, Object>();
		String userId = obj.getString("userId");
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		Integer pageNum = Integer.parseInt(obj.getString("pageNum"));
		Integer pageSize = Integer.parseInt(obj.getString("pageSize"));
		try {
			TbMobileUser entity = mUsrService.findEntityByIdToken(Long.parseLong(userId), token);
			if (null == entity) {
				map.put("msg", "用户id与token信息不符,校验失败");
				map.put("data", "FAIL");
				map.put("status", "400");
				return map;
			}
			TbMobileUser entity2 = mUsrService.findEntity(userId, token, clientId);
			if(null == entity2){
				map.put("msg", "无效的clientId");
				map.put("data","FAIL");
				map.put("status", "400");
				return map;
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "用户id不合法");
			map.put("data", "FAIL");
			map.put("status", "400");
			return map;
		}
		PageResult pageResult = mUsrService.findMsgByPage(pageNum,pageSize,userId);
		return pageResult;
	}
	
	
	// 获取用户好友列表
	@RequestMapping("/getUsrFrendsList/allInfo")
	@ResponseBody
	private PageResult findUsrAllInfo(@RequestBody JSONObject jObject, HttpServletRequest request) throws Exception{
		
		Long ownerId =Long.parseLong(jObject.getString("userId"));
		Integer page = Integer.parseInt(jObject.getString("page"));
		Integer rows = Integer.parseInt(jObject.getString("rows"));
		if (ownerId == null || ownerId.toString().length() != 15) {
			throw new Exception("用户id不合法");
		}
		
		return mUsrService.findPageUsrAllInfo(ownerId,page,rows);
	}
	
//	/**
//	 * 返回分页
//	 * @return
//	 */
//	@RequestMapping("/findPage")
//	@ResponseBody
//	public PageResult findPage(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows){
//		return newsService.findPage(page,rows);
//	}
	
	// 获取用户好友列表
	@RequestMapping("/getUsrFriendList/partInfo")
	@ResponseBody
	private PageResult findUsrPartInfo(@RequestBody JSONObject jObject, HttpServletRequest request) throws Exception{
		
		Long ownerId =Long.parseLong(jObject.getString("userId"));
		Integer page = Integer.parseInt(jObject.getString("page"));
		Integer rows = Integer.parseInt(jObject.getString("rows"));
		if (ownerId == null || ownerId.toString().length() != 15) {
			throw new Exception("用户id不合法");
		}
		
		return mUsrService.findPageUsrPartInfo(ownerId,page,rows);
	}
	
	// 获取我关注的人、认证用户、关注我的人
	@RequestMapping(value = "/getRelatedUser", method = RequestMethod.POST)
	@ResponseBody
	private Object findRelatedUsers(@RequestBody JSONObject jsonObject,HttpServletRequest request) throws IOException{

		Map<Object, Object> map = new HashMap<Object,Object>();
		String userId =  jsonObject.getString("userId");
		Integer pageNum = Integer.parseInt(jsonObject.getString("pageNum"));
		Integer pageSize = Integer.parseInt(jsonObject.getString("pageSize"));
		String token = request.getHeader("Authorization");
		String clientId = jsonObject.getString("clientId");
		String type = jsonObject.getString("type");
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "userId无效,拒绝访问!","FAIL");
		}
		
//		List<TbUserConnected> idList = new ArrayList<TbUserConnected>();    // 查找我关注的人id列表。 --- 我关注的人，以我的id为cascading_id
		if ("1".equals(type)) {
			PageResult page = mUsrService.findPage(Long.parseLong(userId), pageNum, pageSize);
			return page;
		}else if ("2".equals(type)) {
			PageResult page = mUsrService.findPage2(Long.parseLong(userId), pageNum, pageSize);    // 查找关注我的人。--- 以我的id作为cascaded_id
			return page;
		}
		map.put("status", 500);
		map.put("msg", "服务器内部错误");
		map.put("data", "FAIL");
		return map;
//		List<TbMobileUser> idList3 = mUsrService.findaAuthUsrsList();
	}
	
	// 关注/取消关注
	@RequestMapping(value="addConnWith", method=RequestMethod.POST)
	@ResponseBody
	private Object addConnWith(@RequestBody JSONObject jsonObject,HttpServletRequest request, HttpServletResponse response){
		
		String token = request.getHeader("Authorization");
		String clientId = jsonObject.getString("clientId");
		String userId = jsonObject.getString("userId");    // 登陆用户id
		String aimUsrId = jsonObject.getString("aimUserId");    // 被关注的人

		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "请求参数usrId无效，拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "无效的token,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "无效的clientId,拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数usrId无效，拒绝访问!", "FAIL");
		}
		TbUserConnected userConnected = mUsrService.findEntityByUsrAimUsrId(Long.parseLong(userId), Long.parseLong(aimUsrId));
		if (null == userConnected) {
			try {
				// 无关联，建立
				mUsrService.addConnWith(Long.parseLong(userId), Long.parseLong(aimUsrId));
				try {
					TbMobileUser aimEntity = mUsrService.findEntityById(aimUsrId);
					
					Map<String, String> m = new HashMap<String, String>();
					m.put("pushType", "4");
					m.put("pushContent", aimEntity.getNickName() + "关注了您");
					m.put("sourceUsrId", userId);
					TransmissionTemplateUtils.pushTransMsg(clientId, JSONObject.toJSONString(m));
				} catch (Exception e) {
					System.out.println("=====:推送失败，请优先排查推送平台服务是否异常");
					e.printStackTrace();
				}
				return Result.build(200, "关注成功", "SUCCESS");
			} catch (Exception e) {
				return Result.build(500, "服务器内部错误，请稍后重试", "FAIL");
			}
		}
		// 有关联,取消关联
		else if(null != userConnected){
			try {
				mUsrService.cancelConnWith(Long.parseLong(userId), Long.parseLong(aimUsrId));
				return Result.build(200, "取消关注成功!", "SUCCESS");
			} catch (Exception e) {
				e.printStackTrace();
				return Result.build(200, "取消关注数据库操作失败","FAIL");
			}
		}
		return Result.build(500, "系统内部错误!", "FAIL");
	}
	
	/**
	 * 个人中心 - 关注:  获取"关注" 分页列表
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/findMyConcernByPage", method = RequestMethod.POST)
	@ResponseBody
	public Object findMyConcernByPage(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Map<String, String> map = new HashMap<String,String>();
		String token = request.getHeader("Authorization");
		String userId = jsonObject.getString("userId");    // 登陆用户id
		Integer pageNum = null;
		Integer pageSize = null;
		String isFansOrConcern = jsonObject.getString("type");    // fansType, concernType
		String clientId = jsonObject.getString("clientId");

		try {
			pageNum = Integer.parseInt(jsonObject.getString("pageNum"));
			pageSize = Integer.parseInt(jsonObject.getString("pageSize"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "pageNum或pageSize无效,拒绝访问!", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(200, "userId无效,拒绝访问!", "FAIL");
		}
		if (!("concernType".equals(isFansOrConcern) ||"fansType".equals(isFansOrConcern))) {
			return Result.build(200, "请求参数type无效,拒绝访问!", "FAIL");
		}
		
		/**
		 * 先从用户关联关系表中查出id关联List,再以id list作为参数，去查询分页列表
		 */
		List<String> idList = new ArrayList<String>();
		if ("concernType".equals(isFansOrConcern)) {    // 我的关注
			idList = mUsrService.findMyConcernListById(userId);
			if (idList.size() == 0) {
				return Result.build(200,"我的关注为空","SUCCESS");
			}
		}else if ("fansType".equals(isFansOrConcern)) {    // 我的粉丝
			idList = mUsrService.findMyFansIdsById(userId);
			if (idList.size() == 0) {
				return Result.build(200,"我的粉丝为空","SUCCESS");
			}
		}
		PageResult myConcernPage = mUsrService.findMyConcernPageById(idList,Long.parseLong(userId), pageNum, pageSize);
		
		// 用户id, 头像、用户名、认证信息、是否互相关注
		return myConcernPage;
	}
	
	// 用户资格认证
	@ResponseBody
	@RequestMapping(value = "userQualify" ,method = RequestMethod.POST)
	public Object userQualify(@RequestBody JSONObject obj, HttpServletRequest request){
	
		String userId = obj.getString("userId");
		String clientId= obj.getString("clientId");
		String token = request.getHeader("Authorization");
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200,"userId不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientI不合法,拒绝访问!");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
		// 检测是否发布过论题和新思路论题
		List<TbThesis> thesisList = thesisService.findAllTypeThesisById(userId);
		if (thesisList.size() == 0 ) {
			return Result.build(200, "未发布过论题和新思路论题,校验不通过", "FAIL");
		}
		return Result.build(200, "验证通过","SUCCESS");
	}
	
	
	// 用户认证
	@ResponseBody
	@RequestMapping(value = "userVerify", method = RequestMethod.POST)
	public Object userVerify(@RequestBody JSONObject obj, HttpServletRequest request){
		
		long start = System.currentTimeMillis();
		
		Map<String, String> map = new HashMap<String, String>();
		String userId = obj.getString("userId");
		String clientId = obj.getString("clientId");
		String token = request.getHeader("Authorization");
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效,拒绝访问","FAIL");
			}
			if (mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问","FAIL");
			}
			if (mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误","FAIL");
		}
		
		// 检测是否发布过论题和新思路论题
		List<TbThesis> thesisList = thesisService.findAllTypeThesisById(userId);
		if (thesisList.size() == 0 ) {
			return Result.build(200, "未发布过论题和新思路论题,校验不通过", "FAIL");
		}
		String verifyType = obj.getString("verifyType");    // 认证    // 0 - THINKER认证;  1 - 学校认证;   2 - 企业认证
		
		String prefix = null;
		String suffix = obj.getString("suffix");
		if ("0".equals(verifyType)) {
			prefix = "THIBKER认证";
			suffix = "";
		}
		else if ("1".equals(verifyType)) {    // 学校认证
			prefix = "学校认证";
			suffix = "来自"+suffix;
		}else if ("2".equals(verifyType)) {
			prefix = "企业认证";
			suffix = "来自"+suffix;
					
		}
		String nick_name = obj.getString("nickName");
		String contactPhone = obj.getString("contactPhone");
		String idType = obj.getString("idType");    // 证件类型:  身份证 - IdCard
		String realName = obj.getString("realName");    // 真实姓名
		String idNumber = obj.getString("idNumber");    // 证件号码
		String licAuthority = obj.getString("licAuthority");    // 发证机关 
		
		String companyName = obj.getString("companyName");    // 企业/单位名称
		String credentialUrl = obj.getString("credentialUrl");    // 证明材料
		
		TbMobileUser bean = mUsrService.findEntityById(userId);
		bean.setNickName(nick_name);
		bean.setPhone(contactPhone);
		bean.setIdType(idType);
		bean.setRealName(realName);
		bean.setIdNumber(idNumber);
		bean.setUserType(verifyType);
		bean.setPrefix(prefix);
		bean.setSuffix(suffix);
		if ("2".equals(verifyType)) {
			bean.setLicAuthority(licAuthority);
			bean.setCompanyName(companyName);
			bean.setCredentialUrl(credentialUrl);
		}
		try {
			mUsrService.update(bean);
			return Result.build(200,"认证申请提交成功","SUCCESS");
		} catch (Exception e) {
			e.getStackTrace();
			Result.build(200, "认证申请提交失败","FAIL");
		}
		System.out.println("======:" + (System.currentTimeMillis() - start) + "ms");
		return map;
	}
	
	
	// 个人中心 - 推送设置:  点赞/收藏通知(type=1)、评论/回复通知(type=2)、关注通知(type=3)
	@ResponseBody
	@RequestMapping(value = "/chgPushSwitch", method = RequestMethod.POST)
	public Object chgPushSwitch(@RequestBody JSONObject jsonObj, HttpServletRequest request){
		
		Map<String, String> map = new HashMap<String, String>();
		try {
			String usrId = jsonObj.getString("userId");
			String token = request.getHeader("Authorization");
			String type = jsonObj.getString("type");    // type = 1,2,3
			String switchState = jsonObj.getString("switchStatus");
			TbMobileUser bean = mUsrService.findEntityByIdToken(Long.parseLong(usrId),token);
			if ("1".equals(type)) {
				if ("open".equals(switchState)) {
					bean.setHeatCollectNotify("open");
				}
				else if ("close".equals(switchState)) {
					bean.setHeatCollectNotify("close");
				}
				else {
					bean.setHeatCollectNotify("open");
				}
			}else if ("2".equals(type)) {
				if ("open".equals(switchState)) {
					bean.setCommentReplyNotify("open");
				}
				else if ("close".equals(switchState)) {
					bean.setCommentReplyNotify("close");
				}
				else {
					bean.setCommentReplyNotify("open");
				}
			}else if ("3".equals(type)) {
				if ("open".equals(switchState)) {
					bean.setConcernNotify("open");
				}
				else if ("close".equals(switchState)) {
					bean.setConcernNotify("close");
				}
				else {
					bean.setConcernNotify("open");
				}
			}
			mUsrService.update(bean);
			map.put("status", "200");
			map.put("msg", "推送设置切换成功");
			map.put("data", "SUCCESS");
			return map;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			map.put("status", "500");
			map.put("msg", "服务器内部错误");
			map.put("data", "FAIL");
			return map;
		}
	}
}

package com.shopx5.controller.xunsi;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor.STRING;
import javax.servlet.http.HttpServletRequest;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.common.Result;
import com.shopx5.common.shop.PageResult;
import com.shopx5.jedis.JedisClient;
import com.shopx5.pojo.TbArticle;
import com.shopx5.pojo.TbCollectXS;
import com.shopx5.pojo.TbCommentXS;
import com.shopx5.pojo.TbHeatXS;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbMobileUserTransient;
import com.shopx5.pojo.TbPicVideoChar;
import com.shopx5.pojo.TbPushXSMsg;
import com.shopx5.pojo.TbQuestionXS;
import com.shopx5.pojo.TbSpeechXS;
import com.shopx5.pojo.TbThesis;
import com.shopx5.service.xunsi.CommentXSService;
import com.shopx5.service.xunsi.HeatXSService;
import com.shopx5.service.xunsi.MobileUserService;
import com.shopx5.service.xunsi.PicCharVideoArticleService;
import com.shopx5.service.xunsi.ThesisService;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.TransmissionTemplateUtils;

import net.sf.json.JSON;

/**
 * 图文，视频+文字
 * @author sjx
 * 2019年7月2日 下午6:03:01
 */
@Controller
@CrossOrigin
@RequestMapping("/mobile/xunsi/picCharVideoArticle")
public class PicCharVideoArticleController {

	@Autowired
	private PicCharVideoArticleService picCharService;
	
	@Autowired
	private CommentXSService cmtXSService;
	
	@Autowired
	private MobileUserService mobileUsrService;
	
	@Autowired
	private HeatXSService heatXSService;
	
	@Autowired
	private ThesisService thesisService;
	
	@Autowired
	private JedisClient jedisClient;
	
	
	// 反对/赞同 某个质疑
	@ResponseBody
	@RequestMapping(value="voteQuestion",method = RequestMethod.POST)
	public Object voteQuestion(@RequestBody JSONObject obj,HttpServletRequest request){
		String userId = obj.getString("userId");
		String clientId = obj.getString("clientId");
		String token = request.getHeader("Authorization");
		String questionId = obj.getString("questionId");
		String voteType = obj.getString("voteType");    // agree - 赞同;  disagree - 反对
		TbQuestionXS tbQuestionXS = null;
		try {
			tbQuestionXS = thesisService.findQuestionEntity(questionId);
			if (null == tbQuestionXS) {
				return Result.build(200, "questionId无效,拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId不合法,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
		
		
		List<String> list = new ArrayList<String>();
		boolean flag = true;
		if ("agree".equals(voteType)) {
			
			/**
			 * 首先判断userId是否存在于disagreeCollect字段中
			 */
			// 判断start...
			String[] disagreeArr = tbQuestionXS.getDisagreeCollect().split(",");
			for(String tmp:disagreeArr){
				if (userId.equals(tmp)) {
					return Result.build(200, "已经进行过反对该质疑,禁止当前操作","FAIL");
				}
			}
			// 判断end...
			if (null == tbQuestionXS.getAgreeCollect()||"".equals(tbQuestionXS.getAgreeCollect())) {
				tbQuestionXS.setAgreeCollect(userId+",");
				tbQuestionXS.setAgreeCount(tbQuestionXS.getAgreeCount()+1);
				thesisService.updateQuestionEntityAgree(tbQuestionXS);
				return Result.build(200, "支持成功", "SUCCESS");
			}else if (null != tbQuestionXS.getAgreeCollect()) {
				String[] strArr = tbQuestionXS.getAgreeCollect().split(",");
				for(String tmp:strArr){
					if (!userId.equals(tmp)) {
						list.add(tmp+",");
					}else if (userId.equals(tmp)) {
						flag = false;
					}
				}
				if (flag == false) {
					tbQuestionXS.setAgreeCollect(list.toString().replace(",,", ",").replace("[", "").replace("]", "").replace(" ", ""));
					tbQuestionXS.setAgreeCount(tbQuestionXS.getAgreeCount()-1);
					thesisService.updateQuestionEntityAgree(tbQuestionXS);
					return Result.build(200, "支持取消成功","SUCCESS");
				}else if (flag == true) {
					list.add(userId+",");
					tbQuestionXS.setAgreeCollect(list.toString().replace(",,", ",").replace("[", "").replace("]", "").replace(" ", ""));
					tbQuestionXS.setAgreeCount(tbQuestionXS.getAgreeCount()+1);
					thesisService.updateQuestionEntityAgree(tbQuestionXS);
					return Result.build(200, "支持成功","SUCCESS");
				}
			}
			
		}else if ("disagree".equals(voteType)) {
			
			/**
			 * 首先判断userId是否存在于agreeCollect字段中
			 */
			// 判断start...
			String[] agreeArr = tbQuestionXS.getAgreeCollect().split(",");
			for(String tmp:agreeArr){
				if (userId.equals(tmp)) {
					return Result.build(200, "已经进行过赞同该质疑,禁止当前操作","FAIL");
				}
			}
			// 判断end...
			
			if (null == tbQuestionXS.getDisagreeCollect()||"".equals(tbQuestionXS.getDisagreeCollect())) {
				tbQuestionXS.setDisagreeCollect(userId+",");
				tbQuestionXS.setDisagreeCount(tbQuestionXS.getDisagreeCount()+1);
				thesisService.updateQuestionEntityDisagree(tbQuestionXS);
				return Result.build(200, "反对成功", "SUCCESS");
			}else if (null != tbQuestionXS.getAgreeCollect()) {
				String[] strArr = tbQuestionXS.getDisagreeCollect().split(",");
				for(String tmp:strArr){
					if (!userId.equals(tmp)) {
						list.add(tmp+",");
					}else if (userId.equals(tmp)) {
						flag = false;
					}
				}
				if (flag == false) {
					tbQuestionXS.setDisagreeCollect(list.toString().replace(",,", ",").replace("[", "").replace("]", "").replace(" ", ""));
					tbQuestionXS.setDisagreeCount(tbQuestionXS.getDisagreeCount()-1);
					thesisService.updateQuestionEntityDisagree(tbQuestionXS);
					return Result.build(200, "反对取消成功","SUCCESS");
				}else if (flag == true) {
					list.add(userId+",");
					tbQuestionXS.setDisagreeCollect(list.toString().replace(",,", ",").replace("[", "").replace("]", "").replace(" ", ""));
					tbQuestionXS.setDisagreeCount(tbQuestionXS.getDisagreeCount()+1);
					thesisService.updateQuestionEntityDisagree(tbQuestionXS);
					return Result.build(200, "反对成功","SUCCESS");
				}
			}
		}
		return Result.build(500, "服务器内部错误","FAIL");
	}
	
	
	// 获取质疑分页列表
	@ResponseBody
	@RequestMapping(value="/findQuestionPage",method=RequestMethod.POST)
	public Object findQuestionPage(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		String userId = obj.getString("userId");
		String speechId = obj.getString("speechId");
		Integer pageNum = null;
		Integer pageSize = null;
		
		try {
			pageNum = Integer.parseInt(obj.getString("pageNum"));
			pageSize = Integer.parseInt(obj.getString("pageSize"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "pageNum或pageSize无效,拒绝访问","FAIL");
		}
		TbSpeechXS tbSpeechXS = null;
		try {
			tbSpeechXS = thesisService.findSpeechEntity(speechId);
			if (null == tbSpeechXS) {
				return Result.build(200, "speechId无效,拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId不合法,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
		try {
			PageResult pageResult = cmtXSService.findQuestionPage(speechId,pageNum,pageSize);
			return pageResult;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
	}
	
	
	// 发言 - 质疑
	@ResponseBody
	@RequestMapping(value="/addQuestion",method=RequestMethod.POST)
	public Object addQuestion(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		String userId = obj.getString("userId");
		String thesisId = obj.getString("thesisId");
		String speechId = obj.getString("speechId");
		String url = obj.getString("url");
		String quesReason = obj.getString("quesReason");    // 对应app输入框 - "请输入质疑原因"
		String quesMaterial = obj.getString("quesMaterial");    // 对应app输入框 - "请举例说明"
		
		TbThesis tbThesis = null;
		try {
			tbThesis = thesisService.findEntityById(thesisId);
			if (null == tbThesis) {
				return Result.build(200, "thesisId无效,拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId不合法,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
		
		TbQuestionXS entity = new TbQuestionXS();
		entity.setId(IDUtils.get32UUID());
		entity.setUserId(mobileUser.getId().toString());
		entity.setNickName(mobileUser.getNickName());
		entity.setUserImageUrl(mobileUser.getImage());
		entity.setSuffix("来自"+mobileUser.getSuffix());
		entity.setThesisId(thesisId);
		entity.setSpeechId(speechId);
		entity.setUrl(url);
		entity.setQuesReason(quesReason);
		entity.setQuesMaterial(quesMaterial);
		entity.setAgreeCount(0);
		entity.setDisagreeCount(0);
		entity.setAgreeCollect("");
		entity.setDisagreeCollect("");
		try {
			cmtXSService.saveQuesEntity(entity);
			return Result.build(200, "质疑成功!","SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
	}
	
	// 获取某论题下已经发言的人分页列表
	@ResponseBody
	@RequestMapping(value="/findSpeechPeoplePage",method=RequestMethod.POST)
	public Object findSpeechPeoplePage(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String userId =  obj.getString("userId");
		String thesisId = obj.getString("thesisId");
		String clientId = obj.getString("clientId");
		String token = request.getHeader("Authorization");
//		Integer pageNum = null;
//		Integer pageSize = null;
//		
//		try {
//			pageNum = Integer.parseInt(obj.getString("pageNum"));
//			pageSize = Integer.parseInt(obj.getString("pageSize"));
//		} catch (Exception e) {
//			e.getStackTrace();
//			return Result.build(200, "pageNum或pageSize无效,拒绝访问","FAIL");
//		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效,拒绝访问","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误","FAIL");
		}
		List<String> list1 = new ArrayList<String>();
		List<TbMobileUserTransient> list2 = new ArrayList<TbMobileUserTransient>();
		
		TbMobileUser mobileUser2 = null;
		TbMobileUserTransient tbMobileUserTransient = new TbMobileUserTransient();
		list1 = cmtXSService.findUserListInSpeechByThesisId(thesisId);
		for(String str: list1){
			mobileUser2 = mobileUsrService.findEntityById(str);
			tbMobileUserTransient.setId(mobileUser2.getId().toString());
			tbMobileUserTransient.setImage(mobileUser2.getImage());
			tbMobileUserTransient.setNickName(mobileUser2.getNickName());
			tbMobileUserTransient.setPrefix(mobileUser2.getPrefix());
			tbMobileUserTransient.setSuffix(mobileUser2.getSuffix());
			list2.add(tbMobileUserTransient);
		}
		return new PageResult(list2.size(), list2);
	}
	
	// 获取推送消息分页列表
	@ResponseBody
	@RequestMapping(value="/findPushMsgPage",method=RequestMethod.POST)
	public Object findPushMsgPage(@RequestBody JSONObject obj, HttpServletRequest request){
		
		String userId = obj.getString("userId");
		String clientId= obj.getString("clientId");
		String msgType = obj.getString("msgType");
		String token = request.getHeader("Authorization");
		Integer pageNum = null;
		Integer pageSize = null;
		if (!("atType".equals(msgType) || "collectType".equals(msgType) || "heatType".equals(msgType) ||
				"commentType".equals(msgType) || "replyType".equals(msgType) || "concnernType".equals(msgType) ||
				"earningType".equals(msgType))) {
			return Result.build(200, "atType无效,拒绝访问","FAIL");
		}
		try {
			pageNum = Integer.parseInt(obj.getString("pageNum"));
			pageSize = Integer.parseInt(obj.getString("pageSize"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "pageNum或pageSize无效,拒绝访问","FAIL");
		}
		TbMobileUser mobileUser = null;
		try{
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效,拒绝访问","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问","FAIL");
			}
		}catch(Exception e){
			e.getStackTrace();
			return Result.build(500, "服务器内部错误","FAIL");
		}
		PageResult pageResult = null;
		pageResult = picCharService.findPushMsgPage(userId,msgType,pageNum,pageSize);
		return pageResult;
	}
	
	
	// 发言
	@ResponseBody
	@RequestMapping(value="/addSpeech",method=RequestMethod.POST)
	public Object addSpeech(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		String userId = obj.getString("userId");
		String thesisId = obj.getString("thesisId");
		String speechFlag = obj.getString("speechFlag");    // left - 代表正方发表观点;  right - 代表反方发表观点
		String videoUrl = obj.getString("videoUrl");
		String speech1 = obj.getString("speech1");
		String speech2 = obj.getString("speech2");
		String atPeople = obj.getString("atPeople");    // 发言时@的人id集合,逗号,隔开
		
		TbThesis tbThesis = null;
		try {
			tbThesis = thesisService.findEntityById(thesisId);
			if (null == tbThesis) {
				return Result.build(200, "thesisId无效,拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId不合法,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
		
		if (!("left".equals(speechFlag) || "right".equals(speechFlag))) {
			return Result.build(200, "speechFlag无效,拒绝访问!", "FAIL");
		}
		String tbSpeechId = IDUtils.get32UUID();
		TbSpeechXS entity = new TbSpeechXS();
		entity.setId(tbSpeechId);
		entity.setUserId(mobileUser.getId().toString());
		entity.setNickName(mobileUser.getNickName());
		entity.setUserImageUrl(mobileUser.getImage());
		entity.setSuffix("来自"+mobileUser.getSuffix());
		entity.setThesisId(thesisId);
		if (null == speechFlag || "".equals(speechFlag.trim())) {
			entity.setSpeechFlag("");
		}else{
			entity.setSpeechFlag(speechFlag);
		}
		entity.setVideoUrl(videoUrl);
		entity.setSpeech1(speech1);
		entity.setSpeech2(speech2);
		entity.setHeatCount(0);
		if ("".equals(atPeople) || null == atPeople) {
			entity.setAtPeople("");
		}else{
			entity.setAtPeople(atPeople);
			String[] atIdArr = atPeople.split(",");
			for(String idStr:atIdArr){
				TbMobileUser tbMobileUser = mobileUsrService.findEntityById(idStr);
				Map<Object, Object> map = new HashMap<Object,Object>();
				map.put("pushType", "atType");
				map.put("pushContent_nickName", mobileUser.getNickName());
				map.put("pushContent_tmp1", "在");
				map.put("pushContent_speech1",speech1);
				map.put("pushContent_tmp2",",");
				map.put("pushContent_speech2",speech2);
				map.put("pushContent_tmp3", "提到了您");
				map.put("pushTime", new Date());
				map.put("infoId", tbSpeechId);
				map.put("sourceUsrId", userId);
				// 1. 推送
				String transmissionStr = JSONObject.toJSONString(map);
				
				try {
					TransmissionTemplateUtils.pushTransMsg(tbMobileUser.getClientId(), transmissionStr);
				} catch (Exception e) {
					e.printStackTrace();
					return Result.build(200, "推送动作异常","FAIL");
				}
				
				// 2. 持久化到数据库
				TbPushXSMsg bean = new TbPushXSMsg();
				bean.setId(IDUtils.get32UUID());
				bean.setTargetUserId(idStr);
				bean.setMsgType("atType");
				bean.setMsgContent(transmissionStr);
				mobileUsrService.savePushMsgEntity(bean);
				
			}
		}
		try {
			cmtXSService.saveSpeechEntity(entity);
			return Result.build(200, "发言成功!","SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
	}
	
	// 获取论题的发言分页列表
	@ResponseBody
	@RequestMapping(value="/findSpeechPage",method=RequestMethod.POST)
	public Object findSpeechPage(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		String userId = obj.getString("userId");
		String thesisId = obj.getString("thesisId");
		String orderType = obj.getString("orderType");    // byHeat - 按热度;  byTime - 按时间(倒序)
		String searchType = obj.getString("searchType");    // mixType - 查找所有发言;  videoType - 查找视频发言; // charType - 查找文字发言
		Integer pageNum = null;
		Integer pageSize = null;
		
		try {
			pageNum = Integer.parseInt(obj.getString("pageNum"));
			pageSize = Integer.parseInt(obj.getString("pageSize"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "pageNum或pageSize无效,拒绝访问!","FAIL");
		}
		if (!("byHeat".equals(orderType) || "byTime".equals(orderType))) {
			return Result.build(200, "orderType无效,拒绝访问","FAIL");
		}
		if (!("mixType".equals(searchType) || "videoType".equals(searchType))) {
			return Result.build(200, "searchType无效,拒绝访问", "FAIL");
		}
		
		TbThesis tbThesis = null;
		try {
			tbThesis = thesisService.findEntityById(thesisId);
			if (null == tbThesis) {
				return Result.build(200, "thesisId无效,拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token不合法,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId不合法,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误!","FAIL");
		}
		
		PageResult pageResult = cmtXSService.findSpeechPage(thesisId,orderType,searchType,pageNum,pageSize);
		return pageResult;
	}
	
	
	// 寻思发布接口（原图文、视频发布）
	@RequestMapping(value = "/addPicVideoChar",method=RequestMethod.POST)
	@ResponseBody
	public Object addPicChar(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		
		Map<String, String> map = new HashMap<String,String>();
		String token = request.getHeader("Authorization");
		String usrId = jsonObject.getString("userId");
		String title = jsonObject.getString("title");
		String content = jsonObject.getString("content");
		String url = jsonObject.getString("url");
		String isAnon = jsonObject.getString("isAnon");    // 是否为匿名发布, yes - 是/no - 否
		String clientId = jsonObject.getString("clientId");
		
		if (token == null || token.length() != 32) {
			map.put("status","200");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.length() != 15)  {
			map.put("status","200");
			map.put("msg", "userId格式不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		try {
			Long temp = Long.parseLong(usrId);
			System.out.println(temp);
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mUserEntity = null;
		try {
			mUserEntity = mobileUsrService.findEntityById(usrId);    // 查询一个实体，仅进行一次实体查询用于后续操作
			if (null == mUserEntity || null == mUserEntity.getToken()) {
				map.put("status","200");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		if (!token.equals(mUserEntity.getToken())) {
			map.put("status","200");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		if (!mUserEntity.getClientId().equals(clientId)) {
			map.put("status","200");
			map.put("msg", "无效的clientId,拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		TbPicVideoChar tbPicVideoChar = new TbPicVideoChar();
		
		tbPicVideoChar.setId(IDUtils.get32UUID());
		tbPicVideoChar.setTitle(title);
		tbPicVideoChar.setContent(content);
		tbPicVideoChar.setIsAnon(isAnon);
		tbPicVideoChar.setUrl(url);
		tbPicVideoChar.setSharedTimes(0);
		tbPicVideoChar.setHeatCount(0);
		tbPicVideoChar.setReadTimes(0);
		tbPicVideoChar.setCollectTimes(0);
		tbPicVideoChar.setCommentsCount(0);
		
		tbPicVideoChar.setUserId(usrId);
		tbPicVideoChar.setUserNickName(mUserEntity.getNickName());
		tbPicVideoChar.setUserImageUrl("http://39.100.126.205:8080/pic_upload/"+mUserEntity.getImage());
		tbPicVideoChar.setUserSuffix(mUserEntity.getSuffix());
		
		try {
			picCharService.save(tbPicVideoChar);
			return Result.build(200, "新增成功", "SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
	}

	// 删除寻思（图文/视频）
	@RequestMapping(value="delTwvById", method = RequestMethod.POST)
	@ResponseBody
	public Object delTwvById(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Map<String, String> map = new HashMap<String,String>();
		String token = request.getHeader("Authorization");
		String usrId = jsonObject.getString("usrId");
		String twvId = jsonObject.getString("twvId");
		String clientId = jsonObject.getString("clientId");
		
		if (token == null || token.length() != 32) {
			map.put("status","200");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.length() != 15)  {
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		try {
			Long temp = Long.parseLong(usrId);
			System.out.println(temp);
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(usrId);
			if (null == mobileUser ||null == mobileUser.getToken()) {
				map.put("status","200");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				map.put("status","200");
				map.put("msg", "无效的clientId,拒绝访问!");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		if (!token.equals(mobileUser.getToken())) {
			map.put("status","200");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		picCharService.delTwvById(usrId, twvId);
		map.put("status","200");
		map.put("msg", "删除成功");
		map.put("data", "SUCCESS");
		return map;
	}
	
	
	/**
	 * 学富 - 原创文章
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addArticle",method = RequestMethod.POST)
	@ResponseBody
	public Object addArticle(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String userId = obj.getString("userId");
		String clientId= obj.getString("clientId");
		String title = obj.getString("title");
		String content = obj.getString("content");
		String tags = obj.getString("tags");
		String readPerm = obj.getString("readPerm");    // 0 - 免费阅读、1 - 收费阅读
		String price = obj.getString("price");    // 收费阅读下需要的平台币数目
		String isAnon = obj.getString("isAnon");    // 是否匿名:  yes  - 是;  no - 否 
		String isAllowShare = obj.getString("isAllowShare");    // yes - 允许分享; no - 不允许分享
		
		
		if (!("yes".equals(isAnon) || "no".equals(isAnon))) {
			return Result.build(200, "isAnon无效,拒绝访问!", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "userId无效,拒绝访问!", "FAIL");
		}
		
		TbArticle tbArticle = new TbArticle();
		tbArticle.setId(IDUtils.get32UUID());
		tbArticle.setUserId(userId);
		tbArticle.setUserImageUrl(mobileUser.getImage());
		tbArticle.setNickName(mobileUser.getNickName());
		tbArticle.setIsAllowShare(isAllowShare);
		tbArticle.setIsAnon(isAnon);
		tbArticle.setTitle(title);
		tbArticle.setContent(content);
		if (readPerm.equals("0")) {    // 0 - 免费阅读;   1 - 收费阅读
			tbArticle.setReadPerm("free");
			tbArticle.setPrice("0");    // 设置费用值为0
		}else if(readPerm.equals("1")){
			tbArticle.setPrice(price);
			tbArticle.setReadPerm("charge");
		}
		tbArticle.setReadCount(0);
		tbArticle.setHeatCount(0);
		tbArticle.setSharedCount(0);
		
		List<String> list = JSONObject.parseArray(tags,String.class);
		// 需求说明: 原创文章最多支持3个标签
		for(String str: list){
			switch (str) {
			case "1":    // 科技
				tbArticle.setTechType("yes");
				break;
			case "2":    // 生活
				tbArticle.setLifeType("yes");
				break;
			case "3":    // 名人
				tbArticle.setCelebrityType("yes");
				break;
			case "4":    // 影视
				tbArticle.setFilmType("yes");
				break;
			case "5":    // 哲学
				tbArticle.setPhilosophyType("yes");
				break;
			case "6":    // 工业
				tbArticle.setIndustryType("yes");
				break;
			case "7":    // 文学
				tbArticle.setLiteratureType("yes");
				break;
			case "8":    // 互联网
				tbArticle.setInternetType("yes");
				break;
			case "9":    // 体育
				tbArticle.setSportsType("yes");
				break;
			case "10":    // 健康
				tbArticle.setHealthType("yes");
				break;
			case "11":    // 经济
				tbArticle.setEconomicsType("yes");
				break;
			case "12":    // 其他
				tbArticle.setOtherType("yes");
				break;
			default:
				break;
			}
		}
		try {
			picCharService.saveArticle(tbArticle);
			return Result.build(200, "新增成功", "SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
	}
	
	/**
	 * 获取学富分页列表
	 * @param obj
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/findArticlePage" ,method = RequestMethod.POST)
	public Object findArticlePage(@RequestBody JSONObject obj, HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String userId = obj.getString("userId");    // 登陆用户id
		String isById = obj.getString("isById");    //  // yes – 个人中心: 我的原创文章;  no – 原创文章
		String category = obj.getString("category");    // 分类
		String clientId = obj.getString("clientId");
		Integer pageNum = null;
		Integer pageSize = null;
		if (!("tech_type".equals(category) || "life_type".equals(category)|| "celebrity_type".equals(category) || "film_type".equals(category)||
				"philosophy_type".equals(category) || "industry_type".equals(category) || "literature_type".equals(category) ||
				"internet_type".equals(category) || "sports_type".equals(category) || "health_type".equals(category) || "economics_type".equals(category)||
				"other_type".equals(category))) {
			return Result.build(200, "category无效,拒绝访问","FAIL");
		}
		try {
			pageNum = Integer.parseInt(obj.getString("pageNum"));
			pageSize = Integer.parseInt(obj.getString("pageSize"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数pageNum或pageSize无效,拒绝访问!", "FAIL");
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "请求参数userId无效,拒绝访问!","FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "请求参数token无效,拒绝访问!","FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "请求参数clientId无效,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数userId无效,拒绝访问!","FAIL");
		}
		
		PageResult pageResult = null;
		if ("tech_type".equals(category)) {
			pageResult=  picCharService.findTechTypeArtiPage(pageNum,pageSize);
		}else if ("life_type".equals(category)) {
			pageResult = picCharService.findLifeTypeArtiPage(pageNum,pageSize);
		}else if ("celebrity_type".equals(category)) {
			pageResult = picCharService.findCelebrityTypeArtiPage(pageNum,pageSize);
		}else if ("film_type".equals(category)) {
			pageResult = picCharService.findFilmTypeArtiPage(pageNum,pageSize);
		}else if ("philosophy_type".equals(category)) {
			pageResult = picCharService.findPhilosophyTypeArtiPage(pageNum,pageSize);
		}else if ("industry_type".equals(category)) {
			pageResult = picCharService.findIndustryTypeArtiPage(pageNum,pageSize);
		}else if ("literature_type".equals(category)) {
			pageResult = picCharService.findLiteratureTypeArtiPage(pageNum,pageSize);
		}else if ("internet_type".equals(category)) {
			pageResult = picCharService.findInternetTypeArtiPage(pageNum,pageSize);
		}else if ("sports_type".equals(category)) {
			pageResult = picCharService.findSportsTypeArtiPage(pageNum,pageSize);
		}else if ("health_type".equals(category)) {
			pageResult = picCharService.findHealthTypeArtiPage(pageNum,pageSize);
		}else if ("economics_type".equals(category)) {
			pageResult = picCharService.findEconomicsTypeArtiPage(pageNum,pageSize);
		}else if ("other_type".equals(category)) {
			pageResult = picCharService.findOtherTypeArtiPage(pageNum,pageSize);
		}
//		if ("yes".equals(isById)) {
//			result=  picCharService.findArtiPageById(Long.parseLong(userId), pageNum, pageSize);
//		}
//		else if ("no".equals(isById) || null == isById) {
//			result=  picCharService.findArtiPage(Long.parseLong(userId), pageNum, pageSize);
//		}
		return pageResult;
	}
	
	/**
	 * 获取首页内容
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getIndex" ,method=RequestMethod.POST)
	public Object findIndex(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Map<Object, Object> map = new HashMap<Object,Object>();
		String token = request.getHeader("Authorization");
		String userId = jsonObject.getString("userId");
		if (token == null || token.length() != 32) {
			map.put("status","400");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (userId == null || userId.length() != 15)  {
			map.put("status","400");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		String token_db = null;
		try {
			token_db = mobileUsrService.findTokenById(Long.parseLong(userId));
			if (null == token_db) {
				map.put("status","400");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		if (!token.equals(token_db)) {
			map.put("status","400");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		
		Integer pageNum = Integer.valueOf(jsonObject.getString("pageNum"));
		Integer pageSize = Integer.valueOf(jsonObject.getString("pageSize"));
		PageResult result1 =  picCharService.findPageByTimeDesc(userId,pageNum,pageSize);
		PageResult result2 = picCharService.findPageByHeatDesc(userId,pageNum,pageSize);
		PageResult result3 = picCharService.findPageByConcern(Long.parseLong(userId), pageNum, pageSize);
		map.put("bytime", result1.getRows());
		map.put("byheat", result2.getRows());
		map.put("byconcern",result3.getRows());
		return map;
	}
	
	
	/**
	 * 获取关注的人图文/视频
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getConcern" ,method=RequestMethod.POST)
	public Object findIndexContent(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Map<Object, Object> map = new HashMap<Object,Object>();
		String token = request.getHeader("Authorization");
		String userId = jsonObject.getString("userId");
		if (token == null || token.length() != 32) {
			map.put("status","400");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (userId == null || userId.length() != 15)  {
			map.put("status","400");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		String token_db = null;
		try {
			token_db = mobileUsrService.findTokenById(Long.parseLong(userId));
			if (null == token_db) {
				map.put("status","400");
				map.put("msg", "该用户token不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		if (!token.equals(token_db)) {
			map.put("status","400");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		Integer pageNum = Integer.valueOf(jsonObject.getString("pageNum"));
		Integer pageSize = Integer.valueOf(jsonObject.getString("pageSize"));
		PageResult result =  picCharService.findPageByConcern(Long.parseLong(userId), pageNum,pageSize);
		return result;
	}
	
	
	
	/**
	 * 查询图文/视频的分页信息 - 按时间递减排序
	 * @param jsonObject
	 * @return
	 */
	@RequestMapping(value = "/findPageByTime", method=RequestMethod.POST)
	@ResponseBody
	public Object findListByTimeDesc(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Integer pageNum = Integer.valueOf(jsonObject.getString("pageNum"));
		Integer pageSize = Integer.valueOf(jsonObject.getString("pageSize"));
		String isById = jsonObject.getString("isById");    // yes / no
		String access_token = request.getHeader("Authorization");
		String usrId = jsonObject.getString("usrId");
		
		try {
			String token = mobileUsrService.findTokenById(Long.parseLong(usrId));
			if (!token.equals(access_token)) {
				return Result.build(200, "token无效,拒绝访问","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "usrId无效,拒绝访问","FAIL");
		}
		if ("yes".equals(isById)) {    // 个人中心 - 我的发布
			return picCharService.findPageByIdByTimeDesc(usrId, pageNum,pageSize);
		}else if ("no".equals(isById) || null == isById) {
			return picCharService.findPageByTimeDesc(usrId,pageNum,pageSize);
		}
		
		return Result.build(500, "服务器内部错误","FAIL");
	 }
	
	/**
	 * 查询图文/视频的分页信息 - 按热度递减排序
	 * @param jsonObject
	 * @return
	 */
	@RequestMapping(value = "/findPageByHeat", method=RequestMethod.POST)
	@ResponseBody
	public Object findListByHeatDesc(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		Map<String, String> map = new HashMap<String,String>();
		
		Integer pageNum = Integer.valueOf(jsonObject.getString("pageNum"));
		Integer pageSize = Integer.valueOf(jsonObject.getString("pageSize"));
		String access_token = request.getHeader("Authorization");
		Long usrId = null;
		
		try {
			usrId = Long.parseLong(jsonObject.getString("usrId"));
		} catch (Exception e) {
			e.getStackTrace();
			map.put("msg", "usrId不合法，请求失败");
			map.put("data", "FAIL");
			map.put("status", "400");
			return map;
		}
		
		try {
			String token = mobileUsrService.findTokenById(usrId);
			if (!token.equals(access_token)) {
				map.put("msg", "该token不存在，请求失败");
				map.put("data", "FAIL");
				map.put("status", "400");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
			map.put("msg", "该usrId不存在，请求失败");
			map.put("data", "FAIL");
			map.put("status", "400");
			return map;
		}
/*//		jedisClient.get("twvByHeat")
		if (null == jedisClient.get("twvByHeat") || " ".equals(jedisClient.get("twvByHeat"))) {
			List<TbPicVideoChar> list = new ArrayList<TbPicVideoChar>();
			list = picCharService.findListByHeat(usrId.toString());
			jedisClient.hSetByte("twvByHeat",SerializeUtil.serialize(list));
			return picCharService.findPageByHeatDesc(usrId.toString(),pageNum,pageSize);
		}else if (null != jedisClient.get("twvByHeat")) {
			byte[] bs = jedisClient.hGetByte("twvByHeat");
//			byte[] bs = {-84, -19, 0, 5, 115, 114, 0, 30, 99, 111, 109, 46, 115, 104, 111, 112, 120, 53, 46, 112, 111, 106, 111, 46, 84, 98, 80, 105, 99, 86, 105, 100, 101, 111, 67, 104, 97, 114, -116, 6, 85, 0, -55, 12, -55, -99, 2, 0, 18, 76, 0, 12, 99, 111, 108, 108, 101, 99, 116, 84, 105, 109, 101, 115, 116, 0, 19, 76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 73, 110, 116, 101, 103, 101, 114, 59, 76, 0, 13, 99, 111, 109, 109, 101, 110, 116, 115, 67, 111, 117, 110, 116, 113, 0, 126, 0, 1, 76, 0, 11, 99, 111, 110, 99, 101, 114, 110, 70, 108, 97, 103, 116, 0, 18, 76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 83, 116, 114, 105, 110, 103, 59, 76, 0, 7, 99, 111, 110, 116, 101, 110, 116, 113, 0, 126, 0, 2, 76, 0, 10, 99, 114, 101, 97, 116, 101, 84, 105, 109, 101, 116, 0, 16, 76, 106, 97, 118, 97, 47, 117, 116, 105, 108, 47, 68, 97, 116, 101, 59, 76, 0, 6, 99, 121, 84, 121, 112, 101, 113, 0, 126, 0, 2, 76, 0, 14, 102, 114, 105, 101, 110, 100, 115, 73, 110, 118, 105, 116, 101, 100, 113, 0, 126, 0, 2, 76, 0, 9, 104, 101, 97, 116, 67, 111, 117, 110, 116, 113, 0, 126, 0, 1, 76, 0, 2, 105, 100, 113, 0, 126, 0, 2, 76, 0, 12, 105, 115, 65, 108, 108, 67, 97, 110, 82, 101, 97, 100, 113, 0, 126, 0, 2, 76, 0, 12, 105, 115, 86, 105, 100, 101, 111, 79, 114, 80, 105, 99, 113, 0, 126, 0, 2, 76, 0, 13, 112, 101, 111, 112, 108, 101, 67, 97, 110, 82, 101, 97, 100, 113, 0, 126, 0, 2, 76, 0, 9, 114, 101, 97, 100, 84, 105, 109, 101, 115, 113, 0, 126, 0, 1, 76, 0, 12, 114, 101, 108, 97, 116, 101, 100, 84, 111, 112, 105, 99, 113, 0, 126, 0, 2, 76, 0, 11, 115, 104, 97, 114, 101, 100, 84, 105, 109, 101, 115, 113, 0, 126, 0, 1, 76, 0, 3, 117, 114, 108, 113, 0, 126, 0, 2, 76, 0, 6, 117, 115, 101, 114, 73, 100, 113, 0, 126, 0, 2, 76, 0, 10, 117, 115, 101, 114, 83, 117, 102, 102, 105, 120, 113, 0, 126, 0, 2, 120, 112, 112, 112, 112, 116, 0, 36, -26, -75, -117, -24, -81, -107, 32, 45, 32, -27, -122, -123, -27, -82, -71, -17, -68, -116, -27, -113, -81, -28, -69, -91, -23, -102, -113, -26, -105, -74, -27, -120, -96, -23, -103, -92, 112, 112, 112, 112, 116, 0, 32, 102, 51, 101, 54, 55, 98, 49, 54, 54, 50, 57, 55, 52, 102, 56, 55, 57, 53, 53, 98, 55, 51, 102, 102, 53, 98, 57, 101, 49, 52, 99, 102, 112, 112, 112, 112, 112, 112, 112, 112, 112};
			TbPicVideoChar entity = (TbPicVideoChar) SerializeUtil.deserialize(bs);
			
			System.out.println("=====" + entity.getId() );
			
		}*/
		return picCharService.findPageByHeatDesc(usrId.toString(),pageNum,pageSize);
	 }
	
	

	// 原创文章/论题/寻思 -  点赞
	@RequestMapping(value = "/heat", method = RequestMethod.POST)
	@ResponseBody
	public Object heat(@RequestBody JSONObject obj,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String userId = obj.getString("userId");    // 用户id
		String aimInfoId = obj.getString("aimInfoId");    // 信息id
		String clientId = obj.getString("clientId");
		String type = obj.getString("type");    // 点赞的对象类型  type=article,thesis,xunsi,speech
		
		TbMobileUser mbUser = null;
		try {
			mbUser = mobileUsrService.findEntityById(userId);
			if (null == mbUser) {
				return Result.build(200, "userId无效,拒绝访问!","FAIL");
			}
			if (!mbUser.getToken().equals(token)) {
				return Result.build(200, "token无效,拒绝访问!","FAIL");
			}
			if (!mbUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问!","FAIL");
			}
			if (!("article".equals(type) || "thesis".equals(type) || "xunsi".equals(type) || "speech".equals(type))) {
				return Result.build(200, "type不合法,拒绝访问!","FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "userId无效,拒绝访问!","FAIL");
		}
		TbHeatXS entity1 = heatXSService.findEntity(userId,aimInfoId,type);
		if (null != entity1) {
			heatXSService.cancel(userId,aimInfoId,type);
			if ("xunsi".equals(type)) {
				TbPicVideoChar bean = picCharService.findEntityByPrimaryId(aimInfoId);
				Integer heatCount = bean.getHeatCount()-1;
				picCharService.updateHeatCount(heatCount,bean.getId());
			}else if ("thesis".equals(type)) {
				TbThesis bean = thesisService.findEntityById(aimInfoId);
				Integer heatCount = bean.getHeatCount()-1;
				thesisService.updateHeatCount(heatCount,bean.getId());
			}
			else if ("article".equals(type)) {
				TbArticle bean = picCharService.findArtiEntityByPrimaryId(aimInfoId);
				Integer heatCount = bean.getHeatCount()-1;
				picCharService.updateAricleHeat(heatCount,bean.getId());
			}else if ("speech".equals(type)) {
				TbSpeechXS bean = thesisService.findSpeechEntity(aimInfoId);
				Integer heatCount = bean.getHeatCount()-1;
				thesisService.updateHeatCountSpeech(heatCount, aimInfoId);
			}
			return Result.build(200, "取消点赞成功","SUCCESS");
		}
		TbHeatXS entity2 = new TbHeatXS();
		entity2.setUserId(userId);
		entity2.setAimInfoId(aimInfoId);
		entity2.setType(type);
		try {
			heatXSService.save(entity2);
			if ("xunsi".equals(type)) {
				TbPicVideoChar bean = picCharService.findEntityByPrimaryId(aimInfoId);
				Integer heatCount = bean.getHeatCount()+1;
				picCharService.updateHeatCount(heatCount,bean.getId());
			}else if ("thesis".equals(type)) {
				TbThesis bean = thesisService.findEntityById(aimInfoId);
				Integer heatCount = bean.getHeatCount()+1;
				thesisService.updateHeatCount(heatCount,bean.getId());
			}
			else if ("article".equals(type)) {
				TbArticle bean = picCharService.findArtiEntityByPrimaryId(aimInfoId);
				Integer heatCount = bean.getHeatCount()+1;
				picCharService.updateAricleHeat(heatCount,bean.getId());
			}
			else if ("speech".equals(type)) {
				TbSpeechXS bean = thesisService.findSpeechEntity(aimInfoId);
				Integer heatCount = bean.getHeatCount()+1;
				thesisService.updateHeatCountSpeech(heatCount, aimInfoId);
			}
			return Result.build(200, "点赞成功","SUCCESS");
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误","FAIL");
		}
	}
	
	/**
	 * 评论功能，针对寻思（原图文、视频）、原创文章
	 * @return
	 */
	@RequestMapping(value = "/addComment", method=RequestMethod.POST)
	@ResponseBody
	public Object addComment(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String aimInfoId = jsonObject.getString("aimInfoId");
		String commentId = jsonObject.getString("commentId");
		String userId = jsonObject.getString("userId");
		String comment = jsonObject.getString("comment");
		String clientId = jsonObject.getString("clientId");
		if (aimInfoId.length() != 32) {
			return Result.build(200, "请求参数aimInfoId不合法", "FAIL");
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "请求参数clientId无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "请求参数token无效,拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数userId不合法", "FAIL");
		}
		
		TbCommentXS tbComment = new TbCommentXS();
		tbComment.setId(IDUtils.get32UUID());
		tbComment.setAimInfoId(aimInfoId);
		tbComment.setUserId(userId);
		tbComment.setUserImageUrl("http://39.100.126.205:8080/pic_upload/"+mobileUser.getImage());
		tbComment.setUserNickName(mobileUser.getNickName());
		tbComment.setComment(comment);
		tbComment.setCommentId(commentId);
		cmtXSService.save(tbComment);
		
		TbPicVideoChar bean = null;
		try {
			bean = picCharService.findEntityByPrimaryId(aimInfoId);
			if (null == bean) {
				return Result.build(200, "该信息已删除，请刷新后重试", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "该信息已删除，请刷新后重试", "FAIL");
		}
		
		TbMobileUser tbMobileUser = mobileUsrService.findEntityById(bean.getUserId());
		
		Map<String, String> m = new HashMap<String, String>();
		if (null == commentId || "".equals(commentId)) {
			m.put("pushType", "2");
		}
		m.put("pushType", "3");
		m.put("pushContent", tbMobileUser.getNickName() + "评论了您的" + bean.getContent());
		m.put("sourceUsrId", userId);
		m.put("aimInfoId", aimInfoId);
		try {
			TransmissionTemplateUtils.pushTransMsg(tbMobileUser.getClientId(),JSONObject.toJSONString(m));
		} catch (Exception e) {
			System.out.println("=====:推送失败，请优先排查推送平台服务是否异常");
			e.printStackTrace();
		}
		return Result.build(200, "评论成功", "SUCCESS");
		
	}
	
	/**
	 * 删除评论
	 * @param jsonObject
	 * @return
	 */
	@RequestMapping(value = "/delCommentById", method=RequestMethod.POST)
	@ResponseBody
	public Result delCommentById(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		
		String aimInfoId = jsonObject.getString("aimInfoId");
		String userId = jsonObject.getString("userId");
		String clientId = jsonObject.getString("clientId");
		String token = request.getHeader("Authorization");
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "请求参数userId无效,拒绝访问", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "请求参数clientId无效，拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "请求参数token无效，拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(200, "请求参数userId无效,拒绝访问", "FAIL");
		}
		try {
			cmtXSService.delById(aimInfoId);
			return Result.build(200, "删除成功", "SUCCESS");
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(500, "服务器内部错误", "FAIL");
		}
	}
	
	// 获取评论列表(一级评论+二级评论)
	@RequestMapping(value = "/findCommentsPageById", method=RequestMethod.POST)
	@ResponseBody
	public Object findCommentsPageById(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Map<Object, Object> map = new HashMap<Object,Object>();
		String userId = jsonObject.getString("userId");
		String clientId = jsonObject.getString("clientId");
		String aimInfoId = jsonObject.getString("id");
		String type = jsonObject.getString("type");    // byHeat - 按时间倒叙;  byTime - 按时间倒叙
		String token = request.getHeader("Authorization");
		Integer pageNum = null;
		Integer pageSize = null;
		
		try {
			pageNum = Integer.parseInt(jsonObject.getString("page"));
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status", "200");
			map.put("msg", "请求参数page无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		try {
			pageSize = Integer.parseInt(jsonObject.getString("rows"));
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status", "200");
			map.put("msg", "请求参数rows无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (aimInfoId.length() != 32) {
			map.put("status", "200");
			map.put("msg", "请求参数id无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (!("byHeat".equals(type) || "byTime".equals(type))) {
			map.put("status", "200");
			map.put("msg", "请求参数type无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				map.put("status", "200");
				map.put("msg", "userId无效，拒绝访问!");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			map.put("status", "200");
			map.put("msg", "userId无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (!mobileUser.getClientId().equals(clientId)) {
			map.put("status", "200");
			map.put("msg", "clientId无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (!mobileUser.getToken().equals(token)) {
			map.put("status", "200");
			map.put("msg", "token无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		// 获取一级评论分页信息
		PageResult list1 = cmtXSService.findFstCommentPage(aimInfoId,type,pageNum,pageSize);
		// 获取二级评论分页信息
		PageResult list2 = cmtXSService.findSndCommentPage(aimInfoId,type, pageNum,pageSize);
		
		map.put("fstCmts", list1);
		map.put("sndCmts", list2);
		return map;
	}
	
	// 获取评论列表(一级评论)
	@RequestMapping(value = "/findFstCmtsPageById", method=RequestMethod.POST)
	@ResponseBody
	public Object findFstCmtsPageById(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		String token = request.getHeader("Authorization");
		String aimInfoId = jsonObject.getString("aimInfoId");
		String type = jsonObject.getString("type");
		String clientId = jsonObject.getString("clientId");
		String userId = jsonObject.getString("userId");
		Integer pageNum = null;
		Integer pageSize = null;
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效，拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效，拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "userId无效，拒绝访问!", "FAIL");
		}
		try {
			pageNum = Integer.parseInt(jsonObject.getString("page"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数page无效,拒绝访问!","FAIL");
		}
		try {
			pageSize = Integer.parseInt(jsonObject.getString("rows"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数rows无效,拒绝访问!","FAIL");
		}
		if (!("byTime".equals(type) || "byHeat".equals(type))) {
			return Result.build(200, "请求参数type无效", "FAIL");
		}
		// 获取一级评论分页信息
		PageResult page = cmtXSService.findFstCommentPage(aimInfoId,type,pageNum,pageSize);
		return page;
	}
	
	// 获取评论列表(二级评论)
	@RequestMapping(value = "/findSndCmtsPageById", method=RequestMethod.POST)
	@ResponseBody
	public Object findSndCmtsPageById(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		String token = request.getHeader("Authorization");
		String aimInfoId = jsonObject.getString("aimInfoId");
		String type = jsonObject.getString("type");
		String clientId = jsonObject.getString("clientId");
		String userId = jsonObject.getString("userId");
		Integer pageNum = null;
		Integer pageSize = null;
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "userId无效，拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "clientId无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "token无效，拒绝访问", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "userId无效，拒绝访问!", "FAIL");
		}
		try {
			pageNum = Integer.parseInt(jsonObject.getString("page"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数page无效,拒绝访问!","FAIL");
		}
		try {
			pageSize = Integer.parseInt(jsonObject.getString("rows"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数rows无效,拒绝访问!","FAIL");
		}
		if (!("byTime".equals(type) || "byHeat".equals(type))) {
			return Result.build(200, "请求参数type无效", "FAIL");
		}
		// 获取二级评论分页信息
		PageResult list = cmtXSService.findSndCommentPage(aimInfoId, type, pageNum,pageSize);
		return list;
	}
	
	
	/**
	 * 设置图文/视频/原创文章 增加浏览数 +1
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addReadTime", method = RequestMethod.POST)
	@ResponseBody
	public Object setReadTimes(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		Map<String, String> map = new HashMap<String,String>();
		String token = request.getHeader("Authorization");
		String type = jsonObject.getString("type");    // twvideo - 图文/视频;    art - 原创文章
		String infoId = jsonObject.getString("infoId");
		Long usrId = null;    // 登陆用户id
		
		// ========= 参数校验 start...
		try {
			usrId = Long.parseLong(jsonObject.getString("usrId"));
		} catch (Exception e) {
			map.put("status","400");
			map.put("msg", "请求参数格式不正确，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		if (token == null || token.length() != 32) {
			map.put("status","400");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.toString().length() != 15)  {
			map.put("status","400");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		String token_db = null;
		try {
			token_db = mobileUsrService.findTokenById(usrId);
			if (null == token_db) {
				map.put("status","400");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		if (!token.equals(token_db)) {
			map.put("status","400");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		// ========= 参数校验 end...
		if ("twvideo".equals(type)) {
			picCharService.addReadTimeTwv(usrId, infoId);    // 增加图文/视频浏览次数，并记录浏览者信息
		}else if ("art".equals(type)) {
			picCharService.addReadTimeArti(usrId, infoId);    // 增加原创文章浏览次数，并记录浏览者信息
		}
		
		map.put("status", "200");
		map.put("msg", "浏览量+1");
		map.put("data", "SUCCESS");
		return map;
	}
	
	/**
	 * 获取图文/视频/原创文章 浏览数
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getReadTime", method = RequestMethod.GET)
	@ResponseBody
	public Object getReadTimes(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
		Map<String, Object> map = new HashMap<String,Object>();
		String token = request.getHeader("Authorization");
		String type = jsonObject.getString("type");    // twvideo - 图文/视频;    art - 原创文章
		String infoId = jsonObject.getString("infoId");
		Long usrId = null;    // 登陆用户id
		
		// ========= 参数校验 start...
		try {
			usrId = Long.parseLong(jsonObject.getString("usrId"));
		} catch (Exception e) {
			map.put("status","400");
			map.put("msg", "请求参数格式不正确，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		if (token == null || token.length() != 32) {
			map.put("status","400");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.toString().length() != 15)  {
			map.put("status","400");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		String token_db = null;
		try {
			token_db = mobileUsrService.findTokenById(usrId);
			if (null == token_db) {
				map.put("status","400");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		if (!token.equals(token_db)) {
			map.put("status","400");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		// ========= 参数校验 end...
		Integer times = null;
		if ("twvideo".equals(type)) {
			times = picCharService.getReadTimeTwv(infoId);    // 获取图文/视频浏览次数
		}else if ("art".equals(type)) {
			times = picCharService.getReadTimeArti(infoId);    // 获取原创文章浏览次数
		}
		
		map.put("status", "200");
		map.put("msg", "获取浏览数成功!");
		map.put("data", "SUCCESS");
		map.put("readed_time",times);
		return map;
	}
	
	/**
	 * 图文/视频、原创文章的收藏/取消收藏
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addCollect", method = RequestMethod.POST)
	public Object addCollect(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		
		Map<String, Object> map = new HashMap<String,Object>();
		String token = request.getHeader("Authorization");
		String aimType = jsonObject.getString("type");    // tw - 图文;  video - 视频; art - 原创文章;
		String aimId = jsonObject.getString("infoId");
		String usrId = jsonObject.getString("usrId");
		String clientId = jsonObject.getString("clientId");
		// ========= 参数校验 start...
		
		if (token == null || token.length() != 32) {
			map.put("status","400");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.toString().length() != 15)  {
			map.put("status","400");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(usrId);
			if (null == mobileUser) {
				return Result.build(200, "无效userId,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "无效token,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "无效clientId,拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "无效userId,拒绝访问!", "FAIL");
		}
		// ========= 参数校验 end...
		if ("tw".equals(aimType)) {    // tw - 图文;  video - 视频; art - 原创文章;
			
			TbCollectXS bean = picCharService.findIsCollected(usrId, aimId);
			if (null != bean) {
				TbPicVideoChar tbChar = null;
				try {
					tbChar = picCharService.findEntityByPrimaryId(aimId);
					if (null == tbChar) {
						return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
					}
				} catch (Exception e) {
					e.getStackTrace();
					return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
				}
				picCharService.cancelCollect(usrId, aimId);
				Integer collectCount = tbChar.getCollectTimes()-1;
				picCharService.updateCollectHeat(collectCount, tbChar.getId());
				map.put("status", "200");
				map.put("msg", "取消了收藏");
				map.put("data", "SUCCESS");
			}else{
				TbPicVideoChar tbChar = null;
				try {
					tbChar = picCharService.findEntityByPrimaryId(aimId);
					if (null == tbChar) {
						return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
					}
				} catch (Exception e) {
					e.getStackTrace();
					return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
				}
				picCharService.addCollect(usrId, aimId, aimType);
				Integer collectCount = tbChar.getCollectTimes()-1;
				picCharService.updateCollectHeat(collectCount, tbChar.getId());
				map.put("status", "200");
				map.put("msg", "收藏成功");
				map.put("data", "SUCCESS");
			}
			return map;
		}else if ("video".equals(aimType)) {
			TbCollectXS bean = picCharService.findIsCollected(usrId, aimId);
			if (bean != null) {
				TbPicVideoChar tbChar = null;
				try {
					tbChar = picCharService.findEntityByPrimaryId(aimId);
					if (null == tbChar) {
						return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
					}
				} catch (Exception e) {
					e.getStackTrace();
					return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
				}
				picCharService.addCollect(usrId, aimId, aimType);
				Integer collectCount = tbChar.getCollectTimes()-1;
				picCharService.updateCollectHeat(collectCount, tbChar.getId());
				map.put("status", "200");
				map.put("msg", "取消了收藏");
				map.put("data", "SUCCESS");
			}else{
				TbPicVideoChar tbChar = null;
				try {
					tbChar = picCharService.findEntityByPrimaryId(aimId);
					if (null == tbChar) {
						return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
					}
				} catch (Exception e) {
					e.getStackTrace();
					return Result.build(200, "请求参数infoId无效，拒绝访问", "FAIL");
				}
				picCharService.addCollect(usrId, aimId, aimType);
				Integer collectCount = tbChar.getCollectTimes()-1;
				picCharService.updateCollectHeat(collectCount, tbChar.getId());
				map.put("status", "200");
				map.put("msg", "收藏成功");
				map.put("data", "SUCCESS");
			}
			return map;
		}else if ("art".equals(aimType)) {
			TbCollectXS bean = picCharService.findIsCollected(usrId, aimId);
			if (bean != null) {
				picCharService.cancelCollect(usrId, aimId);
				map.put("status", "200");
				map.put("msg", "取消了收藏");
				map.put("data", "SUCCESS");
			}else{
				picCharService.addCollect(usrId, aimId, aimType);
				map.put("status", "200");
				map.put("msg", "收藏成功");
				map.put("data", "SUCCESS");
			}
			return map;
		}
		map.put("status", "500");
		map.put("msg", "服务器内部错误");
		map.put("data", "FAIL");
		return map;
	}
}

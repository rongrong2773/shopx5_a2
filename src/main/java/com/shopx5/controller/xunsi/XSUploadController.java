package com.shopx5.controller.xunsi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.controller.sso.MobilRegLoginController;
import com.shopx5.pojo.TbAppInfo;
import com.shopx5.service.xunsi.AppInfoService;
import com.shopx5.service.xunsi.MobileUserService;
import com.shopx5.utils.IDUtils;


import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;

/**
 * 文件上传controller
 * @author sjx
 * 2019年7月1日 下午10:55:32
 */
@Controller
@CrossOrigin
@RequestMapping("/mobile/xunsi")
public class XSUploadController {
	
	@Autowired
	private AppInfoService appInfoService;
	
	@Autowired
	private MobileUserService mobileUsrService;
	
	private static Logger logger = LoggerFactory.getLogger(XSUploadController.class);

	/**
	 * 文件上传
	 * @param pics
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping(value = "/upload", method=RequestMethod.POST)
	public void upload(@RequestParam("pics") MultipartFile[] pics,HttpServletRequest request, HttpServletResponse response) throws Exception {
		Properties prop = new Properties();
		String sysPlat = System.getProperties().getProperty("os.name");
		String absPath = MobilRegLoginController.class.getResource("").getFile().replace("com/shopx5/controller/sso/", "");
		String pth = null;
		try {
			if (sysPlat.toLowerCase().contains("windows")) {
				pth = absPath.replace("/C:", "C:") + "server.properties";
			}else if (sysPlat.toLowerCase().contains("linux")) {
				pth = absPath + "server.properties";
			}
			// 配置绝对路径 C:/Users/Administrator/Desktop/云主机搭建/workspace_szc/shopx5_a2/target/classes/server.properties
			FileInputStream in = new FileInputStream(pth);
			prop.load(in);
			
			// 获取token
			String access_token = request.getHeader("Authorization");

			List<String> dbUrls = new ArrayList<String>();
			String fileNameInDb = null;    // 保存在db里的文件名+后缀
			String path = null;
			String dbUrl = null;
			boolean convertChangeExtFlag = false;
			String video_duration = null;
			String videoFramePath = null;    // 视频第n帧图片路径
			String extension = null;    // 文件扩展名
			String videoFramePathUrl = null;    // 视频的第一帧图片url
			for(int i=0; i<pics.length; i++){
				MultipartFile pic = pics[i];
				// 获取图片原始文件名
				String originalFilename = pic.getOriginalFilename();
				// 文件名使用当前时间
				String name = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
				// 获取上传图片的扩展名(jpg/png/mp4/apk...)
				extension = FilenameUtils.getExtension(originalFilename);
				
				if (extension.equals("apk")) {
					path = prop.getProperty("apk_path") + name + "." + extension;
					fileNameInDb = name + "." + extension;
				}
				else if (extension.equalsIgnoreCase("mp4") || extension.equalsIgnoreCase("MOV") || extension.equalsIgnoreCase("AVI") || extension.equalsIgnoreCase("RMVB") ||
						extension.equalsIgnoreCase("RM") || extension.equalsIgnoreCase("FLASH") || extension.equalsIgnoreCase("3GP") ||
						extension.equalsIgnoreCase("WMV") || extension.equalsIgnoreCase("MKV")) {
					path = prop.getProperty("video_path") + name + "." + extension;
					fileNameInDb = name + "." + extension;
				}else{
					// 图片上传的相对路径（因为相对路径放到页面上就可以显示图片）
					path = prop.getProperty("pic_path") + name + "." + extension;
					fileNameInDb = name + "." + extension;
				}
				
				// 存入数据库的url
				dbUrl = prop.getProperty("server_addr") + path;
				dbUrls.add(dbUrl);
				
				System.out.println("======prop.getProperty_video_path:\t" + prop.getProperty("video_path"));
				// 文件上传的绝对路径 
				String url = request.getSession().getServletContext().getRealPath("") + path;
				
				System.out.println("文件上传的绝对路径url:\t" + url);

				File dir = new File(url);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				// 执行上传
				pic.transferTo(new File(url));
				

				
				if (extension.equalsIgnoreCase("MOV") || extension.equalsIgnoreCase("AVI") || extension.equalsIgnoreCase("RMVB") ||
						extension.equalsIgnoreCase("RM") || extension.equalsIgnoreCase("FLASH") || extension.equalsIgnoreCase("3GP") ||
						extension.equalsIgnoreCase("WMV") || extension.equalsIgnoreCase("MKV")) {
					System.out.println("======:  MOV === 进入非mp4文件上传处理方法");
					if (sysPlat.toLowerCase().contains("linux")) {
					// 视频时长
						video_duration = prop.getProperty("ffmpeg_path")+" -i "+ url +" 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//";
						video_duration = executeShellCmd(video_duration);
					}
					// 截取第n帧，并将图片放在视频同目录下
					fetchFrame(url,url.replace(url.substring(url.lastIndexOf("."), url.length()), ".jpg"));
					videoFramePath = name + ".jpg";
					videoFramePathUrl = prop.getProperty("server_addr") + prop.getProperty("video_path") + videoFramePath;
					if (sysPlat.toLowerCase().contains("linux")) {
						videoConvert(url.replace("\\", "/"));
					}else if (sysPlat.toLowerCase().contains("windows")) {
						videoConvert(url);
					}
					
					convertChangeExtFlag = true;
				}else if (extension.equalsIgnoreCase("MP4")) {
					if (sysPlat.toLowerCase().contains("linux")) {
						// 视频时长
						video_duration = prop.getProperty("ffmpeg_path")+" -i "+ url +" 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//";
						video_duration = executeShellCmd(video_duration);
					}
					// 截取第n帧，并将图片放在视频同目录下
					fetchFrame(url,url.replace(url.substring(url.lastIndexOf("."), url.length()), ".jpg"));
					videoFramePath = name + ".jpg";
					videoFramePathUrl = prop.getProperty("server_addr") + prop.getProperty("video_path") + videoFramePath;
				}
				System.out.println("=======原文件上传绝对路径:" + url);
				if (path.contains("apk_upload")) {
					TbAppInfo tbAppInfo = new TbAppInfo();
					tbAppInfo.setId(IDUtils.get32UUID());
					tbAppInfo.setReleaseTime(new Date());
					tbAppInfo.setDownloadUrl(fileNameInDb);
//				tbAppInfo.setVersion(appVer);/
					tbAppInfo.setIsForceUpdate("1");    // 1 - 非强制更新; 0 - 强制更新
					tbAppInfo.setUpdateDesc("默认更新描述，硬编码于代码中，可随时根据续期做适配");
					appInfoService.save(tbAppInfo);
				}
			}
			in.close();
			// 将相对路径写回（json格式）
			JSONObject jsonObj = new JSONObject();
			// 为true时，说明上传的文件时视频格式，经过了转码，这里修改存入数据库里的文件后缀名为.mp4
			if (convertChangeExtFlag == true) {
				Integer start = dbUrls.get(0).lastIndexOf(".");
				Integer end = dbUrl.length();
				String aimStr = dbUrl.substring(start, end);
				String out = dbUrl.replace(aimStr, ".mp4");
				jsonObj.put("url",out);
				jsonObj.put("duration", video_duration);
				jsonObj.put("videoFramePath", videoFramePathUrl);
			}else if (extension.equals("mp4")) {
				jsonObj.put("url",dbUrl);
				jsonObj.put("duration", video_duration);
				jsonObj.put("videoFramePath", videoFramePathUrl);
			}else {
				jsonObj.put("url",dbUrls);
				jsonObj.put("status", "200");
				jsonObj.put("msg", "上传成功");
			}
			// 设置响应数据的类型json
			response.setContentType("application/json; charset=utf-8");
			// 写回
			response.getWriter().write(jsonObj.toString());
		} catch (Exception e) {
			e.printStackTrace();
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("msg", "服务器内部错误");
			jsonObj.put("status", "500");
			response.setContentType("application/json; charset=utf-8");
			response.getWriter().write(jsonObj.toString());
		}
	}
	
	/**
	 * 非mp4格式转码为.mp4
	 * @param path    文件上传的绝对路径
	 */
	public void videoConvert(String path) {
		String sysPlat = System.getProperties().getProperty("os.name");
		System.out.println("====== videoConvert()方法开始执行，视频转码中  Starting...======");
		long startTime = System.currentTimeMillis();
		
		List<String> cmd = new ArrayList<String>();
		Integer ext_length = path.substring(path.indexOf(".") + 1, path.length()).length();
		Object ext_converted = path.substring(0, path.length() - ext_length - 1);
		if (sysPlat.toLowerCase().contains("linux")) {
			// 生产环境下
//			 cmd.add("/monchickey/ffmpeg/bin/ffmpeg -i ");
			cmd.add("/monchickey/ffmpeg/bin/ffmpeg -i ");
			cmd.add(path);
			cmd.add("-vcodec libx264 -preset medium -b:v 2000k");
		}else if (sysPlat.toLowerCase().contains("windows")) {
			// 开发环境下
			cmd.add("C:\\ffmpeg-20190706\\bin\\ffmpeg.exe" +" -i");
			cmd.add(path);
		} 
//		cmd.add(path);
		cmd.add(ext_converted + ".mp4");

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < cmd.size(); i++) {
			sb.append(cmd.get(i) + " ");
		}
		try {
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(sb.toString());
			InputStream stderr = proc.getErrorStream();
			InputStreamReader isr = new InputStreamReader(stderr);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			System.out.println("<ERROR>");
			while ((line = br.readLine()) != null){
				System.out.println(line);
			}
			System.out.println("</ERROR>");
			int exitVal = proc.waitFor();
			System.out.println("Process exitValue: " + exitVal);

		} catch (Exception e) {
			e.getStackTrace();
		}
		System.out.println("====== videoConvert()方法执行完毕，视频转码成功  end...======");
		System.out.println("******转码耗时:\t" + (System.currentTimeMillis() - startTime)/1000 + "s");
		new File(path).delete();    // 删除未转码前的原始文件
		
	}
	
	/**
	 * 获取视频的指定帧 - 这里设置为地5帧，过滤掉开头可能存在的黑屏
	 * @param videofile
	 * @param framefile
	 * @return 
	 * @throws Exception
	 */
	public static void fetchFrame(String videofile, String framefile) throws Exception {
		long startTime = System.currentTimeMillis();
		FFmpegFrameGrabber ff = new FFmpegFrameGrabber(videofile); // videofile视频路径
		ff.start();
		int lenght = ff.getLengthInFrames();
		int i = 0;
		Frame f = null;
		while (i < lenght) {
			// 过滤前5帧，避免出现全黑的图片
			f = ff.grabFrame();
			if ((i > 5) && (f.image != null)) {
				break;
			}
			i++;
		}
		int owidth = f.imageWidth;
		int oheight = f.imageHeight;
		// 对截取的帧进行等比例缩放
		int width = 800;
		int height = (int) (((double) width / owidth) * oheight);
		Java2DFrameConverter converter = new Java2DFrameConverter();
		BufferedImage fecthedImage = converter.getBufferedImage(f);
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		bi.getGraphics().drawImage(fecthedImage.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);
		File targetFile = new File(framefile);
		ImageIO.write(bi, "jpg", targetFile);
		// ff.flush();
		ff.stop();
		System.out.println("======获取帧图片耗时:" + (System.currentTimeMillis() - startTime) + ":ms");
	}
	
	/**
	 * 执行shell命令，并返回结果
	 * @param cmd
	 * @return
	 */
//	public String executeShellCmd(String[] strs) {
	public String executeShellCmd(String cmd) {
		System.out.println("=======\t进入shell命令执行方法;命令是:" + cmd );
		StringBuffer sb = new StringBuffer();
		try {
			String[] cmdC = new String[] { "/bin/sh", "-c", cmd };
			Process ps = Runtime.getRuntime().exec(cmdC);
			BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
			
			String line;
			while ((line = br.readLine()) != null) {
//				sb.append(line).append("\n");    // 执行结果多行时，使用该语句
				sb.append(line);
			}
//			String result = sb.toString();
			System.out.println("=======\t shell命令执行完成;返回结果是:" + sb.toString() );
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
}

package com.shopx5.controller.xunsi;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.pojo.TbAppInfo;
import com.shopx5.service.xunsi.AppInfoService;
/**
 * app版本控制
 * @author sjx
 * 2019年7月2日 上午9:58:55
 */
@CrossOrigin
@Controller
@RequestMapping("/mobile/xunsi/app")
public class XSAppInfoController {

	@Autowired
	private AppInfoService appInfoService;
	
	// 获取最新的app版本号
	@RequestMapping(value = "/lstVer", method = RequestMethod.GET)
	public void  getLatestVersion(HttpServletRequest request,HttpServletResponse response) throws IOException{
		List<TbAppInfo> list = appInfoService.findVers();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("app_version", list.get(0).getVersion());
		jsonObject.put("app_url", list.get(0).getDownloadUrl());
		jsonObject.put("update_desc", list.get(0).getUpdateDesc());
		jsonObject.put("is_force_update", list.get(0).getIsForceUpdate());
		response.setContentType("application/json;charset=utf-8");
		response.getWriter().write(jsonObject.toString());
	}
	
}

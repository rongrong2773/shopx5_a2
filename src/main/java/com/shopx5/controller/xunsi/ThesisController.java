package com.shopx5.controller.xunsi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.common.Result;
import com.shopx5.common.shop.PageResult;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbThesis;
import com.shopx5.service.xunsi.MobileUserService;
import com.shopx5.service.xunsi.ThesisService;
import com.shopx5.utils.IDUtils;

/**
 * 论题controller
 * @author sjx
 * 2019年7月1日 上午9:32:30
 */
@Controller
@CrossOrigin
@RequestMapping("/mobile/xunsi/thesis")
public class ThesisController {

	@Autowired
	private ThesisService thesisService;
	
	@Autowired
	private MobileUserService mobileUsrService;
	
	
	// 查询从某条寻思发起的议题分页列表
	@ResponseBody
	@RequestMapping(value="/findConnTwvThesisPage",method=RequestMethod.POST)
	public Object findConnTwvThesisPage(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		
		Map<Object, Object> map = new HashMap<Object,Object>();
		String userId = jsonObject.getString("userId");
		String clientId = jsonObject.getString("clientId");
		String aimInfoId = jsonObject.getString("aimInfoId");
		String type = jsonObject.getString("type");    // byHeat - 按时间倒叙;  byTime - 按时间倒叙
		String token = request.getHeader("Authorization");
		Integer pageNum = null;
		Integer pageSize = null;
		
		try {
			pageNum = Integer.parseInt(jsonObject.getString("page"));
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status", "200");
			map.put("msg", "请求参数page无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		try {
			pageSize = Integer.parseInt(jsonObject.getString("rows"));
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status", "200");
			map.put("msg", "请求参数rows无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (aimInfoId.length() != 32) {
			map.put("status", "200");
			map.put("msg", "请求参数id无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (!("byHeat".equals(type) || "byTime".equals(type))) {
			map.put("status", "200");
			map.put("msg", "请求参数type无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				map.put("status", "200");
				map.put("msg", "userId无效，拒绝访问!");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			map.put("status", "200");
			map.put("msg", "userId无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (!mobileUser.getClientId().equals(clientId)) {
			map.put("status", "200");
			map.put("msg", "clientId无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		if (!mobileUser.getToken().equals(token)) {
			map.put("status", "200");
			map.put("msg", "token无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		PageResult page = thesisService.findConnTwvThesisPage(pageNum,pageSize,aimInfoId,type);
		return page;
	}
	
	// 增加"论题"
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Object addThesis(@RequestBody JSONObject jsonObject,HttpServletRequest request){
		
		Map<String, String> map = new HashMap<String,String>();
		String token = request.getHeader("Authorization");
		String usrId = jsonObject.getString("userId");
		String connTwvId = jsonObject.getString("connTwvId");
		String clientId = jsonObject.getString("clientId");
		
		if (token == null || token.length() != 32) {
			map.put("status","200");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.length() != 15)  {
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		try {
			Long temp = Long.parseLong(usrId);
			System.out.println(temp);
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(usrId);
			if (null == mobileUser.getToken() || null == mobileUser) {
				map.put("status","200");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status","200");
			map.put("msg", "userId不存在，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (!mobileUser.getClientId().equals(clientId)) {
			map.put("status","200");
			map.put("msg", "clientId无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		
		if (!token.equals(mobileUser.getToken())) {
			map.put("status","200");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		String title = jsonObject.getString("title");
		String background =jsonObject.getString("background");
		String url = jsonObject.getString("url");
		String thesisType = jsonObject.getString("thesisType");    // 共同探讨 - 0/两方辩论 - 1
		List<String> thesisTags = JSONObject.parseArray(jsonObject.getString("thesisTags"),String.class);
		String leftOpinion = jsonObject.getString("leftOpinion");    // 正方观点
		String rightOpinion = jsonObject.getString("rightOpinion");    // 反方观点
		String isAnon = jsonObject.getString("isAnon");    // 是否为匿名发布, yes - 是; no - 否
		
		TbThesis tbThesis = new TbThesis();    // new一个论题实体
		String primaryId = IDUtils.get32UUID();
		tbThesis.setId(primaryId);
		if (null != connTwvId) {
			tbThesis.setIsConnTwv("yes");
			tbThesis.setConnTwvId(connTwvId);
			thesisService.addTwvConn(primaryId,connTwvId);
		}
		tbThesis.setUserId(usrId);
		tbThesis.setTitle(title);
		tbThesis.setBackground(background);
		
		tbThesis.setUrl(url);
		tbThesis.setThesisType(thesisType);
		tbThesis.setMobileUserId(usrId);
		tbThesis.setIsAnon(isAnon);
		
		tbThesis.setLeftOpinion(leftOpinion);    // 正方
		tbThesis.setRightOpinion(rightOpinion);    // 反方
		
		for(String str: thesisTags){
			switch (str) {
			case "1":    // 科技
				tbThesis.setTechType("yes");
				break;
			case "2":    // 环保
				tbThesis.setEnvironmentType("yes");
				break;
			case "3":    // 影视
				tbThesis.setFilmType("yes");
				break;
			case "4":    // 名人
				tbThesis.setCelebrityType("yes");
				break;
			case "5":    // 情感
				tbThesis.setEmotionType("yes");
				break;
			case "6":    // 经济
				tbThesis.setEconomicsType("yes");
				break;
			case "7":    // 文学
				tbThesis.setLiteratureType("yes");
				break;
			case "8":    // 哲学
				tbThesis.setPhilosophyType("yes");
				break;
			case "9":    // 旅行
				tbThesis.setJourneyType("yes");
				break;
			case "10":    // 运动
				tbThesis.setSportsType("yes");
				break;
			case "11":    // 理学
				tbThesis.setScienceType("yes");
				break;
			case "12":    // 创意
				tbThesis.setCreativityType("yes");
				break;
			case "13":    // 工业
				tbThesis.setIndustryType("yes");
				break;
			case "14":    // 法律
				tbThesis.setLawType("yes");
				break;
			case "15":    // 美食
				tbThesis.setFoodType("yes");
				break;
			case "16":    // 医学
				tbThesis.setMedicalType("yes");
				break;
			case "17":    // 其他
				tbThesis.setOtherType("yes");
				break;
			default:
				break;
			}
		}
		try {
			// 保存议题到数据库
			thesisService.save(tbThesis);
			return Result.build(200, "发布成功","SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(500, "发布失败","FAIL");
		}
			
	}
	
	// 删除论题
	@RequestMapping(value = "/delThesisById", method = RequestMethod.POST)
	@ResponseBody
	public Object delThesisById(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		Map<String, String> map = new HashMap<String,String>();
		String token = request.getHeader("Authorization");
		String thesisId = jsonObject.getString("thesisId");
		String usrId = jsonObject.getString("usrId");
		String clientId = jsonObject.getString("clientId");
		
		if (token == null || token.length() != 32) {
			map.put("status","200");
			map.put("msg", "token不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (usrId == null || usrId.length() != 15)  {
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		try {
			Long temp = Long.parseLong(usrId);
			System.out.println(temp);
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status","200");
			map.put("msg", "userId不合法，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(usrId);
			if (null == mobileUser.getToken() || null == mobileUser) {
				map.put("status","200");
				map.put("msg", "userId不存在，请求失败");
				map.put("data", "FAIL");
				return map;
			}
		} catch (Exception e) {
			e.getStackTrace();
			map.put("status","200");
			map.put("msg", "userId不存在，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		if (!mobileUser.getClientId().equals(clientId)) {
			map.put("status","200");
			map.put("msg", "clientId无效，拒绝访问!");
			map.put("data", "FAIL");
			return map;
		}
		
		if (!token.equals(mobileUser.getToken())) {
			map.put("status","200");
			map.put("msg", "请求的token不属于该用户，请求失败");
			map.put("data", "FAIL");
			return map;
		}
		
		thesisService.delThesisById(thesisId, usrId);
		map.put("status", "200");
		map.put("msg", "删除成功");
		return map;
	}
	
	
	/**
	 * 查找论题分页列表
	 * @param jsonObject
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/findThesisByPage", method = RequestMethod.GET)
	@ResponseBody
	public Object findThesisByPage(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		
//		Map<String, String> map = new HashMap<String,String>();
		Integer page = null;
		Integer rows = null;
		String type = jsonObject.getString("type");    // byHeat - 按热度; byTime - 按时间
		String token = request.getHeader("Authorization");
		String userId = jsonObject.getString("userId");
		String isById = jsonObject.getString("isById");    // yes - 个人中心: 我的发布 - 论道;    no - 论道模块
		String clientId = jsonObject.getString("clientId");
		
		try {
			page = Integer.parseInt(jsonObject.getString("page"));
			rows = Integer.parseInt(jsonObject.getString("rows"));
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数page或rows无效,拒绝访问!", "FAIL");
		}
		
		TbMobileUser mobileUser = null;
		try {
			mobileUser = mobileUsrService.findEntityById(userId);
			if (null == mobileUser) {
				return Result.build(200, "请求参数userId无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getToken().equals(token)) {
				return Result.build(200, "请求参数token无效,拒绝访问!", "FAIL");
			}
			if (!mobileUser.getClientId().equals(clientId)) {
				return Result.build(200, "请求参数clientId无效,拒绝访问!", "FAIL");
			}
		} catch (Exception e) {
			e.getStackTrace();
			return Result.build(200, "请求参数userId无效,拒绝访问!", "FAIL");
		}
		
		if (!(isById.equals("yes") || isById.equals("no"))) {
			return Result.build(200, "请求参数isById无效,拒绝访问!", "FAIL");
		}
		if (!(type.equals("byHeat") || type.equals("byTime"))) {
			return Result.build(200, "请求参数type无效,拒绝访问!", "FAIL");
		}
		PageResult p = null;
		if ("yes".equals(isById)) {
			p = thesisService.findThesisByIdByPage(userId, type, page, rows);
		}else if ("no".equals(isById) || null == isById) {
			 p = thesisService.findThesisByPage(type, page, rows);
		}
		return p;
	}
	
}

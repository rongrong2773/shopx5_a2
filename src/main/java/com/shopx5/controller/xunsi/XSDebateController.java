package com.shopx5.controller.xunsi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.pojo.TbDebateTimeLimited;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.service.xunsi.DebateService;
import com.shopx5.service.xunsi.MobileUserService;
import com.shopx5.utils.IDUtils;

/**
 * 星秀controller
 * @author sjx
 * 2019年8月6日 上午10:21:36
 */
@Controller
@RequestMapping("/mobile/xunsi/debate")
@CrossOrigin
public class XSDebateController {
	
	@Autowired
	private MobileUserService mUserService;
	
	@Autowired
	private DebateService debateService;
	
	@ResponseBody
	@RequestMapping(value="/add", method=RequestMethod.POST)
	private Object addDetateTimeLimited(@RequestBody JSONObject obj, HttpServletRequest request){
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		// 获取请求参数&校验  start...
		String userId = obj.getString("userId");
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		String title = obj.getString("title");
		String debateType = obj.getString("debateType");    // 共同探讨 - 0     两方辩论 - 1
		String positiveOp = obj.getString("positiveOp");    // 正方观点
		String negativeOp = obj.getString("negativeOp");    // 反方观点
		String debateBackground = obj.getString("debateBackground");    // 辩论背景
		String attachUrl = obj.getString("attachUrl");    // 附件url
		Integer jifenAmount = null;    // 奖池积分
		Integer debateDuration = null;    // 辩论时效(1-30天)
		Integer debatePersonAmount = null;    // 为空时则为不限制人数
		try {
			if(null != jifenAmount || !(" ".equals(jifenAmount))){
				jifenAmount = Integer.parseInt(obj.getString("jifenAmount"));
			}else{
				jifenAmount = 0;
			}
			if (null != debatePersonAmount || !(" ".equals(debatePersonAmount))) {
				debatePersonAmount = Integer.parseInt(obj.getString("debatePersonAmount"));    
			}else{
				debatePersonAmount = 0;
			}
			if (null != debateDuration || !(" ".equals(debateDuration))) {
				debateDuration = Integer.parseInt(obj.getString("debateDuration"));
			}else{
				debateDuration = 0;
			}
		} catch (Exception e) {
			e.getStackTrace();
			map.put("msg", "存在参数格式不合法");
			map.put("status", "FAIL");
			return map;
		}
		// 获取请求参数&校验  end...
		
		TbMobileUser entity = null;
		try {
			entity = mUserService.findEntityByIdToken(Long.parseLong(userId), token);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "用户id不合法");
			map.put("status", "FAIL");
			return map;
		}
		
		if (null == entity) {
			map.put("msg", "用户id与token信息不符,校验失败");
			map.put("status", "FAIL");
			return map;
		}
		TbMobileUser entity2 = mUserService.findEntity(userId, token, clientId);
		if(null == entity2){
			map.put("msg", "无效的clientId");
			map.put("status","FAIL");
			return map;
		}
		
		TbDebateTimeLimited entity1 = new TbDebateTimeLimited();
		entity1.setId(IDUtils.get32UUID());
		entity1.setUserId(userId);
		entity1.setTitle(title);
		entity1.setDebateType(debateType);
		if ("0".equals(debateType)) {
			entity1.setPositiveOp("");
			entity1.setNegativeOp("");
		}else if ("1".equals(debateType)) {
			entity1.setPositiveOp(positiveOp);
			entity1.setNegativeOp(negativeOp);
		}
		entity1.setBackground(debateBackground);
		entity1.setUrl(attachUrl);
		if (0 == jifenAmount) {
			entity1.setIsHasIntegral("no");    // 奖池里是否有积分
			entity1.setIntegral(jifenAmount);
		}else{
			entity1.setIsHasIntegral("yes");
			entity1.setIntegral(jifenAmount);
		}
		if (0 == debatePersonAmount) {
			entity1.setIsLimitPersonCount("no");    // 无人数限制
		}else{
			entity1.setIsLimitPersonCount("yes");
			entity1.setDebeatPersonCount(debatePersonAmount);
		}
		entity1.setDebeatDuration(debateDuration*24*60*60);
		entity1.setIsStopped("running");
		
		debateService.save(entity1);
		return "hello";
	}
	
	
	// 获取某用户进行中的辩题列表
	@ResponseBody
	@RequestMapping(value="/findRunningDebateListById", method=RequestMethod.POST)
	private Object findRunningDebateListById(@RequestBody JSONObject obj, HttpServletRequest request){
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		String userId = obj.getString("userId");
		String token = request.getHeader("Authorization");
		String clientId = obj.getString("clientId");
		
		TbMobileUser entity = null;
		try {
			entity = mUserService.findEntityByIdToken(Long.parseLong(userId), token);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "用户id不合法");
			map.put("status", "FAIL");
			return map;
		}
		if (null == entity) {
			map.put("msg", "用户id与token信息不符,校验失败");
			map.put("status", "FAIL");
			return map;
		}
		TbMobileUser entity2 = mUserService.findEntity(userId, token, clientId);
		if(null == entity2){
			map.put("msg", "无效的clientId");
			map.put("status","FAIL");
			return map;
		}
		
		List<TbDebateTimeLimited> list = debateService.findRunningDebateListById(userId);
		return list;
	}
}

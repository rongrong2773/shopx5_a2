package com.shopx5.controller.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;
/**
 * @author XS多商户商城系统
 */
public class CustomGlobalStrToDateConverter implements Converter<String, Date> {
	@Override
	public Date convert(String source) {
		//表单传入格式: yyyy-MM-dd hh:mm:ss  String转成对象Date类型
		try {
			if (source.length()<11) {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(source);
				return date;
			}else if (source.length()>10) {
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(source);
				return date;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}

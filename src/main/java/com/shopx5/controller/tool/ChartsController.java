package com.shopx5.controller.tool;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shopx5.common.AmChartsNode;
import com.shopx5.mapper.TbItemMapper;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.TbItemExample;
import com.shopx5.utils.JsonUtils;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/tool")
public class ChartsController {
	@Autowired
	private TbItemMapper itemMapper;
	
	//柱状图页面
	@RequestMapping("/amcharts")
	public String amCharts(Model model){
		/** 柱状图json格式        [{"country":"名称","visits":4025,"color":"#FF0F00"},,,] */
		//1.获取itemsku数据
		TbItemExample example = new TbItemExample();
		List<TbItem> itemList = itemMapper.selectByExample(example);
		
		//2.遍历封装到map
		String[] colors = {"#FF0F00","#FF6600","#FF9E01","#FCD202","#F8FF01","#B0DE09","#04D215","#0D8ECF","#0D52D1","#2A0CD0","#8A0CCF",
				           "#CD0D74","#754DEB","#DDDDDD","#999999","#333333","#000000"};
		int i = 0;
		int k = 0;
		List<AmChartsNode> list = new ArrayList<AmChartsNode>();
		for (TbItem item : itemList) {
			k++;
			if(k == 13) break;
			AmChartsNode ac = new AmChartsNode();
			ac.setCountry(item.getCategory());
			ac.setVisits(String.valueOf(item.getNum()));
			ac.setColor(colors[i++]);
			ac.setTitle(item.getCategory());
			ac.setValue(String.valueOf(item.getNum()));
			list.add(ac);
			//颜色超过后重置
			if(i >= colors.length) i = 0;
			if(i == 18) break;
		}
		String json = JsonUtils.objectToJson(list);
		
		//3.json数据传递给页面
		model.addAttribute("result", json);				
		return "tool/charts/index";
	}
}

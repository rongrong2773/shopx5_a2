package com.shopx5.controller.tool;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.shopx5.utils.Const;
import com.shopx5.utils.SmsUtil;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/tool")
public class SmsDayuController {
	@Value("${mail.from}")
	private String mailFrom;
	
	@RequestMapping("/toSms")
	public String update(String accessKeyId, String accessKeySecret, String template_code, String sign_name, String param, String mobile) throws Exception {
		//1.发送短信 配置sms数据
		if (StringUtils.isBlank(sign_name)) {
			sign_name = Const.sign_name;
		}
		if (StringUtils.isBlank(template_code)) {
			template_code = Const.template_code;
		}
		String s = "{\"code\":\"+++\"}";
		param = s.replace("+++", param);
		
		//2.发送短信验证码
		SendSmsResponse response = SmsUtil.sendSms(mobile, sign_name, template_code, param);
		if (response.getCode().equals("OK") && response.getMessage().equals("OK")) {
			return "redirect:/temp/tool/sms/index?id=1";
		}
		return null;
	}
}

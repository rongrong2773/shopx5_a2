package com.shopx5.controller.tool;

import java.io.File;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.shopx5.utils.IDUtils;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/tool")
public class EmailController {
	@Value("${mail.from}")
	private String mailFrom;
	@Autowired
	private JavaMailSender mailSender;
	
	@RequestMapping("/toEmail")
	public String update(String toEmail, String subject, String text,
			             MultipartFile picFile, MultipartFile rarFile, HttpServletRequest request) throws Exception{
		
		//1.发送邮件 创建一封允许带图片，同时带附件的邮件对象
		MimeMessage message = mailSender.createMimeMessage();
	    //为了更好的操作MimeMessage对象，借用一个工具类来操作它
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		//通过工具类设置主题，内容，图片，附件
		helper.setFrom(mailFrom);
		helper.setTo(toEmail);
		helper.setSubject(subject); //主题
		helper.setText("<html><head></head><body>"
				     +  text
				     + "<img src=cid:imga />"
				     + "</body></html>", true); //第二个参数说明内容要解析为html代码
		
		if (picFile != null) {
			//2.获取图片 完整名称
			String fileStr = picFile.getOriginalFilename();
			// 目录命名
			String newFileName = IDUtils.genImageName();
			// 保存目录
			String path = request.getSession().getServletContext().getRealPath("/images/email");
			File uploadPic = new File(path + "/" + newFileName + "/" + fileStr); // D:/../webapp/images/email/15987987815151/xxxx.xxx
			if(!uploadPic.exists()) uploadPic.mkdirs();
			// 保存到硬盘
			picFile.transferTo(uploadPic);
			
			//添加图片 *
			FileSystemResource img = new FileSystemResource(uploadPic);
			helper.addInline("imga", img);   //跟上面cid一致
		}
		if(rarFile != null){
			//3.获取附件 完整名称
			String fileStr = rarFile.getOriginalFilename();
			// 目录命名
			String newFileName = IDUtils.genImageName();
			// 保存目录
			String path = request.getSession().getServletContext().getRealPath("/images/email");
			File uploadPic = new File(path + "/" + newFileName + "/" + fileStr);
			if(!uploadPic.exists()) uploadPic.mkdirs();
			// 保存到硬盘
			rarFile.transferTo(uploadPic);
			
			//发送时带附件 *
			FileSystemResource file = new FileSystemResource(uploadPic);
			helper.addAttachment(fileStr, file);
		}

		//发送
		mailSender.send(message);
		return "redirect:/temp/tool/email/index?id=1";
	}
}

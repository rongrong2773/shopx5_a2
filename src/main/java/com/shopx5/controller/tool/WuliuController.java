package com.shopx5.controller.tool;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.shopx5.utils.kdniao.TrackQuery;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/tool")
public class WuliuController {
	@RequestMapping("/findExp")
	@ResponseBody
	public List<Map> findExp(String expNo) throws Exception {
		//查询物流轨迹
		String result = TrackQuery.result(expNo);
		Map map = JSON.parseObject(result, Map.class);
		List<Map> list = (List<Map>) map.get("Traces");
		/*for (Map map2 : list) {
			System.out.println(map2.get("AcceptStation") + " " + map2.get("AcceptTime"));
		}*/
		return list;
	}
}

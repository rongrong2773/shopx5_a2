package com.shopx5.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shopx5.service.common.IndexService;
/**
 * 
 * @author sjx
 * 2019年6月20日 下午4:46:58
 */
@Controller
@RequestMapping("/mobile/backend/xs/")
public class XSCoreController {

	
	@Autowired
	private IndexService indexService;
	
	// 跳转至 资讯列表页面
	@RequestMapping("/temp/{type}")
	public String test(@PathVariable String type, HttpServletRequest request, Model model) {
//		Map<String, Integer> QXmap = indexService.getPower(request);
//		model.addAttribute("QXmap", QXmap);
		return "xs/"+type;
	}
	
	
/*	// 跳转至 资讯列表页面
	@RequestMapping("/news/tolist")
	public String showNewslist(HttpServletRequest request, Model model){
		
		return "xunsi/news";
	}
	
	// 跳转至 资讯发布页面
	@RequestMapping("/news/topublish")
	public String publishNews(HttpServletRequest request, Model model){
		
		return "xunsi/news_add";
	}
	
	@RequestMapping("/news/toedit/{pagename}")
	public String showItemList(@PathVariable String pagename, HttpServletRequest request, Model model) {
//		Map<String, Integer> QXmap = indexService.getPower(request);
//		model.addAttribute("QXmap", QXmap);
		return "xunsi/"+pagename;
	}*/
	

}

package com.shopx5.controller.sso;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.shopx5.common.Result;
import com.shopx5.jedis.JedisClient;
import com.shopx5.pojo.TbMobilePartUser;
import com.shopx5.pojo.TbMobileUser;
import com.shopx5.pojo.TbMobileUserLoginInfo;
import com.shopx5.service.common.MobilePartService;
import com.shopx5.service.common.MobileRegisterService;
import com.shopx5.utils.BaiduMapUtils;
import com.shopx5.utils.Const;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.SmsUtil;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

//import nl.bitwalker.useragentutils.Browser;
//import nl.bitwalker.useragentutils.OperatingSystem;
//import nl.bitwalker.useragentutils.UserAgent;

/**
 * 移动端注册控制器
 * @author sjx
 * 2019年6月26日 下午2:47:26
 */
@CrossOrigin
@Controller
public class MobilRegLoginController {

	
	@Autowired 
	private JedisClient jedisClient;
	
	@Autowired
	private MobileRegisterService mobileRegisterService;
	
	@Autowired
	private MobilePartService  mobilePartService;
	
	@Autowired
	private HttpServletRequest request; 
	
	
	/**
	 * 发送验证码
	 * @param phone
	 * @param request
	 * @return
	 * @throws Exception
	 */
	/**
	 * GET、POST方式提交的请求：

		Content-type：
		
		1、application/x-www-form-urlencoded:@RequestBody不是必须加的
		
		2、mutipart/form-data:@RequestBody不能处理这种格式
		
		3、其他格式，比如application/json,application/xml等，必须使用@RequestBody来处理
		
		PUT方式提交的请求：
		
		以上1和3的场景都是必须使用@RequestBody来处理的，2场景也是不支持的
	 */
	@RequestMapping(value = "/mobile/user/getSmsCaptcha", method =RequestMethod.POST)
	@ResponseBody
	public Result getSmsCaptcha(@RequestBody JSONObject jsonObj, HttpServletRequest request) throws Exception{
		
		String phone = (String)jsonObj.get("phone");
		HttpSession session = request.getSession();
		//1. 手机号非空校验
		if (StringUtils.isBlank(phone)) {
			return Result.build(401, "手机号不能为空", "FAIL");
		}
		//2. sms数据配置
		Map<String,String> map = new HashMap<>();
		map.put("mobile", phone);
		map.put("template_code", Const.template_code);
		map.put("sign_name", Const.sign_name);
		String num = IDUtils.get6NUM();
		map.put("param", "{\"code\":\""+num+"\"}");
		// 发送验证码
		SendSmsResponse response = SmsUtil.sendSms(map.get("mobile"), map.get("sign_name"), map.get("template_code"), map.get("param"));
		if (response.getCode().equals("OK") && response.getMessage().equals("OK")) {
			// 将手机号码和验证码保存到session/redis中
//			session.setAttribute(phone, num);
			jedisClient.set(phone, num);
			jedisClient.expire(phone, 300);
			return Result.build(200, "验证码发送成功", "SUCCESS");
		}
		return Result.build(400, "验证码发送失败", "FAIL");
	}
	

	
	/**
	 * 验证码校验
	 * ***如果是新用户注册，校验验证码成功后，自动为该用户注册 ；
	 *    否则，为用户登录时的普通校验
	 * @param phone
	 * @param sms_captcha
	 * @param request
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping("/mobile/user/checkCaptcha")
	@ResponseBody
//	public Result checkCaptcha(@RequestBody String phone, @RequestBody String sms_captcha, HttpServletRequest request){
	public Object checkCaptcha(@RequestBody JSONObject jsonObj, HttpServletRequest request) throws IOException{
		
		Map<String, Object> map = new HashMap<String,Object>();
		
		String phone = (String) jsonObj.get("phone");
		String sms_captcha = (String) jsonObj.get("sms_captcha");
		
		String device = jsonObj.getString("model");    // 登录设备型号
		String clientId = jsonObj.getString("clientid");    // 个推客户端id
		String langitude = jsonObj.getString("langitude");    // 经度
		String latitude = jsonObj.getString("latitude");    // 纬度
		
		// 从jedis中取出验证码
//		String sms = jedisClient.get(phone);
//		
//		if (null == sms) {
//			map.put("status", "401");
//			map.put("msg", "该手机号未进行发送验证码操作");
//			map.put("data", "FAIL");
//			return map;
//		}else if(!sms.equals(sms_captcha)){
//			map.put("status", "400");
//			map.put("msg", "验证码校验失败");
//			map.put("data", "FAIL");
//			return map;
//		}
		

		Properties properties = System.getProperties();    // 读取平台信息
		Properties prop = new Properties();    // 读取resources下文件
		String absPath = MobilRegLoginController.class.getResource("").getFile().replace("com/shopx5/controller/sso/", "");    // 当前文件绝对路径
		String userInfoProPath = null;    // userInfo.properties配置文件路径
		String tokenId = IDUtils.get32UUID();
		Random rand = new Random();
		
		TbMobileUserLoginInfo tbLoginInfo = new TbMobileUserLoginInfo();    // 记录  个人中心 - 设置 - 最近登陆信息
		/**
		 *  用户未注册,进行注册，并返回验证码校验结果
		 */
		if (null == checkPhone(phone)) {
			// C:\Users\Administrator\Desktop\云主机搭建\workspace_szc\shopx5_a2\src\main\java\com\shopx5\controller\sso\MobilRegLoginController.java
			// C:\Users\Administrator\Desktop\云主机搭建\workspace_szc\shopx5_a2\src\main\resources\merchantInfo.properties
			if (properties.getProperty("os.name").toLowerCase().contains("linux")) {
				System.out.println("======:\t "+ absPath);  // absPath = /root/tomcat7/webapps/ROOT/WEB-INF/classes/
				userInfoProPath = absPath + "userInfo.properties";
				
			}else if (properties.getProperty("os.name").toLowerCase().contains("windows")) {
				userInfoProPath = absPath.replace("/C:", "C:") + "userInfo.properties";
			}
//			InputStream in = new BufferedInputStream(new FileInputStream(userInfoProPath));
			prop.load(new InputStreamReader(new FileInputStream(userInfoProPath), "UTF-8"));
			//C:\Users\Administrator\Desktop\云主机搭建\workspace_szc\shopx5_a2\src\main\webapp\pic_upload\20190710165423867.png
			List<String> fstNickNameList = new ArrayList<String>();
			List<String> lstNickNameList = new ArrayList<String>();
			List<String> sigList = new ArrayList<String>();
			String[] arr =  prop.getProperty("nick_name_fst").split(",");
			String[] arr2 =  prop.getProperty("nick_name_lst").split(",");
			String[] arr3 =  prop.getProperty("signature").split(";");
			Collections.addAll(fstNickNameList, arr);
			Collections.addAll(lstNickNameList, arr2);
			Collections.addAll(sigList, arr3);    // 字符串数组转List
			Integer sizeFst = fstNickNameList.size();
			Integer sizeLst = lstNickNameList.size();
			Integer sizeSig = sigList.size();
			
			
			String nick_name = fstNickNameList.get(rand.nextInt(sizeFst)) + prop.getProperty("connect_char") +
					lstNickNameList.get(rand.nextInt(sizeLst));    // 随机生成一个昵称
			String signature = sigList.get(rand.nextInt(sizeSig));    // 随机获取一个签名
			String usr_image = prop.getProperty("defaultUsrImageName");    // 设置默认头像
			
			Long usrItemId = IDUtils.genItemId();
			/**
			 * new两个实体
			 */
			TbMobileUser mbUser = new TbMobileUser();
			TbMobilePartUser mPartUser = new TbMobilePartUser();
			
			// ============================TbMobileUser start...================================================
			mbUser.setId(usrItemId);
			mbUser.setClientId(clientId);    // unipush客户端id
			mbUser.setPhone(phone);
			mbUser.setRealName("");
			mbUser.setNickName(nick_name);
			
			mbUser.setToken(tokenId);
			
			mbUser.setHeatCollectNotify("open");
			mbUser.setCommentReplyNotify("open");
			mbUser.setConcernNotify("open");
			mbUser.setClientId(clientId);

			mbUser.setSignature(signature);
			mbUser.setPrefix("");
			mbUser.setSuffix("");
			mbUser.setImage(usr_image);
			
			mbUser.setConcernCount(0);     //release_counts, money_counts, jf_counts, unread_msg_cnts
			mbUser.setFansCount(0);
			mbUser.setReleaseCounts(0);
			mbUser.setUserType("2");    // 用户类型    0-个人   1-企业   2 - 未认证
			mbUser.setMoneyCounts(0);
			mbUser.setJfCounts(0);
			mbUser.setUnReadMsgCnts(0);
			
			mbUser.setCreateTime(new Date());
			mbUser.setUpdateTime(new Date());
			mbUser.setLoginTimes(1);
			mobileRegisterService.add(mbUser);    // mobileUser保存到数据库
			// ============================TbMobileUser end...================================================
			
			
			// ============================TbMobileUserLoginInfo start...================================================
			tbLoginInfo.setId(IDUtils.get32UUID());
			tbLoginInfo.setDevice(device);
			if (null != langitude && null != latitude) {
				tbLoginInfo.setLongitude(Double.parseDouble(langitude));
				tbLoginInfo.setLatitude(Double.parseDouble(latitude));
			}
			mobileRegisterService.saveLoginInfoEntity(tbLoginInfo);   // save操作
			// ============================TbMobileUserLoginInfo end...==================================================
			
			
			// ============================TbMobilePartUser start...================================================
			mPartUser.setId(usrItemId);
			mPartUser.setClientId(clientId);
			mPartUser.setImage(usr_image);
			mPartUser.setRealName("");
			mPartUser.setPhone(phone);
			mPartUser.setNickName(nick_name);
			mPartUser.setUserType("2");
			mPartUser.setToken(tokenId);
			mobilePartService.add(mPartUser);    // mPartUser保存到数据库
			// ============================TbMobilePartUser end...================================================
			
			map.put("status", "200");
			map.put("msg", "校验通过，新用户注册成功");
			map.put("data", "SUCCESS");
			map.put("userId", usrItemId.toString());
			map.put("access_token", tokenId);
			if (properties.getProperty("os.name").toLowerCase().contains("linux")) {
				usr_image = "http://api.wosiwz.com:8080/pic_upload/" + usr_image;
			}else if (properties.getProperty("os.name").toLowerCase().contains("windows")) {
				usr_image = "http://localhost:8085/pic_upload/" + usr_image;
			}
			map.put("nick_name", nick_name);     // 昵称
			map.put("image_url", usr_image);    // 头像
			map.put("signature", signature);    // 签名
			if (null != langitude && null != latitude) {
				map.put("currLoginInfo", device + " " + new Date() + " " + BaiduMapUtils.getAddress(langitude, latitude));
			}else{
				map.put("currLoginInfo", device + " " + new Date());
			}
			map.put("prefix", "");
			map.put("suffix", "");
			map.put("heat_collect_notify", "open");
			map.put("comment_reply_notify", "open");
			map.put("concern_notify", "open");
			
			map.put("concernCnts", "0");    // 关注用户数目
			map.put("fansCnts", "0");    // 粉丝数目
			map.put("releaseCnts", "0");    // 发布数目(包括图文/视频/原创)
			map.put("userType", "2");    // 2 - 未认证
			map.put("jfCnts", "0");    // 积分数
			map.put("moneyCnts", "0");    // 余额
			map.put("unReadMsgCnts", "0");    // 未读消息数目
			return map;
		}
		
		/**
		 * 用于已注册，只进行校验
		 */
		TbMobileUser tbMobileUser = mobileRegisterService.findPartFieldsByPhone(phone);
		Integer loginTimes = tbMobileUser.getLoginTimes() + 1;
		mobileRegisterService.updateTokenClientIdLoginTimes(tokenId, clientId, loginTimes, tbMobileUser.getId());
		
		tbLoginInfo.setId(IDUtils.get32UUID());
		tbLoginInfo.setDevice(device);
		if (null != langitude && null != latitude) {
			tbLoginInfo.setLongitude(Double.parseDouble(langitude));
			tbLoginInfo.setLatitude(Double.parseDouble(latitude));
		}
		mobileRegisterService.saveLoginInfoEntity(tbLoginInfo);   // save操作
		
		map.put("status", "201");
		map.put("msg", "校验通过，该用户可以登录");
		map.put("data", "SUCCESS");
		map.put("userId",tbMobileUser.getId().toString());
		map.put("access_token", tokenId);
		
		map.put("nick_name", tbMobileUser.getNickName());     // 昵称
		map.put("image_url", tbMobileUser.getImage());    // 头像
		map.put("signature", tbMobileUser.getSignature());    // 签名
		map.put("prefix", tbMobileUser.getPrefix());    // 认证key
		map.put("suffix", tbMobileUser.getSuffix());    // 认证value
		map.put("heat_collect_notify", tbMobileUser.getHeatCollectNotify());
		map.put("comment_reply_notify", tbMobileUser.getCommentReplyNotify());
		map.put("concern_notify", tbMobileUser.getConcernNotify());
		
		if (null != langitude && null != latitude) {
			map.put("currLoginInfo", device + " " + new Date() + " " + BaiduMapUtils.getAddress(langitude, latitude));
		}else{
			map.put("currLoginInfo", device + " " + new Date());
		}
		map.put("concernCnts", tbMobileUser.getConcernCount());    // 关注用户数目
		map.put("fansCnts", tbMobileUser.getFansCount());    // 粉丝数目
		map.put("releaseCnts", tbMobileUser.getReleaseCounts());    // 发布数目(包括图文/视频/原创)
		map.put("userType", tbMobileUser.getUserType());    // 2 - 未认证
		map.put("jfCnts", tbMobileUser.getJfCounts());    // 积分数
		map.put("moneyCnts", tbMobileUser.getMoneyCounts());    // 余额
		map.put("unReadMsgCnts", tbMobileUser.getUnReadMsgCnts());    // 未读消息数目
		
//		jedisClient.del(phone);
		return map;
	}
	
	
	
	/**
	 * 检测手机号是否已经注册
	 * @param phone
	 * @return 主键id
	 */
	private Long checkPhone(String phone) {
		Long m_user_id = mobileRegisterService.checkPhone(phone);
		return m_user_id;
	}
	
	/**
	 * 获取ajax请求中header里的token值 - Authorization
	 *      headers :{
                "Authorization":"xxxxx",
                'Content-Type':'application/x-www-form-urlencoded'
            },
            
      APP端的格式一般为：    data['headers']['Authorization'] = 'SK ' + tokenData.session_key;
	 * @param headers
	 * @return
	 */
	@RequestMapping(value="/mobile/test/getToken",method=RequestMethod.POST)
	@ResponseBody
	public String getTokenInHead(@RequestHeader("User-Agent")String userAgent,@RequestHeader(value="Accept")String []accepts){
		
		System.out.println("通过@RequestHeader获得User-Agent:\t" + userAgent);
		System.out.println("通过@RequestHeader获得Accept: ");
		for(String accept:accepts) {
			System.out.println(accept + "\t");
		}
		// 打印token信息 Authorization 的值
		System.out.println("\r Authorization from request:" + request.getHeader("Authorization"));  
		return "ok";
	}
	
	
	
	/**
	 * 测试读取properties属性文件
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/mobile/test/property", method=RequestMethod.POST)
	@ResponseBody
	public String TestProperty() throws IOException{

		Properties pro = new Properties();
		// 绝对路径 C:/Users/Administrator/Desktop/云主机搭建/workspace_szc/shopx5_a2/target/classes/server.properties
		String path = (MobilRegLoginController.class.getResource("/").getPath()+"server.properties").replace("/C:/", "C:/");
		FileInputStream in = new FileInputStream(path);
		pro.load(in);
		System.out.println("======:\t" + pro.getProperty("upload_file_path"));
		in.close();
		return "ok";
	}
}

package com.shopx5.controller.sso;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.Result;
import com.shopx5.controller.BaseController;
import com.shopx5.jedis.JedisClient;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.result.User;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.common.LoginService;
import com.shopx5.service.common.RegisterService;
import com.shopx5.utils.Const;
import com.shopx5.utils.CookieUtils;
import com.shopx5.utils.JsonUtils;
/**
 * @author XS多商户商城系统
 */
@Controller
public class LoginController extends BaseController {
	@Value("${TOKEN_KEY}")
	private String TOKEN_KEY;         //TOKEN_X5
	@Value("${CURRENT_USER_INFO}")
	private String CURRENT_USER_INFO; //_CURRENT_USER
	@Value("${CART_KEY}")
	private String CART_KEY;     //X5_CART
	@Value("${USER_CART}")
	private String USER_CART;
	@Autowired
	private JedisClient jedisClient;
	@Autowired
	private RegisterService registerService;
	@Autowired
	private LoginService loginService;	
	@Autowired
	private UserMapper uMapper;
	@Autowired
	private IndexService indexService;
		
	//默认登录_手机登录
	@RequestMapping("/user/login")
	@ResponseBody
	public Result login(String userName, String password, String captcha, String auto_login, String saveName,
			            String phone, String sms_captcha, String sclog,
			            HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		//记录日志
		logger.info("登录操作: "+userName+password+captcha);
		logger.debug("测试handler的类型：");
		logger.error("系统发生异常");
		logBefore(logger, "登录操作开始");
		
		//手机动态码登录_认证
		if (StringUtils.isNotBlank(phone)) {
			String phoneCode = (String) request.getSession().getAttribute(phone);
			if (!phoneCode.equals(sms_captcha))  return Result.build(400, "认证码不正确!");
			
			//1.获取用户密码
			SpUser spUser = registerService.getSpUserByPhone(phone);
			userName = spUser.getUserName();
			password = spUser.getPassword();
		}else {
			//用户密码不为空
			if (StringUtils.isBlank(userName)) {
				return Result.build(400, "用户名不能为空");
			}
			if (StringUtils.isBlank(password)) {
				return Result.build(400, "密码不能为空");
			}
		}
		
		if(sclog == null){ //紧紧过滤下收藏登录
			//获取一次性验证码 用完之后从session中移除
			HttpSession session = request.getSession();
			String sCode = (String) session.getAttribute(Const.SESSION_LOGIN_CODE);
			session.removeAttribute(Const.SESSION_LOGIN_CODE);
			//2. redis方式
			//	String sCode = getjedisCode(Const.SESSION_REG_CODE, request, 1);

			//判断两个验证码是否一致. 去掉字符串两边空格长度还为0 空值
			if(StringUtils.isBlank(captcha) || sCode == null){
				//验证码有问题 提示信息 页面跳转到login.jsp
				return Result.build(400, "请重新输入验证码");
			}
			if(!captcha.equalsIgnoreCase(sCode)){
				//验证码输入不一致 提示信息 页面跳转到login.jsp
				return Result.build(400, "验证码输入错误");
			}
		}
		//取user,保存redis并设置过期时间★
		Result result = loginService.login(userName, password, auto_login);
		//登录成功后_把token写入cookie★
		if (result.getStatus() != 200) {
			return Result.build(400, "用户或密码错误");			
		}
		CookieUtils.setCookie(request, response, TOKEN_KEY, result.getData().toString());
		//判断是否勾选了自动登录  若勾选了将用户名和密码放入cookie
		if("ok".equals(saveName)){
			CookieUtils.setCookie(request, response, "saveName", userName+"-"+password, 3600, true);
		}
		
		//操作日志,持久化		
		saveLog(userName, "登录成功", request);		
		
		//获取token
		Result re = loginService.getUserByToken((String)result.getData());
		SpUser spUser = (SpUser) re.getData();
		logAfter(logger,"登录操作完成!!!");
		if (spUser != null) {
			/*
			 *  登陆成功后,判断是否有购物车 cookie合并如redis
			 */
	//		if (indexService.getCart(request).getCartItemList().size() > 0) {
	//			this.cartToServer(request, response, spUser.getUserId());
	//		}					
			/*
			 *  查询用户角色:输入哪个组
			 */
			User userNew = uMapper.findUserAndRole(spUser.getUserId());
			if (userNew == null) {
				result.setMsg("/shop/toMain");
				return result;
			}
			Integer i = userNew.getRoleList().get(0).getCategory();
			//1系统管理组
			if (i == 1){
				result.setMsg("/manage/toMain");
				return result;
			}
			//2会员组
			if (i == 2){
				result.setMsg("/shop/toMain");
				return result;
			}
		}
		
		//给数据返回
		return result;
	}
	
	//1获取短信验证码 ("/user/getSmsCaptcha")随机注册,并发验证码
	//2手机动态码登录
	@RequestMapping("/user/tel_login")
	@ResponseBody
	public Result login(String phone, String captcha, String sms_captcha, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		String sms = (String) request.getSession().getAttribute(phone);
		if (sms_captcha.equals(sms)) {			
			/*
			 *  登陆成功后,判断是否有购物车 cookie合并如redis
			 */
			//通过手机号码查询对象
			User user = uMapper.getUserByPhone(phone);
			//取user,保存redis并设置过期时间
			Result result = loginService.login(user.getUserName(), "phonePass", null);
			//登录成功后_把token写入cookie
			if (result.getStatus() != 200) {
				return Result.build(400, "用户或密码错误");			
			}
			CookieUtils.setCookie(request, response, TOKEN_KEY, result.getData().toString());
			
			//操作日志,持久化		
			saveLog(user.getUserName(), "登录成功", request);		
			
			//获取token
			Result re = loginService.getUserByToken((String)result.getData());
			SpUser spUser = (SpUser) re.getData();
			if (spUser != null) {
				/*
				 *  登陆成功后,判断是否有购物车 cookie合并如redis
				 */
				/*if (indexService.getCart(request).getCartItemList().size() > 0) {
					this.cartToServer(request, response, spUser.getUserId());
				}*/
				/*
				 *  查询用户角色:输入哪个组
				 */
				User userNew = uMapper.findUserAndRole(spUser.getUserId());
				if (userNew == null) {
					result.setMsg("/shop/toMain");
					return result;
				}
				Integer i = userNew.getRoleList().get(0).getCategory();
				//1系统管理组
				if (i == 1){
					result.setMsg("/manage/toMain");
					return result;
				}
				//2会员组
				if (i == 2){
					result.setMsg("/shop/toMain");
					return result;
				}
			}
			
			//给数据返回
			return result;
		}
		return Result.build(400, "验证码输入错误");
	}
	
	/**
	 * 退出
	 * @param request
	 * @return
	 */
	@RequestMapping("/{user}/logout")
	public String logout(@PathVariable String user, HttpServletRequest request){
		//1. session方式
		//request.getSession().removeAttribute(CURRENT_USER_INFO); //清空    request.getSession().invalidate(); 销毁session	    
		
		//2. redis方式
	    String value = CookieUtils.getCookieValue(request, TOKEN_KEY);
		loginService.logout(value);
		if(user.equals("user")){
			user = "page";
		}
		return "redirect:/"+user+"/login";
	}
		
	
	//不加 dataType:"jsonp"请求为          http://localhost:8081/user/token/3b794c61-0854-4112-8996-8be609c49cee
	//  加dataType:"jsonp"请求变为       http://localhost:8081/user/token/3b794c61-0854-4112-8996-8be609c49cee?callback=jQuery9415502&_=1532066695890
	//                              ::返回  为user的Json对象 或 Result.data(user)  200 "OK" user ★★
    //3.通过coookie中token获取用户信息	                                                  //指定返回响应数据的content-type为json格式 jsonp函数才能执行★
	@RequestMapping(value="/user/token/{token}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public String getUserByToken(@PathVariable String token, String callback) {
		Result result = loginService.getUserByToken(token);
		//判断是否为jsonp请求
		if (StringUtils.isNotBlank(callback)) {
			return callback + "(" + JsonUtils.objectToJson(result) + ");";  //返回执行js语句
		}        //callback(Result);
		//同端口时
		return JsonUtils.objectToJson(result);  //对象内对象\"会去掉★
	}
		
	//获取Jedis_code验证码
	private String getjedisCode(String code, HttpServletRequest request, Integer i){
		Cookie cookie = CookieUtils.getCookieByName(code, request.getCookies());
		String regCode = jedisClient.get(code + ":" + cookie.getValue());
		if (i == 1) {
			//删除key
			jedisClient.del(code + ":" + cookie.getValue());
		}
		return regCode;
	}
}

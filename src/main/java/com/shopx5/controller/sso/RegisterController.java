package com.shopx5.controller.sso;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.shopx5.common.Result;
import com.shopx5.jedis.JedisClient;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.service.common.RegisterService;
import com.shopx5.utils.Const;
import com.shopx5.utils.CookieUtils;
import com.shopx5.utils.IDUtils;
import com.shopx5.utils.SmsUtil;
/**
 * ShopX5 ShopX5多商户商城系统
 * @author sjx
 * 2019年6月26日 下午2:45:51
 */
@Controller
public class RegisterController {
	@Autowired
	private JedisClient jedisClient;
	@Autowired
	private RegisterService registerService;
	
	//1.校验数据 Get请求
	@RequestMapping("/check/{param}/{type}")
	@ResponseBody
	public String checkUserData(@PathVariable String param, @PathVariable Integer type, HttpServletRequest request, 
			                    String userName, String phone, String email, String captcha) throws Exception{
		//请求转码
		param = new String(param.getBytes("iso-8859-1"),"utf-8");
		if (StringUtils.isNotBlank(userName)) {
			param = new String(userName.getBytes("iso-8859-1"), "utf-8");
		}else if (StringUtils.isNotBlank(phone)) {
			param = phone;
		}else if (StringUtils.isNotBlank(email)) {
			param = new String(email.getBytes("iso-8859-1"), "utf-8");
		}else if (StringUtils.isNotBlank(captcha) && type == 4) {
			//1. session方式 校验Code
			if(!captcha.equalsIgnoreCase((String) request.getSession().getAttribute(Const.SESSION_REG_CODE))){
				return "false";
			}else {
				return "true";
			}
//			//2. redis方式 校验Code
//			String code = getjedisCode(Const.SESSION_REG_CODE, request, null);
//			if(!captcha.equalsIgnoreCase(code)){
//				return "false";
//			}else {
//				return "true";
//			}
		}		
		//校验数据
		String check = registerService.checkData(param, type);
		//响应
		return check;
	}
	
	//用户注册
	@RequestMapping("/user/register")
	@ResponseBody
	public Result register(SpUser spUser, SpUserInfo spUserInfo, String captcha, HttpServletRequest request) throws Exception{
		//1. session方式 获取一次性验证码 用完之后从session中移除
		HttpSession session = request.getSession();
		String sCode = (String) session.getAttribute(Const.SESSION_REG_CODE);
		session.removeAttribute(Const.SESSION_REG_CODE);
		//2. redis方式
//		String sCode = getjedisCode(Const.SESSION_REG_CODE, request, 1);
		
		//判断两个验证码是否一致. 去掉字符串两边空格长度还为0 空值
		if(StringUtils.isBlank(captcha) || StringUtils.isBlank(sCode)){
			//验证码有问题 提示信息 页面跳转到login.jsp
			return Result.build(400, "请重新输入验证码");
		}
		if(!captcha.equalsIgnoreCase(sCode)){
			//验证码输入不一致 提示信息 页面跳转到login.jsp
			return Result.build(400, "验证码输入错误");
		}
		//检查数据的有效性
		if (StringUtils.isBlank(spUser.getUserName())) {
			return Result.build(400, "用户名不能为空");
		}
		//判断用户名是否重复
		String checkUserName = registerService.checkData(spUser.getUserName(), 1);
		if (checkUserName.equals("false")) {
			return Result.build(400, "用户名重复");
		}
		//判断密码是否为空
		if (StringUtils.isBlank(spUser.getPassword())) {
			return Result.build(400, "密码不能为空");
		}
	    if (StringUtils.isNotBlank(spUserInfo.getTelephone())) {
			//是否重复校验
	    	String checkPhone = registerService.checkData(spUserInfo.getTelephone(), 2);
			if (checkPhone.equals("fasle")) {
				return Result.build(400, "电话号码重复");
			}
		}
		//如果email不为空的话进行是否重复校验
		String checkEmail = registerService.checkData(spUserInfo.getEmail(), 3);
		if (checkEmail.equals("false")) {
			return Result.build(400, "email重复");
		}
		//插入数据
		Result result = registerService.register(spUser, spUserInfo);
		//返回注册成功
		return result;
	}
	
	//获取短信验证码
	@RequestMapping("/user/getSmsCaptcha")
	@ResponseBody
	public String getSmsCaptcha(Integer type, String captcha, String phone, HttpServletRequest request) throws Exception{
		//1. 校验Code
		HttpSession session = request.getSession();
		if (type == 1) {			
			if(!captcha.equalsIgnoreCase((String) session.getAttribute(Const.SESSION_REG_CODE)))
			//2. redis方式
//			if(!captcha.equalsIgnoreCase(getjedisCode(Const.SESSION_REG_CODE, request, null)))	
				return "false";
		}else if (type == 2) {
			if(!captcha.equalsIgnoreCase((String) session.getAttribute(Const.SESSION_LOGIN_CODE)))
			//2. redis方式
//			if(!captcha.equalsIgnoreCase(getjedisCode(Const.SESSION_LOGIN_CODE, request, null)))
				return "false";
			//1.判断手机是否注册过
			SpUserInfo userInfo = registerService.getUserInfo(phone);
			if (userInfo == null) {
				//2.没注册过_生成随机用户/密码 注册成功
				String userName = IDUtils.getUserName(7, 3);
				String password = IDUtils.getPassword(1, 8);
				//判断用户名 是否重复
				userName = checkName(userName);
				SpUser spUser = new SpUser();
				spUser.setUserName(userName);
				spUser.setPassword(password);
				SpUserInfo spUserInfo = new SpUserInfo();
				spUserInfo.setTelephone(phone);
				//2. 插入数据
				Result result = registerService.register(spUser, spUserInfo);
			}
			
		}
		//2. 配置sms数据
		Map<String,String> map = new HashMap<>();
		map.put("mobile", phone);
		map.put("template_code", Const.template_code);
		map.put("sign_name", Const.sign_name);
		String num = IDUtils.get6NUM();
		map.put("param", "{\"code\":\""+num+"\"}");
		//发送验证码
		SendSmsResponse response = SmsUtil.sendSms(map.get("mobile"), map.get("sign_name"), map.get("template_code"), map.get("param"));
		if (response.getCode().equals("OK") && response.getMessage().equals("OK")) {
			//将手机号码和验证码保存到session/redis中
			session.setAttribute(phone, num);
			return "true";
		}
		return "false";
	}
	
	//短信下一步
	@RequestMapping("/user/checkCaptcha")
	@ResponseBody
	public Result checkCaptcha(String phone, String sms_captcha, HttpServletRequest request) throws Exception{
		//1. 校验手机验证码
		String sms   = (String) request.getSession().getAttribute(phone);
		String sCode = (String) request.getSession().getAttribute(Const.SESSION_REG_CODE);
		
		//2. redis方式
//		String sms = getjedisCode(phone, request, null);
//		String sCode = getjedisCode(Const.SESSION_REG_CODE, request, null);
		
		if (!sms.equals(sms_captcha)) {
			return Result.build(400, "false");
		}		
		//2. 设置随机用户名,随机密码
		String username = IDUtils.getUserName(7, 3);
		String password = IDUtils.getPassword(1, 8);
		//判断用户名 是否重复
		username = checkName(username);
		//封装数据
		Map<String, String> map = new HashMap<>();
		map.put("phone", phone);
		map.put("username", username);
		map.put("password", password);
		map.put("captcha", sCode);
		//3. 数据响应
		return Result.ok(map);
	}
	
	//校验_随机用户名_是否重复
	public String checkName(String userName) throws Exception{
		String check = registerService.checkData(userName, 1);
		if (check.equals("false")) {
			userName = IDUtils.getUserName(7, 3);
			userName = checkName(userName);
		}
		return userName;
	}
	
	//获取_Jedis_code验证码
	public String getjedisCode(String code, HttpServletRequest request, Integer i){
		String value = CookieUtils.getCookieValue(request, code);
		String regCode = jedisClient.get(code + ":" + value);
		if (i == 1) {
			//删除key
			jedisClient.del(code + ":" + value);
		}
		return regCode;
	}
}

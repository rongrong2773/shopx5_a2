package com.shopx5.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.shopx5.pojo.SpUser;
import com.shopx5.utils.CookieUtils;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
public class AutoLoginInterceptor implements HandlerInterceptor {
	@Value("${CURRENT_USER_INFO}")
	private String CURRENT_USER_INFO;
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
	}
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {		
		//判断session中是否登录用户
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		SpUser spUser = (SpUser) session.getAttribute(CURRENT_USER_INFO);
		
		//没有自动登录
		if(spUser == null){
			//判断访问的资源是否与登录注册相关,排除登录页面,其他页面才自动登录
			String path = request.getRequestURI();
			if(!path.contains("/login")){				
				//获取指定的cookie
				//Cookie c = CookieUtils.getCookieByName("autoLogin", request.getCookies());
				//String username=c.getValue().split("-")[0];
				String value = CookieUtils.getCookieValue(request, "autoLogin", true);
				if(value != null){
					String userName = value.split("-")[0];
					String password = value.split("-")[1];
					//调用serivce完成登录
					try {
						//1.得到Subject			
						//2.调用登录方法
						UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
						subject.login(token);//当这一代码执行时，就会自动跳入到AuthRealm中认证方法			
						//3.登录成功时，就从Shiro中取出用户的登录信息
						spUser = (SpUser) subject.getPrincipal();
						//4.将用户放入session域中
						session.setAttribute(CURRENT_USER_INFO, spUser);
					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}					
				}
			}			
		}
		//放行
		return true;
	}
}

package com.shopx5.controller.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.shopx5.common.Result;
import com.shopx5.jedis.JedisClient;
import com.shopx5.mapper.result.RoleMapper;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpModule;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.result.Button;
import com.shopx5.pojo.result.Role;
import com.shopx5.pojo.result.User;
import com.shopx5.service.common.LoginService;
import com.shopx5.service.manager.ModuleService;
import com.shopx5.utils.CookieUtils;
import com.shopx5.utils.JsonUtils;
import com.shopx5.utils.RightsHelper;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
public class RolePowerInterceptor implements HandlerInterceptor {
	@Value("${TOKEN_KEY}")
	private String TOKEN_KEY;
	@Value("${CURRENT_USER_INFO}")
	private String CURRENT_USER_INFO;
	@Value("${ALL_MODULE}")
	private String ALL_MODULE;
	@Value("${USER_QX}")
	private String USER_QX;
	@Value("${USER_BUTTON}")
	private String USER_BUTTON;
	@Value("${SESSION_EXPIRE}")
	private Integer SESSION_EXPIRE;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private LoginService loginService;
	@Autowired
	private JedisClient jedisClient;
	
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		try {
			//获取用户 通过token
			String token = CookieUtils.getCookieValue(request, TOKEN_KEY);
			Result result = loginService.getUserByToken(token);
			SpUser spUser = (SpUser) result.getData();
			if (spUser == null) return false;
			//获取完整用户
			User user = userMapper.findUserAndRole(spUser.getUserId());
			//取请求路径   可正则内部过滤
			String path = request.getServletPath();
			/*if (!path.matches(Constant.NO_INTERCEPTOR_PATH)) return false; */
			
			//获取用户角色列表			
			List<Role> rList = user.getRoleList();
			
			//1.封装所有角色按钮权限
			if (jedisClient.get(USER_BUTTON + ":" + token) == null) {
				Map<String, Integer> bMap = new HashMap<>();
				//取所有角色的按钮
				for (Role role : rList) {
					role = roleMapper.findRoleAndButton(role.getRoleId());
					if (role == null) {
						continue;
					}
					List<Button> list = role.getButtonList();
					for (Button button : list) {
						bMap.put(button.getQxName(), 1);   // 如: fromExcel,toExcel,sms,email
					}
				}
				jedisClient.set(USER_BUTTON + ":" + token, JsonUtils.objectToJson(bMap));
				jedisClient.expire(USER_BUTTON + ":" + token, SESSION_EXPIRE);
			}
			
			//2.封装所有角色CURD权限  *匹配当前url**************
			//2.1先取session中allMenuList
			/*if (jedisClient.get(ALL_MODULE) == null) {
				List<SpModule> mList = moduleService.findAll();
				jedisClient.set(ALL_MODULE, JsonUtils.objectToJson(mList));
				jedisClient.expire(ALL_MODULE, SESSION_EXPIRE);
			}
			//2.2校验匹配 请求路径url
			String string = jedisClient.get(ALL_MODULE);
			List<SpModule> mList = JsonUtils.jsonToList(string, SpModule.class);
			Integer moduleId = null;
			for (SpModule spModule : mList) {
				if (spModule.getCurl().equals(path))
					moduleId = spModule.getModuleId();
			}*/
			//2.1获取路径的模块id
			List<SpModule> list = moduleService.getByCURL(path);
			//2.3封装curd权限
			if (list != null && list.size() > 0) {
				Integer moduleId = list.get(0).getModuleId();
				Map<String, Integer> rMap = new HashMap<>();
				for (Role role : rList) {
					if (role.getAddQx() != null && RightsHelper.testRights(role.getAddQx(), moduleId)) {
						rMap.put("add", 1);
					}
					if (role.getDelQx() != null && RightsHelper.testRights(role.getDelQx(), moduleId)) {
						rMap.put("del", 1);
					}
					if (role.getEditQx() != null && RightsHelper.testRights(role.getEditQx(), moduleId)) {
						rMap.put("edit", 1);	
					}
					if (role.getSeeQx() != null && RightsHelper.testRights(role.getSeeQx(), moduleId)) {
						rMap.put("see", 1);
					}
				}
				jedisClient.set(USER_QX + ":" + token, JsonUtils.objectToJson(rMap));
				jedisClient.expire(USER_QX + ":" + token, SESSION_EXPIRE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}
}

package com.shopx5.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.group.Good;
import com.shopx5.service.shop.CollectionService;
import com.shopx5.service.shop.ViewService;
/**
 * @author tang S h o p X 5 多商户商城系统
 * @qq 1503501970
 */
@Controller
public class IndexController {
	@Autowired
	private CollectionService collectionService;
	@Autowired
	private ViewService viewService;
	
	//1  判断user是否登录  "收藏商品"
	@RequestMapping(value="/index/login", method=RequestMethod.POST)
	@ResponseBody
	public Result indexLogin(HttpServletRequest request){
		SpUser user = (SpUser) request.getAttribute("spUser");
		if (user == null) {
			return Result.build(400, "未登录!");
		}
		return Result.ok(1);
	}
	
	// 首页 演示瀑布流效果
	@RequestMapping("/indexScroll")
	public String findPageScroll(@RequestParam(defaultValue="1") Integer page, HttpServletRequest request, Model model){
		//获取收藏数据
		PageBean<Good> pageBean = viewService.findPage(page, "65b1d0fff68f4a718fa6d5ae9ec1a65e");
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/shop/user/index_collection_scroll";
	}
}

package com.shopx5.controller.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SsSpecificationOption;
import com.shopx5.service.shop.SpecificationOptionService;

/**
 * SpecificationOption控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/specificationOption")
public class SpecificationOptionController {
	@Autowired
	private SpecificationOptionService specificationOptionService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<SsSpecificationOption> findAll() {
		return specificationOptionService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<SsSpecificationOption> findByParentId(Long parentId){
		return specificationOptionService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return specificationOptionService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param specificationOption, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody SsSpecificationOption specificationOption, Integer page, Integer rows) {
		return specificationOptionService.findPage(specificationOption, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody SsSpecificationOption specificationOption) {
		try {
			specificationOptionService.add(specificationOption);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public SsSpecificationOption findOne(Long id) {
		return specificationOptionService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody SsSpecificationOption specificationOption) {
		try {
			specificationOptionService.update(specificationOption);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			specificationOptionService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
}

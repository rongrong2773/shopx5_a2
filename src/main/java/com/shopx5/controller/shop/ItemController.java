package com.shopx5.controller.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbItem;
import com.shopx5.service.shop.ItemService;

/**
 * Item控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/item")
public class ItemController {
	@Autowired
	private ItemService itemService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbItem> findAll() {
		return itemService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbItem> findByParentId(Long parentId){
		return itemService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return itemService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param item, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbItem item, Integer page, Integer rows) {
		return itemService.findPage(item, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbItem item) {
		try {
			itemService.add(item);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbItem findOne(Long id) {
		return itemService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbItem item) {
		try {
			itemService.update(item);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			itemService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
}

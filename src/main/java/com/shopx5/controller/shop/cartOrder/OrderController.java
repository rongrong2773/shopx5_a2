package com.shopx5.controller.shop.cartOrder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbOrder;
import com.shopx5.pojo.TbOrderItem;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.OrderService;
import com.shopx5.utils.CookieUtils;

/**
 * Order控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/order")
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private IndexService indexService;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbOrder> findAll() {
		return orderService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbOrder> findByParentId(Long parentId){
		return orderService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return orderService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param order, page, rows, status级别
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbOrder order, Integer page, Integer rows, Integer grade) {
		return orderService.findPage(order, page, rows, grade);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbOrder order) {
		//获取当前登录人账号
//		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		String userId = indexService.getUser(request).getUserId();
		order.setUserId(userId);
		order.setSourceType("2"); //订单来源  PC
		try {
			String outTradeNo = orderService.add(order);
			// 本地购物车清除
			CookieUtils.deleteCookie(request, response, "itemIds");
			// 返回订单id串
			return new Result(true, outTradeNo);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "增加失败");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbOrder findOne(Long id) {
		return orderService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbOrder order) {
		try {
			orderService.update(order);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			orderService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 根据orderId 获取订单项
	@RequestMapping("/findItem")
	public List<TbOrderItem> findItem(Long orderId) {
		return orderService.findItem(orderId);
	}
	// 更新状态
	@RequestMapping("/updateStatus")
	public Result updateStatus(Long orderId, String status) {
		try {
			orderService.updateStatus(orderId, status);
			return new Result(true, "设置成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "设置失败!");
		}
	}
}

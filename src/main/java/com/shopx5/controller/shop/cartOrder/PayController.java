package com.shopx5.controller.shop.cartOrder;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbOrder;
import com.shopx5.pojo.TbPayLog;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.OrderService;
import com.shopx5.service.shop.WeixinPayService;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/pay")
public class PayController {
	@Autowired
	private WeixinPayService weixinPayService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private IndexService indexService;
	@Autowired
	private HttpServletRequest request;
	
	// 创建二维码
	@RequestMapping("/createNative")
	public Map createNative(String outTradeNo, String orderId) {
		// 1.获取当前登录用户
//		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		String userId = indexService.getUser(request).getUserId();
		// 2.1提取支付日志（从缓存取:订单号,支付金额 ）
		TbPayLog payLog = null;
		if(!outTradeNo.equals("undefined") && StringUtils.isNotBlank(outTradeNo)){
			payLog = orderService.searchPayLogFromRedis(userId);
			if (payLog != null) {
				// 调用微信支付接口
				return weixinPayService.createNative(payLog.getOutTradeNo(), payLog.getTotalFee()+"");
			}
		}
		// 2.2用户中心订单
		if(!orderId.equals("undefined") && StringUtils.isNotBlank(orderId)){
			TbOrder order = orderService.findOne(Long.valueOf(orderId));
			return weixinPayService.createNative(orderId, order.getPayment().doubleValue()*100+"");
		}
		return new HashMap<>();
	}

	// 查询状态
	@RequestMapping("/queryPayStatus")
	public Result queryPayStatus(String out_trade_no) throws Exception {
	  //测试:停顿会进入
		Thread.sleep(6000);
		
		Result result = null;
		int x = 0;
		while (true) {
			Map<String, String> map = weixinPayService.queryPayStatus(out_trade_no);// 调用查询
			if (map == null) {
				result = new Result(false, "支付发生错误");
				break;
			}
			if (map.get("trade_state").equals("SUCCESS")) {// 支付成功
				result = new Result(true, "支付成功");
				// 修改订单状态
				orderService.updateOrderStatus(out_trade_no, map.get("transaction_id")); //交易号码
				break;
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			x++;
			if (x >= 100) {
				result = new Result(false, "二维码超时");
				break;
			}
		}
		return result;
	}
}

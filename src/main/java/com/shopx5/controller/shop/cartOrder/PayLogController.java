package com.shopx5.controller.shop.cartOrder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbPayLog;
import com.shopx5.service.shop.PayLogService;

/**
 * PayLog控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/payLog")
public class PayLogController {
	@Autowired
	private PayLogService payLogService;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private HttpServletRequest request;
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbPayLog> findAll() {
		return payLogService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbPayLog> findByParentId(Long parentId){
		return payLogService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return payLogService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param payLog, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbPayLog payLog, Integer page, Integer rows) {
		return payLogService.findPage(payLog, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbPayLog payLog) {
		try {
			payLogService.add(payLog);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbPayLog findOne(Long id) {
		return payLogService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbPayLog payLog) {
		try {
			payLogService.update(payLog);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			payLogService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 获取支付日志 5
	@RequestMapping("/findPayData")
	public TbPayLog findPayData() {
		SpUser user = (SpUser) request.getAttribute("spUser");
		return (TbPayLog) redisTemplate.boundHashOps("payLog").get(user.getUserId());
	}
}

package com.shopx5.controller.shop.cartOrder;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbOrderItem;
import com.shopx5.pojo.group.Cart;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.CartService;
import com.shopx5.utils.CookieUtils;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/cart")
public class CartController {
	@Autowired
	private CartService cartService;
	@Autowired
	private IndexService indexService;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	
	// 添加商品到购物车
	@RequestMapping("/addGoodsToCartList")
//	@CrossOrigin(origins = "http://localhost:9105") // allowCredentials="true" 可以缺省
	public Result addGoodsToCartList(Long itemId, Integer num) {
		// response.setHeader("Access-Control-Allow-Origin", "http://localhost:9105"); //可以访问的域(当此方法不需要操作cookie)
		// response.setHeader("Access-Control-Allow-Credentials", "true");             //如果操作cookie，必须加上这句话
		// 当前登录人账号
//		String name = SecurityContextHolder.getContext().getAuthentication().getName();
//		System.out.println("当前登录人：" + name);
		SpUser user = indexService.getUser(request);
		try {
			// 提取购物车
			List<Cart> cartList = findCartList();
			// 调用服务方法操作购物车
			cartList = cartService.addGoodsToCartList(cartList, itemId, num);
			
			// 将新的购物车存入cookie
			// 匿名角色 未登录
//			if (name.equals("anonymousUser")) {
			if (user == null) {
				String cartListString = JSON.toJSONString(cartList);
//				util.CookieUtil.setCookie(request, response, "cartList", cartListString, 3600*24, "UTF-8");
				CookieUtils.setCookie(request, response, "cartList", cartListString, 3600*24, "UTF-8");
				System.out.println("向cookie中存储购物车");
			// 如果登录
			} else {
				cartService.saveCartListToRedis(user.getUserId(), cartList);
			}
			return new Result(true, "存入购物车成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "存入购物车失败");
		}
	}
	
	//工具类:获取购物车列表
	@RequestMapping("/findCartList")
	public List<Cart> findCartList() {
		// 当前登录人账号
//		String username = SecurityContextHolder.getContext().getAuthentication().getName();
//		System.out.println("当前登录人：" + username);
		SpUser user = indexService.getUser(request);
		String cartListString = CookieUtils.getCookieValue(request, "cartList", "UTF-8");
		if (cartListString == null || cartListString.equals("")) {
			cartListString = "[]";
		}
		List<Cart> cartList_cookie = JSON.parseArray(cartListString, Cart.class);

		// 如果未登录   从cookie中提取购物车
//		if (username.equals("anonymousUser")) {
		if (user == null) {
			System.out.println("从cookie中提取购物车");
			return cartList_cookie;
		// 如果已登录  获取redis购物车
		} else {
			List<Cart> cartList_redis = cartService.findCartListFromRedis(user.getUserId());
			// 判断本地cookie中购物车,存在数据合并
			if (cartList_cookie.size() > 0) {
				// 得到合并后的购物车
				List<Cart> cartList = cartService.mergeCartList(cartList_cookie, cartList_redis);
				// 将合并后的购物车存入redis
				cartService.saveCartListToRedis(user.getUserId(), cartList);
				// 本地购物车清除
				CookieUtils.deleteCookie(request, response, "cartList");
				System.out.println("执行了购物车cookie与redis合并");
				return cartList;
			}
			return cartList_redis;
		}
	}
	
	//工具类:获取已选购物车列表
	@RequestMapping("/findCartYesList")
	public List<Cart> findCartYesList() {
		// 获取已选itemIds
		String itemIds = CookieUtils.getCookieValue(request, "itemIds", "UTF-8");
		List<String> list = JSON.parseArray(itemIds, String.class);
		// 重新封装已选
		List<Cart> cartList = this.findCartList();
		for (Cart cart : cartList) {
			List<TbOrderItem> orderItemList = cart.getOrderItemList();
			Iterator<TbOrderItem> it = orderItemList.iterator();
			while(it.hasNext()){
			    TbOrderItem item = it.next();
			    if(!list.contains(String.valueOf(item.getItemId()))){  //如果itemId不包含,移除
			    	System.out.println(item.getItemId());
			        it.remove();
			    }
			}
		}
		return cartList;
	}
	
	//将itemIdsYes存入cookie []
	@RequestMapping("/addItemIdsYes")
	public Result addItemIdsYes(String[] itemIds) {
		try {
			SpUser user = indexService.getUser(request);
			if (user == null) {
				return new Result(false, "请登录!");
			}
			String ids = JSON.toJSONString(itemIds);
			CookieUtils.setCookie(request, response, "itemIds", ids, 3600*24, "UTF-8");
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 删除商品
	 */
	@RequestMapping("/delete/{itemId}")
	@ResponseBody
	public Result deleteCartItem(@PathVariable Long itemId, HttpServletRequest request, HttpServletResponse response) {
		try {
			SpUser user = indexService.getUser(request);
			List<Cart> cartList = null;
			//1 未登录  获取cookie中cart shopx5
			if (user == null) {
				cartList = findCartList();
				//查询对应商品,并删除
				cartList = this.delNum(cartList, itemId);
				//购物车重新写入cookie
				String cartListString = JSON.toJSONString(cartList);
				CookieUtils.setCookie(request, response, "cartList", cartListString, 3600*24, "UTF-8");
			//2 已登录 获取redis中cart
			}else{
				cartList = cartService.findCartListFromRedis(user.getUserId());
				//查询对应商品,并删除
				cartList = this.delNum(cartList, itemId);
				//购物车重新写入redis
				cartService.saveCartListToRedis(user.getUserId(), cartList);
			}
			//重定向,展示购物车
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	//删除
	private List<Cart> delNum(List<Cart> cartList, Long itemId){
		for (Cart cart : cartList) {
			List<TbOrderItem> orderItemList = cart.getOrderItemList();
			for (TbOrderItem orderItem : orderItemList) {
				if (orderItem.getItemId().equals(itemId)) {
					orderItemList.remove(orderItem);
					break;
				}
			}
			if(orderItemList.size()==0){
				cartList.remove(cart);
				break;
			}
		}
		return cartList;
	}
}

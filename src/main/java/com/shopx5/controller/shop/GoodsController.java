package com.shopx5.controller.shop;

import java.util.List;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbGoods;
import com.shopx5.pojo.group.Goods;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.GoodsService;
import com.shopx5.service.shop.ItemPageService;

/**
 * Goods控制层
 * @author XS 多商户商城系统
 */
@RestController
@CrossOrigin
@RequestMapping("/mobile/shopx5/goods")
public class GoodsController {
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private ItemPageService itemPageService;
	@Autowired
	private IndexService indexService; 
	@Autowired
	private HttpServletRequest request;
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbGoods> findAll() {
		return goodsService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbGoods> findByParentId(Long parentId){
		return goodsService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
//	public PageResult findPage(Integer page, Integer rows) {
	public PageResult findPage(@RequestBody JSONObject jsonObject) {
		Integer page = Integer.parseInt(jsonObject.getString("page"));
		Integer rows = Integer.parseInt(jsonObject.getString("rows"));
		return goodsService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param goods, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbGoods goods, Integer page, Integer rows) {
		// 获得商家信息:并设置
//		String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();
		String sellerId = indexService.getUser(request).getSellerId();
		goods.setSellerId(sellerId);
		return goodsService.findPage(goods, page, rows);
	}
	/**
	 * 运营商: 所有
	 * @param goods, page, rows
	 * @return
	 */
	@RequestMapping("/searchAll")
	public PageResult searchAll(@RequestBody TbGoods goods, Integer page, Integer rows) {
		return goodsService.findPage(goods, page, rows);
	}
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody Goods goods) {
		try {
			// 获得商家信息:并设置
//			String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();
			
			
//			String sellerId = indexService.getUser(request).getSellerId();
//			if (StringUtils.isNotBlank(sellerId)) {
//				goods.getGoods().setSellerId(sellerId);
//			}else {
//				return new Result(false, "商户号才可增加商品!");
//			}
			goodsService.add(goods);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public Goods findOne(Long id) {
		return goodsService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody Goods goods) {
		try {
			goodsService.update(goods);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
		
	//============================= 1.
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private Destination	queueSolrDestination;
	@Autowired
	private Destination queueSolrDeleteDestination;
	
	@Autowired
	private Destination topicPageDestination;
	@Autowired
	private Destination topicPageDeleteDestination;
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(final Long[] ids) {
		try {
			goodsService.delete(ids);
			//从索引库中删除
			//itemSearchService.deleteByGoodsIds(Arrays.asList(ids)); //耦合模式
			jmsTemplate.send(queueSolrDeleteDestination, new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					return session.createObjectMessage(ids);
				}
			});
			//删除每个服务器上的商品详细页
			jmsTemplate.send(topicPageDeleteDestination, new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					return session.createObjectMessage(ids);
				}
			});
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */

	// 商品审核: 更新订单状态
	@RequestMapping("/updateStatus")
	public Result updateStatus(final Long[] ids, String status){
		try {
			goodsService.updateStatus(ids, status);
			
			//审核通过且状态为1
			if("1".equals(status)){
				//获取SPU的ids集合的SKU列表
			//	List<TbItem> itemList = goodsService.findItemListByGoodsIdListAndStatus(ids, status);

				//***导入到solr索引库
				//itemSearchService.importList(itemList); //耦合模式
			//	final String jsonString = JSON.toJSONString(itemList); //转换为json传输
				jmsTemplate.send(queueSolrDestination, new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
			//			return session.createTextMessage(jsonString);
						return session.createObjectMessage(ids);
					}
				});
				
				//***生成商品详细页
				//for(Long goodsId:ids){
				//	boolean bl = itemPageService.genItemHtml(goodsId); //耦合模式
				//}
				for(final Long goodsId:ids){
					jmsTemplate.send(topicPageDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(goodsId+"");
						}
					});
				}
			}
			return new Result(true, "修改状态成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改状态失败");
		}
	}
	
	// 静态化_测试
	@RequestMapping("/genHtml")
	public void genHtml(Long goodsId){
		boolean bl = itemPageService.genItemHtml(goodsId);
		System.out.println(bl);
	}
	
	
	
	//============================= 2.
	// 上下架: 状态更新
	@RequestMapping("/updateMarketable")
	public Result updateMarketable(Long[] ids, String status) {
		try {
			goodsService.updateMarketable(ids, status);
			return new Result(true, "修改状态成功"); 
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改状态失败");
		}
	}
}

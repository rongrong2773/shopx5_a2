package com.shopx5.controller.shop.seller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbSeller;
import com.shopx5.service.common.RegisterService;
import com.shopx5.service.shop.SellerService;

/**
 * Seller控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/seller")
public class SellerController {
	@Autowired
	private SellerService sellerService;
	@Autowired
	private RegisterService registerService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbSeller> findAll() {
		return sellerService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbSeller> findByParentId(Long parentId){
		return sellerService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return sellerService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param seller, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbSeller seller, Integer page, Integer rows) {
		return sellerService.findPage(seller, page, rows);
	}
	
	/**
	 * 增加    #注册
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbSeller seller) {
		try {
			// 校验 用户名与商户ID
			if (registerService.checkData(seller.getSellerId(), 1).equals("false")) {
				return new Result(false, "商户ID已注册!");
			}
			// 校验 店铺名称
			if (StringUtils.isNotBlank(seller.getNickName()) &&
					sellerService.checkData(seller.getNickName(),"nickName").equals("false")) {
				return new Result(false, "店铺名称已注册!");
			}
			sellerService.add(seller);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbSeller findOne(String id) {
		return sellerService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbSeller seller) {
		try {
			sellerService.update(seller);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(String[] ids) {
		try {
			sellerService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	
	//更新状态
	@RequestMapping("/updateStatus")
	public Result updateStatus(String sellerId, String status) {
		try {
			sellerService.updateStatus(sellerId, status);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
}

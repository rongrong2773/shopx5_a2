package com.shopx5.controller.shop.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SxCollection;
import com.shopx5.pojo.group.Good;
import com.shopx5.service.shop.CollectionService;
/**
 * Collection表现层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/shop/index/collection")
public class CollectionController {
	@Autowired
	private CollectionService collectionService;
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("")
	public String findPage(@RequestParam(defaultValue="1") Integer page, HttpServletRequest request, Model model){
		//取用户id
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		PageBean<Good> pageBean = collectionService.findPage(page, spUser.getUserId());
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/shop/user/index_collection";
	}
	/**
	 * 增加
	 * @param SxCollection
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SxCollection sxCollection){
		try {
			collectionService.add(sxCollection);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SxCollection
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SxCollection sxCollection){
		try {
			collectionService.update(sxCollection);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public String findOne(Long id, Model model){
		SxCollection sxCollection = collectionService.getOne(id);
		//数据传递给页面
		model.addAttribute("sxCollection", sxCollection);
		//返回结果
		return "/shop/collection/view";
	}
	/**
	 * 批量删除
	 * @param ids SH OPX5
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(Long [] ids){
		try {
			collectionService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
}

package com.shopx5.controller.shop.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.PageBean;
import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SxView;
import com.shopx5.pojo.group.Good;
import com.shopx5.service.shop.ViewService;
/**
 * View表现层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/shop/index/view")
public class ViewController {
	@Autowired
	private ViewService viewService;
	/**
	 * 返回Page列表
	 * @return
	 */
	@RequestMapping("")
	public String findPage(@RequestParam(defaultValue="1") Integer page, HttpServletRequest request, Model model){
		//取用户id
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		PageBean<Good> pageBean = viewService.findPage(page, spUser.getUserId());
		//数据传递给页面
		model.addAttribute("pageBean", pageBean);
		//返回结果
		return "/shop/user/index_view";
	}
	/**
	 * 增加
	 * @param SxView
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(SxView sxView){
		try {
			viewService.add(sxView);
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SxView
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(SxView sxView){
		try {
			viewService.update(sxView);
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public String findOne(Long id, Model model){
		SxView sxView = viewService.findOne(id);
		//数据传递给页面
		model.addAttribute("sxView", sxView);
		//返回结果
		return "/shop/view/view";
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(Long [] ids){
		try {
			viewService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
}

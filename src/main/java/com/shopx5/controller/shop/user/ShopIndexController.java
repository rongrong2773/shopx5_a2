package com.shopx5.controller.shop.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shopx5.common.OrderParam;
import com.shopx5.common.PageBean;
import com.shopx5.mapper.TbOrderMapper;
import com.shopx5.mapper.result.OrderMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbOrder;
import com.shopx5.pojo.TbOrderExample;
import com.shopx5.pojo.TbOrderExample.Criteria;
import com.shopx5.pojo.result.Order;
/**
 * ShopIndex用户订单中心
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
public class ShopIndexController {
	@Value("${PAGE_SIZE}")
	private String PAGE_SIZE;
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private TbOrderMapper tbOrderMapper;
	
	//用户订单中心
	@RequestMapping("/shop/index/order")
	public String findAllOrderByState(OrderParam orderParam ,HttpServletRequest request, Model model) throws Exception {
		//@RequestParam(defaultValue="1") Integer page, @RequestParam(defaultValue="0") Integer state_type,
        //String query_start_date, String query_end_date, String keyword
		
		//查询订单 根据state 1.未付款，2.已付款，3.未发货，4.已发货，5.交易成功，6.交易关闭
		SpUser user = (SpUser) request.getAttribute("spUser");
		orderParam.setUserId(user.getUserId());
		
		//设置分页信息   页码,数量
		PageBean<Order> pageBean = new PageBean<>();
		PageHelper.startPage(orderParam.getPage(), Integer.parseInt(PAGE_SIZE));
		
		//1.根据state_type 订单状态
		//2.根据query_start_date,query_end_date 订单时间		
		//3.根据keyword 商品标题 采用自建mapper
		List<Order> list = orderMapper.findOrders(orderParam);
		
		//取查询结果
		PageInfo<Order> pageInfo = new PageInfo<>(list);
		//设置数据
		pageBean.setList(list);
		pageBean.setCurrPage(pageInfo.getPageNum());
		pageBean.setPageSize(pageInfo.getPageSize());
		pageBean.setTotalCount(Integer.parseInt(String.valueOf(pageInfo.getTotal())));
		pageBean.setTotalPage(pageInfo.getPages());
		pageBean.setCurrSize(pageInfo.getSize());		
		
		//传递数据
		model.addAttribute("pageBean", pageBean);
		model.addAttribute("orderParam", orderParam);
		return "shop/user/index_order";
	}
	
	//待付款
	@RequestMapping("/shop/index/order_new")
	@ResponseBody
	private Integer getOrderNew(HttpServletRequest request) throws Exception {
		//取用户id
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		//返回数据
		return getSize(spUser.getUserId(),1+"");
	}
	
	//待收货
	@RequestMapping("/shop/index/order_send")
	@ResponseBody
	private Integer getOrderSend(HttpServletRequest request) throws Exception {
		//取用户id
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		//返回数据
		return getSize(spUser.getUserId(),4+"");
	}
	
	//待评价
	@RequestMapping("/shop/index/order_noeval")
	@ResponseBody
	private Integer getOrderNoeval(HttpServletRequest request) throws Exception {
		//取用户id
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		//返回数据
		return getSize(spUser.getUserId(),5+"");
	}
	
	public int getSize(String userId, String status){
		//查询订单  userId,status=1
		TbOrderExample example = new TbOrderExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		criteria.andStatusEqualTo(status);
		List<TbOrder> list = tbOrderMapper.selectByExample(example);
		//返回数据
		return list.size();
	}
}

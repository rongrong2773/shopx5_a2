package com.shopx5.controller.shop.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserAddr;
import com.shopx5.service.shop.UserAddrService;
import com.shopx5.utils.IDUtils;
/**
 * UserAddr表现层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
@RequestMapping("/shop/index/addr")
public class UserAddrController {
	@Autowired
	private UserAddrService userAddrService;
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * 返回用户全部地址列表
	 * @return
	 */
	@RequestMapping("")
	public String findAll(Model model, HttpServletRequest request){
		//取用户
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		//查询全部列表
		List<SpUserAddr> list = userAddrService.findUserAddrByUserId(spUser.getUserId());
		//数据传递给页面
		model.addAttribute("list", list);
		//返回结果
		return "/shop/user/user_address";
	}
	/**
	 * 增加
	 * @param SpUserAddr
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Result add(HttpServletRequest request, SpUserAddr spUserAddr){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			spUserAddr.setAddrId(IDUtils.get32UUID());
			spUserAddr.setUserId(spUser.getUserId());
			userAddrService.add(spUserAddr, spUser.getUserId());
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 * @param SpUserAddr
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Result update(HttpServletRequest request, SpUserAddr spUserAddr){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//更新
			userAddrService.update(spUserAddr, spUser.getUserId());
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/getOne")
	public String getOne(String id, Model model){
		SpUserAddr spUserAddr = userAddrService.getOne(id);
		//数据传递给页面
		model.addAttribute("spUserAddr", spUserAddr);
		//返回结果
		return "/shop/userAddr/view";
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(String [] ids){
		try {
			userAddrService.delete(ids);
			return Result.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "删除失败");
		}
	}
	
	// to更新
	@RequestMapping("/toUpdate")
	@ResponseBody
	public Result toUpdate(String id, Model model){
		SpUserAddr spUserAddr = userAddrService.getOne(id);
		//返回结果
		return Result.ok(spUserAddr);
	}
	
	// 获取登录用户 地址列表
	@RequestMapping("/findListByLoginUser")
	@ResponseBody
	public List<SpUserAddr> findListByLoginUser() {
		// 获取登陆用户
		//取用户
		SpUser spUser = (SpUser) request.getAttribute("spUser");
		return userAddrService.findListByUserId(spUser.getUserId());
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/addJ")
	@ResponseBody
	public Result addJ(HttpServletRequest request, @RequestBody SpUserAddr spUserAddr){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//补充数据
			spUserAddr.setAddrId(IDUtils.get32UUID());
			spUserAddr.setUserId(spUser.getUserId());
			userAddrService.add(spUserAddr, spUser.getUserId());
			return Result.ok("增加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "增加失败");
		}
	}
	/**
	 * 修改
	 */
	@RequestMapping("/updateJ")
	@ResponseBody
	public Result updateJ(HttpServletRequest request, @RequestBody SpUserAddr spUserAddr){
		try {
			//取用户
			SpUser spUser = (SpUser) request.getAttribute("spUser");
			//更新
			userAddrService.update(spUserAddr, spUser.getUserId());
			return Result.ok("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.build(400, "修改失败");
		}
	}
}

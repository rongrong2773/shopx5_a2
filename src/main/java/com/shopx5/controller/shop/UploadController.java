package com.shopx5.controller.shop;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopx5.common.shop.Result;
import com.shopx5.utils.IDUtils;
/**
 * XS 多用户商城
 * @author sjx
 * 2019年6月26日 下午10:41:37
 */
@RestController
@RequestMapping("/shopx5/upload")
public class UploadController {

	@Value("${FILE_SERVER_URL}")
	private String file_server_url;

	@RequestMapping("/uploadFile")
	public Result uploadFile(MultipartFile file, HttpServletRequest request) {
		/*try {
			// 获得文件名:
			String fileName = file.getOriginalFilename();
			// 获得文件的扩展名:
			String extName = fileName.substring(fileName.lastIndexOf(".") + 1);
			// 创建工具类
		//	util.FastDFSClient client = new FastDFSClient("classpath:fastDFS/fdfs_client.conf");
		//	String path = client.uploadFile(file.getBytes(), extName); // group1/M00/
		//	String url = file_server_url + path;
		//	return new Result(true, url);
			return new Result(true, "http://www.xinhuanet.com/photo/2019-01/23/1124028025_15482008643491n.jpg");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "上传失败！");
		}*/
		
		try {
			//重命名
			String fileStr = file.getOriginalFilename();
			String newFileName = IDUtils.genImageName() + fileStr.substring(fileStr.lastIndexOf("."));
			//保存目录
			String path = request.getSession().getServletContext().getRealPath("/images/picture");
			
			System.out.println("文件的保存路径是：\t\t" + path);
			File uploadPic = new File(path + "/" + newFileName);
			if(!uploadPic.exists()) uploadPic.mkdirs();
			// 保存到硬盘
			file.transferTo(uploadPic);
			
			return new Result(true, "/images/picture/"+newFileName);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "上传失败！");
		}
	}
}

package com.shopx5.controller.shop.search;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.service.shop.ItemSearchService;
/**
 * @author ShopX5多商户商城系统
 */
@RestController
@RequestMapping("/shopx5/itemsearch")
public class ItemSearchController {
	@Autowired
	private ItemSearchService itemSearchService;

	@RequestMapping("/search")
	public Map search(@RequestBody Map searchMap) {
		System.out.println("搜索参: "+searchMap.toString());
		return itemSearchService.search(searchMap);
	}
}

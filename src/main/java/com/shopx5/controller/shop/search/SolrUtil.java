package com.shopx5.controller.shop.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.shopx5.common.shop.Result;
import com.shopx5.mapper.TbGoodsDescMapper;
import com.shopx5.mapper.TbGoodsMapper;
import com.shopx5.mapper.TbItemMapper;
import com.shopx5.pojo.TbGoods;
import com.shopx5.pojo.TbGoodsDesc;
import com.shopx5.pojo.TbGoodsExample;
import com.shopx5.pojo.TbGoodsExample.Criteria;
import com.shopx5.pojo.TbItem;
import com.shopx5.pojo.TbItemExample;
import com.shopx5.pojo.group.SolrGoods;
/**
 * @author ShopX5多商户商城系统
 */
@RestController
public class SolrUtil {
	@Autowired
	private TbItemMapper itemMapper;
	@Autowired
	private TbGoodsMapper goodsMapper;
	@Autowired
	private TbGoodsDescMapper goodsDescMapper;
	@Autowired
	private SolrTemplate solrTemplate;
	
	// 导入数据
	@RequestMapping("/shopx5/importItem")
	public Result importItemData() {
		try {
			// 数据
			List<SolrGoods> sgList = new ArrayList<>();
			// 获取goods
			TbGoodsExample example = new TbGoodsExample();
			Criteria criteria = example.createCriteria();
			criteria.andAuditStatusEqualTo("1"); //审核通过导入
			List<TbGoods> goodsList = goodsMapper.selectByExample(example);
			// 执行
			int i = 0;
			for (TbGoods goods : goodsList) {
				// 获取goods内item
				TbItemExample itemExample = new TbItemExample();
				com.shopx5.pojo.TbItemExample.Criteria itemCriteria = itemExample.createCriteria();
				itemCriteria.andGoodsIdEqualTo(goods.getId());
				itemCriteria.andStatusEqualTo("1");
				List<TbItem> itemList = itemMapper.selectByExample(itemExample);
				if(itemList==null) continue;
				// 获取默认item 重新封装spec
				Map<String,String> map = new HashMap<>();
				for (TbItem item : itemList) {
					if(item.getSpec()==null) continue;
					Map<String,String> specMap = JSON.parseObject(item.getSpec(), Map.class);
					for (String k : specMap.keySet()) {
						String v = map.get(k);
						String sv = specMap.get(k);
						if (map.containsKey(k) && v.indexOf(sv)==-1 ) { //两组map有相同key 重组value
							map.put(k, v+","+sv);
						}
						if (!map.containsKey(k)) {
							map.put(k, sv);
						}
					}
				}
				// 获取默认item 封装导入实体
				for (TbItem item : itemList) {
					i++;
					if (item.getIsDefault().equals("1") || itemList.size()==i) { //默认或到最后
						SolrGoods good = new SolrGoods();
						good.setId(item.getId());
						good.setTitle(item.getTitle());
						good.setPrice(item.getPrice());
						//描叙中图片处理
						//获取goods的goodsDesc
						TbGoodsDesc goodsDesc = goodsDescMapper.selectByPrimaryKey(goods.getId());
						//大图封装
						//good.setImage(goodsDesc.getItemImages());
						List<Map> imagesList = new ArrayList<>();
						String itemImages = goodsDesc.getItemImages();
						if (itemImages!=null) {
							List<Map> iList = JSON.parseArray(itemImages, Map.class);
							for (Map iMap : iList) {
								String color = (String) iMap.get("color");
								String url = (String) iMap.get("url");
								if(color.contains("大图")){
									Map m = new HashMap<>();
									m.put("color", color);
									m.put("url", url);
									imagesList.add(m);
								}
							}
						}
						String images = JSON.toJSONString(imagesList);
						good.setImage(images);
						good.setCategory(item.getCategory());
						good.setBrand(item.getBrand());
						good.setGoodsId(item.getGoodsId());
						good.setSeller(item.getSeller());
						good.setUpdateTime(item.getUpdateTime());
						//动态域处理
						//Map specMap = JSON.parseObject(item.getSpec(), Map.class);
						//good.setSpecMap(specMap);
						good.setSpecMap(map);
						sgList.add(good);
						i = 0;
						break;
					}
				}
			}
			// 执行
			solrTemplate.saveBeans(sgList);
			solrTemplate.commit();
			return new Result(true, "导入成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "导入失败!");
		}
	}
	
	/**
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext*.xml");
		SolrUtil solrUtil = (SolrUtil) context.getBean("solrUtil");
		solrUtil.importItemData();
	}
	*/
	
	/**		
	TbItem 数据导入
	try {
		TbItemExample example = new TbItemExample();
		Criteria criteria = example.createCriteria();
		criteria.andStatusEqualTo("1"); //审核通过导入
		List<TbItem> itemList = itemMapper.selectByExample(example);
		// 动态域处理
		for (TbItem item : itemList) {
			Map specMap = JSON.parseObject(item.getSpec(), Map.class); //规格为json字符串
			item.setSpecMap(specMap);
		}
		// 执行
		solrTemplate.saveBeans(itemList);
		solrTemplate.commit();
		return new Result(true, "导入成功!");
	} catch (Exception e) {
		e.printStackTrace();
		return new Result(false, "导入失败!");
	}
	*/
}

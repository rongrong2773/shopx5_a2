package com.shopx5.controller.shop.comment;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbComment;
import com.shopx5.pojo.group.Comment;
import com.shopx5.service.shop.CommentService;
/**
 * Comment控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/comment")
public class CommentController {
	@Autowired
	private CommentService commentService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbComment> findAll() {
		return commentService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbComment> findByParentId(Long parentId){
		return commentService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return commentService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param comment, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbComment comment, Integer page, Integer rows) {
		return commentService.findPage(comment, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbComment comment) {
		try {
			comment.setGoodsId(102531761813510L);
			comment.setUserId("用户shopx5");
			comment.setSellerId("shopx5");
			comment.setCreated(new Date());
			comment.setUpdated(new Date());
			comment.setStatus("1");
			commentService.add(comment);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbComment findOne(Long id) {
		return commentService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbComment comment) {
		try {
			commentService.update(comment);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			commentService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 获取SPU评论数据  根据goodsId
	@RequestMapping("/findByGoodsId")
	public List<Comment> findByGoodsId(String goodsId) {
		return commentService.findByGoodsId(Long.valueOf(goodsId));
	}
}

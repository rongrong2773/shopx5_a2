package com.shopx5.controller.shop.seckill;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbSeckillGoods;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.SeckillGoodsService;

/**
 * SeckillGoods控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/seckill/goods")
public class SeckillGoodsController {
	@Autowired
	private SeckillGoodsService seckillGoodsService;
	@Autowired
	private IndexService indexService;
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbSeckillGoods> findAll() {
		return seckillGoodsService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbSeckillGoods> findByParentId(Long parentId){
		return seckillGoodsService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return seckillGoodsService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param seckillGoods, page, rows, status级别
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbSeckillGoods seckillGoods, Integer page, Integer rows, Integer grade) {
		return seckillGoodsService.findPage(seckillGoods, page, rows, grade);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbSeckillGoods seckillGoods) {
		try {
			// 获取user
			SpUser user = indexService.getUser(request);
			if (user==null) return new Result(false, "保存失败!");
			// 设置sellerId
			seckillGoods.setSellerId(user.getSellerId());
			// 设置时间
			seckillGoods.setCreateTime(new Date());
			seckillGoodsService.add(seckillGoods);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbSeckillGoods findOne(Long id) {
		return seckillGoodsService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbSeckillGoods seckillGoods) {
		try {
			seckillGoodsService.update(seckillGoods);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			seckillGoodsService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	// 返回正在参与秒杀的商品
	@RequestMapping("/findList")
	public List<TbSeckillGoods> findList(){
		return seckillGoodsService.findList();
	}
	
	// 根据ID获取实体(从缓存中读取)
	@RequestMapping("/findOneFromRedis")
	public TbSeckillGoods findOneFromRedis(Long id){
		return seckillGoodsService.findOneFromRedis(id);		
	}
}

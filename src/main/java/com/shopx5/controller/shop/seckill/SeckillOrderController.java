package com.shopx5.controller.shop.seckill;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.TbSeckillGoods;
import com.shopx5.pojo.TbSeckillOrder;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.SeckillOrderService;

/**
 * SeckillOrder控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/seckill/order")
public class SeckillOrderController {
	@Autowired
	private SeckillOrderService seckillOrderService;
	@Autowired
	private IndexService indexService; 
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbSeckillOrder> findAll() {
		return seckillOrderService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<TbSeckillOrder> findByParentId(Long parentId){
		return seckillOrderService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return seckillOrderService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param seckillOrder, page, rows, status级别
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbSeckillOrder seckillOrder, Integer page, Integer rows, Integer grade) {
		return seckillOrderService.findPage(seckillOrder, page, rows, grade);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbSeckillOrder seckillOrder) {
		try {
			seckillOrderService.add(seckillOrder);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbSeckillOrder findOne(Long id) {
		return seckillOrderService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbSeckillOrder seckillOrder) {
		try {
			seckillOrderService.update(seckillOrder);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			seckillOrderService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	
	// 秒杀下单
	@RequestMapping("/submitOrder")
	public Result submitOrder(Long seckillId){
		//提取当前用户
//		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		SpUser user = indexService.getUser(request);
//		if("anonymousUser".equals(username)){
		if (user == null) {
			return new Result(false, "当前用户未登录");
		}
		try {
			seckillOrderService.submitOrder(seckillId, user.getUserId());
			return new Result(true, "提交订单成功");
		}catch (RuntimeException e) {
			e.printStackTrace();
			return new Result(false, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "提交订单失败");
		}
	}
	
	// 根据orderId 获取订单项
	@RequestMapping("/findItem")
	public TbSeckillGoods findItem(Long orderId) {
		return seckillOrderService.findItem(orderId);
	}
	// 更新状态
	@RequestMapping("/updateStatus")
	public Result updateStatus(Long orderId, String status) {
		try {
			seckillOrderService.updateStatus(orderId, status);
			return new Result(true, "设置成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "设置失败!");
		}
	}
}

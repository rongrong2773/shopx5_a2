package com.shopx5.controller.shop.seckill;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.Result;
import com.shopx5.pojo.TbSeckillOrder;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.SeckillOrderService;
import com.shopx5.service.shop.WeixinPayService;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/pay2")
public class PaySeckillController {
	@Autowired
	private WeixinPayService weixinPayService;
	@Autowired
	private SeckillOrderService seckillOrderService;
	@Autowired
	private IndexService indexService;
	@Autowired
	private HttpServletRequest request;
	
	// 创建二维码
	@RequestMapping("/createNative")
	public Map createNative(){
		//1.获取当前登录用户
//		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		String userId = indexService.getUser(request).getUserId();
		//2.提取秒杀订单（从缓存 ）
		TbSeckillOrder seckillOrder = seckillOrderService.searchOrderFromRedisByUserId(userId);
		//3.调用微信支付接口
		if(seckillOrder!=null){
			return weixinPayService.createNative(seckillOrder.getId()+"", (long)(seckillOrder.getMoney().doubleValue()*100)+"");	
		}else{
			return new HashMap<>();
		}
	}
	
	// 查询状态
	@RequestMapping("/queryPayStatus")
	public Result queryPayStatus(String out_trade_no) throws Exception{
	  //测试:停顿会进入
		Thread.sleep(6000);
		
		//1.获取当前登录用户
//		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		String username = "用户shopx5";
		Result result=null;
		int x=0;
		while(true){
			Map<String,String> map = weixinPayService.queryPayStatus(out_trade_no);//调用查询
			if(map==null){
				result=new Result(false, "支付发生错误");
				break;
			}
			if(map.get("trade_state").equals("SUCCESS")){//支付成功
				result=new Result(true, "支付成功");
				//保存订单
				seckillOrderService.saveOrderFromRedisToDb(username, Long.valueOf(out_trade_no), map.get("transaction_id"));
				break;
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			x++;
			if(x>=100){
				result=new Result(false, "二维码超时");
				// 关闭支付
				Map<String,String> payResult = weixinPayService.closePay(out_trade_no);
				if(payResult!=null && "FAIL".equals(payResult.get("return_code"))){
					if("ORDERPAID".equals(payResult.get("err_code"))){
						result=new Result(true, "支付成功");
						//保存订单
						seckillOrderService.saveOrderFromRedisToDb(username, Long.valueOf(out_trade_no), map.get("transaction_id"));
					}
				}
				//删除订单
				if(result.isSuccess()==false){
					seckillOrderService.deleteOrderFromRedis(username, Long.valueOf(out_trade_no));
				}
				break;
			}
		}
		return result;
	}
}

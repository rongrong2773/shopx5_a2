package com.shopx5.controller.shop;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SsTemplateType;
import com.shopx5.service.shop.TemplateTypeService;

/**
 * TemplateType控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/templateType")
public class TemplateTypeController {
	@Autowired
	private TemplateTypeService templateTypeService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<SsTemplateType> findAll() {
		return templateTypeService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<SsTemplateType> findByParentId(Long parentId){
		return templateTypeService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return templateTypeService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param templateType, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody SsTemplateType templateType, Integer page, Integer rows) {
		return templateTypeService.findPage(templateType, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody SsTemplateType templateType) {
		try {
			templateTypeService.add(templateType);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public SsTemplateType findOne(Long id) {
		return templateTypeService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody SsTemplateType templateType) {
		try {
			templateTypeService.update(templateType);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			templateTypeService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	@RequestMapping("/selectOptionList")
	public List<Map> selectOptionList() {
		return templateTypeService.selectOptionList();
	}
	
	// 根据模板ID,获得规格封装选项
	@RequestMapping("/findBySpecList")
	public List<Map> findSpecList(Long id){
		return templateTypeService.findSpecList(id);
	}
}

package com.shopx5.controller.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SsItemCat;
import com.shopx5.service.shop.ItemCatService;

/**
 * ItemCat控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/itemCat2")
public class ItemCatController {
	@Autowired
	private ItemCatService itemCatService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<SsItemCat> findAll() {
		return itemCatService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<SsItemCat> findByParentId(Long parentId){
		return itemCatService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return itemCatService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param itemCat, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody SsItemCat itemCat, Integer page, Integer rows) {
		return itemCatService.findPage(itemCat, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody SsItemCat itemCat) {
		try {
			itemCatService.add(itemCat);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public SsItemCat findOne(Long id) {
		return itemCatService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody SsItemCat itemCat) {
		try {
			itemCatService.update(itemCat);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			itemCatService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	
	//搜索前 缓存分类   manager模块
	@RequestMapping("/cacheCat")
	public Result cacheCat() {
		try {
			itemCatService.cacheCat();
			return new Result(true, "缓存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "缓存失败!");
		}
	}
}

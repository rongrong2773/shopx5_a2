package com.shopx5.controller.shop;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopx5.common.shop.PageResult;
import com.shopx5.common.shop.Result;
import com.shopx5.pojo.SsBrandCat;
import com.shopx5.service.shop.BrandCatService;

/**
 * BrandCat控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/brandCat")
public class BrandCatController {
	@Autowired
	private BrandCatService brandCatService;
	
	/**
	 * 返回全部
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<SsBrandCat> findAll() {
		return brandCatService.findAll();
	}
	
	/**
	 * 根据父ID查询
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/findByParentId")
	public List<SsBrandCat> findByParentId(Long parentId){
		return brandCatService.findByParentId(parentId);
	}
	
	/**
	 * 返回分页
	 * @param page, rows
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(Integer page, Integer rows) {
		return brandCatService.findPage(page, rows);
	}
	
	/**
	 * 返回分页: 带条件
	 * @param brandCat, page, rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody SsBrandCat brandCat, Integer page, Integer rows) {
		return brandCatService.findPage(brandCat, page, rows);
	}
	
	/**
	 * 增加
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody SsBrandCat brandCat) {
		try {
			brandCatService.add(brandCat);
			return new Result(true, "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "保存失败!");
		}
	}
	
	/**
	 * 根据id获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public SsBrandCat findOne(Long id) {
		return brandCatService.findOne(id);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody SsBrandCat brandCat) {
		try {
			brandCatService.update(brandCat);
			return new Result(true, "修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败!");
		}
	}
	
	/**
	 * 批量删除
	 * @param ids
	 */
	@RequestMapping("/delete")
	public Result delete(Long[] ids) {
		try {
			brandCatService.delete(ids);
			return new Result(true, "删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败!");
		}
	}
	
	/**
	 * 自定义
	 */
	@RequestMapping("/selectOptionList")
	public List<Map> selectOptionList() {
		return brandCatService.selectOptionList();
	}
}

package com.shopx5.controller.shop;

import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.shopx5.pojo.TbGoods;
import com.shopx5.service.shop.GoodsService;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * Item控制层
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@RestController
@RequestMapping("/shopx5/show")
public class ShowController {
	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	private Destination	topicPageDestination;
	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;
	@Value("${pagedir}")
	private String pagedir;
	@Value("${indexdir}")
	private String indexdir;
	@Autowired
	private GoodsService goodsService;
	
	//1.生成相关静态                           /shopx5/show/index?type=index
	@RequestMapping("/index")
	public String showIndex(String type){
		Configuration configuration = freeMarkerConfigurer.getConfiguration();
		try {
			Template template = configuration.getTemplate(type+".ftl");
			// 创建数据模型
			Map map = new HashMap<>();
			// 封装数据
			String dir = "";
			if (type.equals("index")) {
				dir = indexdir;
			}else{
				dir = pagedir;
			}
			Writer out = new FileWriter(dir+type+".htm");
			template.process(map, out);
			out.close();
			return "ok! ==> /shop/"+type+".htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "no!";
		}
	}
	@RequestMapping("/all")
	public String showAll(){
		Configuration configuration = freeMarkerConfigurer.getConfiguration();
		try {
			String[] arr = {"index","search","cart","confirm_order","confirm_pay","pay","payfail","paysuccess"};
			for (String str : arr) {
				Template template = configuration.getTemplate(str+".ftl");
				// 创建数据模型
				Map map = new HashMap<>();
				// 封装数据
				String dir = "";
				if (str.equals("index")) {
					dir = indexdir;
				}else{
					dir = pagedir;
				}
				Writer out = new FileWriter(dir+str+".htm");
				template.process(map, out);
				out.close();
			}
			return "ok! ==> showAll";
		} catch (Exception e) {
			e.printStackTrace();
			return "no!";
		}
	}
	
	//2.生成商品静态
	@RequestMapping("/ftl2")
	public String showGoods(final String goodsId){
		if (goodsId.equals("all")) {
			List<TbGoods> list = goodsService.findAll();
			for (final TbGoods goods : list) {
				jmsTemplate.send(topicPageDestination, new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						return session.createTextMessage(goods.getId()+"");
					}
				});
			}
			return "ok! ==> /shop/goodsId.htm";
		}
		// 单独生成
		jmsTemplate.send(topicPageDestination, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(goodsId);
			}
		});
		return "ok! ==> /shop/"+goodsId+".htm";
	}
}

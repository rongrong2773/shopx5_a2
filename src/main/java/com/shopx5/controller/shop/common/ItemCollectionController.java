package com.shopx5.controller.shop.common;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SxCollection;
import com.shopx5.service.shop.ItemCollectionService;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
public class ItemCollectionController {
	@Autowired
	private ItemCollectionService collectionService;
	
	//1 判断是否已收藏+收藏次数
	@RequestMapping("/collect/index")
	@ResponseBody
	public Result indexCollect(String goodsId, HttpServletRequest request){		
		SpUser user = (SpUser) request.getAttribute("spUser");
		if (user == null) {
			return Result.build(400, "未登录!");
		}
		//判断是否已收藏+收藏次数
		return collectionService.indexCollect(goodsId, user.getUserId());
	}
	
	//2 添加收藏
	@RequestMapping("/collect/add")
	@ResponseBody
	public Result addCollect(SxCollection collection, String goodsId, HttpServletRequest request){
		SpUser user = (SpUser) request.getAttribute("spUser");
		collection.setUserId(user.getUserId());
		return collectionService.addCollect(collection, goodsId);
	}
	
	//3 删除收藏
	@RequestMapping("/collect/del")
	@ResponseBody
	public Result deleteCollect(String goodsId, HttpServletRequest request){
		SpUser user = (SpUser) request.getAttribute("spUser");
		return collectionService.deleteCollect(goodsId, user.getUserId());
	}	
}

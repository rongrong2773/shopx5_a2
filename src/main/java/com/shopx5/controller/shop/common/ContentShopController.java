package com.shopx5.controller.shop.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.EasyUITreeNode;
import com.shopx5.common.Result;
import com.shopx5.mapper.SxContentCategoryMapper;
import com.shopx5.mapper.SxContentMapper;
import com.shopx5.pojo.SxContent;
import com.shopx5.pojo.SxContentCategory;
import com.shopx5.pojo.SxContentCategoryExample;
import com.shopx5.pojo.SxContentCategoryExample.Criteria;
import com.shopx5.pojo.SxContentExample;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
public class ContentShopController {
	@Autowired
	private SxContentCategoryMapper contentCategoryMapper;
	@Autowired
	private	SxContentMapper contentMapper;
	
	// 广告分类列表
	@RequestMapping("/content/category/list")
	@ResponseBody                              // 默认加载取父id=0  tree选中取父id=id ?id=30
	public List<EasyUITreeNode> findContentCatList(@RequestParam(name="id", defaultValue="0") Long parentId){
		//根据parentId查询子节点列表
		SxContentCategoryExample example = new SxContentCategoryExample();
		//设置查询条件
		Criteria criteria = example.createCriteria();
		//设置parentid
		criteria.andParentIdEqualTo(parentId);
		//执行查询
		List<SxContentCategory> list = contentCategoryMapper.selectByExample(example);
		//转换成EasyUITreeNode列表
		List<EasyUITreeNode> resultList = new ArrayList<>();
		for (SxContentCategory tbContentCategory : list) {
			EasyUITreeNode node = new EasyUITreeNode();
			node.setId(tbContentCategory.getId());
			node.setText(tbContentCategory.getName());
			//如果节点下有子节点“closed”，如果没有子节点“open”
			//逆向工程会将getIsParent()值0和1转成boolean型
			node.setState(tbContentCategory.getIsParent() ? "closed" : "open");  
			//添加到结果列表
			resultList.add(node);
		}
		return resultList;
	}
	
	// 增加分类	当前parentId下新增子类
	@RequestMapping("/content/category/create")
	public Result addContentCategory(Long parentId, String name){
		//创建一个pojo对象
		SxContentCategory contentCategory = new SxContentCategory();
		//补全对象的属性
		contentCategory.setParentId(parentId);
		contentCategory.setName(name);
		//状态。可选值:1(正常),2(删除)
		contentCategory.setStatus(1);
		//排序，默认为1
		contentCategory.setSortOrder(1);
		contentCategory.setIsParent(false);
		contentCategory.setCreated(new Date());
		contentCategory.setUpdated(new Date());
		//插入到数据库
		contentCategoryMapper.insert(contentCategory);
		//判断父节点的状态
		SxContentCategory parent = contentCategoryMapper.selectByPrimaryKey(parentId);
		if (!parent.getIsParent()) {
			//如果父节点为叶子节点应该改为父节点
			parent.setIsParent(true);
			//更新父节点
			contentCategoryMapper.updateByPrimaryKey(parent);
		}			
		//返回结果
		return Result.ok(contentCategory);
	}
	
	// 新增广告数据
	@RequestMapping(value="/content/save", method=RequestMethod.POST)
	@ResponseBody
	public Result addContent(SxContent content){
		System.out.println(content);
		//补全pojo的属性
		content.setCreated(new Date());
		content.setUpdated(new Date());
		//插入到内容表
		contentMapper.insert(content);
		return Result.ok();
	}
	
	// 根据分类ID获取广告列表
	@RequestMapping(value="/content/findByCategoryId", method=RequestMethod.POST)
	@ResponseBody
	public Result findContentList(String categoryId){
		SxContentExample example = new SxContentExample();
		com.shopx5.pojo.SxContentExample.Criteria criteria = example.createCriteria();
		criteria.andCategoryIdEqualTo(Long.valueOf(categoryId));
		List<SxContent> list = contentMapper.selectByExample(example);
		return Result.ok(list);
	}
}

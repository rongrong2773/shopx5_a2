package com.shopx5.controller.shop.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.Result;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SxView;
import com.shopx5.pojo.group.Good;
import com.shopx5.service.common.IndexService;
import com.shopx5.service.shop.GoodsService;
import com.shopx5.service.shop.ItemViewService;
import com.shopx5.utils.CookieUtils;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 * 足迹
 */
@Controller
public class ItemViewController {
	@Autowired
	private IndexService indexService;
	@Autowired
	private ItemViewService viewService;
	@Autowired
	private GoodsService goodsService;
	
	// 获取商品浏览历史 限制5个
	@RequestMapping("/view/ajax_load")
	@ResponseBody
	public Result findViews(HttpServletRequest request){
		SpUser user = indexService.getUser(request);
		List<Good> itemList = new ArrayList<>();
		//1.未登录,采用cookie记录的数据
		Cookie c = null;
		if (user == null) {
			c = CookieUtils.getCookieByName("viewed", request.getCookies());
			if (c == null) {
				return Result.build(400, "未登录!");
			}
			String[] arr = c.getValue().split("-"); //ids = 1-2-3
			for (String goodsId : arr) {
				Good good = goodsService.getGood(Long.valueOf(goodsId));
				if(good != null){
					itemList.add(good);
				}
			}
			return Result.ok(itemList);
		}
		
		//2.已登录,新增已经同步cookie中记录到sql
		List<SxView> viewlist = viewService.findViews(user.getUserId());
		//获取商品数据
		int n = 0;
		for (SxView view : viewlist) {
			Good good = goodsService.getGood(view.getItemId());
			if(good != null){
				itemList.add(good);
				n++;
			}
			if (n > 7) break; //限制7条
		}
		//返回数据
		return Result.ok(itemList);
	}
		
	// 添加商品浏览历史
	@RequestMapping("/view/add")
	@ResponseBody
	public String addView(String goodsId, HttpServletRequest request, HttpServletResponse response){
		SpUser user = indexService.getUser(request);		
		//1.未登录,采用cookie添加数据
		String ids = "";
		Cookie c = null;
		if (user == null) {
			c = CookieUtils.getCookieByName("viewed", request.getCookies());
			//a.判断cookie是否为空
			if(c == null){
				ids = goodsId; //若cookie为空  需要将当前商品id放入ids中
			}else{            //若cookie非空  判断ids中是否已有该id  ids = 1-2-3
				ids = c.getValue();
				String[] arr = ids.split("-");
				List<String> asList = Arrays.asList(arr);
				LinkedList<String> list = new LinkedList<>(asList); //转有序集合
				//若ids中包含id 移除放首位
				if(list.contains(goodsId)){	 //足迹中包含, 20以内移除加新, 20以外追加(不移除), 淘宝足迹方式
					if(list.size() < 20){
						list.remove(goodsId);
						list.addFirst(goodsId);
					}else{
						list.addFirst(goodsId);
					}
				}else{
					//若ids中不包含id 限制长度为30条
					if(list.size() > 30){
						list.removeLast();
						list.addFirst(goodsId);
					}else{
						list.addFirst(goodsId);
					}
				}
				//将list转成cookie存储格式
				ids = "";
				for (String s : list) {
					ids += (s + "-");
				}
				ids = ids.substring(0, ids.length()-1);
			}
			//b.重新写入cookie到浏览器
			CookieUtils.setCookie(request, response, "viewed", ids, 86400);
			return null;
		}
		
		//2.暂定为:登录后访问页面,新增记录时添加---判断cookie中是否有记录  , 非订单,登录时就同步到server
		//a.先判断cookie是否存在数据,先合并
		c = CookieUtils.getCookieByName("viewed", request.getCookies());
		if(c != null){
			String[] arr = c.getValue().split("-");
			for (int i = arr.length - 1; i > -1; i--) { //采用倒数保存到数据库中
				// 校验数据库中是否存在
				List<SxView> viewlist = viewService.findViews(user.getUserId());
				List<Long> list = new ArrayList(); //用来只封装20条做对比
				int n = 0;
				for (SxView view : viewlist) {
					list.add(view.getItemId());
					n++;
					if (n > 19) break;
				}
				if(!list.contains(Long.valueOf(arr[i]))){
					SxView view = new SxView();
					view.setUserId(user.getUserId());
					view.setItemId(Long.valueOf(arr[i]));
					viewService.addView(view); //添加数据
				}
			}
			//注意:删除cookie
			CookieUtils.deleteCookie(request, response, "viewed", 0);
		}
		
		//获取数据库中前20条数据对比处理后, 保存 ***
		List<SxView> viewlist = viewService.findViews(user.getUserId());
		Map<String,SxView> map = new HashMap<>(); //用来只封装20条做对比
		int n = 0;
		for (SxView view : viewlist) {
			map.put(String.valueOf(view.getItemId()),view);
			n++;
			if (n > 19) break;
		}
		if(map.containsKey(goodsId)){
			viewService.delView(map.get(goodsId)); //足迹中包含, 20以内移除加新, 20以外追加(不移除)
			this.addOne(goodsId, user);
		}else{
			this.addOne(goodsId, user);
		}
		return null;
	}
	
	//保存浏览记录
	public void addOne(String goodsId, SpUser user){
		SxView view = new SxView();
		view.setItemId(Long.valueOf(goodsId));
		view.setUserId(user.getUserId());
		viewService.addView(view); //添加数据
	}
}

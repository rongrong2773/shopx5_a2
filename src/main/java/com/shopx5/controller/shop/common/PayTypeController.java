package com.shopx5.controller.shop.common;

import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.shop.ConfirmPay;
import com.shopx5.mapper.TbOrderMapper;
import com.shopx5.pojo.TbOrder;
import com.shopx5.pojo.TbPayLog;
import com.shopx5.service.shop.OrderService;
import com.shopx5.service.shop.PayLogService;
import com.shopx5.utils.alipay.HttpsRequest;
import com.shopx5.utils.alipay.Sign;
import com.shopx5.utils.chinabank.PaymentUtil;
/**
 * @author tang ShopX5多商户商城系统
 * @qq 1503501970
 */
@Controller
public class PayTypeController {
	@Autowired
	private TbOrderMapper orderMapper;	
	@Autowired
	private OrderService orderService;
	@Autowired
	private PayLogService payLogService;
	/**
	 * 3种支付方式
	 */	
	@RequestMapping(value="/orderSku/payType", method=RequestMethod.POST)
	public String createOrder(ConfirmPay confirmPay) {
		//支付类型
		String paymentCode = confirmPay.getPaymentCode();
		//支付宝支付
		if (paymentCode.equals("alipay")) {
			return "forward:/orderSku/alipay";
		}		
		//微信支付
		if (paymentCode.equals("wxpay")) {
			if(StringUtils.isNotBlank(confirmPay.getOutTradeNo())){
				return "redirect:/shop/pay.htm#?outTradeNo="+confirmPay.getOutTradeNo();
			}else{
				return "redirect:/shop/pay.htm#?orderId="+confirmPay.getOrderId();
			}
		}		
		//银行支付
		if (paymentCode.equals("chinabank")) {
			return "forward:/orderSku/chinabank";
		}		
		return null;
	}
	
	//支付宝支付1  请求网址,生成支付请求数据
	@RequestMapping(value="/orderSku/alipay")
	public String alipayPay(ConfirmPay confirmPay, Model model) {
		//获取订单 实付金额
		double payment = getPayment(confirmPay);
		
		//1. 请求参数
		String partner = "2088911395487764";                        //PID 查看后台
		String seller_id = partner;                                 //PID 查看后台
		String key = "vuq3ivaaln732a5vuz0oio7iyza9hvvk";            //MD5密钥 查看后台
		String notify_url = "http://bruser3.vicp.hk/pay/yiBu";      //异步通知
		String return_url = "http://bruser3.vicp.hk/pay/tongBu";    //同步通知
		String sign_type = "MD5";                                   //签名方式                剔除 生成MD5签名时,
		String _input_charset ="UTF-8";                             //商户网站编码
		String payment_type = "1";                                  //支付类型
		String service = "create_direct_pay_by_user";               //即时到账交易接口
		String subject = "测试123";                                 //商品标题                 表单提交post时,
/*		String subject = null;
	    try {
			subject = URLEncoder.encode("测试123", "utf-8");        //此用于get提交,用post表单提交不用这样
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}*/
		double total_fee = 0.01;                                   //交易金额
		String out_trade_no = confirmPay.getOrderId();             //订单号 生成随机码
		//2. 拼接TreeMap  默认:key升序排序
		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
		parameters.put("partner", partner);
		parameters.put("seller_id", seller_id);
		parameters.put("notify_url", notify_url);
		parameters.put("return_url", return_url);
		parameters.put("_input_charset", _input_charset);
		parameters.put("payment_type", payment_type);
		parameters.put("service", service);
		parameters.put("subject", subject);
		parameters.put("total_fee", total_fee);
		parameters.put("out_trade_no", out_trade_no);
		//3. 生成MD5签名  ★剔除sign与sign_type参数
		String sign = Sign.creatSign(parameters, key);
		System.out.println("sign: "+sign);
		//发送给第三方
		StringBuffer sb = new StringBuffer("https://mapi.alipay.com/gateway.do?");
		sb.append("partner=").append(partner).append("&");
		sb.append("seller_id=").append(seller_id).append("&");
		sb.append("notify_url=").append(notify_url).append("&");
		sb.append("return_url=").append(return_url).append("&");
		sb.append("payment_type=").append(payment_type).append("&");
		sb.append("service=").append(service).append("&");
		sb.append("subject=").append(subject).append("&");
		sb.append("total_fee=").append(total_fee).append("&");
		sb.append("out_trade_no=").append(out_trade_no).append("&");
		sb.append("_input_charset=").append(_input_charset).append("&");
		sb.append("sign_type=").append("MD5").append("&");
		sb.append("sign=").append(sign);
		// 请求转发第三方
		return "redirect:" + sb.toString();
	}
	//同步通知回调, GET方式获取/1分钟超时          http://商户自定义地址?is_success=T&sign=b1af584504b8e845ebe40b8e0e733729&sign_type=MD5&..
	@RequestMapping("/pay/tongBu")
	public String tongBu(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		//编码 取中文时
		//request.setCharacterEncoding("utf-8");
		//response.setCharacterEncoding("utf-8");
		String is_success = request.getParameter("is_success");
		String sign = request.getParameter("sign");
		System.out.println("is_success="+is_success+", sign="+sign);
		
		model.addAttribute("is_success", "T");
		model.addAttribute("sign", "56s5df9sd592sd6f55dsf");
		return "pay";
	}
	//异步通知回调, POST方式获取/无时间限制
	@RequestMapping("/pay/yiBu")
	@ResponseBody                                                               //注意: 异步不能throws抛,只能try
	public String yiBu(HttpServletRequest request, HttpServletResponse response, Model model) {
		//编码 取中文时 由于异步系统不能抛出异常,使用try★
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String notify_time = request.getParameter("notyfy_time");
		String notify_id = request.getParameter("notyfy_id");		System.out.println("notyfy_id="+notify_id+", notyfy_time="+notify_time);
		String out_trade_no = request.getParameter("out_trade_no"); //订单号id
		
		if (notify_id == null) notify_id = "notify_id";
		//读取页面反馈html代码 true,false
		String url = "https://mapi.alipay.com/gateway.do?service=notify_verify&partner=PARTNER&notify_id=NOTIFY_ID";
		String partner = "2088911395487764";
		url = url.replaceAll("PARTNER", partner).replaceAll("NOTIFY_ID", notify_id); //防止notify_id空指针		
		String flag = null;
		try {
			flag = HttpsRequest.httpsRequest(url, "GET", null);  //"success"在下面回复
			
		//2方式 String flag = HttpClientUtil.getInstance().sendHttpsGet(url);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("flag: "+flag);
		//修改订单状态为2
		if (flag.equals("true")) {
			orderService.updateOrderStatus(out_trade_no, "alipay"); //交易流水号
			// 修改订单状态
			//orderService.updateOrderStatus(out_trade_no, "alipay"); //交易号码 Order2Service
		}else{
			return null;
		}
		                   //★重点:如果是true是支付宝发来的,false就是黑客发过来的
						   //★然后:数据库_修改订单号支付状态_整个支付流程完成
		return "success";  //★注意:必须在return "success"回复支付宝成功之前验证, 若已经回复了支付宝,notify_id就失效了,无法验证
		
		 //补充:  防止黑客,可再验证 out_trade_no            订单号
         //                      total_fee               金额
		 //                      seller_id(seller_email) PID		    
	}
	
	//微信支付2
	
	//银行支付3       支付流程 OK
	@RequestMapping(value="/orderSku/chinabank")
	public String chinabankPay(ConfirmPay confirmPay, String pd_FrpId, Model model) {
		//获取订单 实付金额
		double payment = getPayment(confirmPay);
		// 组织发送支付公司需要哪些数据
		//String pd_FrpId = ;                                                              //1. 获取 银行pd_FrpId
		String p0_Cmd = "Buy";
		String p1_MerId = ResourceBundle.getBundle("merchantInfo").getString("p1_MerId");  //2. 获取 p1_MerId
		String p2_Order = confirmPay.getOrderId();                                         //3. 设置 订单id
		String p3_Amt = "0.01";                                                            //4. 支付 金额
		String p4_Cur = "CNY";
		String p5_Pid = "";
		String p6_Pcat = "";
		String p7_Pdesc = "";
		// 支付成功回调地址 ---- 第三方支付公司会访问、用户访问
		// 第三方支付可以访问网址
		String p8_Url = ResourceBundle.getBundle("merchantInfo").getString("responseURL"); //5. 获取 回调地址 ###
		String p9_SAF = "";
		String pa_MP = "";
		String pr_NeedResponse = "1";
		// 加密hmac 需要密钥
		String keyValue = ResourceBundle.getBundle("merchantInfo").getString("keyValue");  //6. 获取 密钥
		// 使用工具类生成加密
		String hmac = PaymentUtil.buildHmac(p0_Cmd, p1_MerId, p2_Order, p3_Amt, p4_Cur, p5_Pid,
				 p6_Pcat, p7_Pdesc, p8_Url, p9_SAF, pa_MP, pd_FrpId, pr_NeedResponse, keyValue);			
		//发送给第三方
		StringBuffer sb = new StringBuffer("https://www.yeepay.com/app-merchant-proxy/node?");
		sb.append("p0_Cmd=").append(p0_Cmd).append("&");
		sb.append("p1_MerId=").append(p1_MerId).append("&");
		sb.append("p2_Order=").append(p2_Order).append("&");
		sb.append("p3_Amt=").append(p3_Amt).append("&");
		sb.append("p4_Cur=").append(p4_Cur).append("&");
		sb.append("p5_Pid=").append(p5_Pid).append("&");
		sb.append("p6_Pcat=").append(p6_Pcat).append("&");
		sb.append("p7_Pdesc=").append(p7_Pdesc).append("&");
		sb.append("p8_Url=").append(p8_Url).append("&");
		sb.append("p9_SAF=").append(p9_SAF).append("&");
		sb.append("pa_MP=").append(pa_MP).append("&");
		sb.append("pd_FrpId=").append(pd_FrpId).append("&");
		sb.append("pr_NeedResponse=").append(pr_NeedResponse).append("&");
		sb.append("hmac=").append(hmac);
		// 请求转发第三方
		return "redirect:" + sb.toString();
	}
	/**
	 * 支付成功之后的回调
	 * @throws Exception 
	 */	
	@RequestMapping(value="/orderSku/pay/callback")
	public String callback(String p1_MerId,
                           String r0_Cmd,
                           String r1_Code,
                           String r2_TrxId,
                           String r3_Amt,
                           String r4_Cur,
                           String r5_Pid,
                           String r6_Order,
                           String r7_Uid,
                           String r8_MP,
                           String r9_BType,
                           String rb_BankId, String ro_BankOrderId, String rp_PayDate, String rq_CardNo, String ru_Trxtime,
                           String hmac, Model model, HttpServletResponse response) throws Exception {
		// 身份校验 --- 判断是不是支付公司通知你
		String keyValue = ResourceBundle.getBundle("merchantInfo").getString("keyValue");
		// 自己对上面数据进行加密 --- 比较支付公司发过来hamc
		boolean isValid = PaymentUtil.verifyCallback(hmac, p1_MerId, r0_Cmd, r1_Code, r2_TrxId, r3_Amt, r4_Cur, r5_Pid,
				          r6_Order, r7_Uid, r8_MP, r9_BType, keyValue);				 
		if(isValid){
			// 响应数据有效
			if(r9_BType.equals("1")){
				// 浏览器重定向
				System.out.println("回调校验匹配成功!");
				model.addAttribute("msg", "您的订单号为:"+r6_Order+",金额为:"+r3_Amt+"已经支付成功,等待发货~~");				
			}else if(r9_BType.equals("2")){
				// 服务器点对点 --- 支付公司通知你
				System.out.println("付款成功！");
				// 修改订单状态 为已付款
				// 回复支付公司
				response.getWriter().print("success");
			}			
			//修改订单状态  已支付 orderId = r6_Order
			orderService.updateOrderStatus(r6_Order, rb_BankId); //交易流水号
			// 修改订单状态
			//orderService.updateOrderStatus(out_trade_no, "bankpay"); //交易号码 Order2Service
		}else{
			// 数据无效
			System.out.println("数据被篡改！");
		}		
		return "shop/pay/pay_success";
	}
	
	//获取订单 实付金额
	public double getPayment(ConfirmPay confirmPay){
		double payment = 0.0;
		String outTradeNo = confirmPay.getOutTradeNo();
		if(StringUtils.isNotBlank(outTradeNo)){ //多订单
			TbPayLog payLog = payLogService.findOne(Long.valueOf(outTradeNo));
			payment = Double.valueOf(payLog.getTotalFee()/100);
		}else{
			TbOrder order = orderMapper.selectByPrimaryKey(Long.valueOf(confirmPay.getOrderId()));
			payment = order.getPayment().doubleValue();
		}
		return payment;
	}
}

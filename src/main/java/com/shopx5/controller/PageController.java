package com.shopx5.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.mapper.SpUserInfoMapper;
import com.shopx5.mapper.result.UserMapper;
import com.shopx5.pojo.SpUser;
import com.shopx5.pojo.SpUserInfo;
import com.shopx5.pojo.result.Role;
import com.shopx5.pojo.result.User;
import com.shopx5.service.common.IndexService;
/**
 * 展示登录和注册页面的Controller
 * @author 多商户商城系统
 */
@Controller
public class PageController {
	@Autowired
	private IndexService indexService;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private SpUserInfoMapper userInfoMapper;
	
	// 注册
	@RequestMapping("/page/register")
	public String showRegister() {
		return "manager/sso/register";
	}
	// 登录/回调
	@RequestMapping("/page/login")
	public String showLogin(String url, Model model) {
		model.addAttribute("redirect", url);
		return "manager/sso/login";
	}
	@RequestMapping("/admin/login")
	public String showManagerLogin() {
		return "manager/login";
	}
	// 登录用户管理后台
	@RequestMapping("/manage/toMain")
	public String toMain(HttpServletRequest request) {
		List<Integer> list = this.getCategory(request);
		if (list != null && list.contains(1)) {
			return "manager/index";
		}
		return "redirect:/page/login";
	}
	// 登录用户商城后台
	@RequestMapping("/shop/toMain")
	public String toShopMain(HttpServletRequest request) {
		//1.获取用户角色模块
		SpUser spUser = indexService.getUser(request);
		/*List<Integer> list = this.getCategory(request);
		if (list != null && list.contains(2)) {
			return "forward:/shop/index/order";
		}*/
		if(spUser != null){
			return "forward:/shop/index/order";
		}
		return "redirect:/page/login";
	}
	// 获取角色Category权限
	public List<Integer> getCategory(HttpServletRequest request){
		//1.获取用户角色模块
		SpUser spUser = indexService.getUser(request);
		if (spUser == null) return null;
		//2.获取用户角色
		User user = userMapper.findUserAndRole(spUser.getUserId());
		List<Role> rList = user.getRoleList();
		List<Integer> list = new ArrayList<>();
		for (Role role : rList) {
			list.add(role.getCategory());
		}
		return list;
	}
////////////////////////////////////////////////////////////////////
	
	//管理页面
	@RequestMapping("/temp/{temp}/{type}/{jsp}")
	public String showManagerList(@PathVariable String temp, @PathVariable String type, @PathVariable String jsp,
								  String id, String curdQX, HttpServletRequest request, Model model) {
		//获取用户CURD/BUTTON权限
		Map<String, Integer> QXmap = indexService.getPower(request);
		model.addAttribute("QXmap", QXmap); //列表jsp校验
		model.addAttribute("id", id);
		model.addAttribute("curdQX", curdQX);
		return temp+"/"+type+"/"+jsp;
	}
	//商城页面
	@RequestMapping("/temp/shopx5/{type}")
	public String showItemList(@PathVariable String type, HttpServletRequest request, Model model) {
		Map<String, Integer> QXmap = indexService.getPower(request);
		model.addAttribute("QXmap", QXmap);
		return "shopx5/"+type;
	}
	


////////////////////////////////////////////////////////////////////
	
	//进入商城_修改用户详情
	@RequestMapping("/shop/user/_info")
	public String toShopInfo() {
		return "/shop/user/user_info";
	}
	//进入商城_修改用户头像
	@RequestMapping("/shop/user/_head")
	public String toShopHead() {
		return "/shop/user/user_head";
	}
	// 获取用户详情
	@RequestMapping("/shop/user/info")
	@ResponseBody
	public SpUserInfo getUserInfo(HttpServletRequest request) {
		SpUser user = (SpUser) request.getAttribute("spUser");
		if (user == null) return null;
		SpUserInfo userInfo = userInfoMapper.selectByPrimaryKey(user.getUserId());
		return userInfo;
	}

////////////////////////////////////////////////////////////////////
	

}

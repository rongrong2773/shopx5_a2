package com.shopx5.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopx5.common.AreaVo;
import com.shopx5.common.Result;
import com.shopx5.pojo.ScArea;
import com.shopx5.service.common.AreaService;
import com.shopx5.utils.JsonUtils;
/**
 * @author tang S h o p X 5 多商户商城系统
 */
@Controller
public class AreaController {
	@Autowired
	private AreaService areaService;
	
	//1 省市区三级联动 导入
	@RequestMapping(value="/area/import")
	@ResponseBody
	public Result areaImport(String areaJson) {
		/* areaJson格式
			var provinceList = [
			{name:'北京', shortName:"京", cityList:[
				{name:'市辖区', areaList:[{name:'东城区'},{name:'西城区'},{name:'怀柔区'},{name:'平谷区'}]},
				{name:'县', areaList:[{name:'密云县'},{name:'延庆县'}]}
				]},		
			{name:'安徽', shortName:"", cityList:[
				{name:'合肥市', areaList:[{name:'瑶海区'},{name:'庐阳区'},{name:'肥东县'},{name:'肥西县'}]},
				{name:'芜湖市', areaList:[{name:'镜湖区'},{name:'马塘区'},{name:'繁昌县'},{name:'南陵县'}]},
				]}
			]
		*/
		long startTime = System.currentTimeMillis();
		List<AreaVo> provinceList = JsonUtils.jsonToList(areaJson, AreaVo.class);		
		//1 保存省份
		int code1 = 1010000;
		int code2 = 1010101;
		int code3 = 1010201;
		int id = 1;
		for (AreaVo pro : provinceList) {
			String proName = pro.getName();
			String shortName = pro.getShortName();
			int proId = id;
			//添加省份                     id,父id,名称,简称,等级,code
			areaService.addArea(id, 0, proName, shortName, 1, code1);
			
			//2 保存市
			List<AreaVo> cityList = pro.getCityList();
			for (AreaVo city : cityList) {
				String cityName = city.getName();
				int cityId = id+1;
				//添加市                   id,父id,名称,等级,code
				areaService.addArea(++id, proId, cityName, 2, code2);
				
				//3 保存区
				List<AreaVo> areaList = city.getAreaList();
				for (AreaVo area : areaList) {
					String areaName = area.getName();
					//添加区                   id,父id,名称,等级,code
					areaService.addArea(++id, cityId, areaName, 3, code3);
					//区间隔+1
					code3 += 1;
				}
				
				//市间隔+1
				code2 += 1;
			}
			
			//省市区间隔+10000
			code1 += 10000;
			code2 = 101; code2 += code1; //重置为开始+10000 sh opx5
			code3 = 201; code3 += code1; //重置为开始+10000
			//id到新省份循环+1
			id++;
		}
		long endTime = System.currentTimeMillis();
		float excTime = (float) (endTime-startTime) / 1000;
		return Result.ok("area数据导入成功! 用时"+excTime+"秒");
	}
	
	//2 自动加载联动一级菜单
	@RequestMapping({"/area/getProvince", "/area/getCity", "/area/getArea"})
	@ResponseBody
	public Result getProvince(@RequestParam(defaultValue="0") Integer parentId) {
		List<ScArea> areaList = areaService.getArea(parentId);
		return Result.ok(areaList);
	}
}

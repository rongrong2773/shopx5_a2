package com.shopx5.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CORS过滤器，用于解决浏览器访问url的跨域问题
 * @author sjx
 * 2019年6月28日 下午3:53:15
 */
public class CorsFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}
	
	/**
	 * 参数设置参见    https://blog.csdn.net/root_miss/article/details/82740399
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		// 响应头指定了该响应的资源是否被允许给指定的origin共享。
		// *表示所有域都可以访问，同时可以将*改为指定的url，表示只有指定的url可以访问到资源
		// 多个以逗号分隔
		response.setHeader("Access-Control-Allow-Origin", "*");
		// 设置允许请求的方法，多个方法以逗号分隔。以可以设置为*
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		// 设置允许请求自定义的请求头字段，多个字段以逗号分开
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type, Authorization");
//		response.setHeader("Access-Control-Allow-Headers", "Authentication,Origin, X-Requested-With, Content-Type, Accept");
		response.setHeader("Access-Control-Allow-Credentials", "true");    // 设置是否允许发送Cookie
		
		if (request.getMethod().equals("OPTIONS")) {
			System.out.println("=========: 发送了预检请求");
			// 跨域资源共享标准新增了一组HTTP首部字段，允许服务器声明哪些源站有权限访问哪些资源
			// 另外，规范要求对那些可能对服务器数据产生副作用的HTTP请求方法（特别是GET以外的HTTP请求，或者搭配某些MIME类型的POST请求）
			// 浏览器必须首先使用OPTIONS方法发起一个预检请求（preflight request），从而获知服务端是否允许该跨域请求。
			// 服务器确认允许之后，才能发起实际的HTTP请求。
			// 在预检请求的返回中，服务器端也可以通知客户端，是否需要携带身份凭证（包括Cookies和HTTP认证相关数据）
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		}
		
		// 响应头设置
		/*response.setHeader("Access-Control-Allow-Headers","content-type,token,id");*/
//		response.setHeader("Access-Control-Request-Headers"," Origin, X-Requested-With, content-Type, Accept, Authorization");
//		
//		response.setHeader("X-Powered-By", "3.2.1");
//		response.setHeader("Content-Type", "application/json;charset=utf-8");
		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {

	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>商品编辑</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 富文本编辑器 -->
<link rel="stylesheet" href="/shopx5/plugins/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="/shopx5/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="/shopx5/plugins/kindeditor/lang/zh_CN.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/goodsController2.js"></script>
<script type="text/javascript" src="/shopx5/js/service/goodsService2.js"></script>
<script type="text/javascript" src="/shopx5/js/service/itemCatService.js"></script> <!-- 引入 分类-->
<script type="text/javascript" src="/shopx5/js/service/templateTypeService2.js"></script> <!-- 引入 模版-->
<script type="text/javascript" src="/shopx5/js/service/uploadService2.js"></script> <!-- 上传 -->
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5" ng-controller="goodsController" ng-init="selectItemCat1List();findOne()">
	<!-- 正文区域 -->							<!--增加为:获取分类0, 修改为:获取{goods,goodsDesc,List<TbItem>}-->
	<section class="content">
		<div class="box-body">
			<!--tab页-->
			<div class="nav-tabs-custom">
				<!--tab头-->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#home" data-toggle="tab">商品基本信息</a></li>
					<li><a href="#pic_upload" data-toggle="tab">商品图片</a></li>
					<li><a href="#customAttribute" data-toggle="tab">扩展属性</a></li>
					<li><a href="#spec" data-toggle="tab">规格</a></li>
				</ul>
				<!--tab头/-->
				<!--tab内容-->
				<div class="tab-content">
					<!--表单内容-->
					<div class="tab-pane active" id="home">
						<div class="row data-type">
							<div class="col-md-2 title">商品分类</div>
							<div class="col-md-10 data">
								<table>
									<tr>			<!-- angular下拉框   定义category1Id变化 -->
										<td><select class="form-control"
											ng-model="entity.goods.category1Id"
											ng-options="item.id as item.name for item in itemCat1List">
										</select></td>
										<td><select class="form-control select-sm"
											ng-model="entity.goods.category2Id"
											ng-options="item.id as item.name for item in itemCat2List"></select>
										</td>
										<td><select class="form-control select-sm"
											ng-model="entity.goods.category3Id"
											ng-options="item.id as item.name for item in itemCat3List"></select>
										</td>
										<!--<td>ID:{{entity.goods.templateTypeId}}</td>-->
									</tr>
								</table>
							</div>
							<div class="col-md-2 title">商品名称</div>
							<div class="col-md-10 data">
								<input type="text" ng-model="entity.goods.goodsName"
									class="form-control" placeholder="商品名称" value="">
							</div>
							<div class="col-md-2 title">品牌</div>
							<div class="col-md-10 data">
								<select class="form-control" ng-model="entity.goods.brandId"
									ng-options="item.id as item.text for item in templateType.brandIds"></select> <!-- ng-options -->
							</div>
							<div class="col-md-2 title">副标题</div>
							<div class="col-md-10 data">
								<input type="text" ng-model="entity.goods.caption"
									class="form-control" placeholder="副标题" value="">
							</div>
							<div class="col-md-2 title">价格</div>
							<div class="col-md-10 data">
								<div class="input-group">
									<span class="input-group-addon">¥</span> <input type="text"
										ng-model="entity.goods.price" class="form-control"
										placeholder="价格" value="">
								</div>
							</div>
							<div class="col-md-2 title editer">商品介绍</div>
							<div class="col-md-10 data editer">
								<textarea name="content"
									style="width: 800px; height: 400px; visibility: hidden;"></textarea>
							</div>
							<div class="col-md-2 title rowHeight2x">包装列表</div>
							<div class="col-md-10 data rowHeight2x">

								<textarea rows="4" class="form-control"
									ng-model="entity.goodsDesc.packageList" placeholder="包装列表"></textarea>
							</div>
							<div class="col-md-2 title rowHeight2x">售后服务</div>
							<div class="col-md-10 data rowHeight2x">
								<textarea rows="4" class="form-control"
									ng-model="entity.goodsDesc.saleService" placeholder="售后服务"></textarea>
							</div>
						</div>
					</div>
					
					
					<!--图片上传-->
					<div class="tab-pane" id="pic_upload">
						<div class="row data-type">
							<!-- 颜色图片 -->
							<div class="btn-group">
								<button type="button" ng-click="image_entity={}" class="btn btn-default" data-target="#uploadModal" data-toggle="modal" title="新增图片">
									<i class="fa fa-file-o"></i> 新增图片
								</button>
							</div>
							<table class="table table-bordered table-striped table-hover dataTable">
								<thead>
									<tr>
										<th class="sorting">颜色</th>
										<th class="sorting">图片</th>
										<th class="sorting">操作</th>
								</thead>
								<tbody>
									<tr ng-repeat="pojo in entity.goodsDesc.itemImages">
										<td>{{pojo.color}}</td>
										<td><img alt="{{pojo.color}}" src="{{pojo.url}}" height="75"></td>
										<td>
											<button type="button" ng-click="remove_iamge_entity($index)" class="btn btn-default" title="删除">
												<i class="fa fa-trash-o"></i> 删除
											</button>
										</td>
									</tr>
								</tbody>
							</table>
							<!--{{entity.goodsDesc.itemImages}}-->
						</div>
					</div>
					
					
					<!--扩展属性-->
					<div class="tab-pane" id="customAttribute">
						<div class="row data-type">
							<div ng-repeat="pojo in entity.goodsDesc.customAttributeItems">
								<div class="col-md-2 title">{{pojo.text}}</div>
								<div class="col-md-10 data">
									<input class="form-control" ng-model="pojo.value" placeholder="{{pojo.text}}">
								</div>
							</div>
						</div>
					</div>
					<!--规格-->
					<div class="tab-pane" id="spec">
						<div class="row data-type">
							<div class="col-md-2 title">是否启用规格</div>
							<div class="col-md-10 data">
								<input type="checkbox" ng-model="entity.goods.isEnableSpec"
									ng-true-value="1" ng-false-value="0"> <span style="color:red">请选择</span>
							</div>
						</div>
						<p>
						<div ng-if="entity.goods.isEnableSpec == 1">
							<!-- 规格若选中:显示
						   	  specList: [{"id":27,"text":"网络",options:[{id:98,optionName:"移动2G",spec_id:27,orders:1}]},,]
							-->
							<div class="row data-type">
								<div ng-repeat="pojo in specList">
									<div class="col-md-2 title">{{pojo.text}}</div> <!-- 网络 -->
									<div class="col-md-10 data">
										<span ng-repeat="option in pojo.options">
										<!-- 复杂:规格显示, 选择规格的选项名 checkAttributeValue() 修改时,判断是否选中
										                                      点击规格的选项名  updateSpecAttribute() 增加时,
																		   createItemList()      增加时,创建sku信息填单每行栏 -->
										<input ng-checked="checkAttributeValue(pojo.text,option.optionName)"
									   		ng-click="updateSpecAttribute($event,pojo.text,option.optionName);createItemList()"
											type="checkbox">{{option.optionName}} <!-- 移动2G,移动3G,移动4G -->
										</span>
									</div>
								</div>
							</div>
							
							<div class="row data-type">
								<table class="table table-bordered table-striped table-hover dataTable">
									<thead>
										<tr>
											<th class="sorting" ng-repeat="pojo in entity.goodsDesc.specificationItems">
												{{pojo.attributeName}}
											</th>
											<th class="sorting">市场价</th>
											<th class="sorting">价格</th>
											<th class="sorting">库存</th>
											<th class="sorting">是否启用</th>
											<th class="sorting">是否默认</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="pojo in entity.itemList">
											<td ng-repeat="item in entity.goodsDesc.specificationItems">
												{{pojo.spec[item.attributeName]}}
											</td>
											<td><input class="form-control" ng-model="pojo.marketPrice" placeholder="市场价" /></td>
											<td><input class="form-control" ng-model="pojo.price" placeholder="价格" /></td>
											<td><input class="form-control" ng-model="pojo.num" placeholder="库存数量" /></td>
											<td><input type="checkbox" ng-model="pojo.status" ng-true-value="1" ng-false-value="0" /></td>
											<td><input type="checkbox" ng-model="pojo.isDefault" ng-true-value="1" ng-false-value="0" /></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!--tab内容/-->
				<!--表单内容/-->
			</div>
		</div>
		<div class="btn-toolbar list-toolbar">
			<button class="btn btn-primary" ng-click="save()"><i class="fa fa-save"></i>保存商品</button>
		</div>
	</section>
	
	
	<!-- 图片上传窗口 -->
	<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">上传商品图</h3>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped">
						<tr>
							<td></td>
							<td><span style="color:red">颜色填：</span>
								大图1，大图2，大图3，大图4，大图5<br>
								<span style="padding-left:60px;">
								黑色，蓝色，金色，粉色默认（若为默认sku）
								<span>
							</td>
						</tr>
						<tr>
							<td>颜色</td>
							<td><input ng-model="image_entity.color" class="form-control" placeholder="颜色"></td>
						</tr>
						<tr>
							<td>图片</td>
							<td>
								<table>
									<tr>
										<td><input type="file" id="file" /><br>
											<button class="btn btn-primary" type="button" ng-click="uploadFile()">上传</button>
										</td>
										<td ng-if="image_entity.url == null"></td>
										<td ng-if="image_entity.url != null"><img src="{{image_entity.url}}" height="100"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success"
					  data-dismiss="modal" ng-click="add_image_entity()" aria-hidden="true">保存</button>
					<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</button>
				</div>
				<!--{{image_entity}}-->
			</div>
		</div>
	</div>
	
	<!-- 正文区域 /-->
	<script type="text/javascript">
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="content"]', {
				allowFileManager : true
			});
		});
	</script>
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>品牌管理</title>
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script><!-- 分页初始化 -->
<script type="text/javascript" src="/shopx5/js/service/brandService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/brandCatService.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/brandController.js"></script>
<!-- 引入select2的相关的css和js -->
<link rel="stylesheet" href="/shopx5/plugins/select2/select2.css" />
<link rel="stylesheet" href="/shopx5/plugins/select2/select2-bootstrap.css" />
<script src="/shopx5/plugins/select2/select2.min.js" type="text/javascript"></script> <!-- 这个3个是select2原生引入 -->
<script type="text/javascript" src="/shopx5/js/angular-select2.js"></script> <!-- 注意: angular整合select2 必须放在base_pagination.js下面 使用app -->
<!-- UEditor编辑器  -->
<link type="text/css" href="/cpts/ueditor/themes/default/css/ueditor.css" rel="stylesheet" />
<script type="text/javascript"charset="utf-8" src= "/cpts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5"	ng-controller="brandController" ng-init="findBrandCatList()">
	<!-- .box-body -->
	<!-- <div class="box-header with-border">
		<h3 class="box-title">品牌管理</h3>
	</div> -->
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<c:if test="${QXmap.add == 1 }">
						<button type="button" class="btn btn-default" title="新建"
							data-toggle="modal" data-target="#editModal" ng-click="entity={}"> <!-- 定义变量,添加后再点置空遍历{} -->
							<i class="fa fa-file-o"></i> 新建
						</button>
						</c:if>
						<c:if test="${QXmap.del == 1 }">
						<button type="button" class="btn btn-default" title="删除"	ng-click="dele()">
							<i class="fa fa-trash-o"></i> 删除
						</button>
						</c:if>
						<button type="button" class="btn btn-default" title="刷新" onclick="window.location.reload();">
							<i class="fa fa-refresh"></i> 刷新
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback">
					品牌名称：<input type="text" ng-model="searchEntity.name">&nbsp;
					首字母：<input type="text" ng-model="searchEntity.firstChar">
					<input class="btn btn-default" ng-click="reloadList()"
						type="button" value="查询">
				</div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList"
				class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right:0px"><input id="selall" type="checkbox" class="icheckbox_square-blue"
							ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting_asc">品牌ID</th>
						<th class="sorting">品牌名称</th>
						<th class="sorting">品牌首字母</th>
						<th class="sorting">品牌图</th>
						<th class="sorting">所属类目</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{entity.name}}</td>
						<td>{{entity.firstChar}}</td>
						<td style="margin:0;padding:0;"><img src="{{entity.image}}" height="30" /></td>
						<td>{{jsonToString(entity.cids,'text')}}</td>
						<td class="text-center">
							<c:if test="${QXmap.edit == 1 }">
							<button type="button" class="btn bg-olive btn-xs"
								ng-click="findOne(entity.id)" data-toggle="modal"
								data-target="#editModal">修改</button>
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
		<!-- 分页 -->
		<tm-pagination conf="paginationConf"></tm-pagination>
	</div>
	<!-- /.box-body -->
	
	<!-- 编辑窗口 -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">品牌编辑</h3>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" width="800px">
						<tr>
							<td>品牌名称</td>
							<td><input class="form-control" ng-model="entity.name" placeholder="品牌名称"></td>
							<!-- 填数据,自动封装到entity对象中 -->
						</tr>
						<tr>
							<td>首字母</td>
							<td><input class="form-control" ng-model="entity.firstChar" placeholder="首字母"></td>
						</tr>
						<tr>
							<td>品牌图</td>
							<td><input class="form-control" ng-model="entity.image" type="hidden" id="pic">
								<!-- <img id='npic' height='50' src='' /> -->
								<img ng-if="entity.image != null" src="{{entity.image}}" height="50" />
								<input type="button" onclick="upImageE('pic');" value="上传图片">
							</td>
						</tr>
						<tr>
							<td>所属类目</td>
							<td><!-- <input class="form-control" ng-model="entity.cids" placeholder="所属类目"> -->
							<!--
			      				ng-model:     绑定下拉列表的id         {,}
			      				select2-model:绑定下拉列表的id和value  [{},{},]
			      				config:       代表的是数据的来源              [{},]
		      			 	-->
		      			 	<input ng-model="entity.cid" select2 select2-model="entity.cids"
								config="brandCatList" multiple placeholder="支持多选哦"
								class="form-control" type="text" />      <!-- config="brandCatList" 查询关联的品牌信息 -->
							</td>                                        <!-- select2-model="entity.cids" 取遍历{}的id -->
							</td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" ng-click="save()">保存</button>
					<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</button>
				</div>
			</div>
		</div>
	</div>
<script id="UeditorEdit" type="text/plain"></script>
<script type="text/javascript">
	//监听图片上传
	var imgId;
    var _ueEdit = UE.getEditor('UeditorEdit');
    _ueEdit.ready(function() {
        //设置编辑器不可用
        //_ueEdit.setDisabled();  这个地方要注意 一定要屏蔽
        //隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
        _ueEdit.hide();
        //侦听图片上传
        _ueEdit.addListener('beforeinsertimage', function (t, arg) {
            //将地址赋值给相应的input,只去第一张图片的路径
            var imgs = "";
            for(var a in arg){
            	if(a == arg.length-1){
            		imgs += arg[a].src;
            		break;
            	}
            	imgs += arg[a].src+',';
            }
			
            //$("#"+imgId).attr("value", arg[0].src); // 或 imgs
            //图片预览
            $("#"+imgId).next("img").remove();
            for (var i = arg.length-1; i >= 0 ; i--) {
				$("#"+imgId).after($("<img id='npic' height='50' />").attr("src", arg[i].src));
			};
			
			//外部javascript的js修改angularjs域中的值
			var appElement = document.querySelector('[ng-controller=brandController]');
			var scope = angular.element(appElement).scope();
            //要修改变量的值
			scope.entity.image = arg[0].src;
			//修改完成后，需进行提交
			//a、外部改变了scope的值，如果想同步到Angular控制器中，则需要调用$apply()方法即可
			scope.$apply();
			//b、异步提交
			//scope.$applyAsync();
        });
    });
    
    //弹出图片上传对话框
    function upImageE(id) {
    	imgId = id;
        var myImage = _ueEdit.getDialog("insertimage");
		myImage.open();
		//解决遮罩
		$("#edui_fixedlayer").css("z-index","1065");
    };
</script>
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>秒杀商品订单管理</title>
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/seckillOrderController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/seckillOrderService.js"></script>
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5" ng-controller="seckillOrderController" ng-init="">
	<!-- .box-body -->
	<div class="box-header with-border">
		<h3 class="box-title">秒杀商品订单管理</h3>
	</div>
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<button type="button" class="btn btn-default" title="刷新" onclick="window.location.reload();">
							<i class="fa fa-refresh"></i> 刷新
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback">
					状态：<select ng-model="searchEntity.status">
							<option value="">全部</option>
							<option value="1">未付款</option>
							<option value="2">已付款</option>
							<option value="3">未发货</option>
							<option value="4">已发货</option>
							<option value="5">交易成功</option>
							<option value="6">交易关闭</option>
							<option value="7">待评价</option>
						</select>
					收货人：<input type="text" ng-model="searchEntity.receiver">
					<button class="btn btn-default" ng-click="reloadList()">查询</button>
				</div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList" class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right:0px"><input id="selall" type="checkbox" class="icheckbox_square-blue" ng-model="master" ng-click="checkAll(master,allIds)">
						</th>
						<th class="sorting_asc">订单号</th>
						<th class="sorting">订单金额</th>
						<th class="sorting">收货人</th>
						<th class="sorting">电话号码</th>
						<th class="sorting">收货地址</th>
						<th class="sorting">订单状态</th>
						<th class="text-center">详情</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>&yen; {{entity.money.toFixed(2)}}</td>
						<td>{{entity.receiver}}</td>
						<td>{{entity.receiver_mobile}}</td>
						<td>{{entity.receiverAddress}}</td>
						<td><span style="color:{{oColor[entity.status]}};">{{oStatus[entity.status]}}</span></td>
						<td class="text-center">
							<a class="btn bg-olive btn-xs" ng-click="findItem(entity.id)" data-toggle="modal"
								data-target="#sellerModal">订单商品</a>
						</td>
						<td class="text-center">
							<a class="btn bg-olive btn-xs" ng-if="entity.status==2"
								ng-click="updateStatus(entity.id,'4')">发货</a>
						</td>
						<td class="text-center">
							<a class="btn bg-olive btn-xs" ng-if="entity.status==5">评价</a>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
		<!-- 分页 -->
		<tm-pagination conf="paginationConf"></tm-pagination>
	</div>
	<!-- /.box-body -->
	
	<!-- 商家详情 -->
		<div class="modal fade" id="sellerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 id="myModalLabel">秒杀订单商品</h3>
					</div>
					<div class="modal-body">
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane active in" id="home">
								<table class="table table-bordered table-striped" width="800px">
									<tr>
										<td>商品名称</td>
										<td>商品图</td>
										<td>秒杀价</td>
										<td>原价</td>
									</tr>
									<tr>
										<td>{{item.title}}</td>
										<td><img src="{{item.smallPic}}" width="80" height="80" /></td>
										<td>&yen; {{item.costPrice.toFixed(2)}}</td>
										<td>&yen; {{item.price.toFixed(2)}}</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						总价: &yen; {{item.costPrice.toFixed(2)}} &nbsp;
						<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</button>
					</div>
				</div>
			</div>
		</div>
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>商品分类管理</title>
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/itemCatController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/itemCatService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/templateTypeService.js"></script><!-- 引入 模版Service -->
<!-- 引入select2的相关的css和js -->
<link rel="stylesheet" href="/shopx5/plugins/select2/select2.css" />
<link rel="stylesheet" href="/shopx5/plugins/select2/select2-bootstrap.css" />
<script src="/shopx5/plugins/select2/select2.min.js" type="text/javascript"></script> <!-- 这个3个是select2原生引入 -->
<script type="text/javascript" src="/shopx5/js/angular-select2.js"></script> <!-- 注意: angular整合select2 必须放在base_pagination.js下面 使用app -->
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5"	ng-controller="itemCatController" ng-init="findByParentId(0);findTypeList()">
	<!-- .box-body
	<div class="box-header with-border">
		<h3 class="box-title">商品分类管理</h3>
	</div> -->
	<div class="box-body">
		<ol class="breadcrumb">
			<li><a href="#" ng-click="grade=1;selectList({id:0})">顶级分类列表</a></li>
			<li ng-if="grade > 1"><a href="#" ng-click="setGrade(2);selectList(entity_1)" ng-cloak>{{entity_1.name}}</a></li>
			<li ng-if="grade > 2"><a href="#" ng-click="setGrade(3);selectList(entity_2)" ng-cloak>{{entity_2.name}}</a></li>
		</ol>
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<button type="button" class="btn btn-default" title="新建"
							data-toggle="modal" data-target="#editModal" ng-click="entity={}">
							<i class="fa fa-file-o"></i> 新建
						</button>
						<button type="button" class="btn btn-default" ng-click="dele()" title="删除">
							<i class="fa fa-trash-o"></i> 删除 
						</button>
						<button type="button" class="btn btn-default" onclick="window.location.reload();" title="刷新">
							<i class="fa fa-check"></i> 刷新
						</button>
						
					<button type="button" class="btn btn-default" ng-click="cacheCat()" title="缓存分类">
						<i class="fa fa-check"></i> 缓存分类
					</button>
						
					</div>
				</div>
			</div>
			<!--数据列表-->
			<table id="dataList"
				class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right:0px"><input type="checkbox" class="icheckbox_square-blue" ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting_asc">分类ID</th>
						<th class="sorting">分类名称</th>
						<th class="sorting">类型模板ID</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{entity.name}}</td>
						<td>{{entity.typeId}}</td>
						<td class="text-center">
							<span ng-if="grade < 3 ">
								<button type="button" ng-click="setGrade(grade+1);selectList(entity);"
									class="btn bg-olive btn-xs">查询下级</button>
							</span>
							<button type="button" class="btn bg-olive btn-xs" data-toggle="modal"
								ng-click="findOne(entity.id)" data-target="#editModal">修改</button>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
	</div>
	<!-- /.box-body -->
	<!-- 编辑窗口 -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">商品分类编辑</h3>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" width="800px">
						<tr>
							<td>上级商品类目</td>
							<td ng-if="grade == 1">顶级栏目</td>
							<td ng-if="grade == 2">{{entity_1.name}} {{entity_1.id}}</td>
							<td ng-if="grade == 3">{{entity_2.name}} {{entity_2.id}}</td>
						</tr>
						<tr>
							<td>商品分类名称</td>
							<td><input class="form-control" ng-model="entity.name" placeholder="商品分类名称">
							</td>
						</tr>
						<tr>				  <!-- select2 单选 *** -->
							<td>类型模板 typeId: {{entity.typeId}}</td>
							<td><input select2 ng-model="entity.typeId" config="typeList"
								placeholder="商品类型模板" class="form-control" type="text" /></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" data-dismiss="modal"
						aria-hidden="true" ng-click="save()">保存</button>
					<button class="btn btn-default" data-dismiss="modal"
						aria-hidden="true">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>



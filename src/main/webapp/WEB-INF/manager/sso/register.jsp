<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户注册 - ShopX5商城系统</title>
<meta name="keywords" content="用户注册" />
<meta name="description" content="用户注册" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<link href="/static/shop/css/basei.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_headeri.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_login.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesomei.min.css" rel="stylesheet" />
<!--[if IE 7]>
	<link href="/static/shop/css/font-awesome-ie7i.min.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script type="text/javascript" src="/static/shop/js/html5shiv.js"></script>
	<script type="text/javascript" src="/static/shop/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/static/shop/js/jquery.js"></script>
<script type="text/javascript" src="/static/shop/js/jquery.ui.js"></script>
<script type="text/javascript" src="/static/shop/js/jquery.validation.min.js"></script>      <!-- validate校验表单 -->
<script type="text/javascript" src="/static/shop/js/taglibs.js"></script>                    <!-- input框里灰色提示，使用前先引入jquery -->
<script type="text/javascript" src="/static/shop/js/tabulous.js"></script>                   <!-- 注册切换 -->
<script type="text/javascript" src="/static/shop/js/common.js"></script>                     <!-- 公共js 可删除. -->
</head>

<body>
<div class="header-wrap">
  <header class="public-head-layout wrapper">
    <h1 class="site-logo"><a href="/"><img src="/static/shop/images/logo.png" class="pngFix" /></a></h1>
    <div class="nc-login-now">我已经注册，现在就<a href="/page/login" title="登录" class="register">登录</a></div>
  </header>
</div>
<!-- PublicHeadLayout End -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="nc-register-bg">
  <div class="nc-register-box">
    <div class="nc-register-layout">
      <div class="left">
        <div class="nc-register-mode">
          <ul class="tabs-nav">
             <li><a href="#default">账号注册<i></i></a></li>
             <li><a href="#mobile">手机注册<i></i></a></li>
          </ul>          
          <div id="tabs_container" class="tabs-container">          
			<!-- 1.默认注册 -->
            <div id="default" class="tabs-content">            
              <form id="register_form" class="nc-login-form" method="post">
				<dl>
                  <dt>用户名：</dt>
                  <dd>
                    <input type="text" id="userName" name="userName" class="text" tipMsg="请使用6-20个中、英文、数字及"-"符号" />
                  </dd>
                </dl>
                <dl>
                  <dt>设置密码：</dt>
                  <dd>
                    <input type="password" id="password" name="password" class="text" tipMsg="6-20个大小写英文字母、符号或数字" />
                  </dd>
                </dl>
                <dl>
                  <dt>确认密码：</dt>
                  <dd>
                    <input type="password" id="password_confirm" name="password_confirm" class="text" tipMsg="请再次输入密码" />
                  </dd>
                </dl>
                <dl class="mt15">
                  <dt>邮箱：</dt>
                  <dd>
                    <input type="text" id="email" name="email" class="text" tipMsg="输入常用邮箱作为验证及找回密码使用" />
                  </dd>
                </dl>
                <div class="code-div mt15">
                  <dl>
                    <dt>验证码：</dt>
                    <dd>
                      <input type="text" id="captcha" name="captcha" class="text w80" size="10" tipMsg="输入验证码" />
                    </dd>
                  </dl>
                  <span><img src="/code?n=1" name="codeimage" id="codeimage" />
                        <a class="makecode" href="javascript:void(0);">看不清，换一张</a></span>
                </div>
                <dl class="clause-div">
                  <dd>
                    <input name="agree" type="checkbox" class="checkbox" id="clause" value="1" checked="checked" />
                     阅读并同意<a href="javascript:void(0);" target="_blank" class="agreement" title="阅读并同意">《服务协议》</a>
                  </dd>
                </dl>
                <div class="submit-div">
                  <input type="submit" id="submit" value="立即注册" class="submit"/>
                </div>
              </form>
            </div>            
            
			<!-- 2.手机注册 -->
            <div id="mobile" class="tabs-content">
              <form id="post_form" class="nc-login-form" method="post">
                <dl>
                  <dt>手机号：</dt>
                  <dd>
                    <input type="text" class="text" autocomplete="off" value="" name="phone" id="phone" tipMsg="请输入手机号码" />
                  </dd>
                </dl>
                <div class="code-div">
                  <dl>
                    <dt>验证码：</dt>
                    <dd>
                      <input type="text" name="captcha" class="text w100" id="image_captcha" size="10" tipMsg="输入验证码" />
                    </dd>
                  </dl>
                  <span><img src="#" name="codeimage" id="codeimage">
                        <a class="makecode" href="javascript:void(0);">看不清，换一张</a></span>
                </div>
                <div class="tiptext" id="sms_text">
                	确保上方验证码输入正确，点击<span><a href="javascript:void(0);" onClick="get_sms_captcha('1')">
                	<i class="icon-mobile-phone"></i>发送短信验证</a></span>，并将您手机短信所接收到的"6位动态码"输入到下方短信验证，再提交下一步。
                </div>
                <dl>
                  <dt>短信验证：</dt>
                  <dd>
                    <input type="text" name="sms_captcha" autocomplete="off" tipMsg="输入6位短信验证码" class="text" id="sms_captcha" size="15" />
                  </dd>
                </dl>
                <div class="submit-div">
                  <input type="button" id="submitBtn" class="submit" value="下一步">
                </div>
              </form>
              
              <!-- 3.注册下一步 -->
              <form style="display:none;" id="register_sms_form" class="nc-login-form" method="post" action="#">
                <input type="hidden" name="telephone" id="telephone" value="" />
                <input type="hidden" name="captcha" id="captcha" value="" />
                <dl>
                  <dt>用户名：</dt>
                  <dd>
                    <input type="text" id="member_name" name="userName" class="text w150" value="" />
                  </dd>
                  <span class="note">系统生成随机用户名，可选择默认或自行修改一次。</span>
                </dl>
                <dl>
                  <dt>设置密码：</dt>
                  <dd>
                    <input type="text" id="sms_password" name="password" class="text w150" value="" />
                  </dd>
                  <span class="note">系统生成随机密码，请牢记或修改为自设密码。</span>
                </dl>
                <dl class="mt15">
                  <dt>邮箱：</dt>
                  <dd>
                    <input type="text" id="sms_email" name="email" class="text" value="" tipMsg="输入常用邮箱作为验证及找回密码使用" />
                  </dd>
                </dl>
                <dl class="clause-div">
                  <dd>
                    <input name="agree" type="checkbox" class="checkbox" id="sms_clause" value="1" checked="checked" />
                    阅读并同意<a href="javascript:void(0);" class="agreement" title="阅读并同意" target="_blank">《服务协议》</a></dd>
                </dl>
                <div class="submit-div">
                	<input type="submit" value="提交注册" class="submit" title="提交注册" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="right">
          <div class="api-login">
              <h4>使用合作网站账号直接登录</h4>
              <a href="javascript:void(0);" title="QQ账号登录" class="qq"><i></i></a>
              <a href="javascript:void(0);" title="新浪微博账号登录" class="sina"><i></i></a>
              <a href="javascript:void(0);" title="微信账号登录" class="wx"><i></i></a>
          </div>
          <div class="reister-after">
          <h4>注册之后您可以</h4>
          <ol>
            <li class="ico01"><i></i>购买商品支付订单</li>
            <li class="ico02"><i></i>收藏商品关注店铺</li>
            <li class="ico03"><i></i>安全交易诚信无忧</li>
            <li class="ico04"><i></i>积分获取优惠购物</li>
            <li class="ico05"><i></i>会员等级享受特权</li>
            <li class="ico06"><i></i>评价晒单站外分享</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function(){
	//初始化input灰色提示信息  引入taglibs.js
	$('input[tipMsg]').inputTipText({pwd:'password,password_confirm'});
	
	//注册方式切换  引入tabulous.js
	$('.nc-register-mode').tabulous({
		//动画缩放渐变 effect: 'scale'
		//动画左侧滑入 effect: 'slideLeft'		 
		//动画下方滑入 effect: 'scaleUp'
		//动画反转效果 effect: 'flip'
		effect: 'slideLeft'
	});
	var div_form = '#default';
	$(".nc-register-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click"); //找子类中.makecode被点击, 刷新验证码
    	}
	});
	//Code 双切换
	$("#default .makecode").click(function(){
		$("#default #codeimage").prop("src","/code?n=" + Math.random());
	});
	$("#mobile .makecode").click(function(){
		$("#mobile #codeimage").prop("src","/code?n=" + Math.random());
	});	
	
	//validate自定义验证方法: letters_name,mobile
	jQuery.validator.addMethod("letters_name", function(value, element) {
		return this.optional(element) || (/^[A-Za-z0-9\u4e00-\u9fa5_-]+$/i.test(value) && !/^\d+$/.test(value));
	}, "可包含_、-,不能是纯数字");
	jQuery.validator.addMethod("mobile", function(value, element) {
		return this.optional(element) || /^0?(13|15|17|18|14|19|16)[0-9]{9}$/.test(value);
	}, "请输入正确的手机号");

   
	//注册表单验证 ------###
    $("#register_form").validate({
        errorPlacement:function(error, element) { //更改错误信息显示的位置
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success:function(label) { //每个字段验证通过执行函数,取消错误显示提示
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
    	submitHandler:function(form) { //通过验证后运行的函数，里面要加上表单提交的函数，否则表单不会提交。    		
    		$.post("/user/register", $("#register_form").serialize(), function(data){
				if(data.status == 200){
					alert('用户注册成功，请登录！');
					location.href = "/page/login";
				} else {				
					alert(data.msg);
				}
			});
    		// form.submit();
    	},
        onkeyup: false, //默认true
        //rules格式
        rules : {
        	userName : {
                required : true,
                rangelength : [6,20],
                letters_name : true,
                remote   : { //远程校验
                    url :'/check/userName/1',
                    type:'get',
                    dataType: "json",      //接受数据格式  远程地址只能输出 "true" 或 "false"，不能有其他输出。
                    data:{
                    	userName : function(){
                            return $('#userName').val();
                        }
                    }
                }
            },
            password : {
                required : true,
                minlength: 6,
				maxlength: 20
            },
            password_confirm : {
                required : true,
                equalTo  : '#password'
            },
            email : {
                required : true,
                email    : true,
                remote   : {
                    url : '/check/email/3',
                    type:'get',
                    dataType: "json",
                    data:{
                        email : function(){
                            return $('#email').val();
                        }
                    }
                }
            },
            captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : '/check/captcha/4',
                    type:'get',
                    dataType: "json",
                    data:{
                        captcha : function(){
                            return $('#captcha').val();
                        }
                    },
                    complete: function(data) { //complete: 验证完成后,若校验是false,刷新验证码
                        if(data.responseText == 'false') {
                        	$("#default #codeimage").prop("src","/code?n=" + Math.random());
                        }
                    }
                }
            },
			agree : {
                required : true
            }
        },
        //messages格式:
        messages : {
        	userName : {
                required : '<i class="icon-exclamation-sign"></i>用户名不能为空',
                rangelength : '<i class="icon-exclamation-sign"></i>用户名必须在6-20个字符之间',
				letters_name: '<i class="icon-exclamation-sign"></i>可包含"_"、"-"，不能是纯数字',
				remote	 : '<i class="icon-exclamation-sign"></i>该用户名已经存在或含有敏感词'
            },
            password  : {
                required : '<i class="icon-exclamation-sign"></i>密码不能为空',
                minlength: '<i class="icon-exclamation-sign"></i>密码长度应在6-20个字符之间',
				maxlength: '<i class="icon-exclamation-sign"></i>密码长度应在6-20个字符之间'
            },
            password_confirm : {
                required : '<i class="icon-exclamation-sign"></i>请再次输入密码',
                equalTo  : '<i class="icon-exclamation-sign"></i>两次输入的密码不一致'
            },
            email : {
                required : '<i class="icon-exclamation-sign"></i>电子邮箱不能为空',
                email    : '<i class="icon-exclamation-sign"></i>这不是一个有效的电子邮箱',
				remote	 : '<i class="icon-exclamation-sign"></i>该电子邮箱已经存在'
            },
			captcha : {
                required : '<i class="icon-remove-circle" title="请输入验证码"></i>',
                minlength: '<i class="icon-remove-circle" title="请输入验证码"></i>',
				remote	 : '<i class="icon-remove-circle" title="验证码不正确"></i>'
            },
			agree : {
                required : '<i class="icon-exclamation-sign"></i>请勾选服务协议'
            }
        }
    });
});
</script>
<!-- 2. 手机注册 -->
<script type="text/javascript" src="/static/shop/js/connect_sms.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
	$("#submitBtn").click(function(){
        if($("#post_form").valid()){ //valid() 表单校验成功
            check_captcha();         //下一步
    	}
	});
	$("#post_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
        onkeyup: false,
		rules: {
			phone: {
                required : true,
                mobile : true,
                remote   : {
                    url : '/check/phone/2',
                    type: 'get',
                    dataType: "json",
                    data:{
                        captcha : function(){
                            return $('#phone').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	//alert('手机已注册，可直接登录！');
        					//location.href = "/page/login";
                        }
                    }
                }
            },
			captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : '/check/captcha/4',
                    type: 'get',
                    dataType: "json",
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	$("#mobile #codeimage").prop("src","/code?n=" + Math.random());
                        }
                    }
                }
            },
			sms_captcha: {
                required : function(element) {
                    return $("#image_captcha").val().length == 4;
                },
                minlength: 6
            }
		},
		messages: {
			phone: {
                required : '<i class="icon-exclamation-sign"></i>输入正确的手机号',
                mobile   : '<i class="icon-exclamation-sign"></i>输入正确的手机号',
                remote   : '<i class="icon-exclamation-sign"></i>已注册，可直接登录'
            },
			captcha : {
                required : '<i class="icon-remove-circle" title="请输入验证码"></i>',
                minlength: '<i class="icon-remove-circle" title="请输入验证码"></i>',
				remote	 : '<i class="icon-remove-circle" title="验证码不正确"></i>'
            },
			sms_captcha: {
                required : '<i class="icon-exclamation-sign"></i>请输入六位短信动态码',
                minlength: '<i class="icon-exclamation-sign"></i>请输入六位短信动态码'
            }
		}
	});
	
	//下一步表单
    $('#register_sms_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
    	submitHandler:function(form){
    		$.post("/user/register", $("#register_sms_form").serialize(), function(data){
				if(data.status == 200){
					alert('用户注册成功，请登录！');
					location.href = "/page/login";
				} else {
					alert(data.msg);
				}
			});
    	},
        rules : {
        	userName : {
                required : true,
                rangelength : [6,20],
                letters_name : true,
                remote   : {
                	url :'/check/userName/1',
                    type:'get',
                    dataType: "json",
                    data:{
                        member_name : function(){
                            return $('#member_name').val();
                        }
                    }
                }
            },
            password : {
                required   : true,
                minlength: 6,
				maxlength: 20
            },
            email : {
                email    : true,
                remote   : {
                	url : '/check/email/3',
                    type:'get',
                    dataType: "json",
                    data:{
                        email : function(){
                            return $('#sms_email').val();
                        }
                    }
                }
            },
            agree : {
                required : true
            }
        },
        messages : {
            member_name : {
                required : '<i class="icon-exclamation-sign"></i>用户名不能为空',
                rangelength : '<i class="icon-exclamation-sign"></i>用户名必须在6-20个字符之间',
				letters_name: '<i class="icon-exclamation-sign"></i>可包含"_"、"-"，不能是纯数字',
				remote	 : '<i class="icon-exclamation-sign"></i>该用户名已经存在或含有敏感词'
            },
            password  : {
                required : '<i class="icon-exclamation-sign"></i>密码不能为空',
                minlength: '<i class="icon-exclamation-sign"></i>密码长度应在6-20个字符之间',
				maxlength: '<i class="icon-exclamation-sign"></i>密码长度应在6-20个字符之间'
            },
            email : {
                email    : '<i class="icon-exclamation-sign"></i>这不是一个有效的电子邮箱',
				remote	 : '<i class="icon-exclamation-sign"></i>该电子邮箱已经存在'
            },
            agree : {
                required : '<i class="icon-exclamation-sign"></i>请勾选服务协议'
            }
        }
    });
});
</script>
<div id="cti">
  <div class="wrapper">
    <ul>
    </ul>
  </div>
</div>
<div id="footer" class="wrapper">
  <p><a href="/">首页</a>
   | <a href="#">招聘英才</a>
   | <a href="#">合作及洽谈</a>
   | <a href="#">联系我们</a>
   | <a href="#">关于ShopX5</a>
  </p>
  Copyright 2010-2018 ShopX5 Inc.,All rights reserved.<br />
  Powered by <a href="/" target="_blank"><span class="vol"><font class="b">Shop</font><font class="o">X5</font></span></a> 
  <br />
</div>
</body>
</html>



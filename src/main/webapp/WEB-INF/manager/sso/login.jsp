<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户登录 - ShopX5商城系统</title>
<meta name="keywords" content="用户登录" />
<meta name="description" content="用户登录" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<link href="/static/shop/css/basei.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_headeri.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_login.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesomei.min.css" rel="stylesheet" />
<!--[if IE 7]>
	<link href="/static/shop/css/font-awesome-ie7i.min.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script type="text/javascript" src="/static/shop/js/html5shiv.js"></script>
	<script type="text/javascript" src="/static/shop/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/static/shop/js/jquery.js"></script>
<script type="text/javascript" src="/static/shop/js/jquery.ui.js"></script>
<script type="text/javascript" src="/static/shop/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/static/shop/js/taglibs.js"></script>
<script type="text/javascript" src="/static/shop/js/tabulous.js"></script>
<script type="text/javascript" src="/static/shop/js/common.js"></script>
</head>

<body>
<div class="header-wrap">
  <header class="public-head-layout wrapper">
    <h1 class="site-logo"><a href="/"><img src="/static/shop/images/logo.png" class="pngFix" /></a></h1>
    <div class="nc-regist-now">
    	<span class="avatar"><img src="/static/shop/images/default_user_portrait.gif" /></span>
    	<span>您好，欢迎来到ShopX5<br/>已注册的会员请登录，或立即<a href="/page/register" title="注册新会员" class="register">注册新会员</a></span>
    </div>
  </header>
</div>
<!-- PublicHeadLayout End -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="nc-login-layout">
  <div class="left-pic"><img src="/static/shop/images/1.jpg" border="0" /></div>
  <div class="nc-login">
  <div class="arrow"></div>
    <div class="nc-login-mode">
      <ul class="tabs-nav">
         <li><a href="#default">用户登录<i></i></a></li>
         <li><a href="#mobile">手机动态码登录<i></i></a></li>
      </ul>
      <div id="tabs_container" class="tabs-container">      
      	<!-- 1.默认登录 -->
        <div id="default" class="tabs-content">
          <form id="login_form" class="nc-login-form" method="post">
            <dl>
              <dt>账&nbsp;&nbsp;&nbsp;号：</dt>
              <dd>
                <input type="text" class="text" autocomplete="off"  name="userName" tipMsg="可使用已注册的用户名或手机号登录" id="userName" />
              </dd>
            </dl>
            <dl>
              <dt>密&nbsp;&nbsp;&nbsp;码：</dt>
              <dd>
                <input type="password" class="text" name="password" autocomplete="off" tipMsg="6-20个大小写英文字母、符号或数字" id="password" />
              </dd>
            </dl>
            <div class="code-div mt15">
              <dl>
                <dt>验证码：</dt>
                <dd>
                  <input type="text" name="captcha" autocomplete="off" class="text w100" tipMsg="输入验证码" id="captcha" size="10" />                  
                </dd>
              </dl>
              <span><img src="/code?type=login&n=2" name="codeimage" id="codeimage">
                    <a class="makecode" href="javascript:void(0);">看不清，换一张</a></span>
            </div>
            <div class="handle-div">
            <span class="auto"><input type="checkbox" class="checkbox" name="auto_login" value="ok">七天自动登录<em style="display:none;">请勿在公用电脑上使用</em></span>
            <a class="forget" href="javascript:void(0);">忘记密码？</a></div>
            <div class="submit-div">
              <input type="submit" class="submit" value="登&nbsp;&nbsp;&nbsp;录">
            </div>
          </form>
        </div>
        
        <!-- 2.手机登录 -->
        <div id="mobile" class="tabs-content">
          <form id="post_form" method="post" class="nc-login-form">
            <dl>
              <dt>手机号：</dt>
              <dd>
                <input name="phone" type="text" class="text" id="phone" tipMsg="可填写已注册的手机号接收短信" autocomplete="off" value="" />              
              </dd>
            </dl>
            <div class="code-div">
              <dl>
                <dt>验证码：</dt>
                <dd>
                  <input type="text" name="captcha" class="text w100" tipMsg="输入验证码" id="image_captcha" size="10" />                 
                </dd>
              </dl>
              <span><img src="#" title="看不清，换一张" name="codeimage" id="codeimage" />
              <a class="makecode" href="javascript:void(0);">看不清，换一张</a></span>
            </div>
            <div class="tiptext" id="sms_text">
            	正确输入上方验证码后，点击<span><a href="javascript:void(0);" onClick="get_sms_captcha('2')"><i class="icon-mobile-phone"></i>发送手机动态码</a>
            	</span>，查收短信将系统发送的"6位手机动态码"输入到下方验证后登录。
            </div>
            <dl>
              <dt>动态码：</dt>
              <dd>
                <input type="text" name="sms_captcha" autocomplete="off" class="text" tipMsg="输入6位手机动态码" id="sms_captcha" size="15" />                
              </dd>
            </dl>
            <div class="submit-div">
                <input type="submit" id="submit" class="submit" value="登&nbsp;&nbsp;&nbsp;录" />
            </div>
          </form>
        </div>
     </div>
    </div>
    <div class="nc-login-api" id="demo-form-site">
        <h4>使用合作网站账号登录：</h4>
        <a href="javascript:void(0);" title="QQ账号登录" class="qq"><i></i></a>
        <a href="javascript:void(0);" title="新浪微博账号登录" class="sina"><i></i></a>
        <a href="javascript:void(0);" title="微信账号登录" class="wx"><i></i></a>
    </div>
  </div>
  <div class="clear"></div>
</div>
<style type="text/css">  
#loadpagediv{width:160px;height:56px;position:absolute;top:50%;left:50%;margin-left:-80px;margin-top:-28px;
background: url(/images/ico_loading.gif) no-repeat;z-index:9999;display:none}
</style>
<div id="loadpagediv"></div>
<script type="text/javascript">
var redirectUrl = "${redirect }";
$(function(){
	//初始化input的灰色提示信息   引入taglibs.js
	$('input[tipMsg]').inputTipText({pwd:'password'});
	
	//登录方式切换  引入tabulous.js
	$('.nc-login-mode').tabulous({
		 effect: 'flip' //动画反转效果
	});	
	var div_form = '#default';
	$(".nc-login-mode .tabs-nav li a").click(function(){
        if($(this).attr("href") !== div_form){
            div_form = $(this).attr('href');
            $(""+div_form).find(".makecode").trigger("click");
    	}
	});
	//Code 双切换
	$("#default .makecode").click(function(){
		$("#default #codeimage").prop("src","/code?type=login&n=" + Math.random());
	});
	$("#mobile .makecode").click(function(){
		$("#mobile #codeimage").prop("src","/code?type=login&n=" + Math.random());
	});	
	
	//validate自定义验证方法: mobile
	jQuery.validator.addMethod("mobile", function(value, element) {
		return this.optional(element) || /^0?(13|15|17|18|14|19|16)[0-9]{9}$/.test(value);
	}, "请输入正确的手机号");
	
    //勾选自动登录显示隐藏文字
    $('input[name="auto_login"]').click(function(){
        if ($(this).attr('checked')){
            $(this).attr('checked', true).next().show();
        } else {
            $(this).attr('checked', false).next().hide();
        }
    });
    
	//登录表单验证
	$("#login_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
    	submitHandler:function(form){
    		$('#loadpagediv').show();
    		$.post("/user/login", $("#login_form").serialize(), function(data){
    			$('#loadpagediv').hide();
				if(data.status == 200){
					//alert('登录成功!');
					if (redirectUrl == "" && data.msg != "") {
						location.href = data.msg;     //跳转后台 /shop/toMain /manage/toMain
					} else {
						location.href = redirectUrl;  //回调时走这里...						
					}
				} else {
					alert(data.msg);
				}
			});
    		// form.submit();
    	},
        onkeyup: false,
		rules: {
			user_name: "required",
			password: "required",
			captcha : {
                required : true,
                remote   : {
                    url : '/check/captcha/4',
                    type:'get',
                    dataType: "json",
                    data:{
                        captcha : function(){
                            return $('#captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	$("#default #codeimage").prop("src","/code?type=login&n=" + Math.random());
                        }
                    }
                }
            }
		},
		messages: {
			user_name: "<i class='icon-exclamation-sign'></i>请输入已注册的用户名或手机号",
			password: "<i class='icon-exclamation-sign'></i>密码不能为空",
			captcha : {
                required : '<i class="icon-remove-circle" title="验证码不能为空"></i>',
				remote	 : '<i class="icon-remove-circle" title="验证码不能为空"></i>'
            }
		}
	});
});
</script>
<script type="text/javascript" src="/static/shop/js/connect_sms.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
	$("#post_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd');
            error_td.append(error);
            element.parents('dl:first').addClass('error');
        },
        success: function(label) {
            label.parents('dl:first').removeClass('error').find('label').remove();
        },
    	submitHandler:function(form){
    		alert($("#post_form").serialize());
    		$.post("/user/tel_login", $("#post_form").serialize(), function(data){
				if(data.status == 200){
					alert('手机登录成功!');
					if (redirectUrl == "" && data.msg != "") {
						location.href = data.msg;     //跳转后台 /shop/toMain /manage/toMain
					} else {
						location.href = redirectUrl;  //回调时走这里...						
					}
				} else {				
					alert(data.msg);
				}
			});
    		// form.submit();
    	},
        onkeyup: false,
		rules: {
			phone: {
                required : true,
                mobile : true
            },
			captcha : {
                required : true,
                minlength: 4,
                remote   : {
                    url : '/check/captcha/4',
                    type:'get',
                    dataType: "json",
                    data:{
                        captcha : function(){
                            return $('#image_captcha').val();
                        }
                    },
                    complete: function(data) {
                        if(data.responseText == 'false') {
                        	$("#mobile #codeimage").prop("src","/code?type=login&n=" + Math.random());
                        }
                    }
                }
            },
			sms_captcha: {
                required : function(element) {
                    return $("#image_captcha").val().length == 4;
                },
                minlength: 6
            }
		},
		messages: {
			phone: {
                required : '<i class="icon-exclamation-sign"></i>请输入正确的手机号',
                mobile : '<i class="icon-exclamation-sign"></i>请输入正确的手机号'
            },
			captcha : {
                required : '<i class="icon-remove-circle" title="请输入正确的验证码"></i>',
                minlength: '<i class="icon-remove-circle" title="请输入正确的验证码"></i>',
				remote	 : '<i class="icon-remove-circle" title="请输入正确的验证码"></i>'
            },
			sms_captcha: {
                required : '<i class="icon-exclamation-sign"></i>请输入六位短信动态码',
                minlength: '<i class="icon-exclamation-sign"></i>请输入六位短信动态码'
            }
		}
	});
});
</script>
<div id="cti">
  <div class="wrapper">
    <ul>
    </ul>
  </div>
</div>
<div id="footer" class="wrapper">
  <p><a href="/">首页</a>
   | <a href="#">招聘英才</a>
   | <a href="#">合作及洽谈</a>
   | <a href="#">联系我们</a>
   | <a href="#">关于ShopX5</a>
  </p>
  Copyright 2010-2018 ShopX5 Inc.,All rights reserved.<br />
  Powered by <a href="/" target="_blank"><span class="vol"><font class="b">Shop</font><font class="o">X5</font></span></a> 
  <br />
</div>
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<form class="layui-form itemForm" id="itemeAddForm" style="margin:0;padding:0;color:#333">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:450px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">选择类目</label>
			<div class="layui-input-block">
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_add');" value="选择" />
		     <input type="hidden" name="categoryId" id="categoryId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">广告标题</label>
			<div class="layui-input-block">
			  <input type="text" name="title" placeholder="请输入广告标题" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">子标题</label>
			<div class="layui-input-block">
			  <input type="text" name="subTitle" placeholder="请输入子标题" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">URL</label>
			<div class="layui-input-block">
			  <input type="text" name="url" placeholder="请输入URL" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">图片</label>
			<div class="layui-input-block">
				<input type="hidden" name="pic" id="pic" />
				<input type="button" class="topjui-btn-blue layui-btn-sm" onclick="upImage('pic');" value="上传图片" />
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">图片2</label>
			<div class="layui-input-block">
				<input type="hidden" name="pic2" id="pic2" />
				<input type="button" class="topjui-btn-blue layui-btn-sm" onclick="upImage('pic2');" value="上传图片" />
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">简介</label>
			<div class="layui-input-block"style="height:70px" >
				<textarea name="titleDesc" placeholder="请输入内容" style="min-height:70px" class="layui-textarea"></textarea>
			</div>
		  </div>
</div>
<div style="width:280px;float:left">
</div>
		  <div class="layui-form-item layui-form-text">
			<label class="layui-form-label">内容</label>
			<div class="layui-input-block" style="width:630px;padding:0 0 30px 0">
			  <textarea name="content" id="container" placeholder="请输入内容"></textarea>
			</div>
		  </div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script id="UeditorAdd" type="text/plain"></script>
<script type="text/javascript">
$(function(){
	//layui渲染
/* 	layui.use('form', function() {
		var form = layui.form;
		form.render();
	}) */
})
	//主富文本
	var ueAdd = UE.getEditor('container',{
        toolbars: [['fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript',
                    'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
                    'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc']],                    
        autoClearinitialContent:true, //focus时自动清空初始化时的内容           
        elementPathEnabled:false,     //关闭元素路径
        wordCount:false,              //关闭字数统计
        initialFramewidth:630,        //编辑区域宽度
        initialFrameHeight:150,       //编辑区域高度
        //allowDivTransToP:true       //允许进入编辑器的div标签自动变成p标签
	});	
	//监听图片上传
	var imgId;
    var _ueAdd = UE.getEditor('UeditorAdd');
    _ueAdd.ready(function() {
        //设置编辑器不可用
        //_ueAdd.setDisabled();  这个地方要注意 一定要屏蔽
        //隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
        _ueAdd.hide();
        //侦听图片上传
        _ueAdd.addListener('beforeinsertimage', function (t, arg) {
            //将地址赋值给相应的input,只去第一张图片的路径
            var imgs = "";
            for(var a in arg){
            	if(a == arg.length-1){
            		imgs += arg[a].src;
            		break;
            	}
            	imgs += arg[a].src+',';
            }
            $("#manager_add #"+imgId).attr("value", arg[0].src); // 或 imgs
            //图片预览
            $("#manager_add #"+imgId).next("img").remove();
            for (var i = arg.length-1; i >= 0 ; i--) {
				$("#manager_add #"+imgId).after($("<img width='100' height='66' />").attr("src", arg[i].src));
			};
        });
    });
    //弹出图片上传对话框
    function upImage(id) {
    	imgId = id;
        var myImage = _ueAdd.getDialog("insertimage");
		myImage.open();
    };
    
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			title : "required",
			url   : "required",
			pic   : "required",
		},
		messages: {
			itle  : "<dl style='color:red'>请输入广告标题</dl>",
			url   : "<dl style='color:red'>请输入广告链接</dl>",
			pic   : "<dl style='color:red'>请输入广告图片</dl>",
		}
	})

</script>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="renderer" content="webkit" />
    <title>广告分类+广告信息</title>
    <meta name="keywords" content='' />
    <meta name="description" content='' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- UEditor编辑器  -->
    <link type="text/css" href="/cpts/ueditor/themes/default/css/ueditor.css" rel="stylesheet" />
	<script type="text/javascript"charset="utf-8" src= "/cpts/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/ueditor.all.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/lang/zh-cn/zh-cn.js"></script>
	<!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
	<!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
	<!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div class="easyui-panel" title="Nested Panel" data-options="width:'100%',minHeight:500,noheader:true,border:false" style="padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'west',split:false" style="width:176px;padding:5px">
       	 	<ul id="contentCategoryTree"></ul>                    <!-- 对应下面1. 先初始,加载内容类目列表 -->
        </div>
        <div data-options="region:'center'" style="padding:5px">
			<table id="productDg"></table>                        <!-- datagrid数据  对应下面2. 加id查询id 信息列表 -->
        </div>
    </div>
</div>

<!--2.表格工具栏开始----------------->
<div id="productDg-toolbar" style="padding:3px 0 3px 2px;display:none">
<c:if test="${QXmap.add == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true," onclick="toolbar.add();">新增</a>
</c:if>
<c:if test="${QXmap.edit == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-pencil',plain:true," onclick="toolbar.edit();">编辑</a>
</c:if>
<c:if test="${QXmap.del == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="iconCls:'fa fa-trash',plain:true," onclick="toolbar.remove();">删除</a>
</c:if>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-orange" data-options="iconCls:'fa fa-cloud-download',plain:true,">导入</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-red" data-options="iconCls:'fa fa fa-cloud-upload',plain:true,">导出</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-link',plain:true,">新窗口</a>
<a href="javascript:void(0);" class="easyui-menubutton topjui-btn-blue"  data-options="menu:'#exportSubMenu',iconCls:'fa fa-list',plain:true,">更多</a>
<div id="exportSubMenu" class="topjui-toolbar" style="width:150px">
	<c:if test="${QXmap.fromExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导入EXCEL列表</div></c:if>
	<c:if test="${QXmap.toExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导出EXCEL报表</div></c:if>
	<c:if test="${QXmap.email == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发邮件</div></c:if>
	<c:if test="${QXmap.sms == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发短信</div></c:if>
</div>
<div style="padding:5px;color:#333">
	查询标题：<input type="text" name="user" class="textbox" style="width:110px">
	创建时间从：<input type="text" name="date_from" class="easyui-datebox" editable="false" style="width:110px">
	到：               <input type="text" name="date_to" class="easyui-datebox" editable="false" style="width:110px">
	<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-search'" onclick="toolbar.search();">查询</a>
	<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.reload();">刷新</a>
	<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.redo();">取消选中</a>
</div>
</div>
<script type="text/javascript">
$(function(){
	var tree = $("#contentCategoryTree");
	var datagrid = $("#productDg");
	tree.tree({                                   // 初始化 加载内容分类
		url:'/manage/contentCategory/treeList',
		method:'GET',
		animate:true,
		onLoadSuccess : function (node, data) {   // 数据加载成功后触发, 节点展开,触发请求
			if (data) {
				$(data).each(function (index, value) {
					if (this.state == 'closed') {
						$('#contentCategoryTree').tree('expandAll');//展开所有节点
					}
				});
			}
		},
		onClick : function(node){                 // node.target参数是一个节点DOM对象
			if(tree.tree("isLeaf", node.target)){ // 点击到isLeaf是叶子节点,右侧数据表,重新加载 categoryId : node.id
				datagrid.datagrid('reload', {
					categoryId : node.id          // 右侧datagrid重加载
		        });
			}
		}
	});
	
//--1 初始化加载 content所有数据 --------------
	$('#productDg').datagrid({
		fit : true,         //面板大小将自适应父容器
		striped : true,     //斑马线
		rownumbers : true,  //行号
		border : false,		//面板边框
		url : '/manage/content/findGridPage',
		fitColumns : true,  //固定列   若设true,下面属性width:100,自动平均分配
		columns : [[        //纵列
   			{	field : 'id',
   				title : '广告ID',
   				checkbox : true,
   			},
   			{	field : 'categoryName',
   				title : '类目',
   				sortable : true,   //排序
   				width : 122,
   			},
   			{	field : 'title',
   				title : '标题',
   				sortable : true,   //排序
   				width : 100,
   			},
			{	field : 'url',
				title : 'URL',
				width : 100,
				sortable : true,
			},
			{	field : 'pic',
				title : '图片',
				width : 100,
				sortable : true,
				formatter:function (value, row, index) {
					if(value=="" || value==null){
						return null;
					}
			        return htmlstr  = '<img src="'+value+'" width="75" height="50" />';
				}
			},
			{	field : 'pic2',
				title : '图片2',
				width : 100,
				sortable : true,
				formatter:function (value, row, index) {
			        if(value=="" || value==null){
						return null;
					}
			        return htmlstr  = '<img src="'+value+'" width="75" height="50" />';
				}
			},
			{	field : 'subTitle',
				title : '子标题',
				width : 100,
				sortable : true,
			},
			{	field : 'titleDesc',
				title : '简介',
				width : 100,
				sortable : true,
			},
			{	field : 'operate',
				title : '操作',
				width : 100,
				formatter : function (value, row, index) { //单元格格式化函数，三参数：value值,row对象,index索引
			        var htmlstr  = '<c:if test="${QXmap.edit == 1 }"><button class="bjm layui-btn layui-btn-xs" onclick="toolbar.edit(\'' + row.id + '\')">编辑</button></c:if>';
                    	htmlstr += '<c:if test="${QXmap.del == 1 }"><button class="layui-btn layui-btn-xs layui-btn-danger" onclick="toolbar.remove(\'' + row.id + '\')">删除</button></c:if>';
		        	return htmlstr;
				}
			},
		]],
		toolbar : '#productDg-toolbar', //工具栏
		pagination : true,              //分页
		pageSize : 10,
		pageList : [10, 20, 30],
		pageNumber : 1,
		sortName : 'updated',      //初始化请求
		sortOrder : 'desc',
	});
	
	//--2 新增信息  --------------
	$('#manager_add').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '新增信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,                   //注意:初始窗口加载,设ture窗口关闭状态*
		iconCls : 'fa fa-windows',
		href : '/temp/manager/content/content_add', //<body> js,id不重复
		onOpen : function(){             //打开面板后
			$(".l-btn-text:contains('保存')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '保存',
			iconCls : 'fa fa-plus',
			handler : function() { //点击后,执行程序
		//	if ($('#itemeAddForm').form('validate')) { //form表单验证 *
			if ($('#itemeAddForm').valid()){
					//提交表单
					$.ajax({
						type : 'post',
						async : false,
						url : '/manage/content/add',
						data : $("#itemeAddForm").serialize(),
						beforeSend : function() {
							$.messager.progress({
								text : '正在新增中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : data.data + '！', //增加成功!
								});
								$('#manager_add').dialog('close');
								$('#itemeAddForm').form('reset'); //重置表单数据
								$('#itemeAddForm #szbm').next('span').remove(); $('#itemeAddForm #id').val("");
								ueAdd.execCommand('cleardoc');       //ueditor清空内容
								ueAdd.execCommand('clearlocaldata'); //ueditor清空草稿箱
								$('#productDg').datagrid('reload');
							} else {
								$.messager.alert('新增失败！', '未知错误导致失败，请重试！', 'warning');
							}
						},dataType:"json"
					})
				}
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_add').dialog('close'); //关闭窗口
			},
		}],
		onClose : function(){ //窗口关闭后
			//新增成功前,保留表单数据
		}
	});
	
	//--3 编辑信息  --------------
	$('#manager_edit').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '修改信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,
		iconCls : 'fa fa-windows',
		onOpen : function(){
			$(".l-btn-text:contains('更新')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '更新',
			iconCls : 'fa fa-save',
			handler : function() {
			//	if ($('#itemeEditForm').form('validate')) { //form表单验证
				if ($('#itemeEditForm').valid()){	
					//提交更新
					$.ajax({
						url : '/manage/content/update',
						type : 'post',
						data : $("#itemeEditForm").serialize(),
						beforeSend : function() {							
							$.messager.progress({
								text : '正在修改中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : '修改1个信息成功!',
								});
								$('#productDg').datagrid('reload');
								$('#manager_edit').dialog('close');
							} else {
								$.messager.alert('修改失败！', '未知错误或没有任何修改，请重试！', 'warning');
							}
						}
					});
				};
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_edit').dialog('close');
			},
		}],
		onClose : function(){
			$('#itemeEditForm').form('reset');
			$('#itemeEditForm #szbm').next('span').remove(); $('#itemeEditForm #id').val("");
			ueAdd.execCommand('cleardoc');       //ueditor清空内容
			ueAdd.execCommand('clearlocaldata'); //ueditor清空草稿箱
		}
	});
})
//---------------------------------------------------------------------------------------------------------------------------------
//	var showRole_userId = ""; // showModule : function(ID){ showRole_userId = ID; }
	toolbar = {
		search : function() {
			$('#productDg').datagrid('load', {                   //load方法 加载和显示第一页的所有行
				title : $.trim($('input[name="user"]').val()),   //加载时额外添加这些字段
				date_from : $('input[name="date_from"]').val(),
				date_to : $('input[name="date_to"]').val(),
			});
		},
		reload : function(){
			$('#productDg').datagrid('reload');
		},
		redo : function(){
			$('#productDg').datagrid('unselectAll');
		},
		add : function(){
			$("#pic1").next("img").remove();
			$("#pic2").next("img").remove();
			//打开add窗口
			$('#manager_add').dialog('open');
			//$("#itemeAddForm input[name='name']").focus();
	    	$(".panel").css("z-index","999");
			$(".window-shadow").css("z-index","998");
			$(".window-mask").css("z-index","997");
		},
		edit : function (logId) {
			var rows = $('#productDg').datagrid('getSelections'); //获取所有被选中行,>1警告,=1处理,=0错误
			if (rows.length > 1 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录只能选定一条数据！', 'warning');
				return ;
			};
			if (rows.length == 0 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录至少选定一条数据！', 'warning');
				return ;
			};
			if (rows.length == 1 || logId != null) {
				if(logId != null){
					id = logId;
				}else{
					id = rows[0].id;
				}
				$('#productDg').datagrid('unselectAll');
				$.ajax({
					url : '/manage/content/getOne',
					type : 'post',
					data : {"id" : id},
					beforeSend : function() {
						$.messager.progress({
							text : '正在获取中...',
						});
					},
					success : function (data, response, status) {
						$.messager.progress('close');
						if (data) {
							//-- 1 加载分类
							$.post("/manage/contentCategory/getOne", {"id" : data.data.categoryId}, function(data) {
									if (data.status == 200) {
										$('#itemeEditForm #szbm').next('span').remove();
										$('#itemeEditForm #szbm').after('<span style="margin-left:10px">'+data.data.name+'</span>');
									}
								}, "json"
							)
							//-- 2 加载图片
							if(data.data.pic=="" || data.data.pic==null){
								$("#pic").next("img").remove();
							}else{
								$("#pic").next("img").remove();
								$("#pic").after($("<img width='100' height='66' />").attr("src", data.data.pic));
							}
							if(data.data.pic2=="" || data.data.pic2==null){
								$("#pic2").next("img").remove();
							}else{
								$("#pic2").next("img").remove();
								$("#pic2").after($("<img width='100' height='66' />").attr("src", data.data.pic2));
							}
							//-- 3 内容
							ueEdit.setContent(data.data.content);
							//--加载user属性数据 打开对话框
							$('#manager_edit').form('load', data.data);
							//layuiShow(); //layui渲染 回显数据
							$('#manager_edit').dialog('open');
							$(".panel").css("z-index","999");
							$(".window-shadow").css("z-index","998");
							$(".window-mask").css("z-index","997");	
						} else {
							$.messager.alert('获取失败！', '未知错误导致失败，请重试！', 'warning');
						};
					}
				})
			};
		},
		remove : function (id) {
		alert('权限限制!');return;//***---+++
			var rows = $('#productDg').datagrid('getSelections');
			if (rows.length > 0 || id != null) {
				$.messager.confirm('确定操作', '您正在要删除所选的记录吗？', function (flag) { //确定执行删除
					if (flag) {
						var ids = [];
						for (var i = 0; i < rows.length; i ++) {
							ids.push(rows[i].id);
						}
						if(id != null){
							sid = id;
						}else{
							sid = ids.join(',');
						}
						$.ajax({
							type : 'POST',
							url : '/manage/content/delete',
							data : { "ids" : sid },
							beforeSend : function() {
								$('#productDg').datagrid('loading');
							},
							success : function (data) {
								if (data) {
									$('#productDg').datagrid('loaded');
									$('#productDg').datagrid('load'); //首列
									$.messager.show({
										title : '提示',
										msg : data.data + '！', //删除成功！
									});
								}
							},
						});
					};
				});
			} else {
				$.messager.alert('提示', '请选择要删除的记录！', 'info');
			};
		},
	};
</script>

<!-- /////////////////////////////////////////////////////////// -->

<!-- add块 -->
<div id="manager_add" style="display:none"></div>

<!-- edit块 -->
<div id="manager_edit" style="padding:3px 0 0 20px;display:none">
<form class="layui-form itemForm" id="itemeEditForm" method="post">
	<input type="hidden" name="id" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:450px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">选择类目</label>
			<div class="layui-input-block">
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_edit');" value="选择" />
		     <input type="hidden" name="categoryId" id="categoryId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">广告标题</label>
			<div class="layui-input-block">
			  <input type="text" name="title" placeholder="请输入广告标题" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">子标题</label>
			<div class="layui-input-block">
			  <input type="text" name="subTitle" placeholder="请输入子标题" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">URL</label>
			<div class="layui-input-block">
			  <input type="text" name="url" placeholder="请输入URL" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">图片</label>
			<div class="layui-input-block">
				<input type="hidden" name="pic" id="pic" />
				<input type="button" class="topjui-btn-blue layui-btn-sm" onclick="upImageE('pic');" value="上传图片" />
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">图片2</label>
			<div class="layui-input-block">
				<input type="hidden" name="pic2" id="pic2" />
				<input type="button" class="topjui-btn-blue layui-btn-sm" onclick="upImageE('pic2');" value="上传图片" />
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">简介</label>
			<div class="layui-input-block"style="height:70px" >
				<textarea name="titleDesc" placeholder="请输入内容" style="min-height:70px" class="layui-textarea"></textarea>
			</div>
		  </div>
</div>
<div style="width:280px;float:left">
</div>
		  <div class="layui-form-item layui-form-text">
			<label class="layui-form-label">内容</label>
			<div class="layui-input-block" style="width:610px;padding:0 0 30px 0">
			  <textarea name="content" id="containerEdit" placeholder="请输入内容"></textarea>
			</div>
		  </div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script id="UeditorEdit" type="text/plain"></script>
<script type="text/javascript">
//--layui回显数据时渲染
/* function layuiShow(){
	layui.use('form', function() {
		var form = layui.form;
		form.render();
	})
} */
//validate校验
$("#itemeEditForm").validate({
	rules: {
		title : "required",
		url   : "required",
		pic   : "required",
	},
	messages: {
		itle  : "<dl style='color:red'>请输入广告标题</dl>",
		url   : "<dl style='color:red'>请输入广告链接</dl>",
		pic   : "<dl style='color:red'>请输入广告图片</dl>",
	}
})
//主富文本
	var ueEdit = UE.getEditor('containerEdit',{
        toolbars: [['fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript',
                    'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
                    'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc']],                    
        autoClearinitialContent:true, //focus时自动清空初始化时的内容           
        elementPathEnabled:false,     //关闭元素路径
        wordCount:false,              //关闭字数统计
        initialFramewidth:610,        //编辑区域宽度
        initialFrameHeight:150,       //编辑区域高度
        //allowDivTransToP:true       //允许进入编辑器的div标签自动变成p标签
	});	
	//监听图片上传
	var imgId;
    var _ueEdit = UE.getEditor('UeditorEdit');
    _ueEdit.ready(function() {
        //设置编辑器不可用
        //_ueEdit.setDisabled();  这个地方要注意 一定要屏蔽
        //隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
        _ueEdit.hide();
        //侦听图片上传
        _ueEdit.addListener('beforeinsertimage', function (t, arg) {
            //将地址赋值给相应的input,只去第一张图片的路径
            var imgs = "";
            for(var a in arg){
            	if(a == arg.length-1){
            		imgs += arg[a].src;
            		break;
            	}
            	imgs += arg[a].src+',';
            }
            $("#manager_edit #"+imgId).attr("value", arg[0].src); // 或 imgs
            //图片预览
            $("#manager_edit #"+imgId).next("img").remove();
            for (var i = arg.length-1; i >= 0 ; i--) {
				$("#manager_edit #"+imgId).after($("<img width='100' height='66' />").attr("src", arg[i].src));
			};
        });
    });
    //弹出图片上传对话框
    function upImageE(id) {
    	imgId = id;
        var myImage = _ueEdit.getDialog("insertimage");
		myImage.open();
    };
//取所在部门  3 tree  easyui
function getSzbm(aeid){
	$("<div>").css({padding:"5px"}).html("<ul></ul>").window({
		width : '300',
	    height : '380',
	    title : '选择所属部门',
	    iconCls : 'icon-save',
	    modal : true,
	    closed : true,
	    onOpen : function(){
	    	var _win = this;                                 //this <div><ul>_DOM
	    	$("ul", _win).tree({                             //子,父 父内找子
	    		url : '/manage/contentCategory/findContentCategoryEUtree',        //异步数据[id text state]
	    		animate : true,
	    		onClick : function(node){                    //点击父节点,发送请求,展开父节点*
	    			if($(this).tree("isLeaf", node.target)){ //点击是叶子节点,$(this)=$("ul")
	    				$('#'+aeid+' input[name="categoryId"]').val(node.id);
	    				$('#'+aeid+' #szbm').next('span').remove();
	    				$('#'+aeid+' #szbm').after('<span style="margin-left:10px">'+node.text+'</span>');
	    				$(_win).window('close');
	    			}
	    		}
	    	})
	    }
	}).window('open');
}
</script>
</div>
</body>
</html>






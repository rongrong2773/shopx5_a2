<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>地区tree树</title>
    <meta name="keywords" content='地区tree树' />
    <meta name="description" content='地区tree树' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div id="productDg-toolbar" style="padding:3px 0 3px 2px;display:none">
<c:if test="${QXmap.add == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true," onclick="toolbar.add();">新增</a>
</c:if>
<c:if test="${QXmap.edit == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-pencil',plain:true," onclick="toolbar.edit();">编辑</a>
</c:if>
<c:if test="${QXmap.del == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="iconCls:'fa fa-trash',plain:true," onclick="toolbar.remove();">删除</a>
</c:if>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-orange" data-options="iconCls:'fa fa-cloud-download',plain:true,">导入</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-red" data-options="iconCls:'fa fa fa-cloud-upload',plain:true,">导出</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-link',plain:true,">新窗口</a>
<a href="javascript:void(0);" class="easyui-menubutton topjui-btn-blue"  data-options="menu:'#exportSubMenu',iconCls:'fa fa-list',plain:true,">更多</a>
<div id="exportSubMenu" class="topjui-toolbar" style="width:150px">
	<c:if test="${QXmap.fromExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导入EXCEL列表</div></c:if>
	<c:if test="${QXmap.toExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导出EXCEL报表</div></c:if>
	<c:if test="${QXmap.email == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发邮件</div></c:if>
	<c:if test="${QXmap.sms == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发短信</div></c:if>
</div>
</div>
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center',title:'',fit:true,split:true,border:false" style="padding:0 0 30px 0">
        <table id="productDg"> </table>
	</div>
</div>

<script type="text/javascript">
$(function() {
	$('#productDg').treegrid({
		iconCls: 'icon-ok',
		fit : true,
		striped : true,     //斑马线
		rownumbers : true,  //行号
		animate: true,
		url : '/manage/area/findTreeGrid',
		idField : 'id',
		treeField : 'name',
		border : false,		//面板边框
		fitColumns : true,
		columns : [[
			{
				field : 'name',
				title : '地区',
				width : 100,
			},
			{
				field : 'shortName',
				title : '简称',
				width : 100,
			},
			{
				field : 'id',
				title : '地区编号',
				width : 100,
			//	checkbox : true,
			},
			{
				field : 'parentId',
				title : '父级编号',
				width : 100,
			},
			{
				field : 'lev',
				title : '层级',
				width : 100,
				formatter : function (value, row, index) {
					 if(value == 1) return '<span style="color:blue;">一级</span>';
				     if(value == 2) return '<span style="color:red;">二级</span>';
				     if(value == 3) return '三级';
				}
			},
			{
				field : 'code',
				title : '编码',
				width : 100,
			},
			{	field : 'operate',
				title : '操作',
				width : 100,
				formatter : function (value, row, index) { //单元格格式化函数，三参数：value值,row对象,index索引
					var htmlstr  = '<c:if test="${QXmap.edit == 1 }"><button class="bjm layui-btn layui-btn-xs" onclick="toolbar.edit(\'' + row.id + '\')">编辑</button></c:if>';
                    htmlstr += '<c:if test="${QXmap.del == 1 }"><button class="layui-btn layui-btn-xs layui-btn-danger" onclick="toolbar.remove(\'' + row.id + '\')">删除</button></c:if>';
		       		return htmlstr;
				}
			},
		]],
		toolbar : '#productDg-toolbar', //工具栏
		onLoadSuccess : function(){
			//$('#productDg').treegrid("collapseAll"); //全部折叠  expandAll展开
		},
		onBeforeExpand:function(row){ //在节点展开之前触发
            $('#productDg').treegrid('options').url = '/manage/area/findTreeGrid?parentId='+row.id;  //补充节点 异步url *           
        },
	});
	
	
	//--2 新增信息  --------------
	$('#manager_add').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '新增信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,                   //注意:初始窗口加载,设ture窗口关闭状态*
		iconCls : 'fa fa-windows',
		href : '/temp/manager/area/list_add', //<body> js,id不重复
		onOpen : function(){             //打开面板后
			$(".l-btn-text:contains('保存')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '保存',
			iconCls : 'fa fa-plus',
			handler : function() { //点击后,执行程序
		//	if ($('#itemeAddForm').form('validate')) { //form表单验证 *
			if ($('#itemeAddForm').valid()){
					//提交表单
					$.ajax({
						type : 'post',
						async : false,
						url : '/manage/area/add',
						data : $("#itemeAddForm").serialize(),
						beforeSend : function() {
							$.messager.progress({
								text : '正在新增中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : data.data + '！', //增加成功!
								});
								$('#manager_add').dialog('close');
								$('#itemeAddForm').form('reset'); //重置表单数据
								$('#itemeAddForm #szbm').next('span').remove(); $('#itemeAddForm #parentId').val("");
								$('#productDg').treegrid('reload');
							} else {
								$.messager.alert('新增失败！', '未知错误导致失败，请重试！', 'warning');
							}
						},dataType:"json"
					})
				}
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_add').dialog('close'); //关闭窗口
			},
		}],
		onClose : function(){ //窗口关闭后
			//新增成功前,保留表单数据
		}
	});
	
	//--3 编辑信息  --------------
	$('#manager_edit').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '修改信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,
		iconCls : 'fa fa-windows',
		onOpen : function(){
			$(".l-btn-text:contains('更新')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '更新',
			iconCls : 'fa fa-save',
			handler : function() {
			//	if ($('#itemeEditForm').form('validate')) { //form表单验证
				if ($('#itemeEditForm').valid()){
					//提交更新
					$.ajax({
						url : '/manage/area/update',
						type : 'post',
						data : $("#itemeEditForm").serialize(),
						beforeSend : function() {							
							$.messager.progress({
								text : '正在修改中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : '修改1个信息成功!',
								});
								$('#manager_edit').dialog('close');
								$('#productDg').treegrid('reload');
							} else {
								$.messager.alert('修改失败！', '未知错误或没有任何修改，请重试！', 'warning');
							}
						}
					});
				};
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_edit').dialog('close');
			},
		}],
		onClose : function(){
			$('#itemeEditForm').form('reset');
			$('#itemeEditForm #szbm').next('span').remove(); $('#itemeEditForm #parentId').val("");
		}
	});
})
//---------------------------------------------------------------------------------------------------------------------------------
//	var showRole_userId = ""; // showModule : function(ID){ showRole_userId = ID; }
	toolbar = {
		reload : function(){
			$('#productDg').treegrid('reload');
		},
		redo : function(){
			$('#productDg').treegrid('unselectAll');
		},
		add : function(){
			//打开add窗口
			$('#manager_add').dialog('open');
			//$("#itemeAddForm input[name='name']").focus();
		},
		edit : function (logId) {
			var rows = $('#productDg').treegrid('getSelections'); //获取所有被选中行,>1警告,=1处理,=0错误
			if (rows.length > 1 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录只能选定一条数据！', 'warning');
				return ;
			};
			if (rows.length == 0 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录至少选定一条数据！', 'warning');
				return ;
			};			
			if (rows.length == 1 || logId != null) {
				if(logId != null){
					id = logId;
				}else{
					id = rows[0].id;
				}
				$('#productDg').treegrid('unselectAll');
				$.ajax({
					url : '/manage/area/getOne',
					type : 'post',
					data : {"id" : id},
					beforeSend : function() {
						$.messager.progress({
							text : '正在获取中...',
						});
					},
					success : function (data, response, status) {
						$.messager.progress('close');
						if (data) {
							//--1 选择所在部门
							$.post("/manage/area/getOne", {"id" : data.data.parentId}, function(data) {
									if (data.status == 200) {
										$('#itemeEditForm #szbm').next('span').remove();
										$('#itemeEditForm #szbm').after('<span style="margin-left:10px">'+data.data.name+'</span>');
									}
								}, "json"
							)
							//--加载user属性数据 打开对话框
							$('#manager_edit').form('load', data.data).dialog('open');
						} else {
							$.messager.alert('获取失败！', '未知错误导致失败，请重试！', 'warning');
						};
					}
				})
			};
		},
		remove : function (id) {
		alert('权限限制!');return;//***---+++
			var rows = $('#productDg').treegrid('getSelections');
			if (rows.length > 0 || id != null) {
				$.messager.confirm('确定操作', '您正在要删除所选的记录吗？', function (flag) { //确定执行删除
					if (flag) {
						var ids = [];
						for (var i = 0; i < rows.length; i ++) {
							ids.push(rows[i].id);
						}
						if(id != null){
							sid = id;
						}else{
							sid = ids.join(',');
						}
						$.ajax({
							type : 'POST',
							url : '/manage/area/delete',
							data : { "ids" : sid },
							beforeSend : function() {
								$('#productDg').treegrid('loading');
							},
							success : function (data) {
								if (data) {
									$('#productDg').treegrid('loaded');
									$('#productDg').treegrid('load'); //首列
									$.messager.show({
										title : '提示',
										msg : data.data + '！', //删除成功！
									});
								}
							},
						});
					};
				});
			} else {
				$.messager.alert('提示', '请选择要删除的记录！', 'info');
			};
		},
	};
</script>

<!-- /////////////////////////////////////////////////////////// -->

<!-- add块 -->
<div id="manager_add" style="display:none"></div>
<!-- edit块 -->
<div id="manager_edit" style="padding:3px 0 0 20px;display:none">
<form class="layui-form itemForm" id="itemeEditForm" method="post">
	<input type="hidden" name="id" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">选择上级</label>
			<div class="layui-input-block">
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_edit');" value="选择" />
		     <input type="hidden" name="parentId" id="parentId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">名称</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入名称" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">简称</label>
			<div class="layui-input-block">
			  <input type="text" name="shortName" placeholder="请输入简称" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">层级</label>
			<div class="layui-input-block">
			  <input type="text" name="lev" placeholder="请输入层级" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">编码</label>
			<div class="layui-input-block">
			  <input type="text" name="code" placeholder="请输入编码" autocomplete="off" class="layui-input">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
//validate校验
$("#itemeAddForm").validate({
	rules: {
		name : "required",
	},
	messages: {
		name : "<dl style='color:red'>请输入名称</dl>",
	}
})
//取所在部门  3 tree  easyui
function getSzbm(aeid){
	$("<div>").css({padding:"5px"}).html("<ul></ul>").window({
		width : '300',
	    height : '380',
	    title : '选择上级区域',
	    iconCls : 'icon-save',
	    modal : true,
	    closed : true,
	    onOpen : function(){
	    	var _win = this;                                 //this <div><ul>_DOM
	    	$("ul", _win).tree({                             //子,父 父内找子
	    		url : '/manage/area/findAreaEUtree',        //异步数据[id text state]
	    		animate : true,
	    		onClick : function(node){                    //点击父节点,发送请求,展开父节点*
	    		//	if($(this).tree("isLeaf", node.target)){ //点击是叶子节点,$(this)=$("ul")
	    				$('#'+aeid+' input[name="parentId"]').val(node.id);
	    				$('#'+aeid+' #szbm').next('span').remove();
	    				$('#'+aeid+' #szbm').after('<span style="margin-left:10px">'+node.text+'</span>');
	    				$(_win).window('close');
	    		//	}
	    		}
	    	})
	    }
	}).window('open');
}
</script>
</div>
</body>
</html>







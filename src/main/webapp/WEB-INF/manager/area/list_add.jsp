<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<form class="layui-form itemForm" id="itemeAddForm" style="margin:0;padding:0;color:#333">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">上级区域</label>
			<div class="layui-input-block">
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_add');" value="选择" />
		     <input type="hidden" name="parentId" id="parentId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">名称</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入名称" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">简称</label>
			<div class="layui-input-block">
			  <input type="text" name="shortName" placeholder="请输入简称" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">层级</label>
			<div class="layui-input-block">
			  <input type="radio" name="layerNum" value="1" title="一层">
			  <input type="radio" name="layerNum" value="2" title="二层">
              <input type="radio" name="layerNum" value="3" title="三层">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">编码</label>
			<div class="layui-input-block">
			  <input type="text" name="code" placeholder="请输入编码" autocomplete="off" class="layui-input">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
$(function(){
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			name : "required",
		},
		messages: {
			name : "<dl style='color:red'>请输入名称</dl>",
		}
	})
})
</script>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<form class="layui-form itemForm" id="itemeAddForm" style="margin:0;padding:0;color:#333">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">所在部门</label>
			<div class="layui-input-block">
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_add');" value="选择" />
		     <input type="hidden" name="parentId" id="parentId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">部门名称</label>
			<div class="layui-input-block">
			  <input type="text" name="deptName" placeholder="请输入名称" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">状态</label>
			<div class="layui-input-block">
			  <input type="checkbox" name="stateON" checked lay-skin="switch">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">排序号</label>
			<div class="layui-input-block">
			  <input type="text" name="orderNo" placeholder="请输入排序号" autocomplete="off" class="layui-input">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
$(function(){
	//layui渲染
	layui.use('form', function() {
		var form = layui.form;
		form.render();
	})
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			deptName : "required",
			orderNo  : "digits",
		},
		messages: {
			deptName : "<dl style='color:red'>请输入名称</dl>",
			orderNo  : "<dl style='color:red'>请输入输入整数</dl>",
		}
	})
})
</script>



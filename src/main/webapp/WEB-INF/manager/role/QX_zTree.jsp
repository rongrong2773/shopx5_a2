<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/cpts/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css" />
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script type="text/javascript">
$(function() {
	//var userId = ${id };    //1 写回数据  可$('#roleQXListForm #roleId').val()
	//alert(showRole_userId); //2 父层预定义
	$.post("/manage/role/findRoleQXZtree", {"id":$('#roleQXListForm #roleId').val(), "curdQX":$('#curdQX').val()}, function(data) {
			if (data) {
				zTreeObj_QX = $.fn.zTree.init($('#QXTree'), setting_module, data);	//jkTree 树的id，支持多个树
				zTreeObj_QX.expandAll(true);									    //展开所有树节点
			}
		}, "json"
	)
})
var zTreeObj_QX;
var setting_module = {
	check : { enable : true	},
	data  : { simpleData : {enable:true} }
};
//获取所有选择的节点
function QXCheckedNodes() {
	var nodes = new Array();
	nodes = zTreeObj_QX.getCheckedNodes(true);	//取得选中的结点
	var str = "";
	for (i = 0; i < nodes.length; i++) {
		if (str != "") {
			str += ",";
		}
		str += nodes[i].id;
	}
	$("#roleQXListForm #moduleIds").val(str);
	//保存数据
	$.post("/manage/role/updateQX", $("#roleQXListForm").serialize(), function(data) {
			if (data.status == 200) {
				$.messager.show({ title:'提示', msg:'设置成功!',	});
			}
		}, "json"
	)
	$('#QXDialog').dialog('close');
}
</script>
	
	<div style="padding:5px 0 0 0">
	<form id="roleQXListForm">
		<ul id="QXTree" class="ztree"></ul>
		<input type="hidden" name="roleId"  id="roleId"  value="${id }" />
		<input type="hidden" name="moduleIds" id="moduleIds" value="" />
		<input type="hidden" name="curdQX" id="curdQX"  value="${curdQX }" />
		<a href="javascript:void(0);" class="layui-btn layui-btn-xs" style="margin:10px 0 20px 38px" onclick="QXCheckedNodes();">保存</a>
		<a href="javascript:void(0);" class="layui-btn layui-btn-xs layui-btn-danger"
			onclick="$('#QXDialog').dialog('close');" style="margin:10px 0 20px 10px">取消</a>
	</form>	 
	</div>		 
		 
		 
		 
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/cpts/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css" />
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script type="text/javascript">
$(function() {
	//var userId = ${id };    //1 写回数据  可$('#roleModuleListForm #roleId').val()
	//alert(showRole_userId); //2 父层预定义
	$.post("/manage/role/findModuleZtree", {"id":$('#roleModuleListForm #roleId').val()}, function(data) {
			if (data) {
				zTreeObj_module = $.fn.zTree.init($('#moTree'), setting_module, data);	//jkTree 树的id，支持多个树
				zTreeObj_module.expandAll(true);									    //展开所有树节点
			}
		}, "json"
	)
})
var zTreeObj_module;
var setting_module = {
	check : { enable : true	},
	data  : { simpleData : {enable:true} }
};
//获取所有选择的节点
function moduleCheckedNodes() {
	var nodes = new Array();
	nodes = zTreeObj_module.getCheckedNodes(true);	//取得选中的结点
	var str = "";
	for (i = 0; i < nodes.length; i++) {
		if (str != "") {
			str += ",";
		}
		str += nodes[i].id;
	}
	$("#roleModuleListForm #moduleIds").val(str);
	//保存数据
	$.post("/manage/roleModule/add", $("#roleModuleListForm").serialize(), function(data) {
			if (data.status == 200) {
				$.messager.show({ title:'提示', msg:'设置成功!',	});
			}
		}, "json"
	)
	$('#ModuleDialog').dialog('close');
}
</script>

	<div style="padding:5px 0 0 0">
	<form id="roleModuleListForm">
		<ul id="moTree" class="ztree"></ul>
		<input type="hidden" name="roleId"  id="roleId"  value="${id }" />
		<input type="hidden" name="moduleIds" id="moduleIds" value="" />
		<a href="javascript:void(0);" class="layui-btn layui-btn-xs" style="margin:10px 0 20px 38px" onclick="moduleCheckedNodes();">保存</a>
		<a href="javascript:void(0);" class="layui-btn layui-btn-xs layui-btn-danger"
			onclick="$('#ModuleDialog').dialog('close');" style="margin:10px 0 20px 10px">取消</a>
	</form>	 
	</div>		 
		 
		 
		 
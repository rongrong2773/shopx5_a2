<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<form class="layui-form itemForm" id="itemeAddForm" style="margin:0;padding:0;color:#333">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0;margin-left:-10px;"><!-- tabs1 -->
<div style="width:360px;float:left">
          <div class="layui-form-item">
			<label class="layui-form-label">父模块</label>
			<div class="layui-input-block">
             <!-- <select name="deptId">
               <option value="">--请选择--</option>
             </select> -->
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_add');" value="选择" />
		     <input type="hidden" name="parentId" id="parentId" />
		     <input type="hidden" name="parentName" id="parentName" />
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">模块名</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入模块名" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">层数</label>
			<div class="layui-input-block">
			   <input type="radio" name="layerNum" value="1" title="一层">
			  <input type="radio" name="layerNum" value="2" title="二层">
              <input type="radio" name="layerNum" value="3" title="三层">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">权限标识</label>
			<div class="layui-input-block">
			  <input type="text" name="cpermission" placeholder="请输入权限标识" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">链接</label>
			<div class="layui-input-block">
			  <input type="text" name="curl" placeholder="请输入链接" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">类型</label>
			<div class="layui-input-block">
			  <input type="radio" name="ctype" value="1" title="主菜单">
			  <input type="radio" name="ctype" value="2" title="左侧菜单">
              <input type="radio" name="ctype" value="3" title="按钮">
              <input type="radio" name="ctype" value="4" title="链接">
              <input type="radio" name="ctype" value="5" title="状态">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">状态</label>
			<div class="layui-input-block">
			  <input type="checkbox" name="stateON" checked lay-skin="switch">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">从属</label>
			<div class="layui-input-block">
			  <input type="text" name="belong" placeholder="请输入从属" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">复用标识</label>
			<div class="layui-input-block">
			  <input type="text" name="cwhich" placeholder="请输入复用标识" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
				<label class="layui-form-label">排序号</label>
				<div class="layui-input-block">
				  <input type="text" name="orderNo" placeholder="请输入排序号" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item layui-form-text">
				<label class="layui-form-label">说明</label>
				<div class="layui-input-block">
				  <textarea name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
				</div>
			</div>
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
$(function(){
	//layui渲染
	layui.use('form', function() {
		var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
		form.render();         //渲染全部
	})
	
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			name : {required:true},
			curl : {required:true},
		},
		messages: {
			name : "<dl style='color:red'>请输入模块名</dl>",
			curl : "<dl style='color:red'>请输入模块链接</dl>",
		}
	});
})
</script>



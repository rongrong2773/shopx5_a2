<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>模型tree树</title>
    <meta name="keywords" content='模型tree树' />
    <meta name="description" content='模型tree树' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui --> 
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center',title:'',fit:true,split:true,border:false">
        <table id="productDg"> </table>
	</div>
</div>
<div id="productDg-toolbar" style="padding:3px 0 3px 2px;display:none">
<c:if test="${QXmap.add == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true," onclick="toolbar.add();">新增</a>
</c:if>
<c:if test="${QXmap.edit == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-pencil',plain:true," onclick="toolbar.edit();">编辑</a>
</c:if>
<c:if test="${QXmap.del == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="iconCls:'fa fa-trash',plain:true," onclick="toolbar.remove();">删除</a>
</c:if>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-orange" data-options="iconCls:'fa fa-cloud-download',plain:true,">导入</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-red" data-options="iconCls:'fa fa fa-cloud-upload',plain:true,">导出</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-link',plain:true,">新窗口</a>
<a href="javascript:void(0);" class="easyui-menubutton topjui-btn-blue"  data-options="menu:'#exportSubMenu',iconCls:'fa fa-list',plain:true,">更多</a>
<div id="exportSubMenu" class="topjui-toolbar" style="width:150px">
	<c:if test="${QXmap.fromExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导入EXCEL列表</div></c:if>
	<c:if test="${QXmap.toExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导出EXCEL报表</div></c:if>
	<c:if test="${QXmap.email == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发邮件</div></c:if>
	<c:if test="${QXmap.sms == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发短信</div></c:if>
</div>
<div style="padding:5px;color:#333">
	查询模块名：<input type="text" name="user" class="textbox" style="width:110px">
	创建时间从：<input type="text" name="date_from" class="easyui-datebox" editable="false" style="width:110px">
	到：               <input type="text" name="date_to" class="easyui-datebox" editable="false" style="width:110px">
	<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-search'" onclick="toolbar.search();">查询</a>
	<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.reload();">刷新</a>
	<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.redo();">取消选中</a>
</div>
</div>
<script type="text/javascript">
$(function() {
	$('#productDg').treegrid({
		iconCls: 'icon-ok',
		fit : true,
		striped : true,     //斑马线
		rownumbers : true,  //行号
		animate: true,
		url : '/manage/module/findTreeGrid',
		idField : 'moduleId',
		treeField : 'name',
		border : false,		//面板边框
		fitColumns : true,
		columns : [[
			{
				field : 'moduleId',
				title : '模块编号',
				width : 100,
				checkbox : true,
			},
			{
				field : 'name',
				title : '名称',
				width : 130,
			},
			{
				field : 'curl',
				title : '路径',
				width : 230,
			},
   			{	field : 'ctype',
   				title : '类型',
   				sortable : true,
   				width : 100,
   				formatter : function (value, row, index) {
					 if(value == 2) return '左侧菜单'; 
   					 if(value == 1) return '主菜单';
				     if(value == 3) return '按钮';
				     if(value == 4) return '链接';
				     if(value == 5) return '状态';
				}
   			},
   			{	field : 'layerNum',
   				title : '层数',
   				sortable : true,
   				width : 100,
   				formatter : function (value, row, index) {
					 if(value == 1) return '一层'; 
  					 if(value == 2) return '二层';
				     if(value == 3) return '三层';
				}
   			},
   			{	field : 'orderNo',
   				title : '排序',
   				sortable : true,
   				width : 100,
   			},
   			{	field : 'remark',
   				title : '说明',
   				sortable : true,
   				width : 100,
   			},
   			{	field : 'updateTime',
   				title : '更新时间',
   				sortable : true,
   				width : 100,
   				formatter:SHOPX5.formatDateTime,
   			},
			{	field : 'operate',
				title : '操作',
				width : 100,
				formatter : function (value, row, index) { //单元格格式化函数，三参数：value值,row对象,index索引
					var htmlstr  = '<c:if test="${QXmap.edit == 1 }"><button class="bjm layui-btn layui-btn-xs" onclick="toolbar.edit(\'' + row.moduleId + '\')">编辑</button></c:if>';
                    htmlstr += '<c:if test="${QXmap.del == 1 }"><button class="layui-btn layui-btn-xs layui-btn-danger" onclick="toolbar.remove(\'' + row.moduleId + '\')">删除</button></c:if>';
		       		return htmlstr;
				}
			},
		]],
		toolbar : '#productDg-toolbar', //工具栏
		onBeforeExpand:function(row){ //在节点展开之前触发
            $('#productDg').treegrid('options').url = '/manage/module/findTreeGrid?parentId='+row.moduleId;  //补充节点 异步url *           
        },
        sortName : 'orderNo',      //初始化请求
		sortOrder : 'asc',
		onLoadSuccess : function(){
			$('#productDg').treegrid("expand", 5);//全部collapseAll折叠  expandAll展开
		},
	});
	
	//--2 新增信息  --------------
	$('#manager_add').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '新增信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,                   //注意:初始窗口加载,设ture窗口关闭状态*
		iconCls : 'fa fa-windows',
		href : '/temp/manager/module/list_add', //<body> js,id不重复
		onOpen : function(){             //打开面板后
			$(".l-btn-text:contains('保存')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '保存',
			iconCls : 'fa fa-plus',
			handler : function() { //点击后,执行程序
				/* if($('#parentId').val() == ""){
					$.messager.alert('新增失败！', '请选择父模块！', 'warning');
					return;
				} */
		//	if ($('#itemeAddForm').form('validate')) { //form表单验证 *
			if ($('#itemeAddForm').valid()){
					//提交表单
					$.ajax({
						type : 'post',
						async : false,
						url : '/manage/module/add',
						data : $("#itemeAddForm").serialize(),
						beforeSend : function() {
							$.messager.progress({
								text : '正在新增中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : data.data + '！', //增加成功!
								});
								$('#manager_add').dialog('close');
								$('#itemeAddForm').form('reset'); //重置表单数据
								$('#itemeAddForm #szbm').next('span').remove();
								$('#itemeAddForm #parentId').val(""); $('#itemeAddForm #parentName').val("");
								$('#productDg').treegrid('reload');
								window.parent.generateMenu(0, null); //左导航重新加载
							} else {
								$.messager.alert('新增失败！', '未知错误导致失败，请重试！', 'warning');
							}
						},dataType:"json"
					})
				}
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_add').dialog('close'); //关闭窗口
			},
		}],
		onClose : function(){ //窗口关闭后
			//新增成功前,保留表单数据
		}
	});
	
	//--3 编辑信息  --------------
	$('#manager_edit').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '修改信息',
		width : 770,
		height : 430,
		left : 150,
		top : 70,
		modal : true,
		closed : true,
		iconCls : 'fa fa-windows',
		onOpen : function(){
			$(".l-btn-text:contains('更新')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '更新',
			iconCls : 'fa fa-save',
			handler : function() {
			//	if ($('#itemeEditForm').form('validate')) { //form表单验证
				if ($('#itemeEditForm').valid()){	
					//提交更新
					$.ajax({
						url : '/manage/module/update',
						type : 'post',
						data : $("#itemeEditForm").serialize(),
						beforeSend : function() {
							$.messager.progress({
								text : '正在修改中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : '修改1个信息成功!',
								});
								$('#manager_edit').dialog('close');
								$('#productDg').treegrid('reload');
								window.parent.generateMenu(0, null); //左导航重新加载
							} else {
								$.messager.alert('修改失败！', '未知错误或没有任何修改，请重试！', 'warning');
							}
						}
					});
				};
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_edit').dialog('close');
			},
		}],
		onClose : function(){
			$('#itemeEditForm').form('reset');
			$('#itemeEditForm #szbm').next('span').remove(); $('#itemeEditForm #parentId').val("");
		}
	});
	
	//扩展 列_进度条progressbar
	function progressFormatter(value, row, index) {
     var htmlstr = '<div style="">';
        htmlstr += '<div class="progressbar-text" style="width:220px;height:26px;line-height:26px;">' + row.cid + '%</div>';
        htmlstr += '<div class="progressbar-value" style="width:' + row.cid + '%;height:26px;line-height:26px;">';
        htmlstr += '<div class="progressbar-text" style="width:220px;height:26px;line-height:26px;">' + row.cid + '%</div>';
        htmlstr += '</div>';
        htmlstr += '</div>';
        return htmlstr;
    };
})
//---------------------------------------------------------------------------------------------------------------------------------
	var showRole_userId = "";
	toolbar = {
		search : function() {
			$('#productDg').treegrid('load', {                   //load方法 加载和显示第一页的所有行
				title : $.trim($('input[name="user"]').val()),   //加载时额外添加这些字段
				date_from : $('input[name="date_from"]').val(),
				date_to : $('input[name="date_to"]').val(),
			});
		},
		reload : function(){
			$('#productDg').treegrid('reload');
		},
		redo : function(){
			$('#productDg').treegrid('unselectAll');
		},
		showRole : function(ID){
			showRole_userId = ID;
			$('#productDg').treegrid('unselectAll');
			$('#roleDialog').dialog({
			    title: '设置用户角色', 
			    width: 238,
			    height: 280,
			    closed: false,
			    cache: false,
			    modal: true,
			    href: "/temp/manager/user/role_zTree?id="+ID
			});
		},
		add : function(){
			//加载tree1
			//打开add窗口
			$('#manager_add').dialog('open');
			//$("input[name='title']").focus();
		},
		edit : function (logId) {
			var rows = $('#productDg').treegrid('getSelections'); //获取所有被选中行,>1警告,=1处理,=0错误
			if (rows.length > 1 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录只能选定一条数据！', 'warning');
				return ;
			};
			if (rows.length == 0 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录至少选定一条数据！', 'warning');
				return ;
			};			
			if (rows.length == 1 || logId != null) {
				if(logId != null){
					id = logId;
				}else{
					id = rows[0].moduleId;
				}
				$('#productDg').treegrid('unselectAll');
				$.ajax({
					url : '/manage/module/getOne',
					type : 'post',
					data : {"id" : id},
					beforeSend : function() {
						$.messager.progress({
							text : '正在获取中...',
						});
					},
					success : function (data, response, status) {
						$.messager.progress('close');
						if (data) {
							//--1 设置父模块名
							$('#manager_edit #szbm').next('span').remove();
							$('#manager_edit #szbm').after('<span style="margin-left:10px">'+data.data.parentName+'</span>');
							//--2 设置类型
							//--3 设置状态
							if(data.data.state == 1){
								$('#itemeEditForm [name="stateON"]').attr('checked', true);
								layuiShow(); //layui渲染 回显数据
							}else{
								$('#itemeEditForm [name="stateON"]').removeAttr('checked');
								layuiShow(); //layui渲染 回显数据
							}
							//--4 加载user属性数据 打开对话框
							$('#manager_edit').form('load', data.data);
							layuiShow(); //layui渲染 回显数据
							$('#manager_edit').dialog('open');
						} else {
							$.messager.alert('获取失败！', '未知错误导致失败，请重试！', 'warning');
						};
					}
				})
			};
		},
		remove : function (id) {
		/* alert('权限限制1!');return;//***---+++ */
			var rows = $('#productDg').treegrid('getSelections');
			if (rows.length > 0 || id != null) {
				$.messager.confirm('确定操作', '您正在要删除所选的记录吗？', function (flag) { //确定执行删除
					if (flag) {
						var ids = [];
						for (var i = 0; i < rows.length; i ++) {
							ids.push(rows[i].moduleId);
						}
						if(id != null){
							sid = id;
						}else{
							sid = ids.join(',');
						}
						$.ajax({
							type : 'POST',
							url : '/manage/module/delete',
							data : { "ids" : sid },
							beforeSend : function() {
								$('#productDg').treegrid('loading');
							},
							success : function (data) {
								if (data) {
									$('#productDg').treegrid('loaded');
									$('#productDg').treegrid('load'); //首列
									$.messager.show({
										title : '提示',
										msg : data.data + '！', //删除成功！
									});
									window.parent.generateMenu(0, null); //左导航重新加载
								}
							},
						});
					};
				});
			} else {
				$.messager.alert('提示', '请选择要删除的记录！', 'info');
			};
		},
		//自定义方法
		myQuery : function(){
			// 提示信息
	        $.messager.alert('自定义方法', '自定义方法被执行了！', 'messager-info');
	        var checkedRows = $('#productDg').treegrid('getChecked');  //在复选框被选中时返回所有行 
	        var selectedRow = $('#productDg').treegrid('getSelected'); //返回第一个被选中的行 或没有选中的行返回null。
	        // 提交参数查询表格数据 username=?
	        $('#productDg').treegrid('reload', {
	            username: $('#username').textbox('getValue'),
	        });			
		},
	};
</script>

<!-- /////////////////////////////////////////////////////////// -->

<!-- add块 -->
<div id="manager_add" style="display:none"></div>

<!-- edit块 -->
<div id="manager_edit" style="padding:3px 0 0 20px;display:none">
<form class="layui-form itemForm" id="itemeEditForm" method="post">
	<input type="hidden" name="moduleId" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0;margin-left:-10px;"><!-- tabs1 -->
<div style="width:360px;float:left">
          <div class="layui-form-item">
			<label class="layui-form-label">父模块</label>
			<div class="layui-input-block">
             <!-- <select name="deptId">
               <option value="">--请选择--</option>
             </select> -->
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_edit');" value="选择" />
		     <input type="hidden" name="parentId" id="parentId" />
		     <input type="hidden" name="parentName" id="parentName" />		              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">模块名</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入模块名" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">层数</label>
			<div class="layui-input-block">
			  <input type="radio" name="layerNum" value="1" title="一层">
			  <input type="radio" name="layerNum" value="2" title="二层">
              <input type="radio" name="layerNum" value="3" title="三层">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">权限标识</label>
			<div class="layui-input-block">
			  <input type="text" name="cpermission" placeholder="请输入权限标识" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">链接</label>
			<div class="layui-input-block">
			  <input type="text" name="curl" placeholder="请输入链接" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">类型</label>
			<div class="layui-input-block">
			  <input type="radio" name="ctype" value="1" title="主菜单">
			  <input type="radio" name="ctype" value="2" title="左侧菜单">
              <input type="radio" name="ctype" value="3" title="按钮">
              <input type="radio" name="ctype" value="4" title="链接">
              <input type="radio" name="ctype" value="5" title="状态">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">状态</label>
			<div class="layui-input-block">
			  <input type="checkbox" name="stateON" lay-skin="switch">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">从属</label>
			<div class="layui-input-block">
			  <input type="text" name="belong" placeholder="请输入从属" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">复用标识</label>
			<div class="layui-input-block">
			  <input type="text" name="cwhich" placeholder="请输入复用标识" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
				<label class="layui-form-label">排序号</label>
				<div class="layui-input-block">
				  <input type="text" name="orderNo" placeholder="请输入排序号" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item layui-form-text">
				<label class="layui-form-label">说明</label>
				<div class="layui-input-block">
				  <textarea name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
				</div>
			</div>
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
//--layui渲染 用于回显数据时渲染 也起渲染整表单作用 ★
function layuiShow(){
	layui.use('form', function() {
		var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
		form.render();         //渲染全部
	})	
}
$(function(){		
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			name : {required:true},
			curl : {required:true},
		},
		messages: {
			name : "<dl style='color:red'>请输入模块名</dl>",
			curl : "<dl style='color:red'>请输入模块链接</dl>",
		}
	});
})
//取所在部门  3 tree  easyui
function getSzbm(aeid){
	$("<div>").css({padding:"5px"}).html("<ul></ul>").window({
		width : '300',
	    height : '380',
	    title : '选择父模块',
	    iconCls : 'icon-save',
	    modal : true,
	    closed : true,
	    onOpen : function(){
	    	var _win = this;                                 //this <div><ul>_DOM
	    	$("ul", _win).tree({                             //子,父 父内找子
	    		url : '/manage/module/findModuleEUtree',    //异步数据[id text state]
	    		animate : true,
	    		onClick : function(node){                    //点击父节点,发送请求,展开父节点*
	    		//	if($(this).tree("isLeaf", node.target)){ //点击是叶子节点,$(this)=$("ul")
	    				$('#'+aeid+' input[name="parentId"]').val(node.id);
	    				$('#'+aeid+' input[name="parentName"]').val(node.text);
	    				$('#'+aeid+' #szbm').next('span').remove();
	    				$('#'+aeid+' #szbm').after('<span style="margin-left:10px">'+node.text+'</span>');
	    				$(_win).window('close');
	    		//	}
	    		}
	    	})
	    }
	}).window('open');
}
</script>
</div>
</body>
</html>






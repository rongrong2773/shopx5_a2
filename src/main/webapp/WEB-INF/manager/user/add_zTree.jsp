<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/cpts/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css" />
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script type="text/javascript">	
$(function() {
	var zTreeObj;
	var setting = {
		check : { enable : true	},
		data  : { simpleData : {enable:true} },
		callback: { onClick: zTreeOnClick }
	};
	function zTreeOnClick(event, treeId, treeNode) {
		$('#'+of+' input[name="managerId"]').val(treeNode.id);
		$('#'+of+' #zsld').next('span').remove();
		$('#'+of+' #zsld').after('<span style="margin-left:10px">'+treeNode.name+'</span>');
		$('#zsldDialog').dialog('close');
	};
	$.ajax({type:"post", url : "/manage/user/findUserZtree", async:false, dataType:"json", 
		success : function(data){
			zTreeObj = $.fn.zTree.init($('#jkTree'), setting, data);	//jkTree 树的id，支持多个树
			zTreeObj.expandAll(true);		//展开所有树节点
		}
	})
})
</script>

<ul id="jkTree" class="ztree"></ul>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/cpts/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css" />
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/cpts/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script type="text/javascript">
$(function() {
	//var userId = ${id };    //1 写回数据  可$('#userRoleListForm #userId').val()
	//alert(showRole_userId); //2 父层预定义
	$.post("/manage/user/findRoleZtree", {"id":showRole_userId}, function(data) {
			if (data) {
				zTreeObj_role = $.fn.zTree.init($('#rlTree'), setting_role, data);	//jkTree 树的id，支持多个树
				zTreeObj_role.expandAll(true);									    //展开所有树节点
			}
		}, "json"
	)
})
var zTreeObj_role;
var setting_role = {
	check : { enable : true	},
	data  : { simpleData : {enable:true} }
};
//获取所有选择的节点
function submitCheckedNodes() {
	var nodes = new Array();
	nodes = zTreeObj_role.getCheckedNodes(true);	//取得选中的结点
	var str = "";
	for (i = 0; i < nodes.length; i++) {
		if (str != "") {
			str += ",";
		}
		str += nodes[i].id;
	}
	$("#userRoleListForm #roleIds").val(str);
	//保存数据
	$.post("/manage/roleUser/add", $("#userRoleListForm").serialize(), function(data) {
			if (data.status == 200) {
				$.messager.show({ title:'提示', msg:'设置成功!',	});
			}
		}, "json"
	)
	$('#roleDialog').dialog('close');
}
</script>
	<div style="padding:5px 0 0 0">
	<form id="userRoleListForm">
		<ul id="rlTree" class="ztree"></ul>
		<input type="hidden" name="userId"  id="userId"  value="${id }" />
		<input type="hidden" name="roleIds" id="roleIds" value="" />
		<a href="javascript:void(0);" class="layui-btn layui-btn-xs" style="margin:10px 0 0 20px" onclick="submitCheckedNodes();">保存</a>
		<a href="javascript:void(0);" class="layui-btn layui-btn-xs layui-btn-danger"
			onclick="$('#roleDialog').dialog('close');" style="margin:10px 0 0 10px">取消</a>
	</form>	 
	</div>		 
		 
		 
		 
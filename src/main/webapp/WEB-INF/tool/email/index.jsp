<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>发送邮件</title>
    <meta name="keywords" content='发送邮件' />
    <meta name="description" content='发送邮件' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
	<!-- UEditor编辑器  -->
    <link type="text/css" href="/cpts/ueditor/themes/default/css/ueditor.css" rel="stylesheet" />
	<script type="text/javascript"charset="utf-8" src= "/cpts/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/ueditor.all.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>

<body>
<div id="manager_add">
<form class="layui-form itemForm" id="itemeAddForm" action="/tool/toEmail" enctype="multipart/form-data" method="post">
	<input type="hidden" name="emailId" id="emailId" value="${id }" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="填写信息" data-options="iconCls:'fa fa-th'" style="padding:20px 0 0 20px"><!-- tabs1 -->
<div style="width:800px;float:left">

          <div class="layui-form-item">
			<label class="layui-form-label">收件人</label>
			<div class="layui-input-block">
			  <input type="text" name="toEmail" placeholder="请输入收件人邮箱 (多个用;分号隔开)" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">主题</label>
			<div class="layui-input-block">
			  <input type="text" name="subject" placeholder="请输入邮件主题" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item layui-form-text">
			<label class="layui-form-label">内容</label>
			<div class="layui-input-block">
			  <textarea name="text" placeholder="请输入邮件内容" id="container"></textarea>
			</div>
		  </div>
		  <div class="layui-form-item" style="margin:16px 0 10px 0">
			<label class="layui-form-label" style="margin:-4px 0 0 0">添加图片</label>
			<div class="layui-input-block">
			  <input type="file" name="picFile" />
			</div>
		  </div>
		  <div class="layui-form-item" style="margin:16px 0 10px 0">
			<label class="layui-form-label" style="margin:-4px 0 0 0">添加附件</label>
			<div class="layui-input-block">
			  <input type="file" name="rarFile" />
			</div>
		  </div>
		  <div class="layui-form-item">
			<div class="layui-input-block">
			  <button class="layui-btn" type="submit" lay-submit lay-filter="formDemo">立即发送</button>
			  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		  </div>
		  
</div>
<div style="width:100px;float:left">
</div>
		</div>
    </div>
</form>
</div>
<script type="text/javascript">
$(function(){
	//Editor
	var ueAdd = UE.getEditor('container',{
		toolbars: [
		           ['fullscreen', 'source', 'undo', 'redo'],
		           ['bold', 'italic', 'underline' ,'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc']
		       ],
	    elementPathEnabled:false,     //关闭元素路径
	    wordCount:false,              //关闭字数统计
	    initialFramewidth:800,        //编辑区域宽度
	    initialFrameHeight:150,       //编辑区域高度
	});
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			toEmail : "required",
			subject : "required",
		},
		messages: {
			toEmail : "<dl style='color:red'>请输入邮箱 (多个用;分号隔开)</dl>",
			subject : "<dl style='color:red'>请输入邮件主题</dl>",
		}
	})
	//回调信息
	if($('#emailId').val() == 1){
		alert("邮件发送成功!")
	}
})
</script>
</body>
</html>






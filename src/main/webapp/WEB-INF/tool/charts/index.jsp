<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>amCharts图表组件</title>
    <meta name="keywords" content='amCharts图表组件' />
    <meta name="description" content='amCharts图表组件' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- amCharts -->
	<link type="text/css" href="/cpts/amcharts/samples/style.css" rel="stylesheet" />
	<script type="text/javascript" src="/cpts/amcharts/amcharts/amcharts.js"></script>
	<script type="text/javascript" src="/cpts/amcharts/amcharts/serial.js"></script>
	<script type="text/javascript" src="/cpts/amcharts/amcharts/pie.js"></script>
	<script type="text/javascript" src="/cpts/amcharts/amcharts/funnel.js"></script>
</head>

<body>
<div id="manager_add">
<form class="layui-form itemForm" id="itemeAddForm" action="/tool/toSms" method="post">
<input type="hidden" name="smsId" id="smsId" value="${id }" />
<div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
<div title="anCharts图形报表" data-options="iconCls:'fa fa-th'" style="padding:20px 0 150px 30px"><!-- tabs1 -->
	<script>
        var chart;
        var chartData = ${result };     // var chartData = [{"country":"USA","visits":4025},,,]
        var data      = ${result };     // var data      = [{"title":"USA","value":200 },,,]
        var chartData3= ${result };     // var chartData = [{"country":"USA","visits":4025,"color":"#FF0F00"},,,]
        AmCharts.ready(function () {
			//--------------1.饼图
            // PIE CHART
            chart = new AmCharts.AmPieChart();
            // title of the chart
            chart.addTitle("数据分布", 16);
            chart.dataProvider = chartData;
            chart.titleField = "country";
            chart.valueField = "visits";
            chart.sequencedAnimation = true;
            chart.startEffect = "elastic";
            chart.innerRadius = "30%";
            chart.startDuration = 2;
            chart.labelRadius = 15;
            chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
            // the following two lines makes the chart 3D
            chart.depth3D = 10;
            chart.angle = 15;
            // WRITE
            chart.write("chartdiv1");
           
            //--------------2.锥形图
            chart = new AmCharts.AmFunnelChart();
            chart.rotate = true;
            chart.titleField = "title";
            chart.balloon.fixedPosition = true;
            chart.marginRight = 210;
            chart.marginLeft = 15;
            chart.labelPosition = "right";
            chart.funnelAlpha = 0.9;
            chart.valueField = "value";
            chart.startX = -500;
            chart.dataProvider = data;
            chart.startAlpha = 0;
            chart.depth3D = 100;
            chart.angle = 30;
            chart.outlineAlpha = 1;
            chart.outlineThickness = 2;
            chart.outlineColor = "#FFFFFF";
            chart.write("chartdiv2");
            
            //--------------3.柱状图
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData3;
            chart.categoryField = "country";
            // the following two lines makes chart 3D
            chart.depth3D = 20;
            chart.angle = 30;
            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 90;
            categoryAxis.dashLength = 5;
            categoryAxis.gridPosition = "start";
            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = "Numbers";
            valueAxis.dashLength = 5;
            chart.addValueAxis(valueAxis);
            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "visits";
            graph.colorField = "color";
            graph.balloonText = "<span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 1;
            chart.addGraph(graph);
            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);
            chart.creditsPosition = "top-right";
            // WRITE
            chart.write("chartdiv3");
            
            
        });
    </script>
<div style="width:525px;height:400px;float:left;margin:0 0 20px 0;border:1px solid #ccc;border-right:none">
	<div id="chartdiv1" style="width:550px;height:400px"></div>
</div>
<div style="width:525px;height:400px;float:left;margin:0 0 20px 0;border:1px solid #ccc;border-left:none">
	<div id="chartdiv2" style="width:550px;height:400px"></div>
</div>
<div style="width:1050px;height:400px;float:left;border:1px solid #ccc">
    <div id="chartdiv3" style="width:100%;height:400px"></div>
</div>

</div>
</div>
</form>
</div>
<script type="text/javascript">
$(function(){

})
</script>
</body>
</html>






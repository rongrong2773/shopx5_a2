<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<style type="text/css">.nc-appbar-tabs a.compare{display:none !important;}</style>
<link href="/shop/css/base.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/home_cart.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--[if IE 7]>
	<link href="/shop/css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript">
var _CHARSET = 'utf-8';
var PRICE_FORMAT = '&yen;%s';
Number.prototype.toFixed = function(d) {
    var s = this+"";
	if(!d) d=0;
    if(s.indexOf(".")==-1) s+=".";
	s += new Array(d+1).join("0");
    if(new RegExp("^(-|\\+)?(\\d+(\\.\\d{0,"+ (d+1) +"})?)\\d*$").test(s)) {
        var s="0"+ RegExp.$2, pm=RegExp.$1, a=RegExp.$3.length, b=true;
        if (a==d+2) {
			a=s.match(/\d/g);
			if (parseInt(a[a.length-1])>4) {
				for(var i=a.length-2; i>=0; i--) {a[i] = parseInt(a[i])+1;
				if(a[i]==10){a[i]=0; b=i!=1;} else break;}
			}
			s=a.join("").replace(new RegExp("(\\d+)(\\d{"+d+"})\\d$"),"$1.$2");
    	}
		if(b) s=s.substr(1);
		return (pm+s).replace(/\.$/, "");
	}
	return this+"";
};
</script>
<script src="/static/shop/js/jquery.js" type="text/javascript"></script>
<script src="/static/shop/js/jquery.ui.js" type="text/javascript"></script>
<script src="/static/shop/js/jquery.validation.min.js" type="text/javascript"></script>
<script src="/static/shop/js/jquery.poshytip.min.js" type="text/javascript"></script>
<script src="/static/shop/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="/static/shop/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="/static/shop/js/common.js" type="text/javascript"></script>
<script src="/static/shop/js/goods_cart.js" type="text/javascript"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="/static/shop/js/html5shiv.js" type="text/javascript"></script>
	<script src="/static/shop/js/respond.min.js" type="text/javascript"></script>
<![endif]-->

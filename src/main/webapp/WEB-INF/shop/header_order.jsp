<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="/static/shop/js/ss/common.js"></script>
<!-- PublicTopHead Begin -->
<div class="public-top-layout w">
  <div class="topbar wrapper">
    <div class="user-entry"> 您好 <span><a href="#">shopx5</a>
      <div class="nc-grade-mini" style="cursor:pointer;" onClick="javascript:go('#');">V0</div>
      </span> ，欢迎来到 <a href="#" title="ShopX5"><span>ShopX5</span></a> <span>[<a href="#">退出</a>] </span>
    </div>
    <div class="quick-menu">
      <dl>
        <dt><a href="/shop/index/order">我的订单</a><i></i></dt>
        <dd>
          <ul>
            <li><a href="/shop/index/order">待付款订单</a></li>
            <li><a href="/shop/index/order">待确认收货</a></li>
            <li><a href="/shop/index/order">待评价交易</a></li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt><a href="/shop/index/collection">我的收藏</a><i></i></dt>
        <dd>
          <ul>
            <li><a href="/shop/index/collection">商品收藏</a></li>
            <li><a href="javascript:void(0);">店铺收藏</a></li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt>客户服务<i></i></dt>
        <dd>
          <ul>
            <li><a href="javascript:void(0);">帮助中心</a></li>
            <li><a href="javascript:void(0);">售后服务</a></li>
            <li><a href="javascript:void(0);">客服中心</a></li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt>站点导航<i></i></dt>
        <dd>
          <ul>
            <li><a href="javascript:void(0);" target="_blank">资讯频道</a></li>
            <li><a href="javascript:void(0);" target="_blank">社区圈子</a></li>
            <li><a href="javascript:void(0);" target="_blank">商城首页</a></li>
          </ul>
        </dd>
      </dl>
      <dl class="weixin">
        <dt>关注我们<i></i></dt>
        <dd>
          <h4>扫描二维码<br />关注商城微信号</h4>
          <img src="/static/shop/images/04757778960230182.jpg" />
        </dd>
      </dl>
    </div>
  </div>
</div>

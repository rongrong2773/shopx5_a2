<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户头像 - ShopX5商城系统,Java商城系统</title>
<meta name="keywords" content="ShopX5,Java商城系统,ShopX5商城系统,电子商务解决方案" />
<meta name="description" content="ShopX5专注于研发符合时代发展需要的电子商务商城系统" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<link href="/static/shop/css/base.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_header.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<%@ include file="../header_js.jsp" %>
<!-- KindEditor -->
<link type="text/css" href="/js/kindeditor/themes/default/default.css" rel="stylesheet" />
<script type="text/javascript" src="/js/kindeditor/kindeditor-all-min.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/kindeditor/lang/zh-CN.js" charset="utf-8"></script>
</head>

<body>
<%@ include file="../header.jsp" %>
<link href="/static/shop/css/member2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/static/shop/js/member.js"></script>
<script type="text/javascript" src="/static/shop/js/ToolTip.js"></script>
<script>
$(document).ready(function() {
    $.each($(".side-menu > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
            if (ulNode.css('display') == 'block') {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),1);
            } else {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),null);
            }
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
	$.each($(".side-menu-quick > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
})
</script>
<div class="ncm-container">
<%@ include file="header_left_user.jsp" %>
<div class="right-layout">
<script type="text/javascript" src="/static/shop/js/jquery.Jcrop/jquery.Jcrop.js"></script>
<link href="/static/shop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" id="cssfile2" />
<div class="wrap">
  <div class="tabmenu">
    <ul class="tab pngFix">
  		<li class="normal"><a href="/shop/user/_info">基本信息</a></li>
  		<!-- <li class="normal"><a href="javascript:void(0);">兴趣标签</a></li> -->
  		<li class="active"><a href="/shop/user/_head">更换头像</a></li>
    </ul>
  </div>
    <form action="" enctype="multipart/form-data" id="form_avaster" method="post">
    <input type="hidden" name="form_submit" value="ok" />
    <div class="ncm-default-form">
      <dl>
        <dt>头像预览：</dt>
        <dd>
          <div class="user-avatar"><span><img src="" nc_type="avatar" width='150' height='150' /></span></div>
          <p class="hint mt5">完善个人信息资料，上传头像图片有助于您结识更多的朋友。<br />
          <span style="color:orange;">头像默认尺寸为120x120像素，请根据系统操作提示进行裁剪并生效。</span></p>
        </dd>
      </dl>
      <dl>
        <dt>更换头像：</dt>
        <dd>
          <div class="ncm-upload-btn">
			<input type="button" onclick="picUpload()" value="上传图片" /><span id="pics"></span>
          </div>
        </dd>
      </dl>
    </div>
  </form>
  </div>
<script type="text/javascript">
var kindEditorParams = {
		cssPath : '/css/index.css',
        filterMode : true,
		uploadJson : '/picture/upload',
		filePostName  : "uploadFile",
		dir : "image",
}
function picUpload(){
	//初始化图片上传器
	KindEditor.editor(kindEditorParams).loadPlugin('multiimage',function(){
		var editor = this;
		editor.plugin.multiImageDialog({
			clickFn : function(urlList) {
				var imgArray = [];
				KindEditor.each(urlList, function(i, data) { //处理返回 json
					imgArray.push(data.url);
					$('.user-avatar').find('span').html(
						"<a href='"+data.url+"' target='_blank'><img src='"+data.url+"' width='150' height='150' /></a>");
				});
				editor.hideDialog();
			}
		})
	})
}

$(function(){
	$('#pic').change(function(){
		var filepath=$(this).val();
		var extStart=filepath.lastIndexOf(".");
		var ext=filepath.substring(extStart,filepath.length).toUpperCase();
		if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
			alert("文件类型错误，请选择图片文件（png|gif|jpg|jpeg）");
			$(this).attr('value','');
			return false;
		}
		if ($(this).val() == '') return false;
		$("#form_avaster").submit();
	});
});
</script>
</div>
<div class="clear"></div>
</div>
<%@ include file="../footer.jsp" %>
</body>
</html>



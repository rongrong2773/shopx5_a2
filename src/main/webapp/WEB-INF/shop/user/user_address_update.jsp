<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="fwin_my_address_edit" class="dialog_wrapper shop_nc ui-draggable" style="z-index: 1100; position: absolute; width: 550px; left: 436.5px; top: 281px;"><div class="dialog_body" style="position: relative;"><h3 class="dialog_head" style="cursor: move;"><span class="dialog_title"><span class="dialog_title_icon">编辑地址</span></span><span class="dialog_close_button">X</span></h3><div class="dialog_content" style="margin: 0px; padding: 0px;">
<div class="eject_con">
  <div class="adds">
    <div id="warning"></div>
    <form method="post" action="" id="address_form" target="_parent">
      <input type="hidden" name="form_submit" value="ok">
      <input type="hidden" name="id" value="69">
      <input type="hidden" value="111" name="city_id" id="_area_2">
      <input type="hidden" value="1558" name="area_id" id="_area">
      <dl>
        <dt><i class="required">*</i>收货人：</dt>
        <dd>
          <input type="text" class="text w100" name="true_name" value="倒萨范德萨">
          <p class="hint">请填写您的真实姓名</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>所在地区：</dt>
        <dd><input type="hidden" name="region" id="region" value="辽宁 本溪市 明山区"><span id="_area_span" class="_region_value">辽宁 本溪市 明山区</span><input type="button" class="input-btn" value="编辑">
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>街道地址：</dt>
        <dd>
          <input class="text w300" type="text" name="address" value="倒萨范德萨大范德萨">
          <p class="hint">不必重复填写地区</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>电话号码：</dt>
        <dd>
          <input type="text" class="text w200" name="tel_phone" value="1595564598">
          <p class="hint">区号 - 电话号码 - 分机</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>手机号码：</dt>
        <dd>
          <input type="text" class="text w200" name="mob_phone" value="15955006987">
        </dd>
      </dl>
            <dl>
        <dt><em class="pngFix"></em>设为默认地址：</dt>
        <dd>
          <input type="checkbox" class="checkbox vm mr5" checked="" name="is_default" id="is_default" value="">
          <label for="is_default">设置为默认收货地址</label>
        </dd>
      </dl>
                  <dl>
        <dt>&nbsp;</dt>
        <dd> <a href="javascript:void(0);" class="ncbtn-mini ncbtn-bittersweet" id="zt"><i class="icon-flag"></i>使用门店代收</a>
        <p class="hint">当您需要对自己的收货地址保密或担心收货时间冲突时可使用该业务，<br>添加后可在购物车中作为收货地址进行选择，货品将直接发送至门店，<br>到货后短信、站内消息进行通知，届时您可使用“自提码”至该门店兑码取货。</p> </dd>
      </dl>
            <div class="bottom">
        <label class="submit-border">
          <input type="submit" class="submit" value="编辑地址">
        </label>
        <a class="ncbtn ml5" href="javascript:DialogManager.close('my_address_edit');">取消</a> </div>
    </form>
  </div>
</div>
</div></div><div style="clear:both; display:block;"></div></div>







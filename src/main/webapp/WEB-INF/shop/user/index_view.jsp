<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>我的足迹 - ShopX5商城系统,Java商城系统</title>
<meta name="keywords" content="ShopX5,Java商城系统,ShopX5商城系统,电子商务解决方案" />
<meta name="description" content="ShopX5专注于研发符合时代发展需要的电子商务商城系统" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<meta name="renderer" content="webkit" />
<meta name="renderer" content="ie-stand" />
<link href="/static/shop/css/base.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_header.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<%@ include file="../header_js.jsp" %>
</head>

<body>
<%@ include file="../header.jsp" %>
<link href="/static/shop/css/member.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/static/shop/js/member.js"></script>
<script type="text/javascript" src="/static/shop/js/ToolTip.js"></script>
<script>
$(document).ready(function() {
	//点击左侧导航,使其变点中状态
    $.each($(".side-menu > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
            if (ulNode.css('display') == 'block') {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'), 1);
            } else {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'), null);
            }
			ulNode.slideToggle();
			if ($(this).hasClass('shrink')) {
				$(this).removeClass('shrink');
			} else {
				$(this).addClass('shrink');
			}
        });
    });
	$.each($(".side-menu-quick > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
			ulNode.slideToggle();
			if ($(this).hasClass('shrink')) {
				$(this).removeClass('shrink');
			} else {
				$(this).addClass('shrink');
			}
        });
    });
});
</script>
<div class="ncm-container">
<%@ include file="header_left_shop.jsp" %>
<div class="right-layout">
<style type="text/css">
#box{background:#F30;width:16px;height:16px;margin-left:150px;display:block;border-radius:100%;position:absolute;z-index:999;opacity:0.5}
</style>
<div class="wrap">
  <div class="tabmenu">
    <ul class="tab pngFix">
  		<li class="active"><a href="/shop/index/view">我的足迹</a></li><!--
  		<li class="normal"><a href="/shop/index/view">我的足迹</a></li>-->
  	</ul>
  </div>
  <div id="favoritesGoods">
   	<div class="favorite-goods-list">
    <ul>    
    <c:forEach items="${pageBean.list }" var="item">
      <li class="favorite-pic-list" >
        <div class="favorite-goods-thumb"><a href="/shop/${item.id }.htm" alt="${item.goodsName }" target="_blank"><img src="${item.goodInfo.itemImages }" /></a></div>
        <div class="handle">
            <a href="javascript:void(0)" onClick="delOne(${item.collectionId });" class="fr ml5" title="删除"><i class="icon-trash"></i></a>
            <!--<a href="javascript:void(0)" nc_type="sharegoods" data-param='{"gid":""}' class="fl w40" title="分享"><i class="icon-share"></i>分享</a>-->
        </div>
        <dl class="favorite-goods-info">
          <dt><a href="/shop/${item.id }.htm" title="${item.goodsName }" target="_blank">${item.goodsName }</a></dt>
          <dd class="goods-price">
          	&yen;<strong><fmt:formatNumber value="${item.price }" groupingUsed="true" maxFractionDigits="2" minFractionDigits="2" /></strong></dd>
        </dl>
      </li>
      </c:forEach>
     </ul>
    </div>
    <div>
	    <form id="stable" action="/shop/index/view" method="post">
			<div class="pagination">
				<ul>
					<li><span>共${pageBean.totalCount }条</span></li>
					<!-- 判断当前页是否首页  -->
					<c:if test="${pageBean.currPage == 1 }">
						<li><span>首页</span></li>
					</c:if>
					<c:if test="${pageBean.currPage != 1 }">
						<li><a href="/shop/index/view?page=1"><span>首页</span></a></li>
						<li><a href="/shop/index/view?page=${pageBean.currPage-1 }"><span>上一页</span></a></li>
					</c:if>
					<!-- 展示所有页码 前四后五-->
					<c:forEach begin="${pageBean.currPage-5>0?pageBean.currPage-5:1 }"
						end="${pageBean.currPage+4>pageBean.totalPage?pageBean.totalPage:pageBean.currPage+4 }" step="1" var="n">
						<!-- 判断是否当前页 -->
						<c:if test="${pageBean.currPage == n }">
							<li><span class="currentpage">${n }</span></li>
						</c:if>
						<c:if test="${pageBean.currPage != n }">
							<li><a href="/shop/index/view?page=${n }"><span>${n }</span></a></li>
						</c:if>
					</c:forEach>
					<!-- 判断是否最后一页 -->
					<c:if test="${pageBean.currPage == pageBean.totalPage }">
						<li><span>末尾</span></li>
					</c:if>
					<c:if test="${pageBean.currPage != pageBean.totalPage }">
						<li><a href="/shop/index/view?page=${pageBean.currPage+1 }"><span>下一页</span></a></li>
						<li><a><span>页码:<input type="text" name="page" size="3"
						       style="height:10px;" onKeyDown="javascript:if(event.keyCode==13) search(this.value);" /></span></a></li>
						<li><a href="/shop/index/view?page=${pageBean.totalPage }"><span>末尾</span></a></li>
					</c:if>
				</ul>
			</div>
		</form>
		<!--分页结束-->
    </div>
  </div>
  <!-- 猜你喜欢 -->
  <div id="guesslike_div" style="width:980px"></div>
</div>
<script src="/static/shop/js/jquery.thumb.min.js"></script>
<script>
function search(value){
	$('#stable').submit();
}
function delOne(viewId){
	if(confirm('确实要删除吗?')){
		$.post("/shop/index/view/delete", {"ids":viewId}, function(data) {
				if (data.status == 200) {
					window.location.reload();
				}
			}, "json");
	}
}
$(function(){
	$('.favorite-goods-thumb img').jqthumb({
		width: 150,
		height: 150,
		after: function(imgObj){
			imgObj.css('opacity', 0.3).attr('title',$(this).attr('alt')).animate({opacity: 1}, 2000);
		}
	});
	/*猜你喜欢
	$('#guesslike_div').load('?get_guesslike', function(){
        $(this).show();
    });
	*/
	/*分享单个商品
	$("[nc_type='sharegoods']").bind('click',function(){
		var data_str = $(this).attr('data-param');
	    eval( "data_str = "+data_str);
	    ajaxget('?sharegoods_one&id);
	});
	*/
});
</script>
</div>
<div class="clear"></div>
</div>
<%@ include file="../footer.jsp" %>
</body>
</html>



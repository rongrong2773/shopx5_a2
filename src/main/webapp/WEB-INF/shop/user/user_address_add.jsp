<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="fwin_my_address_edit" class="dialog_wrapper shop_nc ui-draggable" style="z-index: 1100; position: absolute; width: 550px; left: 436.5px; top: 280.5px;"><div class="dialog_body" style="position: relative;"><h3 class="dialog_head" style="cursor: move;"><span class="dialog_title"><span class="dialog_title_icon">新增地址</span></span><span class="dialog_close_button">X</span></h3><div class="dialog_content" style="margin: 0px; padding: 0px;">
<div class="eject_con">
  <div class="adds">
    <div id="warning"></div>
    <form method="post" action="" id="address_form" target="_parent">
      <input type="hidden" name="form_submit" value="ok">
      <input type="hidden" name="id" value="">
      <input type="hidden" value="" name="city_id" id="_area_2">
      <input type="hidden" value="" name="area_id" id="_area">
      <dl>
        <dt><i class="required">*</i>收货人：</dt>
        <dd>
          <input type="text" class="text w100" name="true_name" value="">
          <p class="hint">请填写您的真实姓名</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>所在地区：</dt>
        <dd><select><option value="">-请选择-</option><option value="1">北京</option><option value="2">天津</option><option value="3">河北</option>
        <option value="4">山西</option><option value="5">内蒙古</option><option value="6">辽宁</option><option value="7">吉林</option>
        <option value="8">黑龙江</option><option value="9">上海</option><option value="10">江苏</option><option value="11">浙江</option>
        <option value="12">安徽</option><option value="13">福建</option><option value="14">江西</option><option value="15">山东</option>
        <option value="16">河南</option><option value="17">湖北</option><option value="18">湖南</option><option value="19">广东</option>
        <option value="20">广西</option><option value="21">海南</option><option value="22">重庆</option><option value="23">四川</option>
        <option value="24">贵州</option><option value="25">云南</option><option value="26">西藏</option><option value="27">陕西</option>
        <option value="28">甘肃</option><option value="29">青海</option><option value="30">宁夏</option><option value="31">新疆</option>
        <option value="32">台湾</option><option value="33">香港</option><option value="34">澳门</option><option value="35">海外</option>
        </select><input type="hidden" name="region" id="region" value="">
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>街道地址：</dt>
        <dd>
          <input class="text w300" type="text" name="address" value="">
          <p class="hint">不必重复填写地区</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>电话号码：</dt>
        <dd>
          <input type="text" class="text w200" name="tel_phone" value="">
          <p class="hint">区号 - 电话号码 - 分机</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>手机号码：</dt>
        <dd>
          <input type="text" class="text w200" name="mob_phone" value="">
        </dd>
      </dl>
            <dl>
        <dt><em class="pngFix"></em>设为默认地址：</dt>
        <dd>
          <input type="checkbox" class="checkbox vm mr5" name="is_default" id="is_default" value="">
          <label for="is_default">设置为默认收货地址</label>
        </dd>
      </dl>
                  <dl>
        <dt>&nbsp;</dt>
        <dd> <a href="javascript:void(0);" class="ncbtn-mini ncbtn-bittersweet" id="zt"><i class="icon-flag"></i>使用门店代收</a>
        <p class="hint">当您需要对自己的收货地址保密或担心收货时间冲突时可使用该业务，<br>添加后可在购物车中作为收货地址进行选择，货品将直接发送至门店，<br>到货后短信、站内消息进行通知，届时您可使用“自提码”至该门店兑码取货。</p> </dd>
      </dl>
            <div class="bottom">
        <label class="submit-border">
          <input type="submit" class="submit" value="新增地址">
        </label>
        <a class="ncbtn ml5" href="javascript:DialogManager.close('my_address_edit');">取消</a> </div>
    </form>
  </div>
</div>
</div></div><div style="clear:both; display:block;"></div></div>




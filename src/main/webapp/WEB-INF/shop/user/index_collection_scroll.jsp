<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<c:forEach items="${pageBean.list }" var="item">
<div class="item">
	<div class="scope">
		<dl class="goods">
			<dt class="goods-thumb"><a href="/shop/${item.id }.htm" title="${item.goodsName }" target="_blank"><img src="${item.goodInfo.itemImages }"></a></dt>
			<dd class="goods-name"><a href="/shop/${item.id }.htm" target="_blank">${item.goodsName }</a></dd>
		</dl>
		<div class="goods-price">
			<span class="sale">商城价：<em>¥<fmt:formatNumber value="${item.price }" groupingUsed="true" maxFractionDigits="2" minFractionDigits="2" /></em></span>
		</div>
	</div>
</div>
</c:forEach>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="ncm-header">
  <div class="ncm-header-top">
    <div class="ncm-member-info">
      <div class="avatar">
      	<a href="/shop/user/_head" title="修改头像"><img src="" /><div class="frame"></div></a>
      </div>
      <dl>
        <dt><a href="javascript:void(0);" title="修改资料">tom</a></dt>
        <dd>会员等级：
          <div class="nc-grade-mini" style="cursor:pointer;">V0会员</div>
        </dd>
        <dd>登录绑定：
          <div class="user-account">
            <ul>
              <li id="qq"><a href="javascript:void(0);" title="登录绑定QQ账号"><span class="icon"></span></a></li>
              <li id="weichat"><a href="javascript:void(0);" title="登录绑定微信账号"><span class="icon"></span></a></li>
              <li id="weibo"><a href="javascript:void(0);" title="登录绑定微博账号"><span class="icon"></span></a></li>
            </ul>
          </div>
        </dd>
      </dl>
    </div>
    <div class="ncm-set-menu">
      <dl class="zhaq">
        <dt>账户安全</dt>
        <dd>
          <ul>
            <li><a href="/shop/user/_info"><span class="zhaq01"></span><sub></sub><h5>修改密码</h5></a></li>
            <li ><a href="/shop/user/_info"><span class="zhaq02"></span><sub></sub><h5>邮箱绑定</h5></a></li>
            <li ><a href="/shop/user/_info"><span class="zhaq03"></span><sub></sub><h5>手机绑定</h5></a></li>
            <li ><a href="/shop/user/_info"><span class="zhaq04"></span><sub></sub><h5>支付密码</h5></a></li>
          </ul>
        </dd>
      </dl>
      <dl class="zhcc">
        <dt>账户财产</dt>
        <dd>
          <ul>
            <li><a href="javascript:void(0);"><span class="zhcc01"></span><h5>在线充值</h5></a></li>
            <li><a href="javascript:void(0);"><span class="zhcc02"></span><h5>充值卡充值</h5></a></li>
            <li><a href="javascript:void(0);"><span class="zhcc03"></span><h5>领取代金券</h5></a></li>
            <li><a href="javascript:void(0);"><span class="zhcc04"></span><h5>领取红包</h5></a></li>
          </ul>
        </dd>
      </dl>
      <dl class="xgsz">
        <dt>相关设置</dt>
        <dd>
          <ul class="trade-function-03">
            <li><a href="/shop/index/addr"><span class="xgsz01"></span><h5>收货地址</h5></a></li>
            <li><a href="javascript:void(0);"><span class="xgsz02"></span><h5>消息接收</h5></a></li>
          </ul>
        </dd>
      </dl>
    </div>
  </div>
  <div class="ncm-header-nav">
    <ul class="nav-menu">
      <li class="shop"><a href="javascript:void(0);">我的商城<i></i></a>
        <div class="sub-menu">
          <dl>
            <dt><a href="/shop/index/order" style="color:#398EE8">交易管理</a></dt>
            <dd><a href="/shop/index/order">交易订单</a></dd>
            <dd><a href="javascript:void(0);">到货通知</a></dd>
          </dl>
          <dl>
            <dt><a href="/shop/index/collection" style="color:#3AAC8A">收藏关注</a></dt>
            <dd><a href="/shop/index/collection">收藏的商品</a></dd>
            <dd><a href="/shop/index/collection">收藏的店铺</a></dd>
            <dd><a href="/shop/index/view">浏览足迹</a></dd>
          </dl>
          <dl>
            <dt><a href="javascript:void(0);" style="color:#B68571">服务售后</a></dt>
            <dd><a href="javascript:void(0);">退款/退货</a></dd>
            <dd><a href="javascript:void(0);">交易投诉</a></dd>
            <dd><a href="javascript:void(0);">商品咨询</a></dd>
            <dd><a href="javascript:void(0);">平台客服</a></dd>
          </dl>
        </div>
      </li>
      <li><a href="javascript:void(0);" class="current">用户设置</a> </li>
      <li><a href="javascript:void(0);">个人主页<i></i></a>
        <div class="sub-menu">
          <dl>
            <dd><a href="javascript:void(0);">新鲜事</a></dd>
            <dd><a href="javascript:void(0);">个人相册</a></dd>
            <dd><a href="javascript:void(0);">分享商品</a></dd>
            <dd><a href="javascript:void(0);">分享店铺</a></dd>
          </dl>
        </div>
      </li>
      <li><a href="javascript:void(0);">其他应用<i></i></a>
        <div class="sub-menu">
          <dl>
            <dd><a href="javascript:void(0);">我的CMS</a></dd>
            <dd><a href="javascript:void(0);">我的圈子</a></dd>
            <dd><a href="javascript:void(0);">我的微商城</a></dd>
          </dl>
        </div>
      </li>
    </ul>
    <div class="notice">
      <ul class="line">
      </ul>
    </div>
	<script type="text/javascript">
    $(function() {
        var _wrap = $('ul.line');
        var _interval = 2000;
        var _moving;
        _wrap.hover(function() {
            clearInterval(_moving);
        },
        function() {
            _moving = setInterval(function() {
                var _field = _wrap.find('li:first');
                var _h = _field.height();
                _field.animate({
                    marginTop: -_h + 'px'
                },
                600,
                function() {
                    _field.css('marginTop', 0).appendTo(_wrap);
                })
            }, _interval)
        }).trigger('mouseleave');
    });
    </script>
  </div>
</div>

<div class="left-layout">
  <ul id="sidebarMenu" class="ncm-sidebar">
    <li class="side-menu"><a href="javascript:void(0)" key="info">
      <h3>会员资料</h3>
      </a>
      <ul >
        <li ><a href="/shop/user/_info">账户信息</a></li>
        <li class="selected"><a href="/shop/index/addr">收货地址</a></li>
        <li ><a href="javascript:void(0);">账户安全</a></li>
        <li ><a href="javascript:void(0);">我的消息</a></li>
        <li ><a href="javascript:void(0);">第三方账号登录</a></li>
      </ul>
    </li>
    <li class="side-menu"><a href="javascript:void(0)" key="property">
      <h3>财产中心</h3>
      </a>
      <ul >
        <li ><a href="javascript:void(0);">消费记录</a></li>
        <li ><a href="javascript:void(0);">我的积分</a></li>
        <li ><a href="javascript:void(0);">我的代金券</a></li>
        <li ><a href="javascript:void(0);">我的红包</a></li>
      </ul>
    </li>
  </ul>
</div>

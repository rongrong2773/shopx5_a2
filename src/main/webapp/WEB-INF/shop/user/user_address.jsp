<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户收货地址 - ShopX5商城系统,Java商城系统</title>
<meta name="keywords" content="ShopX5,Java商城系统,ShopX5商城系统,电子商务解决方案" />
<meta name="description" content="ShopX5专注于研发符合时代发展需要的电子商务商城系统" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<link href="/static/shop/css/base.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_header.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<%@ include file="../header_js.jsp" %>
</head>

<body>
<%@ include file="../header.jsp" %>
<link href="/static/shop/css/member2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/static/shop/js/member.js"></script>
<script type="text/javascript" src="/static/shop/js/ToolTip.js"></script>
<script>
//sidebar-menu
$(document).ready(function() {
    $.each($(".side-menu > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
            if (ulNode.css('display') == 'block') {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),1);
            } else {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),null);
            }
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
	$.each($(".side-menu-quick > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
});
</script>
<div class="ncm-container">
<%@ include file="header_left_user.jsp" %>
<div class="right-layout">
<div class="wrap">
  <div class="tabmenu">
    <ul class="tab pngFix">
  	  <li class="active"><a href="javascript:void(0);">地址列表</a></li>
  	</ul>
    <a href="javascript:void(0)" class="ncbtn ncbtn-bittersweet" onClick="toAdd();" title="新增地址"><i class="icon-map-marker"></i>新增地址</a>
    <!--<a href="javascript:void(0)" class="ncbtn ncbtn-bittersweet" style="right:100px" title=""><i class="icon-flag"></i>使用门店代收服务</a>-->
  </div>
  <div class="alert alert-success">
    <h4>操作提示：</h4>
    <ul>
      <li>最多可保存20个有效地址</li>
    </ul>
  </div>
  <table class="ncm-default-table" >
    <thead>
      <tr>
        <th class="w80">收货人</th>
        <th class="w150">所在地区</th>
        <th class="tl">街道地址</th>
        <th class="w120">电话/手机</th>
        <th class="w100"></th>
        <th class="w110">操作</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${list }" var="addr">
      <tr class="bd-line">
        <td>${addr.receiverName }</td>
        <td><c:if test="${addr.receiverState != null}">${addr.state[1] } ${addr.city[1] } ${addr.district[1] }</c:if></td>
        <td class="tl">${addr.receiverAddress }</td>
        <td>
        	<c:if test="${!empty addr.receiverPhone }"><p><i class="icon-phone"></i>${addr.receiverPhone }</p></c:if>
        	<p><i class="icon-mobile-phone"></i>${addr.receiverMobile }</p>
        </td>
        <td class="ncm-table-handle">
          <span><a href="javascript:void(0);" class="btn-bluejeans" onClick="toUptate('${addr.addrId }');">
                   <i class="icon-edit"></i><p>编辑</p></a></span>
          <span><a href="javascript:void(0)" class="btn-grapefruit" onClick="delOne('${addr.addrId }');">
                   <i class="icon-trash"></i><p>删除</p></a></span>
        </td>
        <c:if test="${addr.df == 1 }"><td><i class="icon-ok-sign green" style="font-size:18px"></i>默认地址</td></c:if>
		<c:if test="${addr.df != 1 }"><td></td></c:if>
      </tr>
    </c:forEach>
    </tbody>
  </table>
</div>
</div>
<div class="clear"></div>
</div>
<script src="/static/shop/js/jquery.ajaxContent.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="/cpts/layer-v3.1.1/layer.js"></script>
<script type="text/javascript">
$(function(){
	$('.submit').click(function(){
		if($('#is_default').attr('checked') == 'checked')   $('#is_default').val(1);
		$('[name="receiverState"]').val($('[name="rState"]').val());
		$('[name="receiverCity"]').val($('[name="rCity"]').val());
		$('[name="receiverDistrict"]').val($('[name="rDistrict"]').val());
		$.post($('#address_form').attr("action"), $("#address_form").serialize(), function(data) {
				if (data.status == 200) {
					document.location.reload();
				}
			}, "json"
		);
	})
	$('#subclose').on('click',function(){
		layer.close(layeri);
	})
})
function toAdd(){
	clform();
	$('#address_form').attr("action", "/shop/index/addr/add");
	$('.submit').val("添加地址");
	layeri = layer.open({
		type: 1,
		title: false,
		area: ['553px', '420px'],
		shade: 0.2,
		shadeClose: true,
		content: $('#fwin_my_address_edit'),
	});
}
function toUptate(addrId){
	clform();
	$('#sel_province').click(function(){
		$('#r_area').hide();
	})
	$.post("/shop/index/addr/toUpdate", {"id":addrId}, function(data) {
		if (data.status == 200) {
			var addr = data.data;
			$('#address_form').attr("action", "/shop/index/addr/update");
			$('input[name="addrId"]').val(addr.addrId);
			$('input[name="userId"]').val(addr.userId);
			$('input[name="receiverName"]').val(addr.receiverName);
			$('input[name="receiverState"]').val(addr.receiverState);
			$('input[name="receiverCity"]').val(addr.receiverCity);
			$('input[name="receiverDistrict"]').val(addr.receiverDistrict);
			if(addr.receiverState != ""){
				$('#_area_span').text(addr.state[1]+" "+addr.city[1]+" "+addr.district[1]);
			}
			$('input[name="receiverAddress"]').val(addr.receiverAddress);
			$('input[name="receiverPhone"]').val(addr.receiverPhone);
			$('input[name="receiverMobile"]').val(addr.receiverMobile);
			if(addr.df == 1){
				$('input[name="df"]').val(1);
				$('input[name="df"]').attr('checked','true');
			}
			$('.submit').val("编辑地址");
			//layer
			layeri = layer.open({
				type: 1,
				title: false,
				area: ['553px', '420px'],
				shade: 0.2,
				shadeClose: true,
				content: $('#fwin_my_address_edit'),
			});
		}
	}, "json");
}
function delOne(addrId){
	if(confirm('确实要删除吗?')){
		$.post("/shop/index/addr/delete", {"ids":addrId}, function(data) {
			if (data.status == 200) {
				window.location.reload();
			}
		}, "json");
	}
}
function clform(){
	$('[name="addrId"]').val('');
	$('[name="userId"]').val('');
	$('[name="receiverName"]').val('');
	$('[name="receiverState"]').val('');
	$('[name="receiverCity"]').val('');
	$('[name="receiverDistrict"]').val('');
	$('#_area_span').text('');
	$('[name="receiverAddress"]').val('');
	$('[name="receiverPhone"]').val('');
	$('[name="receiverMobile"]').val('');
	$('[name="df"]').removeAttr('checked');
}
</script>
<%@ include file="../footer.jsp" %>


<!-- toUptate -->
<div id="fwin_my_address_edit" class="dialog_wrapper shop_nc ui-draggable" style="z-index:1100;position:absolute;width:550px;display:none;">
<div class="dialog_body"><div class="dialog_content" style="margin:20px 0 0 0;padding:0px"><div class="eject_con">
  <div class="adds">
    <form action="" id="address_form" target="_parent" method="post">
      <input type="hidden" name="addrId" value="" />
      <input type="hidden" name="userId" value="" />
      <dl>
        <dt><i class="required">*</i>收货人：</dt>
        <dd>
          <input type="text" class="text w100" name="receiverName" value="" />
          <p class="hint">请填写您的真实姓名</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>所在地区：</dt>
        <dd style="height:33px">
	        <select name="rState" id="sel_province"><option value="">选择地区</option></select>
			<select name="rCity" id="sel_city"></select>
			<select name="rDistrict" id="sel_area"></select>
		<span id="r_area">
			<input type="hidden" name="receiverState" value="" />
        	<input type="hidden" name="receiverCity" value="" />
        	<input type="hidden" name="receiverDistrict" value="" />
        	<span id="_area_span" class="_region_value"></span><!-- 区域显示 -->
        <span>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>街道地址：</dt>
        <dd>
          <input class="text w300" type="text" name="receiverAddress" value="" />
          <p class="hint">不必重复填写地区</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>电话号码：</dt>
        <dd>
          <input type="text" class="text w200" name="receiverPhone" value="" />
          <p class="hint">区号 - 电话号码 - 分机</p>
        </dd>
      </dl>
      <dl>
        <dt><i class="required">*</i>手机号码：</dt>
        <dd><input type="text" class="text w200" name="receiverMobile" value="" /></dd>
      </dl>
      <dl>
        <dt><em class="pngFix"></em>默认地址：</dt>
        <dd>
          <input type="checkbox" class="checkbox vm mr5" name="df" id="is_default" value="" />
          <label for="is_default">设置为默认收货地址</label>
        </dd>
      </dl>
        <div class="bottom">
          <label class="submit-border"><input type="button" class="submit" value="" /></label>
          <a class="ncbtn ml5" id="subclose" href="javascript:void(0);">取消</a>
        </div>
    </form>
  </div>
</div></div></div></div>



<script type="text/javascript">
$(document).ready(function(){
	// 加载省市联动
	$('#sel_city').hide();
	$('#sel_area').hide();
	//自动加载联动一级菜单
	$.ajax({
		url: "/area/getProvince",
		success: function(data) {
			if (data.status != 200) return;
			var html = "";
			for(var e in data.data){
				html += '<option value='+data.data[e].id+'&'+data.data[e].name+'>'+data.data[e].name+'</option>';
			}
			$("#sel_province").append(html);
			//通过change方法调用
			$("#sel_province").change(function(){
				$("#sel_city").html("");
				$("#sel_area").html("");
			  //$('#sel_city').hide();
			  //$('#sel_area').hide();
			  	var arr = $(this).val().split("&");
				getCity(arr[0]); //parentId
			});
		},
		dataType: "json"
	})
	function getCity(id){
		//加载二级菜单
		$.ajax({
			type: "post",
			url: "/area/getCity",
			data: {"parentId" : id},
			success: function(data) {
				if (data.status != 200) return;
				var html = "";
				for(var e in data.data){
					html += '<option value='+data.data[e].id+'&'+data.data[e].name+'>'+data.data[e].name+'</option>';
				}
				$("#sel_city").append(html);
				$('#sel_city').show();
				//默认加载第一项
				$("#sel_city").get(0).selectedIndex = 0; //选中项为第0项
				if ($("#sel_city").val() != null)        //防止港澳台等二三级bug*	
					getArea($("#sel_city").val().split("&")[0]);       //parentId   此3行放开为默认加载二三级,不放开一级一级加载★
				//通过change方法调用
				$("#sel_city").change(function(){
					$("#sel_area").html("");
					var arr = $(this).val().split("&");
					getArea(arr[0]);
				});
			},
			dataType: "json"
		});
	}
	function getArea(id){
		//加载三级菜单
		$.ajax({
			type: "post",
			url: "/area/getArea",
			data: {"parentId" : id},
			success: function(data) {
				if (data.status != 200) return;
				var html = "";
				for(var e in data.data){
					html += '<option value='+data.data[e].id+'&'+data.data[e].name+'>'+data.data[e].name+'</option>';
				}
				$("#sel_area").append(html);
				$('#sel_area').show();
			},
			dataType: "json"
		});
	}
});
</script>

</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>文章编辑</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 富文本编辑器 -->
<link rel="stylesheet" href="/shopx5/plugins/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="/shopx5/plugins/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="/shopx5/plugins/kindeditor/lang/zh_CN.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>

<script type="text/javascript" src="/shopx5/js/controller/xfArticlesController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/xfArticlesService.js"></script>

<script type="text/javascript" src="/shopx5/js/service/uploadService2.js"></script> <!-- 上传 -->
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5" ng-controller="xfArticlesController" ng-init="findOne()">
	<!-- 正文区域 -->	
	<section class="content">
		<div class="box-body">
			<!--tab页-->
			<div class="nav-tabs-custom">
				<!--tab头-->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#home" data-toggle="tab">文章编辑</a></li>
					<li><a href="#pic_upload" data-toggle="tab">预留</a></li>
				</ul>
				<!--tab内容-->
				<div class="tab-content">
					<!--表单内容-->
					<div class="tab-pane active" id="home">
						<div class="row data-type">
							<div class="col-md-2 title">标题</div>
							<div class="col-md-10 data">
								<input type="text" ng-model="entity.title"
									class="form-control" placeholder="文章标题" value="">
							</div>
							
							<div class="col-md-2 title editer">正文</div>
							<div class="col-md-10 data editer">
								<textarea name="content"
									style="width: 800px; height: 400px; visibility: hidden;"></textarea>
							</div>
							
							<div class="col-md-2 source">来源</div>
							<div class="col-md-10 data">
								<input type="text" ng-model="entity.source"
									class="form-control" placeholder="文章来源" value="">
							</div>
							
<!-- 							<div class="col-md-2 source">标签</div>
							<div class="col-md-10 data">
								<input type="text" ng-model="entity.source"
									class="form-control" placeholder="标签" value="">
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="btn-toolbar list-toolbar">
			<button class="btn btn-primary" ng-click="save()"><i class="fa fa-save"></i>保存</button>
		</div>
	</section>
	
	<!-- 正文区域 /-->
	<script type="text/javascript">
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="content"]', {
				allowFileManager : true
			});
		});
	</script>
</body>
</html>



//购物车控制层
app.controller('cartController',function($scope,$location,cartService){
	
	//查询购物车列表 1
	$scope.findCartList = function(){
		cartService.findCartList().success(
			function(response){
				$scope.cartList=response;
				if($scope.itemIdsNo.length<=0){
					$scope.totalValue=cartService.sum($scope.cartList); //{totalNum:0,totalMoney:0}总数,总金额
				}else{
					$scope.totalValue=cartService.sumCk($scope.cartList,$scope.itemIdsNo); //求选中的合计数 #
				}
				$scope.totalAll=cartService.sum($scope.cartList); //对比状态 全选
				//封装所有已选itemIds
				$scope.itemIdsYes=cartService.sumIds($scope.cartList);
			}
		);
	}
	//查询购物车已选列表
	$scope.findCartYesList = function(){
		cartService.findCartYesList().success(
			function(response){
				$scope.cartList=response;
				$scope.totalValue=cartService.sum($scope.cartList); //{totalNum:0,totalMoney:0}总数,总金额
			}
		);
	}
	
	//数量加减 2
	$scope.addGoodsToCartList = function(itemId,num){
		cartService.addGoodsToCartList(itemId,num).success(
			function(response){
				if(response.success){//如果成功
					$scope.findCartList();//刷新列表
				}else{
					alert(response.message);
				}
			}
		);
	}
	
	//获取当前用户的地址列表 3
	$scope.findAddressList = function(){
		cartService.findAddressList().success(
			function(response){
				$scope.addressList=response;
				for(var i=0;i<$scope.addressList.length;i++){
				  //if($scope.addressList[i].isDefault=='1'){ //获取默认地址
					if($scope.addressList[i].df=='1'){ //获取默认地址
						$scope.address=$scope.addressList[i];
						break;
					}
				}
			}
		);
	}
	//选择地址 3.1
	$scope.selectAddress = function(address){
		$scope.address=address;
	}
	//判断某地址对象是不是当前选择的地址 3.2
	$scope.isSeletedAddress = function(address){
		if(address==$scope.address){
			return true;
		}else{
			return false;
		}
	}
	
	//订单对象 支付方式 4
	$scope.order = {paymentType:'1'}; //默认值,不点击直接下一步
	
	//选择支付类型 4.1
	$scope.selectPayType = function(type){
		$scope.order.paymentType=type;
	}
	$scope.checkPayType = function(id){
		if($scope.order.paymentType==id){
			return true;
		}
		return false;
	}
	
	//保存订单 5
	$scope.orderIds = $location.search()['orderIds'];
	$scope.orderId = $location.search()['orderId'];
	$scope.submitOrder = function(){
		if($scope.address.receiverMobile==null) alert("请填写收货地址!");
		$scope.order.receiverAreaName=$scope.address.receiverAddress; //地址
		$scope.order.receiverMobile  =$scope.address.receiverMobile;  //手机
		$scope.order.receiver        =$scope.address.receiverName;    //联系人
		cartService.submitOrder($scope.order).success(                //支付类型 {paymentType:'1'};  4个参数
			function(response){
				if(response.flag){
					//页面跳转
					if($scope.order.paymentType=='1'){//如果是微信支付，跳转到支付页面
						location.href="/shop/confirm_pay.htm#?outTradeNo="+response.message;
						//location.href="/shop/pay.htm";
					}else{//如果货到付款，跳转到提示页面
						location.href="/shop/paysuccess.htm";
					}
				}else{
					alert(response.message); //也可以跳转到提示页面
				}
			}
		);
	}
	

	// 复选框All 计算总价格>>>
	$scope.updateAll = function($event){
		// 全选
		if($event.target.checked){
			$scope.findCartList();
			$scope.itemIdsNo = [];
		}else{
			// 全不选
			$scope.totalValue = {"totalNum":0,"totalMoney":0};
			$scope.itemIdsNo = cartService.sumIds($scope.cartList);
			$scope.itemIdsYes = [];
			
		}
	}
	
	// 复选框  计算总价格>>>
	$scope.updateSelection = function($event,item){
		// itemIdsNo注入
		var id = item.itemId;
		var i = $scope.itemIdsNo.indexOf(id);
		var j = $scope.itemIdsYes.indexOf(id);
		if(i == -1){
			$scope.itemIdsNo.push(id); //无id注入
			$scope.itemIdsYes.splice(j,1); //删除id
		}else{ //再次删除
			$scope.itemIdsNo.splice(i,1);
			$scope.itemIdsYes.push(id); //增加id不规格,结算页过滤按数序展示
		}
		
		// 增加 数量+价格
		if($event.target.checked){ //选中时
			$scope.totalValue.totalNum = $scope.totalValue.totalNum + item.num;
			$scope.totalValue.totalMoney = $scope.totalValue.totalMoney + item.totalFee;
			if($scope.totalValue.totalMoney > 0){
				angular.element('#next_submit').addClass('ok');
			}
			testValue();
		}else{
			// 减除 数量+价格
			$scope.totalValue.totalNum = $scope.totalValue.totalNum - item.num;
			$scope.totalValue.totalMoney = $scope.totalValue.totalMoney - item.totalFee;
			if($scope.totalValue.totalMoney <= 0){
				angular.element('#next_submit').unbind('click').removeClass('ok');
			}
			testValue();
		}
	}
	
	//校验是否全选
	testValue = function(){
		if($scope.totalValue.totalNum == $scope.totalAll.totalNum){
			angular.element('#selectAll').attr('checked',true);
		}else{
			angular.element('#selectAll').attr('checked',false);
		}
	}
	
	//校验是否清除单选
	$scope.checkItemId = function(itemId){
		if($scope.itemIdsNo.indexOf(itemId) == -1){
			return true;
		}else{
			return false;
		}
	}
	
	//结算页 进入 3
	$scope.orderInfo = function(){
		if($scope.itemIdsYes.length==0){
			alert("请先选择商品 !");return;
		}
		//	1.将itemIdsYes加入当前用户redis缓存中
		cartService.addItemIdsYes($scope.itemIdsYes).success(
			function(response){
				if(response.success){
					//加入成功,跳转结算页
					location.href="/shop/confirm_order.htm";
				}else{
					location.href="/page/login?url=/shop/cart.htm";
				}
			}
		);
	}
	// 地址更新: 新增/修改
	$scope.addr_save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.address.addrId != null){
			object = cartService.addrUpdate($scope.address);//修改
		}else{
			object = cartService.addrAdd($scope.address);  //新增
		}
		object.success(function(response){
			if(response.status==200){
				alert(response.data);//更新成功
				$scope.findAddressList();    //刷新地址列表
			}else{
				alert(response.data);//更新失败
			}
		});
	}
	//检测
	$scope.$watch("itemIdsYes.length",function(newValue,oldValue){
		if(newValue == 0){
			$scope.totalValue.totalMoney=0;
			angular.element('.ncc-next-submit').removeClass('ok');
		}else{
			angular.element('.ncc-next-submit').addClass('ok');
		}
	});
	$scope.$watch("totalValue.totalMoney",function(newValue,oldValue){
		if(newValue > 50.00){
			$scope.payment = (newValue-50.00).toFixed(2);
		}
		if(newValue == 0){
			angular.element('#next_submit').removeClass('ok');
		}
	});
	
	//获取支付日志 5
	$scope.findPayData = function(){
		//提交到支付
		cartService.findPayData().success(
			function(response){
				$scope.payData=response;
			}
		);
	};
	//页面进入
	$scope.loadPayGrade = function(){
		$scope.orderId = $location.search()['orderId'];
		if($scope.orderId==null){
			$scope.findPayData();
		}else{
			cartService.findOrder($scope.orderId).success(
				function(response){
					$scope.payData=response;
				}
			);
		}
	}
	
	//删除
	$scope.dropCartItem = function(itemId){
		cartService.dropCartItem(itemId).success(
			function(response){
				if(response.success){
					alert(response.message);
					window.location.reload();
				}else{
					alert(response.message);
				}
			}
		);
	};
	
	//收藏
	$scope.collectGoods = function(goodId){
		cartService.collectGoods(goodId).success(
			function(response){
				if(response){
					alert("收藏成功!");
				}else{
					alert("收藏失败!");
				}
			}
		);
	};
	
});















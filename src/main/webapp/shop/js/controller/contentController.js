// 定义控制器:
app.controller("contentController",function($scope,contentService){
	
	// 轮播图[]
	$scope.contentList = [];
	// 根据分类ID获取轮播图
	$scope.findByCategoryId = function(categoryId){
		contentService.findByCategoryId(categoryId).success(function(response){
			if(response.status==200){
				$scope.contentList[categoryId] = response.data;
			}
		});
	}
	
	//搜索(传递参数)
	$scope.search = function(){
		location.href="/shop/search.htm#?keywords="+$scope.keywords;
	}
});

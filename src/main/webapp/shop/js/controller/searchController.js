app.controller('searchController', function($scope,$location,searchService){
	
	// 定义搜索对象结构
	$scope.searchMap = 
	{'keywords':'','category':'','brand':'','spec':{},'price':'','pageNo':1,'pageSize':40,'sort':'','sortField':''};
	
	// 搜索 1
	$scope.search = function(pageNo){
		$scope.searchMap.pageNo = parseInt($scope.searchMap.pageNo); //转换为数字
		searchService.search($scope.searchMap).success( //searchMap对象作为data查询         ###### 1
			function(response){
				$scope.resultMap = response; //map{rows集合,totalPages总页,total总条,categoryList分类列表,
				//构建分页栏  3                      brandList品牌,specList规格}
				buildPageLabel();
				$scope.searchMap.pageNo = 1; //查询后显示第一页
				if(pageNo != null){
					$scope.searchMap.pageNo = pageNo; //分页设置选择页
				}
			}
		);
	}
	
	// 分页栏 3.1
	buildPageLabel = function(){
		//定义页码
		$scope.pageLabel = [];
		var firstPage = 1;//开始页码
		var lastPage  = $scope.resultMap.totalPages;//截止页码 "总页数"
		$scope.firstDot = true;//前面有点
		$scope.lastDot  = true;//后边有点
		if($scope.resultMap.totalPages > 5){//如果页码数量大于5
			if($scope.searchMap.pageNo <= 3){//如果当前页码小于等于3,显示前5页
				lastPage = 5;
				$scope.firstDot = false;//前面没点
			}else if($scope.searchMap.pageNo >= $scope.resultMap.totalPages-2){//显示后5页
				firstPage = $scope.resultMap.totalPages-4;
				$scope.lastDot = false;//后边没点
			}else{//显示以当前页为中心的5页 前后都有点
				firstPage = $scope.searchMap.pageNo-2;
				lastPage  = $scope.searchMap.pageNo+2;
			}
		}else{
			$scope.firstDot = false;//前面无点
			$scope.lastDot  = false;//后边无点
		}
		//构建页码
		for(var i=firstPage; i<=lastPage; i++){
			$scope.pageLabel.push(i);
		}
	}
	
	// 添加搜索项 2.1   改变searchMap值
	$scope.addSearchItem = function(key,value){
		if(key=='category' || key=='brand' || key=='price'){//若点击分类,品牌,价格
			$scope.searchMap[key] = value;
		}else{//点击的是规格
			$scope.searchMap.spec[key] = value;		
		}
		$scope.search();//查询
	}
	
	// 撤销搜索项 2.2
	$scope.removeSearchItem = function(key){
		if(key=='category' || key=='brand' || key=='price' ){//若点击分类,品牌,价格
			$scope.searchMap[key] = "";
		}else{//用户点击的是规格
			delete $scope.searchMap.spec[key];		
		}
		$scope.search();//查询
	}
	
	// 分页查询 3.2
	$scope.queryByPage = function(pageNo){
		if(pageNo<1 || pageNo>$scope.resultMap.totalPages){
			return ;
		}
		$scope.searchMap.pageNo = pageNo;
		$scope.search(pageNo);//查询
	}
	// 是否为第一页 3.3
	$scope.isTopPage = function(){
		if($scope.searchMap.pageNo==1){
			return true;
		}else{
			return false;
		}		
	}
	// 是否为最后一页 3.4
	$scope.isEndPage = function(){
		if($scope.searchMap.pageNo==$scope.resultMap.totalPages){
			return true;
		}else{
			return false;
		}	
	}
	
	// 排序查询 4
	$scope.sortSearch = function(sortField,sort){
		if($scope.searchMap.sortField=="prices" && $scope.searchMap.sort=="ASC" ){
			$scope.searchMap.sort = "DESC";
		}else if($scope.searchMap.sortField=="updatetime" && $scope.searchMap.sort=="DESC" ){
			$scope.searchMap.sort = "ASC";
		}else{
			$scope.searchMap.sortField=sortField;
			$scope.searchMap.sort=sort;
		}
		$scope.search();//查询
		
		
	}
	// 0默认 1销量 2人气 3价格
	$scope.sortbar = 0;
	$scope.isSelected = function(id){
		angular.element('.nch-sortbar-array ul li').removeClass('selected');
		var name;
		if(id == 0) name = '默认';
		if(id == 1) name = '销量';
		if(id == 2) name = '人气';
		if(id == 3) name = '价格';
		if($scope.sortbar==id){
			$scope.sortbar = 0;
			angular.element('.nch-sortbar-array ul li:contains("'+name+'")').addClass('selected');
			angular.element('.nch-sortbar-array ul li i:contains("'+name+'排序")').removeClass('up');
		}else{
			$scope.sortbar = id;
			angular.element('.nch-sortbar-array ul li:contains("'+name+'")').addClass('selected');
			angular.element('.nch-sortbar-array ul li i:contains("'+name+'排序")').addClass('up');
			return;
		}
	}
	
	// image 图片触发
	$scope.showPic = function(goodsId,image,index){
		angular.element('#'+goodsId).find('img').attr('src',image.url);
	  //angular.element('#'+goodsId).find('a').attr('href','/shop/'+goodsId+'.htm#?itemId='+image.itemId+'');
		angular.element('.goods-pic-scroll-show ul li').removeClass('selected');
		angular.element('.goods-pic-scroll-show ul li:eq('+index+')').addClass('selected');
	}
	
	// 判断关键字是否是品牌 5
	$scope.keywordsIsBrand = function(){
		for(var i=0; i<$scope.resultMap.brandList.length; i++){
			if($scope.searchMap.keywords.indexOf($scope.resultMap.brandList[i].text) >= 0){
				return true;
			}
		}
		return false;
	}
	
	// 首页搜索进入 6  "http://localhost:9104/search.htm#?keywords=" + $scope.keywords;
	// 注意单独打开搜索页面 此处报空指针
	$scope.loadkeywords = function(){
		$scope.searchMap.keywords = $location.search()['keywords'];
		$scope.search();//查询
	}
	
	// 品牌显示筛选
	$scope.brandshow = function(brand_show){
		var div=angular.element('ul[nctype="ul_initial"]');
		if(div.is(':visible')) {
			div.hide();
			angular.element('ul[nctype="ul_brand"] > li:lt(14)').show(); // < 14
			angular.element('ul[nctype="ul_brand"] > li:gt(13)').hide();
			angular.element('span[nctype="brand_show"]').html('<i class="icon-angle-down"></i>更多');
		}else{
			div.show();
			angular.element('ul[nctype="ul_brand"] > li').show();
			angular.element('span[nctype="brand_show"]').html('<i class="icon-angle-up"></i>收起');
		}
	}
	// 品牌按首字母切换
	$scope.brandmouse = function(id){
		var div = angular.element('li[data-initial="'+id+'"]');
		div.addClass('current').siblings().removeClass('current');
		if (div.attr('data-initial') == 'all') {
			$('ul[nctype="ul_brand"] > li').show();
			return;
		}
		$('ul[nctype="ul_brand"] > li').hide();
		$('ul[nctype="ul_brand"] > li[data-initial="'+id+'"]').show();
	}


});


















// 定义控制器:
app.controller("sellerController",function($scope,$controller,$http,sellerService){
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		sellerService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		sellerService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		sellerService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
		});
	}
	
	// 更新: 增加/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = sellerService.update($scope.entity);//修改
		}else{
			object = sellerService.add($scope.entity);   //增加
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag){
				alert(response.message);//更新成功
				$scope.reloadList();    //刷新列表
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		sellerService.findOne(id).success(function(response){ //{id:xx,name:yy,firstChar:zz}
			$scope.entity = response;
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		sellerService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	// 增加商户
	$scope.addSeller = function(){
		sellerService.add($scope.entity).success(function(response){
			if(response.flag){
				// 成功进登录页
				location.href="/admin/login";
			}else{
				alert(response.message);
			}
		});	
	}
});




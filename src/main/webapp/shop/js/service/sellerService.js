// 定义服务层:
app.service("sellerService",function($http){
	//所有列表
	this.findAll = function(){
		return $http.get("../seller/findAll");
	}
	//分页列表
	this.findPage = function(page,rows){
		return $http.get("../seller/findPage?page="+page+"&rows="+rows);
	}
	//分页列表 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("../seller/search?page="+page+"&rows="+rows,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/seller/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("../seller/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("../seller/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("../seller/delete?ids="+ids);
	}
	//自定义
	this.selectOptionList = function(){
		return $http.get("../seller/selectOptionList");
	}
	
});

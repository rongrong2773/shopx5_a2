// 定义服务层:
app.service("commentService",function($http){
	//所有列表
	this.findAll = function(){
		return $http.get("/shopx5/comment/findAll");
	}
	//分页列表
	this.findPage = function(page,rows){
		return $http.get("/shopx5/comment/findPage?page="+page+"&rows="+rows);
	}
	//分页列表 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/comment/search?page="+page+"&rows="+rows,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/comment/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/shopx5/comment/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/shopx5/comment/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/comment/delete?ids="+ids);
	}
	//自定义
	
	//所有列表
	this.findByGoodsId = function(goodsId){
		return $http.get("/shopx5/comment/findByGoodsId?goodsId="+goodsId);
	}
	
});

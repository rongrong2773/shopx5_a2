// 定义模块:
var app = angular.module("shopx5", []);

//定义过滤器 search
app.filter('trustHtml', ['$sce', function($sce) {
	return function(data) {            //传入参数时被过滤的内容
		return $sce.trustAsHtml(data); //返回的是过滤后的内容（信任html的转换）
	}}]                                //<h1 ng-bind-html="item.title | trustHtml"></h1> 过滤显示代码
);

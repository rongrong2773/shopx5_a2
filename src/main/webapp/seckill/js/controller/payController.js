app.controller('payController', function($scope,$location,payService){
	
	//创建二维码 1
	$scope.createNative = function(){
		payService.createNative().success(
			function(response){
				//显示订单号和金额
				$scope.out_trade_no = response.out_trade_no;
				$scope.money = (response.total_fee/100).toFixed(2);
				$scope.wx_pay_url = response.code_url;

				//生成二维码
				var qr = new QRious({
					    element: document.getElementById('qrious'),
						size: 250,
						level: 'H',
						value: response.code_url
			     });
				 queryPayStatus(); //查询状态
			}
		);
	}
	
	//查询状态 2
	queryPayStatus = function(){
		payService.queryPayStatus($scope.out_trade_no).success(
			function(response){
				if(response.success){
					location.href = "/seckill/paysuccess.htm#?money="+$scope.money;
				}else{
					if(response.message == '二维码超时'){
						$scope.createNative(); //重新生成二维码
					}else{
						location.href = "/seckill/payfail.htm";
					}
				}
			}
		);
	}
	
	//成功页, 显示支付金额
	$scope.getMoney = function(){
		return $location.search()['money'];
	}
	
});



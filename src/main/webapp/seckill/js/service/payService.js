app.service('payService',function($http){
	//本地支付
	this.createNative = function(){
		return $http.get('/shop/pay2/createNative');
	}
	
	//查询支付状态
	this.queryPayStatus = function(out_trade_no){
		return $http.get('/shop/pay2/queryPayStatus?out_trade_no='+out_trade_no);
	}
});
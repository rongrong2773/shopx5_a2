<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="ncc-form-default">
  <form id="addr_form" method="post">
    <dl>
      <dt><i class="required">*</i>收货人姓名：</dt>
      <dd>
        <input type="text" class="text w100" name="receiverName" id="receiverName" maxlength="20" value="" />
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>所在地区：</dt>
      <dd>
        <div><!--<input type="hidden" name="region" id="region" />
            	 <input type="hidden" name="city_id" id="_area_2" />
            	 <input type="hidden" name="area_id" id="_area" /> -->
		    <select name="receiverState" id="sel_province">
				<option value="">请选择省</option>
			</select>
			<select name="receiverCity" id="sel_city">
				<!-- <option value="">请选择市</option> -->
			</select>
			<select name="receiverDistrict" id="sel_area">
				<!-- <option value="">请选择区</option> -->
			</select>
        </div>
      </dd>
    </dl>
    <dl>
      <dt><i class="required">*</i>详细地址：</dt>
      <dd>
        <input type="text" class="text w500" name="receiverAddress" id="receiverAddress" maxlength="80" value="" />
        <p class="hint">请填写真实地址，不需要重复填写所在地区</p>
      </dd>
    </dl>
    <dl>
      <dt> <i class="required">*</i>手机号码：</dt>
      <dd>
        <input type="text" class="text w200" id="receiverMobile" name="receiverMobile" maxlength="15" value="" />
           &nbsp;&nbsp;(或)&nbsp;固定电话：
        <input type="text" class="text w200" id="receiverPhone" name="receiverPhone" maxlength="20" value="" />
      </dd>
    </dl>
  </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// 加载省市联动
	$('#sel_city').hide();
	$('#sel_area').hide();
	//自动加载联动一级菜单
	$.ajax({
		url: "/area/getProvince",
		success: function(data) {
			if (data.status != 200) return;
			var html = "";
			for(var e in data.data){
				html += '<option value='+data.data[e].id+'&'+data.data[e].name+'>'+data.data[e].name+'</option>';
			}
			$("#sel_province").append(html);
			//通过change方法调用
			$("#sel_province").change(function(){
				$("#sel_city").html("");
				$("#sel_area").html("");
			  //$('#sel_city').hide();
			  //$('#sel_area').hide();
			  	var arr = $(this).val().split("&");
				getCity(arr[0]); //parentId
			});
		},
		dataType: "json"
	})
	function getCity(id){
		//加载二级菜单
		$.ajax({
			type: "post",
			url: "/area/getCity",
			data: {"parentId" : id},
			success: function(data) {
				if (data.status != 200) return;
				var html = "";
				for(var e in data.data){
					html += '<option value='+data.data[e].id+'&'+data.data[e].name+'>'+data.data[e].name+'</option>';
				}
				$("#sel_city").append(html);
				$('#sel_city').show();
				//默认加载第一项
				$("#sel_city").get(0).selectedIndex = 0; //选中项为第0项
				if ($("#sel_city").val() != null)        //防止港澳台等二三级bug*	
					getArea($("#sel_city").val().split("&")[0]);       //parentId   此3行放开为默认加载二三级,不放开一级一级加载★
				//通过change方法调用
				$("#sel_city").change(function(){
					$("#sel_area").html("");
					var arr = $(this).val().split("&");
					getArea(arr[0]);
				});
			},
			dataType: "json"
		});
	}
	function getArea(id){
		//加载三级菜单
		$.ajax({
			type: "post",
			url: "/area/getArea",
			data: {"parentId" : id},
			success: function(data) {
				if (data.status != 200) return;
				var html = "";
				for(var e in data.data){
					html += '<option value='+data.data[e].id+'&'+data.data[e].name+'>'+data.data[e].name+'</option>';
				}
				$("#sel_area").append(html);
				$('#sel_area').show();
			},
			dataType: "json"
		});
	}	
	
	// validate js检查
	$('#addr_form').validate({
        rules : {
        	receiverName : {
                required : true
            },
            receiver : {
            	checklast: true
            },
            receiverAddress : {
                required : true
            },
            receiverMobile : {
                required : checkPhone,
                minlength : 11,
				maxlength : 11,
                digits : true
            },
            receiverPhone : {
                required : checkPhone,
                minlength : 6,
				maxlength : 20
            }
        },
        messages : {
        	receiverName : {
                required : '<i class="icon-exclamation-sign"></i>请填写收货人姓名'
            },
            receiver : {
            	checklast: '<i class="icon-exclamation-sign"></i>请将地区选择完整'
            },
            receiverAddress : {
                required : '<i class="icon-exclamation-sign"></i>请填写收货人详细地址'
            },
            receiverMobile : {
                required : '<i class="icon-exclamation-sign"></i>手机号码或固定电话请至少填写一个',
                minlength: '<i class="icon-exclamation-sign"></i>手机号码只能是11位',
				maxlength: '<i class="icon-exclamation-sign"></i>手机号码只能是11位',
                digits : '<i class="icon-exclamation-sign"></i>手机号码只能是11位'
            },
            receiverPhone : {
                required : '<i class="icon-exclamation-sign"></i>手机号码或固定电话请至少填写一个',
                minlength: '<i class="icon-exclamation-sign"></i>',
				maxlength: '<i class="icon-exclamation-sign"></i>'
            }
        },
        groups : {
            phone : 'receiverMobile receiverPhone'
        }
	})
	function checkPhone(){
	    return ($('input[name="receiverMobile"]').val() == '' && $('input[name="receiverPhone"]').val() == '');
	}
});

// 4.添加地址表单 load_addr.jsp
function submitAddAddr(){
    if ($('#addr_form').valid()){  //valid验证表单内是否有name
        //$('#buy_city_id').val($('#region').fetch('area_id_2'));  //城市ID(运费)
        // 5.保存收货地址
        var datas = $('#addr_form').serialize();
        $.post('/orderSku/addr', datas ,function(data){
            if (data.status == 200){
                var true_name = $.trim($("#receiverName").val());   //姓名
                var tel_phone = $.trim($("#receiverMobile").val()); //电话
                var mob_phone = $.trim($("#receiverPhone").val());  //手机       //省市区
            	var area_info = $("#sel_province").val().split("&")[1]+$("#sel_city").val().split("&")[1]+$("#sel_area").val().split("&")[1];
            	var address = $.trim($("#receiverAddress").val());  //详细地址
            	
            	// 6.异步显示每个店铺运费 city_id计算运费area_id计算是否支持货到付款 order-cart-sku.jsp
            	//showShippingPrice($('#region').fetch('area_id_2'), $('#region').fetch('area_id')); //fetch在检测★★★
            	
            	// 7.隐藏收货地址列表 order-cart-sku.jsp  id,姓名,地址,电话
            	alert("添加新地址成功!");
            	hideAddrList(data.addrId, true_name, area_info+'&nbsp;&nbsp;'+address, (mob_phone != '' ? mob_phone : tel_phone));
            }else{
                alert("添加失败!");
            }
        },'json');
    }else{
        return false;
    }
}
</script>



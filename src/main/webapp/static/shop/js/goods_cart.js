/**
 * 购物车JS
 */
 
/**
 * 更改购物车数量
 * @param cart_id
 * @param input
 */                      // id  , this
function change_quantity(cart_id, input){
    var jjgId = $('#cart_id' + cart_id).attr('data-jjg') || -1;
    var subtotal = $('#item' + cart_id + '_subtotal'); //小计
    //暂存为局部变量，否则如果用户输入过快有可能造成前后值不一致的问题
    var _value = input.value; //数量 2
    $.post('/cartSku/update/num/'+cart_id+'/'+_value, function(result){
        $(input).attr('changed', _value);        
        if(result.status == 200){
        	$(result.data.cartItemList).each(function(n,i){
        		if(cart_id == i.itemSku.skuId){
        			$('#item' + cart_id + '_price').html(parseFloat(i.itemSku.price / 100).toFixed(2));
                    subtotal.html(parseFloat(i.subtotal / 100).toFixed(2));
                    //$('#cart_id'+cart_id).val(cart_id+'|'+_value);
                    $(input).val(i.count);
                    /*
                    var bl_id = $(input).attr('bl_id');
                    $('em[ncType="blnum'+bl_id+'"]').html(result.goods_num);
                    $('em[ncType="bltotal'+bl_id+'"]').each(function(){
                    	$(this).html((parseFloat($(this).attr('price'))*result.goods_num).toFixed(2));
                    });
                    */
        		}
        	});            
        }
        if(result.state == 'invalid'){
          subtotal.html(0.00);
          $('#cart_id'+cart_id).remove();
          $('tr[nc_group="'+cart_id+'"]').addClass('item_disabled');
          $(input).parent().next().html('');
          $(input).parent().removeClass('ws0').html('已下架');
        }
        if(result.state == 'shortage'){
          $('#item' + cart_id + '_price').html(parseFloat(result.goods_price).toFixed(2));
          $('#cart_id'+cart_id).val(cart_id+'|'+result.goods_num);
          $(input).val(result.goods_num);
          subtotal.html(parseFloat(result.subtotal).toFixed(2));
        }
        if(result.state == '') {
            //更新失败
            $(input).val($(input).attr('changed'));
        }
        calc_cart_price(jjgId);
    }, "json");
}
/**
 * 购物车减少商品数量
 * @param cart_id
 */
function decrease_quantity(cart_id){
    var item = $('#input_item_' + cart_id);
    var orig = Number(item.val());
    if(orig > 1){
        item.val(orig - 1);
        item.keyup();
    }
}
/**
 * 购物车增加商品数量
 * @param cart_id
 */
function add_quantity(cart_id){
    var item = $('#input_item_' + cart_id);
    var orig = Number(item.val());
    item.val(orig + 1);
    item.keyup();
}

/////////////////////

/**
 * 删除购物车
 * @param cart_id
 */
function drop_cart_item(cart_id){
    var jjgId = $('#cart_id' + cart_id).attr('data-jjg') || -1;
    var parent_tr = $('#cart_item_' + cart_id).parent();
    var amount_span = $('#cart_totals');
    if(confirm('确认删除吗?')){
        $.post('/cartSku/delete/'+cart_id, function(result){
            if(result.status == 200){
                //删除成功
                if(result.data.some == 0){//判断购物车是否为空
                    window.location.reload();    //刷新
                } else {
                    $('tr[nc_group="'+cart_id+'"]').remove();//移除本商品或本套装
                    if (parent_tr.children('tr').length == 2) {//只剩下店铺名头和店铺合计尾，则全部移除
                        parent_tr.remove();
                    }
                    calc_cart_price(jjgId);
                }
            }else{
                alert(result.msg);
            }
        });
    };
}

/**
 *收藏商品
 *
 */
function collect_goods(skuId){
	$.post("/collect/add", {"skuId":skuId}, function(data) {
			if (data.status == 200) {
				alert("商品收藏成功!");
			}
		}, "json"
	)
}

/**
 * 购物车 选中商品价格统计
 */
/**
var calc_cart_price = (function() {	
    var realCalculate = function() {
        //每个店铺商品价格小计
        obj = $('table[nc_type="table_cart"]');
        if(obj.children('tbody').length==0) return;
        //购物车已选择商品的总价格
        var allTotal = 0;
        obj.children('tbody').each(function(){
            //购物车每个店铺已选择商品的总价格
            var eachTotal = 0;
            $(this).find('em[nc_type="eachGoodsTotal"]').each(function(){
                if ($(this).parent().parent().find('input[type="checkbox"]').eq(0).attr('checked') != 'checked') return; //当前需被选中
                eachTotal += parseFloat($(this).html());
				eachTotal = parseFloat(eachTotal.toFixed(2));
            });
            allTotal += eachTotal;            //order-cart-sku.jsp                    common.js格式化金额
            $(this).children('tr').last().find('em[nc_type="eachStoreTotal"]').eq(0).html(number_format(eachTotal, 2));
        });
        $('#cartTotal').html(number_format(allTotal, 2));
    };
    
    window.jjgRecalculator = realCalculate;    
    return function(jjgId) { // jjg callback
        if (window.jjgCallback) {
            window.jjgCallback(jjgId);
        }
        realCalculate();
    }
})();
*/

$(function(){
	//初始化 购物车选中的总价格★ -----------------
    //calc_cart_price(0);
    	//全选  3
	//$('#next_submit').addClass('ok');
    $('#selectAll').on('click',function(){
        if ($(this).attr('checked')) {
            $('input[type="checkbox"]').attr('checked',true);           //单选,全选中
            $('input[type="checkbox"]:disabled').attr('checked',false); //不可用元素不选中
            if ($('input[nc_type="eachGoodsCheckBox"]:checked').size() > 0) {
            	//$('#next_submit').on('click',function(){$('#form_buy').submit();}).addClass('ok');
            	$('#next_submit').addClass('ok');
            }
        } else {
            $('input[type="checkbox"]').attr('checked',false);
            $('#next_submit').unbind('click').removeClass('ok');
        }
		//计算总价格
        //calc_cart_price(0);
    });
	
    /**
	//初始化 页面打开 选中后提交按钮效果 1
    if ($('input[nc_type="eachGoodsCheckBox"]:checked').size() == 0) {
    	$('#next_submit').unbind('click');  //默认代码无class="ok",不用removeClass('ok')
    } else {
		//选中1 2 3个
    	$('#next_submit').on('click',function(){$('#form_buy').submit();}).addClass('ok');
    }
    
	//单选/多选/或全选 确认订单 2
    $('input[nc_type="eachGoodsCheckBox"]').on('click',function(){
        var jjgId = $(this).attr('data-jjg') || -1;
		//1.单选未选中
        if (!$(this).attr('checked')) {
            $('#selectAll').attr('checked',false); //单选取消,全选取消*
            if ($('input[nc_type="eachGoodsCheckBox"]:checked').size() == 0) {
            	$('#next_submit').unbind('click').removeClass('ok');//单选未全选中,提交失效*
            }
        //2.单选选中
        } else {
            // 如果都选中 则全选复选框是选中的
            var b = true;                         //非当前        非不可用元素  ,除了当前选中的遍历★
            $('input[nc_type="eachGoodsCheckBox"]').not(this).not(':disabled').each(function() {
                if (!this.checked) { //其他有一个为选中,全选取消选中
                    b = false;
                    return false;    //当前函数内,终止处理  意思是有一个错误了就终止了,效果一样★
                }
            });
            if (b) {
                $('#selectAll').attr('checked', true);
            }
            $('#next_submit').on('click',function(){$('#form_buy').submit();}).addClass('ok');
        }
		//计算总价格
        //calc_cart_price(jjgId);
    });
	*/
    
	/**
	$('#next_submit').on('click',function(){
		if($(document).find('input[nc_type="eachGoodsCheckBox"]:checked').size() == 0){
			showDialog('请选中要结算的商品', 'eror','','','','','','','','',2);
			return false;
		}else{
			$('#form_buy').submit();
		}
	})
    */
});





















/* item.jsp页面 被删js 存放处 */

var $cur_area_list;
var $cur_tab;
var next_tab_id = 0;
var cur_select_area = [];
var calc_area_id = '';
var calced_area = [];
var cur_select_area_ids = [];
$(document).ready(function(){
	//请选择地区
	$("#ncs-freight-selector").hover(function() {
		//如果店铺没有设置默认显示区域，马上异步请求
		if (typeof nc_a === "undefined") {
	 		$.getJSON(SITEURL + "/index.php?act=index&op=json_area&callback=?", function(data) {
	 			nc_a = data;
	 			$cur_tab = $('#ncs-stock').find('li[data-index="0"]');
	 			_loadArea(0);
	 		});
		}
		$(this).addClass("hover");
		$(this).on('mouseleave',function(){
			$(this).removeClass("hover");
		});
	});

	$('ul[class="area-list"]').on('click','a',function(){
		$('#ncs-freight-selector').unbind('mouseleave');
		var tab_id = parseInt($(this).parents('div[data-widget="tab-content"]:first').attr('data-area'));
		if (tab_id == 0) {cur_select_area = [];cur_select_area_ids = []};
		if (tab_id == 1 && cur_select_area.length > 1) {
			cur_select_area.pop();
			cur_select_area_ids.pop();
			if (cur_select_area.length > 1) {
				cur_select_area.pop();
				cur_select_area_ids.pop();
			}
		}
		next_tab_id = tab_id + 1;
		var area_id = $(this).attr('data-value');
		$cur_tab = $('#ncs-stock').find('li[data-index="'+tab_id+'"]');
		$cur_tab.find('em').html($(this).html());
		$cur_tab.find('i').html(' ∨');
		if (tab_id < 2) {
			calc_area_id = area_id;
			cur_select_area.push($(this).html());
			cur_select_area_ids.push(area_id);
			$cur_tab.find('a').removeClass('hover');
			$cur_tab.nextAll().remove();
			if (typeof nc_a === "undefined") {
    	 		$.getJSON(SITEURL + "/index.php?act=index&op=json_area&callback=?", function(data) {
    	 			nc_a = data;
    	 			_loadArea(area_id);
    	 		});
			} else {
				_loadArea(area_id);
			}
		} else {
			//点击第三级，不需要显示子分类
			if (cur_select_area.length == 3) {
				cur_select_area.pop();
				cur_select_area_ids.pop();
			}
			cur_select_area.push($(this).html());
			cur_select_area_ids.push(area_id);
			$('#ncs-freight-selector > div[class="text"] > div').html(cur_select_area.join(''));
			$('#ncs-freight-selector').removeClass("hover");
			_calc();
		}
		$('#ncs-stock').find('li[data-widget="tab-item"]').on('click','a',function(){
			var tab_id = parseInt($(this).parent().attr('data-index'));
			if (tab_id < 2) {
				$(this).parent().nextAll().remove();
				$(this).addClass('hover');
				$('#ncs-stock').find('div[data-widget="tab-content"]').each(function(){
					if ($(this).attr("data-area") == tab_id) {
						$(this).show();
					} else {
						$(this).hide();
					}
				});
			}
		});
	});
	function _loadArea(area_id){
		if (nc_a[area_id] && nc_a[area_id].length > 0) {
			$('#ncs-stock').find('div[data-widget="tab-content"]').each(function(){
				if ($(this).attr("data-area") == next_tab_id) {
					$(this).show();
					$cur_area_list = $(this).find('ul');
					$cur_area_list.html('');
				} else {
					$(this).hide();
				}
			});
			var areas = [];
			areas = nc_a[area_id];
			for (i = 0; i < areas.length; i++) {
				if (areas[i][1].length > 8) {
					$cur_area_list.append("<li class='longer-area'><a data-value='" + areas[i][0] + "' href='#none'>" + areas[i][1] + "</a></li>");
				} else {
				    $cur_area_list.append("<li><a data-value='" + areas[i][0] + "' href='#none'>" + areas[i][1] + "</a></li>");
				}
			}
			if (area_id > 0){
		      $cur_tab.after('<li data-index="'+(next_tab_id)+'" data-widget="tab-item"><a class="hover" href="#none"><em>请选择</em><i> ∨</i></a></li>');
			}
		} else {
			//点击第一二级时，已经到了最后一级
			$cur_tab.find('a').addClass('hover');
			$('#ncs-freight-selector > div[class="text"] > div').html(cur_select_area);
			$('#ncs-freight-selector').removeClass("hover");
			_calc(); //计算运费，是否配送
		}
	}	
	//计算运费，是否配送
	function _calc() {
		$.cookie('dregion', cur_select_area_ids.join(' ')+'|'+cur_select_area.join(' '), { expires: 30 });
		return;
		var _args = "tid=0&goods_id=100540&goods_price=5399.00&area_id=" + calc_area_id+'&num='+$("#quantity").val();
		if (typeof calced_area[calc_area_id] == 'undefined') {
			//需要请求配送区域设置
			$.getJSON(SITEURL + "/index.php?act=goods&op=calc&" + _args + "&myf=0.00&callback=?", function(data){
				allow_buy = data.total ? true : false;
				calced_area[calc_area_id] = data.total;
				if (data.total === false) {
					$('#ncs-freight-prompt > strong').html('无货').next().remove();
					$('a[nctype="buynow_submit"]').addClass('no-buynow');
					$('a[nctype="addcart_submit"]').addClass('no-buynow');
				} else {
					$('#ncs-freight-prompt > strong').html('有货 ').next().remove();
					$('#ncs-freight-prompt > strong').after('<span>' + data.total + '</span>');
					$('a[nctype="buynow_submit"]').removeClass('no-buynow');
					$('a[nctype="addcart_submit"]').removeClass('no-buynow');
				}
			});	
		} else {
			if (calced_area[calc_area_id] === false) {
				$('#ncs-freight-prompt > strong').html('无货').next().remove();
				$('a[nctype="buynow_submit"]').addClass('no-buynow');
				$('a[nctype="addcart_submit"]').addClass('no-buynow');
			} else {
				$('#ncs-freight-prompt > strong').html('有货 ').next().remove();
				$('#ncs-freight-prompt > strong').after('<span>' + calced_area[calc_area_id] + '</span>');
				$('a[nctype="buynow_submit"]').removeClass('no-buynow');
				$('a[nctype="addcart_submit"]').removeClass('no-buynow');
			}
		}
	}
	//如果店铺设置默认显示配送区域
});

$(function(){
	//门店服务
	$('[nctype="show_chain"]').click(function(){
	    _goods_id = $(this).attr('data-goodsid');
	    ajax_form('show_chain', '查看门店', '/index.php?act=goods&op=show_chain&goods_id=' + _goods_id, 640);
	});
	$('.couRuleScrollbar').perfectScrollbar({suppressScrollX:true});

});





document.writeln('<script type="text/javascript" src="/static/shop/js/jquery.qtip.min.js"></script>');
document.writeln('<link href="/static/shop/css/jquery.qtip.min.css" rel="stylesheet" type="text/css" />');
document.writeln('<!-- 加入对比 -->');
document.writeln('<script type="text/javascript" src="/static/shop/js/compare.js"></script>');












































<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>省市区三级联动</title>
<style type="text/css">
	body  { padding-left:400px;padding-top:150px;background-color:#00CC99;}
	select{ width:100px;height:30px;cursor:pointer;} /*background-color:#99FF33;*/
</style>
</head>

<body>

	<input type="button" onclick="areaAdd()" value="添加三级区域" />
	
	<form id="areaForm" action="#">
		<input type="hidden" name="areaJson" value="" />
	</form>

<script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
<link type="text/css" href="/topjui/topjui/css/topjui.core.min.css" rel="stylesheet" />

<script type="text/javascript" src="/js/area.js"></script><!-- 加载json数据:provinceList -->
<script type="text/javascript">
function areaAdd(){
	/* areaJson格式
		var provinceList = [
		{name:'北京', shortName:"京", cityList:[
			{name:'市辖区', areaList:[{name:'东城区'},{name:'西城区'},{name:'怀柔区'},{name:'平谷区'}]},
			{name:'县', areaList:[{name:'密云县'},{name:'延庆县'}]}
			]},		
		{name:'安徽', shortName:"", cityList:[
			{name:'合肥市', areaList:[{name:'瑶海区'},{name:'庐阳区'},{name:'肥东县'},{name:'肥西县'}]},
			{name:'芜湖市', areaList:[{name:'镜湖区'},{name:'马塘区'},{name:'繁昌县'},{name:'南陵县'}]},
			]}
		]
    */
	var area = JSON.stringify(provinceList);
	$("#areaForm [name='areaJson']").val(area);	
	$.ajax({
		type: "post",
		url: "/area/import",
		data : $("#areaForm").serialize(),
		beforeSend : function() {
			$.messager.progress({
				text : '正在新增中...',
			});
		},
		success: function(data) {
			$.messager.progress('close');
			if (data.status == 200) {
				$.messager.show({
					title : '提示',
					msg : data.data,
				});
				//alert(data.data);
			}
		},
		dataType: "json"
	});	
}
</script>
</body>
</html>

﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>登录</title>
<script src="/static/shop/js/jquery.js"></script>
<style type="text/css">
.hide-center{width:308px;height:225px;position:absolute;right:0;left:0;bottom:0;top:0;margin:auto;z-index:1}
.hide-center #formhead{width:300px;height:30px;margin:0;padding-top:7px;/*border-top-left-radius:10px;border-top-right-radius:10px;*/background-color:#FFF;
border:1px solid #CCC;border-bottom:0;overflow:hidden}
.hide-center #formhead-title{width:100px;height:24px;margin-left:106px;margin-right:36px;color:#888;font-weight:900;display:inline-block}
.hide-center #formbody{border:1px solid #CCC;width:300px;height:185px;background-color:#FFF;/*border-bottom-left-radius:10px;border-bottom-right-radius:10px;*/
overflow:hidden}
.hide-center .loginHead{margin-right:-8px;padding:10px;width:25px;height:28px;background-color:#FFF;vertical-align:middle}
.hide-center .loginInput{height:45px;width:230px;padding-left:10px;border:1px solid #CCC;background-color:#FFF;color:#000}
.hide-center .passInput{border-top:0}
.hide-center .loginUserName{padding-left:30px;padding-top:18px}
.hide-center .loginPassword{padding-left:30px}
.hide-center #formfoot{margin-top:15px;margin-left:30px}
.hide-center #BSignIn{border:0;background:#329d90;color:#fff;width:240px;height:40px;border-bottom-left-radius:5px;border-bottom-right-radius:5px;
border-top-left-radius:5px;border-top-right-radius:5px}
</style>
</head>

<body>
<!-- 登录框 -->
<div class="hide-center">
<form id="login_form">
<input type="hidden" name="sclog" value="sclog" />
    <div id="formhead">
        <div id="formhead-title">用户登录</div>
    </div>
    <div id="formbody">
        <div class="loginUserName">
            <input id="input-topright-loginInput" name="userName" class="loginInput" placeholder="用户名" type="text" />
        </div>
        <div class="loginPassword">
            <input id="input-bottomright-loginInput" name="password" class="loginInput passInput" placeholder="密码" type="password"
            style="border-bottom-right-radius:5px" />
        </div>
        <div id="formfoot">
            <button id="BSignIn" type="submit">登录</button>
        </div>
    </div>
</form>
</div>
<script type="text/javascript">
$(function(){
	var index;
	$('#BSignIn').on('click', function(){
		if($('.loginInput').val() == "" || $('.passInput').val() == ""){
			alert("用户名或密码不能为空!");
			return;
		}
		$.ajax({
		    type: "post",
		    url: "/user/login",
		    async: false,
		    data: $("#login_form").serialize(),
		    success: function(data) {
				if (data.status == 200) {
					index = window.parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭
					parent.location.href = parent.location.href;
				} else {
					alert(data.msg);
				}
		    },
		    dataType: "json"
		});
	
	});
});
</script>
</body>
</html>



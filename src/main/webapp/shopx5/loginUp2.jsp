﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>登录</title>
<script src="/static/shop/js/jquery.js"></script>
</head>

<body>

<div class="quick-login">
	<form class="bg" method="post" id="ajax_login" action="">
		<dl>
			<dt>用户名</dt>
			<dd>
				<input type="text" name="userName" autocomplete="off" class="text">
			</dd>
		</dl>
		<dl>
			<dt>密&nbsp;&nbsp;&nbsp;码</dt>
			<dd>
				<input type="password" autocomplete="off" name="password" class="text">
			</dd>
		</dl>
		<dl>
			<dt>验证码</dt>
			<dd>
				<input type="text" size="10" maxlength="4" class="text fl w60" name="captcha"><img border="0"
					onclick="this.src='/code?type=login&n=2' + Math.random()" name="codeimage" title="看不清，换一张"
					src="/code?type=login&n=2" class="fl ml10"><span>不区分大小写</span>
			</dd>
		</dl>
		<ul>
			<li>› 如果您没有登录账号，请先<a class="register" href="">注册会员</a>然后登录</li>
			<li>› 如果您<a class="forget" href="">忘记密码</a>？，申请找回密码</li>
		</ul>
		<div class="enter">
			<input type="submit" name="Submit" value="登&#12288;&#12288;录" class="submit">
			<span class="other">
				<a href="" title="QQ账号登录" class="qq"><i></i></a>
				<a href="''+encodeURIComponent(ref_url)  title="新浪微博账号登录" class="sina"><i></i></a>
				<a href="javascript:void(0);" onclick="weixin_login()" title="微信账号登录" class="wx"><i></i></a>
			</span>
		</div>
	</form>
</div>

</body>
</html>

下面压缩的:
<div class="quick-login"><form class="bg" method="post" id="ajax_login" action=""><dl><dt>用户名</dt><dd><input type="text" name="userName" autocomplete="off" class="text"></dd></dl><dl><dt>密&nbsp;&nbsp;&nbsp;码</dt><dd><input type="password" autocomplete="off" name="password" class="text"></dd></dl><dl><dt>验证码</dt><dd><input type="text" size="10" maxlength="4" class="text fl w60" name="captcha"><img border="0" onclick="this.src='/code?type=login&n=2' + Math.random()" name="codeimage" title="看不清，换一张" src="/code?type=login&n=2" class="fl ml10"><span>不区分大小写</span></dd></dl><ul><li>› 如果您没有登录账号，请先<a class="register" href="">注册会员</a>然后登录</li><li>› 如果您<a class="forget" href="">忘记密码</a>？，申请找回密码</li></ul><div class="enter"><input type="submit" name="Submit" value="登&#12288;&#12288;录" class="submit"><span class="other"><a href="" title="QQ账号登录" class="qq"><i></i></a><a href="''+encodeURIComponent(ref_url)  title="新浪微博账号登录" class="sina"><i></i></a><a href="javascript:void(0);" onclick="weixin_login()" title="微信账号登录" class="wx"><i></i></a></span></div></form></div>


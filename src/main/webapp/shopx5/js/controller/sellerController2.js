// 定义控制器:
app.controller("sellerController",function($scope,$controller,$http,sellerService){
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		sellerService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		sellerService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		sellerService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].sellerId;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 增加/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = sellerService.update($scope.entity);//修改
		}else{
			object = sellerService.add($scope.entity);   //增加
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag){
				alert(response.message);//更新成功
				$scope.reloadList();    //刷新列表
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		sellerService.findOne(id).success(function(response){ //{id:xx,name:yy,firstChar:zz}
			$scope.entity = response;
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		sellerService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	///---
	$scope.add = function(){
		sellerService.add($scope.entity).success(
			function(response){
				if(response.flag){
					// 重新查询 
		        	// $scope.reloadList();//重新加载
					location.href="shoplogin.html";
				}else{
					alert(response.message);
				}
			}
		);	
	}
	
});

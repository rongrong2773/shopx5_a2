// 定义控制器:
app.controller("contentController",function($scope,$controller,$http,contentService,uploadService,contentCatService){
	// AngularJS中的继承: 伪继承
	$controller("baseController",{$scope:$scope});
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		contentService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		contentService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		contentService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 增加/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = contentService.update($scope.entity);//修改
		}else{
			object = contentService.add($scope.entity);   //增加
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag){
				alert(response.message);//更新成功
				$scope.reloadList();    //刷新列表
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		contentService.findOne(id).success(function(response){ //{id:xx,name:yy,firstChar:zz}
			$scope.entity = response;
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		contentService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	///------
	
	// 文件上传的方法
	$scope.uploadFile = function(){
		uploadService.uploadFile().success(function(response){
			if(response.flag){
				$scope.entity.pic = response.message;
			}else{
				alert(response.message);
			}
		});
	}
		
	//定义新分类[]
	$scope.contentCatNewList = [];
	// 查询所有广告分类
	$scope.findContentCatList = function(){
		contentCatService.findAll().success(function(response){
			$scope.contentCatList = response;
			for(var i=0;i<response.length;i++){
				$scope.contentCatNewList[response[i].id] = response[i].name;
			}
		});
	}
	
	// 状态显示
	$scope.status = ["无效","有效"];
	
});

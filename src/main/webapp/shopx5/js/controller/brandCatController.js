// 定义控制器:
app.controller("brandCatController",function($scope,$controller,$http,brandCatService){
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询列表: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		brandCatService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 查询分页列表:
	$scope.findPage = function(page,rows){
		brandCatService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 查询分页列表 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		brandCatService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 新增/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = brandCatService.update($scope.entity);//修改
		}else{
			object = brandCatService.add($scope.entity);  //新增
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag==true){
				alert(response.message);//更新成功
				$scope.reloadList();    //刷新列表
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		brandCatService.findOne(id).success(function(response){ //{id:xx,name:yy,firstChar:zz}
			$scope.entity = response;
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		brandCatService.dele($scope.selectIds).success(function(response){
			if(response.flag==true){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //清空
			}else{
				alert(response.message);
			}
		});
	}
	
	///------------------
	// 显示品牌分类
	$scope.ppcat = ["其他","数码办公","服饰鞋帽","生活服务"];
	
	$scope.brandCatList = {data:[]}
	// 查询关联的品牌信息: mapper自定义
	$scope.findBrandCatList = function(){
		brandCatService.selectOptionList().success(function(response){
			$scope.brandCatList = {data:response};
		});
	}
	
	
});









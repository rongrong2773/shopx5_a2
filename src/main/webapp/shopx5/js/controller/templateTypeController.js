// 定义控制器:
app.controller("typeTemplateController",function($scope,$controller,$http,templateTypeService,brandService,specificationService){
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询列表: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		templateTypeService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 查询分页列表:
	$scope.findPage = function(page,rows){
		templateTypeService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 查询分页列表 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		templateTypeService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 增加/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = templateTypeService.update($scope.entity);//修改
		}else{
			//console.log(JSON.stringify($scope.entity));console.log($scope.c);return;
			object = templateTypeService.add($scope.entity);   //增加
					//*参数 { "customAttributeItems": [{"$$hashKey":"00G","text":"好看"},{"$$hashKey":"00I","text":"不错"}],
					//       "name": "类型名",
					//       "brandIds": [{"id":11,"text":"诺基亚"},{"id":7,"text":"中兴"}],
					//       "specIds": [{"id":32,"text":"机身内存"},{"id":26,"text":"尺码"}]
					//     }
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag==true){
				alert(response.message);//更新成功
				$scope.reloadList();    //刷新列表
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		templateTypeService.findOne(id).success(function(response){ //{id:xx,name:yy,firstChar:zz}
			$scope.entity = response;
			// eval()   JSON.parse();   ///------
			$scope.entity.brandIds             = JSON.parse($scope.entity.brandIds); //转json对象
			$scope.entity.specIds              = JSON.parse($scope.entity.specIds);
			$scope.entity.customAttributeItems = JSON.parse($scope.entity.customAttributeItems); //[,]
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		templateTypeService.dele($scope.selectIds).success(function(response){
			if(response.flag==true){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	///------
	$scope.brandList = {data:[]}
	// 查询关联的品牌信息: mapper自定义
	$scope.findBrandList = function(){
		brandService.selectOptionList().success(function(response){
			$scope.brandList = {data:response};
		});
	}
	
	$scope.specList = {data:[]}
	// 查询关联的规格信息:
	$scope.findSpecList = function(){
		specificationService.selectOptionList().success(function(response){
			$scope.specList = {data:response};
		});
	}
	
	//添加: 一行扩展属性
	$scope.entity={customAttributeItems:[]};
	$scope.addTableRow = function(){
		$scope.entity.customAttributeItems.push({});
	}
	//删除: 一行扩展属性
	$scope.deleteTableRow = function(index){
		$scope.entity.customAttributeItems.splice(index,1);
	}
	
});

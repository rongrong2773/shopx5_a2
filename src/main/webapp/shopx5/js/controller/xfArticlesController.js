// 定义控制器:
app.controller('xfArticlesController',function($scope,$controller,$location, xfArticlesService,uploadService){ //url更新
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 定义实体类 TbXfArticle
	$scope.entity = {};
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		xfArticlesService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		xfArticlesService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		xfArticlesService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");
		alert($scope.selectIds)
		return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		xfArticlesService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	
	// 定义实体类 TbXfArticle
	// $scope.entity = {xfArticle:{}};  [废弃] 
	//  $scope.entity = {};
	
	///---------------------------------------------------
	// 更新: 增加/修改       entity {news:{},newsDesc:{}}
	$scope.save = function(){
		// 再添加之前，获得富文本编辑器中HTML内容。
		$scope.entity.content = editor.html();
		console.log($scope.entity)
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = xfArticlesService.update($scope.entity);//修改
		}else{
			object = xfArticlesService.add($scope.entity);   //增加
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag){
				alert(response.message);//更新成功
				console.log(location.href)
//				if(window.parent.showList==undefined){
//					location.href='xfArticle_list';
//				}else{
				
					window.parent.showList('文章列表','/mobile/backend/xs/temp/xfArticle_list');
//				}
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	
	// KindEditor上传图片
	$scope.uploadFile = function(){
		// 调用uploadService的方法完成文件上传
		uploadService.uploadFile().success(function(response){
			if(response.flag){
				$scope.image_entity.url = response.message; //获得url  ng-click="image_entity={}"
			}else{
				alert(response.message);
			}
		});
	}
	

	
	
	//查询实体   update:1
	$scope.findOne = function(){	
		var id = $location.search()['id']; //获取跳转参数id
		//设置属性
		xfArticlesService.findOne(id).success(
			function(response){
				$scope.entity= response;
				// 调用处理富文本编辑器：
				editor.html($scope.entity.content);
			}
		);				
	}
	
	
	
	// 显示上下架状态
	$scope.markets = ["下架","上架"];
	// 上下架: 状态更新
	$scope.updateMarketable = function(status){
		if($scope.selectIds.length<1){
			alert("请至少选择一项上下架");return;
		}
		xfArticlesService.updateMarketable($scope.selectIds,status).success(function(response){
			if(response.flag){
				$scope.reloadList();//刷新列表
				$scope.selectIds = [];
			}else{
				alert(response.message);
			}
		});
	}

});









//服务层
app.service('specificationOptionService',function($http){
	//读取列表数据绑定到表单中
	this.findAll=function(){
		return $http.get('/shopx5/specificationOption/findAll');		
	}
	//分页
	this.findPage=function(page,rows){
		return $http.get('/shopx5/specificationOption/findPage?page='+page+'&rows='+rows);
	}
	//搜索
	this.search=function(page,rows,searchEntity){
		return $http.post('/shopx5/specificationOption/search?page='+page+"&rows="+rows, searchEntity);
	}
	//查询实体
	this.findOne=function(id){
		return $http.get('/shopx5/specificationOption/findOne?id='+id);
	}
	//增加
	this.add=function(entity){
		return  $http.post('/shopx5/specificationOption/add', entity);
	}
	//修改
	this.update=function(entity){
		return  $http.post('/shopx5/specificationOption/update', entity);
	}
	//删除
	this.dele=function(ids){
		return $http.get('/shopx5/specificationOption/delete?ids='+ids);
	}
});

//服务层
app.service('goodsService',function($http){
	    	
	//读取列表数据绑定到表单中
	this.findAll=function(){
		return $http.get('/shopx5/goods/findAll');		
	}
	//分页 
	this.findPage=function(page,rows){
		return $http.get('/shopx5/goods/findPage?page='+page+'&rows='+rows);
	}
	//查询实体
	this.findOne=function(id){
		return $http.get('/shopx5/goods/findOne?id='+id);
	}
	//增加 
	this.add=function(entity){
		return  $http.post('/shopx5/goods/add',entity );
	}
	//修改 
	this.update=function(entity){
		return  $http.post('/shopx5/goods/update',entity );
	}
	//删除
	this.dele=function(ids){
		return $http.get('/shopx5/goods/delete?ids='+ids);
	}
	//搜索
	this.search=function(page,rows,searchEntity){
		return $http.post('/shopx5/goods/searchAll?page='+page+"&rows="+rows, searchEntity);
	}
	
	//商品审核: 状态更新
	this.updateStatus = function(ids,status){
		return $http.get('/shopx5/goods/updateStatus?ids='+ids+"&status="+status);
	}
	
});

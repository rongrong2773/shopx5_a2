// 定义服务层:
app.service("itemCatService",function($http){
	//查询全部
	this.findAll = function(){
		return $http.get("/shopx5/itemCat2/findAll");
	}
	//根据父ID查询
	this.findByParentId = function(parentId){
		return $http.get("/shopx5/itemCat2/findByParentId?parentId="+parentId);
	}
	//分页
	this.findPage = function(page,rows){
		return $http.get("/shopx5/itemCat2/findPage?page="+page+"&rows="+rows);
	}
	//分页 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/itemCat2/search?page="+page+"&rows="+rows,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/itemCat2/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/shopx5/itemCat2/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/shopx5/itemCat2/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/itemCat2/delete?ids="+ids);
	}
	//自定义
	
	//搜索前 缓存分类
	this.cacheCat = function(){
		return $http.post("/shopx5/itemCat2/cacheCat");
	}
	
});

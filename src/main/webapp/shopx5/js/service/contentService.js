// 定义服务层:
app.service("contentService",function($http){
	//查询全部
	this.findAll = function(){
		return $http.get("/shopx5/content/findAll");
	}
	//根据父ID查询
	this.findByParentId = function(parentId){
		return $http.get("/shopx5/content/findByParentId?parentId="+parentId);
	}
	//分页
	this.findPage = function(page,rows){
		return $http.get("/shopx5/content/findPage?page="+page+"&rows="+rows);
	}
	//分页 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/content/search?page="+page+"&rows="+rows,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/content/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/shopx5/content/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/shopx5/content/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/content/delete?ids="+ids);
	}
	//自定义
	
});

//日期格式
Date.prototype.format = function(format){ 
    var o =  { 
    "M+" : this.getMonth()+1, //month 
    "d+" : this.getDate(), //day 
    "h+" : this.getHours(), //hour 
    "m+" : this.getMinutes(), //minute 
    "s+" : this.getSeconds(), //second 
    "q+" : Math.floor((this.getMonth()+3)/3), //quarter 
    "S" : this.getMilliseconds() //millisecond 
    };
    if(/(y+)/.test(format)){ 
    	format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
    }
    for(var k in o)  { 
	    if(new RegExp("("+ k +")").test(format)){ 
	    	format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
	    } 
    } 
    return format; 
};

var X5 = SHOPX5 = {
	// 格式化时间
	formatDateTimes : function(val){
		var now = new Date(val);
    	return now.format("yyyy-MM-dd hh:mm:ss");
	},
	formatDateTime : function(val){
		var now = new Date(val);
    	return now.format("yyyy-MM-dd");
	},
	// 格式化连接
	formatUrl : function(val, row){
		if(val){
			return "<a href='"+val+"' target='_blank'>查看</a>";			
		}
		return "";
	},
	// 格式化价格
	formatPrice : function(val, row){
		return (val/100).toFixed(2);
	},
	// 格式化商品的状态
	formatItemStatus : function(val, row){
        if (val == 1){
            return '正常';
        } else if(val == 2){
        	return '<span style="color:red;">下架</span>';
        } else {
        	return '关闭';
        }
    },
    
	// 编辑器 参数
	kingEditorParams : {
		//指定上传文件参数名称
		filePostName  : "uploadFile",                 //1. 上传名★     <form action="/pic/upload">
		//指定上传文件请求的url										    <input type="file" name="uploadFile">
		uploadJson : '/pic/upload',                   //2. 上传url★ </form>
		//上传类型，分别为image、flash、media、file
		dir : "image"                                 //3. 文件类型★
	},
	// 初始化 KindEditor
    createEditor : function(select){
    	return KindEditor.create(select, X5.kingEditorParams);
    },
    /**
     * 初始化单图片上传组件 <br/>
     * 选择器为：.onePicUpload <br/>
     * 上传完成后会设置input内容以及在input后面追加<img> 
     */
    initOnePicUpload : function(){
    	$(".onePicUpload").click(function(){
			var _self = $(this);
			KindEditor.editor(X5.kingEditorParams).loadPlugin('image', function() {
				this.plugin.imageDialog({
					showRemote : false,
					clickFn : function(url, title, width, height, border, align) {
						var input = _self.siblings("input");
						input.parent().find("img").remove();
						input.val(url);
						input.after("<a href='"+url+"' target='_blank'><img src='"+url+"' width='80' height='50'/></a>");
						this.hideDialog();
					}
				});
			});
		});
    },
    
    // 初始化 类目选择+图片上传组件
    init : function(data){
    	// 初始化图片上传组件                 //1.图片单独 上传★  "pics" : data.image,
    	this.initPicUpload(data);
    	// 初始化选择类目组件                 //2.商品分类 加载★  "cid" : data.cid,
    	this.initItemCat(data);
    },
    // 初始化图片 秒杀图片简化*
    initPic : function(data){
    	$("#smallPic_eidt").each(function(i,e){
    		var _ele = $(e);
    		_ele.next("div.pics").remove();
    		_ele.next("img").remove();
    		_ele.after('<span class="pics"></span>');
    		// 回显图片
        	if(data.data.smallPic){
        		var imgs = data.data.smallPic.split(",");
        		for(var i in imgs){
        			if($.trim(imgs[i]).length > 0){
        				_ele.siblings(".pics").append("<a href='"+imgs[i]+"' target='_blank'><img src='"+imgs[i]+"' width='100' height='66' /></a>");
        			}
        		}
        	}
    	})
    },
    // 初始化图片上传组件
    initPicUpload : function(data){
    	$("#previewEdit").each(function(i,e){ //i当前下标0  e当前对象<a..>                    ★
    		console.log($(e)); //0-object
    		var _ele = $(e);
    		_ele.next("div.pics").remove(); //1.1查找当前对象内所有元素并删除元素体  1.2在标签后加新标签<div class="pics"><ul></ul></div> ★
    		_ele.next("img").remove();
    		_ele.after('\
    			<div class="pics">\
        			<ul></ul>\
        		</div>');
    		// 回显图片                                                       1.3之前上传过图片回显出来★
        	if(data && data.pics){
        		var imgs = data.pics.split(",");
        		for(var i in imgs){
        			if($.trim(imgs[i]).length > 0){
        				_ele.siblings(".pics").find("ul").append("<a href='"+imgs[i]+"' target='_blank'><img src='"+imgs[i]+"' width='100' height='66' /></a>");
        			}
        		}
        	}
        	
        	//给“上传图片按钮”绑定click事件       2.没有图片时★
        	$(e).click(function(){
        		var form = $(this).parentsUntil("form").parent("form");
        		//打开图片上传窗口                3.打开的是复文本图片上传插件,不是之前类目框了★
        		KindEditor.editor(X5.kingEditorParams).loadPlugin('multiimage',function(){
        			var editor = this;                                   //4. multiimage是插件名
        			editor.plugin.multiImageDialog({                     //5. 选择上传图片,并返回json
						clickFn : function(urlList) {
							var imgArray = [];
							KindEditor.each(urlList, function(i, data) { //6. 图片插入,并显示
								imgArray.push(data.url);
								form.find(".pics ul").append("<li><a href='"+data.url+"' target='_blank'><img src='"+data.url+"' width='80' height='50' /></a></li>");
							});
							form.find("[name=image]").val(imgArray.join(","));
							editor.hideDialog();
						}
					});
        		});
        	});
    	});
    },
    
    // 初始化选择类目组件
    initItemCat : function(data){
    	$(".selectItemCat").each(function(i,e){ //i循环下标  e循环DOM对象<div> ★
    		var _ele = $(e);    		
    		if(data && data.cid){              //data.cid商品的类目id
    			_ele.next("span").remove();
				$.post("/shop/itemCat/itemCatName", {"id":data.cid}, function(data){
						 _ele.after("<span style='margin-left:10px;'>"+data.name+"</span>"); //1.修改,类目对象,展示类目id
					}, "json"
				);
    		}else{
    			_ele.after("<span style='margin-left:10px;'></span>");  //2.添加,标签后加新标签 <a>选择类目</a><span style='margin-left:10px;'></span>
    		}
    		_ele.unbind('click').click(function(){                //unbind取消绑定元素的事件处理程序和函数 再绑定新click事件 ★
    			$("<div>").css({padding:"5px"}).html("<ul></ul>") //创建元素 <div style="padding:5px;"><ul></ul></div>
    			.window({                                         //easyui.window()
    				width : '500',
    			    height : '450',
    			    title : '选择类目',
    			    iconCls : 'icon-save',
    			    modal : true,
    			    closed : true,    			    
    			    onOpen : function(){                     //打开面板之后触发
    			    	var _win = this;                     //this=<div><ul> DOM
    			    	$("ul",_win).tree({                  //子,父;父内找子
    			    		url : '/shop/itemCat/treeList',          //异步数据[id text state] ★  取商品分类
    			    		animate : true,
    			    		onClick : function(node){        //点击父节点,发送请求,展开父节点★★  id=1
    			    			if($(this).tree("isLeaf",node.target)){ //点击是叶子节点,$(this)=$("ul")
    			    				_ele.parent().find("[name=cid]").val(node.id);   //1.hidden值: <td> - <input type="hidden" name="cid" value="123" />
    			    				_ele.next().text(node.text).attr("cid",node.id); //2.span值    : _ele - <span cid="123">分类名称</span>
    			    				$(_win).window('close'); //值设置完,关闭窗口
    			    				
    			    			 // fun : function(node){
    		        			 //     SHOPX5.changeItemParam(node, "itemeEditForm");
    		        			 // }
    			    				if(data && data.fun){
    			    					data.fun.call(this, node); //替换了node  通过call方法,将原本属于Animal对象的showName()方法交给当前对象cat来使用了★
    			    				}
    			    			}
    			    		}
    			    	});
    			    },
    			    onClose : function(){
    			    	$(this).window("destroy");
    			    }
    			}).window('open');
    		});
    	});
    },
    //
    /**
     * 创建一个窗口，关闭窗口后销毁该窗口对象。<br/>
     * 
     * 默认：<br/>
     * width : 80% <br/>
     * height : 80% <br/>
     * title : (空字符串) <br/>
     * 
     * 参数：<br/>
     * width : <br/>
     * height : <br/>
     * title : <br/>
     * url : 必填参数 <br/>
     * onLoad : function 加载完窗口内容后执行<br/>
     * 
     * 
     */
    createWindow : function(params){
    	$("<div>").css({padding:"5px"}).window({
    		width : params.width?params.width:"80%",
    		height : params.height?params.height:"80%",
    		modal:true,
    		title : params.title?params.title:" ",
    		href : params.url,          //新增内容 url : "/shop_manager/content-add" ★
		    onClose : function(){
		    	$(this).window("destroy");
		    },
		    onLoad : function(){        //加载远程数据时触发
		    	if(params.onLoad){
		    		params.onLoad.call(this);
		    	}
		    }
    	}).window("open");
    },    
    closeCurrentWindow : function(){
    	$(".panel-tool-close").click();
    },
    
    changeItemParam : function(node,formId){
    	$.getJSON("/item/param/query/itemcatid/" + node.id,function(data){
			  if(data.status == 200 && data.data){
				 $("#"+formId+" .params").show();
				 var paramData = JSON.parse(data.data.paramData);
				 var html = "<ul>";
				 for(var i in paramData){
					 var pd = paramData[i];
					 html+="<li><table>";
					 html+="<tr><td colspan=\"2\" class=\"group\">"+pd.group+"</td></tr>";
					 
					 for(var j in pd.params){
						 var ps = pd.params[j];
						 html+="<tr><td class=\"param\"><span>"+ps+"</span>: </td><td><input autocomplete=\"off\" type=\"text\"/></td></tr>";
					 }
					 
					 html+="</li></table>";
				 }
				 html+= "</ul>";
				 $("#"+formId+" .params td").eq(1).html(html);
			  }else{
				 $("#"+formId+" .params").hide();
				 $("#"+formId+" .params td").eq(1).empty();
			  }
		  });
    },
    getSelectionsIds : function (select){
    	var list = $(select);
    	var sels = list.datagrid("getSelections");
    	var ids = [];
    	for(var i in sels){
    		ids.push(sels[i].id);
    	}
    	ids = ids.join(",");
    	return ids;
    }
    
    
};



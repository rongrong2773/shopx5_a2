// 定义服务层:
app.service("sellerService",function($http){
	//所有列表
	this.findAll = function(){
		return $http.get("/shopx5/seller/findAll");
	}
	//分页列表
	this.findPage = function(page,rows){
		return $http.get("/shopx5/seller/findPage?page="+page+"&rows="+rows);
	}
	//分页列表 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/seller/search?page="+page+"&rows="+rows,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/seller/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/shopx5/seller/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/shopx5/seller/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/seller/delete?ids="+ids);
	}
	//自定义
	this.selectOptionList = function(){
		return $http.get("/shopx5/seller/selectOptionList");
	}
	
	//更新状态
	this.updateStatus = function(sellerId,status){
		return $http.get('/shopx5/seller/updateStatus?sellerId='+sellerId+"&status="+status);
	}
	
});

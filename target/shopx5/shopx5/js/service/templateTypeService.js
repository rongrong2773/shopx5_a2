// 定义服务层:
app.service("templateTypeService",function($http){
	//所有列表
	this.findAll = function(){
		return $http.get("/shopx5/templateType/findAll");
	}
	//分页列表
	this.findPage = function(page,rows){
		return $http.get("/shopx5/templateType/findPage?page="+page+"&rows="+rows);
	}
	//分页列表 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/templateType/search?page="+page+"&rows="+rows,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/templateType/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/shopx5/templateType/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/shopx5/templateType/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/templateType/delete?ids="+ids);
	}
	//自定义 select2商品类目
	this.selectOptionList = function(){
		return $http.get("/shopx5/templateType/selectOptionList");
	}
});

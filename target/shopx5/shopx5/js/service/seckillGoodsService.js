// 定义服务层:
app.service("seckillGoodsService",function($http){
	//所有列表
	this.findAll = function(){
		return $http.get("/seckill/goods/findAll");
	}
	//分页列表
	this.findPage = function(page,rows){
		return $http.get("/seckill/goods/findPage?page="+page+"&rows="+rows);
	}
	//分页列表 搜索:带条件
	this.search = function(page,rows,grade,searchEntity){
		return $http.post("/seckill/goods/search?page="+page+"&rows="+rows+"&grade="+grade,searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/seckill/goods/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/seckill/goods/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/seckill/goods/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/seckill/goods/delete?ids="+ids);
	}
});

// 定义服务层:
app.service("specificationService",function($http){
	//所有列表
	this.findAll = function(){
		return $http.get("/shopx5/specification/findAll");
	}
	//分页列表
	this.findPage = function(page,rows){
		return $http.get("/shopx5/specification/findPage?page="+page+"&rows="+rows);
	}
	//分页列表 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/specification/search?page="+page+"&rows="+rows, searchEntity);
	}
	//增加
	this.add = function(entity){
		return $http.post("/shopx5/specification/add",entity);
	}
	//查询实体
	this.findOne = function(id){
		return $http.get("/shopx5/specification/findOne?id="+id);
	}
	//修改 
	this.update = function(entity){
		return $http.post("/shopx5/specification/update",entity);
	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/specification/delete?ids="+ids);
	}
	//自定义 select2模版管理
	this.selectOptionList = function(){
		return $http.get("/shopx5/specification/selectOptionList");
	}
	
});

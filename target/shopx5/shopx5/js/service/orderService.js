//服务层
app.service('orderService',function($http){
	
	//搜索
	this.search=function(page,rows,grade,searchEntity){
		return $http.post('/shopx5/order/search?page='+page+"&rows="+rows+"&grade="+grade, searchEntity);
	}
	
	//查询订单项
	this.findItem=function(orderId){
		return $http.get('/shopx5/order/findItem?orderId='+orderId);
	}
	
	//更新状态
	this.updateStatus = function(orderId,status){
		return $http.get('/shopx5/order/updateStatus?orderId='+orderId+"&status="+status);
	}
});

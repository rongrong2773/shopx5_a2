// 定义服务层:
app.service("newsSaveService",function($http){
	//查询全部
	this.findAll = function(){
		return $http.get("/shopx5/goods/findAll");
	}
	//分页
	this.findPage = function(page,rows){
		return $http.get("/shopx5/goods/findPage?page="+page+"&rows="+rows);
	}
	//分页 搜索:带条件
	this.search = function(page,rows,searchEntity){
		return $http.post("/shopx5/goods/search?page="+page+"&rows="+rows,searchEntity);
	}
	
	//增加
	this.add = function(entity){
		return $http.post("/xunsi/news/add",entity);
	}
//	//增加
//	this.add = function(entity){
//		return $http.post("/shopx5/goods/add",entity);
//	}
//	//查询实体
//	this.findOne = function(id){
//		return $http.get("/shopx5/goods/findOne?id="+id);
//	}
	
	//查询实体
	this.findOne = function(id){
		return $http.get("/xunsi/news/findOne?id="+id);
	}
	//修改
	this.update = function(entity){
		return $http.post("/xunsi/news/update",entity);
	}
	
	//修改 
//	this.update = function(entity){
//		return $http.post("/shopx5/goods/update",entity);
//	}
	//删除
	this.dele = function(ids){
		return $http.get("/shopx5/goods/delete?ids="+ids);
	}
	//根据父ID查询
	this.findByParentId = function(parentId){
		return $http.get("/shopx5/goods/findByParentId?parentId="+parentId);
	}
	//自定义
	
	//上下架: 状态更新
	this.updateMarketable = function(ids,status){
		return $http.get('/shopx5/goods/updateMarketable?ids='+ids+"&status="+status);
	}
	
});

app.controller("baseController",function($scope){
	
	// 分页配置信息: 初始化加载
	$scope.paginationConf = {
		currentPage: 1,   // 当前页数
		itemsPerPage: 10, // 每页显示多少条记录
		totalItems: 10,   // 总记录数,搜索返回封装总数
		perPageOptions: [10, 20, 30, 40, 50],// 显示多少条下拉列表
		onChange: function(){  // 当页码、每页显示多少条下拉列表发生变化的时候，自动触发了
			$scope.reloadList();// 重新加载列表 
		 }
	};
	
	// 获取分页列表:
	$scope.reloadList = function(){
	  //$scope.findByPage($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage);
		$scope.search($scope.paginationConf.currentPage, $scope.paginationConf.itemsPerPage);
	}
	
	// 全选复选框：
	$scope.allIds = [];		//所有ids[]数据
	$scope.selectIds = [];	//定义一个数组:用于记录选中/取消
	$scope.c = false;		//单选默认未选中
	// 全选
	$scope.checkAll = function(master,allIds){
		if(master==false||master==undefined){
			$scope.c=true;
			$scope.selectIds=angular.copy(allIds);
		}else{
			$scope.c=false;
			$scope.selectIds=[];
		}
	}
	
	// 单选或多选
	$scope.check = function($event,id,c){
		if(c==false&&$event.target.checked==true){
			if($scope.selectIds.indexOf(id)==-1)
				$scope.selectIds.push(id);//选中 true true
		}else{
			if($scope.selectIds.indexOf(id)!=-1)
				$scope.selectIds.splice($scope.selectIds.indexOf(id),1);//取消选中 false false
		}
		if(c==true&&$event.target.checked==true){
			if($scope.selectIds.indexOf(id)==-1)
				$scope.selectIds.push(id);
		}
		if($scope.selectIds.length==$scope.list.length){
			$scope.master=true
		}else if($scope.selectIds.length==0){
			$scope.master=false
		};
	}
	
	// 更新复选框：ng-click="updateSelection($event,entity.id)"
	$scope.updateSelection = function($event,id){
		// 向数组中添加元素
		if($event.target.checked){ //复选框选中
			$scope.selectIds.push(id);
		}else{
			// 从数组中移除
			var idx = $scope.selectIds.indexOf(id);
			$scope.selectIds.splice(idx,1);
		}
	}
	
	// 获取JSON字符串中某个key对应值的集合      规格含选项[{"id":27,"text":"网络",options:[{id:2,optionName:移动2G},]}] options
	$scope.jsonToString = function(jsonStr,key){
		// 将字符串转成JSON:
		var jsonObj = JSON.parse(jsonStr);
		var value = "";
		for(var i=0;i<jsonObj.length;i++){
			if(i>0){
				value += " , ";
			}
			value += jsonObj[i][key];
		}
		return value;
	}
	
	// 从集合中查询某个名称的值是否存在
	$scope.searchObjectByKey = function(list,keyName,keyValue){
		for(var i=0;i<list.length;i++){
			if(list[i][keyName] == keyValue){
				return list[i];
			}
		}
		return null;
	}
});



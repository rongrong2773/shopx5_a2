// 定义控制器:
app.controller('newsController',function($scope,$controller,$location,
										  newsService,uploadService,itemCatService,templateTypeService){ //url更新
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		newsService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		newsService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		newsService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 增加/修改
	
	// 查询一个:
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		newsService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	
	///---------------------------------------------------
	// 更新: 增加/修改         entity {goods:{},goodsDesc:{},itemList:[]}
	$scope.type = $location.search()['type'];
	$scope.save = function(){
		
		if($scope.type!='edit'){
			alert("type!= edit, 禁止此次操作!");
			return;
		}
		// 再添加之前，获得富文本编辑器中HTML内容。
		$scope.entity.newsDesc.newsDesc = editor.html();
		// 区分是新增还是修改
		var object;
		if($scope.entity.news.id != null){
			object = newsService.update($scope.entity);//修改
		}else{
			object = newsService.add($scope.entity);   //增加
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag){
				alert(response.message);//更新成功
				if(window.parent.showList==undefined){
					location.href='news';
				}else{
					window.parent.showList('商品列表_商户','/temp/xs/test/news');
				}
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	
	// KindEditor上传图片  2.
	$scope.uploadFile = function(){
		// 调用uploadService的方法完成文件上传
		uploadService.uploadFile().success(function(response){
			if(response.flag){
				$scope.image_entity.url = response.message; //获得url  ng-click="image_entity={}"
			}else{
				alert(response.message);
			}
		});
	}
	
	// 定义entity 获取image_entity实体数据{"color":"褐色","url":"../group1/M00/00/00/wKjRhFn.jpg"}
	$scope.entity = {goods:{"isEnableSpec":"1"}, goodsDesc:{itemImages:[],specificationItems:[]}};                   //###### 1   entity
	
	// 展示已保存图片
	$scope.add_image_entity = function(){
		$scope.entity.goodsDesc.itemImages.push($scope.image_entity);
	}
	// 移除  :移除的缓存数据,数据库未处理
	$scope.remove_iamge_entity = function(index){
		$scope.entity.goodsDesc.itemImages.splice(index,1);
	}
	
	// 查询一级分类列表: 3   init初始加载..
	$scope.selectItemCat1List = function(){
		itemCatService.findByParentId(0).success(function(response){
			$scope.itemCat1List = response;
		});
	}
	// 查询二级分类列表: 监控变量变化
	$scope.$watch("entity.goods.category1Id",function(newValue,oldValue){
		itemCatService.findByParentId(newValue).success(function(response){
			$scope.itemCat2List = response;
		});
	});
	// 查询三级分类列表:
	$scope.$watch("entity.goods.category2Id",function(newValue,oldValue){
		itemCatService.findByParentId(newValue).success(function(response){
			$scope.itemCat3List = response;
		});
	});
	// 查询模块ID: 4   根据3级分类
	$scope.$watch("entity.goods.category3Id",function(newValue,oldValue){
		itemCatService.findOne(newValue).success(function(response){
			$scope.entity.goods.templateTypeId = response.typeId;     //最终选中 商品分类的模版id
		});
	});
	
	// 查询模板下的品牌列表: 5 根据已选模版选品牌
	// $scope.templateType{brandIds:[{id:,text:},]}                                         //###### 2   templateType
	// $scope.specList                                                                      //###### 3   specList
	$scope.$watch("entity.goods.templateTypeId",function(newValue,oldValue){
		// 根据模板ID查询模板的数据
		templateTypeService.findOne(newValue).success(function(response){
			$scope.templateType = response;
			// 将品牌字符串,数据转成JSON
			$scope.templateType.brandIds = JSON.parse($scope.templateType.brandIds);
			
			// 将扩展属性字符串,转成JSON  6 存入到goodsDesc中
			if($location.search()['id'] == null){  // update:1.4
				$scope.entity.goodsDesc.customAttributeItems = JSON.parse($scope.templateType.customAttributeItems);
			}
		});
		
		// 根据模板ID获得规格的列表数据：7 java读取规格,遍历封装选项到options中  
		templateTypeService.findBySpecList(newValue).success(function(response){
			$scope.specList = response;//[{"id":27,"text":"网络", options:[{id:98,optionName:"移动2G",spec_id:27,orders:1}]}]
		});
	});

	// 修改时:判断选择的规格名是否选中  update:1.6    为ture选中执行updateSpecAttribute()
	$scope.checkAttributeValue = function(specName,optionName){
		var items = $scope.entity.goodsDesc.specificationItems;
		var object = $scope.searchObjectByKey(items,"attributeName",specName); //第一次,"attributeName"未设置
		if(object != null){
			if(object.attributeValue.indexOf(optionName)>=0){ //"规格名"下"选项名"存在的
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	// 7.1 点击规格的选项名 ng-click=""先执行     参: 网络,移动3G   规格名,选项名
	$scope.updateSpecAttribute = function($event,name,value){
		// 调用封装的方法判断 勾选的名称是否存在:
		var object = $scope.searchObjectByKey($scope.entity.goodsDesc.specificationItems,"attributeName",name); //返回object为遍历的单个{}
		if(object != null){
			// 找到了
			if($event.target.checked){
				object.attributeValue.push(value); //第二次添加 ["移动3G","移动4G"] ###关键点 规格值加到一起
			}else{
				object.attributeValue.splice(object.attributeValue.indexOf(value),1); //取消选择:移除
			}
			if(object.attributeValue.length == 0){ //规格的选项为0,移除此规格*
				var idx = $scope.entity.goodsDesc.specificationItems.indexOf(object); //取索引
				$scope.entity.goodsDesc.specificationItems.splice(idx,1);
			}
		}else{
			// 没找到: 首次,初始复制      [{"attributeName":"网络","attributeValue":["移动3G"]},,]
			$scope.entity.goodsDesc.specificationItems.push({"attributeName":name,"attributeValue":[value]});
		}
	}
	// 7.2 选择规格的选项名时: SKU信息填单 ***
	$scope.createItemList = function(){
		// 初始化基础数据:
		$scope.entity.itemList = [{spec:{},price:0,num:9999,status:'0',isDefault:'0'}]; //首次,再点成倍合并              //###### 4  entity.itemList
		var items = $scope.entity.goodsDesc.specificationItems; //规格
		for(var i=0;i<items.length;i++){ //首次为1,之后为n
			// 添加每行栏,用于保存集合数据               itemList,网络,["移动3G"]      机身内存,["32G"]
			$scope.entity.itemList = addColumn($scope.entity.itemList, items[i].attributeName, items[i].attributeValue); //:核心步骤3★★ 
		}
	}
	// 7.3  添加每行栏
	addColumn = function(list,columnName,columnValues){
		// 定义一个集合用于保存生成的每行的数据:
		var newList = [];
		// 遍历该集合的数据:
		for(var i=0;i<list.length;i++){             //首次1 [{spec:{},price:0,num:9999,status:'0',isDefault:'0'}] 
			var oldRow = list[i];
			for(var j=0;j<columnValues.length;j++){ //n ["移动4G","电信3G"]
				// 对oldRow数据进行克隆:
				var newRow = JSON.parse(JSON.stringify(oldRow)); //json解析为String 再被转json
				newRow.spec[columnName] = columnValues[j];       //[{"spec":{"网络":"移动3G"},,}组装  :核心步骤1★ 同样网络:值覆盖 尺寸会添加组合
				// 将newRow存入到newList中
				newList.push(newRow);             //核心步骤2★★ 返回itemList的size为多少  ["移动3G"]返会1个对象,["移动3G","移动4G"]返2个,
			}                                                                          //与机身内存,["32G"]做成2套行       循环做成多套
		}
		return newList;
	}
	
	/////////--------- 以上保存为SKU

	// 商品列表:
	// 显示状态  list:1
	$scope.status = ["未审核","审核通过","审核未通过","关闭"];
	
	// 显示分类: 获取所有分类重新封装  list:2
	$scope.itemCatList = []; //定义新分类[]
	$scope.findItemCatList = function(){
		itemCatService.findAll().success(function(response){
			for(var i=0;i<response.length;i++){
				$scope.itemCatList[response[i].id] = response[i].name; //取原id:name封装到新[]
			}
		});
	}
	
	//商品修改:
	//查询实体   update:1
	$scope.findOne = function(){	
		var id = $location.search()['id']; //获取跳转参数id
		//设置属性
		newsService.findOne(id).success(
			function(response){
				$scope.entity= response;
				// 调用处理富文本编辑器：
				editor.html($scope.entity.newsDesc.newsDesc);
				
			}
		);				
	}
	
	// 显示上下架状态
	$scope.markets = ["下架","上架"];
	// 上下架: 状态更新
	$scope.updateMarketable = function(status){
		if($scope.selectIds.length<1){
			alert("请至少选择一项上下架");return;
		}
		goodsService.updateMarketable($scope.selectIds,status).success(function(response){
			if(response.flag){
				$scope.reloadList();//刷新列表
				$scope.selectIds = [];
			}else{
				alert(response.message);
			}
		});
	}

});









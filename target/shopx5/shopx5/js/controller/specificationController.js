// 定义控制器:
app.controller("specificationController",function($scope,$controller,$http,specificationService){
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询列表: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		specificationService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 查询分页列表:
	$scope.findPage = function(page,rows){
		specificationService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 查询分页列表 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		specificationService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 增加/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.specification.id != null){
			object = specificationService.update($scope.entity);//修改
		}else{
			object = specificationService.add($scope.entity);   //增加        *参数 {specification:{},specificationOptionList:[{}]}
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag==true){
				alert(response.message);//更新成功
				$scope.reloadList();    //刷新列表
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		specificationService.findOne(id).success(function(response){ //   *返回 {specification:{},specificationOptionList:[{}]}
			$scope.entity = response;
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		specificationService.dele($scope.selectIds).success(function(response){
			if(response.flag==true){
				$scope.reloadList();   //刷新列表
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	//添加: 一行规格选项
	$scope.addTableRow = function(){
		$scope.entity.specificationOptionList.push({});
	}
	//删除: 一行规格选项
	$scope.deleteTableRow = function(index){
		$scope.entity.specificationOptionList.splice(index,1);
	}
	
});

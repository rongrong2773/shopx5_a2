// 定义控制器:
app.controller('seckillOrderController',function($scope,$controller,seckillOrderService){ //url更新
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 分页 搜索:带条件
	$scope.grade=2;
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		seckillOrderService.search(page,rows,$scope.grade,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 查询订单项: 通过订单id查询订单项list展示
	$scope.findItem = function(orderId){
		seckillOrderService.findItem(orderId).success(function(response){
			$scope.item = response;
		});
	}
		
	// 显示类型,状态
	$scope.oType = ["","在线支付","货到付款"];
	$scope.oStatus = ["","未付款","已付款","未发货","已发货","交易成功","交易关闭","待评价"];
	$scope.oColor  = ["","#F00","#00F","#900","#000","","",""];
	
	//更新状态
	$scope.updateStatus = function(orderId,status){
		if(confirm('确认发货吗?')){
			seckillOrderService.updateStatus(orderId,status).success(function(response){
				if(response.flag){
					$scope.reloadList();//重新加载
				}else{
					alert(response.message);
				}
			});
		}
	}
});



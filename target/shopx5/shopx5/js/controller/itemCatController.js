// 定义控制器:
app.controller('itemCatController',function($scope,$controller,$http,itemCatService,templateTypeService){
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		itemCatService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		itemCatService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		itemCatService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
		});
	}
	
	// 更新: 增加/修改
	$scope.save = function(){
		// 区分是新增还是修改
		var object;
		if($scope.entity.id != null){
			object = itemCatService.update($scope.entity);//修改
		}else{
			//设置parentId
			if($scope.grade == 1){
				$scope.entity.parentId = 0;
			}else if($scope.grade == 2){
				$scope.entity.parentId = $scope.entity_1.id;
			}else if($scope.grade == 3){
				$scope.entity.parentId = $scope.entity_2.id;
			}
			object = itemCatService.add($scope.entity);   //增加
		}
		object.success(function(response){ //PageResult:{flag:true,message:xxx}
			if(response.flag){
				//alert(response.message);//成功
				if($scope.grade == 1){
					$scope.findByParentId(0);			   //刷新列表
				}else if($scope.grade == 2){
					$scope.selectList($scope.entity_1);    //刷新列表
				}else if($scope.grade == 3){
					$scope.selectList($scope.entity_2);    //刷新列表
				}
			}else{
				alert(response.message);//失败
			}
		});
	}
	
	// 查询一个:
	$scope.findOne = function(id){
		itemCatService.findOne(id).success(function(response){ //{id:xx,name:yy,firstChar:zz}
			$scope.entity = response;
		});
	}
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		itemCatService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				if($scope.grade == 1){
					$scope.findByParentId(0);			   //刷新列表
				}else if($scope.grade == 2){
					$scope.selectList($scope.entity_1);    //刷新列表
				}else if($scope.grade == 3){
					$scope.selectList($scope.entity_2);    //刷新列表
				}
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	///------
	
	// 根据父ID查询分类
	$scope.findByParentId = function(parentId){
		itemCatService.findByParentId(parentId).success(function(response){
			$scope.list = response;
			//全选allIds[]
			$scope.allIds = [];
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 定义一个变量记录当前是第几级分类
	$scope.grade = 1;
	$scope.setGrade = function(value){
		$scope.grade = value;
	}
	// 面包屑 一级二级向下赋值
	$scope.selectList = function(p_entity){ // 传入每一级对象entity <tr ng-repeat="entity in list">
		if($scope.grade == 1){
			$scope.entity_1 = null;
			$scope.entity_2 = null;
		}
		if($scope.grade == 2){
			$scope.entity_1 = p_entity;
			$scope.entity_2 = null;
		}
		if($scope.grade == 3){
			$scope.entity_2 = p_entity;
		}
		$scope.findByParentId(p_entity.id); // 为父类id 再查询
	}
	
	///------
	$scope.typeList = {data:[]}
	// 查询关联的模版信息: 自定义mapper
	$scope.findTypeList = function(){
		templateTypeService.selectOptionList().success(function(response){
			$scope.typeList = {data:response};
		});
	}
	
	// 搜索前 缓存分类
	$scope.cacheCat = function(){
		itemCatService.cacheCat().success(function(response){
			if(response.flag){
				alert(response.message);//更新成功
			}else{
				alert(response.message);//更新失败
			}
		});
	}
});

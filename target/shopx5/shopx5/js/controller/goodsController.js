// 定义控制器:
app.controller('goodsController',function($scope,$controller,$http,goodsService,itemCatService){ //url更新
	// AngularJS中的继承: 伪继承
	$controller('baseController',{$scope:$scope});
	
	// 查询所有: 数据绑定
	$scope.findAll = function(){
		// 向后台发送请求获取数据
		goodsService.findAll().success(function(response){
			$scope.list = response;
		});
	}
	
	// 分页:
	$scope.findPage = function(page,rows){
		goodsService.findPage(page,rows).success(function(response){
			$scope.paginationConf.totalItems = response.total; //更新总记录数
			$scope.list = response.rows;
		});
	}
	
	// 分页 搜索:带条件
	$scope.searchEntity = {}; //定义一个查询的实体
	$scope.search = function(page,rows){
		goodsService.search(page,rows,$scope.searchEntity).success(function(response){
			$scope.paginationConf.totalItems = response.total;
			$scope.list = response.rows;
			//全选allIds[]
			for(var i=0;i<$scope.list.length;i++){
				var id = $scope.list[i].id;
				if($scope.allIds.indexOf(id) == -1){
					$scope.allIds.push(id);
				}
			}
		});
	}
	
	// 更新: 增加/修改
	
	// 查询一个:
	
	// 批量删除:
	$scope.dele = function(){
		alert("权限限制");return;
		if($scope.selectIds.length<1){
			alert("请至少选择一项删除");return;
		}
		goodsService.dele($scope.selectIds).success(function(response){
			if(response.flag){
				$scope.showAll();      //展示所有
				$scope.selectIds = []; //置空
			}else{
				alert(response.message);
			}
		});
	}
	
	// 显示状态  list:1
	$scope.status = ["未审核","审核通过","审核未通过","关闭"];
	$scope.oColor = ["#F00","#00F","#900","#000","","",""];
	
	// 显示分类: 获取所有分类重新封装  list:2
	$scope.itemCatList = []; //定义新分类[]
	$scope.findItemCatList = function(){
		itemCatService.findAll().success(function(response){
			for(var i=0;i<response.length;i++){
				$scope.itemCatList[response[i].id] = response[i].name; //取原id:name封装到新['id'=>name,]
			}
		});
	}
	
	// 审核的方法:
	$scope.updateStatus = function(status){
		if($scope.selectIds.length<1){
			alert("请至少选择一项审核/驳回");return;
		}
		goodsService.updateStatus($scope.selectIds,status).success(function(response){
			if(response.flag){
				$scope.reloadList();//刷新列表
				$scope.selectIds = [];
			}else{
				alert(response.message);
			}
		});
	}
	$scope.updateState = function(id){
		goodsService.updateStatus(id,"1").success(function(response){
			if(response.flag){
				alert(response.message);
				$scope.reloadList();//刷新列表
				$scope.selectIds = [];
			}else{
				alert(response.message);
			}
		});
	}
	
	// 显示全部产品: 审核,未审核的
	$scope.showAll = function(){
		$scope.searchEntity.auditStatus = null;
		$scope.reloadList();//刷新列表
	}
	
	// 静态页
	$scope.show = function(id){
		window.open("/shop/"+id+".htm");
	}
});









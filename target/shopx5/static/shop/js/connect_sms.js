/* 手机注册JS
 * @copyright  Copyright (c) 2007-2018 ShopX5 Inc. (http://www.)
 * @license    http://www.
 * @link       http://www.
*/	

//获取短信验证码
function get_sms_captcha(type){
    if($("#phone").val().length == 11 && $("#image_captcha").val().length == 4){
		$.ajax({
			type: "post",
			data: {"type":type, "captcha":$('#image_captcha').val(), "phone":$('#phone').val()},
			url: "/user/getSmsCaptcha",
			async: false,
			success: function(rs){
                if(rs == 'true') {
                	$("#sms_text").html('短信动态码已发出');
                } else {
                    alert('获取短信失败,请重新获取!');
                }
		    }
		});
	} else {
		alert("请输入正确手机号或验证码 !");
	}
}

//手机注册 下一步
function check_captcha(){
    if($("#phone").val().length == 11 && $("#sms_captcha").val().length == 6){
		$.ajax({
			type: "post",
			data: {"sms_captcha":$('#sms_captcha').val(), "phone":$('#phone').val()},
			url: "/user/checkCaptcha",
			async: false,
			success: function(rs){
        	    if(rs.status == 200) {
        	    	$("#member_name").val(rs.data.username);  //赋值: 随机用户名
        	    	$("#sms_password").val(rs.data.password); //赋值: 随机密码
        	    	$("#telephone").val(rs.data.phone);       //赋值: 电话
        	    	$("#register_sms_form #captcha").val(rs.data.captcha);       //赋值: Code
        	        $("#register_sms_form").show();
        	        $("#post_form").hide();
        	    } else {
        	    	alert('系统故障!');
        	    }
		    }
		});
	}
}











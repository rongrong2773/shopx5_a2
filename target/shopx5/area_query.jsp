<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>省市区三级联动</title>
<style type="text/css">
	body  { padding-left:400px;padding-top:150px;background-color:#00CC99;}
	select{ width:100px;height:30px;cursor:pointer;} /*background-color:#99FF33;*/
</style>
</head>

<body>

	点击时，自动加载第二级以及第三级的数据。（二、三级默认的第一项）。<br>
	
	<select name="province" id="sel_province">
		<option value="">请选择省</option>
	</select>
	<select name="city" id="sel_city">
		<!-- <option value="">请选择市</option> -->
	</select>
	<select name="area" id="sel_area">
		<!-- <option value="">请选择区</option> -->
	</select>
	
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	$('#sel_city').hide();
	$('#sel_area').hide();
	//自动加载联动一级菜单
	$.ajax({
		url: "/area/getProvince",
		success: function(data) {
			if (data.status != 200) return;
			var html = "";
			for(var e in data.data){
				html += '<option value='+data.data[e].id+'>'+data.data[e].name+'</option>';
			}
			$("#sel_province").append(html);
			//通过change方法调用
			$("#sel_province").change(function(){
				$("#sel_city").html("");
				$("#sel_area").html("");
			  //$('#sel_city').hide();
				$('#sel_area').hide();
				getCity($(this).val()); //parentId				
			});
		},
		dataType: "json"
	});
})

function getCity(id){
	//加载二级菜单
	$.ajax({
		type: "post",
		url: "/area/getCity",
		data: {"parentId" : id},
		success: function(data) {
			if (data.status != 200) return;
			var html = "";
			for(var e in data.data){
				html += '<option value='+data.data[e].id+'>'+data.data[e].name+'</option>';
			}
			$("#sel_city").append(html);
			$('#sel_city').show();			
			//默认加载第一项
		//	$("#sel_city").get(0).selectedIndex = 0; //选中项为第0项
		//	if ($("#sel_city").val() != null)        //防止港澳台等二三级bug*	
		//		getArea($("#sel_city").val());       //parentId   此3行放开为默认加载二三级,不放开一级一级加载★
			//通过change方法调用
			$("#sel_city").change(function(){
				$("#sel_area").html("");
				getArea($(this).val());
			});
		},
		dataType: "json"
	});
}

function getArea(id){
	//加载三级菜单
	$.ajax({
		type: "post",
		url: "/area/getArea",
		data: {"parentId" : id},
		success: function(data) {
			if (data.status != 200) return;
			var html = "";
			for(var e in data.data){
				html += '<option value='+data.data[e].id+'>'+data.data[e].name+'</option>';
			}
			$("#sel_area").append(html);
			$('#sel_area').show();
		},
		dataType: "json"
	});
}
</script>
</body>
</html>

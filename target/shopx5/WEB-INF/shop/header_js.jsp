<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--[if IE 7]>
	<link href="/shop/css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="/static/shop/js/html5shiv.js" type="text/javascript"></script>
	<script src="/static/shop/js/respond.min.js" type="text/javascript"></script>
<![endif]-->
<script type="text/javascript">
var _CHARSET = 'utf-8';
</script>
<script src="/static/shop/js/jquery.js" type="text/javascript"></script>
<script src="/static/shop/js/jquery.ui.js" type="text/javascript"></script>
<script src="/static/shop/js/jquery.validation.min.js" type="text/javascript"></script>
<script src="/static/shop/js/common.js" charset="utf-8" type="text/javascript"></script>
<script type="text/javascript">
var PRICE_FORMAT = '&yen;%s';
$(function(){
	//首页左侧分类菜单
	$(".category ul.menu").find("li").each(
		function() {
			$(this).hover(
				function() {
				    var cat_id = $(this).attr("cat_id");
					var menu = $(this).find("div[cat_menu_id='"+cat_id+"']");
					menu.show();
					$(this).addClass("hover");					
					var menu_height = menu.height();
					if (menu_height < 60) menu.height(80);
					menu_height = menu.height();
					var li_top = $(this).position().top;
					$(menu).css("top",-li_top + 38);
				},
				function() {
					$(this).removeClass("hover");
				    var cat_id = $(this).attr("cat_id");
					$(this).find("div[cat_menu_id='"+cat_id+"']").hide();
				}
			);
		}
	);
	$(".head-user-menu dl").hover(function() {
		$(this).addClass("hover");
	},
	function() {
		$(this).removeClass("hover");
	});
	//我的商城 右上 ------/
	$('.head-user-menu .my-mall').mouseover(function(){
		load_history_information();
		$(this).unbind('mouseover');
	});
	//购物车 右上 ------/
	$('.head-user-menu .my-cart').mouseover(function(){
		load_cart_information();
		$(this).unbind('mouseover');
	});
	//初始化购物车 数量显示 ------/
	$.post("/shopx5/cart/findCartList", function(result) {
		var some=0;
		$(result).each(function(i,r){
			$(r.orderItemList).each(function(j,n){
				some++;
			})
		})
		var obj = $('.head-user-menu .my-cart');
		if (obj.find('.addcart-goods-num').size()==0) { //创建
			obj.prepend('<div class="addcart-goods-num">0</div>');
		}
		obj.find('.addcart-goods-num').html(some); //右侧购物车显示数量
		$('#rtoobar_cart_count').html(some).show();
	}, "json");
	//搜索 关键词
	$('#button').click(function(){
	    if ($('#keyword').val() == '') {
		    if ($('#keyword').attr('data-value') == '') {
			    return false
			} else {
                var op_val = $('#search_op').val();
				window.location.href="/index?act=search&op="+op_val+"&keyword="+$('#keyword').attr('data-value');
			    return false;
			}
	    }
	});
	$(".head-search-bar").hover(null,
	function() {
		$('#search-tip').hide();
	});
	// input ajax tips
	$('#keyword').focus(function(){
    if ($('#search_op').val() == 'index') {
      $('#search-tip').show();
    }
  }).autocomplete({
		//minLength:0,
        source: function (request, response) {
            $.getJSON('/index?act=search&op=auto_complete', request, function (data, status, xhr) {
                $('#top_search_box > ul').unwrap();
                response(data);
                if (status == 'success') {
                    $('#search-tip').hide();
                    $(".head-search-bar").unbind('mouseover');
                    $('body > ul:last').wrap("<div id='top_search_box'></div>").css({'zIndex':'1000','width':'362px'});
                }
            });
       },
		select: function(ev,ui) {
			$('#keyword').val(ui.item.label);
			$('#top_search_form').submit();
		}
	});
	$('#search-his-del').on('click',function(){$.cookie('9204_his_sh',null,{path:'/'});$('#search-his-list').empty();});
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>我的订单 - ShopX5商城系统,Java商城系统</title>
<meta name="keywords" content="ShopX5,Java商城系统,ShopX5商城系统,电子商务解决方案" />
<meta name="description" content="ShopX5专注于研发符合时代发展需要的电子商务商城系统" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<meta name="renderer" content="webkit" />
<meta name="renderer" content="ie-stand" />
<link href="/static/shop/css/base.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_header.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<%@ include file="../header_js.jsp" %>
</head>

<body>
<%@ include file="../header.jsp" %>
<link href="/static/shop/css/member.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/static/shop/js/member.js"></script>
<script type="text/javascript" src="/static/shop/js/ToolTip.js"></script>
<script>
$(document).ready(function() {
    $.each($(".side-menu > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
            if (ulNode.css('display') == 'block') {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),1);
            } else {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),null);
            }
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
	$.each($(".side-menu-quick > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
});
</script>
<div class="ncm-container">
<%@ include file="header_left_shop.jsp" %>
<div class="right-layout">
<link rel="stylesheet" type="text/css" href="/static/shop/css/jquery.ui.css" />
<div class="wrap">
  <div class="tabmenu">
    <ul class="tab pngFix">
 	 <li class="active"><a  href="/shop/index/order">订单列表</a></li>
  	 <li class="normal"><a  href="javascript:void(0);">回收站</a></li>
    </ul>
  </div>
  
<script type="text/javascript">
	$(function(){
		$('select[name="state_type"]').change(function(){
			query();
		})
	});
	function query() {
		$('#query_start_date').val('');
		$('#query_end_date').val('');
		$('#keyword').val('');
		queryList();
	}
	function queryList() {
		$('#orderForm').submit();
	}
</script>
  <form id="orderForm" action="/shop/index/order" method="post" target="_self">
    <table class="ncm-search-table">
      <tr>
      	<td>&nbsp;</td>
        <th>订单状态</th>
        <td class="w100">
          <select name="state_type">
            <option value=""  <c:if test="${orderParam.state_type == 0 }">selected</c:if>>所有订单</option>
            <option value="1" <c:if test="${orderParam.state_type == 1 }">selected</c:if>>待付款</option>
            <option value="2" <c:if test="${orderParam.state_type == 2 }">selected</c:if>>待发货</option>
            <option value="4" <c:if test="${orderParam.state_type == 4 }">selected</c:if>>待收货</option>
            <option value="5" <c:if test="${orderParam.state_type == 5 }">selected</c:if>>已完成</option>
            <option value="5" <c:if test="${orderParam.state_type == 5 }">selected</c:if>>待评价</option>
            <option value="6" <c:if test="${orderParam.state_type == 6 }">selected</c:if>>已取消</option>
          </select>
        </td>
        <th>下单时间</th>
        <td class="w240" style="width:260px">
        	<input type="text" class="text w70" name="query_start_date" id="query_start_date" value="${orderParam.query_start_date}" />
       	    <label class="add-on"><i class="icon-calendar"></i></label> &nbsp;&#8211;&nbsp;            
            <input type="text" class="text w70" name="query_end_date" id="query_end_date" value="${orderParam.query_end_date}" />
            <label class="add-on"><i class="icon-calendar"></i></label>            
            <!-- <input type="text" name="" value="" onClick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyy-MM-dd'});" /> -->
        </td>
        <td class="w240 tr"><input class="text w200" type="text" id="keyword" name="keyword" placeholder="输入商品标题或订单号进行搜索" value="${orderParam.keyword}" /></td>
        <td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="搜索" /></label></td>
      </tr>
    </table>
  </form>  
  <table class="ncm-default-table order">
    <thead>
      <tr>
        <th class="w10"></th>
        <th colspan="2">商品</th>
        <th class="w90">单价（元）</th>
        <th class="w40">数量</th>
        <th class="w90">优惠活动</th>
        <th class="w90">售后维权</th>
        <th class="w110">订单金额</th>
        <th class="w90">交易状态</th>
        <th class="w120">交易操作</th>
      </tr>
    </thead>
    
    <c:forEach items="${pageBean.list }" var="o">    
    <tbody class="pay" >
      <tr>
        <td colspan="19" class="sep-row"></td>
      </tr>
      <c:if test="${o.status==1 }">
      <tr><td colspan="19" class="pay-td"><span class="ml15">在线支付金额：
        <em>￥<fmt:formatNumber value="${o.payment }" groupingUsed="true" maxFractionDigits="2" minFractionDigits="2" /></em></span>
        <a class="ncbtn ncbtn-bittersweet fr mr15" href="/shop/confirm_pay.htm#?orderId=${o.orderId }" target="_blank"><i class="icon-shield"></i>支付订单</a></td>
      </tr>
      </c:if>
      <tr>
        <th colspan="19">
        	<span class="ml10">订单号：${o.orderId }</span>
			<span>下单时间：<fmt:formatDate value="${o.createTime }" type="date" pattern="yyyy-MM-dd HH:mm" /></span> 
			<span><a href="javascript:void(0);" title="${o.sellerId }">${o.sellerId }</a></span> 
			<span member_id="1"></span>
			<a href="javascript:void(0)" class="share-goods" nc_type="sharegoods" data-param='{"gid":"${o.orderId }"}'>
			<!-- <i class="icon-share"></i>分享</a> -->
        </th>
      </tr>
    <c:forEach items="${o.orderItems }" var="oi" varStatus="vs">
      <tr>
        <td class="bdl"></td>
        <td class="w70">
        <div class="ncm-goods-thumb">
        	<a href="/shop/${oi.goodsId }.htm" target="_blank"><img src="${oi.picPath }" onMouseOver="toolTip('<img src=${oi.picPath }>')" onMouseOut="toolTip()" /></a>
        </div>
        </td>        
        <td class="tl">
        <dl class="goods-name">
            <dt><a href="/shop/${oi.goodsId }.htm" target="_blank">${oi.title }</a>
            <!-- <span class="rec"><a href="/" target="_blank">[交易快照]</a></span> --></dt>
        </dl>
        </td>
        <td><fmt:formatNumber value="${oi.price }" groupingUsed="true" maxFractionDigits="2" minFractionDigits="2" /><p class="green"> </p></td>
        <td>${oi.num }</td>
        <td><!-- 优惠活动 --></td>
        <td><!-- 售后维权:退款, 投诉 --></td>
     <c:if test="${vs.count == 1 }">
        <td class="bdl" rowspan="1" style="border-bottom:none">
          <p class=""><strong>&yen;<fmt:formatNumber value="${o.payment }" groupingUsed="true" maxFractionDigits="2" minFractionDigits="2" /></strong></p>
          <p class="goods-freight">（免运费）</p>
          <c:if test="${o.paymentType == 1 }"><p title="支付方式：在线付款">在线付款</p></c:if>
          <c:if test="${o.paymentType == 0 }"><p title="支付方式：货到付款">货到付款</p></c:if>
        </td>
        <td class="bdl" rowspan="1" style="border-bottom:none">
          <c:if test="${o.status == 1 }"><p>待付款 </p></c:if>
          <c:if test="${o.status == 2 }"><p>已付款 </p></c:if>
          <c:if test="${o.status == 3 }"><p>未发货 </p></c:if>
          <c:if test="${o.status == 4 }"><p>已发货 </p></c:if>
          <c:if test="${o.status == 5 }"><p>交易成功</p></c:if>
          <c:if test="${o.status == 6 }"><p>交易关闭 </p></c:if>
          <p><a href="javascript:void(0);" target="_blank">订单详情</a></p>
        </td>
        <td class="bdl bdr" rowspan="1" style="border-bottom:none">
          <c:if test="${o.status == 0 }">
          <p><a href="javascript:void(0)" class=""><i class="icon-ban-circle"></i>已取消订单</a></p>
		  </c:if>
          <c:if test="${o.status == 1 }">
          <p><a href="javascript:void(0)" onclick="qxdd('${o.orderId }')" class="ncbtn ncbtn-grapefruit">取消订单</a></p>
		  </c:if>
          <c:if test="${o.status == 2 }">
          <p><a href="javascript:void(0)" class="ncbtn ncbtn-grapefruit">等待发货</a></p>
		  </c:if>
		  <c:if test="${o.status == 3 }">
          <p><a href="javascript:void(0)" class="ncbtn ncbtn-grapefruit ">等待发货</a></p>
		  </c:if>
		  <c:if test="${o.status == 4 }">
          <p><a href="javascript:void(0)" class="ncbtn ncbtn-grapefruit">确认收货</a></p>
		  </c:if>
		  <c:if test="${o.status == 5 }">
          <p><a href="javascript:void(0)" class="ncbtn ncbtn-grapefruit">评价商品</a></p>
		  </c:if>
		  <c:if test="${o.status == 6 }">
          <p><a href="javascript:void(0)" class=""><i class="icon-ban-circle"></i>已删除订单</a></p>
		  </c:if>          
          <!-- 收货 -->
          <!-- 评价 -->
          <!-- 追加评价 -->
        </td>
      </tr>
    </c:if>
    <c:if test="${vs.count > 1 }">
        <td class="bdl" rowspan="1" style="border-bottom:none"></td>
        <td class="bdl" rowspan="1" style="border-bottom:none"></td>
        <td class="bdl bdr" rowspan="1" style="border-bottom:none"></td>
      </tr>
    </c:if>
    </c:forEach>
    </tbody>
    </c:forEach>
      
    <tfoot>
      <tr>
        <td colspan="19">
        	<form id="stable" action="/shop/index/order" method="post">
			<div class="pagination">
				<ul>
					<li><span>共${pageBean.totalCount }条</span></li>
					<!-- 判断当前页是否首页  -->
					<c:if test="${pageBean.currPage == 1 }">
						<li><span>首页</span></li>
					</c:if>
					<c:if test="${pageBean.currPage != 1 }">
						<li><a href="/shop/index/order?page=1"><span>首页</span></a></li>
						<li><a href="/shop/index/order?page=${pageBean.currPage-1 }"><span>上一页</span></a></li>
					</c:if>
					<!-- 展示所有页码 前四后五-->
					<c:forEach begin="${pageBean.currPage-5>0?pageBean.currPage-5:1 }"
						end="${pageBean.currPage+4>pageBean.totalPage?pageBean.totalPage:pageBean.currPage+4 }" step="1" var="n">
						<!-- 判断是否当前页 -->
						<c:if test="${pageBean.currPage == n }">
							<li><span class="currentpage">${n }</span></li>
						</c:if>
						<c:if test="${pageBean.currPage != n }">
							<li><a href="/shop/index/order?page=${n }"><span>${n }</span></a></li>
						</c:if>
					</c:forEach>
					<!-- 判断是否最后一页 -->
					<c:if test="${pageBean.currPage == pageBean.totalPage }">
						<li><span>末尾</span></li>
					</c:if>
					<c:if test="${pageBean.currPage != pageBean.totalPage }">
						<li><a href="/shop/index/order?page=${pageBean.currPage+1 }"><span>下一页</span></a></li>
						<li><a><span>页码:<input type="text" name="page" size="3"
						                style="height:10px;" onKeyDown="javascript:if(event.keyCode==13) search(this.value);" /></span></a></li>
						<li><a href="/shop/index/order?page=${pageBean.totalPage }"><span>末尾</span></a></li>
					</c:if>
				</ul>
			</div>
			</form>
			<!--分页结束-->
    	</td>
      </tr>
    </tfoot>
</table>
</div>
<script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
<script charset="utf-8" type="text/javascript" src="/static/shop/js/zh-CN.js"></script>
<script charset="utf-8" type="text/javascript" src="/static/shop/js/sns.js"></script>
<script type="text/javascript" src="/static/shop/js/jquery.thumb.min.js"></script>
<script type="text/javascript">
function search(value){
	$('#stable').submit();
}
function qxdd(id){
	if(confirm('确定要取消订单吗?')){
		$.post("/shopx5/order/delete", {"ids":id}, function(data) {
				if (data.flag) {
					document.location.reload();
				}
			}, "json"
		)
	}
}
$(function(){
	$('.ncm-goods-thumb img').jqthumb({
		width: 60,
		height: 60,
		after: function(imgObj){
			imgObj.css('opacity', 0).attr('title', $(this).attr('alt')).animate({opacity: 1}, 2000);
		}
	});
    $('#query_start_date').datepicker({dateFormat: 'yy-mm-dd'});
    $('#query_end_date').datepicker({dateFormat: 'yy-mm-dd'});
});
</script>
</div>
<div class="clear"></div>
</div>
<%@ include file="../footer.jsp" %>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="/static/shop/js/userindex.js"></script>
<div class="ncm-header">
  <div class="ncm-header-top">
    <div class="ncm-member-info">
      <div class="avatar">
        <a href="/shop/user/_head" title="修改头像"><img src="" /><div class="frame"></div></a>
      </div>
      <dl>
        <dt><a href="#" title="修改资料">tom</a></dt>
        <dd>会员等级：
          <div class="nc-grade-mini" style="cursor:pointer;">V0会员</div>
        </dd>
        <dd>账户安全：
          <div class="SAM"><a href="javascript:void(0);" title="安全设置">
            <div id="low" class="SAM-info"><span><em></em></span><strong></strong></div></a>
          </div>
        </dd>
        <dd>用户财产：
          <div class="user-account">
            <ul>
              <li id="pre-deposit"><a href="javascript:void(0);" title="我的余额：￥0.00"><span class="icon"></span></a></li>
              <li id="points"><a href="javascript:void(0);" title="我的积分：0分"><span class="icon"></span></a></li>
              <li id="voucher"><a href="javascript:void(0);" title="我的代金券：0张"><span class="icon"></span></a></li>
              <li id="envelope"><a href="javascript:void(0);" title="我的红包：0张"><span class="icon"></span></a></li>
            </ul>
          </div>
        </dd>
      </dl>
    </div>
    <div class="ncm-trade-menu">
      <div class="line-bg"></div>
      <dl class="trade-step-01">
        <dt>关注中</dt>
        <dd></dd>
      </dl>
      <ul class="trade-function-01">
        <li><a href="/shop/index/collection"><span class="tf01"></span><h5>商品</h5></a></li>
        <li><a href="/shop/index/collection"><span class="tf02"></span><h5>店铺</h5></a></li>
        <li><a href="/shop/index/view"><span class="tf03"></span><h5>足迹</h5></a></li>
      </ul>
      <dl class="trade-step-02">
        <dt>交易进行</dt>
        <dd></dd>
      </dl>
      <ul class="trade-function-02">
        <li><a href="javascript:void(0);"><sup>0</sup><span class="tf04"></span><h5>待付款</h5></a> </li>
        <li><a href="javascript:void(0);"><span class="tf05"></span><h5>待收货</h5></a> </li>
        <li><a href="javascript:void(0);"><span class="tf06"></span><h5>待自提</h5></a> </li>
        <li><a href="javascript:void(0);"><span class="tf07"></span><h5>待评价</h5></a> </li>
      </ul>
      <dl class="trade-step-03">
        <dt>售后服务</dt>
        <dd></dd>
      </dl>
      <ul class="trade-function-03">
        <li><a href="javascript:void(0);"><span class="tf08"></span><h5>退款</h5></a></li>
        <li><a href="javascript:void(0);"><span class="tf09"></span><h5>退货</h5></a></li>
        <li><a href="javascript:void(0);"><span class="tf10"></span><h5>投诉</h5></a></li>
      </ul>
    </div>
  </div>
  <div class="ncm-header-nav">
    <ul class="nav-menu">
      <li><a href="javascript:void(0);" class="current">我的商城</a></li>
      <li class="set"><a href="javascript:void(0);">用户设置<i></i></a>
        <div class="sub-menu">
          <dl>
            <dt><a href="javascript:void(0);" style="color:#3AAC8A">安全设置</a></dt>
            <dd><a href="javascript:void(0);">修改登录密码</a></dd>
            <dd><a href="javascript:void(0);">手机绑定</a></dd>
            <dd><a href="javascript:void(0);">邮件绑定</a></dd>
            <dd><a href="javascript:void(0);">支付密码</a></dd>
          </dl>
          <dl>
            <dt><a href="/shop/user/_info" style="color:#EA746B">个人资料</a></dt>
            <dd><a href="/shop/index/addr">收货地址</a></dd>
            <dd><a href="/shop/user/_info">修改头像</a></dd>
            <dd><a href="javascript:void(0);">消息接受设置</a></dd>
          </dl>
          <dl>
            <dt><a href="javascript:void(0);" style="color:#FF7F00">账户财产</a></dt>
            <dd><a href="javascript:void(0);">余额充值</a></dd>
            <dd><a href="javascript:void(0);">领取代金券</a></dd>
            <dd><a href="javascript:void(0);">领取红包</a></dd>
          </dl>
          <dl>
            <dt><a href="javascript:void(0);" style="color:#398EE8">账号绑定</a></dt>
            <dd><a href="javascript:void(0);">QQ绑定</a></dd>
            <dd><a href="javascript:void(0);">微博绑定</a></dd>
            <dd><a href="javascript:void(0);">微信绑定</a></dd>
            <dd><a href="javascript:void(0);">分享绑定</a></dd>
          </dl>
        </div>
      </li>
      <li><a href="javascript:void(0);">个人主页<i></i></a>
        <div class="sub-menu">
          <dl>
            <dd><a href="javascript:void(0);">新鲜事</a></dd>
            <dd><a href="javascript:void(0);">个人相册</a></dd>
            <dd><a href="javascript:void(0);">分享商品</a></dd>
            <dd><a href="javascript:void(0);">分享店铺</a></dd>
          </dl>
        </div>
      </li>
      <li><a href="javascript:void(0);">其他应用<i></i></a>
        <div class="sub-menu">
          <dl>
            <dd><a href="javascript:void(0);">我的CMS</a></dd>
            <dd><a href="javascript:void(0);">我的圈子</a></dd>
            <dd><a href="javascript:void(0);">我的微商城</a></dd>
          </dl>
        </div>
      </li>
    </ul>
    <div class="notice">
      <ul class="line">
      </ul>
    </div>
    <script>
	$(function() {
		var _wrap = $('ul.line');
		var _interval = 2000;
		var _moving;
		_wrap.hover(function() {
			clearInterval(_moving);
		},
		function() {
			_moving = setInterval(function() {
				var _field = _wrap.find('li:first');
				var _h = _field.height();
				_field.animate({
					marginTop: -_h + 'px'
				},
				600,
				function() {
					_field.css('marginTop', 0).appendTo(_wrap);
				})
			}, _interval)
		}).trigger('mouseleave');
	});
	</script>
  </div>
</div>
<div class="left-layout">
  <ul id="sidebarMenu" class="ncm-sidebar">
    <li class="side-menu"><a href="javascript:void(0);" key="trade">
      <h3>交易中心</h3>
      </a>
      <ul >
        <li class="selected"><a href="/shop/index/order">实物交易订单</a></li>
        <li ><a href="javascript:void(0);">交易评价</a></li>
        <li ><a href="javascript:void(0);">到货通知</a></li>
      </ul>
    </li>
    <li class="side-menu"><a href="javascript:void(0);" key="follow">
      <h3>关注中心</h3>
      </a>
      <ul >
        <li ><a href="/shop/index/collection">商品收藏</a></li>
        <li ><a href="/shop/index/collection">店铺收藏</a></li>
        <li ><a href="/shop/index/view">我的足迹</a></li>
      </ul>
    </li>
    <li class="side-menu"><a href="javascript:void(0);" key="client">
      <h3>客户服务</h3>
      </a>
      <ul style="display:none">
        <li ><a href="javascript:void(0);">退款及退货</a></li>
        <li ><a href="javascript:void(0);">交易投诉</a></li>
        <li ><a href="javascript:void(0);">商品咨询</a></li>
        <li ><a href="javascript:void(0);">违规举报</a></li>
        <li ><a href="javascript:void(0);">平台客服</a></li>
      </ul>
    </li>
    <li class="side-menu"><a href="javascript:void(0);" key="info">
      <h3>会员资料</h3>
      </a>
      <ul >
        <li ><a href="/shop/index/addr">收货地址</a></li>
        <li ><a href="/shop/user/_head">账户信息</a></li>
      </ul>
    </li>
    <li class="side-menu"><a href="javascript:void(0);" key="property">
      <h3>财产中心</h3>
      </a>
      <ul style="display:none">
        <li ><a href="javascript:void(0);" class="hidfont">账户余额</a></li>
        <li ><a href="javascript:void(0);">我的代金券</a></li>
        <li ><a href="javascript:void(0);">我的红包</a></li>
      </ul>
    </li>
  </ul>
</div>

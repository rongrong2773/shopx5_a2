<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!doctype html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户详情 - ShopX5商城系统,Java商城系统</title>
<meta name="keywords" content="ShopX5,Java商城系统,ShopX5商城系统,电子商务解决方案" />
<meta name="description" content="ShopX5专注于研发符合时代发展需要的电子商务商城系统" />
<meta name="author" content="ShopX5" />
<meta name="copyright" content="ShopX5 Inc. All Rights Reserved" />
<link href="/static/shop/css/basei.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/home_headeri.css" rel="stylesheet" type="text/css" />
<link href="/static/shop/css/font-awesomei.min.css" rel="stylesheet" type="text/css" />
<%@ include file="../header_js.jsp" %>
</head>

<body>
<%@ include file="../header.jsp" %>
<link href="/static/shop/css/member2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/static/shop/js/member.js"></script>
<script type="text/javascript" src="/static/shop/js/ToolTip.js"></script>
<script>
$(document).ready(function() {
    $.each($(".side-menu > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
            if (ulNode.css('display') == 'block') {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),1);
            } else {
            	$.cookie(COOKIE_PRE+'Mmenu_'+$(this).attr('key'),null);
            }
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
	$.each($(".side-menu-quick > a"), function() {
        $(this).click(function() {
            var ulNode = $(this).next("ul");
			ulNode.slideToggle();
				if ($(this).hasClass('shrink')) {
					$(this).removeClass('shrink');
				} else {
					$(this).addClass('shrink');
				}
        });
    });
});
</script>
<div class="ncm-container">
<%@ include file="header_left_user.jsp" %>

<div class="right-layout">
<link rel="stylesheet" type="text/css" href="/static/shop/css/jquery.ui.css"  />

<div class="wrap">
  <div class="tabmenu">
    <ul class="tab pngFix">
  	  <li class="active"><a href="/shop/user/_info">基本信息</a></li>
  	  <!-- <li class="normal"><a href="javascript:void(0);">兴趣标签</a></li> -->
  	  <li class="normal"><a href="/shop/user/_head">更换头像</a></li>
  	</ul>
  </div>
    <div class="ncm-default-form">
      <form method="post" id="profile_form" action="">
        <input type="hidden" name="form_submit" value="ok" />
        <input type="hidden" name="old_member_avatar" value="" />
        <input type="hidden" name="area_ids" id="area_ids" value="" />
        <dl>
          <dt>用户名称：</dt>
          <dd>
              <span class="w400">xiaoxiong&nbsp;&nbsp;
                <div class="nc-grade-mini" style="cursor:pointer">V0</div>
              </span>
              <span>&nbsp;&nbsp;隐私设置</span>
         </dd>
        </dl>
        <dl>
          <dt>邮箱：</dt>
          <dd><span class="w400"><span id="email">asfsadf@qq.com</span> &nbsp;&nbsp; <a href="">绑定邮箱</a></span>
            <span>
            <select name="privacy[email]">
              <option value="0" selected="selected">公开</option>
              <option value="1" >好友可见</option>
              <option value="2" >保密</option>
            </select>
            </span>
          </dd>
        </dl>
        <dl>
          <dt>真实姓名：</dt>
          <dd><span class="w400"><input type="text" id="name" class="text" maxlength="20" name="name" value="" /></span>
            <span>
            <select name="privacy[truename]">
              <option value="0" selected="selected">公开</option>
              <option value="1" >好友可见</option>
              <option value="2" >保密</option>
            </select>
            </span></dd>
        </dl>
        <dl>
          <dt>性别：</dt>
          <dd><span class="w400">
            <label>
              <input type="radio" name="member_sex" value="3" checked="checked" />
              保密</label>
            &nbsp;&nbsp;
            <label>
              <input type="radio" name="member_sex" value="2"  />
              女</label>
            &nbsp;&nbsp;
            <label>
              <input type="radio" name="member_sex" value="1"  />
              男</label>
            </span><span>
            <select name="privacy[sex]">
              <option value="0" selected="selected">公开</option>
              <option value="1" >好友可见</option>
              <option value="2" >保密</option>
            </select>
            </span></dd>
        </dl>
        <dl>
          <dt>生日：</dt>
          <dd><span class="w400">
            <input type="text" class="text" name="birthday" maxlength="10" id="birthday" value="" />
            </span><span>
            <select name="privacy[birthday]">
              <option value="0" selected="selected">公开</option>
              <option value="1" >好友可见</option>
              <option value="2" >保密</option>
            </select>
            </span></dd>
        </dl>
        <dl>
          <dt>QQ：</dt>
          <dd><span class="w400">
            <input type="text" class="text" maxlength="30" name="member_qq" value="" />
            </span><span>
            <select name="privacy[qq]">
              <option value="0" selected="selected">公开</option>
              <option value="1" >好友可见</option>
              <option value="2" >保密</option>
            </select>
            </span> </dd>
        </dl>
        <dl>
          <dt>阿里旺旺：</dt>
          <dd><span class="w400">
            <input name="member_ww" type="text" class="text" maxlength="50" id="member_ww" value="" />
            </span><span>
            <select name="privacy[ww]">
              <option value="0" selected="selected">公开</option>
              <option value="1" >好友可见</option>
              <option value="2" >保密</option>
            </select>
            </span></dd>
        </dl>
        <dl class="bottom">
          <dt></dt>
          <dd>
            <label class="submit-border">
              <input type="submit" class="submit" value="保存修改" />
            </label>
          </dd>
        </dl>
      </form>
  </div>
</div>
<script type="text/javascript">
//注册表单验证
$(function(){
	// userInfo
	$.post("/shop/user/info", function(data) {
			if (data != null) {
				$('#email').html(data.email);
				$('#name').val(data.name);
				$('#birthday').val(format(data.birthday));
			}
		}, "json"
	)
	// date
	$('#birthday').datepicker({dateFormat: 'yy-mm-dd'});
	// form
    $('#profile_form').validate({
    	submitHandler:function(form){
    		$('#area_ids').val($('#region').fetch('area_ids'));
			ajaxpost('profile_form', '', '', 'onerror')
		},
        rules : {
            member_truename : {
				minlength : 2,
                maxlength : 20
            },
            member_qq : {
				digits  : true,
                minlength : 5,
                maxlength : 12
            }
        },
        messages : {
            member_truename : {
				minlength : '姓名长度大于等于2位小于等于20位',
                maxlength : '姓名长度大于等于2位小于等于20位'
            },
            member_qq  : {
				digits    : '请填入正确的QQ号码',
                minlength : '请填入正确的QQ号码',
                maxlength : '请填入正确的QQ号码'
            }
        }
    });
});
function add0(m){return m<10?'0'+m:m}
function format(shijianchuo){
	//shijianchuo是整数，否则要parseInt转换
	var time = new Date(shijianchuo);
	var y = time.getFullYear();
	var m = time.getMonth()+1;
	var d = time.getDate();
	var h = time.getHours();
	var mm = time.getMinutes();
	var s = time.getSeconds();
	return y+'-'+add0(m)+'-'+add0(d);
}
</script> 
<script charset="utf-8" type="text/javascript" src="/static/shop/js/zh-CN.js" ></script>
</div>
<div class="clear"></div>
</div>
<%@ include file="../footer.jsp" %>
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="cti">
  <div class="wrapper">
    <ul>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/7day_60.gif" /></span><span class="name">7天退货</span></li>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/pz_60.gif" /></span><span class="name">品质承诺</span></li>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/psbf_60.gif" /></span><span class="name">破损补寄</span></li>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/jswl_60.gif" /></span><span class="name">急速物流</span></li>
    </ul>
  </div>
</div>
<div id="faq">
  <div class="wrapper">
    <ul>
      <li>
        <dl class="s1">
          <dt>帮助中心</dt>
          <dd><i></i><a href="#" title="积分细则" target="_blank">积分细则</a></dd>
          <dd><i></i><a href="#" title="积分兑换说明" target="_blank">积分兑换说明</a></dd>
          <dd><i></i><a href="#" title="忘记密码" target="_blank">忘记密码</a></dd>
        </dl>
      </li>
      <li>
        <dl class="s2">
          <dt>店主之家</dt>
          <dd><i></i><a href="#" title="如何申请开店" target="_blank">如何申请开店</a></dd>
		  <dd><i></i><a href="#" title="如何管理店铺" target="_blank">如何管理店铺</a></dd>
		  <dd><i></i><a href="#" title="商城商品推荐" target="_blank">商城商品推荐</a></dd>
        </dl>
      </li>
      <li>
        <dl class="s3">
          <dt>支付方式</dt>
          <dd><i></i><a href="#" title="分期付款" target="_blank">分期付款</a></dd>
          <dd><i></i><a href="#" title="公司转账" target="_blank">公司转账</a></dd>
          <dd><i></i><a href="#" title="在线支付" target="_blank">在线支付</a></dd>
        </dl>
      </li>
      <li>
        <dl class="s4">
          <dt>售后服务</dt>
          <dd><i></i><a href="#" title="退换货流程" target="_blank">退换货流程</a></dd>
          <dd><i></i><a href="#" title="返修/退换货" target="_blank">返修/退换货</a></dd>
          <dd><i></i><a href="#" title="退款申请" target="_blank">退款申请</a></dd>
        </dl>
      </li>
      <li>
        <dl class="s5">
          <dt>客服中心</dt>
          <dd><i></i><a href="#" title="会员修改密码" target="_blank">会员修改密码</a></dd>
          <dd><i></i><a href="#" title="会员修改个人资料" target="_blank">会员修改个人资料</a></dd>
          <dd><i></i><a href="#" title="修改收货地址" target="_blank">修改收货地址</a></dd>
        </dl>
      </li>
      <li>
        <dl class="s6">
          <dt>关于我们</dt>
          <dd><i></i><a href="#" title="合作及洽谈" target="_blank">合作及洽谈</a></dd>
          <dd><i></i><a href="#" title="联系我们" target="_blank">联系我们</a></dd>
          <dd><i></i><a href="#" title="关于ShopX5" target="_blank">关于ShopX5</a></dd>
        </dl>
      </li>
    </ul>
  </div>
</div>
<div id="footer" class="wrapper">
  <p>
      <a href="#">首页</a> |
      <a href="#">招聘英才</a> |
      <a href="#">合作及洽谈</a> |
      <a href="#">联系我们</a> |
      <a href="#">关于ShopX5</a>
  </p>
  Copyright 2010-2018 ShopX5 Inc.,All rights reserved.<br />
  Powered by <a href="/" target="_blank"><span class="vol"><font class="b">Shop</font><font class="o">X5</font></span></a>
  <br />
</div>

<script type="text/javascript" src="/static/shop/js/jquery.cookie.js"></script>
<link rel="stylesheet" type="text/css" href="/shop/css/perfect-scrollbar.min.css" /><!-- 拉动条 -->
<script type="text/javascript" src="/static/shop/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript">
$(function(){
	// Membership card购物卡
	$('[nctype="mcard"]').membershipCard({type:'shop'});
});
</script>

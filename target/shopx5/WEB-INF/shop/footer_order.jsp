<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="cti">
  <div class="wrapper">
    <ul>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/7day_60.gif" /></span><span class="name">7天退货</span></li>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/pz_60.gif" /></span><span class="name">品质承诺</span></li>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/psbf_60.gif" /></span><span class="name">破损补寄</span></li>
      <li><span class="line"></span><span class="icon"><img style="width:60px" src="/static/shop/images/jswl_60.gif" /></span><span class="name">急速物流</span></li>
    </ul>
  </div>
</div>
<div id="faq">
  <div class="wrapper"> </div>
</div>
<div id="footer" class="wrapper">
  <p><a href="/" target="_blank">首页</a> |
  <a href="#">招聘英才</a> |
  <a href="#">合作及洽谈</a> |
  <a href="#">联系我们</a> |
  <a href="#">关于ShopX5</a> </p>
  Copyright 2010-2018 ShopX5 Inc.,All rights reserved.<br />
  Powered by <span class="vol"><font class="b">Shop</font><font class="o">X5</font></span> <br />
</div>
<script type="text/javascript" src="/static/shop/js/jquery.cookie.js"></script>
<link href="/shop/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/static/shop/js/perfect-scrollbar.min.js"></script> 
<script type="text/javascript">
$(function(){
	// Membership card 购物卡
	$('[nctype="mcard"]').membershipCard({type:'shop'});
});
</script>

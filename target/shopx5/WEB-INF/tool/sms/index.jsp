<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>发送短信_阿里大于</title>
    <meta name="keywords" content='发送短信_阿里大于' />
    <meta name="description" content='发送短信_阿里大于' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div id="manager_add">
<form class="layui-form itemForm" id="itemeAddForm" action="/tool/toSms" method="post">
	<input type="hidden" name="smsId" id="smsId" value="${id }" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="填写信息" data-options="iconCls:'fa fa-th'" style="padding:20px 0 0 20px"><!-- tabs1 -->
<div style="width:550px;float:left">
		  
		  <!-- <div class="layui-form-item">
			<label class="layui-form-label">KeyId</label>
			<div class="layui-input-block">
			  <input type="text" name="accessKeyId" placeholder="请输入 阿里大于accessKeyId" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">KeySecret</label>
			<div class="layui-input-block">
			  <input type="text" name="accessKeySecret" placeholder="请输入 阿里大于accessKeySecret" autocomplete="off" class="layui-input">
			</div>
		  </div> -->
		  <div class="layui-form-item">
			<label class="layui-form-label">template</label>
			<div class="layui-input-block">
			  <input type="text" name="template_code" placeholder="请输入 阿里大于template_code短信模板" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">param</label>
			<div class="layui-input-block">
			  <input type="text" name="sign_name" placeholder="请输入 阿里大于签名" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">接收号码</label>
			<div class="layui-input-block">
			  <input type="text" name="mobile" placeholder="请输入 接收号码 (多个用;分号隔开)" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">验证码</label>
			<div class="layui-input-block">
			  <input type="text" name="param" placeholder="请输入 验证码" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item" style="margin-top:15px">
			<div class="layui-input-block">
			  <button class="layui-btn" type="submit" lay-submit lay-filter="formDemo">立即发送</button>
			  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		  </div>
		  
</div>
<div style="width:100px;float:left">
</div>
		</div>
    </div>
</form>
</div>
<script type="text/javascript">
$(function(){
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			mobile : "required",
			param  : {required:true, range:[0,999999]},
		},
		messages: {
			mobile : "<dl style='color:red'>请输入接收号码 (多个用;分号隔开)</dl>",
			param  : {required:"<dl style='color:red'>验证码不能为空</dl>", range:"<dl style='color:red'>输入的范围在{0}~{999999}之间</dl>"},
		}
	})
	//回调信息
	if($('#smsId').val() == 1){
		alert("短信发送成功!")
	}
})
</script>
</body>
</html>






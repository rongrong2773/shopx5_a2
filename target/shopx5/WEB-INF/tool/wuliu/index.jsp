<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>查询物流轨迹</title>
    <meta name="keywords" content='查询物流轨迹' />
    <meta name="description" content='查询物流轨迹' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div id="manager_add">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="填写信息" data-options="iconCls:'fa fa-th'" style="padding:20px 0 0 20px"><!-- tabs1 -->
<div style="width:850px;float:left">
		  
		  <div class="layui-form-item">
			<label class="layui-form-label"></label>
			<div class="layui-input-block" style="color:blue;font-size:16px;font-weight:bold;">
			  只需填写单号，自动查询国内外<span style="color:red;">350多家快递信息。</span>
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">物流单号：</label>
			<div class="layui-input-block" style="width:230px">
			  <input type="text" name="expNo" id="expNo" value="" placeholder="输入 物流单号" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">物流信息：</label>
			<div class="layui-input-block">
			<table class="layui-table">
			  <colgroup>
			    <col width="110">
			    <col width="110">
			    <col>
			  </colgroup>
			  <thead>
			    <tr>
			      <th>日期</th>
			      <th>时间</th>
			      <th>物流轨迹</th>
			    </tr> 
			  </thead>
			  <tbody id="expcent">
			    
			  </tbody>
			</table>
			</div>
		  </div>
		  
		  <div class="layui-form-item" style="margin-top:15px">
			<div class="layui-input-block">
			  <input class="layui-btn" type="submit" lay-submit lay-filter="formDemo" id="submit"></input>
			</div>
		  </div>
		  
</div>
<div style="width:100px;float:left">
</div>
		</div>
    </div>
</form>
</div>
<script type="text/javascript">
$(function(){
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			expNo : {required:true, digits:true}
		},
		messages: {
			expNo : "<dl style='color:red'>请输入整数</dl>",
		}
	})
	$("#submit").on("click",function(){
		$.post("/tool/findExp", {"expNo":$("#expNo").val()}, function(data) {
				if (data) {
					$("#expcent").html("");
					var html = "";
					for(var i in data){
						var pd = data[i];
						var date = pd.AcceptTime;
						var date1 = date.substring(0,10);
						var date2 = date.substring(11);
						html += "<tr><td style='padding-left:3px'>"+date1+"</td><td style='padding-left:3px'>"+date2+"</td><td style='padding-left:5px'>"+pd.AcceptStation+"</td>";
					}  
					$("#expcent").append(html);
				}
			}, "json"
		)
	})
})
</script>
</body>
</html>

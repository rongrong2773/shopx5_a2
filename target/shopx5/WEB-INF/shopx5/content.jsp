<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>管理</title>
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/service/contentService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/contentCatService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/uploadService.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/contentController.js"></script>
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5"	ng-controller="contentController" ng-init="findContentCatList()">
	<!-- .box-body
	<div class="box-header with-border">
		<h3 class="box-title">管理</h3>
	</div> -->
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<button type="button" class="btn btn-default" title="新建"
							data-toggle="modal" data-target="#editModal" ng-click="entity={}">
							<i class="fa fa-file-o"></i> 新建
						</button>
						<button type="button" class="btn btn-default" title="删除" ng-click="dele()">
							<i class="fa fa-trash-o"></i> 删除
						</button>
						<button type="button" class="btn btn-default" title="刷新"
							onclick="window.location.reload();">
							<i class="fa fa-refresh"></i> 刷新
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback"></div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList" class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right: 0px"><input id="selall" type="checkbox" class="icheckbox_square-blue"ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting">ID</th>
						<th class="sorting">广告类型</th>
						<th class="sorting">内容标题</th>
						<th class="sorting">链接</th>
						<th class="sorting">图片绝对路径</th>
						<th class="sorting">状态</th>
						<th class="sorting">排序</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{contentCatNewList[entity.categoryId]}}</td>
						<td>{{entity.title}}</td>
						<td>{{entity.url}}</td>
						<td><img src="{{entity.pic}}" width="100" height="50"></td>
						<td>{{status[entity.status]}}</td>
						<td>{{entity.sortOrder}}</td>
						<td class="text-center">
							<button type="button" class="btn bg-olive btn-xs"
								data-toggle="modal" data-target="#editModal"
								ng-click="findOne(entity.id)">修改</button>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
		<!-- 分页 -->
		<tm-pagination conf="paginationConf"></tm-pagination>
	</div>
	<!-- /.box-body -->
	<!-- 编辑窗口 -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">×</button>
					<h3 id="myModalLabel">编辑</h3>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" width="800px">
						<tr>
							<td>内容类目ID</td>
							<td><select class="form-control"
								ng-model="entity.categoryId"
								ng-options="item.id as item.name for item in contentCatList"></select>
							</td>
						</tr>
						<tr>
							<td>内容标题</td>
							<td><input class="form-control" ng-model="entity.title"
								placeholder="内容标题"></td>
						</tr>
						<tr>
							<td>链接</td>
							<td><input class="form-control" ng-model="entity.url"
								placeholder="链接"></td>
						</tr>
						<tr>
							<td>图片绝对路径</td>
							<td><input type="file" id="file" />
								<input type="button" value="上传" ng-click="uploadFile()" />
								<img ng-if="entity.pic!=null" src="{{entity.pic}}" width="200" height="100"></td>
						</tr>
						<tr>
							<td>状态</td>
							<td><input type="checkbox" ng-model="entity.status"
								ng-true-value="1" ng-false-value="0" /></td>
						</tr>
						<tr>
							<td>排序</td>
							<td><input class="form-control" ng-model="entity.sortOrder"
								placeholder="排序"></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" data-dismiss="modal"
						aria-hidden="true" ng-click="save()">保存</button>
					<button class="btn btn-default" data-dismiss="modal"
						aria-hidden="true">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>



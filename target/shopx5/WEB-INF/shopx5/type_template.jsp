<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>类型模板管理</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/templateTypeController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/templateTypeService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/brandService.js"></script><!-- 引入 品牌Service -->
<script type="text/javascript" src="/shopx5/js/service/specificationService.js"></script><!-- 引入 规格Service -->
<!-- 引入select2的相关的css和js -->
<link rel="stylesheet" href="/shopx5/plugins/select2/select2.css" />
<link rel="stylesheet" href="/shopx5/plugins/select2/select2-bootstrap.css" />
<script src="/shopx5/plugins/select2/select2.min.js" type="text/javascript"></script> <!-- 这个3个是select2原生引入 -->
<script type="text/javascript" src="/shopx5/js/angular-select2.js"></script> <!-- 注意: angular整合select2 必须放在base_pagination.js下面 使用app -->
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5"	ng-controller="typeTemplateController" ng-init="findBrandList();findSpecList()">
	<!-- .box-body
	<div class="box-header with-border">
		<h3 class="box-title">商品类型模板管理</h3>
	</div> -->
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<button type="button" class="btn btn-default" title="新建"
							data-toggle="modal" data-target="#editModal" ng-click="entity={customAttributeItems:[]}">
							<i class="fa fa-file-o"></i> 新建
						</button>
						<button type="button" class="btn btn-default" ng-click="dele()"
							title="删除">
							<i class="fa fa-trash-o"></i> 删除
						</button>

						<button type="button" class="btn btn-default" title="刷新"
							onclick="window.location.reload();">
							<i class="fa fa-refresh"></i> 刷新
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback">
					模板名称：<input type="text" ng-model="searchEntity.name">
					<button class="btn btn-default" ng-click="reloadList()">查询</button>
				</div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList"
				class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right: 0px"><input id="selall" type="checkbox" class="icheckbox_square-blue" ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting_asc">模板ID</th>
						<th class="sorting">分类模板名称</th>
						<th class="sorting">关联品牌</th>
						<th class="sorting">关联规格</th>
						<th class="sorting">扩展属性</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{entity.name}}</td>
						<td>{{jsonToString(entity.brandIds,'text')}}</td>
						<td>{{jsonToString(entity.specIds,'text')}}</td>
						<td>{{jsonToString(entity.customAttributeItems,'text')}}</td>
						<td class="text-center">
							<button type="button" class="btn bg-olive btn-xs"
								ng-click="findOne(entity.id)" data-toggle="modal"
								data-target="#editModal">修改</button>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
			<!-- 分页 -->
			<tm-pagination conf="paginationConf"></tm-pagination>
		</div>
		<!-- 数据表格 /-->
						
	</div>
	<!-- /.box-body -->

	<!-- 编辑窗口 -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">商品类型模板编辑</h3>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" width="800px">
						<tr>
							<td>模板类型</td>
							<td><input class="form-control" placeholder="模板类型" ng-model="entity.name"></td>
						</tr>
						<tr>
							<td>关联品牌</td>
							<td>
							<!--
			      				ng-model:     绑定下拉列表的id
			      				select2-model:绑定下拉列表的id和value  [{},{},]
			      				config:       代表的是数据的来源              [{},]
		      			 	-->
		      			 	<input ng-model="c" select2 select2-model="entity.brandIds"
								config="brandList" multiple placeholder="支持多选哦"
								class="form-control" type="text" />      <!-- config="brandList" 查询关联的品牌信息 -->
							</td>                                        <!-- select2-model="entity.brandIds" 取遍历{}的id -->
						</tr>
						<tr>
							<td>关联规格</td>
							<td><input select2 select2-model="entity.specIds"
								config="specList" multiple placeholder="支持多选哦"
								class="form-control" type="text" /></td> <!-- config="brandList" 查询关联的规格信息 -->
						</tr>                                            <!-- select2-model="entity.specIds" 取遍历{}的id -->
						<tr>
							<td>扩展属性</td>
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-default"
										ng-click="addTableRow()" title="新增扩展属性">
										<i class="fa fa-file-o"></i> 新增扩展属性
									</button>

								</div>
								<table class="table table-bordered table-striped" width="800px">
									<thead>
										<tr>
											<!--<td><input type="checkbox" class="icheckbox_square-blue"></td>-->
											<td>属性名称</td>
											<td>操作</td>
										</tr>
									</thead>
									<tbody>                                                         <!-- 去掉ng-repeat产生的$$hashkey和object -->
										<tr ng-repeat="pojo in entity.customAttributeItems track by $index"> <!-- 加track by $index -->
											<!--<td><input type="checkbox" class="icheckbox_square-blue"></td>-->
											<td><input class="form-control" ng-model="pojo.text" placeholder="属性名称"></td>
											<td><button type="button" ng-click="deleteTableRow($index)" class="btn btn-default" title="删除">
												<i class="fa fa-trash-o"></i> 删除 </button>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" data-dismiss="modal"
						aria-hidden="true" ng-click="save()">保存</button>
					<button class="btn btn-default" data-dismiss="modal"
						aria-hidden="true">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>商品管理</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/goodsController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/goodsService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/itemCatService.js"></script>
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5" ng-controller="goodsController"
						ng-init="searchEntity={auditStatus:'0',sellerId:''};findItemCatList()">
	<!-- .box-body -->
	<div class="box-header with-border">
		<h3 class="box-title">商品审核</h3>
	</div>
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<button type="button" ng-click="showAll()"
							class="btn btn-default" title="所有商品">
							<i class="fa fa-ban"></i> 所有商品
						</button>
						<button type="button" onclick="window.location.reload();"
							class="btn btn-default" title="未审商品">
							<i class="fa fa-refresh"></i> 未审商品
						</button>
						<button type="button" ng-click="updateStatus('1')"
							class="btn btn-default" style="color:#F00;" title="审核">
							<i class="fa fa-check"></i> 审核
						</button>
						<button type="button" ng-click="updateStatus('0')"
							class="btn btn-default" title="驳回">
							<i class="fa fa-ban"></i> 驳回
						</button>
						<button type="button" ng-click="dele()"
							class="btn btn-default" title="删除">
							<i class="fa fa-trash-o"></i> 删除
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback">
					商品名称：<input type="text" ng-model="searchEntity.goodsName">
					<button class="btn btn-default" ng-click="reloadList()">查询</button>
				</div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList"
				class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right:0px"><input id="selall" type="checkbox" class="icheckbox_square-blue" ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting_asc">商品ID</th>
						<th class="sorting">商品名称</th>
						<th class="sorting">商户ID</th>
						<th class="sorting">商品价格</th>
						<th class="sorting">一级分类</th>
						<th class="sorting">二级分类</th>
						<th class="sorting">三级分类</th>
						<th class="sorting">状态</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{entity.goodsName}}</td>
						<td>{{entity.sellerId}}</td>
						<td>&yen; {{entity.price}}</td>
						<td>{{itemCatList[entity.category1Id]}}</td>
						<td>{{itemCatList[entity.category2Id]}}</td>
						<td>{{itemCatList[entity.category3Id]}}</td>
						<td><span style="color:{{oColor[entity.auditStatus]}};"> {{status[entity.auditStatus]}} </span></td>
						<td class="text-center">
							<button ng-if="entity.auditStatus==0" type="button" class="btn bg-olive btn-xs" ng-click="updateState(entity.id)">审核</button>
							<a ng-if="entity.auditStatus==1" href="/shop/{{entity.id}}.htm" class="btn bg-olive btn-xs" target="_blank">查看</a>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
		<!-- 分页 -->
		<tm-pagination conf="paginationConf"></tm-pagination>
	</div>
	<!-- /.box-body -->
</body>
</html>



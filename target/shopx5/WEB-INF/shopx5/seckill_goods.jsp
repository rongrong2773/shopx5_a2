<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>秒杀商品管理</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/seckillGoodsController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/seckillGoodsService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/uploadService2.js"></script> <!-- 上传 -->
<script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script> 
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5" ng-controller="seckillGoodsController" ng-init="">
	<!-- .box-body -->
	<div class="box-header with-border">
		<h3 class="box-title">秒杀商品管理</h3>
	</div>
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<button type="button" class="btn btn-default" title="新建"
							data-toggle="modal" data-target="#editModal" ng-click="entity={}">
							<i class="fa fa-file-o"></i> 新建
						</button>
						<button type="button" class="btn btn-default" ng-click="dele()" title="删除">
							<i class="fa fa-trash-o"></i> 删除
						</button>
						<button type="button" class="btn btn-default" title="刷新"
							onclick="window.location.reload();">
							<i class="fa fa-refresh"></i> 刷新
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback">
					状态：<select ng-model="searchEntity.status">
						<option value="">全部</option>
						<option value="0">未申请</option>
						<option value="1">审核通过</option>
						<option value="2">审核未通过</option>
						<option value="3">关闭</option>
					</select> 商品名称：<input type="text" ng-model="searchEntity.title">
					<button class="btn btn-default" ng-click="reloadList()">查询</button>
				</div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList"
				class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right:0px"><input id="selall" type="checkbox" class="icheckbox_square-blue" ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting_asc">商品ID</th>
						<th class="sorting">商品名称</th>
						<th class="sorting">商品原价</th>
						<th class="sorting">秒杀价</th>
						<th class="sorting">商品描述</th>
						<th class="sorting">商品图</th>
						<th class="sorting">状态</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{entity.title}}</td>
						<td>&yen; {{entity.price.toFixed(2)}}</td>
						<td>&yen; {{entity.costPrice.toFixed(2)}}</td>
						<td>{{entity.introduction}}</td>
						<td><img src="{{entity.smallPic}}" width="50" height="30" /></td>
						<td><span style="color:{{oColor[entity.status]}}">{{oStatus[entity.status]}}</span></td>
						<td class="text-center">
							<a class="btn bg-olive btn-xs"
								ng-click="findOne(entity.id)" data-toggle="modal"
								data-target="#editModal">修改</button>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
		<!-- 分页 -->
		<tm-pagination conf="paginationConf"></tm-pagination>
	</div>
	<!-- /.box-body -->
	<!-- 编辑窗口 -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">秒杀商品编辑</h3>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped" width="800px">
						<tr>
							<td>商品标题</td>
							<td><input class="form-control" ng-model="entity.title" placeholder="商品标题"></td>
							<!-- 填数据,自动封装到entity对象中 -->
						</tr>
						<tr>
							<td>描述</td>
							<td><input class="form-control" ng-model="entity.introduction" placeholder="描述"></td>
						</tr>
						<tr>
							<td>商品图片</td>
							<td><img ng-if="entity.smallPic != null" src="{{entity.smallPic}}" height="50" />
								<input type="file" id="file" />
								<button class="btn btn-primary" type="button" ng-click="uploadFile()">上传</button>
							</td>
						</tr>
						<tr>
							<td>原价格</td>
							<td><input class="form-control" ng-model="entity.price" placeholder="原价格"></td>
						</tr>
						<tr>
							<td>秒杀价格</td>
							<td><input class="form-control" ng-model="entity.costPrice" placeholder="秒杀价格"></td>
						</tr>
						<tr>
							<td>秒杀商品数</td>
							<td><input class="form-control" ng-model="entity.num" placeholder="秒杀商品数"></td>
						</tr>
						<tr>
							<td>开始时间</td>
							<td><input class="form-control" ng-model="entity.startTime" id="startTime" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyy-MM-dd'});" placeholder="开始时间"></td>
						</tr>
						<tr>
							<td>结束时间</td>
							<td><input class="form-control" ng-model="entity.endTime"  id="endTime" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyy-MM-dd'});" placeholder="结束时间"></td>
						</tr>
						
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" ng-click="save()">保存</button>
					<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</button>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="keywords" content="ShopX5管理框架">
    <meta name="description" content="topjui管理框架，专注你的后端业务开发！">
    <title>xs后台管理系统 - 用户登录</title>
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico">
    <link rel="stylesheet" href="/static/shopx5ui/static/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css">
    <style type="text/css">
       html,body{height:100%}.box{background:url("/static/shopx5ui/topjui/images/loginBg.jpg") no-repeat center center;background-size:cover;
       margin:0 auto;position:relative;width:100%;height:100%}.login-box{width:100%;max-width:500px;height:400px;position:absolute;
       top:50%;margin-top:-200px}@media screen and (min-width:500px){.login-box{left:50%;margin-left:-250px}}.form{width:100%;
       max-width:500px;height:275px;margin:2px auto 0 auto}.login-content{border-bottom-left-radius:8px;border-bottom-right-radius:8px;
       height:250px;width:100%;max-width:500px;background-color:rgba(255,250,2550,.6);float:left}
       .input-group{margin:30px 0 0 0!important}.form-control,.input-group{height:40px}.form-actions{margin-top:30px}
       .form-group{margin-bottom:0!important}.login-title{border-top-left-radius:8px;border-top-right-radius:8px;padding:20px 10px;
       background-color:rgba(0,0,0,.6)}.login-title h1{margin-top:10px!important}.login-title small{color:#fff}
       .link p{line-height:20px;margin-top:30px}.btn-sm{padding:8px 24px!important;font-size:16px!important}
       .flag{position:absolute;top:10px;right:10px;color:#fff;font-weight:bold;
       font:14px/normal "microsoft yahei","Times New Roman","宋体",Times,serif}
    </style>
</head>

<body>
<div class="box">
    <div class="login-box">
        <div class="login-title text-center">
            <span class="flag"><i class="fa fa-user"></i> 用户登陆</span>
            <h1>
                <small>XS后台管理系统 多用户</small>
            </h1>
        </div>
        <div class="login-content" style="height:300px">
            <div class="form">
                <form action="#" id="modifyPassword" class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-xs-10 col-xs-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" id="username" name="userName" class="form-control" placeholder="用户名" value="shopx5">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-10 col-xs-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" id="password" name="password" class="form-control" placeholder="密码" value="123456">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-10 col-xs-offset-1">
                            <div style="padding:30px 0 0 0">
                                <input type="text" name="captcha" placeholder=" 输入验证码" size="8" style="height:32px"
                                		onkeydown="javascript:if(event.keyCode==13){submitForm();}">
                                <img src="/code?type=login&n=2" id="codeimage" style="margin-top:-2px" />
                                <a id="makecode" href="javascript:void(0);">看不清，换一张</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-center">
                            <button type="button" id="login" class="btn btn-sm btn-success">
                                <span class="fa fa-check-circle"></span> 登录
                            </button>
                            <button type="button" id="reset" class="btn btn-sm btn-danger" style="margin-left:10px;">
                                <span class="fa fa-close"></span> 重置
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="text-danger"><i class="fa fa-warning"></i> 用户名或密码错误，请重试！</span>
            </div>
        </div>
    </div>
</div>
<style type="text/css">  
#loadpagediv{width:160px;height:56px;position:absolute;top:50%;left:50%;margin-left:-80px;margin-top:-28px;
background:url(/images/ico_loading.gif) no-repeat;z-index:9999;display:none}
</style>
<div id="loadpagediv"></div>
<!-- 引入jQuery -->
<script src="/static/shopx5ui/static/plugins/jquery/jquery.min.js"></script>
<script src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
<script src="/static/shopx5ui/static/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="/static/shopx5ui/static/plugins/bootstrap/plugins/html5shiv.min.js"></script>
<script src="/static/shopx5ui/static/plugins/bootstrap/plugins/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if(navigator.appName == "Microsoft Internet Explorer" &&
        (navigator.appVersion.split(";")[1].replace(/[ ]/g, "") == "MSIE6.0" ||
         navigator.appVersion.split(";")[1].replace(/[ ]/g, "") == "MSIE7.0" ||
         navigator.appVersion.split(";")[1].replace(/[ ]/g, "") == "MSIE8.0")
    ){
        alert("您的浏览器版本过低，请使用360安全浏览器的极速模式或IE9.0以上版本的浏览器");
    }
</script>
<script type="text/javascript">
    $(function(){
    	$("#codeimage").on("click", function(){
			$("#codeimage").prop("src","/code?type=login&n=" + Math.random());
		});
    	$("#makecode").on("click", function(){
			$("#codeimage").prop("src","/code?type=login&n=" + Math.random());
		});
        $('#password').keyup(function (event) {
            if (event.keyCode == "13") {
                $("#login").trigger("click");
                return false;
            }
        });
        $("#login").on("click", function () {
            submitForm();
        });
        $("#reset").on("click", function () {
            $("#username").val("");
            $("#password").val("");
        });
    });
   	function submitForm() {
       	$('#loadpagediv').show();
		if(navigator.appName == "Microsoft Internet Explorer" &&
			(navigator.appVersion.split(";")[1].replace(/[ ]/g, "") == "MSIE6.0" ||
			 navigator.appVersion.split(";")[1].replace(/[ ]/g, "") == "MSIE7.0" ||
			 navigator.appVersion.split(";")[1].replace(/[ ]/g, "") == "MSIE8.0")
		){
			alert("您的浏览器版本过低，请使用360安全浏览器的极速模式或IE9.0以上版本的浏览器");
		}else{
			$.ajax({type:"post", url:"/user/login", data:$("#modifyPassword").serialize(), async:false, dataType:"json", 
				success:function(data){
					$('#loadpagediv').hide();
					if(data.status==200){
						if (data.msg!="") {
							location.href=data.msg;
						}
					}else{
						alert(data.msg);
					}
				}
			});
		}
    };
</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<form class="layui-form itemForm" id="itemeAddForm" style="margin:0;padding:0;color:#333">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
          <div class="layui-form-item">
			<label class="layui-form-label">用户名</label>
			<div class="layui-input-block">
			  <input type="text" name="username" placeholder="请输入名称" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">信息记录</label>
			<div class="layui-input-block">
			  <input type="text" name="content" placeholder="请输入信息记录" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">IP地址</label>
			<div class="layui-input-block">
			  <input type="text" name="ipAddress" placeholder="请输入IP地址" autocomplete="off" class="layui-input">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
$(function(){
	//validate校验
	$("#itemeAddForm").validate({
		rules: {
			userName : "required",
			content  : "required",
		},
		messages: {
			userName : "<dl style='color:red'>请输入用户名</dl>",
			content  : "<dl style='color:red'>请输入信息记录</dl>",
		}
	})
})
</script>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="renderer" content="webkit" />
    <title>ShopX5后台管理系统</title>
    <meta name="keywords" content='' />
    <meta name="description" content='' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" /><!-- 浏览器标签图片 -->
	
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" /><!-- ShopX5系统框架样式 -->
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
<!--<link rel="stylesheet" type="text/css" href="../easyui/themes/metro/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css" />-->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" /><!-- 自定义css -->
	
    <!-- layui框架样式 框架js-->
    <link type="text/css" href="/static/shopx5ui/static/plugins/layui/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/static/shopx5ui/static/plugins/layui/layui.js" charset="utf-8"></script>
    
    <!-- UEditor编辑器  -->
    <link type="text/css" href="/cpts/ueditor/themes/default/css/ueditor.css" rel="stylesheet" />
	<script type="text/javascript"charset="utf-8" src= "/cpts/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/ueditor.all.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>

<body>
<!--1.加载
<div id="loading" class="loading-wrap">
    <div class="loading-content">
        <div class="loading-round"></div>
        <div class="loading-dot"></div>
    </div>
</div>-->

<!--2.layout 布局 -->
<div id="index_layout" class="easyui-layout" data-options="fit:true">
	<!--2.1 头部 -->
    <div id="north" class="banner" data-options="region:'north',border:false,split:false" style="height:50px;padding:0;margin:0;overflow:hidden;">
        <table style="float:left;border-spacing:0px;">
            <tr>
                <td class="webname">
                    <span class="fa fa-home" style="font-size:26px;padding-right:8px"></span>
                </td>
                <td class="collapseMenu" style="text-align:center;cursor:pointer">
                    <span class="fa fa-chevron-circle-left" style="font-size:18px"></span><!-- <<收放 -->
                </td>
                <td>
                    <table id="topmenucontent" cellpadding="0" cellspacing="0">
                        <td id="0" title="系统导航" class="topmenu selected systemName">
                            <a class="l-btn-text bannerMenu" href="javascript:void(0)">
                                <p><lable class="fa fa-tree"></lable></p>
                                <p><span style="white-space:nowrap">&nbsp;&nbsp;&nbsp;系统导航&nbsp;&nbsp;&nbsp;</span></p>
                            </a>
                        </td>
                        <td id="9001" title="商城首页" class="topmenu">
                            <a class="l-btn-text bannerMenu" href="/" target="_blank">
                                <p><lable class="fa fa-envira"></lable></p>
                                <p><span style="white-space:nowrap">&nbsp;&nbsp;&nbsp;商城首页&nbsp;&nbsp;&nbsp;</span></p>
                            </a>
                        </td>
                        <td id="9000" title="动态演示" class="topmenu">
                            <a class="l-btn-text bannerMenu" href="/" target="_blank">
                                <p><lable class="fa fa-leaf"></lable></p>
                                <p><span style="white-space:nowrap">&nbsp;&nbsp;&nbsp;动态演示&nbsp;&nbsp;&nbsp;</span></p>
                            </a>
                        </td>
						<td id="8999" title="开发文档" class="topmenu systemName">
                            <a class="l-btn-text bannerMenu" href="javascript:void(0)">
                                <p><lable class="fa fa-book"></lable></p>
                                <p><span style="white-space:nowrap">&nbsp;&nbsp;&nbsp;开发文档&nbsp;&nbsp;&nbsp;</span></p>
                            </a>
                        </td>
                        <td id="9002" title="交流中心" class="topmenu">
                            <a class="l-btn-text bannerMenu" href="javascritp:void(0);">
                                <p><lable class="fa  fa-wechat"></lable></p>
                                <p><span style="white-space:nowrap">&nbsp;&nbsp;&nbsp;交流中心&nbsp;&nbsp;&nbsp;</span></p>
                            </a>
                        </td>
                    </table>
                </td>
            </tr>
        </table>
        <span style="float:right;padding-right:10px;height:50px; line-height:50px;">
            <a href="javascript:void(0)" class="easyui-menubutton"
               data-options="iconCls:'fa fa-user',hasDownArrow:false" style="color:#fff;"><dl id="maName"></dl></a>|
            <a href="javascript:void(0)" id="mb3" class="easyui-menubutton"
               data-options="menu:'#mm3',iconCls:'fa fa-cog',hasDownArrow:true" style="color:#fff;">设置</a>
            <div id="mm3" style="width:74px;">
                <div data-options="iconCls:'fa fa-info-circle'" onclick="javascript:modifyPwd(0)">个人信息</div>
                <div class="menu-sep"></div>
                <div data-options="iconCls:'fa fa-key'" onclick="javascript:modifyPwd(0)">修改密码</div>
            </div>|
            <div id="pwdDialog">
            <table class="editTable" style="margin:30px 0 0 0"><form id="pwdForm">
			  <tr><td class="label">用户名</td><td><input type="text" data-toggle="topjui-textbox" name="userName"></td></tr>
			  <tr><td class="label">姓名</td><td><input type="text" data-toggle="topjui-textbox" name="name"></td></tr>
			  <tr><td class="label">新密码</td><td><input type="text" data-toggle="topjui-passwordbox" name="password" id="password"></td></tr></form>
			</table>
            </div>
            <a href="javascript:void(0)" id="mb2" class="easyui-menubutton"
               data-options="menu:'#mm2',iconCls:'fa fa-tree',hasDownArrow:true" style="color:#fff;">主题</a>|
            <div id="mm2" style="width:180px;">
                <div data-options="iconCls:'fa fa-tree blue'" onclick="changeTheme('blue')">默认主题</div>
                <div data-options="iconCls:'fa fa-tree'" onclick="changeTheme('black')">黑色主题</div>
                <div data-options="iconCls:'fa fa-tree'" onclick="changeTheme('blacklight')">黑色主题-亮</div>
                <div data-options="iconCls:'fa fa-tree red'" onclick="changeTheme('red')">红色主题</div>
                <div data-options="iconCls:'fa fa-tree red'" onclick="changeTheme('redlight')">红色主题-亮</div>
                <div data-options="iconCls:'fa fa-tree green'" onclick="changeTheme('green')">绿色主题</div>
                <div data-options="iconCls:'fa fa-tree green'" onclick="changeTheme('greenlight')">绿色主题-亮</div>
                <div data-options="iconCls:'fa fa-tree purple'" onclick="changeTheme('purple')">紫色主题</div>
                <div data-options="iconCls:'fa fa-tree purple'" onclick="changeTheme('purplelight')">紫色主题-亮</div>
                <div data-options="iconCls:'fa fa-tree blue'" onclick="changeTheme('blue')">蓝色主题</div>
                <div data-options="iconCls:'fa fa-tree blue'" onclick="changeTheme('bluelight')">蓝色主题-亮</div>
                <div data-options="iconCls:'fa fa-tree orange'" onclick="changeTheme('yellow')">橙色主题</div>
                <div data-options="iconCls:'fa fa-tree orange'" onclick="changeTheme('yellowlight')">橙色主题-亮</div>
            </div>
            <a href="javascript:void(0)" onclick="logout()" class="easyui-menubutton"
               data-options="iconCls:'fa fa-sign-out',hasDownArrow:false" style="color:#fff;">注销</a>
        </span>
    </div>
    
	<!--2.2 左西 导航 -->
    <div id="west" data-options="region:'west',split:true,width:230,border:false,headerCls:'border_right',bodyCls:'border_right'"
         title="" iconCls="fa fa-dashboard">
        <div id="RightAccordion">
        	
        </div>
    </div>
    
	<!--2.3 右中 tabs -->
    <div id="center" data-options="region:'center',border:false" style="overflow:hidden;">
        <div id="index_tabs" style="width:100%;height:100%">
            <div title="系统首页" iconCls="fa fa-home" data-options="border:true,
    content:'<iframe src=\'../../static/shopx5ui/html/portal/index.html\' scrolling=\'auto\' frameborder=\'0\' style=\'width:100%;height:100%;\'></iframe>',">
            </div>
        </div>
    </div>
    
	<!--2.4 底部 -->
    <div data-options="region:'south',border:true"
         style="text-align:center;height:30px;line-height:30px;border-bottom:0;overflow:hidden;">
        <span style="float:left;padding-left:5px;width:30%;text-align:left;" id="maName2">当前用户：</span>
        <span style="padding-right:5px;width:40%"> 版权所有 © 2019 -
            <a href="http://106.12.204.102/manage/toMain" target="_blank">XS后台管理系统</a>
            <a href="http://www.miitbeian.gov.cn" target="_blank"></a>
        </span>
        <span style="float:right;padding-right:5px;width:30%;text-align:right;">版本：<script>document.write("version 2.3")</script></span>
    </div>
</div>
<!-- layout布局 end -->

<!-- tabs右键关闭组件 -->
<div id="mm" class="submenubutton" style="width:140px;display:none;">
    <div id="mm-tabclose" name="6" iconCls="fa fa-refresh">刷新</div>
    <div class="menu-sep"></div><!-- line -->
    <div id="Div1" name="1" iconCls="fa fa-close">关闭</div>
    <div id="mm-tabcloseother" name="3">关闭其他</div>
    <div id="mm-tabcloseall" name="2">关闭全部</div>
    <div class="menu-sep"></div>
    <div id="mm-tabcloseright" name="4">关闭右侧标签</div>
    <div id="mm-tabcloseleft" name="5">关闭左侧标签</div>
    <div class="menu-sep"></div>
    <div id="mm-newwindow" name="7">新窗口中打开</div>
</div>

<!-- 文章面板panel -->
<div id="artpanel"></div>

<!--[if lte IE 8]>
<div id="ie6-warning">
    <p>您正在使用低版本浏览器，在本页面可能会导致部分功能无法使用，建议您升级到
		<a href="http://www.microsoft.com/china/windows/internet-explorer/" target="_blank">IE9或以上版本的浏览器</a>
		或使用<a href="http://se.360.cn/" target="_blank">360安全浏览器</a>的极速模式浏览
    </p>
</div>
<![endif]-->
<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script><!-- cookie保存主体样式 -->
<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script><!-- 自定义 -->

<script type="text/javascript">
//首页加载完后，取消加载中状态
$(window).load(function(){
    $("#loading").fadeOut("fast", function(){});
});
// 全屏参
var isFullScreen = false;

$(function(){
	// 折叠展开导航
    $(".collapseMenu").on("click", function(){
    	var p = $("#index_layout").layout("panel", "west")[0].clientWidth;  //返回指定面板 panel宽度
        if (p > 0) {
            $('#index_layout').layout('collapse', 'west'); //折叠指定面板
            $(this).children('span').removeClass('fa-chevron-circle-left').addClass('fa-chevron-circle-right');
        } else {
            $('#index_layout').layout('expand', 'west');   //展开指定面板
            $(this).children('span').removeClass('fa-chevron-circle-right').addClass('fa-chevron-circle-left');
        }
    });
    
 	// 首页tabs选项卡
	var index_tabs = $('#index_tabs').tabs({
		fit: true,
		border : true,
		tools: [{  //右上角4按钮
            iconCls: 'fa fa-home',
            handler: function(){
                $('#index_tabs').tabs('select', 0);
            }
        }, {
            iconCls: 'fa fa-refresh',
            handler: function(){
                var refresh_tab = $('#index_tabs').tabs('getSelected');
                //var index = $('#index_tabs').tabs('getTabIndex',refresh_tab); alert(index); //0
                var refresh_iframe = refresh_tab.find('iframe')[0]; //content:'<iframe..'会给iframe加载内部代码中,为后台给find()★
                refresh_iframe.contentWindow.location.href = refresh_iframe.src;  //contentWindow属性是指定的frame或iframe所在的window对象
                //$('#index_tabs').trigger(TOPJUI.eventType.initUI.base);
                //var index = $('#index_tabs').iTabs('getTabIndex', $('#index_tabs').iTabs('getSelected'));
                //console.log(index);
                //$('#index_tabs').iTabs('getTab', index).iPanel('refresh');
            }
        }, {
            iconCls: 'fa fa-close',
            handler: function(){
                var index = $('#index_tabs').tabs('getTabIndex', $('#index_tabs').tabs('getSelected')); //获取选择的选项卡面板索引
                var tab = $('#index_tabs').tabs('getTab', index); //获取指定选项卡面板
                //if (tab.panel('options').closable)              //只删带X
                  if (index != 0)                                 //除首页,其他都删
                    $('#index_tabs').tabs('close', index);
            }
        }, {
            iconCls: 'fa fa-arrows-alt',
            handler: function(){
                App().handleFullScreen(); //使用固定组件
            }
        }],
        //监听右键事件，创建右键菜单，右击一个选项卡面板时触发
        onContextMenu: function (e, title, index) {
            e.preventDefault();
            if (index >= 0) {
                $('#mm').menu('show', {
                    left: e.pageX,
                    top: e.pageY
                }).data("tabTitle", title); //★: $.data向元素附加数据(,) 下面方法用到取数据
            }
        }
	});

	//tab右键菜单,节点被点击
    $("#mm").menu({
        onClick: function (item) {
            tabMenuOprate(this, item.name); //刷新,关闭等   DOM对象 $封装对象
        }
    });
	
    // 初始化accordion   //########第1步 左侧组件 实例化
    $("#RightAccordion").accordion({
        fit: true,
        border: false
    });
 	
    // 绑定横向导航菜单点击事件   //########第3步 导航加载 开始前
    $(".systemName").on("click", function (e) {
        //generateMenu(e.currentTarget.dataset.menuid, e.target.textContent); //IE9及以下不兼容data-menuid属性
        //generateMenu(e.target.getAttribute('data-menuid'), e.target.textContent);
        generateMenu($(this).attr("id"), $(this).attr("title"));  //########第4步 导航加载 显示方法
        $(".systemName").removeClass("selected");
        $(this).addClass("selected");
    });

    // 主页打开初始化时显示第一个系统的菜单
    //$('.systemName').eq('0').trigger('click'); //触发被选元素的指定事件类型
    $('.systemName').eq('0').trigger('click');  //########第2步 顶部导航 默认点击 id="1325" ShopX5开发文档
    //$('.systemName:eq(0)').trigger('click');
    //generateMenu(1325, "系统配置");

    // 显示系统首页
    /*setTimeout(function(){
     var indexTab = [];
     indexTab.iconCls = "icon-house";
     indexTab.text = "系统门户";
     var portal = $.getUrlParam("portal");
     if (portal == "system" || portal == null) portal = "system";
     indexTab.url = "html/portal/index.html";
     indexTab.closable = false;
     indexTab.border = false;
     addTab(indexTab);
     }, 1);*/
});

//########第4步 导航加载 显示方法 id="1325" ShopX5系统导航
//生成左侧导航菜单               0     ShopX5系统导航
function generateMenu(menuId, systemName) {
 if (menuId < 9000) {
	 // 点击顶部导航另一项,清空之前项
	 var allPanel = $("#RightAccordion").accordion('panels'); //0.获取所有面板 首次为0
     var size = allPanel.length;
     if (size > 0) {
         for (i = 0; i < size; i++) {
        	 $("#RightAccordion").accordion('remove', 0);     //0.多次循环,每次删首项
         }
     }
	 // 获取第一层目录
     $.get("/manage/treeList", {"parentId": menuId}, function (data) {
             if(data == "" || data == undefined || data == null) return;
    	 	 // 加载循环创建手风琴项
             $.each(data, function (i, e) {
                 var isSelected = i == 3 ? true : false; //第一项默认选择 下面
                 $('#RightAccordion').accordion('add', { //2 开始增加左导航 一条一条加 **
                     fit: false,
                     title: e.text,
                     content: "<ul id='tree" + e.id + "'></ul>",
                     border: false,
                     selected: isSelected,
                     iconCls: e.iconCls       //图标样式
                 });
                 // 加载tree
                 $("#tree" + e.id).tree({
                     data: e.children,     //加载数据
                     lines: false,
                     animate: true,
                     onClick: function (node) {
                         if ($(this).tree("isLeaf", node.target)) { // 或  点击是叶子节点,$(this)=$("ul")
                      // if (node.curl) {
                             /*if(typeof node.attributes != "object") { //自定义属性不是"boject"类型,将json转对象类型
                             	 node.attributes = $.parseJSON(node.attributes);
                              }*/
                             addTab(node);
                         } else {
                             if (node.state == "closed") {
                                 $("#tree" + e.id).tree('expand', node.target);
                             } else if (node.state == 'open') {
                                 $("#tree" + e.id).tree('collapse', node.target);
                             }
                         }
                     }
                 });
             });
             //$.each end
         }, "json"
     );
 }
}

//3.打开Tab窗口
function addTab(node) {
	//1.第一种方式
    var t = $('#index_tabs');
    var $selectedTab = t.tabs('getSelected');            //默认选项卡,打开的是系统首页
    var selectedTabOpts = $selectedTab.panel('options');
    var iframe = '<iframe src="' + node.curl + '" scrolling="auto" frameborder="0" style="width:100%;height:100%;"></iframe>';
    if (node.curl != '#') {
		if ($('#index_tabs').tabs('exists', node.text)) {
			$('#index_tabs').tabs('select', node.text);
		} else {
			$('#index_tabs').tabs('add', {
				title    : node.text,
				iconCls  : node.iconCls,
				closable : true,   //显关闭按钮
				cache    : false,
				content  : iframe,
			});
		}
	}
    //2.第二种方式
    /* if($('#RightAccordion').tree("isLeaf",node.target)){ //判断点击的是: 叶子节点
		var tabs = $("#index_tabs");
		var tab = tabs.tabs("getTab",node.text);  //或"exists"
		if(tab){
			tabs.tabs("select",node.text);
		}else{
			tabs.tabs('add',{
			    title    : node.text,
			    iconCls  : node.iconCls,
			    closable : true,
			    cache    : false,
			    href     : node.curl,  //href★
			});
		}
	} */
}
// 展示另一窗口
function showAdd(text,url){
	if($('#index_tabs').tabs('exists', text)){
		$('#index_tabs').tabs('select', text);
	}else{
		$('#index_tabs').tabs('add',{
			title    : text,
			closable : true,
			cache    : false,
			content  : '<iframe src="'+url+'" scrolling="auto" frameborder="0" style="width:100%;height:100%;"></iframe>',
		});
	}
}
function showList(text,url){
	if($('#index_tabs').tabs('exists', text)){
		$('#index_tabs').tabs('select', text);
	}else{
		$('#index_tabs').tabs('add',{
			title    : text,
			closable : true,
			cache    : false,
			content  : '<iframe src="'+url+'" scrolling="auto" frameborder="0" style="width:100%;height:100%;"></iframe>',
		});
	}
}


//4.tabs右键菜单相关操作    (this, item.name);
function tabMenuOprate(menu, type) {
    var allTabs = $('#index_tabs').tabs('tabs');  //返回所有选项卡面板
    var allTabtitle = [];
    $.each(allTabs, function (i, n) {             //(索引, 选项卡)
        var opt = $(n).panel('options');          //返回属性对象
        //if (opt.closable)                       //1.若关闭按钮显示了,[]加入选项卡title   限制了有关闭按钮的才能全部关闭★★
            allTabtitle.push(opt.title);          //直接添加,是所有
    });
    var curTabTitle = $(menu).data("tabTitle");   //undefined=0 向元素附加数据(,),或取回该数据(目前)★
    var curTabIndex = $('#index_tabs').tabs("getTabIndex", $('#index_tabs').tabs("getTab", curTabTitle)); //上面加入数据(选择卡标题)
  //console.log('--'+menu+'--'+type+'--'+curTabTitle+'--'+curTabIndex);
  //    [object HTMLDivElement]--1--       系统首页---        0    
    switch (type) {
        case "1"://关闭当前
            if (curTabIndex > 0) {
                $('#index_tabs').tabs("close", curTabTitle);
                return false;
                break;
            } else {
                $.messager.show({
                    title: '操作提示',
                    msg: '首页不允许关闭！'
                });
                break;
            }
        case "2"://关闭全部
            for (var i = 1; i < allTabtitle.length; i++) {      //索引1开始,不关闭系统首页★
                $('#index_tabs').tabs('close', allTabtitle[i]);
            }
            break;
        case "3"://除此关闭其他全部
            for (var i = 1; i < allTabtitle.length; i++) {
                if (curTabTitle != allTabtitle[i])
                    $('#index_tabs').tabs('close', allTabtitle[i]);
            }
            $('#index_tabs').tabs('select', curTabTitle);
            break;
        case "4"://当前侧面右边
            for (var i = curTabIndex + 1; i < allTabtitle.length; i++) {
                $('#index_tabs').tabs('close', allTabtitle[i]);
            }
            $('#index_tabs').tabs('select', curTabTitle);
            break;
        case "5": //当前侧面左边
            for (var i = 1; i < curTabIndex; i++) {
                $('#index_tabs').tabs('close', allTabtitle[i]);
            }
            $('#index_tabs').tabs('select', curTabTitle);
            break;
        case "6": //刷新
            var currentTab = $('#index_tabs').tabs('getSelected');
            var current = currentTab.panel("options");            
        	//console.log(current);                       ★★★  发现新大陆, 如何制作刷新当前, 下面代码自己研究出来
        	$('#index_tabs').tabs("close", curTabTitle);
            $('#index_tabs').tabs('add', {
            	index : current.index,
				title : curTabTitle,
				iconCls : current.iconCls,
				closable : current.closable,   //显关闭按钮
				content : current.content,     //<iframe>
			});
            break;
        case "7": //在新窗口打开
            var refresh_tab = $('#index_tabs').tabs('getSelected');
            var refresh_iframe = refresh_tab.find('iframe')[0];
            window.open(refresh_iframe.src);
            break;
    }
}

//5.个人信息/修改密码
$('#pwdDialog').dialog({
	closed : true,
	title: '修改密码',	
    iconCls: 'fa fa-key',
    width: 400,
    height: 300,
    modal : true,    
    collapsible : true,
    minimizable : true,
    maximizable : true,
    resizable : true,
    buttons: [{
        text: '确定',
        iconCls: 'fa fa-save',
        handler: function(){
            if ($('#pwdDialog').form('validate')) {
                if ($("#password").val().length < 6) {
                    $.messager.alert('警告', '密码长度不能小于6位', 'warning');
                } else {
                    $.ajax({
                        url: '/manage/user/updateInfo',
                        type: 'post',
                        cache: false,
                        data: $("#pwdForm").serialize(),
                        beforeSend: function(){
                            $.messager.progress({
                                text: '正在操作...'
                            });
                        },
                        success: function (data, response, status) {
                            $.messager.progress('close');
                            if (data.status == 200) {
                                $.messager.show({
                                    title: '提示',
                                    msg: '操作成功'
                                });
                                $("#pwdDialog").dialog('close').form('reset');
                            } else {
                                $.messager.alert('操作失败！', '未知错误或没有任何修改，请重试！', 'error');
                            }
                        }
                    });
                }
            }
        }
    }, {
        text: '关闭',
        iconCls: 'fa fa-close',
        btnCls: 'topjui-btn-red',
        handler: function(){
            $("#pwdDialog").dialog('close');
        }
    }]
});
function modifyPwd() {
	$.post("/manage/user/getInfo", function(data) {
			if (data.status == 200) {
				$('[name="userName"]').val(data.data.userName);
				$('[name="name"]').val(data.data.userInfo.name);
			}
		}, "json"
	)
	$('#pwdDialog').dialog('open');
};

/**
 * 6.更换页面风格
 *   更换主题                     red
 */
function changeTheme(themeName) {
    var $dynamicTheme = $('#dynamicTheme');    //href="./topjui/themes/default/topjui.blue.css" id="dynamicTheme"
    var themeHref = $dynamicTheme.attr('href');
    var themeHrefNew = themeHref.substring(0, themeHref.indexOf('themes')) + 'themes/default/topjui.' + themeName + '.css';
    // 更换主页面风格
    $dynamicTheme.attr('href', themeHrefNew);
    
    // 更换iframe页面风格
    var $iframe = $('iframe');  //当前打开选择卡1个,也要更换样式★   
    if ($iframe.length > 0) {
        for (var i = 1; i < $iframe.length; i++) { //i = 1 此设置当前不执行
            var ifr = $iframe[i];
            var $iframeDynamicTheme = $(ifr).contents().find('#dynamicTheme'); //获取iframe内页面所有节点,再获取css=dynamicTheme修改样式★★
            var iframeThemeHref = $iframeDynamicTheme.attr('href');
            var iframeThemeHrefNew = iframeThemeHref.substring(0, iframeThemeHref.indexOf('themes')) + 'themes/default/topjui.' + themeName + '.css';
            $iframeDynamicTheme.attr('href', iframeThemeHrefNew);
        }
    }
	// 缓存记录7天
    $.cookie('topjuiThemeName', themeName, {
        expires: 7,
        path: '/'
    });
};
//查缓存
if ($.cookie('topjuiThemeName')) {
    changeTheme($.cookie('topjuiThemeName'));
}

//7.退出系统
function logout() {
    $.messager.confirm('提示', '确定要退出吗?', function (r) {
        if (r) {
            $.messager.progress({
                text: '正在退出中...'
            });
            window.location.href = '/admin/logout' + location.search;
        }
    });
}

//8.打开文章,tabs
// <a href="javascript:window.parent.addParentTab({href:'.html',title:'标题'})">标题</a>
function addParentTab(options) {
    var src, title;
    src = options.href;
    title = options.title;
    var iframe = '<iframe src="' + src + '" frameborder="0" style="border:0;width:100%;height:100%;"></iframe>';
    //方式1:
/*  window.parent.$('#index_tabs').tabs("add", {
        title: title,
        content: iframe,
        closable: true,
        iconCls: 'fa fa-th',
        border: true,
    }); */
    //方式2:
    $("#artpanel").dialog({
    	title : '面板',             //面板显示的标题文本。默认为 null。
    	width : 800,
    	height : 600,
		iconCls : 'fa fa-save',    //设置一个 16x16 图标的 CSS 类 ID 显示在面板左上角。默认为 null。
		collapsible : true,        //定义是否显示可折叠按钮。默认值 false
		//minimizable : true,        //定义是否显示最小化按钮。默认值 false。
		maximizable : true,        //定义是否显示最大化按钮。默认值 false。
		resizable : true,          //定义是否可以改变对话框窗口大小。默认false。
		//inline : true,
		//fit : true,         //面板大小将自适应父容器
		//fitColumns : true,  //自适应
    	content: iframe,    	
    });
}


//99.tabs全屏插件
var App = function(){
    var setFullScreen = function(){
        var docEle = document.documentElement;
        if (docEle.requestFullscreen) {
            //W3C
            docEle.requestFullscreen();
        } else if (docEle.mozRequestFullScreen) {
            //FireFox
            docEle.mozRequestFullScreen();
        } else if (docEle.webkitRequestFullScreen) {
            //Chrome等
            docEle.webkitRequestFullScreen();
        } else if (docEle.msRequestFullscreen) {
            //IE11
            docEle.msRequestFullscreen();
        } else {
            $.iMessager.alert('温馨提示', '该浏览器不支持全屏', 'messager-warning');
        }
    };
    //退出全屏 判断浏览器种类
    var exitFullScreen = function(){
        // 判断各种浏览器，找到正确的方法
        var exitMethod = document.exitFullscreen || //W3C
            document.mozCancelFullScreen ||    //Chrome等
            document.webkitExitFullscreen || //FireFox
            document.msExitFullscreen; //IE11
        if (exitMethod) {
            exitMethod.call(document);
        }
        else if (typeof window.ActiveXObject !== "undefined") {//for Internet Explorer
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    };
    return {
        init: function(){ },
        handleFullScreen: function(){
            if (isFullScreen) {
                exitFullScreen();
                isFullScreen = false;
            } else {
                setFullScreen();
                isFullScreen = true;
            }
        }
    };
};
</script>
<script type="text/javascript" src="/static/shop/js/ss/common.js"></script>
</body>
</html>






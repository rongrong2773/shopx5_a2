<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="renderer" content="webkit" />
    
    <title>内容分类--增删改</title>
    <meta name="keywords" content='' />
    <meta name="description" content='' />    
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" /><!-- 浏览器标签图片 -->
    
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" /><!-- TopJUI框架样式 -->
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
<!--<link rel="stylesheet" type="text/css" href="../easyui/themes/metro/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css" /> -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" /><!-- 自定义css -->
	
    <!-- layui框架样式 框架js-->
    <link type="text/css" href="/static/shopx5ui/static/plugins/layui/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/static/shopx5ui/static/plugins/layui/layui.js" charset="utf-8"></script>
    
    <!-- UEditor编辑器  -->
    <link type="text/css" href="/cpts/ueditor/themes/default/css/ueditor.css" rel="stylesheet" />
	<script type="text/javascript"charset="utf-8" src= "/cpts/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/ueditor.all.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="/cpts/ueditor/lang/zh-cn/zh-cn.js"></script>
	
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script><!-- cookie保存主体样式 -->
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script><!-- 自定义 -->
</head>
<body>

	<div>
		<ul id="contentCategory"></ul>
	</div>
	<div id="contentCategoryMenu" class="easyui-menu" style="width:120px;" data-options="onClick:menuHandler">
		<div data-options="iconCls:'icon-add',name:'add'">添加</div>
		<div data-options="iconCls:'icon-remove',name:'rename'">重命名</div>
		<div class="menu-sep"></div>
		<div data-options="iconCls:'icon-remove',name:'delete'">删除</div>
	</div>
	<script type="text/javascript">
	$(function(){
		$("#contentCategory").tree({
			url : '/content/category/list',                  //1.内容分类列表  [id text status]
			animate: true,
			method : "GET",
			onLoadSuccess : function (node, data) {          //数据加载成功后触发
				if (data) {
					$(data).each(function (index, value) {
						if (this.state == 'closed') {
							$('#contentCategory').tree('expandAll');//展开所有节点
						}
					});
				}
			},
			onContextMenu: function(e,node){            //2.tree所有节点右键时  (e此事件event对象,node哪个节点)★
				e.preventDefault();
				$(this).tree('select',node.target);     //选中节点 状态改变(背景变色)
				$('#contentCategoryMenu').menu('show',{ //弹出menu组件  ...点击先触发menuHandler()方法再执行下面onAfterEdit★★★
					left: e.pageX,
					top: e.pageY
				});
			},
			//2 编辑结束后,触发
			onAfterEdit : function(node){           //4. 下面默认add添加,onAfterEdit(添加之后)跳上来的真逻辑处理添加★
				var _tree = $(this);
				if(node.id == 0){
					//新增节点 id=0
					$.post("/content/category/create",{parentId:node.parentId,name:node.text},function(data){
						if(data.status == 200){
							_tree.tree("update",{   //a. 添加成功.更新节点id和name ★
								target : node.target,
								id : data.data.id   //b. 返回Result.ok(tbContentCategory);
							});
						}else{
							$.messager.alert('提示','创建'+node.text+' 分类失败!');
						}
					});
				}else{
					//更新节点 id!=0
					$.post("/content/category/update",{id:node.id,name:node.text});
				}
			}
		});
	});
	//1 先tree变成编辑状态 .tree('beginEdit')
	function menuHandler(item){                   //3.点击菜单项时调用的函数 写法固定
		var tree = $("#contentCategory");
		var node = tree.tree("getSelected");      //取选中节点
		
		if(item.name === "add"){
			tree.tree('append', {                 //3.1添加新节点 默认id=0
				parent: (node ? node.target : null),
				data: [{
					text: '新建分类',
					id : 0,
					parentId : node.id,           //写法固定parentId
				}],
			}); 
			var _node = tree.tree('find', 0);     //定义默认节点id=0★
			tree.tree("select",_node.target).tree('beginEdit',_node.target); //a.找新增节点id=0(第一个),为选中状态,变成可编辑状态 ★★
																			 //b.注意:当光标结束.. 跳到上面onAfterEdit真添加逻辑 ★
																			 //注意: 添加总是最上面id=0变成可编辑状态 ★
		}else if(item.name === "rename"){         //3.2编辑节点
			tree.tree('beginEdit',node.target);
			
		}else if(item.name === "delete"){         //3.3删除节点
			$.messager.confirm('确认','确定删除名为 '+node.text+' 的分类吗？',function(r){
				if(r){
					$.post("/content/category/delete/",{id:node.id},function(){  //注:这个取不到: parentId : node.parentId
						tree.tree("remove",node.target);
					});
				}
			});
		}
	}
	</script>
</body>
</html>



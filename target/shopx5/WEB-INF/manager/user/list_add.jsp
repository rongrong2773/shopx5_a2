<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<form class="layui-form itemForm" id="itemeAddForm" style="margin:0;padding:0;color:#333">
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
          <div class="layui-form-item">
			<label class="layui-form-label">所在部门</label>
			<div class="layui-input-block">
             <!-- <select name="deptId">
               <option value="">--请选择--</option>
             </select> -->
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_add');" value="选择" />
		     <input type="hidden" name="deptId" id="deptId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">登录名</label>
			<div class="layui-input-block">
			  <input type="text" name="userName" placeholder="请输入登录名" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">状态</label>
			<div class="layui-input-block">
			  <input type="checkbox" name="stateON" checked lay-skin="switch">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">姓名</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入姓名" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">薪水</label>
			<div class="layui-input-block">
			  <input type="text" name="salary" placeholder="请输入薪水" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">入职时间</label>
			<div class="layui-input-block">
			  <input type="text" name="joinDate" placeholder="请输入入职时间" autocomplete="off" class="layui-input" id="joindate">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">直属领导</label>
			<div class="layui-input-block">
             <!-- <select name="managerId">               
             </select> -->
             <input type="button" class="layui-btn layui-btn-sm" id="zsld" onclick="getZsld(1);" value="选择" />
		     <input type="hidden" name="managerId" id="managerId" />		     
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">出生年月</label>
			<div class="layui-input-block">
			  <input type="text" name="birthday" placeholder="请输入出生年月" autocomplete="off" class="layui-input" id="birthdate">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block">
			  <input type="radio" name="gender" value="1" title="男">
			  <input type="radio" name="gender" value="0" title="女" checked>
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">岗位</label>
			<div class="layui-input-block">
			  <input type="text" name="station" placeholder="请输入岗位" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">电话</label>
			<div class="layui-input-block">
			  <input type="text" name="telephone" placeholder="请输入电话" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">邮箱</label>
			<div class="layui-input-block">
			  <input type="text" name="email" placeholder="请输入邮箱" autocomplete="off" class="layui-input">
			</div>
		  </div>      		  
</div>
		  <div class="layui-form-item">
			<label class="layui-form-label">等级</label>
			<div class="layui-input-block">
			  <input type="radio" name="degree" value="0" title="超级管理员">
			  <input type="radio" name="degree" value="1" title="跨部门跨人员">
              <input type="radio" name="degree" value="2" title="管理所有下属部门和人员">
              <input type="radio" name="degree" value="3" title="管理本部门">
              <input type="radio" name="degree" value="4" title="普通员工" checked>
			</div>
		  </div>		  
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
			<div class="layui-form-item">
				<label class="layui-form-label">排序号</label>
				<div class="layui-input-block">
				  <input type="text" name="orderNo" placeholder="请输入排序号" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item layui-form-text">
				<label class="layui-form-label">说明</label>
				<div class="layui-input-block">
				  <textarea name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
				</div>
			</div>
		</div>
    </div>
</form>
<script type="text/javascript">
$(function(){
	//layui渲染
	layui.use('form', function() {
		var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
		form.render();         //渲染全部
	})
	layui.use('laydate', function(){
		var laydate = layui.laydate;
		laydate.render({
			elem: '#joindate',
		});
		laydate.render({
			elem: '#birthdate',
			type: 'datetime'
		});
	})
		
	//validate校验
	jQuery.validator.addMethod("mobile", function(value, element) {
		return this.optional(element) || /^0?(13|15|17|18|14|19|16)[0-9]{9}$/.test(value);
	}, "请输入正确的手机号");
	$("#itemeAddForm").validate({
		rules: {
			userName : {required:true, minlength:2},
			name     : "required",
			salary   : {required:true, range:[0,999999]},
			telephone: {required:true, mobile:true},
			email    : {required:true, email:true}
		},
		messages: {
			userName : {required:"<dl style='color:red'>请输入登录名</dl>", minlength:"<dl style='color:red'>登录名必需由两个以上字符组成</dl>"},
			name     : "<dl style='color:red'>请输入姓名</dl>",
			salary   : {required:"<dl style='color:red'>请输入薪水</dl>", range:"<dl style='color:red'>请输入薪水范围:0 ~ 999999</dl>"},
			telephone: {required:"<dl style='color:red'>请输入电话</dl>", mobile:"<dl style='color:red'>请输入正确的手机号</dl>"},
			email    : {required:"<dl style='color:red'>请输入邮箱</dl>", email:"<dl style='color:red'>请输入一个正确邮箱</dl>"}
		}
	});
})
</script>

<!-- 选择:直属领导  一个放父层中
<div id="zsldDialog" style="display:none"></div>
-->


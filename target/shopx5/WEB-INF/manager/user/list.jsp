<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>用户列表</title>
    <meta name="keywords" content='用户列表' />
    <meta name="description" content='用户列表' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui --> 
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center',title:'',fit:true,split:true,border:false">
        <table id="productDg"> </table>
	</div>
</div>
<div id="productDg-toolbar" style="padding:3px 0 3px 2px;display:none">
<c:if test="${QXmap.add == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true," onclick="toolbar.add();">新增</a>
</c:if>
<c:if test="${QXmap.edit == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-pencil',plain:true," onclick="toolbar.edit();">编辑</a>
</c:if>
<c:if test="${QXmap.del == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="iconCls:'fa fa-trash',plain:true," onclick="toolbar.remove();">删除</a>
</c:if>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-orange" data-options="iconCls:'fa fa-cloud-download',plain:true,">导入</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-red" data-options="iconCls:'fa fa fa-cloud-upload',plain:true,">导出</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-link',plain:true,">新窗口</a>
<a href="javascript:void(0);" class="easyui-menubutton topjui-btn-blue"  data-options="menu:'#exportSubMenu',iconCls:'fa fa-list',plain:true,">更多</a>
<div id="exportSubMenu" class="topjui-toolbar" style="width:150px">
	<c:if test="${QXmap.fromExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导入EXCEL列表</div></c:if>
	<c:if test="${QXmap.toExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导出EXCEL报表</div></c:if>
	<c:if test="${QXmap.email == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发邮件</div></c:if>
	<c:if test="${QXmap.sms == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发短信</div></c:if>
</div>
<div style="padding:5px;color:#333">
	查询用户名：<input type="text" name="user" class="textbox" style="width:110px">
	创建时间从：<input type="text" name="date_from" class="easyui-datebox" editable="false" style="width:110px">
	到：               <input type="text" name="date_to" class="easyui-datebox" editable="false" style="width:110px">
		<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-search'" onclick="toolbar.search();">查询</a>
		<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.reload();">刷新</a>
		<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.redo();">取消选中</a>
</div>
</div>
<script type="text/javascript">
$(function() {
	$('#productDg').datagrid({
		fit : true,         //面板大小将自适应父容器
		striped : true,     //斑马线
		rownumbers : true,  //行号
		border : false,		//面板边框
		url : '/manage/user/findGridPage',
		fitColumns : true,  //固定列   若设true,下面属性width:100,自动平均分配
		columns : [[        //纵列
   			{	field : 'userId',
   				title : '用户ID',
   				checkbox : true,
   			},
   			{	field : 'userName',
   				title : '用户名',
   				sortable : true,   //排序
   				width : 100,
   			},
			{	field : 'deptName',
				title : '所属部门',
				sortable : true,
				width : 100,
			},
			{	field : 'state',
				title : '状态',
				width : 100,
				sortable : true,
				formatter : SHOPX5.formatItemStatus,
			},
			{	field : 'userRole',
				title : '角色权限',
				width : 100,
				formatter : function (value, row, index) {
					return '<button class="bjm topjui-btn-blue layui-btn-xs" onclick="toolbar.showRole(\''+row.userId+'\')">角色权限</button>';
				}
			},
			{	field : 'updateTime',
				title : '更新日期',
				width : 100,
				sortable : true,
				formatter:SHOPX5.formatDateTime,
			},
			//扩展★
			/* {field : 'rate',
				title : '完成率',
				width : 100,
				formatter : progressFormatter,
			},*/
			{	field : 'operate',
				title : '操作',
				width : 100,
				formatter : function (value, row, index) { //单元格格式化函数，三参数：value值,row对象,index索引
			        var htmlstr  = '<c:if test="${QXmap.edit == 1 }"><button class="bjm layui-btn layui-btn-xs" onclick="toolbar.edit(\'' + row.userId + '\')">编辑</button></c:if>';
                    	htmlstr += '<c:if test="${QXmap.del == 1 }"><button class="layui-btn layui-btn-xs layui-btn-danger" onclick="toolbar.remove(\'' + row.userId + '\')">删除</button></c:if>';
		        	return htmlstr;
				}
			},
		]],
		toolbar : '#productDg-toolbar', //工具栏
		pagination : true,              //分页
		pageSize : 15,
		pageList : [15, 30, 45],
		pageNumber : 1,
		sortName : 'updateTime',      //初始化请求
		sortOrder : 'desc',
	});
	
	//--2 新增信息  --------------
	$('#manager_add').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '新增信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,                   //注意:初始窗口加载,设ture窗口关闭状态*
		iconCls : 'fa fa-windows',
		href : '/temp/manager/user/list_add', //<body> js,id不重复
		onOpen : function(){             //打开面板后
			$(".l-btn-text:contains('保存')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '保存',
			iconCls : 'fa fa-plus',
			handler : function() { //点击后,执行程序
				if($('#deptId').val() == ""){
					$.messager.alert('新增失败！', '请选择所在部门！', 'warning');
					return;
				}
		//	if ($('#itemeAddForm').form('validate')) { //form表单验证 *
			if ($('#itemeAddForm').valid()){
					//提交表单
					$.ajax({
						type : 'post',
						async : false,
						url : '/manage/user/add',
						data : $("#itemeAddForm").serialize(),
						beforeSend : function() {
							$.messager.progress({
								text : '正在新增中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : data.data + '！', //增加成功!
								});
								$('#manager_add').dialog('close');
								$('#itemeAddForm').form('reset'); //重置表单数据
								$('#itemeAddForm #szbm').next('span').remove(); $('#itemeAddForm #deptId').val("");
								$('#itemeAddForm #zsld').next('span').remove(); $('#itemeAddForm #managerId').val("");
								$('#productDg').datagrid('reload');
							} else {
								$.messager.alert('新增失败！', '未知错误导致失败，请重试！', 'warning');
							}
						},dataType:"json"
					})
				}
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_add').dialog('close'); //关闭窗口
			},
		}],
		onClose : function(){ //窗口关闭后
			//新增成功前,保留表单数据
		}
	});
	
	//--3 编辑信息  --------------
	$('#manager_edit').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '修改信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,
		iconCls : 'fa fa-windows',
		onOpen : function(){
			$(".l-btn-text:contains('更新')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '更新',
			iconCls : 'fa fa-save',
			handler : function() {
			//	if ($('#itemeEditForm').form('validate')) { //form表单验证
				if ($('#itemeEditForm').valid()){	
					//提交更新
					$.ajax({
						url : '/manage/user/update',
						type : 'post',
						data : $("#itemeEditForm").serialize(),
						beforeSend : function() {							
							$.messager.progress({
								text : '正在修改中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : '修改1个信息成功!',
								});
								$('#manager_edit').dialog('close');
								$('#productDg').datagrid('reload');
							} else {
								$.messager.alert('修改失败！', '未知错误或没有任何修改，请重试！', 'warning');
							}
						}
					});
				};
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_edit').dialog('close');
			},
		}],
		onClose : function(){
			$('#itemeEditForm').form('reset');
			$('#itemeEditForm #szbm').next('span').remove(); $('#itemeEditForm #deptId').val("");
			$('#itemeEditForm #zsld').next('span').remove(); $('#itemeEditForm #managerId').val("");
		}
	});
	
	//扩展 列_进度条progressbar
	function progressFormatter(value, row, index) {
     var htmlstr = '<div style="">';
        htmlstr += '<div class="progressbar-text" style="width:220px;height:26px;line-height:26px;">' + row.cid + '%</div>';
        htmlstr += '<div class="progressbar-value" style="width:' + row.cid + '%;height:26px;line-height:26px;">';
        htmlstr += '<div class="progressbar-text" style="width:220px;height:26px;line-height:26px;">' + row.cid + '%</div>';
        htmlstr += '</div>';
        htmlstr += '</div>';
        return htmlstr;
    };
})
//---------------------------------------------------------------------------------------------------------------------------------
	var showRole_userId = "";
	toolbar = {
		search : function() {
			$('#productDg').datagrid('load', {                   //load方法 加载和显示第一页的所有行
				title : $.trim($('input[name="user"]').val()),   //加载时额外添加这些字段
				date_from : $('input[name="date_from"]').val(),
				date_to : $('input[name="date_to"]').val(),
			});
		},
		reload : function(){
			$('#productDg').datagrid('reload');
		},
		redo : function(){
			$('#productDg').datagrid('unselectAll');
		},
		showRole : function(ID){
			showRole_userId = ID;
			$('#productDg').datagrid('unselectAll');
			$('#roleDialog').dialog({
			    title: '设置用户角色', 
			    width: 238,
			    height: 280,
			    closed: false,
			    cache: false,
			    modal: true,
			    href: "/temp/manager/user/role_zTree?id="+ID
			});
		},
		add : function(){
			//加载tree1
			//打开add窗口
			$('#manager_add').dialog('open');
			//$("input[name='title']").focus();
		},
		edit : function (logId) {
			var rows = $('#productDg').datagrid('getSelections'); //获取所有被选中行,>1警告,=1处理,=0错误
			if (rows.length > 1 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录只能选定一条数据！', 'warning');
				return ;
			};
			if (rows.length == 0 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录至少选定一条数据！', 'warning');
				return ;
			};			
			if (rows.length == 1 || logId != null) {
				if(logId != null){
					id = logId;
				}else{
					id = rows[0].userId;
				}
				$('#productDg').datagrid('unselectAll');
				$.ajax({
					url : '/manage/user/getOne',
					type : 'post',
					data : {"id" : id},
					beforeSend : function() {
						$.messager.progress({
							text : '正在获取中...',
						});
					},
					success : function (data, response, status) {
						$.messager.progress('close');
						if (data) {
							//--1 选择所在部门
							$.post("/manage/dept/getOne", {"id" : data.data.deptId}, function(data) {
									if (data.status == 200) {
										$('#itemeEditForm #szbm').next('span').remove();
										$('#itemeEditForm #szbm').after('<span style="margin-left:10px">'+data.data.deptName+'</span>');
									}
								}, "json"
							)
							//--2 加载info属性数据 格式化date
							$.post("/manage/userInfo/getOne", {"id" : id}, function(data) {
									if (data.status == 200) {
										$('#itemeEditForm').form('load', data.data);
										$('#joindate_edit').val(SHOPX5.formatDateTime(data.data.joinDate));
										$('#birthdate_edit').val(SHOPX5.formatDateTimes(data.data.birthday));
									}
									//--3 选择直属领导
									$.post("/manage/user/getOne", {"id" : data.data.managerId}, function(data) {
											if (data.status == 200) {
												$('#itemeEditForm #zsld').next('span').remove();
												$('#itemeEditForm #zsld').after('<span style="margin-left:10px">'+data.data.userName+'</span>');
											}
										}, "json"
									)
									//--4 设置性别
									if(data.data.gender == 1){
										$('#itemeEditForm input[title="男"]').attr('checked', true);
										$('#itemeEditForm input[title="女"]').removeAttr('checked');
										layuiShow(); //layui渲染 回显数据*
									}else{
										$('#itemeEditForm input[title="女"]').attr('checked', true);
										$('#itemeEditForm input[title="男"]').removeAttr('checked');
										layuiShow(); //layui渲染 回显数据
									}
								}, "json"
							)
							//--5 设置状态
							if(data.data.state == 1){
								$('#itemeEditForm [name="stateON"]').attr('checked', true);
								layuiShow(); //layui渲染 回显数据
							}else{
								$('#itemeEditForm [name="stateON"]').removeAttr('checked');
								layuiShow(); //layui渲染 回显数据
							}
							//--6 加载user属性数据 打开对话框
							$('#manager_edit').form('load', data.data);
							layuiShow(); //layui渲染 回显数据
							$('#manager_edit').dialog('open');
						} else {
							$.messager.alert('获取失败！', '未知错误导致失败，请重试！', 'warning');
						};
					}
				})
			};
		},
		remove : function (id) {
		alert('权限限制!');return;//***---+++
			var rows = $('#productDg').datagrid('getSelections');
			if (rows.length > 0 || id != null) {
				$.messager.confirm('确定操作', '您正在要删除所选的记录吗？', function (flag) { //确定执行删除
					if (flag) {
						var ids = [];
						for (var i = 0; i < rows.length; i ++) {
							ids.push(rows[i].userId);
						}
						if(id != null){
							sid = id;
						}else{
							sid = ids.join(',');
						}
						$.ajax({
							type : 'POST',
							url : '/manage/user/delete',
							data : { "ids" : sid },
							beforeSend : function() {
								$('#productDg').datagrid('loading');
							},
							success : function (data) {
								if (data) {
									$('#productDg').datagrid('loaded');
									$('#productDg').datagrid('load'); //首列
									$.messager.show({
										title : '提示',
										msg : data.data + '！', //删除成功！
									});
								}
							},
						});
					};
				});
			} else {
				$.messager.alert('提示', '请选择要删除的记录！', 'info');
			};
		},
		//自定义方法
		myQuery : function(){
			// 提示信息
	        $.messager.alert('自定义方法', '自定义方法被执行了！', 'messager-info');
	        var checkedRows = $('#productDg').datagrid('getChecked');  //在复选框被选中时返回所有行 
	        var selectedRow = $('#productDg').datagrid('getSelected'); //返回第一个被选中的行 或没有选中的行返回null。
	        // 提交参数查询表格数据 username=?
	        $('#productDg').datagrid('reload', {
	            username: $('#username').textbox('getValue'),
	        });			
		},
	};
</script>

<!-- /////////////////////////////////////////////////////////// -->

<!-- add块 -->
<div id="manager_add" style="display:none"></div>
<!-- edit块 -->
<div id="manager_edit" style="padding:3px 0 0 20px;display:none">
<form class="layui-form itemForm" id="itemeEditForm" method="post">
	<input type="hidden" name="userId" />
	<input type="hidden" name="userInfoId" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
          <div class="layui-form-item">
			<label class="layui-form-label">所在部门</label>
			<div class="layui-input-block">
             <!-- <select name="deptId">
               <option value="">--请选择--</option>
             </select> -->
			 <input type="button" class="layui-btn layui-btn-sm" id="szbm" onclick="getSzbm('manager_edit');" value="选择" />
		     <input type="hidden" name="deptId" id="deptId" />			              
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">登录名</label>
			<div class="layui-input-block">
			  <input type="text" name="userName" placeholder="请输入登录名" autocomplete="off" class="layui-input">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">状态</label>
			<div class="layui-input-block">
			  <input type="checkbox" name="stateON" lay-skin="switch">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">姓名</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入姓名" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">薪水</label>
			<div class="layui-input-block">
			  <input type="text" name="salary" placeholder="请输入薪水" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">入职时间</label>
			<div class="layui-input-block">
			  <input type="text" name="joinDate" placeholder="请输入入职时间" autocomplete="off" class="layui-input" id="joindate_edit">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
		  <div class="layui-form-item">
			<label class="layui-form-label">直属领导</label>
			<div class="layui-input-block">
             <!-- <select name="managerId">               
             </select> -->
             <input type="button" class="layui-btn layui-btn-sm" id="zsld" onclick="getZsld(2);" value="选择" />
		     <input type="hidden" name="managerId" id="managerId" />		     
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">出生年月</label>
			<div class="layui-input-block">
			  <input type="text" name="birthday" placeholder="请输入出生年月" autocomplete="off" class="layui-input" id="birthdate_edit">
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block">
			  <input type="radio" name="gender" value="1" title="男">
			  <input type="radio" name="gender" value="0" title="女">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">岗位</label>
			<div class="layui-input-block">
			  <input type="text" name="station" placeholder="请输入岗位" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">电话</label>
			<div class="layui-input-block">
			  <input type="text" name="telephone" placeholder="请输入电话" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">邮箱</label>
			<div class="layui-input-block">
			  <input type="text" name="email" placeholder="请输入邮箱" autocomplete="off" class="layui-input">
			</div>
		  </div>      		  
</div>
		  <div class="layui-form-item">
			<label class="layui-form-label">等级</label>
			<div class="layui-input-block">
			  <input type="radio" name="degree" value="0" title="超级管理员">
			  <input type="radio" name="degree" value="1" title="跨部门跨人员">
              <input type="radio" name="degree" value="2" title="管理所有下属部门和人员">
              <input type="radio" name="degree" value="3" title="管理本部门">
              <input type="radio" name="degree" value="4" title="普通员工" checked>
			</div>
		  </div>		  
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
			<div class="layui-form-item">
				<label class="layui-form-label">排序号</label>
				<div class="layui-input-block">
				  <input type="text" name="orderNo" placeholder="请输入排序号" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item layui-form-text">
				<label class="layui-form-label">说明</label>
				<div class="layui-input-block">
				  <textarea name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
				</div>
			</div>
		</div>
    </div>
</form>
<script type="text/javascript">
//--layui渲染 用于回显数据时渲染 也起渲染整表单作用 ★
function layuiShow(){
	layui.use('form', function() {
		var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
		form.render();         //渲染全部
	})	
}
$(function(){
	//layui渲染日期
	layui.use('laydate', function(){
		var laydate = layui.laydate;
		laydate.render({
			elem: '#joindate_edit',
		});
		laydate.render({
			elem: '#birthdate_eidt',
			type: 'datetime'
		});
	})
		
	//validate校验
	jQuery.validator.addMethod("mobile", function(value, element) {
		return this.optional(element) || /^0?(13|15|17|18|14|19|16)[0-9]{9}$/.test(value);
	}, "请输入正确的手机号");
	$("#itemeEditForm").validate({
		rules: {
			userName : {required:true, minlength:2},
			name     : "required",
			salary   : {required:true, range:[0,999999]},
			telephone: {required:true, mobile:true},
			email    : {required:true, email:true}
		},
		messages: {
			userName : {required:"<dl style='color:red'>请输入登录名</dl>", minlength:"<dl style='color:red'>登录名必需由两个以上字符组成</dl>"},
			name     : "<dl style='color:red'>请输入姓名</dl>",
			salary   : {required:"<dl style='color:red'>请输入薪水</dl>", range:"<dl style='color:red'>请输入薪水范围:0 ~ 999999</dl>"},
			telephone: {required:"<dl style='color:red'>请输入电话</dl>", mobile:"<dl style='color:red'>请输入正确的手机号</dl>"},
			email    : {required:"<dl style='color:red'>请输入邮箱</dl>", email:"<dl style='color:red'>请输入一个正确邮箱</dl>"}
		}
	});

	//取直属领导  1 下拉
/* 	$.ajax({type:"post", url:"/manage/user/findAll", async:false, dataType:"json", 
	    success: function(data) {
	    	if (data.status == 200) {
				var list = data.data;
				var html = '<option value="">--请选择--</option>';
				for(var i in list){
					var user = list[i];
					html += '<option value="'+user.userId+'">'+user.userName+'</option>';
				}
				$('[name="managerId"]').append(html);
			}
	    }
	}) */
})

//取直属领导  2 zTree
var of = "";
function getZsld(i){
	if(i == 1){ of = "manager_add"; }else{ of = "manager_edit"; } //1添加 2更新
	$('#zsldDialog').dialog({
	    title: '选择直属领导', 
	    width: 400,
	    height: 500,
	    closed: false,
	    cache: false,
	    modal: true,
	    href: "/temp/manager/user/add_zTree"
	});
}
//取所在部门  3 tree  easyui
function getSzbm(aeid){
	$("<div>").css({padding:"5px"}).html("<ul></ul>").window({
		width : '300',
	    height : '380',
	    title : '选择所属部门',
	    iconCls : 'icon-save',
	    modal : true,
	    closed : true,
	    onOpen : function(){
	    	var _win = this;                                 //this <div><ul>_DOM
	    	$("ul", _win).tree({                             //子,父 父内找子
	    		url : '/manage/dept/findDeptEUtree',        //异步数据[id text state]
	    		animate : true,
	    		onClick : function(node){                    //点击父节点,发送请求,展开父节点*
	    		//	if($(this).tree("isLeaf", node.target)){ //点击是叶子节点,$(this)=$("ul")
	    				$('#'+aeid+' input[name="deptId"]').val(node.id);
	    				$('#'+aeid+' #szbm').next('span').remove();
	    				$('#'+aeid+' #szbm').after('<span style="margin-left:10px">'+node.text+'</span>');
	    				$(_win).window('close');
	    		//	}
	    		}
	    	})
	    }
	}).window('open');
}
</script>

<!-- 选择:直属领导 -->
<div id="zsldDialog" style="display:none"></div>
<!-- 选择:用户角色 -->
<div id="roleDialog" style="display:none"></div>

</div>
</body>
</html>






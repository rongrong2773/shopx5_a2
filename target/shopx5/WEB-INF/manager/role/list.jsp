<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><!-- 避免IE使用兼容模式 -->
    <meta name="renderer" content="webkit" />
    <title>角色列表</title>
    <meta name="keywords" content='角色列表' />
    <meta name="description" content='角色列表' />
    <link rel="shortcut icon" href="/static/shopx5ui/topjui/images/favicon.ico" />
    <link type="text/css" href="/static/shopx5ui/topjui/css/topjui.core.min.css" rel="stylesheet" />
    <link type="text/css" href="/static/shopx5ui/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme" />
    <link type="text/css" href="/static/shopx5ui/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- FontAwesome字体图标 -->
	<link type="text/css" href="/static/shopx5ui/easyui_admin.css" rel="stylesheet" />
    <!-- easyui -->
	<script type="text/javascript" src="/cpts/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="/static/shopx5ui/static/plugins/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="/cpts/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/cpts/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/cpts/easyui/easyui_index.js"></script>
    <!-- layui -->
    <link type="text/css" href="/cpts/layui-v2.4.3/css/layui.css" rel="stylesheet" />
    <script type="text/javascript" src="/cpts/layui-v2.4.3/layui.js" charset="utf-8"></script>
    <!-- jquery.validate -->
    <script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/messages_zh.js"></script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center',title:'',fit:true,split:true,border:false">
        <table id="productDg"> </table>
	</div>
</div>
<div id="productDg-toolbar" style="padding:3px 0 3px 2px;display:none">
<c:if test="${QXmap.add == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true," onclick="toolbar.add();">新增</a>
</c:if>
<c:if test="${QXmap.edit == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-pencil',plain:true," onclick="toolbar.edit();">编辑</a>
</c:if>
<c:if test="${QXmap.del == 1 }">
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="iconCls:'fa fa-trash',plain:true," onclick="toolbar.remove();">删除</a>
</c:if>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-orange" data-options="iconCls:'fa fa-cloud-download',plain:true,">导入</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-red" data-options="iconCls:'fa fa fa-cloud-upload',plain:true,">导出</a>
<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-green" data-options="iconCls:'fa fa-link',plain:true,">新窗口</a>
<a href="javascript:void(0);" class="easyui-menubutton topjui-btn-blue"  data-options="menu:'#exportSubMenu',iconCls:'fa fa-list',plain:true,">更多</a>
<div id="exportSubMenu" class="topjui-toolbar" style="width:150px">
	<c:if test="${QXmap.fromExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导入EXCEL列表</div></c:if>
	<c:if test="${QXmap.toExcel == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">导出EXCEL报表</div></c:if>
	<c:if test="${QXmap.email == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发邮件</div></c:if>
	<c:if test="${QXmap.sms == 1 }"><div data-options="iconCls:'fa fa-file-excel-o',">发短信</div></c:if>
</div>
<div style="padding:5px;color:#333">
	查询名称：    <input type="text" name="user" class="textbox" style="width:110px">
	创建时间从：<input type="text" name="date_from" class="easyui-datebox" editable="false" style="width:110px">
	到：               <input type="text" name="date_to" class="easyui-datebox" editable="false" style="width:110px">
	<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa fa-search'" onclick="toolbar.search();">查询</a>
	<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.reload();">刷新</a>
	<a href="javascript:void(0);" class="easyui-linkbutton topjui-btn-brown" data-options="" onclick="toolbar.redo();">取消选中</a>
</div>
</div>
<script type="text/javascript">
$(function() {
	$('#productDg').datagrid({
		fit : true,         //面板大小将自适应父容器
		striped : true,     //斑马线
		rownumbers : true,  //行号
		border : false,		//面板边框
		url : '/manage/role/findGridPage',
		fitColumns : true,  //固定列   若设true,下面属性width:100,自动平均分配
		columns : [[        //纵列
   			{	field : 'roleId',
   				title : 'ID',
   				checkbox : true,
   			},
   			{	field : 'name',
   				title : '名称',
   				sortable : true,   //排序
   				width : 100,
   			},
			{	field : 'remark',
				title : '说明',
				sortable : true,
				width : 100,
			},
			{	field : 'addQX',
				title : '增',
				width : 23,
				formatter : function (value, row, index) {
					return '<button class="bjm topjui-btn-blue layui-btn-xs" onclick="toolbar.CURD_QX(\''+row.roleId+'\',\'addQX\')">增</button>';
				}
			},
			{	field : 'delQX',
				title : '删',
				width : 23,
				formatter : function (value, row, index) {
					return '<button class="bjm topjui-btn-red layui-btn-xs" onclick="toolbar.CURD_QX(\''+row.roleId+'\',\'delQX\')">删</button>';
				}
			},
			{	field : 'editQX',
				title : '改',
				width : 23,
				formatter : function (value, row, index) {
					return '<button class="bjm layui-btn layui-btn-xs" onclick="toolbar.CURD_QX(\''+row.roleId+'\',\'editQX\')">改</button>';
				}
			},
			{	field : 'seeQX',
				title : '查',
				width : 23,
				formatter : function (value, row, index) {
					return '<button class="bjm topjui-btn-brown layui-btn-xs" onclick="toolbar.CURD_QX(\''+row.roleId+'\',\'seeQX\')">查</button>';
				}
			},
			{	field : 'category',
				title : '进入类别',
				sortable : true,
				width : 50,
				formatter : toolbar.formatCategory,
			},
			{	field : 'roleModule',
				title : '操作模块',
				width : 50,
				formatter : function (value, row, index) {
					return '<button class="bjm layui-btn layui-btn-xs" onclick="toolbar.showModule(\'' + row.roleId + '\')">模块权限</button>';
				}
			},
			{	field : 'roleButton',
				title : '操作按钮',
				width : 50,
				formatter : function (value, row, index) {
					return '<button class="bjm topjui-btn-brown layui-btn-xs" onclick="toolbar.showButton(\''+row.roleId+'\')">按钮权限</button>';
				}
			},
			/* {	field : 'state',
				title : '状态',
				width : 100,
				sortable : true,
				formatter : SHOPX5.formatItemStatus,
			}, */
			{	field : 'updateTime',
				title : '更新日期',
				width : 100,
				sortable : true,
				formatter:SHOPX5.formatDateTime,
			},
			{	field : 'operate',
				title : '操作',
				width : 100,
				formatter : function (value, row, index) { //单元格格式化函数，三参数：value值,row对象,index索引
			        var htmlstr  = '<c:if test="${QXmap.edit == 1 }"><button class="bjm layui-btn layui-btn-xs" onclick="toolbar.edit(\'' + row.roleId + '\')">编辑</button></c:if>';
                    	htmlstr += '<c:if test="${QXmap.del == 1 }"><button class="layui-btn layui-btn-xs layui-btn-danger" onclick="toolbar.remove(\'' + row.roleId + '\')">删除</button></c:if>';
		        	return htmlstr;
				}
			},
		]],
		toolbar : '#productDg-toolbar', //工具栏
		pagination : true,              //分页
		pageSize : 10,
		pageList : [10, 20, 30],
		pageNumber : 1,
		sortName : 'ORDER_NO',      //初始化请求
		sortOrder : 'asc',
	});
	
	//--2 新增信息  --------------
	$('#manager_add').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '新增信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,                   //注意:初始窗口加载,设ture窗口关闭状态*
		iconCls : 'fa fa-windows',
		href : '/temp/manager/role/list_add', //<body> js,id不重复
		onOpen : function(){             //打开面板后
			$(".l-btn-text:contains('保存')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '保存',
			iconCls : 'fa fa-plus',
			handler : function() { //点击后,执行程序
		//	if ($('#itemeAddForm').form('validate')) { //form表单验证 *
			if ($('#itemeAddForm').valid()){
					//提交表单
					$.ajax({
						type : 'post',
						async : false,
						url : '/manage/role/add',
						data : $("#itemeAddForm").serialize(),
						beforeSend : function() {
							$.messager.progress({
								text : '正在新增中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : data.data + '！', //增加成功!
								});
								$('#manager_add').dialog('close');
								$('#itemeAddForm').form('reset'); //重置表单数据
								$('#productDg').datagrid('reload');
							} else {
								$.messager.alert('新增失败！', '未知错误导致失败，请重试！', 'warning');
							}
						},dataType:"json"
					})
				}
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_add').dialog('close'); //关闭窗口
			},
		}],
		onClose : function(){ //窗口关闭后
			//新增成功前,保留表单数据
		}
	});
	
	//--3 编辑信息  --------------
	$('#manager_edit').dialog({
		collapsible : true,
		maximizable : true,
		resizable : true,
		title : '修改信息',
		width : 770,
		height : 470,
		left : 150,
		top : 70,
		modal : true,
		closed : true,
		iconCls : 'fa fa-windows',
		onOpen : function(){
			$(".l-btn-text:contains('更新')").parent().parent().addClass("topjui-btn-green");
			$(".l-btn-text:contains('取消')").parent().parent().addClass("topjui-btn-red");
		},
		buttons : [{
			text : '更新',
			iconCls : 'fa fa-save',
			handler : function() {
			//	if ($('#itemeEditForm').form('validate')) { //form表单验证
				if ($('#itemeEditForm').valid()){	
					//提交更新
					$.ajax({
						url : '/manage/role/update',
						type : 'post',
						data : $("#itemeEditForm").serialize(),
						beforeSend : function() {							
							$.messager.progress({
								text : '正在修改中...',
							});
						},
						success : function (data, response, status) {
							$.messager.progress('close');
							if (data.status == 200) {
								$.messager.show({
									title : '提示',
									msg : '修改1个信息成功!',
								});
								$('#manager_edit').dialog('close');
								$('#productDg').datagrid('reload');
							} else {
								$.messager.alert('修改失败！', '未知错误或没有任何修改，请重试！', 'warning');
							}
						}
					});
				};
			},
		},{
			text : '取消',
			iconCls : 'fa fa-close',
			handler : function() {
				$('#manager_edit').dialog('close');
			},
		}],
		onClose : function(){
			$('#itemeEditForm').form('reset');
		}
	});
})
//---------------------------------------------------------------------------------------------------------------------------------
	var showRole_userId = ""; // showModule : function(ID){ showRole_userId = ID; }
	toolbar = {
		search : function() {
			$('#productDg').datagrid('load', {                   //load方法 加载和显示第一页的所有行
				title : $.trim($('input[name="user"]').val()),   //加载时额外添加这些字段
				date_from : $('input[name="date_from"]').val(),
				date_to : $('input[name="date_to"]').val(),
			});
		},
		reload : function(){
			$('#productDg').datagrid('reload');
		},
		redo : function(){
			$('#productDg').datagrid('unselectAll');
		},
		formatCategory : function(val, row){
			if (val == 1){
	            return '<span style="color:">系统后台</span>';
	        } else if(val == 2){
	        	return '<span style="color:">商城后台</span>';
	        } else {
	        //	return '关闭';
	        }
		},
		//选择1 模块权限 *
		showModule : function(ID){
			$('#productDg').datagrid('unselectAll');
			$('#ModuleDialog').dialog({
			    title: '设置模块权限', 
			    width: 300,
			    height: 420,
			    closed: false,
			    cache: false,
			    modal: true,
			    href: "/temp/manager/role/module_zTree?id="+ID
			});
		},
		//选择2 按钮权限 *
		showButton: function(ID){
			$('#productDg').datagrid('unselectAll');
			$('#ButtonDialog').dialog({
			    title: '设置按钮权限', 
			    width: 300,
			    height: 420,
			    closed: false,
			    cache: false,
			    modal: true,
			    href: "/temp/manager/role/button_zTree?id="+ID
			});
		},
		//选择4 增加权限 CURD ****
		CURD_QX : function(ID, curdQX){
			$('#productDg').datagrid('unselectAll');
			$('#QXDialog').dialog({
			    title: '设置用户增上改查权限', 
			    width: 300,
			    height: 420,
			    closed: false,
			    cache: false,
			    modal: true,
			    href: "/temp/manager/role/QX_zTree?id="+ID+"&curdQX="+curdQX
			});
		},
		add : function(){
			//打开add窗口
			$('#manager_add').dialog('open');
			//$("#itemeAddForm input[name='name']").focus();
		},
		edit : function (logId) {
			var rows = $('#productDg').datagrid('getSelections'); //获取所有被选中行,>1警告,=1处理,=0错误
			if (rows.length > 1 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录只能选定一条数据！', 'warning');
				return ;
			};
			if (rows.length == 0 && logId == null) {
				$.messager.alert('警告操作！', '编辑记录至少选定一条数据！', 'warning');
				return ;
			};			
			if (rows.length == 1 || logId != null) {
				if(logId != null){
					id = logId;
				}else{
					id = rows[0].roleId;
				}
				$('#productDg').datagrid('unselectAll');
				$.ajax({
					url : '/manage/role/getOne',
					type : 'post',
					data : {"id" : id},
					beforeSend : function() {
						$.messager.progress({
							text : '正在获取中...',
						});
					},
					success : function (data, response, status) {
						$.messager.progress('close');
						if (data) {
							//--加载user属性数据 打开对话框
							$('#manager_edit').form('load', data.data);
							layuiShow(); //layui渲染 回显数据
							$('#manager_edit').dialog('open');
						} else {
							$.messager.alert('获取失败！', '未知错误导致失败，请重试！', 'warning');
						};
					}
				})
			};
		},
		remove : function (id) {
		alert('权限限制!');return;//***---+++
			var rows = $('#productDg').datagrid('getSelections');
			if (rows.length > 0 || id != null) {
				$.messager.confirm('确定操作', '您正在要删除所选的记录吗？', function (flag) { //确定执行删除
					if (flag) {
						var ids = [];
						for (var i = 0; i < rows.length; i ++) {
							ids.push(rows[i].roleId);
						}
						if(id != null){
							sid = id;
						}else{
							sid = ids.join(',');
						}
						$.ajax({
							type : 'POST',
							url : '/manage/role/delete',
							data : { "ids" : sid },
							beforeSend : function() {
								$('#productDg').datagrid('loading');
							},
							success : function (data) {
								if (data) {
									$('#productDg').datagrid('loaded');
									$('#productDg').datagrid('load'); //首列
									$.messager.show({
										title : '提示',
										msg : data.data + '！', //删除成功！
									});
								}
							},
						});
					};
				});
			} else {
				$.messager.alert('提示', '请选择要删除的记录！', 'info');
			};
		},
	};
</script>

<!-- /////////////////////////////////////////////////////////// -->

<!-- add块 -->
<div id="manager_add" style="display:none"></div>
<!-- edit块 -->
<div id="manager_edit" style="padding:3px 0 0 20px;display:none">
<form class="layui-form itemForm" id="itemeEditForm" method="post">
	<input type="hidden" name="roleId" />
    <div class="easyui-tabs" data-options="border:false,"><!-- tabs -->
    	<div title="必填信息" data-options="iconCls:'fa fa-th'" style="padding:10px 0 0 0"><!-- tabs1 -->
<div style="width:360px;float:left">
          <div class="layui-form-item">
			<label class="layui-form-label">名称</label>
			<div class="layui-input-block">
			  <input type="text" name="name" placeholder="请输入名称" autocomplete="off" class="layui-input">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">描述</label>
			<div class="layui-input-block">
			  <textarea name="remark" placeholder="请输入描述" autocomplete="off" class="layui-textarea"></textarea>
			</div>
		  </div>
		  <div class="layui-form-item">
			<label class="layui-form-label">类别</label>
			<div class="layui-input-block">
			  <input type="radio" name="category" value="1" title="系统后台">
			  <input type="radio" name="category" value="2" title="商城后台">
			</div>
		  </div>
          <div class="layui-form-item">
			<label class="layui-form-label">排序号</label>
			<div class="layui-input-block">
			  <input type="text" name="orderNo" placeholder="请输入排序号" autocomplete="off" class="layui-input">
			</div>
		  </div>
</div>
<div style="width:370px;float:left">
</div>
		</div>
		<div title="选填信息" data-options="iconCls:'fa fa-th'" style="padding:5px 0 0 20px"><!-- tabs2 -->
		</div>
    </div>
</form>
<script type="text/javascript">
//--layui回显数据时渲染
function layuiShow(){
	layui.use('form', function() {
		var form = layui.form;
		form.render();
	})
}
//validate校验
$("#itemeEditForm").validate({
	rules: {
		name     : "required",
		orderNo  : "digits",
	},
	messages: {
		name     : "<dl style='color:red'>请输入名称</dl>",
		orderNo  : "<dl style='color:red'>请输入输入整数</dl>",
	}
})
</script>

<!-- 设置模块权限 -->
<div id="ModuleDialog" style="display:none"></div>
<!-- 设置按钮权限 -->
<div id="ButtonDialog" style="display:none"></div>
<!-- 设置CURD权限 -->
<div id="QXDialog" style="display:none"></div>

</div>
</body>
</html>







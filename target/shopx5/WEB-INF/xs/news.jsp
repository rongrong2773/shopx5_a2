<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../base.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>资讯列表</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"	name="viewport">
<link rel="stylesheet" href="/shopx5/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet" href="/shopx5/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/shopx5/css/style.css">
<script src="/shopx5/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/shopx5/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 引入angular的JS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/angular.min.js"></script>
<!-- 引入分页相关的JS和CSS -->
<script type="text/javascript" src="/shopx5/plugins/angularjs/pagination.js"></script>
<link rel="stylesheet" href="/shopx5/plugins/angularjs/pagination.css">
<!-- 相关angular业务 -->
<script type="text/javascript" src="/shopx5/js/base_pagination.js"></script>
<script type="text/javascript" src="/shopx5/js/controller/baseController.js"></script>

<!-- <script type="text/javascript" src="/shopx5/js/controller/goodsController2.js"></script>
<script type="text/javascript" src="/shopx5/js/service/goodsService2.js"></script> -->

<script type="text/javascript" src="/shopx5/js/controller/newsTController.js"></script>
<script type="text/javascript" src="/shopx5/js/service/newsTService.js"></script>

<script type="text/javascript" src="/shopx5/js/service/itemCatService.js"></script>
<script type="text/javascript" src="/shopx5/js/service/uploadService2.js"></script>
<script type="text/javascript" src="/shopx5/js/service/templateTypeService.js"></script>
</head>

<body class="hold-transition skin-red sidebar-mini" ng-app="shopx5"	ng-controller="newsController" ng-init="findItemCatList()">
	<!-- .box-body -->
	<div class="box-header with-border">
		<h3 class="box-title">资讯管理</h3>
	</div>
	<div class="box-body">
		<!-- 数据表格 -->
		<div class="table-box">
			<!--工具栏-->
			<div class="pull-left">
				<div class="form-group form-inline">
					<div class="btn-group">
						<a href="news_edit.html?type=add" class="btn btn-default">新建</a>
						<a id="showAdd" class="btn btn-default">新建show</a>
						<button type="button" class="btn btn-default" ng-click="dele()" title="删除">
							<i class="fa fa-trash-o"></i> 删除
						</button>
						<button type="button" class="btn btn-default" ng-click="updateMarketable('1')" title="上架">
							<i class="fa fa-ban"></i> 上架
						</button>
						<button type="button" class="btn btn-default" ng-click="updateMarketable('0')" title="下架">
							<i class="fa fa-ban"></i> 下架
						</button>
						<button type="button" class="btn btn-default" title="刷新"
							onclick="window.location.reload();">
							<i class="fa fa-refresh"></i> 刷新
						</button>
					</div>
				</div>
			</div>
			<div class="box-tools pull-right">
				<div class="has-feedback">
					状态：<select ng-model="searchEntity.auditStatus">
						<option value="">全部</option>
						<option value="0">未申请</option>
						<option value="1">审核通过</option>
						<option value="2">审核未通过</option>
						<option value="3">关闭</option>
					</select> 商品名称：<input type="text" ng-model="searchEntity.goodsName">
					<button class="btn btn-default" ng-click="reloadList()">查询</button>
				</div>
			</div>
			<!--工具栏/-->
			<!--数据列表-->
			<table id="dataList"
				class="table table-bordered table-striped table-hover dataTable">
				<thead>
					<tr>
						<th class="" style="padding-right:0px"><input id="selall" type="checkbox" class="icheckbox_square-blue" ng-model="master" ng-click="checkAll(master,allIds)"></th>
						<th class="sorting_asc">资讯ID</th>
						<th class="sorting">资讯标题</th>
						<th class="sorting">发布者</th>
						<th class="sorting">热度</th>
						<th class="sorting">分享次数</th>
						<th class="sorting">讨论</th>
						<th class="sorting">议题</th>
						<th class="sorting">上架状态</th>
						<th class="sorting">审核状态</th>   <!-- 0-通过；1-驳回 -->
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="entity in list" ng-cloak>
						<td><input type="checkbox" ng-checked="master" ng-model="c" ng-click="check($event,entity.id,c)"></td>
						<td>{{entity.id}}</td>
						<td>{{entity.title}}</td>
						<td>{{entity.publisher}}</td>
						<td>{{entity.heat}}</td>
						<td>{{entity.sharedtimes}}</td>
						<td>****</td>
						<td>****</td>
						<td>{{entity.state}}</td>
						<td><span>{{entity.checkState}}</span></td>
						<td class="text-center"> 
							<a class="btn bg-olive btn-xs" href="/shop/{{entity.id}}.htm" target="_blank">查看</a>
							<a class="btn bg-olive btn-xs" href="news_edit.html#?id={{entity.id}}&type=edit">修改</a>
						</td>
					</tr>
				</tbody>
			</table>
			<!--数据列表/-->
		</div>
		<!-- 数据表格 /-->
		<!-- 分页 -->
		<tm-pagination conf="paginationConf"></tm-pagination>
	</div>
	
	<!-- /.box-body -->
	<script type="text/javascript">
	$(function(){
		$('#showAdd').on('click', function(){
			if(window.parent.showAdd==''){
				location.href='news_edit';
			}else{
				window.parent.showAdd('商品增加','/temp/xs/test/news_edit');
			}
		})
	})
	</script>
</body>
</html>



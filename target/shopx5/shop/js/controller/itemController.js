app.controller("itemController",function($scope,$http,itemService,commentService){

	//数量加减 1
	$scope.addNum = function(x){
		$scope.num+=x;
		if($scope.num<1){
			$scope.num=1;
		}		
	}
	
	//存储用户选择的规格 2
	$scope.specificationItems = {};                  //###### 2 默认初始值
	//用户选择规格 2.1
	$scope.selectSpecification = function(key,value){
		$scope.specificationItems[key]=value;		
		searchSku(); //查询SKU 3.3
	}
	
	//判断某规格是否被选中 2.2
	$scope.isSelected = function(key,value){
		if($scope.specificationItems[key]==value){
			return true;
		}else{
			return false;
		}
	}
	
	//当前选择的SKU 3
	$scope.sku = {};
	//加载默认SKU 3.1
	$scope.loadSku=function(){
		$scope.sku=skuList[0]; //skuList在页面定义的,是全局引用*          ###### 1
		$scope.specificationItems=JSON.parse(JSON.stringify($scope.sku.spec)); //转为默认选择 深克隆★
	}
	
	//匹配两个对象是否相等 3.2  map2为已选择
	matchObject = function(map1,map2){
		for(var k in map1){
			if(map1[k]!=map2[k]){ //{'网络':'移动3G','机身内存':16G}
				return false;     //{'网络':'移动3G','机身内存':16G,'颜色':'土豪金'} 相互找对比,要都一样对象*
			}
		}
		for(var k in map2){
			if(map2[k]!=map1[k]){
				return false;
			}
		}
		return true;
	}
	//根据规格查询sku 3.3
	searchSku = function(){
		for(var i=0;i<skuList.length;i++){ //两个规格对象匹配,有相同显示sku价格
			 if(matchObject(skuList[i].spec, $scope.specificationItems)){
				 $scope.sku=skuList[i];
				 return ; //匹配到跳出循环
			 }
		}
		$scope.sku={id:0,title:'---',price:0}; //为找到数据时
	}
	
	
	//添加商品到购物车 4
	$scope.addToCart = function(){
		$http.get('/shopx5/cart/addGoodsToCartList?itemId='+$scope.sku.id+'&num='+$scope.num, {'withCredentials':true}).success(
			function(response){
				if(response.success){
					location.href='/shop/cart.htm';
				}else{
					alert(response.message);
				}
			}
		);
	}
	
	//保存评价
	$scope.saveComment = function(){
		commentService.add($scope.comment).success(function(response){
			if(response.flag==true){
				alert(response.message);//更新成功
			}else{
				alert(response.message);//更新失败
			}
		});
	}
	
	//获取SPU评论数据 根据goodsId
	$scope.findByGoodsId = function(){
		commentService.findByGoodsId($scope.goodsId).success(function(response){
			$scope.commentList = response;
		});
	}
	
	//实时库存 默认加载 
	$scope.count = 0;
	//实时监控sku变化时
	$scope.$watch("sku.id",function(newValue, oldValue){
		itemService.findOne(newValue).success(function(response){
			$scope.count = response.num;
		});
	});
	
	//获取商品图片 image_json
	
});















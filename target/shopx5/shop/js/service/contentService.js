// 定义服务层:
app.service("contentService",function($http){

	// 根据分类ID获取轮播图
	this.findByCategoryId = function(categoryId){
		return $http.post("/content/findByCategoryId?categoryId="+categoryId);
	}
});

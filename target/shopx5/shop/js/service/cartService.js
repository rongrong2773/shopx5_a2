//购物车服务层
app.service('cartService',function($http){
	//购物车列表 1
	this.findCartList = function(){
		return $http.get('/shopx5/cart/findCartList');
	}
	//购物车已选列表
	this.findCartYesList = function(){
		return $http.get('/shopx5/cart/findCartYesList');
	}
	//求合计数 1.1
	this.sum = function(cartList){
		var totalValue={totalNum:0,totalMoney:0};
		for(var i=0;i<cartList.length;i++){
			var cart=cartList[i];//购物车对象
			for(var j=0;j<cart.orderItemList.length;j++){
				var orderItem=cart.orderItemList[j];//购物车明细
				totalValue.totalNum+=orderItem.num;       //累加数量
				totalValue.totalMoney+=orderItem.totalFee;//累加金额
			}
		}
		return totalValue;
	}
	//求已选合计数 1.2
	this.sumCk = function(cartList,itemIdsNo){
		var totalValue={totalNum:0,totalMoney:0};
		for(var i=0;i<cartList.length;i++){
			var cart=cartList[i];//购物车对象
			for(var j=0;j<cart.orderItemList.length;j++){
				var orderItem=cart.orderItemList[j];//购物车明细
				if(itemIdsNo.indexOf(orderItem.itemId) == -1){
					totalValue.totalNum+=orderItem.num;       //累加数量
					totalValue.totalMoney+=orderItem.totalFee;//累加金额
				}
			}
		}
		return totalValue;
	}
	//封装所有已选itemIds
	this.sumIds = function(cartList){
		var itemIdsYes=[];
		for(var i=0;i<cartList.length;i++){
			var cart=cartList[i];//购物车对象
			for(var j=0;j<cart.orderItemList.length;j++){
				var orderItem=cart.orderItemList[j];//购物车明细
				itemIdsYes.push(orderItem.itemId);  //累加itemId
			}
		}
		return itemIdsYes;
	}
	//数量加减时,添加商品到购物车 2
	this.addGoodsToCartList = function(itemId,num){
		return $http.get('/shopx5/cart/addGoodsToCartList?itemId='+itemId+'&num='+num);
	}
	
	//将itemIdsYes存入cookie []
	this.addItemIdsYes = function(itemIdsYes){
		return $http.get('/shopx5/cart/addItemIdsYes?itemIds='+itemIdsYes);
	}
	
	
	//获取当前登录账号的收货地址 3
	this.findAddressList = function(){
		return $http.get('/shop/index/addr/findListByLoginUser');
	}
	//地址更新: 新增/修改
	this.addrUpdate = function(address){
		return $http.post('/shopx5/index/addr/updateJ',address);
	}
	this.addrAdd = function(address){
		return $http.post('/shopx5/index/addr/addJ',address);
	}
			
	//提交订单 4
	this.submitOrder = function(order){
		return $http.post('/shopx5/order/add',order);		
	}
	
	//获取支付日志 5
	this.findPayData = function(){
		return $http.get('/shopx5/payLog/findPayData');
	}
	this.findOrder = function(orderId){
		return $http.get('/shopx5/order/findOne?id='+orderId);
	}
	
	//删除
	this.dropCartItem = function(itemId){
		return $http.get('/shopx5/cart/delete/'+itemId);
	}
	//收藏
	this.collectGoods = function(goodsId){
		return $http.get('/collect/add?goodsId='+goodsId);
	}
});


